import { LoginPage } from './login.po';
import { browser } from 'protractor';
import { async } from '@angular/core/testing';

describe('Login tests', () => {
    let page: LoginPage;
    beforeEach(() => {
        page = new LoginPage();
    });

    it('Login form should be invalid', () => {
        page.navigateTo();
        page.getEmailTextbox().sendKeys('test@test.com');
        page.getPasswordTextbox().sendKeys('1234');
        const button = page.getForm();
        button.click();
        const form = page.getForm().getAttribute('class');
        expect(form).toContain('ng-valid');
    });

    it('Login form should be valid', async () => {
        page.navigateTo();
        page.getEmailTextbox().sendKeys('krishna.gara@compugain.com');
        page.getPasswordTextbox().sendKeys('Test@123');
        page.getForm().submit();
        const form = page.getForm().getAttribute('class')[0];
        const user = await page.getExecutedScript();
        expect(user).toEqual('krishna.gara@compugain.com');
    });

    it('Publisher profile form should be valid', async () => {
        // page.navigateToPublisher();
        const fullName = page.getPublisherTextbox().sendKeys('test');
        browser.sleep(5000);
        page.nextBtn().click();
        browser.sleep(5000);
        expect('test').toEqual('test');
    });
});
