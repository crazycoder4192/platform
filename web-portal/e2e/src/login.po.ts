import { browser, by, element } from 'protractor';  
export class LoginPage {  
    navigateTo() {  
        return browser.get('/login');  
    }
    navigateToPublisher() {  
        return browser.get('/publisher');  
    }
    getEmailTextbox() {  
        return element(by.name('email'));  
    }  
    getPasswordTextbox() {  
        return element(by.name('password'));  
    }
    getButton(){
        return element(by.buttonText('Login'));    
    }
    getForm(){
        return element(by.tagName('form'));    
    } 
    getExecutedScript(){
        return browser.executeScript("return JSON.parse(window.localStorage.getItem('currentUser')).email");
    }
    
    
    getTitleText() {
        return element(by.css('app-root h1')).getText();
    } 
    
    getPublisherTextbox() {  
        return element(by.name('fullName'));  
    }
    nextBtn() {  
        return element(by.id('nextBtn'));  
    }
} 