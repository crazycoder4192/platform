import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginUserDetails } from './_models/users';
import { AuthenticationService } from './_services/authentication.service';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { TimeoutService } from './_services/timeout.service';

@Injectable()
export class AppInterceptorService implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private timeoutService: TimeoutService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization/accessToken header
    const currentUser: LoginUserDetails = <any>this.authenticationService.currentUserValue;
    const overWriteZone = request.headers.get('overwriteLocalStorageZone') ? request.headers.get('overwriteLocalStorageZone') : null
    if (currentUser && currentUser.token) {
      request = request.clone({
            setHeaders: {
              'access-token': `${currentUser.token}`,
              'zone': (overWriteZone ? overWriteZone :
              localStorage.getItem('zone') ? localStorage.getItem('zone') : '1' )
            }
        });
        if (overWriteZone) {
          request = request.clone({
            headers: request.headers.delete('overwriteLocalStorageZone', overWriteZone)
          })
        }
        this.timeoutService.setIdleTime(environment.idle_timeout);
        this.timeoutService.handleAPIInactivity(true)
            .subscribe(inactive => {
                this.timeoutService.openModal('title', 'message');
            });
    }
       return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
        }
      },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.authenticationService.clearStorage('advertiser');
            }
          }
        }
      ));
  }
}
