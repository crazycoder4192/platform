import { Component, OnInit, EventEmitter, Output, Input, HostListener } from '@angular/core';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { from } from 'rxjs';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import {FlexLayoutModule, BREAKPOINT} from '@angular/flex-layout';

@Component({
  selector: 'app-brands-panel',
  templateUrl: './brands-panel.component.html',
  styleUrls: ['./brands-panel.component.scss']
})
export class BrandsPanelComponent implements OnInit {

  @Output() private toggleBrand = new EventEmitter<boolean>();
  @Input() useremail: string;
  public advertiserUserBrands: Object;
  public brands: any[];
  public clientBrands: any[];
  public clientBrandsUS: any[];
  public clientBrandsInd: any[];
  public watchingBrand: object;
  clickState = false;
  changedBrand: any;
  isAdminBrand: any;
  rolePermissions: any;
  public accountsTab = false;
  selectedMarketPlace = '1';
  totalZones = 1;
  maxShowingBrands: number = 9; // in combinations of (10 + 1) or (1 + 10)
  singlePlatformHeight = 34; // in pixels
  indiaFlexPart: string;
  usFlexPart: string;
  indiaOverlayHt: number;
  usOverlayHt: number;
  scrWidth: number;

  constructor(
    private advertiserUserService: AdvertiserUserService,
    private snackBar: MatSnackBar,
    private router: Router,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private brandService: AdvertiserBrandService,
    private eventEmitterService: EventEmitterService
  ) { 
    this.selectedMarketPlace = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.getScreenSize();
    this.initBrands();
  }
  initBrands () {
    this.brandService.getAllZonesByAdvertiserId().subscribe(
      result => {
        if (result && result.length > 0) {
          // Get total no of zones
          this.totalZones = ([...new Set(result.map((x: any) => x.zone))].length);
        }
    },
      error => {

      });
    this.advertiserUserService.getAdvertiserUserBrands().subscribe(
      res => {
        this.advertiserUserBrands = res;
        this.brands = [];
        this.advertiserUserBrands['brandDetails'].forEach(thisBrandDetails => {
          const matchedBrand = this.advertiserUserBrands['brands'].find((thisBrand) => thisBrand.brandId === thisBrandDetails._id);
          thisBrandDetails.isWatching = matchedBrand.isWatching;
          if (thisBrandDetails.isWatching && (thisBrandDetails.zone === localStorage.getItem('zone'))) {
            this.watchingBrand = thisBrandDetails;
          }
          this.brands.push(thisBrandDetails);
        });
        const source = from(this.brands);
        const groupByClient = source.pipe(
          groupBy(brand => brand.clientName),
          mergeMap(group => group.pipe(toArray()))
        );
        this.clientBrandsUS = [];
        this.clientBrandsInd = [];
        this.clientBrands = [];
        const subscribe = groupByClient.subscribe(val => {
			const groupByName = {};
			this.clientBrands.push({
			  clientName: val[0].clientName,
			  brands: val
			});
			for (const brand of val) {
				this.clientBrandsUS.findIndex(item => item.clientName === val[0].clientName)
				if(brand.zone === '1'){
					if (this.clientBrandsUS.findIndex(item => item.clientName === val[0].clientName) > -1) {
						this.clientBrandsUS[(this.clientBrandsUS.findIndex(item => item.clientName === val[0].clientName))].brands.push(brand);
					} else {						
						this.clientBrandsUS.push({
						  clientName: val[0].clientName,
						  brands: [brand]
						});
					}
				}
				if(brand.zone === '2'){
					if (this.clientBrandsInd.findIndex(item => item.clientName === val[0].clientName) > -1) {
						this.clientBrandsInd[(this.clientBrandsInd.findIndex(item => item.clientName === val[0].clientName))].brands.push(brand);
					} else {						
						this.clientBrandsInd.push({
						  clientName: val[0].clientName,
						  brands: [brand]
						});
					}
				}
      }

      });
    
      let indiaBrandsLength = 0;
      let usBrandsLength = 0;
      let totalCLientsIND = 0;
      let totalClientsUS = 0;
      //scrollbar code 
        for (const clientIN of this.clientBrandsInd) {
          let thisBrands: any[] = clientIN['brands'];
          indiaBrandsLength = indiaBrandsLength + thisBrands.length;
          totalCLientsIND ++;
        }
        for (const clientUS of this.clientBrandsUS) {
          let thisBrands: any[] = clientUS['brands'];
          usBrandsLength = usBrandsLength + thisBrands.length;
          totalClientsUS ++;
        }

        if((indiaBrandsLength + usBrandsLength) <= 9) {
          if(indiaBrandsLength + usBrandsLength + totalCLientsIND + totalClientsUS >= 9) {
          indiaBrandsLength = indiaBrandsLength + totalCLientsIND;
          usBrandsLength = usBrandsLength + totalClientsUS;
        } else {
          indiaBrandsLength = indiaBrandsLength + 1;
        }
      }
      let usFlex: number;
      let indiaFlex: number;
      usFlex = usBrandsLength / (usBrandsLength + indiaBrandsLength);
      indiaFlex = indiaBrandsLength / (usBrandsLength + indiaBrandsLength);
      if( indiaBrandsLength + usBrandsLength > this.maxShowingBrands) {
        if( indiaBrandsLength > 0 && usBrandsLength > 0) {
              if(usFlex > indiaFlex) {
                  indiaFlex = (indiaBrandsLength / (indiaBrandsLength + usBrandsLength)) * 100;
                  usFlex = 100 - indiaFlex;
                  this.indiaFlexPart = indiaFlex + '%';
                  this.usFlexPart = usFlex + '%';
                  if(indiaFlex < 20) {
                    indiaFlex = 24;
                    usFlex = 76;
                  }

              } else if (indiaFlex >= usFlex) {
                  usFlex = (usBrandsLength / (indiaBrandsLength + usBrandsLength)) * 100;
                  indiaFlex = 100 - usFlex;
                  this.indiaFlexPart = indiaFlex + '%';
                  this.usFlexPart = usFlex + '%';
                  if(usFlex < 20) {
                    indiaFlex = 76;
                    usFlex = 24;
                  }
              }
        } else if(indiaBrandsLength === 0) {
          indiaFlex = 0;
          usFlex = 100;
          this.indiaFlexPart = 0 + '%';
          this.usFlexPart = 100 + '%';
        } else if(usBrandsLength === 0) {
          indiaFlex = 100;
          usFlex = 0;
          this.indiaFlexPart = 100 + '%';
          this.usFlexPart = 0 + '%';
        }
        this.indiaOverlayHt = Math.floor((indiaFlex * (window.innerHeight - 248)) / 100);
        this.usOverlayHt = Math.floor((usFlex * (window.innerHeight - 248)) / 100);
      } else {

        if(usBrandsLength === 0){
          this.indiaOverlayHt = window.innerHeight - 248;
          this.usOverlayHt = 0;
        }else if(indiaBrandsLength === 0){
          this.indiaOverlayHt = 0;
          this.usOverlayHt = window.innerHeight - 248;
        }else {
          this.indiaOverlayHt = indiaBrandsLength * this.singlePlatformHeight + totalCLientsIND * 56;
          this.usOverlayHt = usBrandsLength * this.singlePlatformHeight + totalClientsUS * 56;
        }
        usFlex = usBrandsLength / (usBrandsLength + indiaBrandsLength);
        indiaFlex = indiaBrandsLength / (usBrandsLength + indiaBrandsLength);
        this.indiaFlexPart = indiaFlex + '%';
        this.usFlexPart = usFlex + '%';
      }
      //scrollbar code
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }

  
  setSinglePlatformHeight() {
    if(this.scrWidth < 1167) {
      this.singlePlatformHeight = 16;
    } else if (this.checkBetweenWidth(1167,1250,this.scrWidth)) {
      this.singlePlatformHeight = 17;
    } else if (this.checkBetweenWidth(1250,1377,this.scrWidth)) {
      this.singlePlatformHeight = 18;
    } else if (this.checkBetweenWidth(1377,1426,this.scrWidth)) {
      this.singlePlatformHeight = 19;
    } else if (this.checkBetweenWidth(1426,1477,this.scrWidth)) {
      this.singlePlatformHeight = 20;
    } else if (this.checkBetweenWidth(1477,1544,this.scrWidth)) {
      this.singlePlatformHeight = 21;
    } else if (this.checkBetweenWidth(1544,1576,this.scrWidth)) {
      this.singlePlatformHeight = 22;
    } else if (this.checkBetweenWidth(1576,1626,this.scrWidth)) {
      this.singlePlatformHeight = 23;
    } else if (this.checkBetweenWidth(1626,1675,this.scrWidth)) {
      this.singlePlatformHeight = 24;
    } else if (this.checkBetweenWidth(1675,1725,this.scrWidth)) {
      this.singlePlatformHeight = 25;
    } else if (this.checkBetweenWidth(1725,1788,this.scrWidth)) {
      this.singlePlatformHeight = 26;
    } else if (this.checkBetweenWidth(1788,1827,this.scrWidth)) {
      this.singlePlatformHeight = 27;
    } else if (this.checkBetweenWidth(1827,1875,this.scrWidth)) {
      this.singlePlatformHeight = 29;
    } else if (this.checkBetweenWidth(1875,1925,this.scrWidth)) {
      this.singlePlatformHeight = 30;
    } else if (this.checkBetweenWidth(1925,1975,this.scrWidth)) {
      this.singlePlatformHeight = 31;
    } else if (this.checkBetweenWidth(1975,2025,this.scrWidth)) {
      this.singlePlatformHeight = 32;
    } else {
      this.singlePlatformHeight = 32;
    }
    this.singlePlatformHeight = this.singlePlatformHeight + 16;

  }

  checkBetweenWidth(lowerW: number, higherW: number, currentW: number) {
    if (lowerW <= currentW && currentW <= higherW) {
      return true;
    } else {
      return false;
    }
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.scrWidth = window.innerWidth;
        this.setSinglePlatformHeight();
  }

  @HostListener('click')
  clickInside() {
    this.clickState = false;
  }

  @HostListener('document:click')
  clickout() {
    if (this.clickState) {
      this.toggleBrand.emit(false);
    }
    this.clickState = true;
  }
  gotoManageAccounts() {
    localStorage.setItem('activeIcon', JSON.stringify('brand'));
    this.eventEmitterService.onClickNavigation();
    this.toggleBrand.emit(false);
    this.router.navigate(['/advertiser/accountCreation']);
  }
  switchToBrand(brand, isWatching) {
    const brandId = brand._id;
    if (isWatching) {
      this.router.navigate(['/advertiser/brandDashboard'], { state: { 'brandId': JSON.stringify(brandId) } });
    } else if (this.changedBrand !== brandId) {
      this.changedBrand = brandId;
      const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
        width: '50%',
        data: {
          'title' :  'Are you sure you want to switch to "' + brand.brandName + '" ?'
        }
      });
      confirmDialogRef.afterClosed().subscribe(confirmResult => {
        if (confirmResult) {
          this.spinner.showSpinner.next(true);
          this.router.navigate(['/advertiser/dashboard']);
          localStorage.setItem('zone', brand.zone);
          this.advertiserUserService.switchToBrandOfLoggedinUser(brandId).subscribe(
            data => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
              this.eventEmitterService.onSavedAdvertiserProfile();
              this.eventEmitterService.onReadNotification();
              this.initBrands();
              this.router.navigate(['/advertiser/brandDashboard'], { state: { 'brandId': JSON.stringify(brandId) } });
            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
            }
          );
        }
      });
    }
  }
}
