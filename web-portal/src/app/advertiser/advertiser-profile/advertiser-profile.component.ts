import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog, MatDialogConfig, MatAutocompleteSelectedEvent  } from '@angular/material';
import { UserService } from 'src/app/_services/user.service';
import { ZipcodesService } from 'src/app/_services/utils/zipcodes.service';
import { Observable, throwError } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AdvertiserUtilService } from 'src/app/_services/advertiser/advertiser-util.service';
import { Advertiser } from 'src/app/_models/advertiser_model';
import { User, LoginUserDetails } from 'src/app/_models/users';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';
import { UtilService } from 'src/app/_services/utils/util.service';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { CountryCurrenciesModel } from 'src/app/_models/CountryCurrenciesModel';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { USEROLESElement } from 'src/app/admin/configuration/dataModel/userRolesPermissionsModel';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { ClientBrandMetadataService } from 'src/app/_services/advertiser/client-brand-metadata.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AdvUserTermsAndConditionsComponent } from '../adv-user-terms-and-conditions/adv-user-terms-and-conditions.component';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { OptValidationComponent } from 'src/app/common/opt-validation/opt-validation.component';
import { SMSService } from 'src/app/_services/utils/sms.service';
import { WalkthroughComponent } from 'src/app/common/walkthrough/walkthrough.component';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-advertiser-profile',
  templateUrl: './advertiser-profile.component.html',
  styleUrls: ['./advertiser-profile.component.scss']
})
export class AdvertiserProfileComponent implements OnInit {

  public advertiserProfileForm: FormGroup;
  public advertiserBrandForm: FormGroup;
  public currentTab = 1;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  private advertiserId: string;
  public uniqueCities: string[];
  public uniqueStates: string[];
  public filteredStates: Observable<string[]>;
  public filteredCities: Observable<string[]>;
  public filteredCountries: any;
  public typeOfOrganizations: string[];
  private advertiserProfile: Advertiser;
  private usr: User;
  private useremail: string = this.authenticationService.currentUserValue.email;
  public id: string;
  public platformStatus: String = 'add';
  public advertiserType: any = 'Client';
  public uniqeTherapeuticTypes: string[];
  private advertiserBrand: Brand;
  public clientName: string;
  public clientType: string;
  public isValidCity: boolean;
  public organizationType: string;
  public cityStateDetails: any;
  countriesCurrencies: CountryCurrenciesModel[];
  public userPermissions: any;
  public listOfExistedUserRolesPermissions: USEROLESElement[];
  public editPersonalInfo = false;
  public editBusinessAddress = false;
  uniqueZipCodes: any;
  filteredZipCodes: Observable<any>;
  isValidZipCode: any;
  countrySelected: any;
  advertiserDetails: any;
  zone = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  clientBrandList: any;
  userDetails: LoginUserDetails;
  loggedInUserRole: string;
  rolePermissions: any;
  hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
  userRole = JSON.parse(localStorage.getItem('userRole'));
  clientList: any;
  filteredClients: any;
  brandList: any;
  filteredBrands: Observable<string[]>;
  public showTagline;
  public isPhoneNumberVerified;
  private verifiedPhNumber;
  public defaultCountryCode = '+1';
  public countryCodes = ['+1', '+91'];
  filteredTherapeuticTypes: Observable<any>;
  organisationGroup: { name: string; organisations: string[]; }[];
  brandOrganizations: { name: string; organisations: string[]; };
  agencyOrganizations: { name: string; organisations: string[]; };
  marketSelection = '1';
  selectedMarketPlace: string;
  constructor(private router: Router,
    private zipcodesService: ZipcodesService,
    private advertiserUtilService: AdvertiserUtilService,
    private authenticationService: AuthenticationService,
    private advertiserProfileService: AdvertiserProfileService,
    private utilService: UtilService,
    private advertiserBrandService: AdvertiserBrandService,
    private configurationService: ConfigurationService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private eventEmitterService: EventEmitterService,
    private advertiserUserService: AdvertiserUserService,
    private spinner: SpinnerService,
    private snackBar: MatSnackBar,
    private clientBrandMetadataService: ClientBrandMetadataService,
    private dialog: MatDialog,
    private smsService: SMSService,
    private amplitudeService: AmplitudeService
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.spinner.showSpinner.next(false);
    this.setZone();
  }

  ngOnInit() {
    // ***************************************************
    this.organisationGroup = [];
    // ****************************************************
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.advertiserProfileForm = this.formBuilder.group({
      id: [this.advertiserId],
      firstName: ['', Validators.required],
      lastName: [''],
      acceptTerms: [false],
      email: this.userEmail,
      businessName: [''],
      profileMarketSel: ['', Validators.required],
      advertiserType: [''],
      typeOfOrganization: [''],
      addressLine1: [''],
      addressLine2: [''],
      isPhNumberVerified: [false],
      city: [''],
      state: [''],
      country: [''],
      zipCode: [''],
      phoneNumber: [''],
      countryCodePhNumber: ['+1']
    });
    this.advertiserBrandForm = this.formBuilder.group({
      agencyClient: ['', Validators.required],
      typeOfClient: ['', Validators.required],
      marketSel: ['', Validators.required],
      brand: ['', Validators.required],
      therapeuticType: ['', Validators.required],
      typeOfOrganization: ['', Validators.required]
    });
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = [];
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
        this.filteredCountries = this.advertiserProfileForm.controls['country'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterCountry(name))
          );
      }
    );
    this.advertiserUtilService.getUniqueAdvertiserAgencyTypes().subscribe(
      resAgencies => {
        this.typeOfOrganizations = resAgencies;
        this.organisationGroup.push({
          name: 'Agency',
          organisations: resAgencies
        });

        this.advertiserUtilService.getAdvertiserAppTypes().subscribe(
          resBrands => {
            this.typeOfOrganizations = resBrands;
            this.organisationGroup.push({
              name: 'Brand',
              organisations: resBrands
            });
          },
          err => {
            console.log(err);
          }
        );
      },
      err => {
        console.log(err);
      }
    );
    this.utilService.getUniqueTherapeuticTypes().subscribe(
      res => {
        this.uniqeTherapeuticTypes = res;

        this.filteredTherapeuticTypes = this.advertiserBrandForm.controls['therapeuticType'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterTherapeuticType(name))
          );
      },
      err => {
        console.log(err);
      }
    );
    // get user details
    this.userService.getUserByEmail(this.useremail).subscribe(
      res => {
        this.usr = res;
        this.advertiserProfileForm.get('firstName').setValue(this.usr.firstName);
        this.advertiserProfileForm.get('lastName').setValue(this.usr.lastName);
      }
    );
    // end
    // get advertiser by user email.
    this.advertiserProfileService.getAdvertiserByEmail(this.useremail).subscribe(
      res => {
        this.advertiserProfile = res;
        if (!this.advertiserProfile) {
          this.countrySelected = 'null';
          this.showWalkthrough();
          this.editPersonalInfo = true;
          this.editBusinessAddress = true;
          this.advertiserUserService.getAdvertiserDetails(this.useremail).subscribe(
            advertiserDetails => {
              this.advertiserDetails = advertiserDetails;
              this.showTagline = true;
              if (this.advertiserDetails) {
                this.currentTab = 0;
                this.advertiserProfileForm.get('businessName').setValue(this.advertiserDetails.businessName);
                this.advertiserType = this.advertiserDetails.advertiserType;
                this.advertiserProfileForm.get('typeOfOrganization').setValue(this.advertiserDetails.organizationType);
                this.advertiserProfileForm.get('acceptTerms').setValue(this.advertiserDetails.acceptTerms);
                this.advertiserProfileForm.get('addressLine1').setValue(this.advertiserDetails.address.addressLine1);
                this.advertiserProfileForm.get('addressLine2').setValue(this.advertiserDetails.address.addressLine2);
                this.advertiserProfileForm.get('city').setValue(this.advertiserDetails.address.city);
                this.advertiserProfileForm.get('state').setValue(this.advertiserDetails.address.state);
                this.advertiserProfileForm.get('zipCode').setValue(this.advertiserDetails.address.zipcode);
                this.advertiserProfileForm.get('country').setValue(this.advertiserDetails.address.country);
                this.advertiserProfileForm.get('phoneNumber').setValue(this.advertiserDetails.phoneNumber);
                this.advertiserProfileForm.get('isPhNumberVerified').setValue(this.advertiserDetails.isPhNumberVerified);
                if (this.advertiserDetails.isPhNumberVerified) {
                  this.isPhoneNumberVerified = true;
                  this.verifiedPhNumber = this.advertiserProfile.phoneNumber;
                } else {
                  this.isPhoneNumberVerified = false;
                }
                if (this.advertiserProfile.phoneNumber.startsWith('+91')) {
                  this.advertiserProfileForm.get('countryCodePhNumber').setValue('+91');
                  this.advertiserProfileForm.get('phoneNumber').setValue(this.advertiserProfile.phoneNumber.replace('+91', ''));
                } else {
                  this.advertiserProfileForm.get('countryCodePhNumber').setValue('+1');
                  this.advertiserProfileForm.get('phoneNumber').setValue(this.advertiserProfile.phoneNumber.replace('+1', ''));
                }
              }
            }
          );
        } else if (this.advertiserProfile) {
          this.countrySelected = this.advertiserProfile.country === 'US' ? '1' : '2';
          this.currentTab = 0;
          this.showTagline = true;
          if (!this.advertiserProfileForm.get('firstName').value || !this.advertiserProfileForm.get('lastName').value) {
            this.currentTab = 1;
          }
          this.defaultCountryCode = this.advertiserProfile.country === 'India' ? '+91' : '+1';
          this.advertiserProfileForm.get('countryCodePhNumber').setValue(this.defaultCountryCode);
          this.id = this.advertiserProfile._id;
          this.advertiserProfileForm.get('businessName').setValue(this.advertiserProfile.businessName);
          // Handle Radio Button Selection
          this.advertiserType = this.advertiserProfile.advertiserType;
          this.advertiserProfileForm.get('typeOfOrganization').setValue(this.advertiserProfile.organizationType);
          this.advertiserProfileForm.get('acceptTerms').setValue(this.advertiserProfile.acceptTerms);
          this.advertiserProfileForm.get('addressLine1').setValue(this.advertiserProfile.addressLine1);
          this.advertiserProfileForm.get('addressLine2').setValue(this.advertiserProfile.addressLine2);
          this.advertiserProfileForm.get('city').setValue(this.advertiserProfile.city);
          this.advertiserProfileForm.get('state').setValue(this.advertiserProfile.state);
          this.advertiserProfileForm.get('zipCode').setValue(this.advertiserProfile.zipcode);
          this.advertiserProfileForm.get('country').setValue(this.advertiserProfile.country);
          this.advertiserProfileForm.get('phoneNumber').setValue(this.advertiserProfile.phoneNumber);
          this.advertiserProfileForm.get('isPhNumberVerified').setValue(this.advertiserProfile.isPhNumberVerified);
          if (this.advertiserProfile.isPhNumberVerified) {
            this.isPhoneNumberVerified = true;
            this.verifiedPhNumber = this.advertiserProfile.phoneNumber;
          } else {
            this.isPhoneNumberVerified = false;
          }
          if (this.advertiserProfile.phoneNumber.startsWith('+91')) {
            this.advertiserProfileForm.get('countryCodePhNumber').setValue('+91');
            this.advertiserProfileForm.get('phoneNumber').setValue(this.advertiserProfile.phoneNumber.replace('+91', ''));
          } else {
            this.advertiserProfileForm.get('countryCodePhNumber').setValue('+1');
            this.advertiserProfileForm.get('phoneNumber').setValue(this.advertiserProfile.phoneNumber.replace('+1', ''));
          }
        }
        this.initUniqueZipCodes();
        this.initUniqueCities();
        this.initUniqueStates();
      });
      this.adminRolesAndPermissions();
      this.userDetails = this.authenticationService.currentUserValue;
      this.getClientsMetadata();
  }
  changeOrganisationType(organisationType) {
    this.brandOrganizations = this.organisationGroup.find( obj => obj.name === 'Brand');
    this.agencyOrganizations = this.organisationGroup.find( obj => obj.name === 'Agency');
    const isBrandType = this.brandOrganizations.organisations.includes(organisationType);
    const isAgencyType = this.agencyOrganizations.organisations.includes(organisationType);
    if (isBrandType) {
      this.advertiserType = 'Client';
    } else if (isAgencyType) {
      this.advertiserType = 'Agency';
    }
  }
  getTherapeuticData(event: MatAutocompleteSelectedEvent) {
    if (this.marketSelection === '2') {
      const therapyName = this.clientBrandList.filter((item) => item.brand === event.option.value)[0].therapyName;
      if (therapyName) { this.advertiserBrandForm.get('therapeuticType').setValue(therapyName); }
    } else { return event; }
  }
  showWalkthrough() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    const context = JSON.parse(localStorage.getItem('currentUser'))['usertype'];
    dialogConfig.data = {
      context
    };
    const dialogRef = this.dialog.open(WalkthroughComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
        dialogRef.close();
        const eventProperties = {
          type: JSON.parse(localStorage.getItem('currentUser'))['usertype']
        };
        this.amplitudeService.logEvent('onboarding - user clicks on get started', eventProperties);
    });
  }

  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }
  onMarketChange() {
    this.marketSelection = this.selectedMarketPlace;
    this.setZone();
    if (this.advertiserType === 'Agency') {
      this.advertiserBrandForm.get('agencyClient').setValue(null);
      this.advertiserBrandForm.get('typeOfClient').setValue(null);
      this.advertiserBrandForm.get('brand').setValue(null);
    }
    this.getClientsMetadata();
  }

  selectedCountry(countryISO2Code) {
    const isValid = this.advertiserProfileForm.get('city').value
    || this.advertiserProfileForm.get('state').value || this.advertiserProfileForm.get('zipCode').value ;
    if (isValid) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        title: `Are you sure you want to re-enter the address details?`,
        message: '',
        confirm: 'Yes',
        cancel: 'No'
      };
      dialogConfig.minWidth = 400;
      const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.getCountryDetails(countryISO2Code);
          if (this.id) {
            this.advertiserProfile.zipcode = null;
            this.advertiserProfile.state = null;
            this.advertiserProfile.city = null;
          }
          this.advertiserProfileForm.get('city').setValue(null);
          this.advertiserProfileForm.get('state').setValue(null);
          this.advertiserProfileForm.get('zipCode').setValue(null);
          dialogRef.close();
        } else {
          const countryValue = this.countrySelected === '1' ? 'United States' : 'India';
          this.advertiserProfile.country = countryValue;
          this.advertiserProfileForm.get('country').setValue(countryValue);
          dialogRef.close();
        }
      });
    } else {
      this.getCountryDetails(countryISO2Code);
    }

  }
  getCountryDetails(countryISO2Code) {
    this.countrySelected = countryISO2Code === 'US' ? '1' : '2';
    this.defaultCountryCode = countryISO2Code === 'US' ? '+1' : countryISO2Code === 'IN' ? '+91' : '+1';
    this.advertiserProfileForm.get('countryCodePhNumber').setValue(this.defaultCountryCode);
    this.initUniqueZipCodes();
    this.initUniqueCities();
    this.initUniqueStates();
  }
  getClientsMetadata() {
    this.clientBrandMetadataService.getClientsMetadata(this.advertiserBrandForm.get('marketSel').value).subscribe(
      res => {
        this.clientList = res;
        if (this.advertiserType === 'Agency') {
          this.filteredClients = this.advertiserBrandForm.controls['agencyClient'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterClient(name))
          );
        } else if (this.advertiserType === 'Client') {
          this.filteredClients = this.advertiserProfileForm.controls['businessName'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterClient(name))
          );
        }

      },
      err => {
        console.log('Error!');
    });
  }
  getBrandsMetadata(clientId) {
    this.advertiserBrandForm.get('brand').setValue('');
    this.clientBrandMetadataService.getBrandsMetadata(clientId, this.advertiserBrandForm.get('marketSel').value).subscribe(
      res => {
        this.clientBrandList = res;
        this.brandList = this.clientBrandList.map((item => item.brand));
        this.filteredBrands = this.advertiserBrandForm.controls['brand'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterBrand(name))
          );
      },
      err => {
        console.log('Error!');
    });
  }
  editSection(sectionType: any) {
    if (sectionType === 'personalInfoSection' && this.currentTab !== 1) {
      this.editPersonalInfo = !this.editPersonalInfo;
      this.currentTab = 1;
    } else if (sectionType === 'businessAddress' && this.currentTab !== 2) {
      this.editBusinessAddress = !this.editBusinessAddress;
      this.currentTab = 2;
    } else {
      this.currentTab = 0;
    }
  }
  adminRolesAndPermissions() {
    this.configurationService.getalluserolesByModuleType('Advertiser').subscribe(
      res => {
        if (res) {
          this.listOfExistedUserRolesPermissions = <USEROLESElement[]>res;
        }
      },
      err => {
        console.log(err);
      });
  }
  initUniqueStates() {
    this.zipcodesService.getUniqueStates(this.countrySelected).subscribe(
      res => {
        this.uniqueStates = res;
        this.filteredStates = this.advertiserProfileForm.controls['state'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterState(name))
          );
      });
  }
  initUniqueCities() {
    this.zipcodesService.getUniqueCities(this.countrySelected).subscribe(
      res => {
        this.uniqueCities = res;
        this.uniqueCities = this.uniqueCities.filter((el, i, a) => i === a.indexOf(el));
        this.filteredCities = this.advertiserProfileForm.controls['city'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
          );
      },
      err => {
        console.log('Error!');
      });
  }
  initUniqueZipCodes() {
  this.zipcodesService.getUniqueZipCodes(this.countrySelected).subscribe(
      res => {
        this.uniqueZipCodes = res;
        this.filteredZipCodes = this.advertiserProfileForm.controls['zipCode'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
          );
      },
      err => {
        console.log('Error!');
      });
  }
  // searching cities by prefix with atleast 4 charecters
  onChangeCityString() {
    if (this.advertiserProfileForm.controls['city'].value.length > 1) {
      this.zipcodesService.getUniqueCitiesOnChangeCity(this.advertiserProfileForm.controls['city'].value, this.countrySelected).subscribe(
        res => {
          this.uniqueCities = res;
          this.filteredCities = this.advertiserProfileForm.controls['city'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueCities();
    }
  }
  // end
  // searching zipcodes by prefix with atleast 4 charecters
  onChangeZipCodeString() {
    if (this.advertiserProfileForm.controls['city'].value && this.advertiserProfileForm.controls['city'].value.length > 1) {
      this.zipcodesService.getUniqueZipCodesOnChangeZipCode(this.advertiserProfileForm.controls['zipCode'].value, this.countrySelected).subscribe(
        res => {
          this.uniqueZipCodes = res;
          this.filteredZipCodes = this.advertiserProfileForm.controls['zipCode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueZipCodes();
    }
  }
  // end
  // call on city change and change the country state and pin.
  onChangeCity() {
    if (this.uniqueCities.length) {
      const cityName = this.advertiserProfileForm.controls['city'].value;
      this.isValidCity = this.uniqueCities.includes(cityName);
      if (this.isValidCity) {
        this.advertiserProfileForm.get('state').setValue('');
        this.utilService.getCityDetailsByCityName(cityName, this.countrySelected).subscribe(
          res => {
            this.cityStateDetails = res;
            this.uniqueStates = [];
            this.uniqueStates = this.cityStateDetails.map(function(item) {
              return item['stateFullName'];
            });
            this.uniqueStates = Array.from(new Set(this.uniqueStates));
            this.filteredStates = this.advertiserProfileForm.controls['state'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => this._filterState(name))
            );
            this.uniqueZipCodes = [];
            this.uniqueZipCodes = this.cityStateDetails.map(function(item) {
              return item['zipcode'];
            });
            if (!this.uniqueZipCodes.includes(this.advertiserProfileForm.controls['zipCode'].value)) {
              this.advertiserProfileForm.get('zipCode').setValue('');
            }
            this.filteredZipCodes = this.advertiserProfileForm.controls['zipCode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
          },
          err => {
            console.log(err);
          }
        );
        // get
      }
    }
  }
  // end
// call on city change and change the country state and pin.
onChangeZipCode() {
  if (this.uniqueZipCodes.length) {
    const zipCode = this.advertiserProfileForm.controls['zipCode'].value;
    this.isValidZipCode = this.uniqueZipCodes.includes(zipCode);
    if (this.isValidZipCode) {
      this.advertiserProfileForm.get('city').setValue('');
      this.advertiserProfileForm.get('state').setValue('');
      // this.advertiserProfileForm.get('country').setValue('');
      this.utilService.getCityDetailsByZipCode(zipCode, this.countrySelected).subscribe(
        res => {
          this.cityStateDetails = res;
          this.advertiserProfileForm.get('city').setValue(this.cityStateDetails.city);
          this.advertiserProfileForm.get('state').setValue(this.cityStateDetails.stateFullName);
          // this.advertiserProfileForm.get('country').setValue(this.cityStateDetails.country);
        },
        err => {
          console.log(err);
        }
      );
      // get
    }
  }
}
// end

  prevView(n: number) {
    this.currentTab = this.currentTab + n;
    if (this.currentTab === 1) {
      this.advertiserProfileForm.controls['acceptTerms'].enable();
    } else {
      this.advertiserProfileForm.controls['acceptTerms'].disable();
    }
  }

  nextView(n: number) {
    if (n === 1) {
      if (this.validatePersonalInformation()) {
        this.currentTab = this.currentTab + n;
        if (this.currentTab === 1) {
          this.advertiserProfileForm.controls['acceptTerms'].enable();
        } else {
          this.advertiserProfileForm.controls['acceptTerms'].disable();
        }
        this.amplitudeService.logEvent('profile_creation - user enters basic information', null);
      }
    } else if (n === 2) {
      if (this.validateBusinessAddress()) {
        if (this.currentTab === 1) {
          this.advertiserProfileForm.controls['acceptTerms'].enable();
        } else {
          this.advertiserProfileForm.controls['acceptTerms'].disable();
        }
        this.currentTab = this.currentTab + 1;
        if (this.selectedMarketPlace && this.advertiserType === 'Client') {
          this.marketSelection = this.selectedMarketPlace;
          this.setZone();
        }
        this.getClientsMetadata();
        this.amplitudeService.logEvent('profile_creation - user enters business address', null);
      }
    }
  }
  get f() { return this.advertiserProfileForm.controls; }

  selectAdvertiserType(value: String) {
      this.advertiserType = value;
  }

  validatePersonalInformation(): boolean {
    const formGroup = this.advertiserProfileForm;
    const firstNameCtrl = formGroup.controls['firstName'];
    const lastNameCtrl = formGroup.controls['lastName'];
    const typeOfOrganizationCtrl = formGroup.controls['typeOfOrganization'];
    const businessNameCtrl = formGroup.controls['businessName'];
    const acceptTermsCtrl = formGroup.controls['acceptTerms'];
    const marketPlace = formGroup.controls['profileMarketSel'];
    let isValid = true;

    if (!firstNameCtrl.value) {
      firstNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'firstName')) {
      isValid = false;
    }
    if (!lastNameCtrl.value) {
      lastNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'lastName')) {
      isValid = false;
    }
    if (!typeOfOrganizationCtrl.value) {
      typeOfOrganizationCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!marketPlace.value && this.advertiserType && this.advertiserType === 'Client' && !this.id) {
      marketPlace.setErrors({ require: true });
      isValid = false;
    }
    if (!businessNameCtrl.value) {
      businessNameCtrl.setErrors({ require: true });
      isValid = false;
    }
    // else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'businessName')) {
    //   isValid = false;
    // }
    if (!acceptTermsCtrl.value) {
      acceptTermsCtrl.setErrors({ require: true });
      isValid = false;
    }

    return isValid;
  }
  validateBusinessAddress(): boolean {
    const formGroup = this.advertiserProfileForm;
    const addr1Ctrl = formGroup.controls['addressLine1'];

    const cityCtrl = formGroup.controls['city'];
    const stateCtrl = formGroup.controls['state'];
    const countryCtrl = formGroup.controls['country'];
    const zipCodeCtrl = formGroup.controls['zipCode'];
    const phoneNumberCtrl = formGroup.controls['phoneNumber'];
    const countryCodephNumCtrl = formGroup.controls['countryCodePhNumber'];

    let isValid = true;
    if (!addr1Ctrl.value) {
      addr1Ctrl.setErrors({ require: true });
      isValid = false;
    }
    if (!cityCtrl.value) {
      cityCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'city')) {
      isValid = false;
    }
    if (!stateCtrl.value) {
      stateCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'state')) {
      isValid = false;
    }
    if (!countryCtrl.value) {
      countryCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'country')) {
      isValid = false;
    }
    if (!zipCodeCtrl.value) {
      zipCodeCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!Number(zipCodeCtrl.value.trim())) {
      zipCodeCtrl.setErrors({ invalidCharacters: true });
      isValid = false;
    } else if (this.marketSelection === '1' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 5) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
      isValid = false;
    } else if (this.marketSelection === '2' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 6) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    }
    if (!phoneNumberCtrl.value) {
      phoneNumberCtrl.setErrors({ require: true });
      isValid = false;
    } else if (phoneNumberCtrl.value.trim()) {
      const phNumber = countryCodephNumCtrl.value.trim() + phoneNumberCtrl.value.trim();
      let validatePhNUmber = phNumber.replace('+', '');
      // Here we are removing special characters from US number
      validatePhNUmber = (validatePhNUmber.match(/[(), ,+,-]/g) != null) ? validatePhNUmber.replace(/[(), ,+,-]/g, '') : validatePhNUmber;
      if (!validatePhNUmber.match(/^\d+$/)) {
        phoneNumberCtrl.setErrors({ invalidPhNumber: true });
        isValid = false;
      } else {
        const cleaned = ('' + phoneNumberCtrl.value.trim()).replace(/\D/g, '');
        if (countryCodephNumCtrl.value.startsWith('+1')) {
            const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
            if (match) {
              const intlCode = (match[1] ? '+1 ' : '');
              phoneNumberCtrl.setValue([intlCode, '(', match[2], ') ', match[3], '-', match[4]].join(''));
            }
            const trimmedNumber = phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '');
            if (trimmedNumber.length !== 10 || !trimmedNumber.match(/^\d+$/)) {
              phoneNumberCtrl.setErrors({ invalidPhNumber: true });
              isValid = false;
              this.isPhoneNumberVerified = null;
            }
        } else if (countryCodephNumCtrl.value.startsWith('+91')) {
          const indianNumber = phoneNumberCtrl.value.match(/[(), ,+,-]/g) != null ? phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '') :
            phoneNumberCtrl.value;
          phoneNumberCtrl.setValue(indianNumber);
          if (indianNumber.length !== 10 || !indianNumber.match(/^\d+$/)) {
            phoneNumberCtrl.setErrors({ invalidPhNumber: true });
            isValid = false;
            this.isPhoneNumberVerified = null;
          }
        }
      }
    }
    return isValid;
  }

  validatePhoneNumber() {
    const phoneNumberCtrl = this.advertiserProfileForm.controls['phoneNumber'];
    const countryCodephNumCtrl = this.advertiserProfileForm.controls['countryCodePhNumber'];
    if (!phoneNumberCtrl.value) {
      if (!this.id) {
        this.isPhoneNumberVerified = null;
      }
    } else if (phoneNumberCtrl.value.trim()) {
      const phNumber = countryCodephNumCtrl.value.trim() + phoneNumberCtrl.value.trim();
      let validatePhNUmber = phNumber.replace('+', '');
      validatePhNUmber = (validatePhNUmber.match(/[(), ,+,-]/g) != null) ? validatePhNUmber.replace(/[(), ,+,-]/g, '') : validatePhNUmber;
      if (!validatePhNUmber.match(/^\d+$/)) {
        this.isPhoneNumberVerified = null;
      } else {
        const cleaned = ('' + phoneNumberCtrl.value.trim()).replace(/\D/g, '');
        if (countryCodephNumCtrl.value.startsWith('+1')) {
          if (phNumber.startsWith('+1')) {
            const trimmedNumber = phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '');
            const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
            if (match) {
            //  const intlCode = (match[1] ? '+1 ' : '');
              phoneNumberCtrl.setValue(['(', match[2], ') ', match[3], '-', match[4]].join(''));
              if (!this.id && !this.isPhoneNumberVerified && trimmedNumber.length === 10) {
                this.isPhoneNumberVerified = false;
              }  else if (trimmedNumber.length === 10) {
                this.checkChangeInPhoneNumber('+1' + phoneNumberCtrl.value);
              } else {
                this.isPhoneNumberVerified = null;
              }
              if (trimmedNumber.length !== 10 || !trimmedNumber.match(/^\d+$/)) {
                // phoneNumberCtrl.setErrors({ invalidPhNumber: true });
                this.isPhoneNumberVerified = null;
              }
            }
          }
        } else if (countryCodephNumCtrl.value.startsWith('+91')) {
          if (phNumber.startsWith('+91')) {
            const indianNumber = phoneNumberCtrl.value.match(/[(), ,+,-]/g) != null ? phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '') :
            phoneNumberCtrl.value;
            phoneNumberCtrl.setValue(indianNumber);
            const phoneNumber = phNumber.replace(/[(), ,-]/g, '');
            if (!this.id && !this.isPhoneNumberVerified && phoneNumber.length === 13) {
              this.isPhoneNumberVerified = false;
            } else if (phoneNumber.length === 13) {
              this.checkChangeInPhoneNumber(countryCodephNumCtrl.value + phoneNumberCtrl.value);
            } else {
              this.isPhoneNumberVerified = null;
            }
            if (indianNumber.length !== 10 || !indianNumber.match(/^\d+$/)) {
             // phoneNumberCtrl.setErrors({ invalidPhNumber: true });
              this.isPhoneNumberVerified = null;
            }
          }
        } else if (cleaned.length === 10 && !phNumber.startsWith('+91') && !phNumber.startsWith('+1')) {
          this.isPhoneNumberVerified = null;
        } else {
          this.isPhoneNumberVerified = null;
        }
      }
    }
  }
  checkChangeInPhoneNumber(phNumber: string) {
    if (this.verifiedPhNumber) {
      if (this.verifiedPhNumber !== phNumber) {
        this.isPhoneNumberVerified = false;
      } else {
        this.isPhoneNumberVerified = this.advertiserProfileForm.get('isPhNumberVerified').value;
      }
    } else {
      this.isPhoneNumberVerified = false;
    }
  }

  validatePincode() {
    const zipCodeCtrl = this.advertiserProfileForm.controls['zipCode'];
    if (this.countrySelected === '1' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 5) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    } else if (this.countrySelected === '2' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 6) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    }
  }

  private _filterCity(name: string): string[] {
    this.onChangeCityString();
    this.onChangeCity();
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueCities.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }

  private _filterState(name: string): string[] {
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueStates.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }

  private _filterCountry(value: string): CountryCurrenciesModel[] {
    let filterValue: string;
    if (value) {
      filterValue = value.toLowerCase();
    }
    const filteredCountries = this.countriesCurrencies.filter(
      countrycurrency =>
      countrycurrency.countryName.toLowerCase().indexOf(filterValue) === 0);
      return filteredCountries;
  }

  private _filterOrganization(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.typeOfOrganizations.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterTherapeuticType(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.uniqeTherapeuticTypes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterZipCode(name: string): any {
    this.onChangeZipCodeString();
    this.onChangeZipCode();
    const filterValue = name.toLowerCase();
    return this.uniqueZipCodes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterClient(value: string): string[] {
    let filterValue: string;
    if (value) {
      filterValue = value.toLowerCase();
    }
    const filteredClients = this.clientList.filter(
      item =>
      item.client.toLowerCase().indexOf(filterValue) === 0);
      return filteredClients;
  }
  private _filterBrand(value: string): string[] {
    if (value) {
      const filterValue = value.toLowerCase();
      return this.brandList.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }
  }
  validateAddAccount(): boolean {
    const formGroup = this.advertiserBrandForm;
    const advertiserBrandCtrl = formGroup.controls['brand'];
    const therapeuticTypeCtrl = formGroup.controls['therapeuticType'];
    const agencyClientCtrl = formGroup.controls['agencyClient'];
    const typeOfClientCtrl = formGroup.controls['typeOfClient'];
    const marketPlace = formGroup.controls['marketSel'];
    let isValid = true;
    const isValidTerapeuticType: boolean = this.uniqeTherapeuticTypes.includes(therapeuticTypeCtrl.value);
    const isValidTypeOfClient: boolean = this.typeOfOrganizations.includes(typeOfClientCtrl.value);
    if (!advertiserBrandCtrl.value) {
      advertiserBrandCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!marketPlace.value && this.advertiserType && this.advertiserType === 'Agency') {
      marketPlace.setErrors({ require: true });
      isValid = false;
    }
    if (!therapeuticTypeCtrl.value) {
      therapeuticTypeCtrl.setErrors({ require: true });
      isValid = false;
    }
    /*if (therapeuticTypeCtrl.value && !isValidTerapeuticType) {
      therapeuticTypeCtrl.setErrors({ therapeuticTypeInvalid: true });
      isValid = false;
    }*/
    if (this.advertiserType === 'Agency') {
      if (!agencyClientCtrl.value) {
        agencyClientCtrl.setErrors({ require: true });
        isValid = false;
      }
      if (!typeOfClientCtrl.value) {
        typeOfClientCtrl.setErrors({ require: true });
        isValid = false;
      }
      if (typeOfClientCtrl.value && !isValidTypeOfClient) {
        typeOfClientCtrl.setErrors({ typeOfClientInvalid: true });
        isValid = false;
      }
    }
    return isValid;
  }

  saveAdvertiser() {
    this.advertiserProfileForm.controls['acceptTerms'].enable();
    this.organizationType = this.advertiserProfileForm.get('typeOfOrganization').value;
    if (this.advertiserType === 'Client') {
      this.clientName = this.advertiserProfileForm.get('businessName').value;
      // this.clientType = this.advertiserProfileForm.get('typeOfOrganization').value;
      this.clientType = this.organizationType;
    } else if (this.advertiserType === 'Agency') {
      this.clientName = this.advertiserBrandForm.get('agencyClient').value;
      this.clientType = this.advertiserBrandForm.get('typeOfClient').value;
    }
    this.advertiserProfile = new Advertiser('',
      this.advertiserProfileForm.get('businessName').value,
      this.advertiserType,
      this.organizationType,
      this.advertiserProfileForm.get('acceptTerms').value,
      this.advertiserProfileForm.get('addressLine1').value,
      this.advertiserProfileForm.get('addressLine2').value,
      this.advertiserProfileForm.get('city').value,
      this.advertiserProfileForm.get('state').value,
      this.advertiserProfileForm.get('zipCode').value,
      this.advertiserProfileForm.get('country').value,
      this.userEmail,
      this.advertiserProfileForm.get('countryCodePhNumber').value + this.advertiserProfileForm.get('phoneNumber').value,
      this.isPhoneNumberVerified
    );
    this.advertiserBrand = new Brand(
      '',
      this.advertiserBrandForm.get('brand').value,
      this.advertiserBrandForm.get('therapeuticType').value,
      this.clientName,
      this.clientType,
      '',
      this.userEmail,
      {}
    );
    if (this.validateAddAccount()) {
      this.spinner.showSpinner.next(true);
      this.advertiserProfileService.saveAdvertiser(this.advertiserProfile).subscribe(
        advertiser => {
          if ( advertiser) {
            this.advertiserBrand.advertiser = advertiser._id;
            this.updateAdvertiserPersonalInfo();
            this.advertiserBrandService.saveAdvertiserBrand(this.advertiserBrand,
              this.advertiserBrandForm.get('marketSel').value).subscribe(
              brand => {
                if (brand) {
                  const adminPermission = this.listOfExistedUserRolesPermissions.find((thisItem) =>
                  thisItem.roleName.toLowerCase().trim() === 'Admin'.toLowerCase().trim());
                  const advertiserUser = {
                    email: advertiser.user.email,
                    roleName: adminPermission.roleName,
                    roleId: adminPermission._id,
                    userPermissions: adminPermission.permissions,
                    brand: brand.brandName,
                    brandId: brand._id,
                    advertiserId: advertiser._id
                  };
                  this.advertiserUserService.addAdvertiserUser(advertiserUser, this.advertiserBrandForm.get('marketSel').value).subscribe(
                    res => {
                      this.spinner.showSpinner.next(false);
                      this.eventEmitterService.onSavedAdvertiserProfile();
                      this.router.navigate(['/advertiser/advertiserProfileThanks']);
                    },
                    error => {
                      console.log('Error!');
                    });
                }
              this.amplitudeService.logEvent('profile_creation - user enters brand', null);
              },
              err => {
                console.log('Error!');
              });
            }
        },
        err => {
          console.log('Error!');
        });
    }
  }
  updateAdvertiser() {
    this.advertiserProfileForm.controls['acceptTerms'].enable();
    if (this.advertiserType === 'Client' ) {
      this.organizationType = this.advertiserProfileForm.get('typeOfOrganization').value,
      this.clientName = this.advertiserProfileForm.get('businessName').value;
      this.clientType = this.advertiserProfileForm.get('typeOfOrganization').value;
    } else if (this.advertiserType === 'Agency') {
      this.organizationType = this.advertiserProfileForm.get('typeOfOrganization').value;
      this.clientName = this.advertiserBrandForm.get('agencyClient').value;
      this.clientType = this.advertiserBrandForm.get('typeOfClient').value;
    }

    if (this.validatePersonalInformation() &&  this.validateBusinessAddress()) {
      this.advertiserProfile = new Advertiser(this.id,
      this.advertiserProfileForm.get('businessName').value,
      this.advertiserType,
      this.organizationType,
      this.advertiserProfileForm.get('acceptTerms').value,
      this.advertiserProfileForm.get('addressLine1').value,
      this.advertiserProfileForm.get('addressLine2').value,
      this.advertiserProfileForm.get('city').value,
      this.advertiserProfileForm.get('state').value,
      this.advertiserProfileForm.get('zipCode').value,
      this.advertiserProfileForm.get('country').value,
      this.userEmail,
      this.advertiserProfileForm.get('countryCodePhNumber').value + this.advertiserProfileForm.get('phoneNumber').value,
      this.isPhoneNumberVerified
    );
      this.advertiserProfile._id = this.id;
      this.advertiserProfileService.updateAdvertiser(this.advertiserProfile).subscribe(
        res => {
          if (this.validateFirstAndLastName()) {
            this.updateAdvertiserPersonalInfo();
            this.editSection('');
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: 'Details updated successfully'
              }
            });
          }
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
          console.log('Error!');
      });
    }
  }
  // update only first and last name of publisher.
  updateAdvertiserPersonalInfo() {
      if (this.validateFirstAndLastName() && this.usr && (this.usr.firstName !== this.advertiserProfileForm.get('firstName').value.trim() ||
          this.usr.lastName !== this.advertiserProfileForm.get('lastName').value)) {
        this.usr.firstName = this.advertiserProfileForm.get('firstName').value;
        this.usr.lastName = this.advertiserProfileForm.get('lastName').value;
        this.usr.email = this.useremail;

    this.advertiserUserService.getAdvertiserUserPermissions(this.useremail).subscribe(
      permissions => {
        if (!this.userDetails.hasPersonalProfile && permissions['zone'] &&
      (localStorage.getItem('zone') === 'null' || localStorage.getItem('zone') === 'undefined'
      || permissions['zone'] !== localStorage.getItem('zone'))) {
        localStorage.setItem('zone', permissions['zone']);
      }
        this.userService.updateFirstAndLastName(this.usr).subscribe(
          res => {
            if (!this.userDetails.hasPersonalProfile) {
              this.userDetails.hasPersonalProfile = true;
              this.userDetails['first_name'] = this.usr.firstName;
              this.userDetails['last_name'] = this.usr.lastName;
              localStorage.setItem('currentUser', JSON.stringify(this.userDetails));
              this.eventEmitterService.onSavedAdvertiserProfile();
              this.hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
              this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
            }
            // this.router.navigate(['/advertiser/advertiserProfileThanks']);
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
            console.log('Error!');
        });
      },
      error => {

      });
      }
  }
  // end
  addAccount() {
    if (this.validateAddAccount()) {
      this.advertiserBrandService.saveAdvertiserBrand(this.advertiserBrand, this.advertiserBrandForm.get('marketSel').value).subscribe(
        res => {
          // this.savePlatform(res[0]._id);
          this.router.navigate(['/advertiser/advertiserProfileThanks']);
        },
        err => {
          console.log('Error!');
      });
    }
  }

  openTermsAndConditionsDialog() {
    if ( this.currentTab === 1) {
      const dialogRef = this.dialog.open(AdvUserTermsAndConditionsComponent, {
        width: '50%'
      });
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }

  validateFirstAndLastName(): boolean {

    const firstNameCtrl = this.advertiserProfileForm.controls['firstName'];
    const lastNameCtrl = this.advertiserProfileForm.controls['lastName'];
    let isValid = true;

    if (!firstNameCtrl.value) {
      firstNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(this.advertiserProfileForm, 'firstName')) {
      isValid = false;
    }
    if (!lastNameCtrl.value) {
      lastNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(this.advertiserProfileForm, 'lastName')) {
      isValid = false;
    }
    return isValid;
  }
  checkForSpecialCharactersAndNumber(formGroup: FormGroup, ctrlName: string) {
    let isValid = true;
    if (!formGroup.controls[ctrlName].value.trim().replace(/\s/g, '').match(/^[A-Za-z]*$/)) {
      formGroup.controls[ctrlName].setErrors({ invalidCharacters: true });
      isValid = false;
    }
    return isValid;
  }
  requestOTP() {
    if (this.currentTab === 2) {
      this.spinner.showSpinner.next(true);
      const phoneNumber = this.advertiserProfileForm.get('countryCodePhNumber').value + this.advertiserProfileForm.get('phoneNumber').value;
      this.smsService.verifyPhoneNumber(phoneNumber).
      subscribe( res => {
        this.spinner.showSpinner.next(false);
        this.openPhoneNumberValidationDialog(res['otpLimitMsg']);
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      });
    }
  }
  openPhoneNumberValidationDialog(limitMsg: string) {
    const phoneNumber = this.advertiserProfileForm.get('countryCodePhNumber').value + this.advertiserProfileForm.get('phoneNumber').value;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = '50%';
    dialogConfig.data = {
      phNumber: phoneNumber,
      limitMsg: limitMsg,
      userType: 'Advertiser'
    };
      const dialogRef = this.dialog.open(OptValidationComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(res => {
        if ( res && res.status === 'success') {
            this.verifiedPhNumber = res.phNumber;
            this.isPhoneNumberVerified = true;
            if (this.id) {
              this.spinner.showSpinner.next(true);
            //  this.publisherProfileService.getProfileByEmail(this.useremail).subscribe( res => {
                this.advertiserProfileForm.get('isPhNumberVerified').setValue(true);
                this.advertiserProfile = new Advertiser(this.id,
                  this.advertiserProfileForm.get('businessName').value,
                  this.advertiserType,
                  this.organizationType,
                  this.advertiserProfileForm.get('acceptTerms').value,
                  this.advertiserProfileForm.get('addressLine1').value,
                  this.advertiserProfileForm.get('addressLine2').value,
                  this.advertiserProfileForm.get('city').value,
                  this.advertiserProfileForm.get('state').value,
                  this.advertiserProfileForm.get('zipCode').value,
                  this.advertiserProfileForm.get('country').value,
                  this.userEmail,
                  phoneNumber,
                  this.isPhoneNumberVerified
                );
                this.advertiserProfileService.updateAdvertiser(this.advertiserProfile).subscribe(rt => {
                  this.spinner.showSpinner.next(false);
                  this.advertiserDetails = rt;
                },
                err => {
                  this.spinner.showSpinner.next(false);
                });
            }
          }
      });
  }
  convertToPhoneNumber(phNumber: string) {
    if (phNumber.startsWith('+1')) {
      return phNumber;
    } else  {
      return '+1' + phNumber;
    }
  }
  onCountryChange(countryISO2Code) {
    this.advertiserProfileForm.get('state').setValue('');
    this.advertiserProfileForm.get('city').setValue('');
    this.advertiserProfileForm.get('zipCode').setValue('');
    this.marketSelection = countryISO2Code === 'US' ? '1' : '2';
    this.initUniqueZipCodes();
    this.initUniqueCities();
    this.initUniqueStates();
  }
}
