import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SupportService } from 'src/app/_services/admin/support.service';
import { Ticket } from 'src/app/_models/ticket_model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { interval } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-advertiser-ticket-details',
  templateUrl: './advertiser-ticket-details.component.html',
  styleUrls: ['./advertiser-ticket-details.component.scss']
})
export class AdvertiserTicketDetailsComponent implements OnInit {
  ticket: Ticket;
  public editTicketForm: FormGroup;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  userDetails: any;
  commentMsg: string;
  priorityList: string[];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private supportService: SupportService,
    private snackBar: MatSnackBar,
    private spinner: SpinnerService
  ) {

  }

  ngOnInit() {
    this.priorityList = ['Urgent', 'High', 'Normal', 'Low'];
    this.getSuportTicket();
    this.initEditTicketForm();
  }
  initEditTicketForm() {
    this.editTicketForm = this.formBuilder.group({
      comment: ['']
    });
  }
  getSuportTicket () {
    this.route.params.subscribe(params => {
      console.log('params', params);
      const source = interval(10000);
      this.supportService.getTicket(params.id).subscribe(
        resposne => {
          this.ticket = <Ticket>resposne['ticketDetails'];
          this.userDetails = resposne['userDetails'];
        },
        err => {
          console.log('Error! createTicket.supportService.addTicket : ' + err);
      });
      /*source.subscribe( () => {
        this.supportService.getTicket(params.id).subscribe(
          resposne => {
            this.ticket = <Ticket>resposne['ticketDetails'];
            this.userDetails = resposne['userDetails'];
          },
          err => {
            console.log('Error! createTicket.supportService.addTicket : ' + err);
        });
      });*/
    });
  }
  updateSuportTicket (type: string) {
    if (type === 'status') {
      this.ticket.status = 'Solved';
    }
    this.supportService.updateTicket(this.ticket).subscribe(
      resposne => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: resposne['message']
          }
        });
        this.getSuportTicket();
        this.spinner.showSpinner.next(false);
      },
      err => {
        console.log('Error! createTicket.supportService.updateSuportTicket : ' + err);
    });
  }
  addSupportComment() {
    this.commentMsg = '';
    if (!this.editTicketForm.controls['comment'].value) {
      this.commentMsg = 'Comment is required.';
      return false;
    }
    const postComment = {
      comment: this.editTicketForm.controls['comment'].value,
      ticketId: this.ticket.ticketId
    };
    this.editTicketForm.controls['comment'].setValue('');
    this.spinner.showSpinner.next(true);
    this.supportService.addComment(postComment).subscribe(
      resposne => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: resposne['message']
          }
        });
        this.getSuportTicket();
        this.spinner.showSpinner.next(false);
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error.message
          }
        });
        this.spinner.showSpinner.next(false);
    });
  }
}
