import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserTicketDetailsComponent } from './advertiser-ticket-details.component';

describe('AdvertiserTicketDetailsComponent', () => {
  let component: AdvertiserTicketDetailsComponent;
  let fixture: ComponentFixture<AdvertiserTicketDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserTicketDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserTicketDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
