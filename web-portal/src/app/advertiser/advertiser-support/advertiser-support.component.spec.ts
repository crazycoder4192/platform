import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserSupportComponent } from './advertiser-support.component';

describe('AdvertiserSupportComponent', () => {
  let component: AdvertiserSupportComponent;
  let fixture: ComponentFixture<AdvertiserSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserSupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
