import { Component, OnInit } from '@angular/core';
import { SupportService } from 'src/app/_services/admin/support.service';
import { Ticket } from 'src/app/_models/ticket_model';

@Component({
  selector: 'app-advertiser-support',
  templateUrl: './advertiser-support.component.html',
  styleUrls: ['./advertiser-support.component.scss']
})
export class AdvertiserSupportComponent implements OnInit {
  displayedColumns: string[] = ['id', 'subject', 'priority', 'status', 'createdOn', 'updatedOn', 'actions'];
  tickets: Ticket[];

  constructor(
    private supportService: SupportService,
  ) { }

  ngOnInit() {
    this.loadAllTickets();
  }
  loadAllTickets() {
    this.supportService.getTicketsByEmail().subscribe(
      resposne => {
        this.tickets = <Ticket[]>resposne;
      },
      err => {
        console.log('Error! createTicket.supportService.addTicket : ' + err);
    });
  }
}
