import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvertiserComponent } from './advertiser.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { BrandAnalyticsComponent } from './brand-analytics/brand-analytics.component';
import { PaymentsComponent } from './payments/payments.component';
import { DraftsComponent } from './drafts/drafts.component';
import { CampaignCreationComponent } from './campaign-creation/campaign-creation.component';
import { CampaignManageComponent } from './campaign-manage/campaign-manage.component';
import { CreativeHUBComponent } from './creative-hub/creative-hub.component';
import { AccountCreationComponent } from './account-creation/account-creation.component';
import { AdvertiserProfileComponent } from './advertiser-profile/advertiser-profile.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthGuard } from '../_guards/auth-guard.guard';
import { AdvertiserProfileThanksComponent } from './advertiser-profile-thanks/advertiser-profile-thanks.component';
import { PaymentFailureComponent } from './payments/payment-failure/payment-failure.component';
import { PaymentSuccessComponent } from './payments/payment-success/payment-success.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { AdvertiserSupportComponent } from './advertiser-support/advertiser-support.component';
import { AdvertiserTicketDetailsComponent } from './advertiser-support/advertiser-ticket-details/advertiser-ticket-details.component';
import { InvitationCodeComponent } from './invitation-code/invitation-code.component';
import { AudienceTargetComponent } from './audience-target/audience-target.component';
import { PaymentSecureContentComponent } from './payments/payment-secure-content/payment-secure-content.component';

const routes: Routes = [
  {
    path: '', component: AdvertiserComponent, canActivate: [AuthGuard],
    children: [
      {
        path: '', canActivateChild: [AuthGuard],
        children: [
          { path: 'paymentSuccess', component: PaymentSuccessComponent},
          { path: 'advertiserProfile', component: AdvertiserProfileComponent},
          { path: 'campaignCreation', component: CampaignCreationComponent},
          { path: 'campaignManage', component: CampaignManageComponent},
          { path: 'analytics', component: AnalyticsComponent},
          { path: 'brandAnalytics', component: BrandAnalyticsComponent},
          { path: 'payments', component: PaymentsComponent},
          { path: 'paymentFailure', component: PaymentFailureComponent},
          { path: 'securePaymentContent', component: PaymentSecureContentComponent},
          { path: 'drafts', component: DraftsComponent},
          { path: 'creativeHUB', component: CreativeHUBComponent},
          { path: 'accountCreation', component: AccountCreationComponent},
          { path: 'settings', component: SettingsComponent},
          { path: 'advertiserProfileThanks', component: AdvertiserProfileThanksComponent},
          { path: 'dashboard', component: DashboardComponent},
          { path: 'brandDashboard', component: DashboardDetailsComponent, data: {animation: 'brandDashboard'}},
          { path: 'advertiserSupport', component: AdvertiserSupportComponent},
          { path: 'ticketDetails/:id', component: AdvertiserTicketDetailsComponent},
          { path: 'invitationCode', component: InvitationCodeComponent},
          { path: 'audience', component: AudienceTargetComponent},
          { path: '**', redirectTo: '/advertiser/dashboard'}
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvertiserRoutingModule { }
