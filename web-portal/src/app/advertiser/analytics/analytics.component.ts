import { Component, OnInit, OnDestroy,  ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { EMPTY, Subscription, interval } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CampaignElement } from '../../_models/advertiser_campaign';
import { SubCampaignElement } from '../../_models/advertiser_sub_campaign';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AdvertiserAnalyticsService } from 'src/app/_services/advertiser/advertiser-analytics.service';
import { UtilService } from '../../_services/utils/util.service';
import { AdvertiserUserService } from '../../_services/advertiser/advertiser-user.service';
import * as d3 from 'd3';
import { MatSnackBar, MatOption, MatDialogConfig, MatDialog } from '@angular/material';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { AdvertiserBrandService } from '../../_services/advertiser/advertiser-brand.service';
import { Brand } from '../../_models/advertiser_brand_model';
import { DropDownWithTreeComponent } from 'src/app/common/drop-down-with-tree/drop-down-with-tree.component';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { Subject } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { log } from 'util';
import { SpinnerService } from 'src/app/_services/spinner.service';
import config from 'appconfig.json';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit, OnDestroy {
  public rolesAndPermissions: any[];
  public rolePermissions: any;
  public viewBrand = false;
  public audience = false;
  public behavior = false;
  public conversion = false;
  public campaignCtrl = new FormControl();
  public uniqueStates: string[];
  public saveFilterForm: FormGroup;
  public subCampaignName: string;
  public subCampaignWebTypes: string;
  public duration = new FormControl();
  public reach = new FormControl();
  public startDate: Date = new Date(2019, 1, 1);
  public startDateMin = new Date(2019, 1, 1);
  public todayDate: Date = new Date(new Date().setHours(23, 59, 59));
  public endDateMin: Date;
  public endDateMax: Date = this.todayDate;
  public startDateMax = this.todayDate;
  public specializations: any;
  public campaignDates: string;
  public budget: string;
  public subCampaignLocations = '';
  public locationdata: any;
  public showfilters = false;
  public cardsData: any = {};

  public toggleSpends: any = 'Spends';
  public toggleCPM: any = undefined;
  public toggleCTR: any = 'CTR';
  public toggleCPC: any = undefined;
  public activeToggleArray: string[] = ['Spends', 'CTR'];
  public toggleStateMap: Map<string, string> = new Map<string, string>();
  private conversionResultData: any;

  public savedFilters;

  public isCampaignAnalyticsView = false;
  public isSubcampaignSelected = false;
  public isHcpFilterSelected = false;
  public currentlyViewedBrand: Brand;
  public selectedCampaign: CampaignElement;
  public selectedSubCampaign: SubCampaignElement;
  public selectedSubCampaignId = '';

  public brandName: string;
  public brandClientType: string;
  public activeCampaignsInBrand: number;
  public brandCreateDate: Date;

  private advertiserId: string;
  private brandId: string;
  private campaignId: string;
  private subCampaignId: string;



  public campaignsList: CampaignElement[];
  public subCampaignsList: SubCampaignElement[];
  public allBrandSubCampaignsList: SubCampaignElement[];

  public engagementImpressions = 0;
  public engagementReach = 0;
  public engagementClicks = 0;

  public resultSpends = 0;
  public resultCPM = 0;
  public resultCTR = 0;
  public resultCPC = 0;
  public loadGraphs;

  public analyticsGraphsData;
  public noDataForConversionResult = false;
  private navigatedFromManageCampaigns = false;
  public locationDetailsdata;
  private updateSubscription: Subscription;
  @ViewChild('ddTreeLocation') ddTreeLocation: DropDownWithTreeComponent;
  public locationList: any[];
  public locationTreeList: any;
  public allLocationTreeItems: TreeviewItem[];
  private selectedLocation: any[] = [];
  private persistSelectedLocation = [];

  @ViewChild('ddTreeSpecialization') ddTreeSpecialization: DropDownWithTreeComponent;
  public specializationList: any[];
  public specializationTreeList: any;
  public allSpecializationTreeItems: TreeviewItem[];
  private selectedSpecialization: any[] = [];
  private persistSelectedSpecialization = [];

  @ViewChild('ddTreeGender') ddTreeGender: DropDownWithTreeComponent;
  public genderList: any[];
  public genderTreeList: any;
  public allGenderTreeItems: TreeviewItem[];
  private selectedGender: any[] = [];
  private persistSelectedGender = [];

  public showGraphSpinner = true;
  public filteredDuration = [
    { value: 'Custom Duration', name: 'Custom Duration' },
    { value: 'Last 24 hours', name: 'Last 24 hours' },
    { value: 'Last 48 hours', name: 'Last 48 hours' },
    { value: 'Last 1 week', name: 'Last 1 week' },
    { value: 'Last 1 month', name: 'Last 1 month' },
    { value: 'Last 6 months', name: 'Last 6 months' }
  ];

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  isApply: any;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set([
    'advertiser_analytics_spends', 'advertiser_analytics_reach',
    'advertiser_analytics_impressions', 'advertiser_analytics_clicks',
    'advertiser_analytics_ctr', 'advertiser_analytics_cpc',
    'advertiser_analytics_cpm', 'advertiser_analytics_audience_specializations',
    'advertiser_analytics_audience_location', 'advertiser_analytics_audience_hcp_archetypes',
    'advertiser_analytics_audience_frequency', 'advertiser_analytics_behaviour_time_of_the_day',
    'advertiser_analytics_behaviour_display_location', 'advertiser_analytics_behaviour_devices',
    'advertiser_analytics_conversion_engagement', 'advertiser_analytics_conversion_results',
    'advertiser_analytics_behaviour_top_campaigns', 'advertiser_analytics_behaviour_active_campaigns']);
  activeTab: any;
  graphLoaded: boolean;
  startAnalyticsDate: any;
  endAnalyticsDate: any;
  analyticsGraphsDataTemp: any;
  hideExtraGraph: boolean;
  audienceFrequencyLegends: string[];
  enableCampaignsTab: boolean;
  enableConversionTab: boolean;
  enableBehaviorTab: boolean;
  enableAudienceTab: boolean;
  locationListForPdf: any;
  specializationListForPdf: any;
  behaviorLegend: any[];
  private currencySymbol : string;
  zone = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  constructor(private formBuilder: FormBuilder, private campaignService: CampaignService,
    private advertiserAnalyticsService: AdvertiserAnalyticsService, private utilService: UtilService,
    private advertiserUserService: AdvertiserUserService, private snackBar: MatSnackBar,
    private advertiserBrandService: AdvertiserBrandService,
    private eventEmitterService: EventEmitterService,
    private dialog: MatDialog,
    private toolTipService: ToolTipService,
    private router: Router,
    private spinner: SpinnerService
  ) {
    localStorage.setItem('activeIcon', JSON.stringify(''));
    this.eventEmitterService.onClickNavigation();
    if (this.router.getCurrentNavigation().extras.state) {
      if (this.router.getCurrentNavigation().extras.state.campaignId) {
        this.navigatedFromManageCampaigns = true;
        this.campaignId = JSON.parse(this.router.getCurrentNavigation().extras.state.campaignId);
      }
      if (this.router.getCurrentNavigation().extras.state.subCampaignId) {
        this.subCampaignId = JSON.parse(this.router.getCurrentNavigation().extras.state.subCampaignId);
      }
    }
    if (this.subCampaignId) {
      this.activeTab = 'audienceTab';
      this.isSubcampaignSelected = true;
      this.hideExtraGraph = false;
    } else {
      this.activeTab = 'campaignsTab';
    }
  }

  public displayFilters() {
    this.showfilters = !this.showfilters;
  }

  get f() { return this.saveFilterForm.controls; }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.enableCampaignsTab = false;
    this.enableConversionTab = false;
    this.enableBehaviorTab = false;
    this.enableAudienceTab = false;
    this.analyticsGraphsData = {
      campaignsActiveCampaigns: [],
      campaignsTopCampaigns: [],
      audienceGenderExperience: [],
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceFrequency: [],
      behaviorTimeOfDayEnagement: [],
      behaviorDisplayLocation: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResult: null
    };
    this.analyticsGraphsDataTemp = {
      campaignsActiveCampaigns: [],
      campaignsTopCampaigns: [],
      audienceGenderExperience: [],
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceFrequency: [],
      behaviorTimeOfDayEnagement: [],
      behaviorDisplayLocation: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResult: null
    };
    this.hideExtraGraph = true;
    this.graphLoaded = false;
    this.saveFilterForm = this.formBuilder.group({
      _id: [''],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      duration: ['Custom Duration', Validators.required],
      reach: [''],
      specialization: ['', Validators.required],
      filter: [''],
      gender: ['', Validators.required],
      location: ['', Validators.required],
      advertiserId: this.advertiserId,
      filterName: [''],
      subCampaignId: [[this.subCampaignId]],
      campaignId: [[this.campaignId]],
      brandId: [[this.brandId]],
      email: this.userDetails.email,
      isHcpFilterSelected: false,
      timezone: '',
      tabType: ''
    });

    this.initRolesAndPermissions();
    this.loadGraphs = true;

    this.utilService.getFiltersByEmail(this.userDetails.email).subscribe(
      response => {
        this.savedFilters = response;
      },
      err => {

      });

    this.advertiserUserService.getAdvertiserUser(this.userDetails.email).subscribe(
      response => {
        this.advertiserId = response['advertiserId'];
      });
    this.loadCampaignsAndSubCampaigns();

    this.toggleStateMap.set('Spends', 'Spends');
    this.toggleStateMap.set('CPM', undefined);
    this.toggleStateMap.set('CTR', 'CTR');
    this.toggleStateMap.set('CPC', undefined);
    this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }
  ngOnDestroy() {
    this.loadGraphs = null;
  }

  loadCampaignsAndSubCampaigns() {
    this.advertiserBrandService.getAllBrands().pipe(
      tap(bresponse => {
        if (bresponse && bresponse.length > 0) {
          this.currentlyViewedBrand = bresponse[0];
          this.brandName = this.currentlyViewedBrand.brandName;
          this.brandClientType = this.currentlyViewedBrand.clientType;
          this.brandCreateDate = this.currentlyViewedBrand.created.at;
          this.brandId = this.currentlyViewedBrand._id;
        }
      }),
      switchMap(bresponse => {
        if (bresponse && bresponse[0] && bresponse[0]._id) {
          return this.campaignService.getCampaignByBrandId(bresponse[0]._id);
        } else {
          return EMPTY;
        }
      }),
      tap(cresponse => {
        this.campaignsList = <CampaignElement[]>cresponse;
        if (!this.campaignsList.length) {
          this.resetGraphs();
          this.hideExtraGraph = false;
        }
      }),
      switchMap(cresponse => {
        if (cresponse && cresponse[0] && cresponse[0].brandId) {
          return this.campaignService.getSubcampaignByBrandId(cresponse[0].brandId);
        } else {
          this.showGraphSpinner = null;
          return EMPTY;
        }
      })
    ).subscribe(
      fresponse => {
        this.allBrandSubCampaignsList = <SubCampaignElement[]>fresponse;
        let count = 0;
        for (const s of this.allBrandSubCampaignsList) {
          if (s.status.toUpperCase() === 'active'.toUpperCase()) {
            count++;
          }
        }
        this.activeCampaignsInBrand = count;
        this.isApply = false;
        if (this.navigatedFromManageCampaigns === true) {
          if (this.campaignId && !this.subCampaignId) {
              this.onSelectCampaignInBrandView(this.campaignId);
          } else if (this.campaignId && this.subCampaignId) {
              this.onSelectCampaignInBrandView(this.campaignId);
              setTimeout(() => {
                this.onSelectSubCampaign(this.subCampaignId);
              }, 1000);
          }
        } else {
          this.populateValuesInFilters();
          this.getAnalyticsData(this.activeTab);
        }
      });

  }

  resetDropdownFilters() {
    this.isHcpFilterSelected = false;
    this.saveFilterForm.get('specialization').setValue('');
    this.saveFilterForm.get('gender').setValue('');
    this.saveFilterForm.get('location').setValue('');
    this.saveFilterForm.get('isHcpFilterSelected').setValue('');
  }

  onSelectFilter(id) {
    const filterData = this.savedFilters.find((x) => x._id === id);
    this.ddTreeLocation.onSelect(filterData.location);
    this.ddTreeLocation.updateItems(this.allLocationTreeItems, filterData.location);

    this.ddTreeSpecialization.onSelect(filterData.specialization);
    this.ddTreeSpecialization.updateItems(this.allSpecializationTreeItems, filterData.specialization);

    this.ddTreeGender.onSelect(filterData.gender);
    this.ddTreeGender.updateItems(this.allGenderTreeItems, filterData.gender);

    /*this.saveFilterForm.get('specialization').setValue(filter.specialization);
    this.saveFilterForm.get('gender').setValue(filter.gender);
    this.saveFilterForm.get('location').setValue(filter.location);
    this.saveFilterForm.get('isHcpFilterSelected').setValue(true);*/
  }

  saveFilter() {
    const filterName: string = this.saveFilterForm.get('filterName').value;
    this.utilService.checkFilterNameExists({'filterName': filterName}).subscribe(
      response => {
        if (response['filterExist']) {
          // indicates filter name already exist
          const dialogConfig = new MatDialogConfig();
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            title: 'Overwrite Filter?',
            message: `A filter with the name already exists. Do you want to overwrite it?`,
            confirm: 'Yes',
            cancel: 'No'
          };
          dialogConfig.minWidth = 400;
          const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              this.invokeSaveFilter();
              dialogRef.close();
            } else {
              dialogRef.close();
            }
          });
        } else {
          this.invokeSaveFilter();
        }
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }

  private invokeSaveFilter() {
    const filterData = {
      'filterName': this.saveFilterForm.get('filterName').value,
      'specialization': this.selectedSpecialization,
      'gender': this.selectedGender,
      'location': this.selectedLocation
    };
    this.utilService.saveFilter(filterData).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
      },
      err => {

      }
    );
  }

  private populateValuesInFilters() {
    // this.resetDropdownFilters();

    let type = '';
    let id = '';
    if (this.brandId) {
      type = 'brand';
      id = this.brandId;
    } else if (this.campaignId) {
      type = 'campaign';
      id = this.campaignId;
    } else if (this.subCampaignId) {
      type = 'subcampaign';
      id = this.subCampaignId;
    } else {
      console.error('Invalid type: at least one type (brand, campaign or subcampaign) expected');
    }

    this.utilService.getValidHCPLocationsByTypeAndId(type, id).subscribe(response => {
      /* TreeSearch ignore's the first node, that's why inserted 'dummy data' item in the case of location
      as the first item in our case is US and we want to show that*/
      this.locationList = [];
      this.locationList.push('dummy data');
      this.locationList.push(response);
      this.locationTreeList = new TreeviewItem(<any>response);
      this.allLocationTreeItems = [this.locationTreeList];
    });
    this.utilService.getValidHCPLocationListByTypeAndId(type, id).subscribe(response => {
      this.locationListForPdf = response;
    });
    this.utilService.getValidHCPSpecializationByTypeAndId(type, id).subscribe(response => {
      this.specializationList = [];
      this.specializationList.push(response);
      this.specializationTreeList = new TreeviewItem(<any>response);
      this.allSpecializationTreeItems = [this.specializationTreeList];
    });
    this.utilService.getValidHCPSpecializationListByTypeAndId(type, id).subscribe(response => {
      this.specializationListForPdf = response;
    });
    this.utilService.getValidHCPGenderByTypeAndId(type, id).subscribe(response => {
      this.genderList = [];
      this.genderList.push(response);
      this.genderTreeList = new TreeviewItem(<any>response);
      this.allGenderTreeItems = [this.genderTreeList];
    });
  }

  private unwindFilter() {

    this.persistSelectedLocation = JSON.parse(JSON.stringify(this.selectedLocation));
    this.saveFilterForm.get('location').setValue(this.selectedLocation);

    this.persistSelectedSpecialization = JSON.parse(JSON.stringify(this.selectedSpecialization));
    this.saveFilterForm.get('specialization').setValue(this.selectedSpecialization);
    this.persistSelectedGender = JSON.parse(JSON.stringify(this.selectedGender));
    this.saveFilterForm.get('gender').setValue(this.selectedGender);
    this.isHcpFilterSelected = false;
    if (this.selectedSpecialization.length > 0 || this.selectedGender.length > 0 ||
      this.selectedLocation.length > 0) {
        this.isHcpFilterSelected = true;
    }
    this.saveFilterForm.get('isHcpFilterSelected').setValue(this.isHcpFilterSelected);
  }

  treeSearch(tree, value) {
    return tree.reduce(function f(acc, node) {
        return (node.value === value) ? node :
            (node.children && node.children.length) ? node.children.reduce(f, acc) : acc;
    });
  }
  nodeSearch(tree, value) {
    return tree.reduce(function f(acc, node) {
        return (node.value === value) ? node :
            (node.children && node.children.length) ? node.children.reduce(f, acc) : acc;
    });
  }
  onLocationSelectedChange(events) {
    this.selectedLocation = events;
  }

  onSpecializationSelectedChange(events) {
    this.selectedSpecialization = events;
  }

  onGenderSelectedChange(events) {
    this.selectedGender = events;
  }

  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    this.endDateMin = this.startDate;
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDateMin = this.startDate;
    this.startDateMax = new Date(this.saveFilterForm.get('endDate').value);
  }

  onSelectDuration(duration) {
    const endDate = new Date();
    this.saveFilterForm.get('endDate').setValue(endDate);
    const startDate = new Date();
    if (duration === 'Custom Duration') {
      this.saveFilterForm.get('startDate').setValue(this.brandCreateDate);
      this.saveFilterForm.get('startDate').enable();
      this.saveFilterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      startDate.setHours(startDate.getHours() - 24);
      this.saveFilterForm.get('startDate').setValue(startDate);
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
    } else if (duration === 'Last 48 hours') {
      startDate.setHours(startDate.getHours() - 48);
      this.saveFilterForm.get('startDate').setValue(startDate);
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
    } else if (duration === 'Last 1 week') {
      startDate.setHours(startDate.getDay() - 7 * 24);
      this.saveFilterForm.get('startDate').setValue(startDate);
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
    } else if (duration === 'Last 1 month') {
      startDate.setMonth(startDate.getMonth() - 1);
      this.saveFilterForm.get('startDate').setValue(startDate);
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
    } else if (duration === 'Last 6 months') {
      startDate.setMonth(startDate.getMonth() - 6);
      this.saveFilterForm.get('startDate').setValue(startDate);
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
    }
  }

  initRolesAndPermissions() {
    this.rolesAndPermissions = [];
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    if (this.rolePermissions && this.rolePermissions.length) {
      this.rolePermissions.forEach(element => {
        if (element.identifier === 'analytics_view_brand') {
          this.viewBrand = true;
        }

        if (element.identifier === 'analytics_audience') {
          this.audience = true;
        }
        if (element.identifier === 'analytics_behavior') {
          this.behavior = true;
        }

        if (element.identifier === 'analytics_conversion') {
          this.conversion = true;
        }
      });
    }
  }

  switchToBrandView() {
    this.hideExtraGraph = true;
    this.enableCampaignsTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.activeTab = 'campaignsTab';
    this.isCampaignAnalyticsView = false;
    this.isSubcampaignSelected = false;
    this.brandName = this.currentlyViewedBrand.brandName;
    this.brandId = this.currentlyViewedBrand._id;
    this.brandClientType = this.currentlyViewedBrand.clientType;
    this.brandCreateDate = this.currentlyViewedBrand.created.at;
    this.campaignId = null;
    this.subCampaignId = null;
    this.populateValuesInFilters();
    this.getAnalyticsData(this.activeTab);
  }

  onSelectCampaignInBrandView(campaignId: string) {
    this.isSubcampaignSelected = false;
    if (campaignId !== undefined) {
      this.brandId = null;
      this.brandClientType = null;
      this.brandName = null;

      this.isCampaignAnalyticsView = true;
      this.campaignId = campaignId;
      this.selectedCampaign = this.campaignsList.find(x => x._id === campaignId);
      this.saveFilterForm.get('startDate').setValue(this.selectedCampaign.created.at);
      this.saveFilterForm.get('endDate').setValue(new Date());
      this.onSelectCampaignInCampaignView(campaignId);
    }
  }

  onSelectCampaignInCampaignView(campaignId) {
    this.hideExtraGraph = true;
    this.enableCampaignsTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.analyticsGraphsDataTemp = {
      campaignsActiveCampaigns: [],
      campaignsTopCampaigns: [],
      audienceGenderExperience: [],
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceFrequency: [],
      behaviorTimeOfDayEnagement: [],
      behaviorDisplayLocation: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResult: null
    };
    this.isSubcampaignSelected = false;
    if (campaignId !== undefined) {
      // this.subCampaignId = null;
      this.subCampaignsList = [];

      this.campaignId = campaignId;
      this.campaignService.getAllSubCampaignsForSpecificCampaign(campaignId).subscribe(
        response => {
          const all: any = {};
          all._id = 'all';
          all.name = 'All';

          this.subCampaignsList = <SubCampaignElement[]>response;
          this.populateValuesInFilters();
          this.getAnalyticsData(this.activeTab);
        },
        err => {

        }
      );
    }
  }

  onSelectSubCampaign(subcampaignId: string) {
    if (this.activeTab === 'campaignsTab') {
      this.activeTab = 'audienceTab';
    }
    this.hideExtraGraph = true;
    this.enableCampaignsTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.isSubcampaignSelected = true;
    this.analyticsGraphsDataTemp = {
      campaignsActiveCampaigns: [],
      campaignsTopCampaigns: [],
      audienceGenderExperience: [],
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceFrequency: [],
      behaviorTimeOfDayEnagement: [],
      behaviorDisplayLocation: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResult: null
    };
    if (subcampaignId !== undefined) {
      this.selectedSubCampaign = this.subCampaignsList.find(x => x._id === subcampaignId);
      this.selectedSubCampaignId = this.selectedSubCampaign._id;
      let subCampaignElements: SubCampaignElement[] = [];
      if ( this.selectedSubCampaign._id === 'all') {
        subCampaignElements = this.subCampaignsList.splice(this.subCampaignsList.indexOf(this.selectedSubCampaign), 1);
      } else {
        subCampaignElements = [this.selectedSubCampaign];
        this.subCampaignId =  this.selectedSubCampaign._id;
        this.subCampaignName = this.selectedSubCampaign.name;
      }
      this.populateValuesInFilters();
      this.getAnalyticsData(this.activeTab);
    }
  }

  drawAudienceGenderAndExperienceChart() {
    this.drawMaleChart();
    this.drawFemaleChart();
    this.drawOtherChart();
  }

  drawMaleChart() {
    const data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2]
    ]);

    const options: any = {
      colors: ['#78eded', '#a5f3f3', '#d2f9f9'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('malePieChart'));

    chart.draw(data, options);
  }

  drawFemaleChart() {
    const data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2]
    ]);

    const options: any = {
      colors: ['#ce85e0', '#deaeea', '#efd6f5'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('femalePieChart'));

    chart.draw(data, options);
  }

  drawOtherChart() {
    const data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2]
    ]);

    const options: any = {
      colors: ['#79cdcd', '#8deeee', '#97ffff'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('othersPieChart'));

    chart.draw(data, options);
  }

  drawAudienceSpecializationStackedBar(barData) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Specialization');
    data.addColumn('number', 'Male');
    data.addColumn('number', 'Female');
    data.addColumn('number', 'Other');
    for (let i = 0; i < barData.length; i++) {
      data.addRow([barData[i].specialization, barData[i].male, barData[i].female, barData[i].other]);
    }

    const options: any = {
      legend: { position: 'bottom', alignment: 'end' },
      bar: { groupWidth: '40%' },
      isStacked: true,
      colors: ['A569D6', '6646ED','5FBDEA'],
      chartArea: { width: '90%', height: '70%'  },
      vAxis: { viewWindow: {min: 0}},
      width: 1000,
      height: 300,
      hAxis: {
        slantedText: false
      }
    };

    const chart = new google.visualization.ColumnChart(document.getElementById('specializationStackedBarChart'));
    const columns = [
      { title: 'Specialization', dataKey: 'specialization' },
      { title: 'Male', dataKey: 'male' },
      { title: 'Female', dataKey: 'female' },
      { title: 'Other', dataKey: 'other' },
    ];

    const rows: any[] = [];
    for (let i = 0; i < barData.length; i++) {
      const row: any = {specialization: barData[i].specialization, male: barData[i].male,
      female: barData[i].female, other: barData[i].other};
      rows.push(row);
    }
    this.pushToPDF(chart, 'Specializations', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawLocationGraph(audienceLocation) {
    const lData = new google.visualization.DataTable();
    lData.addColumn('string', 'State');
    lData.addColumn('number', 'Popularity');
    for (const loc of audienceLocation) {
      const temp: any[] = [];
      temp.push(loc.state, loc.count);
      lData.addRows([temp]);
    }

    this.locationDetailsdata = audienceLocation;
    const options: any = {
      colorAxis: {colors: ['#5FBDEA', '#A569D6']},
      region: 'US',
      displayMode: 'regions',
      resolution: 'provinces',
      legend: {
        position: 'none'
      }
    };
    options.region = this.zone === '1' ? 'US' : this.zone === '2' ? 'IN' : 'US';

    const chart = new google.visualization.GeoChart(document.getElementById('audienceLocationChart'));
    const columns = [
      { title: 'State', dataKey: 'state' },
      { title: 'Popularity', dataKey: 'popularity' }
    ];

    const rows: any[] = [];
    for (const loc of audienceLocation) {
      const row: any = {state: loc.state, popularity: loc.showValue};
      rows.push(row);
    }
    this.pushToPDF(chart, 'Location', {columns: columns, rows: rows});
    chart.draw(lData, options);
  }

  drawAudienceHCPArcheTypeBarChart(archetypeData) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Archetype');
    data.addColumn('number', 'Numbers');

    if (archetypeData.connectors === 0 && archetypeData.curators === 0 &&
      archetypeData.followers === 0 && archetypeData.pharmaiety === 0 &&
      archetypeData.toolBoxers === 0) {
        return;
    }

    data.addRows([
      ['Connectors', archetypeData.connectors],
      ['Curators', archetypeData.curators],
      ['Followers', archetypeData.followers],
      ['Pharmaiety', archetypeData.pharmaiety],
      ['Tool Boxers', archetypeData.toolBoxers]
    ]);
    const options: any = {
      vAxis: {
        title: '',
        viewWindow: {min: 0}
      },
      colors: ['#A569D6'],
      chartArea: { 'width': '90%', 'height': '70%' },
      bar: { groupWidth: '50%' },
      legend: {
        position: 'bottom',
        alignment: 'end'
      },
      width: 540,
      height: 300
    };

    const chart = new google.visualization.ColumnChart(
      document.getElementById('drawAudienceHCPArcheTypeBarChart'));
    const columns = [
      { title: 'Archetype', dataKey: 'archeType' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    rows.push({archeType: 'Connectors', count: archetypeData.connectors});
    rows.push({archeType: 'Curators', count: archetypeData.curators});
    rows.push({archeType: 'Followers', count: archetypeData.followers});
    rows.push({archeType: 'Pharmaiety', count: archetypeData.pharmaiety});
    rows.push({archeType: 'Tool Boxers', count: archetypeData.toolBoxers});
    this.pushToPDF(chart, 'HCP Archetype', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawAudienceFrequencyPieChart(frequencyData: any) {
    this.audienceFrequencyLegends = [];
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'No of views');
    data.addColumn('number', 'count');
    for (let i = 0; i < frequencyData.length; i++) {
      data.addRow([`${frequencyData[i].count.toString()} views  -  ${frequencyData[i].percent.toFixed(2)}` + ' %', frequencyData[i].count]);
    }

    const options: any = {
      // pieHole: 0.5,
      colors: ['#A569D6', '#5FBDEA', '#E07F75', '#ECB057', '#6646ED', '#B82E2E', '316395'],
      chartArea: { 'width': '90%', 'height': '90%' },
      legend: {
        position: 'right'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('audienceFrequencyPieChart'));
    const columns = [
      { title: 'Views', dataKey: 'noOfViews' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    for (let i = 0; i < frequencyData.length; i++) {
      const row: any = {noOfViews: frequencyData[i]._id.toString(), count: frequencyData[i].count };
      rows.push(row);
    }
    this.pushToPDF(chart, 'Frequency', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawBehaviourHeatMap(data: any) {
    let hasValues = false;
    for (const d of data) {
      if (d.count > 0) {
        hasValues = true;
      }
    }
    if (!hasValues) {
      return;
    }
    const el: HTMLElement = document.getElementById('heatmap');
    if (el !== undefined && el !== null) {
      el.innerHTML = '';
    }
    const yAxisValues: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    const xAxisValues: string[] = ['00-03', '03-06', '06-09', '09-12', '12-15', '15-18', '18-21', '21-24'];
    const margin = { top: 10, right: 20, bottom: 20, left: 40 },
      width = 540 - margin.left - margin.right,
      height = 300 - margin.top - margin.bottom;

      const svg = d3.selectAll('#heatmap')
                    .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                    .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    // Build X scales and axis:
    const x = d3.scaleBand()
      .range([0, width])
      .domain(xAxisValues)
      .padding(0.01);

    svg.append('g')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x));

    // Build Y scales and axis:
    const y = d3.scaleBand()
      .rangeRound([height, 0])
      .domain(yAxisValues)
      .padding(0.10);

    svg.append('g')
      .call(d3.axisLeft(y));

    const myColor = d3.scaleLinear<string>()
      .range(['#A569D6', '#5FBDEA'])
      .domain([100, 1000]);

    const tooltip = d3.select('#heatmap')
      .append('div')
      .style('opacity', 0)
      .attr('class', 'tooltip')
      .style('background-color', 'white')
      .style('border', 'solid')
      .style('border-width', '2px')
      .style('border-radius', '5px')
      .style('padding', '5px');

    svg.selectAll()
      .data(data, function (d: any) { return d; })
      .enter()
      .append('rect')
      .attr('x', function (d: any) { return x(d.hourRange); })
      .attr('y', function (d: any) { return y(d.weekDay); })
      .attr('width', x.bandwidth())
      .attr('height', y.bandwidth())
      .style('fill', function (d: any) { return myColor(d.count); })
      .on('mouseover', function (d) {
        tooltip.style('opacity', 1);
      })
      .on('mousemove', function (d) {
        tooltip
          .html('views: ' + d.count)
          .style('left', (d3.mouse(this)[0] + 70) + 'px')
          .style('top', (d3.mouse(this)[1]) + 'px');
      })
      .on('mouseleave', function (d) {
        tooltip.style('opacity', 0);
      });
  }

  drawBehaviorDisplayLocationStackedBarChart(displayLocation: any[]) {
    const options: any = {

      legend: { position: 'bottom', alignment: 'start' },
      bar: { groupWidth: '20%' },
      isStacked: true,
      colors: ['A569D6', '5FBDEA', 'ECB057', '6646ED', '5FBDEA', 'E07F75', 'B82E2E', '310095', '316300', '006395'],
      chartArea: { 'width': '85%', 'height': '70%' },
      width: 630,
      height: 300,
      vAxis: { viewWindow: {min: 0}}
    };
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'PlatformType');
    this.behaviorLegend = [];
    if (displayLocation.length > 0) {
      for (let p = 0;  displayLocation[0].platform.length > p; p++) {
          data.addColumn('number', displayLocation[0].platform[p].dimension);
          this.behaviorLegend.push({
            labelName: displayLocation[0].platform[p].dimension,
            color: options.colors[p]
          });
      }
    }

    for (const l of displayLocation) {
      const s: any[] = [];
      s.push(l.platformType);
      for (const d of l.platform ) {
        s.push(d.count);
      }
      data.addRow(s);
    }

    const chart = new google.visualization.ColumnChart(document.getElementById('behaviorDisplayLocationStackedBarChart'));
    const columns = [
      { title: 'Platform Type', dataKey: 'platformType' },
      { title: 'Dimension', dataKey: 'dimension' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    for (const l of displayLocation) {
      for (const d of l.platform ) {
        const row: any = {platformType: l.platformType, dimension: d.dimension, count: d.count};
        rows.push(row);
      }
    }
    this.pushToPDF(chart, 'Display Location', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawBehaviorDevicesDonutChart(deviceData: any) {
    if (deviceData.desktop === 0 && deviceData.mobile === 0 &&
      deviceData.tablet === 0) {
        return;
    }

    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Devices');
    data.addColumn('number', 'count');
    data.addRow(['Desktop', deviceData.desktop]);
    data.addRow(['Mobile', deviceData.mobile]);
    data.addRow(['Tablet', deviceData.tablet]);

    const options: any = {
      pieHole: 0.5,
      colors: ['#5FBDEA', '#A569D6', '#5FBDEA'],
      chartArea: { 'width': '90%', 'height': '80%' },
      legend: { position: 'bottom', maxLines: 3, alignment: 'center',  textStyle: {fontSize: 18} },
      width: 370,
      height: 300,

    };

    const chart = new google.visualization.PieChart(document.getElementById('behaviorDevicesDonutChart'));
    const columns = [
      { title: 'Device', dataKey: 'device' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    rows.push({device: 'Desktop', count: deviceData.desktop});
    rows.push({device: 'Mobile', count: deviceData.mobile});
    rows.push({device: 'Tablet', count: deviceData.tablet});
    this.pushToPDF(chart, 'Device', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawConversionEngagementAreaChart(conversionEnagement) {
    this.engagementImpressions = 0;
    this.engagementReach =  0;
    this.engagementClicks =  0;

    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Impressions');
    data.addColumn('number', 'Reach');
    data.addColumn('number', 'Clicks');

    let isAnyDataPresent = false;
    const chartData: any[] = conversionEnagement.data;
    for (const c of chartData) {
      if (c.durationInstance > 0 || c.impressions > 0 || c.reaches > 0 || c.clicks > 0) {
        isAnyDataPresent = true;
      }
      data.addRow([c.durationInstance, c.impressions, c.reaches, c.clicks]);
    }

    if (!isAnyDataPresent) {
      return;
    }

    this.engagementImpressions = conversionEnagement.summary.impressions;
    this.engagementReach =  conversionEnagement.summary.reaches;
    this.engagementClicks =  conversionEnagement.summary.clicks;

    const options: any = {
      hAxis: { titleTextStyle: { color: '#333'} },
      vAxis: { viewWindow: { min: 0 } },
      chartArea: { 'width': '90%', 'height': '80%' },
      width: 973,
      height: 350,
      legend: { position: 'none'},
      series: {
        0: { color: '#ec736b', visibleInLegend: false },
        1: { color: '#92d8f0', visibleInLegend: false },
        2: { color: '#b4a0cd', visibleInLegend: false }
      }
    };

    const chart = new google.visualization.AreaChart(document.getElementById('conversionEngagementAreaChart'));
    const columns = [
      { title: 'Month', dataKey: 'month' },
      { title: 'Impression', dataKey: 'impression' },
      { title: 'Reach', dataKey: 'reach' },
      { title: 'Click', dataKey: 'click' }
    ];

    const rows: any[] = [];
    for (const c of chartData) {
      const row: any = {month: c.durationInstance, impression: c.impressions, reach: c.reaches,  click: c.clicks};
      rows.push(row);
    }
    this.pushToPDF(chart, 'Engagement', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawConversionResultLineChart(conversionResults: any) {

    
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    const toggleValues = Array.from( this.toggleStateMap.values() );
    let dataRows = [];

    toggleValues.forEach(toggleElement => {
      if (this.toggleStateMap.get(toggleElement) !== undefined) {
        data.addColumn('number', this.toggleStateMap.get(toggleElement));
      //custom tooltip with html 
        data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
        //custom tooltip without html 
       //data.addColumn({type: 'string', role: 'tooltip'});
      }
    });

    if (conversionResults.summary.spends === 0 && conversionResults.summary.cpm === 0
      && conversionResults.summary.cpc === 0) {
        return;
    }

    const pdfTableRows: any[] = [];

    this.resultSpends = conversionResults.summary.spends;
    this.resultCPM = conversionResults.summary.cpm;
    this.resultCTR = conversionResults.summary.ctr;
    this.resultCPC = conversionResults.summary.cpc;

    const chartData: any[] = conversionResults.data;
    for (const c of chartData) {
      dataRows = [];
      dataRows.push(c.durationInstance);

      toggleValues.forEach(toggleElement => {
        if ( this.toggleStateMap.get(toggleElement) !== undefined ) {
          if (this.toggleStateMap.get(toggleElement) === 'Spends') {
            dataRows.push(c.totalSpends);
            //custom tooltip with html 
            dataRows.push('<p><b>' + c.durationInstance + '</b></p><p> Spends: <b>'+this.currencySymbol + c.totalSpends.toFixed(2) + '</b></p>');
           //custom tooltip without html 
            // dataRows.push(c.durationInstance + ' Spends: $' + c.totalSpends.toFixed(2));
          } else if (this.toggleStateMap.get(toggleElement) === 'CPM') {
            dataRows.push(c.avgCPM);
            //custom tooltip with html 
            dataRows.push('<p><b>' + c.durationInstance + '</b></p><p> CPM: <b>'+this.currencySymbol + c.avgCPM.toFixed(2) + '</b></p>');
            //custom tooltip without html 
            //dataRows.push(c.durationInstance + ' CPM: $' + c.avgCPM.toFixed(2));
          } else if (this.toggleStateMap.get(toggleElement) === 'CTR') {
            dataRows.push(c.ctr);
            //custom tooltip with html 
            dataRows.push('<p><b>' + c.durationInstance + '</b></p><p> CTR: <b>' + c.ctr.toFixed(2) + '%</b></p>');
            //custom tooltip without html 
           // dataRows.push(c.durationInstance + ' CTR: ' + c.ctr.toFixed(2) + '%');
          } else if (this.toggleStateMap.get(toggleElement) === 'CPC') {
            dataRows.push(c.avgCPC);
            //custom tooltip with html 
            dataRows.push('<p><b>' + c.durationInstance + '</b></p><p> CPC: <b>'+this.currencySymbol + c.avgCPC.toFixed(2) + '</b></p>');
            //custom tooltip without html 
            //dataRows.push(c.durationInstance + ' CPC: $' + c.avgCPC.toFixed(2));
          }
        }
      });

      data.addRow(dataRows);
    }

    const r = this.getSeriesAndVAxes();
    const options: any = {
      hAxis: { titleTextStyle: { color: '#333' } },
      vAxes: r.vAxes,
      series: r.series,
      chartArea: { 'width': '90%', 'height': '80%' },
      legend: { position: 'none'},
      width: 973,
      height: 350,
      //custom tooltip with html 
      tooltip: {isHtml: true}
    };

    const chart = new google.visualization.LineChart(document.getElementById('conversionResultLineChart'));
    const columns = [
      { title: 'Month', dataKey: 'month' },
      { title: 'Spends', dataKey: 'spends' },
      { title: 'CPM', dataKey: 'cpm' },
      { title: 'CTR', dataKey: 'ctr' },
      { title: 'CPC', dataKey: 'cpc' }
    ];
    for (const c of chartData) {
      const row: any = {
        month: c.durationInstance,
        spends: c.totalSpends,
        cpm: c.avgCPM,
        ctr: c.ctr,
        cpc: c.avgCPC
      };
      pdfTableRows.push(row);
    }
    this.pushToPDF(chart, 'Result', {columns: columns, rows: pdfTableRows});
    chart.draw(data, options);
  }

  drawOverviewTopAssetsScatterChart(overviewTopAssets: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('number', 'Total Spends');
    data.addColumn('number', 'Reach');
    data.addColumn({type: 'string', role: 'tooltip'});

    for (const r of overviewTopAssets) {
      if (r.assetType === 'Banner') {
        data.addRow([r.reach, r.totalSpends, r.subcampaignName]);
      }
      if (r.assetType === 'Video') {
        data.addRow([r.reach, r.totalSpends, r.subcampaignName]);
      }
    }

    const options: any = {
      legend: 'none',
      colors: ['A569D6', '6646ED', 'ECB057'],
      chartArea: { 'width': '90%', 'height': '70%' },
      width: 973
    };

    const chart = new google.visualization.ScatterChart(document.getElementById('campaignTopCampaignsScatterChart'));
    const columns = [
      { title: 'Subcampaign Name', dataKey: 'subcampaignName' },
      { title: 'Reach', dataKey: 'reach' },
      { title: 'Spend', dataKey: 'spend' }
    ];

    const rows: any[] = [];
    for (const r of overviewTopAssets) {
      if (r.assetType === 'Banner') {
        const row: any = { subcampaignName: r.subcampaignName, reach: r.reach, spend: r.totalSpends};
        rows.push(row);
      }
    }
    this.pushToPDF(chart, 'Top Campaigns', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawOverviewAdTypeColumnChart(overviewAdType: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Numbers');

    for (const c of overviewAdType) {
      data.addRow([c.durationInstance, c.bannerAds]);
    }

    const options: any = {
      legend: { position: 'bottom', alignment: 'end'},
      bar: { groupWidth: '20%' },
      colors: ['A569D6', '5FBDEA', 'ECB057'],
      chartArea: { 'width': '90%', 'height': '70%' },
      sizeAxis: { maxSize: 7, minSize: 7},
      width: 973,
      hAxis: {
        slantedText: false,
      }
    };

    const chart = new google.visualization.ColumnChart(
      document.getElementById('campaignsActiveCampaignsBarChart'));
    const columns = [
      { title: 'Asset Type', dataKey: 'assetType' },
      { title: 'Month', dataKey: 'month' },
      { title: 'Numbers', dataKey: 'count' }
    ];

    const rows: any[] = [];
    for (const c of overviewAdType) {
      const row: any = { assetType: 'Banner', month: c.durationInstance, count: c.bannerAds };
      rows.push(row);
    }
    this.pushToPDF(chart, 'Active Campaigns', {columns: columns, rows: rows});
    chart.draw(data, options);
  }
  public clearFilter(activeTab) {
    this.isApply = true;
    this.enableCampaignsTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.saveFilterForm.get('duration').setValue('Custom Duration');
    this.onSelectDuration('Custom Duration');
    this.selectedGender = [];
    this.selectedLocation = [];
    this.selectedSpecialization = [];
    this.getAnalyticsData(activeTab);
  }
  public applyFilter(activeTab) {
    this.isApply = true;
    this.enableCampaignsTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.getAnalyticsData(activeTab);
  }
  public changeTab(activeTab) {
    this.hideExtraGraph = true;
    this.activeTab = activeTab;
    this.analyticsGraphsDataTemp = {
      campaignsActiveCampaigns: [],
      campaignsTopCampaigns: [],
      audienceGenderExperience: [],
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceFrequency: [],
      behaviorTimeOfDayEnagement: [],
      behaviorDisplayLocation: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResult: null
    };
    setTimeout(() => {
      this.analyticsGraphsDataTemp = this.analyticsGraphsData;
      // audienceTab
      if (activeTab === 'audienceTab') {
        // this.spinner.showSpinner.next(true);
        if (this.checkNotNull(this.analyticsGraphsData.audienceSpecialization)) {
          this.drawAudienceSpecializationStackedBar(this.analyticsGraphsData.audienceSpecialization);
        } else {
          this.resetGraph('specializationStackedBarChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.audienceArcheType)) {
          this.drawAudienceHCPArcheTypeBarChart(this.analyticsGraphsData.audienceArcheType);
        } else {
          this.resetGraph('drawAudienceHCPArcheTypeBarChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.audienceLocation)) {
          this.drawLocationGraph(this.analyticsGraphsData.audienceLocation);
        } else {
          this.resetGraph('audienceLocationChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.audienceFrequency)) {
          this.drawAudienceFrequencyPieChart(this.analyticsGraphsData.audienceFrequency);
        } else {
          this.resetGraph('audienceFrequencyPieChart');
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableAudienceTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }
      // behaviorTab
      if (activeTab === 'behaviorTab') {
        // this.spinner.showSpinner.next(true);
        if (this.checkNotNull(this.analyticsGraphsData.behaviorDisplayLocation)) {
          this.drawBehaviorDisplayLocationStackedBarChart(this.analyticsGraphsData.behaviorDisplayLocation);
        } else {
          this.resetGraph('behaviorDisplayLocationStackedBarChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.behaviorDevices)) {
          this.drawBehaviorDevicesDonutChart(this.analyticsGraphsData.behaviorDevices);
        } else {
          this.resetGraph('behaviorDevicesDonutChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.behaviorTimeOfDayEnagement)) {
          this.drawBehaviourHeatMap(this.analyticsGraphsData.behaviorTimeOfDayEnagement);
        } else {
          this.resetHeatMap();
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableBehaviorTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }
      // conversionTab
      if (activeTab === 'conversionTab') {
        // this.spinner.showSpinner.next(true);
        if (this.checkNotNull(this.analyticsGraphsData.conversionEnagement)) {
          this.drawConversionEngagementAreaChart(this.analyticsGraphsData.conversionEnagement);
        } else {
          this.resetGraph('conversionEngagementAreaChart');
          this.noDataForConversionResult = true;
        }
        if (this.checkNotNull(this.analyticsGraphsData.conversionResult)) {
          this.conversionResultData = this.analyticsGraphsData.conversionResult;
          this.drawConversionResultLineChart(this.analyticsGraphsData.conversionResult);
        } else {
          this.resetGraph('conversionResultLineChart');
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableConversionTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }
      // campaignsTab
      if (activeTab === 'campaignsTab') {
        // this.spinner.showSpinner.next(true);
        if (this.subCampaignId === undefined || this.subCampaignId === '' || this.subCampaignId === null) {
          if (this.checkNotNull(this.analyticsGraphsData.campaignsTopCampaigns)) {
            this.drawOverviewTopAssetsScatterChart(this.analyticsGraphsData.campaignsTopCampaigns);
          } else {
            this.resetGraph('campaignTopCampaignsScatterChart');
          }

          if (this.checkNotNull(this.analyticsGraphsData.campaignsActiveCampaigns)) {
            this.drawOverviewAdTypeColumnChart(this.analyticsGraphsData.campaignsActiveCampaigns);
          } else {
            this.resetGraph('campaignsActiveCampaignsBarChart');
          }
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableCampaignsTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }
      if (this.ddTreeLocation) {
        this.ddTreeLocation.onSelect(this.persistSelectedLocation);
        this.ddTreeLocation.updateItems(this.allLocationTreeItems, this.persistSelectedLocation);
      }

      if (this.ddTreeSpecialization) {
        this.ddTreeSpecialization.onSelect(this.persistSelectedSpecialization);
        this.ddTreeSpecialization.updateItems(this.allSpecializationTreeItems, this.persistSelectedSpecialization);
      }

      if (this.ddTreeGender) {
        this.ddTreeGender.onSelect(this.persistSelectedGender);
        this.ddTreeGender.updateItems(this.allGenderTreeItems, this.persistSelectedGender);
      }
      this.hideExtraGraph = false;
    }, 100);
  }
  public getAnalyticsData(activeTab) {
    this.hideExtraGraph = true;
    google.charts.load('current', { packages: ['corechart', 'table'], mapsApiKey: config['AGM']['KEY'] });
    this.saveFilterForm.get('advertiserId').setValue(this.advertiserId);
    this.saveFilterForm.get('brandId').setValue(this.brandId);
    this.saveFilterForm.get('campaignId').setValue(this.campaignId);
    this.saveFilterForm.get('subCampaignId').setValue(this.subCampaignId);
    if (!this.isApply) {
      this.saveFilterForm.get('startDate').setValue(new Date(this.brandCreateDate).toString());
      this.saveFilterForm.get('endDate').setValue(new Date(this.todayDate).toString());
    } else {
      let startDate = new Date(this.saveFilterForm.get('startDate').value);
      let endDate = new Date(this.saveFilterForm.get('endDate').value);
      if (this.saveFilterForm.get('duration').value === 'Custom Duration') {
        startDate = new Date(new Date(this.saveFilterForm.get('startDate').value).setHours(0o0, 0o0, 0o0));
        endDate = new Date(new Date(this.saveFilterForm.get('endDate').value).setHours(23, 59, 59));
      }
      this.saveFilterForm.get('startDate').setValue(new Date(startDate).toString());
      this.saveFilterForm.get('endDate').setValue(new Date(endDate).toString());
    }
    this.saveFilterForm.get('timezone').setValue(new Date().getTimezoneOffset());
    this.unwindFilter();

    if (this.saveFilterForm.get('startDate').value === 'Invalid Date') {
      this.saveFilterForm.get('startDate').setValue(new Date());
    }

    if (this.saveFilterForm.get('endDate').value === 'Invalid Date') {
      this.saveFilterForm.get('endDate').setValue(new Date());
    }
    // audienceTab
    this.saveFilterForm.get('tabType').setValue('audienceTab');
    this.advertiserAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
      response => {
        this.analyticsGraphsData.audienceSpecialization = response.audienceSpecialization;
        this.analyticsGraphsDataTemp.audienceSpecialization = this.analyticsGraphsData.audienceSpecialization;

        this.analyticsGraphsData.audienceLocation = response.audienceLocation;
        this.analyticsGraphsDataTemp.audienceLocation = this.analyticsGraphsData.audienceLocation;

        this.analyticsGraphsData.audienceArcheType = response.audienceArcheType;
        this.analyticsGraphsDataTemp.audienceArcheType = this.analyticsGraphsData.audienceArcheType;

        this.analyticsGraphsData.audienceFrequency = response.audienceFrequency;
        this.analyticsGraphsDataTemp.audienceFrequency = this.analyticsGraphsData.audienceFrequency;

        this.startAnalyticsDate = response.startDate;
        this.endAnalyticsDate = response.endDate;
        if (!this.isApply) {
          this.saveFilterForm.get('startDate').setValue(new Date(this.brandCreateDate));
          this.saveFilterForm.get('endDate').setValue(this.todayDate);
        } else {
          this.saveFilterForm.get('startDate').setValue(new Date(this.saveFilterForm.get('startDate').value));
          this.saveFilterForm.get('endDate').setValue(new Date(this.saveFilterForm.get('endDate').value));
        }
        if (this.loadGraphs) {
          if (this.checkNotNull(response.audienceSpecialization)) {
            this.drawAudienceSpecializationStackedBar(response.audienceSpecialization);
          } else {
            this.resetGraph('specializationStackedBarChart');
          }
          if (this.checkNotNull(response.audienceArcheType)) {
            this.drawAudienceHCPArcheTypeBarChart(response.audienceArcheType);
          } else {
            this.resetGraph('drawAudienceHCPArcheTypeBarChart');
          }
          if (this.checkNotNull(response.audienceLocation)) {
            this.drawLocationGraph(response.audienceLocation);
          } else {
            this.resetGraph('audienceLocationChart');
          }
          if (this.checkNotNull(response.audienceFrequency)) {
            this.drawAudienceFrequencyPieChart(response.audienceFrequency);
          } else {
            this.resetGraph('audienceFrequencyPieChart');
          }
          this.enableAudienceTab = true;
          if (this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab && this.enableCampaignsTab) {
            this.hideExtraGraph = false;
            this.activeTab = activeTab;
          }
        }
    },
    err => {

    });
    // behaviorTab
    this.saveFilterForm.get('tabType').setValue('behaviorTab');
    this.advertiserAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
    response => {
      this.analyticsGraphsData.behaviorDisplayLocation = response.behaviorDisplayLocation;
      this.analyticsGraphsDataTemp.behaviorDisplayLocation = this.analyticsGraphsData.behaviorDisplayLocation;

      this.analyticsGraphsData.behaviorDevices = response.behaviorDevices;
      this.analyticsGraphsDataTemp.behaviorDevices = this.analyticsGraphsData.behaviorDevices;

      this.analyticsGraphsData.behaviorTimeOfDayEnagement = response.behaviorTimeOfDayEnagement;
      this.analyticsGraphsDataTemp.behaviorTimeOfDayEnagement = this.analyticsGraphsData.behaviorTimeOfDayEnagement;

      if (this.loadGraphs) {
        if (this.checkNotNull(response.behaviorDisplayLocation)) {
          this.drawBehaviorDisplayLocationStackedBarChart(response.behaviorDisplayLocation);
        } else {
          this.resetGraph('behaviorDisplayLocationStackedBarChart');
        }
        if (this.checkNotNull(response.behaviorDevices)) {
          this.drawBehaviorDevicesDonutChart(response.behaviorDevices);
        } else {
          this.resetGraph('behaviorDevicesDonutChart');
        }
        if (this.checkNotNull(response.behaviorTimeOfDayEnagement)) {
          this.drawBehaviourHeatMap(response.behaviorTimeOfDayEnagement);
        } else {
          this.resetHeatMap();
        }
        this.enableBehaviorTab = true;
        if (this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab && this.enableCampaignsTab) {
          this.hideExtraGraph = false;
          this.activeTab = activeTab;
        }
      }
    },
    err => {

    });
    // conversionTab
    this.saveFilterForm.get('tabType').setValue('conversionTab');
    this.advertiserAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
    response => {
      this.analyticsGraphsData.conversionEnagement = response.conversionEnagement;
      this.analyticsGraphsDataTemp.conversionEnagement = this.analyticsGraphsData.conversionEnagement;

      this.analyticsGraphsData.conversionResult = response.conversionResult;
      this.analyticsGraphsDataTemp.conversionResult = this.analyticsGraphsData.conversionResult;

      if (this.loadGraphs) {
          if (this.checkNotNull(response.conversionEnagement)) {
            this.drawConversionEngagementAreaChart(response.conversionEnagement);
          } else {
            this.resetGraph('conversionEngagementAreaChart');
            this.noDataForConversionResult = true;
          }
          if (this.checkNotNull(response.conversionResult)) {
            this.conversionResultData = response.conversionResult;
            this.drawConversionResultLineChart(response.conversionResult);
          } else {
            this.resetGraph('conversionResultLineChart');
          }
          this.enableConversionTab = true;
          if (this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab && this.enableCampaignsTab) {
            this.hideExtraGraph = false;
            this.activeTab = activeTab;
          }
      }
    },
    err => {

    });
    // campaignsTab
    this.saveFilterForm.get('tabType').setValue('campaignsTab');
    this.advertiserAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
    response => {
      this.analyticsGraphsData.campaignsTopCampaigns = response.campaignsTopCampaigns;
      this.analyticsGraphsDataTemp.campaignsTopCampaigns = this.analyticsGraphsData.campaignsTopCampaigns;

      this.analyticsGraphsData.campaignsActiveCampaigns = response.campaignsActiveCampaigns;
      this.analyticsGraphsDataTemp.campaignsActiveCampaigns = this.analyticsGraphsData.campaignsActiveCampaigns;
      if (this.loadGraphs) {
          if (this.subCampaignId === undefined || this.subCampaignId === '' || this.subCampaignId === null) {
            if (this.checkNotNull(response.campaignsTopCampaigns)) {
              this.drawOverviewTopAssetsScatterChart(response.campaignsTopCampaigns);
            } else {
              this.resetGraph('campaignTopCampaignsScatterChart');
            }

            if (this.checkNotNull(response.campaignsActiveCampaigns)) {
              this.drawOverviewAdTypeColumnChart(response.campaignsActiveCampaigns);
            } else {
              this.resetGraph('campaignsActiveCampaignsBarChart');
            }
          }
          this.enableCampaignsTab = true;
          if (this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab && this.enableCampaignsTab) {
            this.hideExtraGraph = false;
            this.activeTab = activeTab;
          }
        }
    },
    err => {

    });

    if (this.ddTreeLocation) {
      this.ddTreeLocation.onSelect(this.persistSelectedLocation);
      this.ddTreeLocation.updateItems(this.allLocationTreeItems, this.persistSelectedLocation);
    }

    if (this.ddTreeSpecialization) {
      this.ddTreeSpecialization.onSelect(this.persistSelectedSpecialization);
      this.ddTreeSpecialization.updateItems(this.allSpecializationTreeItems, this.persistSelectedSpecialization);
    }

    if (this.ddTreeGender) {
      this.ddTreeGender.onSelect(this.persistSelectedGender);
      this.ddTreeGender.updateItems(this.allGenderTreeItems, this.persistSelectedGender);
    }
    this.advertiserAnalyticsService.getAnalyticsCardsData(this.saveFilterForm.value).subscribe(
      response => {
        this.cardsData = response;
    },
    err => {

    });
  }

  checkNotNull(data: any) {
    if (data instanceof Array) {
      if (data.length > 0) {
        return true;
      }
      return false;
    } else if (data instanceof Object) {
      if (data && (Object.keys(data).length !== 0)) {
        return true;
      }
      return false;
    } else if (typeof data === 'string' || data instanceof String) {
      if (data.length !== 0) {
        return true;
      }
      return false;
    }
  }

  resetGraph(elementName: string) {
    const node = document.getElementById(elementName);
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
      const a = document.createElement('span');
      // a.setAttribute('class', 'noChartData');
      if (elementName === 'campaignTopCampaignsScatterChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-campaign-top-campaign.jpg" alt="loading" style="width:973px; height:300px; margin:-100px -150 -50 -80px"><p style="position: relative; top:-181px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'campaignsActiveCampaignsBarChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-campaign-active-campaign.jpg" alt="loading" style="width:973px; height:300px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-181px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'specializationStackedBarChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-audience-specialization.jpg" alt="loading" style="width:973px; height:300px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-162px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'drawAudienceHCPArcheTypeBarChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-audience-hcparchetype.jpg" alt="loading" style="width:540px; height:300px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-171px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'audienceLocationChart') {
        if(this.zone === '1') {
          a.innerHTML =
          '<img src="../assets/images/graph/adv-audience-location.jpg" alt="loading" style="width:973px; height:300px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-171px;width: 400px; margin: auto !important;text-align: center; left:167px">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
        } else if(this.zone === '2') {
          a.innerHTML =
          '<img src="../assets/images/graph/adv-audience-location_ind.jpg" alt="loading" style="width:973px; height:300px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-171px;width: 400px; margin: auto !important;text-align: center; left:167px">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
        }
        
      } else if (elementName === 'audienceFrequencyPieChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-audience-frequency.jpg" alt="loading" style="width:375px; height:275px;; margin:-100px -150 -50 -80px"> <p style="position: relative;top:-159px;width:270px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'behaviorDisplayLocationStackedBarChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-behavior-display-location.jpg" alt="loading" style="width:750px; height:300px;; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-184px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'behaviorDevicesDonutChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-behavior-devices.jpg" alt="loading" style="width:370px; height:275px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-144px;width:331px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'conversionEngagementAreaChart') {
        a.innerHTML =
        // tslint:disable-next-line:max-line-length
        '<img src="../assets/images/graph/adv-conversion-engagement.jpg" alt="loading" style="width:973px; height:350px; margin:-100px -150 -50 -80px"> <p style="position: relative;top:-198px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else if (elementName === 'conversionResultLineChart') {
        a.innerHTML =
        '<img src="../assets/images/graph/adv-conversion-result.jpg" alt="loading" style="width:973px; height:350px; margin:-100px -150 -50 -80px;"> <p style="position: relative; top:-222px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
      } else {
        a.innerHTML = '<p class="noChartData tac">Not enough data to plot graph</p>';
      }
      document.getElementById(elementName).appendChild(a);
    }
  }

  resetGraphs() {
    this.resetGraph('specializationStackedBarChart');
    this.resetGraph('locationChart');
    this.resetGraph('drawAudienceHCPArcheTypeBarChart');
    this.resetGraph('audienceFrequencyPieChart');
    this.resetGraph('behaviorDevicesDonutChart');
    this.resetHeatMap();
    this.resetGraph('behaviorDisplayLocationStackedBarChart');
    this.resetGraph('conversionEngagementAreaChart');
    this.resetGraph('conversionResultLineChart');
    this.resetGraph('campaignTopCampaignsScatterChart');
    this.resetGraph('campaignsActiveCampaignsBarChart');
    this.resetPdfNode();
  }

  resetHeatMap() {
    const el: HTMLElement = document.getElementById('heatmap');
    if (el !== undefined && el !== null) {
      el.innerHTML =
      '<img src="../assets/images/graph/adv-behavior-heat.jpg" alt="loading" style="width:540px; height:300px; margin:-100px -150 -50 -80px"> <p style="position: relative; top:-162px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>';
    }
  }

  resetPdfNode() {
    const node = document.getElementById('chart-n-data');
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
    }
  }

  private pushToPDF(chart: any, chartName: string, tableData: any) {
    // Remove duplicate charts.
    const parent = document.getElementById('chart-n-data');
    const children = document.getElementById('chart-n-data').childNodes;
    for (let i = 0; i < children.length; i++) {
      if (children[i]['id'] === chartName) {
        parent.removeChild(children[i]);
      }
    }
    for (let i = 0; i < children.length; i++) {
      if (children[i]['id'] === chartName) {
        parent.removeChild(children[i]);
      }
    }
    // end
    google.visualization.events.addListener(chart, 'ready', function() {
      const chartContent = document.createElement('img');
      chartContent.src = chart.getImageURI();
      chartContent.setAttribute('name', chartName);
      chartContent.setAttribute('id', chartName);

      const tableContent = document.createElement('input');
      tableContent.setAttribute('type', 'text');
      tableContent.setAttribute('name', chartName);
      tableContent.setAttribute('id', chartName);
      tableContent.setAttribute('value', JSON.stringify(tableData));
      document.getElementById('chart-n-data').appendChild(tableContent);
      document.getElementById('chart-n-data').appendChild(chartContent);
    });
  }
  public pdfBuilder() {

    const jsPDF = require('jspdf');
    require('jspdf-autotable');

    let pdfBrandName: string;
    let pdfCampaignName: string;
    let pdfSubCampaignName: string;
    let locationPdfNames: string;
    let specializationPdfNames: string;
    let genderPdfNames: string;
    pdfBrandName = this.currentlyViewedBrand.brandName;
    if (this.campaignId) {
      pdfCampaignName = this.campaignsList.find(camp => camp._id === this.campaignId).name;
    } else {
      pdfCampaignName = '-';
    }
    if (this.subCampaignId) {
      pdfSubCampaignName = this.subCampaignsList.find(subCamp => subCamp._id === this.subCampaignId).name;
    } else {
      pdfSubCampaignName = '-';
    }
    locationPdfNames = '';
    if (this.selectedLocation.length) {
      let length = 0;
      this.selectedLocation.forEach((element, index) => {
        if (length < 3) {
          const findedLocation = this.locationListForPdf.find(obj => obj.zipcode === element);
          if (findedLocation) {
            length++;
            locationPdfNames = `${locationPdfNames} ${findedLocation.city}, `;
          }
        }
      });
      locationPdfNames = locationPdfNames.trim().slice(0, -1);
      if (this.selectedLocation.length > 3) {
        locationPdfNames = `${locationPdfNames}, ${this.selectedLocation.length - 3} More...` ;
      }
    } else {
      locationPdfNames = '-';
    }
    specializationPdfNames = '';
    if (this.selectedSpecialization.length) {
      let previousName = '';
      let length = 0;
      this.selectedSpecialization.forEach((element, index) => {
        if (length < 3) {
          const findedSpecialization = this.specializationListForPdf.find(obj => obj.Taxonomy === element);
          if (findedSpecialization && previousName !== findedSpecialization.Classification) {
            length++;
            previousName = findedSpecialization.Classification;
            specializationPdfNames = `${specializationPdfNames} ${findedSpecialization.Classification}, `;
          }
        }
      });
      specializationPdfNames = specializationPdfNames.trim().slice(0, -1);
      if (this.selectedSpecialization.length > 3) {
        specializationPdfNames = `${specializationPdfNames}, ${this.selectedSpecialization.length - 3} More...` ;
      }
    } else {
      specializationPdfNames = '-';
    }
    genderPdfNames = '';
    if (this.selectedGender.length) {
      this.selectedGender.forEach((element) => {
        if (element === 'M') {
          genderPdfNames = `${genderPdfNames} Male, `;
        } else if (element === 'F') {
          genderPdfNames = `${genderPdfNames} Female, `;
        }
      });
      genderPdfNames = genderPdfNames.trim().slice(0, -1);
    } else {
      genderPdfNames = '-';
    }
    const doc = new jsPDF('p', 'pt', 'a4', 0);
    doc.setFillColor(242, 242, 242);
    doc.rect(0, 0, 600, 1000, 'F');
     // HEADER
    let headerBarImage;
    let docereeLogo;
    headerBarImage = document.getElementById('analytics_header');
    docereeLogo = document.getElementById('doceree_logo');
    doc.addImage(docereeLogo, 'png', 30, 25, 85, 22);
    doc.setFontSize(8);
    doc.addImage(headerBarImage, 'png', 15, 60, 565, 20);
    doc.setTextColor(255, 255, 255);
    doc.text(`${
      new Date(this.startAnalyticsDate).toDateString()} - ${new Date(this.endAnalyticsDate).toDateString()}`, 440, 73);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.text(`${this.userDetails.first_name} ${this.userDetails.last_name}`, 30, 73);

    doc.setFontType('normal');
    doc.setFontSize(7);
    doc.setTextColor(0, 0, 0);

    doc.setFillColor(255, 255, 255);
    doc.rect(15, 81, 565, 55, 'F');

    doc.text('Brand Name', 25, 95);
    doc.text(pdfBrandName, 100, 95);

    doc.text('Campaign Name', 25, 112);
    doc.text(pdfCampaignName, 100, 112);

    doc.text('Sub Campaign Name', 25, 128);
    doc.text(pdfSubCampaignName, 100, 128);

    doc.setDrawColor(0, 0, 0);
    doc.line(230, 130, 230, 85);

    doc.text('Location', 250, 95);
    doc.text(locationPdfNames, 300, 95);

    doc.text('Specialization', 250, 112);
    doc.text(specializationPdfNames, 300, 112);

    doc.text('Gender', 250, 128);
    doc.text(genderPdfNames, 300, 128);

    doc.setFontSize(9);
    const images = document.getElementById('chart-n-data');
    const tagsTemp = images.getElementsByTagName('img');
    const tableDataArrayTemp = images.getElementsByTagName('input');

    let tags: any;
    tags = [];
    let tableDataArray: any;
    tableDataArray = [];
    let topAdAssetsHeight: number;
    let topAdTypeHeight: number;
    let audienceArchetypeHeight: number;
    let audienceLocationHeight: number;
    let audienceSpecializationHeight: number;
    let audienceFrequencyHeight: number;
    let displayLocationHeight: number;
    let deviceBehaviorHeight: number;
    let engagementConversionHeight: number;
    let resultConversionHeight: number;
    for (let i = 0; i < tagsTemp.length; i++) {
      if (tagsTemp[i]['id'] === 'Top Campaigns') {
        tags[7] = tagsTemp[i];
        tableDataArray[7] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Active Campaigns') {
        tags[8] = tagsTemp[i];
        tableDataArray[8] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Specializations') {
        tags[0] = tagsTemp[i];
        tableDataArray[0] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Location') {
        tags[9] = tagsTemp[i];
        tableDataArray[9] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'HCP Archetype') {
        tags[1] = tagsTemp[i];
        tableDataArray[1] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Frequency') {
        tags[2] = tagsTemp[i];
        tableDataArray[2] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Display Location') {
        tags[3] = tagsTemp[i];
        tableDataArray[3] = tableDataArrayTemp[i];
      }  else if (tagsTemp[i]['id'] === 'Device') {
        tags[4] = tagsTemp[i];
        tableDataArray[4] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Engagement') {
        tags[5] = tagsTemp[i];
        tableDataArray[5] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Result') {
        tags[6] = tagsTemp[i];
        tableDataArray[6] = tableDataArrayTemp[i];
      }
    }
    if (this.subCampaignId) {
      audienceSpecializationHeight = 171;
      audienceLocationHeight = 319;
      audienceArchetypeHeight = 467;
      audienceFrequencyHeight = 467;
      displayLocationHeight = 655;
      deviceBehaviorHeight = 655;
      engagementConversionHeight = 100;
      resultConversionHeight = 263;
    } else {
      topAdAssetsHeight = 171;
      topAdTypeHeight = 319;
      audienceSpecializationHeight = 500;
      audienceLocationHeight = 648;
      audienceArchetypeHeight = 100;
      audienceFrequencyHeight = 100;
      displayLocationHeight = 290;
      deviceBehaviorHeight = 290;
      engagementConversionHeight = 480;
      resultConversionHeight = 647;
    }
    let tableData;
    let title_name;
    let tableRows: any;
    let count: any;
    if (!this.subCampaignId) {
      // Campaign tab starts.
      doc.addImage(headerBarImage, 'png', 15, topAdAssetsHeight - 21, 75, 20);
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.setTextColor(255, 255, 255);
      doc.text('Campaigns', 25, topAdAssetsHeight - 8);

      doc.setFontSize(9);
      doc.setFontType('normal');
      doc.setTextColor(0, 0, 0);
      // Top Campaigns starts.
      tableData = JSON.parse(tableDataArray[7].getAttribute('value'));
      tableRows = [];
      count = 0;
      tableData.rows.forEach(element => {
        if ((element.reach > 0 || element.spend > 0) && count < 10) {
          count++;
          element.spend = element.spend.toFixed(2);
          tableRows.push(element);
        }
      });
      title_name = tags[7].getAttribute('name');
      doc.setFillColor(255, 255, 255);
      doc.rect(14.5, topAdAssetsHeight, 400, 145, 'F');

      doc.text(title_name, 30, topAdAssetsHeight + 15);
      doc.addImage(tags[7], 'png', 40, topAdAssetsHeight + 20, 374, 120);
      doc.setFillColor(255, 255, 255);
      doc.rect(417, topAdAssetsHeight, 163, 145, 'F');
      doc.autoTable(tableData.columns, tableRows, {
        tableWidth: 153,
        startY: topAdAssetsHeight + 15,
        margin: { horizontal: 422},
        styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
        bodyStyles: { valign: 'top'},
        theme: 'striped',
        headStyles: { fillColor: '#fff', textColor: '#000' }
      });
      // Top Campaigns ends.

      // Active Campaigns starts
      tableData = JSON.parse(tableDataArray[8].getAttribute('value'));
      tableRows = [];
      count = 0;
      tableData.rows.forEach(element => {
        if (element.count > 0 && count < 10) {
          count++;
          tableRows.push(element);
        }
      });
      title_name = tags[8].getAttribute('name');

      doc.setFillColor(255, 255, 255);
      doc.rect(14.5, topAdTypeHeight, 400, 145, 'F');

      doc.text(title_name, 30, topAdTypeHeight + 15);
      doc.addImage(tags[8], 'png', 40, topAdTypeHeight + 20, 374, 120);
      doc.setFillColor(255, 255, 255);
      doc.rect(417, topAdTypeHeight, 163, 145, 'F');
      doc.autoTable(tableData.columns, tableRows, {
        tableWidth: 153,
        startY: topAdTypeHeight + 15,
        margin: { horizontal: 422},
        styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
        bodyStyles: { valign: 'top'},
        theme: 'striped',
        headStyles: { fillColor: '#fff', textColor: '#000' }
      });
      // Active Campaigns ends.
      // Campaign tab ends.
    }
    // Audience tab starts.
    doc.addImage(headerBarImage, 'png', 15, audienceSpecializationHeight - 21, 75, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Audience', 25, audienceSpecializationHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);

    // Specializations starts.
    tableData = JSON.parse(tableDataArray[0].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if ((element.female || element.male || element.other) > 0 && count < 10) {
        count++;
        tableRows.push(element);
      }
    });
    title_name = tags[0].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, audienceSpecializationHeight, 400, 145, 'F');

    doc.text(title_name, 30, audienceSpecializationHeight + 15);
    doc.addImage(tags[0], 'png', 40, audienceSpecializationHeight + 20, 374, 120);

    doc.setFillColor(255, 255, 255);
    doc.rect(417, audienceSpecializationHeight, 163, 145, 'F');

    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 153,
      startY: audienceSpecializationHeight + 15,
      margin: { horizontal: 422},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Specializations ends.

    // Location starts.
    tableData = JSON.parse(tableDataArray[9].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if (element.popularity > 0 && count < 10) {
        count++;
        element.popularity = element.popularity.toFixed(2) + '%';
        tableRows.push(element);
      }
    });
    title_name = tags[9].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, audienceLocationHeight, 400, 145, 'F');
    doc.text(title_name, 30, audienceLocationHeight + 15);
    doc.addImage(tags[9], 'png', 40, audienceLocationHeight + 20, 300, 120);
    doc.setFillColor(255, 255, 255);
    doc.rect(417, audienceLocationHeight, 163, 145, 'F');
    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 153,
      startY: audienceLocationHeight + 15,
      margin: { horizontal: 422},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Location ends.
    if (!this.subCampaignId) {
    // Audience tab ends.
      doc.text('1/2', 300, 830);
      doc.addPage();
      doc.setFillColor(242, 242, 242);
      doc.rect(0, 0, 600, 1000, 'F');
      doc.addImage(docereeLogo, 'png', 30, 25, 85, 22);
      doc.setFontSize(8);
      doc.text(`${
        new Date(this.startAnalyticsDate).toDateString()} - ${new Date(this.endAnalyticsDate).toDateString()}`, 440, 40);
      // Audience tab starts.
      doc.addImage(headerBarImage, 'png', 15, audienceArchetypeHeight - 21, 75, 20);
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.setTextColor(255, 255, 255);
      doc.text('Audience', 25, audienceArchetypeHeight - 8);

      doc.setFontSize(9);
      doc.setFontType('normal');
      doc.setTextColor(0, 0, 0);
    }
    // HCP Archetype starts.
    tableData = JSON.parse(tableDataArray[1].getAttribute('value'));
    title_name = tags[1].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, audienceArchetypeHeight, 231, 151, 'F');

    doc.text(title_name, 30, audienceArchetypeHeight + 15);
    doc.addImage(tags[1], 'png', 40, audienceArchetypeHeight + 20, 195, 130);

    doc.setFillColor(255, 255, 255);
    doc.rect(248, audienceArchetypeHeight, 84, 151, 'F');

    doc.autoTable(tableData.columns, tableData.rows, {
      tableWidth: 75,
      startY: audienceArchetypeHeight + 15,
      margin: { horizontal: 253},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // HCP Archetype ends.

    // Frequency starts.
    tableData = JSON.parse(tableDataArray[2].getAttribute('value'));
    title_name = tags[2].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(336, audienceFrequencyHeight, 162, 151, 'F');
    doc.text(title_name, 355, audienceFrequencyHeight + 15);
    doc.addImage(tags[2], 'png', 337, audienceFrequencyHeight + 20, 159, 120);
    doc.setFillColor(255, 255, 255);
    doc.rect(501, audienceFrequencyHeight, 79, 151, 'F');
    doc.autoTable(tableData.columns, tableData.rows, {
      tableWidth: 71,
      startY: audienceFrequencyHeight + 15,
      margin: { horizontal: 505},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Frequency ends.
    // Audience tab ends.
    // Behavior tab starts.
    doc.addImage(headerBarImage, 'png', 15, displayLocationHeight - 21, 75, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Behavior', 25, displayLocationHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);
    // Display Location starts
    tableData = JSON.parse(tableDataArray[3].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if (element.count > 0 && count < 10) {
        count++;
        tableRows.push(element);
      }
    });
    title_name = tags[3].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, displayLocationHeight, 250, 150, 'F');

    doc.text('HCP Archetype', 30, displayLocationHeight + 15);
    doc.addImage(tags[3], 'png', 40, displayLocationHeight + 20, 200, 125);

    doc.setFontSize(5);

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, displayLocationHeight + 135, 250, 20, 'F');

    doc.setFillColor(this.behaviorLegend[0].color);
    doc.circle(50, displayLocationHeight + 140, 2, 'FD');
    doc.text(this.behaviorLegend[0].labelName, 53, displayLocationHeight + 142);

    doc.setFillColor(this.behaviorLegend[1].color);
    doc.circle(85, displayLocationHeight + 140, 2, 'FD');
    doc.text(this.behaviorLegend[1].labelName, 88, displayLocationHeight + 142);

    doc.setFillColor(this.behaviorLegend[2].color);
    doc.circle(125, displayLocationHeight + 140, 2, 'FD');
    doc.text(this.behaviorLegend[2].labelName, 128, displayLocationHeight + 142);

    doc.setFillColor(this.behaviorLegend[3].color);
    doc.circle(150, displayLocationHeight + 140, 2, 'FD');
    doc.text(this.behaviorLegend[3].labelName, 153, displayLocationHeight + 142);

    doc.setFillColor(this.behaviorLegend[4].color);
    doc.circle(190, displayLocationHeight + 140, 2, 'FD');
    doc.text(this.behaviorLegend[4].labelName, 193, displayLocationHeight + 142);

    doc.setFillColor(this.behaviorLegend[5].color);
    doc.circle(30, displayLocationHeight + 148, 2, 'FD');
    doc.text(this.behaviorLegend[5].labelName, 33, displayLocationHeight + 150);

    doc.setFillColor(this.behaviorLegend[6].color);
    doc.circle(75, displayLocationHeight + 148, 2, 'FD');
    doc.text(this.behaviorLegend[6].labelName, 78, displayLocationHeight + 150);

    doc.setFillColor(this.behaviorLegend[7].color);
    doc.circle(110, displayLocationHeight + 148, 2, 'FD');
    doc.text(this.behaviorLegend[7].labelName, 113, displayLocationHeight + 150);

    doc.setFillColor(this.behaviorLegend[8].color);
    doc.circle(160, displayLocationHeight + 148, 2, 'FD');
    doc.text(this.behaviorLegend[8].labelName, 163, displayLocationHeight + 150);

    doc.setFillColor(this.behaviorLegend[9].color);
    doc.circle(200, displayLocationHeight + 148, 2, 'FD');
    doc.text(this.behaviorLegend[9].labelName, 203, displayLocationHeight + 150);

    doc.setFontSize(9);

    doc.setFillColor(255, 255, 255);
    doc.rect(267, displayLocationHeight, 162, 155, 'F');
    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 153,
      startY: displayLocationHeight + 15,
      margin: { horizontal: 272},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Display Location ends

    // Device starts
    tableData = JSON.parse(tableDataArray[4].getAttribute('value'));
    title_name = tags[4].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(433, deviceBehaviorHeight, 147, 155, 'F');

    doc.text(title_name, 450, deviceBehaviorHeight + 15);
    doc.addImage(tags[4], 'png', 455, deviceBehaviorHeight + 20, 95, 75);
    doc.autoTable(tableData.columns, tableData.rows, {
      tableWidth: 118,
      startY: deviceBehaviorHeight + 100,
      margin: { horizontal: 448},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Device ends
    if (this.subCampaignId) {
      doc.text('1/2', 300, 830);
      doc.addPage();
      doc.setFillColor(242, 242, 242);
      doc.rect(0, 0, 600, 1000, 'F');
      doc.addImage(docereeLogo, 'png', 30, 25, 85, 22);
      doc.setFontSize(8);
      doc.text(`${
        new Date(this.startAnalyticsDate).toDateString()} - ${new Date(this.endAnalyticsDate).toDateString()}`, 440, 40);
    }
    // Conversion tab starts.
    doc.addImage(headerBarImage, 'png', 15, engagementConversionHeight - 21, 75, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Conversion', 25, engagementConversionHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);
    // Engagement starts
    tableData = JSON.parse(tableDataArray[5].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if ((element.click || element.impression || element.reach) > 0 && count < 10) {
        count++;
        tableRows.push(element);
      }
    });
    title_name = tags[5].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, engagementConversionHeight, 400, 165, 'F');

    doc.text(title_name, 30, engagementConversionHeight + 15);
    doc.addImage(tags[5], 'png', 40, engagementConversionHeight + 20, 374, 135);

    doc.setFontSize(5);
    doc.setFillColor('#ec736b');
    doc.circle(297, engagementConversionHeight + 160, 2, 'FD');
    doc.text('Impression', 300, engagementConversionHeight + 162);

    doc.setFillColor('#92d8f0');
    doc.circle(337, engagementConversionHeight + 160, 2, 'FD');
    doc.text('Reach', 340, engagementConversionHeight + 162);

    doc.setFillColor('#b4a0cd');
    doc.circle(367, engagementConversionHeight + 160, 2, 'FD');
    doc.text('Click', 370, engagementConversionHeight + 162);

    doc.setFontSize(9);
    doc.setFillColor('#000');

    doc.setFillColor(255, 255, 255);
    doc.rect(417, engagementConversionHeight, 163, 165, 'F');

    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 153,
      startY: engagementConversionHeight + 15,
      margin: { horizontal: 422},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Engagement ends.

    // Result starts
    tableData = JSON.parse(tableDataArray[6].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if ((element.cpc || element.cpm || element.ctr || element.spends) > 0 && count < 10) {
        count++;
        element.cpc = element.cpc.toFixed(2);
        element.cpm = element.cpm.toFixed(2);
        element.ctr = element.ctr.toFixed(2);
        element.spends = element.spends.toFixed(2);
        tableRows.push(element);
      }
    });
    title_name = tags[6].getAttribute('name');

    doc.setFillColor(255, 255, 255);
    doc.rect(14.5, resultConversionHeight, 400, 165, 'F');

    doc.text(title_name, 30, resultConversionHeight + 15);
    doc.addImage(tags[6], 'png', 40, resultConversionHeight + 20, 374, 135);

    doc.setFontSize(5);

    doc.setFillColor('#5287c6');
    doc.circle(287, resultConversionHeight + 160, 2, 'FD');
    doc.text('Spends', 290, resultConversionHeight + 162);

    doc.setFillColor('#78c068');
    doc.circle(317, resultConversionHeight + 160, 2, 'FD');
    doc.text('CPM', 320, resultConversionHeight + 162);

    doc.setFillColor('#9a67ab');
    doc.circle(347, resultConversionHeight + 160, 2, 'FD');
    doc.text('CTR', 350, resultConversionHeight + 162);

    doc.setFillColor('#f37a6e');
    doc.circle(377, resultConversionHeight + 160, 2, 'FD');
    doc.text('CPC', 380, resultConversionHeight + 162);

    doc.setFontSize(9);
    doc.setFillColor('#000');

    doc.setFillColor(255, 255, 255);
    doc.rect(417, resultConversionHeight, 163, 165, 'F');
    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 153,
      startY: resultConversionHeight + 15,
      margin: { horizontal: 422},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000' }
    });
    // Result ends
    // Conversion tab ends.
    doc.text('2/2', 300, 830);
    doc.save('analytics' + '.pdf');
  }

  calculateAspectRatioFit(srcWidth: number, srcHeight: number, maxWidth: number, maxHeight: number) {
    const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return { width: srcWidth * ratio, height: srcHeight * ratio };
  }

  public twoToggle(toggleValue: any) {
    if (this.toggleStateMap.get(toggleValue) === undefined) {
      this.toggleStateMap.set(toggleValue, toggleValue);
      this.activeToggleArray.push(toggleValue);
      if (this.activeToggleArray.length > 2) {
        this['toggle' + this.activeToggleArray[0]] = undefined;
        this.toggleStateMap.set(this.activeToggleArray[0], undefined);
        this.activeToggleArray.shift();
      }
    } else if (this.toggleStateMap.get(toggleValue) === toggleValue) {
      this.toggleStateMap.set(toggleValue, undefined);
      this['toggle' + toggleValue] = undefined;
      this.activeToggleArray = this.activeToggleArray.filter(item => item !== toggleValue);
    }
    if (this.conversionResultData) {
      this.drawConversionResultLineChart(this.conversionResultData);
    }
  }

  private getSeriesAndVAxes(): any {

    const spendsColor = '#5287c6';
    const cpmColor = '#78c068';
    const cpcColor = '#f37a6e';
    const ctrColor = '#9a67ab';

    let series: any = {};
    let vAxes: any = {};

    let toggleValues = Array.from(this.toggleStateMap.values());
    toggleValues = toggleValues.filter(( element ) => {
      return element !== undefined;
    });

    if (toggleValues && toggleValues.length === 1) {
      if (toggleValues.includes('Spends')) {
        series = {
          0: { color: spendsColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CPM')) {
        series = {
          0: { color: cpmColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CPC')) {
        series = {
          0: { color: cpcColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CTR')) {
        series = {
          0: { color: ctrColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('#\'%\'', null);
      }
    }

    if (toggleValues && toggleValues.length === 2) {
      if (toggleValues.includes('Spends') && toggleValues.includes('CPM')) {
        if (toggleValues.indexOf('Spends') < toggleValues.indexOf('CPM')) {
          series = {
            0: { color: spendsColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: spendsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('Spends') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('Spends') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: spendsColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: spendsColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', this.currencySymbol+'#');
        }
      }

      if (toggleValues.includes('Spends') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('Spends') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: spendsColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: spendsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', this.currencySymbol+'#');
        }
      }

      if (toggleValues.includes('CPC') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPC') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', this.currencySymbol+'#');
        }
      }
    }
    return {series, vAxes};
  }

  populateVAxes(yAxis1Format: string, yAxis2Format: string): any {
    let a: any = {};

    if (yAxis2Format) {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: this.currencySymbol+'#'
        },
        1: {
          minValue: 0,
          max: 100,
          format: '#\'%\''
        }
      };
      a['0']['format'] = yAxis1Format;
      a['1']['format'] = yAxis2Format;
    } else {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: this.currencySymbol+'#'
        }
      };
      a['0']['format'] = yAxis1Format;
    }
    return a;
  }

}
