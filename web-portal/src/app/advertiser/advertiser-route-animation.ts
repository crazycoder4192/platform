import {
    transition,
    trigger,
    query,
    style,
    animate,
    group,
    animateChild
} from '@angular/animations';


export const advertiserSlideInAnimation =
    trigger('routeAnimations', [
        transition('* => dashboard', [
            query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style ({ opacity: '0' }),
                    animate('1s')
                ], { optional: true }),
                query(':leave', [
                    animate('0s')
                ], { optional: true }),
            ])
        ]),
        transition('* => brandDashboard', [
            query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style ({ opacity: '0' }),
                    animate('1s')
                ], { optional: true }),
                query(':leave', [
                    animate('0s')
                ], { optional: true }),
            ])
        ]),
        transition('* => advertiserProfile', [
            query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
            group([
                query(':enter', [
                    style ({ opacity: '0' }),
                    animate('1s')
                ], { optional: true }),
                query(':leave', [
                    animate('0s')
                ], { optional: true }),
            ])
        ]),
    ]);