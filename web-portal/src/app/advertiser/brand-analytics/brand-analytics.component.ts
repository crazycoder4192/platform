import { Component, OnInit } from '@angular/core';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CampaignElement } from '../../_models/advertiser_campaign';
import { Brand } from '../../_models/advertiser_brand_model';
import { SubCampaignElement } from '../../_models/advertiser_sub_campaign';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AdvertiserAnalyticsService } from 'src/app/_services/advertiser/advertiser-analytics.service';
import { UtilService } from '../../_services/utils/util.service';
import { AdvertiserUserService } from '../../_services/advertiser/advertiser-user.service';
import * as d3 from 'd3';
import { MatSnackBar } from '@angular/material';
import { AdvertiserDashboardService } from 'src/app/_services/advertiser/advertiser-dashboard.service';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { Router } from '@angular/router';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-brand-analytics',
  templateUrl: './brand-analytics.component.html',
  styleUrls: ['./brand-analytics.component.scss']
})
export class BrandAnalyticsComponent implements OnInit {
  public viewBrand = false;
  public audience = false;
  public behavior = false;
  public conversion = false;
  public showfilters = false;
  public rolesAndPermissions: any[];
  public rolePermissions: any;
  public campaignCtrl = new FormControl();
  public uniqueStates: string[];
  public filteredCampaigns: Observable<CampaignElement[]>;
  public campaignsList: CampaignElement[];
  public brandList: Brand[];
  public subCampaignsList: SubCampaignElement[];
  public saveFilterForm: FormGroup;
  public subCampaignName: string;
  public subCampaignWebTypes: string;
  public duration = new FormControl();
  public reach = new FormControl();
  public startDate: Date = new Date(2019, 1, 1);
  public startDateMin = new Date(2019, 1, 1);
  public todayDate = new Date();
  public endDateMin: Date;
  public specializations: any;
  private advertiserId: string;
  private brandId: string;
  public brandName: string;
  public clientType: string;
  public cardsData: any = { increasedSpends: 0, totalSpends: 0 };

  public filteredDuration = [
    { value: 'Custom Duration', name: 'Custom Duration' },
    { value: 'Last 24 hours', name: 'Last 24 hours' },
    { value: 'Last 48 hours', name: 'Last 48 hours' },
    { value: 'Last 1 week', name: 'Last 1 week' },
    { value: 'Last 1 month', name: 'Last 1 month' },
    { value: 'Last 6 months', name: 'Last 6 months' }
  ];
  public filteredReach = [
    { value: 'steak-0', name: 'Reach 1' },
    { value: 'steak-0', name: 'Reach 2' },
    { value: 'steak-0', name: 'Reach 3' }
  ];

  public savedFilters;

  public filterSpecialization = [];

  public filterGender = [
    { value: 'male', name: 'Male' },
    { value: 'female', name: 'Female' }
  ];

  public filterExperience = [];

  public filterLocation = [];
  public filterSite = [

    { value: 'steak-0', name: 'Site' },
    { value: 'steak-0', name: 'Site 1' },
    { value: 'steak-0', name: 'Site 2' },
    { value: 'steak-0', name: 'Site 3' }
  ];

  public filterBannerType = [

    { value: 'steak-0', name: 'Banner type' },
    { value: 'steak-0', name: 'Banner type 1' },
    { value: 'steak-0', name: 'Banner type 2' },
    { value: 'steak-0', name: 'Banner type 3' }
  ];

  public heatData = [
    { 'group': 'A', 'variable': 'v1', 'value': 6 },
    { 'group': 'A', 'variable': 'v2', 'value': 16 },
    { 'group': 'A', 'variable': 'v3', 'value': 26 },
    { 'group': 'A', 'variable': 'v4', 'value': 36 },
    { 'group': 'B', 'variable': 'v1', 'value': 6 },
    { 'group': 'B', 'variable': 'v2', 'value': 16 },
    { 'group': 'B', 'variable': 'v3', 'value': 26 },
    { 'group': 'B', 'variable': 'v4', 'value': 36 },

  ];
  private currencySymbol : string;


  private userDetails = JSON.parse(localStorage.getItem('currentUser'));

  public displayFilters() {
    this.showfilters = !this.showfilters;
  }
  constructor(private formBuilder: FormBuilder, private advertiserBrandService: AdvertiserBrandService,
    private advertiserAnalyticsService: AdvertiserAnalyticsService, private utilService: UtilService,
    private advertiserUserService: AdvertiserUserService, private snackBar: MatSnackBar,
    private dashboardService: AdvertiserDashboardService,
    private campaignService: CampaignService, private router: Router) {
    this.initRolesAndPermissions();

    utilService.getFiltersByEmail(this.userDetails.email).subscribe(
      response => {
        this.savedFilters = response;
      },
      err => {

      });

    advertiserUserService.getAdvertiserUser(this.userDetails.email).subscribe(
      response => {
        this.advertiserId = response['advertiserId'];
        advertiserBrandService.getAdvertiserBrandByAdvertiserId(this.advertiserId).subscribe(
          res => {
            this.brandList = res;
            this.onSelectBrand( this.brandList[0]._id);
            campaignService.getCampaignByUserEmailId(this.userDetails.email).subscribe(
              campaignResponse => {
                this.campaignsList = <CampaignElement[]>campaignResponse;
              },
              err => {
              });
          },
          err => {
            console.log(err);
          }
        );
      });

    utilService.getValidHCPSpecialization().subscribe(
      response => {
        this.specializations = response;
      },
      err => {

      });

  }

  get f() { return this.saveFilterForm.controls; }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.saveFilterForm = this.formBuilder.group({
      _id: [''],
      startDate: [new Date(), Validators.required],
      endDate: [new Date(), Validators.required],
      duration: ['Custom Duration', Validators.required],
      reach: [''],
      specialization: ['', Validators.required],
      filter: [''],
      gender: ['', Validators.required],
      experience: ['', Validators.required],
      location: ['', Validators.required],
      site: ['', Validators.required],
      bannerType: ['', Validators.required],
      advertiserId: this.advertiserId,
      filterName: [''],
      brandId: [[this.brandId]],
      email: this.userDetails.email
    });

    // this.drawAudienceGenderAndExperienceChart();
    // this.drawAudienceSpecializationStackedBar();
    //this.drawBehaviourHeatMap(this.heatData);
    //this.drawAudienceHCPArcheTypeBarChart();
    //this.drawLocationGraph();

    this.drawConversionEngagementAreaChart();

  }

  saveFilter() {
    this.utilService.saveFilter(this.saveFilterForm.value).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
      },
      err => {

      }
    )
  }


  initRolesAndPermissions() {
    this.rolesAndPermissions = [];
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    if (this.rolePermissions && this.rolePermissions.length) {
      this.rolePermissions.forEach(element => {
        if (element.identifier === 'analytics_view_brand') {
          this.viewBrand = true;
        }

        if (element.identifier === 'analytics_audience') {
          this.audience = true;
        }
        if (element.identifier === 'analytics_behavior') {
          this.behavior = true;
        }

        if (element.identifier === 'analytics_conversion') {
          this.conversion = true;
        }
      });
    }
  }

  drawAudienceGenderAndExperienceChart() {
    this.drawMaleChart();
    this.drawFemaleChart();
    this.drawOtherChart();
  }

  drawMaleChart() {
    const data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2]
    ]);

    const options: any = {
      colors: ['#78eded', '#a5f3f3', '#d2f9f9'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('malePieChart'));

    chart.draw(data, options);
  }

  drawFemaleChart() {
    const data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2]
    ]);

    const options: any = {
      colors: ['#ce85e0', '#deaeea', '#efd6f5'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('femalePieChart'));

    chart.draw(data, options);
  }

  drawOtherChart() {
    const data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2]
    ]);

    const options: any = {
      colors: ['#79cdcd', '#8deeee', '#97ffff'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('othersPieChart'));

    chart.draw(data, options);
  }
  drawAudienceSpecializationStackedBar(barData) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Specialization');
    data.addColumn('number', 'Male');
    data.addColumn('number', 'Female');
    data.addColumn('number', 'Other');
    for (let i = 0; i < barData.length; i++) {
      data.addRow([barData[i].specialization, barData[i].male, barData[i].female, barData[i].other]);
    }

    const options: any = {
      legend: { position: 'top', maxLines: 3, alignment: 'end' },
      bar: { groupWidth: '40%' },
      isStacked: true,
      colors: ['94e6f7', 'd796f0', '5cf3d3'],
      chartArea: { 'width': '85%', 'height': '80%' }
    };

    const chart = new google.visualization.ColumnChart(document.getElementById('specializationStackedBarChart'));

    chart.draw(data, options);
  }

  drawAudienceHCPArcheTypeBarChart(archetypeData) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Archetype');
    data.addColumn('number', 'Numbers');

    data.addRows([
      ['Connectors', archetypeData.connectors],
      ['Curators', archetypeData.curators],
      ['Followers', archetypeData.followers],
      ['Pharmaiety', archetypeData.pharmaiety],
      ['Tool Boxers', archetypeData.toolBoxers]
    ]);
    const options: any = {

      hAxis: {

        gridlines: {
          color: 'transparent'
        },
        textStyle: { color: '#B2BEB5' },
        baselineColor: '#B2BEB5'
      },
      vAxis: {

      },
      colors: ['#8185f5'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      },
      bar: { groupWidth: '40%' }
    };

    const chart = new google.visualization.ColumnChart(
      document.getElementById('drawAudienceHCPArcheTypeBarChart'));
    chart.draw(data, options);

  }
  drawAudienceFrequencyPieChart(frequencyData: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Specialization');
    data.addColumn('number', 'Male');
    for (let i = 0; i < frequencyData.length; i++) {
      data.addRow([frequencyData[i]._id, frequencyData[i].count]);
    }

    const options: any = {
      // pieHole: 0.5,
      colors: ['#81e0f5', '#ab75f5', '#f37f89'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        position: 'none'
      }

    };

    const chart = new google.visualization.PieChart(document.getElementById('audienceFrequencyPieChart'));
    chart.draw(data, options);

  }

  drawBehaviourHeatMap(data: any) {
    const yAxisValues: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    const xAxisValues: string[] = ['00-03', '03-06', '06-09', '09-12', '12-15', '15-18', '18-21', '21-24'];
    const margin = { top: 30, right: 30, bottom: 30, left: 100 },
      width = 700 - margin.left - margin.right,
      height = 450 - margin.top - margin.bottom;

    const svg = d3.select('#heatmap')
      .append('svg')
      .attr('width', 700)
      .attr('height', 500);

    // Build X scales and axis:
    const x = d3.scaleBand()
      .range([0, width])
      .domain(xAxisValues)
      .padding(0.01);

    svg.append('g')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x));

    // Build Y scales and axis:
    const y = d3.scaleBand()
      .range([height, 0])
      .domain(yAxisValues)
      .padding(0.10);

    svg.append('g')
      .call(d3.axisLeft(y));

    const myColor = d3.scaleLinear<string>()
      .range(['#FF9999', '#FF3333'])
      .domain([0, 10]);

    const tooltip = d3.select('#heatmap')
      .append('div')
      .style('opacity', 0)
      .attr('class', 'tooltip')
      .style('background-color', 'white')
      .style('border', 'solid')
      .style('border-width', '2px')
      .style('border-radius', '5px')
      .style('padding', '5px');

    svg.selectAll()
      .data(data, function (d: any) { return d; })
      .enter()
      .append('rect')
      .attr('x', function (d: any) { return x(d.hourRange); })
      .attr('y', function (d: any) { return y(d.weekDay); })
      .attr('width', x.bandwidth())
      .attr('height', y.bandwidth())
      .style('fill', function (d: any) { return myColor(d.count); })
      .on('mouseover', function (d) {
        tooltip.style('opacity', 1);
      })
      .on('mousemove', function (d) {
        tooltip
          .html('views: ' + d.count)
          .style('left', (d3.mouse(this)[0] + 70) + 'px')
          .style('top', (d3.mouse(this)[1]) + 'px')
      })
      .on('mouseleave', function (d) {
        tooltip.style('opacity', 0);
      });

  }


  drawLocationGraph(data) {
    const Ldata = new google.visualization.DataTable();
    Ldata.addColumn('string', 'State');
    Ldata.addColumn('number', 'Popularity');

    for (const loc of data) {
      const temp: any[] = [];
      temp.push(loc.state, loc.numberOfEntries);
      Ldata.addRows([temp]);
    }

    const options: any = {
      colorAxis: { colors: ['#78eded'] },
      region: 'US',
      displayMode: 'regions',
      resolution: 'provinces',
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.GeoChart(document.getElementById('locationChart'));
    chart.draw(Ldata, options);

  }

  drawConversionEngagementAreaChart() {
    const data = google.visualization.arrayToDataTable([
      ['Year', 'Sales', 'Expenses'],
      ['2013', 1000, 400],
      ['2014', 1170, 460],
      ['2015', 660, 1120],
      ['2016', 1030, 540]
    ]);

    const options: any = {
      // hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
      //vAxis: {minValue: 0},
      chartArea: { 'width': '85%', 'height': '80%' },
      width: 1100,
      height: 300,
      legend: {
        position: 'none'
      }
    };

    const chart = new google.visualization.AreaChart(document.getElementById('conversionEngagementAreaChart'));
    chart.draw(data, options);
  }

  drawBehaviorDisplayLocationStackedBarChart(barData) {
    const data = new google.visualization.DataTable();
    const headers = barData[0];
    for (let i = 0; i < headers.length; i++) {
      if (i === 0) {
        data.addColumn('string', headers[i]);
      } else {
        data.addColumn('number', headers[i]);
      }
    }
    for (let i = 1; i < barData.length; i++) {
      data.addRow(barData[i]);
    }

    const options: any = {
      legend: { position: 'top', maxLines: 10, alignment: 'start' },
      bar: { groupWidth: '10%' },
      isStacked: true,
      colors: ['94e6f7', 'd796f0', '5cf3d3'],
      chartArea: { 'width': '85%', 'height': '80%' }
    };

    const chart = new google.visualization.ColumnChart(document.getElementById('behaviorDisplayLocationStackedBarChart'));

    chart.draw(data, options);
  }

  drawBehaviorDevicesDonutChart(deviceData: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Devices');
    data.addColumn('number', 'count');
    data.addRow(['desktop', deviceData.desktop]);
    data.addRow(['mobile', deviceData.mobile]);
    data.addRow(['tablet', deviceData.tablet]);

    const options: any = {
      pieHole: 0.5,
      colors: ['#81e0f5', '#ab75f5', '#f37f89'],
      chartArea: { 'width': '85%', 'height': '80%' },
      legend: {
        alignment: 'center',
        position: 'bottom'
      }
    };

    const chart = new google.visualization.PieChart(document.getElementById('behaviorDevicesDonutChart'));
    chart.draw(data, options);

  }
  onSelectBrand(brandId: string) {
    const brandDetails = this.brandList.find((x) => x._id === brandId);
    if (brandDetails !== undefined) {
      this.brandId = brandId;
      this.brandName = brandDetails.brandName;
      this.clientType = brandDetails.clientType;

      this.dashboardService.getLastCheckInData(this.brandId).subscribe(
        result => {
          this.cardsData = result;
        },
        err => {
          console.log('Error! getAllBrands : ' + err);
        }
      );
    }
  }

  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    this.endDateMin = this.startDate;
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.saveFilterForm.get('endDate').setValue(new Date());
    this.endDateMin = this.startDate;
    this.endDateMin = this.startDate;
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    const date = this.saveFilterForm.get('endDate').value;
    /*if(new Date() > date){
      console.log('ok');
    }*/
  }


  onSelectDuration(duration) {
    let date = new Date();
    this.saveFilterForm.get('endDate').setValue(date);
    if (duration === 'Custom Duration') {
      //  date = new Date(new Date().setHours(new Date().getHours() - 24));
      this.saveFilterForm.get('startDate').setValue(date);
      this.saveFilterForm.get('startDate').enable();
      this.saveFilterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      date = new Date(new Date().setHours(new Date().getHours() - 24));
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 48 hours') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setHours(new Date().getHours() - 48));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 week') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setDate(new Date().getDate() - 7));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 month') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 1));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 6 months') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 6));
      this.saveFilterForm.get('startDate').setValue(date);
    }
  }

  onSelectFilter(id) {

  }

  getAnalysticsData() {
    if (this.advertiserId !== undefined && this.brandId !== undefined) {
      this.saveFilterForm.get('advertiserId').setValue(this.advertiserId);
      this.saveFilterForm.get('brandId').setValue(this.brandId);
      console.log(this.saveFilterForm.value);
      this.advertiserAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
        response => {
          this.drawAudienceSpecializationStackedBar(response.audienceSpecialization);
          this.drawAudienceHCPArcheTypeBarChart(response.audienceArcheType);
          this.drawLocationGraph(response.audienceLocation);
          this.drawAudienceFrequencyPieChart(response.audienceFrequency);
          this.drawBehaviorDisplayLocationStackedBarChart(response.behaviorDisplayLocation);
          this.drawBehaviorDevicesDonutChart(response.behaviorDevices);
          this.drawBehaviourHeatMap(response.behaviorTimeOfDayEnagement);
          console.log(response);
        },
        err => {

        });
    }
  }
  onSelectCampaign(campaignId) {
    this.router.navigate(['/advertiser/analytics']);
    localStorage.setItem('campaignId', JSON.stringify(campaignId));

  }


}
