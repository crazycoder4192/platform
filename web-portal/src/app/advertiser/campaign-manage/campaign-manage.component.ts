import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';
import { MatSnackBar, MatDialogConfig, MatDialog, MatSort } from '@angular/material';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { Router } from '@angular/router';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import {Sort} from '@angular/material/sort';
import { AddCampaignComponent } from '../campaign-creation/add-campaign/add-campaign.component';
import { SubcampaignStatus } from 'src/app/_models/advertiser_sub_campaign';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { Subject } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UploadToS3Service } from 'src/app/common/upload-to-s3/upload-to-s3.service';
import config from 'appconfig.json';
import { UtilService } from '../../_services/utils/util.service';

@Component({
  selector: 'app-campaign-manage',
  templateUrl: './campaign-manage.component.html',
  styleUrls: ['./campaign-manage.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class CampaignManageComponent implements OnInit {
  SubcampaignStatus = SubcampaignStatus;
  campaignStatus: any;
  campaignsList: any = [];
  columnsToDisplay = ['campImage', 'campaignName', 'status', 'brandName', 'From date', 'To date', 'Spend', 'Reach', 'Edit', 'actions'];
  filteredCampaigns: any = [];
  @ViewChild(MatSort) sort: MatSort;
  grid = false;
  campaign = '';
  Showfilters = false;
  expandedElement: null;
  filteredAdvertisers: any = [];
  brandsOption: any = [];
  minDate = new Date();
  isChecked = 'All';

  userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  campaignIds: any[];
  statusList: string[];
  typeList: string[];

  public todayDate: Date = new Date(new Date().setHours(23, 59, 59));
  public sDate: Date = new Date(2019, 1, 1);
  public startDateMax: Date = this.todayDate;
  public endDateMax: Date = this.todayDate;
  public eDate: Date = this.todayDate;
  createdBy: any;
  status: any;
  type: string;
  rolePermissions: any;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['advertiser_manage_campaigns_campaign', 'advertiser_manage_campaigns_to_date',
  'advertiser_manage_campaigns_from_date', 'advertiser_manage_campaigns_spends',
  'advertiser_manage_campaigns_reach', 'advertiser_manage_campaigns_status',
  'advertiser_manage_campaigns_cpc', 'advertiser_manage_campaigns_cpm',
  'advertiser_manage_campaigns_ctr', 'publisher_manage_assets_lifetime_clicks',
  'publisher_manage_assets_lifetime_impressions']);
  private currencySymbol : string;

  constructor(private campaignService: CampaignService, private spinner: SpinnerService,
    private toolTipService: ToolTipService,
    private advertiserService: AdvertiserProfileService, private snackBar: MatSnackBar, private dialog: MatDialog, private router: Router,
    private eventEmitterService: EventEmitterService,
    private uploadService: UploadToS3Service, private utilService: UtilService) {
    // this.getAllCampaignsAndSubCampaigns();
    // this.getAllCampaignsAndSubCampaignsForAdvertiser();
    this.renderListView();
    localStorage.setItem('activeIcon', JSON.stringify(''));
    this.eventEmitterService.onClickNavigation();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.renderListView();
    this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }

  filterChange(status: string) {
    this.filteredCampaigns = this.campaignsList.filter(item => {
      if (this.isChecked && !item.status.includes(status)) {
        return false;
      }
      return true;
    }
    );
  }

  renderListView() {
    this.spinner.showSpinner.next(true);
    this.campaignService.getManageCampaings().subscribe(
      resposne => {
        this.campaignsList = <[]>resposne;
        this.campaignsList.forEach(element => {
          element.subCampaignInfo.forEach(item => {
            item['imageUrls'] = [];
            item.creativeSpecifications.creativeDetails.forEach(thisCreative => {
              item['imageUrls'].push(thisCreative.url);
            });
          });
        });
        this.filteredCampaigns = this.campaignsList;
        this.filteredCampaigns.sort = this.sort;
        const users = this.campaignsList.map(data => {
          return data.createdBy;
        });
        this.filteredAdvertisers = users.filter((x, i, a) => x && a.indexOf(x) === i);
        this.filteredAdvertisers.unshift('All');
        const brands = this.campaignsList.map(data => {
          return data.brandName;
        });
        this.brandsOption = brands.filter((x, i, a) => x && a.indexOf(x) === i);
        this.spinner.showSpinner.next(false);
        // this.filteredCampaigns = this.campaignsList.filter(item => {
        //   if (!item.status.includes('Active')) {
        //     return false;
        //   }
        //   return true;
        // });
        this.filteredCampaigns.forEach(element => {
          element['showExpand'] = false;
        });
        this.statusList = ['All', 'Error', 'Under review', 'Active', 'Paused', 'On hold', 'Completed'];
        // this.typeList = ['All', 'Banner', 'Video'];
        this.typeList = ['Banner'];
        this.searchCampaign('');
        // this.filterCampaign();
      },
      err => {
        console.log('Error! getAllCampaigns : ' + err);
      });
  }

  filterCampaign() {
    this.filteredCampaigns = [];
      this.campaignsList.forEach(thisCampaign => {
        thisCampaign.filteredSubCampaign = thisCampaign.subCampaignInfo.filter(item => {
          if (this.status && this.status !== 'All' && !item.status.includes(this.status)) {
            return false;
          }
          if (this.createdBy && this.createdBy !== 'All' && !thisCampaign.createdBy.includes(this.createdBy)) {
            return false;
          }
          if (this.type && this.type !== 'All' && !item.creativeType.includes(this.type)) {
            return false;
          }
          let sDate = new Date();
          let eDate = new Date();
          if (this.sDate) {
            sDate = new Date(this.sDate.getFullYear(), this.sDate.getMonth(), this.sDate.getDate(), 0, 0, 0);
          }
          if (this.eDate) {
            eDate = new Date(this.eDate.getFullYear(), this.eDate.getMonth(), this.eDate.getDate(), 23, 59, 59, 59);
          }
          const dbStartDate = new Date(item.operationalDetails.startDate);
          const dbendDate = new Date(item.operationalDetails.endDate);
          if (this.sDate && this.eDate && dbStartDate < sDate && dbendDate > eDate ) {
            return false;
          } else if (this.sDate && !this.eDate && dbStartDate < sDate ) {
              return false;
          } else if (!this.sDate && this.eDate && dbendDate > eDate ) {
              return false;
          }
          return true;
        });
        if (thisCampaign.filteredSubCampaign.length) {
          this.filteredCampaigns.push(thisCampaign);
        }
    });
  }
  editSubCampaign(bId, Id, subId) {
    this.router.navigate(['/advertiser/campaignCreation'],
      { state: { 'brandId': JSON.stringify(bId), 'campaignId': JSON.stringify(Id), 'subCampaignId': JSON.stringify(subId) } });
  }

  navigateToSubCampaignAnalytics(bId, id, subId) {
    this.router.navigate(['/advertiser/analytics'],
    { state: { 'brandId': JSON.stringify(bId), 'campaignId': JSON.stringify(id), 'subCampaignId': JSON.stringify(subId) } });
  }

  deleteSubCampaign(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the sub campaign',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.campaignService.getCreativesUrl(id).subscribe(
          (creativeUrls: any) => {
            this.campaignService.deleteSubCampaign(id).subscribe(
              data => {
                this.uploadService.fetchBrandIdentifierS3();
                this.uploadService._brandIdentifySource.subscribe(
                  brandIdentifier => {
                    for (const url of creativeUrls) {
                      const s3path = 'https://' + config['AWS-S3']['BUCKET'] + '.s3.amazonaws.com/' +
                      config['AWS-S3']['FOLDER'] + '/' + brandIdentifier + '/';
                      const fileName = url.substring(s3path.length);
                      this.uploadService.deletefile(fileName, brandIdentifier);
                    }
                  });
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'success',
                    message: data['message']
                  }
                });
                this.isChecked = 'All';
                this.renderListView();
              },
              err => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
              }
            );
            dialogRef.close();
          }
        );        
      } else {
        dialogRef.close();
      }
    });
  }

  displayView() {
    this.grid = !this.grid;
  }

  displayFilters() {
    this.Showfilters = !this.Showfilters;
    this.clearFilters();
  }

  searchCampaign(name) {
    if (!name) {
      this.filteredCampaigns = this.campaignsList;
      this.campaignsList.forEach(thisCampaign => {
        thisCampaign.filteredSubCampaign = thisCampaign.subCampaignInfo;
      });
    } else {
      this.filteredCampaigns = [];
      this.filteredCampaigns = this.campaignsList.filter((option) =>
          option.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
      );
      if (this.filteredCampaigns.length) {
        this.filteredCampaigns.forEach(thisCampaign => {
          thisCampaign.filteredSubCampaign = thisCampaign.subCampaignInfo;
        });
      } else {
        this.campaignsList.forEach(thisCampaign => {
          thisCampaign.filteredSubCampaign = thisCampaign.subCampaignInfo.filter((option) =>
            option.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
          );
          if (thisCampaign.filteredSubCampaign.length) {
            this.filteredCampaigns.push(thisCampaign);
          }
        });
      }
    }
    return this.filteredCampaigns;
  }

  sortData(sort: Sort) {
    const data: any = this.filteredCampaigns.slice();
    if (!sort.active || sort.direction === '') {
      this.filteredCampaigns = data;
      return;
    }

    this.filteredCampaigns = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'status': return compare(a.status, b.status, isAsc);
        case 'brandName': return compare(a.brandName, b.brandName, isAsc);
        case 'startDate': return compare(a.startDate, b.startDate, isAsc);
        case 'endDate': return compare(a.endDate, b.endDate, isAsc);
        case 'campaingSpend': return compare(a.campaingSpend, b.campaingSpend, isAsc);
        case 'campaingReach': return compare(a.campaingReach, b.campaingReach, isAsc);
        default: return 0;
      }
    });
  }

  sortSubCampaignData(sort: Sort, position: number) {
    const data: any = this.filteredCampaigns[position].filteredSubCampaign.slice();
    if (!sort.active || sort.direction === '') {
      this.filteredCampaigns[position].filteredSubCampaign = data;
      return;
    }

    this.filteredCampaigns[position].filteredSubCampaign = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': {
          if (a && b && b.name && a.name) {
            return compare(a.name, b.name, isAsc);
          }
          break;
        }
        case 'status': {
          if (a && b && a.status !== null && b.status !== null) {
              return compare(a.status, b.status, isAsc);
        }
          break;
        }
        case 'creativeType': {
          if (a && b && a.creativeType !== null && b.creativeType !== null) {
              return compare(a.creativeType, b.creativeType, isAsc);
        }
          break;
        }
        case 'operationalDetails.startDate': {
          if (a && a.operationalDetails && b && b.operationalDetails &&
            a.operationalDetails.startDate !== null && b.operationalDetails.startDate !== null) {
              return compare(a.operationalDetails.startDate, b.operationalDetails.startDate, isAsc);
        }
          break;
        }
        case 'operationalDetails.endDate': {
          if (a && a.operationalDetails && b && b.operationalDetails &&
            a.operationalDetails.endDate !== null && b.operationalDetails.endDate !== null) {
              return compare(a.operationalDetails.endDate, b.operationalDetails.endDate, isAsc);
        }
          break;
        }
        case 'transactionalData[0].TotalAmount': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
            b && b.transactionalData && a.transactionalData.length > 0 &&
            a.transactionalData[0].TotalAmount !== null && b.transactionalData[0].TotalAmount !== null) {
              return compare(a.transactionalData[0].TotalAmount, b.transactionalData[0].TotalAmount, isAsc);
        }
          break;
        }
        case 'transactionalData[0].reachCount': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
            b && b.transactionalData && a.transactionalData.length > 0 &&
            a.transactionalData[0].reachCount !== null && b.transactionalData[0].reachCount !== null) {
              return compare(a.transactionalData[0].reachCount, b.transactionalData[0].reachCount, isAsc);
          }
          break;
        }
        case 'transactionalData[0].Clicks': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
            b && b.transactionalData && a.transactionalData.length > 0 &&
            a.transactionalData[0].Clicks !== null && b.transactionalData[0].Clicks !== null) {
              return compare(a.transactionalData[0].Clicks, b.transactionalData[0].Clicks, isAsc);
          }
          break;
        }
        case 'transactionalData[0].Impressions': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
            b && b.transactionalData && a.transactionalData.length > 0 &&
            a.transactionalData[0].Impressions !== null && b.transactionalData[0].Impressions !== null) {
              return compare(a.transactionalData[0].Impressions, b.transactionalData[0].Impressions, isAsc);
          }
          break;
        }
        case 'transactionalData[0].CTR': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
            b && b.transactionalData && a.transactionalData.length > 0 &&
            a.transactionalData[0].CTR !== null && b.transactionalData[0].CTR !== null) {
              return compare(a.transactionalData[0].CTR, b.transactionalData[0].CTR, isAsc);
          }
          break;
        }
        case 'transactionalData[0].CPM': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
            b && b.transactionalData && a.transactionalData.length > 0 &&
            a.transactionalData[0].CPM !== null && b.transactionalData[0].CPM !== null) {
              return compare(a.transactionalData[0].CPM, b.transactionalData[0].CPM, isAsc);
          }
          break;
        }
        case 'transactionalData[0].CPC': {
          if (a && a.transactionalData && a.transactionalData.length > 0 &&
              b && b.transactionalData && a.transactionalData.length > 0 &&
              a.transactionalData[0].CPC !== null && b.transactionalData[0].CPC !== null) {
            return compare(a.transactionalData[0].CPC, b.transactionalData[0].CPC, isAsc);
          }
          break;
        }
        default: return 0;
      }
    });
  }

  changedStatusToApprove(campaign: any, status: string) {
    this.campaignStatus = {'name': campaign.name, 'status': status};
    let message = '';
    switch (status) {
      case SubcampaignStatus.paused: {
        message = 'Are you sure you want to pause "' + campaign.name + '" sub campaign ?';
        break;
      }
      case SubcampaignStatus.approved: {
        message = 'Do you want to resume "' + campaign.name + '" sub campaign ?';
        break;
      }
      case SubcampaignStatus.recentlyCompleted: {
        message = 'Are you sure you want to end "' + campaign.name + '" sub campaign ?';
        break;
      }
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: message,
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const campainDetails = {
          id: campaign._id,
          status: status,
          name: campaign.name,
          brandId: campaign.brandId
        };
        this.campaignService.approveCampaign(campainDetails).subscribe(
          resposne => {
            this.isChecked = 'All';
            if (this.campaignStatus.status === SubcampaignStatus.recentlyCompleted) {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: 'Sub-campaign "' + this.campaignStatus.name +
                  '" will remain active for next 7 days(though no ads will be pulished); '
                }
            });
            }
            this.eventEmitterService.onReadNotification();
            this.renderListView();
          },
          err => {
            console.log('Error! getAllCampaigns : ' + err);
          });
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
    this.isChecked = 'All';
    this.renderListView();
  }
  clearFilters() {
    this.sDate = null;
    this.eDate = null;
    this.createdBy = '';
    this.status = '';
    this.type = '';
    this.campaign = '';
    this.searchCampaign('');
  }
  editCampaign(id, branddId, campaignName) {
    const dialogRef = this.dialog.open(AddCampaignComponent, {
      width: '50%',
      data: { category: 'single', brandId: branddId, name: campaignName, page: 'Edit' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.name === null) {
          return;
        }
        this.campaignService.updateCampaign(id, result).subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }
  deleteCampaign(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the campaign',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.campaignService.deleteCampaign(id, 'campaign').subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }
  navigateToCampaignAnalytics(bId, id) {
    this.router.navigate(['/advertiser/analytics'],
    { state: { 'brandId': JSON.stringify(bId), 'campaignId': JSON.stringify(id) } });
  }
  toggleExpand(campaign) {
    this.filteredCampaigns.forEach(element => {
      if (element._id === campaign._id) {
        element['showExpand'] = !element['showExpand'];
      } else {
        element['showExpand'] = false;
      }
    });
  }
  dateDiffInDays(date1, date2) {
    // Discard the time and time-zone information.
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }
  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDateMax = new Date(this.eDate);
  }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
