import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignManageComponent } from './campaign-manage.component';

describe('CampaignManageComponent', () => {
  let component: CampaignManageComponent;
  let fixture: ComponentFixture<CampaignManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
