import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-profile-panel',
  templateUrl: './profile-panel.component.html',
  styleUrls: ['./profile-panel.component.scss']
})
export class ProfilePanelComponent implements OnInit {
  @Output() private toggleProfile = new EventEmitter<boolean>();
  @Output() private toggleProfileIcon = new EventEmitter<boolean>();
  clickState: boolean;
  rolePermissions: any;
  public accountsTab = false;
  public hasPersonalProfile: boolean = this.authenticationService.currentUserValue.hasPersonalProfile;
  constructor(
    private router: Router,
    private eventEmitterService: EventEmitterService,
    private authService: AuthenticationService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
  }
  @HostListener('click')
  clickInside() {
    this.clickState = false;
  }
  @HostListener('document:click')
  clickout() {
    if (this.clickState) {
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
    }
    this.clickState = true;
  }
  gotoClickedLink(linkType: string) {
    if (linkType === 'profile') {
      localStorage.setItem('activeIcon', JSON.stringify('profile'));
      this.eventEmitterService.onClickNavigation();
      this.router.navigate(['/advertiser/advertiserProfile']);
      this.toggleProfile.emit(false);
    }
    if (linkType === 'email_settings') {
      localStorage.setItem('settingType', JSON.stringify('email_setings'));
      this.router.navigate(['/advertiser/settings']);
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      this.eventEmitterService.onClickAccountSetting();
    }
    if (linkType === 'change_password') {
      localStorage.setItem('settingType', JSON.stringify('change_password'));
      this.router.navigate(['/advertiser/settings']);
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      this.eventEmitterService.onClickAccountSetting();
    }
    if (linkType === 'invitation_code') {
      localStorage.setItem('settingType', JSON.stringify('invitation_code'));
      this.router.navigate(['/advertiser//invitationCode']);
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      this.eventEmitterService.onClickAccountSetting();
    }
  }
  logout() {
    this.authService.logout('advertiser').pipe().subscribe( data => {
      localStorage.removeItem('roleAndPermissions');
      localStorage.removeItem('userRole');
      console.log('Logout successfull : ' + data);
      this.dialog.closeAll();
    },
    error => {
      console.log('Logout successfull : ' + error);
    });
  }
}
