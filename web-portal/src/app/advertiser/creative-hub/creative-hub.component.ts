import { Component, OnInit, SecurityContext, Pipe, PipeTransform } from '@angular/core';
import { MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { CreativeHubCreationComponent } from '../creative-hub-creation/creative-hub-creation.component';
import { CreativeHubService } from 'src/app/_services/advertiser/creative-hub.service';
import { CreativeHub } from 'src/app/_models/advertise_creative_hub';
import { Banner } from 'src/app/_models/utils';
import { UtilService } from 'src/app/_services/utils/util.service';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { Observable } from 'rxjs/internal/Observable';
import { startWith, map, concatAll } from 'rxjs/operators';
import { forkJoin, Subject } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CreativeHubSizeDetailsComponent } from '../creative-hub-size-details/creative-hub-size-details.component';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import {DomSanitizer} from '@angular/platform-browser';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { BannerCropperComponent } from '../campaign-creation/banner-cropper/banner-cropper.component';
import { HttpClient } from '@angular/common/http';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UploadToS3Service } from 'src/app/common/upload-to-s3/upload-to-s3.service';
import config from 'appconfig.json';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-creative-hub',
  templateUrl: './creative-hub.component.html',
  styleUrls: ['./creative-hub.component.scss']
})
export class CreativeHUBComponent implements OnInit {
  public creativeHubs: any;
  public creativeType = 'image';
  public creativeIndex: any;
  public banners: Banner[];
  public selectedBanners: any[];
  public isShowAll = false;
  public creativeHubIds = [];
  public filteredSectionNames: Observable<string[]>;
  public addImageVideoForm: FormGroup;
  creativeName: any;
  sectionName: any;
  uniqueSectionNames: string[];
  public searchText: string;
  filteredSortBy: string[];
  panelOpenState = false;
  public rolesAndPermissions: any[];
  public rolePermissions: any;
  public Showfilters = false;
  public todayDate: Date = new Date(new Date().setHours(23, 59, 59));
  public endDateMax = this.todayDate;
  public startDateMax = this.todayDate;
  creativeSize: string;
  sDate: any;
  creativeSizes: any[];
  eDate: any = this.todayDate;
  filteredVideoCreativeHubs: any;
  filteredImageCreativeHubs: any;
  brandList: Brand[];

  slideConfig = {
    'slidesToShow': 4,
    'slidesToScroll': 4,
    'prevArrow': '<button type="button" class="slick-prev pull-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
    'nextArrow': '<button type="button" class="slick-next pull-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
    'dots': true,
    'cssEase': 'ease-in'
  };

  /*Todo:
  Implement lazy loading so that images load only after coming in focus
  */


  slickInit(e) {
    console.log('slick initialized');
  }

  // creativeHubDimensions: any[];
  // type350x250: boolean;
  // type300x250: boolean;
  // type300x600: boolean;
  // type250x250: boolean;
  // type200x200: boolean;
  // type468x60: boolean;

  constructor(
    public dialog: MatDialog,
    public creativeService: CreativeHubService,
    private utilHTTPService: UtilService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private eventEmitterService: EventEmitterService,
    private _DomSanitizer: DomSanitizer,
    private brandService: AdvertiserBrandService,
    private http: HttpClient,
    private uploadService: UploadToS3Service
  ) {
    localStorage.setItem('activeIcon', JSON.stringify(''));
    this.eventEmitterService.onClickNavigation();
  }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.initCreativeHubsByType(this.creativeType);
    this.initBanners();
    this.addImageVideoForm = this.formBuilder.group({
      sectionName: [''],
      searchText: [''],
      sortBy: [''],
      filterBy: ['']
    });
    this.filteredSortBy = ['A-Z', 'Recent first', 'Oldest first'];
//
  }
  addCreative(type: string) {
    const dialogRef = this.dialog.open(CreativeHubCreationComponent, {
      width: '50%',
      height: '80%',
      data: {
        creativeType: type,
        uniqueSectionNames: this.uniqueSectionNames
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.initCreativeHubsByType(this.creativeType);
    });
  }

  initCreativeHubsByType(type: string) {
    this.creativeSize = '';
    this.sDate = null;
    this.eDate = null;
    this.creativeHubIds = [];
    this.creativeType = type;
    this.creativeService.getAllCreativeHubs(type).subscribe(
      res => {
        console.log('getAllCreativeHubs', res);
        this.creativeHubs = res;
        this.uniqueSectionNames = [];
        this.uniqueSectionNames.unshift('Default');
        this.creativeHubs.forEach(element => {
          element.creativeHubsSectionWise.forEach(thisItem => {
            const imageUrls = [];
            thisItem.creativeDetails.forEach(item => {
              if (item && item.url) {
                imageUrls.push(item.url);
              }
            });
            thisItem['imageUrls'] = imageUrls;
            thisItem['selected'] = false;
            thisItem['expanded'] = false;
            thisItem['sectionName'] = element.sectionName;
          });
          if (element.sectionName && !this.uniqueSectionNames.includes(element.sectionName)
                && element.sectionName !== 'Default') {
            this.uniqueSectionNames.push(element.sectionName);
          }
        });
        if (this.creativeType === 'image') {
          this.filteredImageCreativeHubs = this.creativeHubs;
        } else if (this.creativeType === 'video') {
          this.filteredVideoCreativeHubs = this.creativeHubs;
        }
        this.searchImageCreatives();
      },
      err => {
        console.log('Error! getAdvertisers : ' + err);
      });
  }

  showAllDimensions(creativeHub, creativeHubItem) {
    this.selectedBanners = [];
    creativeHub.creativeDetails.forEach(element => {
      const findedBanner = this.banners.find(obj => obj.size === element.size);
      if (findedBanner) {
        if (element.resolution === '') {
          element.resolution = element.styleName;
        }
        this.selectedBanners.push(element);
      }
    });
    this.filteredImageCreativeHubs.forEach(element => {
      const cHSW = element.creativeHubsSectionWise;
      cHSW.forEach(thisHub => {
        if (thisHub._id === creativeHub._id) {
          creativeHub.expanded = !creativeHub.expanded;
         } else {
          thisHub.expanded = false;
         }
      });
    });

  }
  initBanners() {
    this.utilHTTPService.getBanners().subscribe(
      res => {
        this.banners = <Banner[]> res;
        this.creativeSizes = [];
        this.banners.map((elem) => this.creativeSizes.push(elem.size));
        this.creativeSizes.unshift('All');
      },
      err => {
        console.log('Error! getBanners : ' + err);
      }
    );
  }

  removeCreativeHub() {
    // iterate over the files and remove from s3
    for (const id of this.creativeHubIds) {
      this.creativeService.getCreativesUrl(id).subscribe(
        creativeUrls => {
          this.creativeService.deleteCreativeHub([id]).subscribe(
            data => {
              this.uploadService.fetchBrandIdentifierS3();
              this.uploadService._brandIdentifySource.subscribe(
                brandIdentifier => {
                  for (const url of creativeUrls) {
                    const s3path = 'https://' + config['AWS-S3']['BUCKET'] + '.s3.amazonaws.com/' +
                      config['AWS-S3']['FOLDER'] + '/' + brandIdentifier + '/';
                    const fileName = url.substring(s3path.length);
                    this.uploadService.deletefile(fileName, brandIdentifier);
                  }
                });

              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
              this.creativeHubIds = [];
              this.initCreativeHubsByType(this.creativeType);
             },
             err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
             }
           );

        }
      );
    }
  }
  openDeleteCofirmDialog(): void {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '40%',
      data: {
        'title' : 'Do you want to delete, This action cannot be undone !'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removeCreativeHub();
      }
    });
  }
  selectCreativeHub(creativeHub) {
    this.creativeHubs.forEach(element => {
      element.creativeHubsSectionWise.forEach(thisItem => {
        if (thisItem._id === creativeHub._id) {
          thisItem['selected'] = thisItem['selected'] ? false : true;

        }
      });
    });
    this.creativeHubIds = [];
    this.creativeHubs.forEach(element => {
      element.creativeHubsSectionWise.forEach(thisItem => {
        if (thisItem['selected']) {
          this.creativeHubIds.push(thisItem._id);
        }
      });
    });
  }
addToSection() {
    const addSectionPutData = {
      sectionName: this.addImageVideoForm.controls['sectionName'].value,
      creativeHubIds: this.creativeHubIds
    };
    if (addSectionPutData.sectionName) {
      this.creativeService.addCreativeHubsToSection(addSectionPutData).subscribe(
        data => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: data['message']
            }
          });
          this.creativeHubIds = [];
          this.addImageVideoForm.get('sectionName').setValue('');
          this.ngOnInit();
         },
         err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
         }
       );
    }
  }
  // call on left aero click.
  slideLeft( creativeHub ) {
    this.creativeHubs.forEach(element => {
      if (element._id === creativeHub._id) {
        element['selected'] = element['selected'] ? false : true;
      }
    });
    this.creativeHubIds = [];
    this.creativeHubs.forEach(element => {
      if (element['selected']) {
        this.creativeHubIds.push(element._id);
      }
    });
  }
  // end

  // call on right aero click.
  slideRight( creativeHub ) {
    this.creativeHubs.forEach(element => {
      if (element._id === creativeHub._id) {
        element['selected'] = element['selected'] ? false : true;
      }
    });
    this.creativeHubIds = [];
    this.creativeHubs.forEach(element => {
      if (element['selected']) {
        this.creativeHubIds.push(element._id);
      }
    });
  }
  // end
  searchImageCreatives() {
    const searchText = this.addImageVideoForm.controls['searchText'].value;
    if (!searchText) {
      this.filteredImageCreativeHubs = this.creativeHubs;
      this.creativeHubs.forEach(element => {
        element.filteredCreatives = element.creativeHubsSectionWise;
      });
    } else {
      this.filteredImageCreativeHubs = [];
      this.creativeHubs.forEach(element => {
        element.filteredCreatives = element.creativeHubsSectionWise.filter((option) =>
          option.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
        );
        if (element.filteredCreatives.length) {
          this.filteredImageCreativeHubs.push(element);
        }
      });
    }
    return this.filteredImageCreativeHubs;
  }
  searchVideoCreatives() {
    const searchText = this.addImageVideoForm.controls['searchText'].value;
    if (!searchText) {
      this.filteredVideoCreativeHubs = this.creativeHubs;
      this.creativeHubs.forEach(element => {
        element.filteredCreatives = element.creativeHubsSectionWise;
      });
    } else {
      this.filteredVideoCreativeHubs = [];
      this.creativeHubs.forEach(element => {
        element.filteredCreatives = element.creativeHubsSectionWise.filter((option) =>
          option.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
        );
        if (element.filteredCreatives.length) {
          this.filteredVideoCreativeHubs.push(element);
        }
      });
    }
    return this.filteredVideoCreativeHubs;
  }
  filterImageCreatives() {
    this.filteredImageCreativeHubs = [];
      this.creativeHubs.forEach(thisCreative => {
        thisCreative.filteredCreatives = thisCreative.creativeHubsSectionWise.filter(item => {
          item.expanded = false;
          if (!this.sDate && !this.eDate && this.creativeSize) {
            if (this.creativeSize !== 'All') {
              item.imageUrls = [];
              item.creativeDetails.forEach(element => {
                if (element.size === this.creativeSize) {
                  item.imageUrls.push(element.url);
                }
              });
            } else if (this.creativeSize === 'All') {
              item.imageUrls = [];
              item.creativeDetails.forEach(element => {
                  item.imageUrls.push(element.url);
              });
            }
            if(item.imageUrls.length === 0) {
              return false;
            } else {
              return true;
            }
          } else {
            if (this.creativeSize && this.creativeSize !== 'All') {
              item.imageUrls = [];
              item.creativeDetails.forEach(element => {
                if (element.size === this.creativeSize) {
                  item.imageUrls.push(element.url);
                }
              });
            } else if (this.creativeSize && this.creativeSize === 'All') {
              item.imageUrls = [];
              item.creativeDetails.forEach(element => {
                  item.imageUrls.push(element.url);
              });
            }
            let sDate = new Date();
            let eDate = new Date();
            if (this.sDate) {
              sDate = new Date(this.sDate.getFullYear(), this.sDate.getMonth(), this.sDate.getDate(), 0, 0, 0);
            }
            if (this.eDate) {
              eDate = new Date(this.eDate.getFullYear(), this.eDate.getMonth(), this.eDate.getDate(), 23, 59, 59);
            }
            const dbDate = new Date(item.created.at);
            if (this.sDate && this.eDate && dbDate >= sDate && dbDate <= eDate) {
              if(item.imageUrls.length === 0) {
                return false;
              } else {
                return true;
              }
            } else if (this.sDate && !this.eDate && dbDate >= sDate) {
              if(item.imageUrls.length === 0) {
                return false;
              } else {
                return true;
              }
            } else if (!this.sDate && this.eDate && dbDate <= eDate) {
              if(item.imageUrls.length === 0) {
                return false;
              } else {
                return true;
              }
            }
              return false;
          }
        });
        if (thisCreative.filteredCreatives.length) {
          this.filteredImageCreativeHubs.push(thisCreative);
        }
    });
    console.log(this.filteredImageCreativeHubs);
  }
  filterVideoCreatives() {
    this.filteredVideoCreativeHubs = [];
      this.creativeHubs.forEach(thisCreative => {
        thisCreative.filteredCreatives = thisCreative.creativeHubsSectionWise.filter(item => {
          let sDate = new Date();
          let eDate = new Date();
          if (this.sDate) {
            sDate = new Date(this.sDate.getFullYear(), this.sDate.getMonth(), this.sDate.getDate(), 0, 0, 0);
          }
          if (this.eDate) {
            eDate = new Date(this.eDate.getFullYear(), this.eDate.getMonth(), this.eDate.getDate(), 23, 59, 59);
          }
          const dbDate = new Date(item.created.at);
          if (this.sDate && this.eDate && dbDate >= sDate && dbDate <= eDate) {
            return true;
          } else if (this.sDate && !this.eDate && dbDate >= sDate) {
            return true;
          } else if (!this.sDate && this.eDate && dbDate <= eDate) {
            return true;
          }
            return false;
        });
        if (thisCreative.filteredCreatives.length) {
          this.filteredVideoCreativeHubs.push(thisCreative);
        }
    });
  }
  sortCreativeByType (type) {
    if (type === 'A-Z') {
      if (this.creativeType === 'image') {
        this.filteredImageCreativeHubs.forEach(element => {
          element.creativeHubsSectionWise.sort(this.dynamicSort('name'));
        });
      } else if (this.creativeType === 'video') {
        this.filteredVideoCreativeHubs.forEach(element => {
          element.creativeHubsSectionWise.sort(this.dynamicSort('name'));
        });
      }
    } else if (type === 'Recent first') {
      if (this.creativeType === 'image') {
        this.filteredImageCreativeHubs.forEach(element => {
          element.creativeHubsSectionWise.sort((a, b) => new Date(a.created.at).getTime() - new Date(b.created.at).getTime());
        });
      } else if (this.creativeType === 'video') {
        this.filteredVideoCreativeHubs.forEach(element => {
          element.creativeHubsSectionWise.sort((a, b) => new Date(a.created.at).getTime() - new Date(b.created.at).getTime());
        });
      }
    } else if (type === 'Oldest first') {
      if (this.creativeType === 'image') {
        this.filteredImageCreativeHubs.forEach(element => {
          element.creativeHubsSectionWise.sort((a, b) => new Date(b.created.at).getTime() - new Date(a.created.at).getTime());
        });
      } else if (this.creativeType === 'video') {
        this.filteredVideoCreativeHubs.forEach(element => {
          element.creativeHubsSectionWise.sort((a, b) => new Date(b.created.at).getTime() - new Date(a.created.at).getTime());
        });
      }
    }
  }
  displayFilters() {
    this.Showfilters = !this.Showfilters;
    this.clearFilters();
  }
  clearFilters() {
    this.creativeSize = '';
    this.sDate = null;
    this.eDate = null;
    this.initCreativeHubsByType(this.creativeType);
  }
  dynamicSort(property) {
    let sortOrder = 1;
    if (property[0] === '-') {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        /* next line works with strings and numbers,
         * and you may want to customize it to your needs
         */
        const result = (a[property].toLowerCase() < b[property].toLowerCase()) ? -1 :
        (a[property].toLowerCase() > b[property].toLowerCase()) ? 1 : 0;
        return result * sortOrder;
    };
  }
  provoked($event) {
    $event.preventDefault();
  }

  async downloadAllBanners(creativeHub: CreativeHub, event) {
    event.stopPropagation();
    /*creativeHub.creativeDetails.forEach(element => {
      const url: string = this._DomSanitizer.sanitize(SecurityContext.URL, element.url);
      const a: any = element;
      const fileName = a.size + '_' + a._id + '.' + a.formatType;
      this.downloadUrl(url, fileName);
    });*/
    this.creativeService.downloadAllBanners(creativeHub.name).subscribe(
      data => {
        const file = new Blob([data], { type: 'application/octet-stream' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(file);
          return;
        } else {
          const timestamp = new Date().valueOf();
          const url = window.URL.createObjectURL(file);
          const link = window.document.createElement('a');
          link.href = url;
          link.download = 'creative-hub_' + timestamp + '.zip';
          link.click();
          window.URL.revokeObjectURL(url);
          link.remove(); // remove the element
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  async downloadBanner(creative, event) {
    event.stopPropagation();
    const newUrl = this._DomSanitizer.sanitize(SecurityContext.URL, creative.url);
    const fileName = creative.size + '_' + creative._id + '.' + creative.formatType;
    this.downloadUrl(newUrl, fileName);
  }

  downloadUrl(url: string, fileName: string) {
    const a: any = document.createElement('a');
    a.href = url;
    a.download = fileName;
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    a.remove();
  }

  previewBanner(banner: any) {
    const resolutionSplit = banner.size.split(' ');
    const selectedData = {banner: banner, url: banner.url,
      width: resolutionSplit[0], height: resolutionSplit[2], previewOrUpload: 'preview'};

    let dialogRef: any;
    dialogRef = this.dialog.open(BannerCropperComponent, {
      width: '75%',
      height: '75%',
      data: selectedData
    });
    dialogRef.afterClosed().subscribe(result => {

    });

  }


  addNewBanners(type: string, creativeHub: CreativeHub) {
    console.log('adding new banners to', creativeHub);
    const dialogRef = this.dialog.open(CreativeHubCreationComponent, {
      width: '50%',
      height: '80%',
      data: {
        creativeType: type,
        creativeHub: creativeHub
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.initCreativeHubsByType(this.creativeType);
    });
  }


  deleteHub(id: any, title: string, event) {
    event.stopPropagation();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Delete Hub',
      message: 'Are you sure you want to delete the Creative - ' + title,
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.creativeHubIds = [];
        this.creativeHubIds.push(id);
        this.removeCreativeHub();
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });

  }


  deleteBanner(banner: Banner, creativeHub: any, event) {
    event.stopPropagation();
    const updatedCreativeHub = creativeHub;
    const toDeleteBannerId = banner._id;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      title: 'Delete Hub',
      message: 'Are you sure you want to delete the Banner - ' + banner['resolution'],
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const creativeToBeDeleted = updatedCreativeHub.creativeDetails.filter(creative => creative._id === toDeleteBannerId);
        this.uploadService.fetchBrandIdentifierS3();
        this.uploadService._brandIdentifySource.subscribe(
          brandIdentifier => {
            if (creativeToBeDeleted && creativeToBeDeleted.length > 0) {
                const s3path = 'https://' + config['AWS-S3']['BUCKET'] + '.s3.amazonaws.com/' +
                  config['AWS-S3']['FOLDER'] + '/' + brandIdentifier + '/';
                const fileName = creativeToBeDeleted[0].url.substring(s3path.length);
                this.uploadService.deletefile(fileName, brandIdentifier);
            }
            const newCreativeDetails = updatedCreativeHub.creativeDetails.filter(creative => creative._id !== toDeleteBannerId);
            updatedCreativeHub.creativeDetails = newCreativeDetails;
            this.creativeService.updateCreativeHub(creativeHub._id, updatedCreativeHub).subscribe(
              data => {
                console.log('response recieved');
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'success',
                    message: data['message']
                  }
                });
                this.creativeHubIds = [];
                if (updatedCreativeHub.creativeDetails.length === 0) {
                  this.creativeHubIds = [];
                  this.creativeHubIds.push(updatedCreativeHub._id);
                  this.removeCreativeHub();
                } else {
                  this.initCreativeHubsByType(this.creativeType);
                }
              },
              err => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
              }
            );
          });
      } else {
        dialogRef.close();
      }
    });
  }

  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.sDate = new Date(this.sDate.getFullYear(), this.sDate.getMonth(), this.sDate.getDate(), 0, 0, 0);
    //this.endDateMin = new Date(this.startDate);
   // this.startDateMax = new Date(this.saveFilterForm.get('endDate').value);
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    //this.endDateMin = this.startDate;
   // this.endDateMin = this.startDate;
    this.startDateMax = new Date(this.eDate);
    //this.startDateMax = new Date(this.saveFilterForm.get('endDate').value);
  }

}
