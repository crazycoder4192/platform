import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreativeHUBComponent } from './creative-hub.component';

describe('CreativeHUBComponent', () => {
  let component: CreativeHUBComponent;
  let fixture: ComponentFixture<CreativeHUBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreativeHUBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreativeHUBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
