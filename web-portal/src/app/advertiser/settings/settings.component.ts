import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { UserService } from 'src/app/_services/user.service';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public billingAlerts: boolean;
  public campaignMaintainance: boolean;
  public newsLetters: boolean;
  public customizedHelpPerformance: boolean;
  public disapprovedAdsPolicy: boolean;
  public reports: boolean;
  public specialOffers: boolean;
  public userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  public settingType = JSON.parse(localStorage.getItem('settingType'));
  private hasPersonalProfile: boolean = this.authenticationService.currentUserValue.hasPersonalProfile;
  public user;
  hasSecurityQuestions;

  constructor(
    private eventEmitterService: EventEmitterService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private advertiserUserService: AdvertiserUserService,
    private authenticationService: AuthenticationService
  ) {
    localStorage.setItem('activeIcon', JSON.stringify('profile'));
    this.userService.getUserByEmail(this.userEmail).subscribe(
      res => {
        this.hasSecurityQuestions = res.hasSecurityQuestions ? res.hasSecurityQuestions : false;
      }
    );
    this.eventEmitterService.onClickNavigation();
  }

  ngOnInit() {
    this.eventEmitterService.invokeAccountSetting.subscribe((name: string) => {
      this.settingType = JSON.parse(localStorage.getItem('settingType'));
    });
    if (this.hasPersonalProfile) {
      this.getAdvertiserUser();
    }
  }
  getAdvertiserUser() {
    this.advertiserUserService.getAdvertiserUser(this.userEmail).subscribe(
      advertiser => {
        this.billingAlerts = advertiser['notificationSettings'].billingAlerts;
        this.campaignMaintainance = advertiser['notificationSettings'].campaignMaintainance;
        this.newsLetters = advertiser['notificationSettings'].newsLetters;
        this.customizedHelpPerformance = advertiser['notificationSettings'].customizedHelpPerformance;
        this.disapprovedAdsPolicy = advertiser['notificationSettings'].disapprovedAdsPolicy;
        this.reports = advertiser['notificationSettings'].reports;
        this.specialOffers = advertiser['notificationSettings'].specialOffers;
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
    });
  }
  callOnNotificationChange() {
    const notification = {
      billingAlerts: this.billingAlerts,
      campaignMaintainance: this.campaignMaintainance,
      newsLetters: this.newsLetters,
      customizedHelpPerformance: this.customizedHelpPerformance,
      disapprovedAdsPolicy: this.disapprovedAdsPolicy,
      reports: this.reports,
      specialOffers: this.specialOffers,
    };
    this.userService.notificationSettings(notification).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
    });
  }
}
