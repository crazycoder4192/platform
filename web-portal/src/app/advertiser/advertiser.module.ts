import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SlideshowModule } from 'ng-simple-slideshow';
import { FileDropModule } from 'ngx-file-drop';
import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModulesModule } from '../common/common-modules/common-modules.module';
import { ConfirmPasswordDailogComponent } from '../common/confirm-password-dailog/confirm-password-dailog.component';
import { DeleteConfirmPopupComponent } from '../common/delete-confirm-popup/delete-confirm-popup.component';
import { GrdFilterPipe } from '../common/pipes/grid-filter.pipe';
import { AccountCreationComponent } from './account-creation/account-creation.component';
import { AdvertiserProfileThanksComponent } from './advertiser-profile-thanks/advertiser-profile-thanks.component';
import { AdvertiserProfileComponent } from './advertiser-profile/advertiser-profile.component';
import { AdvertiserRoutingModule } from './advertiser-routing.module';
import { AdvertiserComponent } from './advertiser.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { BrandAnalyticsComponent } from './brand-analytics/brand-analytics.component';
import { BrandsPanelComponent } from './brands-panel/brands-panel.component';
import { AddCampaignComponent } from './campaign-creation/add-campaign/add-campaign.component';
import { BannerCropperComponent } from './campaign-creation/banner-cropper/banner-cropper.component';
import { CampaignCreationComponent } from './campaign-creation/campaign-creation.component';
import { CampaignManageComponent } from './campaign-manage/campaign-manage.component';
import { CreativeHubCreationComponent } from './creative-hub-creation/creative-hub-creation.component';
import { CreativeHubSizeDetailsComponent } from './creative-hub-size-details/creative-hub-size-details.component';
import { CreativeHUBComponent } from './creative-hub/creative-hub.component';
import { DashboardActiveCampaignsComponent } from './dashboard/dashboard-active-campaigns/dashboard-active-campaigns.component';
import { DashboardRecentCampaignsComponent } from './dashboard/dashboard-recent-campaigns/dashboard-recent-campaigns.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DraftsComponent } from './drafts/drafts.component';
import { CreditLineComponent } from './payments/credit-line/credit-line.component';
import { InvoicesComponent } from './payments/invoices/invoices.component';
import { PaymentFailureComponent } from './payments/payment-failure/payment-failure.component';
import { PaymentMethodsComponent } from './payments/payment-methods/payment-methods.component';
import { PaymentSuccessComponent } from './payments/payment-success/payment-success.component';
import { PaymentsComponent } from './payments/payments.component';
import { ProfilePanelComponent } from './profile-panel/profile-panel.component';
import { SettingsComponent } from './settings/settings.component';
import { UploadCampaignCreativePopupComponent } from './upload-campaign-creative-popup/upload-campaign-creative-popup.component';
import { AskAQuestionComponent } from './ask-a-question/ask-a-question.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { AdvertiserSupportComponent } from './advertiser-support/advertiser-support.component';
import { AdvertiserTicketDetailsComponent } from './advertiser-support/advertiser-ticket-details/advertiser-ticket-details.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import config from 'appconfig.json';
import { FlexLayoutModule } from "@angular/flex-layout";
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { AddSpacePipe } from '../common/pipes/add-space.pipe';
import { InvitationCodeComponent } from './invitation-code/invitation-code.component';
import { AdvUserTermsAndConditionsComponent } from './adv-user-terms-and-conditions/adv-user-terms-and-conditions.component';
import { AudienceTargetComponent } from './audience-target/audience-target.component';
import { PaymentSecureContentComponent } from './payments/payment-secure-content/payment-secure-content.component';

@NgModule({
  providers: [GoogleMapsAPIWrapper],
  declarations: [AdvertiserComponent, CampaignCreationComponent, CampaignManageComponent, DraftsComponent,
    CreativeHUBComponent, AnalyticsComponent, PaymentsComponent, AccountCreationComponent, AdvertiserProfileComponent,
    SettingsComponent, AdvertiserProfileThanksComponent, AddCampaignComponent,
    BannerCropperComponent, CreativeHubCreationComponent, CreativeHubSizeDetailsComponent,
    GrdFilterPipe,
    PaymentSuccessComponent,
    PaymentFailureComponent,
    BrandsPanelComponent,
    PaymentMethodsComponent,
    InvoicesComponent,
    CreditLineComponent,
    BrandAnalyticsComponent,
    DashboardComponent,
    DashboardActiveCampaignsComponent,
    DashboardRecentCampaignsComponent,
    ProfilePanelComponent,
    UploadCampaignCreativePopupComponent,
    AskAQuestionComponent,
    DashboardDetailsComponent,
    AdvertiserSupportComponent,
    AdvertiserTicketDetailsComponent,
    AddSpacePipe,
    InvitationCodeComponent,
    AudienceTargetComponent,
    PaymentSecureContentComponent
  ],
  imports: [
    CommonModule,
    AdvertiserRoutingModule,
    CommonModulesModule,
    FormsModule,
    FileDropModule,
    SlideshowModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    NgxPaginationModule,
    TooltipModule,
    FlexLayoutModule,
    SlickCarouselModule,
    AgmCoreModule.forRoot({
      apiKey: config['AGM']['KEY']
    })
  ],
  entryComponents: [
    AddCampaignComponent,
    BannerCropperComponent,
    CreativeHubCreationComponent,
    DeleteConfirmPopupComponent,
    CreativeHubSizeDetailsComponent,
    ConfirmPasswordDailogComponent,
    UploadCampaignCreativePopupComponent,
    AdvUserTermsAndConditionsComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AdvertiserModule { }
