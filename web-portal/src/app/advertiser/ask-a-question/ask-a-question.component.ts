import { Component, OnInit, HostListener, Output, EventEmitter, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ticket } from 'src/app/_models/ticket_model';
import { SupportService } from 'src/app/_services/admin/support.service';
import { MatSnackBar } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-advertiser-ask-a-question',
  templateUrl: './ask-a-question.component.html',
  styleUrls: ['./ask-a-question.component.scss']
})
export class AskAQuestionComponent implements OnInit {
  clickState = false;
  // private useremail: string = this.authenticationService.currentUserValue.email;
  public questionForm: FormGroup;
  @Output() private toggleAskAQuestion = new EventEmitter<boolean>();
  @Input() useremail: string;
  reasons: any;
  filteredReasons: any;
  tickets: Ticket[];
  rolePermissions: any;
  article: Object;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private supportService: SupportService,
    private snackBar: MatSnackBar,
    private router: Router,
    private eventEmitterService: EventEmitterService,
    private spinner: SpinnerService
  ) {

  }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.createQuestionForm();
    this.getArticles();
  }
  createQuestionForm() {
    this.questionForm = this.formBuilder.group({
      useremail: [this.useremail, Validators.required],
      reason: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  @HostListener('click')
  clickInside() {
    this.clickState = false;
  }

  @HostListener('document:click')
  clickout() {
    if (this.clickState) {
      this.toggleAskAQuestion.emit(false);
    }
    this.clickState = true;
  }
  createTicket() {
    if (!this.validateCreateTicket()) {
      return;
    } else {
      this.spinner.showSpinner.next(true);
      const ticket = new Ticket(
        this.useremail,
        '',
        null,
        this.questionForm.controls['reason'].value,
        this.questionForm.controls['description'].value,
        'New',
        '',
        '',
        [{
          commentTime: new Date(),
          commentBy: this.useremail,
          comment: this.questionForm.controls['description'].value
        }],
        {
          email: '',
          at: null
        },
        [{
          email: '',
          at: null
        }]
      );
      this.supportService.addTicket(ticket).subscribe(
        resposne => {
          this.spinner.showSpinner.next(false);
          this.questionForm.controls['description'].setValue('');
          this.questionForm.controls['reason'].setValue('');
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: resposne['message']
            }
          });
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: 'Unable to register your request. Please try again'
            }
          });
          console.log('Error! createTicket.supportService.addTicket : ' + err);
      });
    }
  }
  validateCreateTicket(): boolean {
    const formGroup = this.questionForm;

    const emailCtrl = formGroup.controls['useremail'];
    const reasonCtrl = formGroup.controls['reason'];
    const descriptionCtrl = formGroup.controls['description'];

    let isValid = true;
    if (!emailCtrl.value) {
      emailCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!reasonCtrl.value) {
      reasonCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!descriptionCtrl.value) {
      descriptionCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }
  gotoServiceRequests() {
    localStorage.setItem('activeIcon', JSON.stringify('question'));
    this.eventEmitterService.onClickNavigation();
    this.toggleAskAQuestion.emit(false);
    this.router.navigate(['/advertiser/advertiserSupport']);
  }
  getArticles() {
    this.supportService.getSupportArticles().subscribe(
      resposne => {
        this.article = resposne;
      }, err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
    });
  }
  goToArticle(link) {
    window.open(link);
  }

}
