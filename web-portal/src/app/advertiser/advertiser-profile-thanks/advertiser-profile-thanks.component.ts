import { Component, OnInit } from '@angular/core';
import { element } from '@angular/core/src/render3';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-advertiser-profile-thanks',
  templateUrl: './advertiser-profile-thanks.component.html',
  styleUrls: ['./advertiser-profile-thanks.component.scss']
})
export class AdvertiserProfileThanksComponent implements OnInit {


  constructor(private amplitudeService: AmplitudeService) { }

  ngOnInit() {
  }

  getStartedClick() {
    let eventProperties = {
      type: JSON.parse(localStorage.getItem('currentUser'))['usertype']
    };
    this.amplitudeService.logEvent('profile_creation - user clicks on get started', eventProperties);
  }
}
