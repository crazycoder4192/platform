import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserProfileThanksComponent } from './advertiser-profile-thanks.component';

describe('AdvertiserProfileThanksComponent', () => {
  let component: AdvertiserProfileThanksComponent;
  let fixture: ComponentFixture<AdvertiserProfileThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserProfileThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserProfileThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
