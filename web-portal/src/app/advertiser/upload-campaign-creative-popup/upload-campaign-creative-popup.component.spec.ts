import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCampaignCreativePopupComponent } from './upload-campaign-creative-popup.component';

describe('UploadCampaignCreativePopupComponent', () => {
  let component: UploadCampaignCreativePopupComponent;
  let fixture: ComponentFixture<UploadCampaignCreativePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCampaignCreativePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCampaignCreativePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
