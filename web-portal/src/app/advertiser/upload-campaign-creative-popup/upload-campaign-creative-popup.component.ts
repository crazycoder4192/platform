import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CreativeHubService } from 'src/app/_services/advertiser/creative-hub.service';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-upload-campaign-creative-popup',
  templateUrl: './upload-campaign-creative-popup.component.html',
  styleUrls: ['./upload-campaign-creative-popup.component.scss']
})
export class UploadCampaignCreativePopupComponent implements OnInit {
  creativeDetails: any;
  creativeHubs: any;
  creativeType: string;

  constructor(
    private dialogRef: MatDialogRef<UploadCampaignCreativePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public creativeData: any,
    public creativeService: CreativeHubService,
    private spinner: SpinnerService
  ) {
    this.creativeDetails = this.creativeData.creativeDetails;
    this.creativeType = this.creativeData.creativeType;
  }

  ngOnInit() {
    if (this.creativeType === 'video') {
      this.getVideoCreatives();
    }
  }
  getBannerCreatives () {
      this.creativeService.getAllCreativeHubsByBannerSize(this.creativeDetails.size).subscribe(
        res => {
          this.creativeHubs = res;
        },
        err => {
          console.log('Error! getAllCreativeHubsByBannerSize : ' + err);
    });
  }
  getVideoCreatives () {
      this.creativeService.getAllVideoCreativeHubsByAdvertiser().subscribe(
        res => {
          this.creativeHubs = res;
        },
        err => {
          console.log('Error! getAllVideoCreativeHubsByAdvertiser : ' + err);
    });
  }
  selectCreative (creativeHub: any) {
    this.dialogRef.close({isClose: false, isFromLocal: false, creative: creativeHub});
  }
  cancelFromLocal () {
    this.dialogRef.close({isClose: false, isFromLocal: true, creative: ''});
  }
  cancelDailog () {
    this.dialogRef.close({isClose: true, isFromLocal: false, creative: ''});
  }

}
