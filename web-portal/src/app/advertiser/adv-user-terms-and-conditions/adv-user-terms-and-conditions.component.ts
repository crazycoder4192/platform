import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-adv-user-terms-and-conditions',
  templateUrl: './adv-user-terms-and-conditions.component.html',
  styleUrls: ['./adv-user-terms-and-conditions.component.scss']
})
export class AdvUserTermsAndConditionsComponent implements OnInit {

  public title: string;
  constructor(
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public entity: any
  ) { }

  ngOnInit() {
    this.title = 'Terms and Conditions';
  }
  onClose() {
    this.dialog.closeAll();
  }

}
