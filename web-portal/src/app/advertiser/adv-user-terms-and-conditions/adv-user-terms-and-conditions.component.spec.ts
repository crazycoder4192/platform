import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvUserTermsAndConditionsComponent } from './adv-user-terms-and-conditions.component';

describe('AdvUserTermsAndConditionsComponent', () => {
  let component: AdvUserTermsAndConditionsComponent;
  let fixture: ComponentFixture<AdvUserTermsAndConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvUserTermsAndConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvUserTermsAndConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
