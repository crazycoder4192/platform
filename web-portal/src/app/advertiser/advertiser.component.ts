import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { EventEmitterService } from '../_services/event-emitter.service';
import { Advertiser } from '../_models/advertiser_model';
import { AdvertiserProfileService } from '../_services/advertiser/advertiser-profile.service';
import { AdvertiserUserService } from '../_services/advertiser/advertiser-user.service';
import { NotificationsService } from '../_services/notifications.service';
import { SpinnerService } from '../_services/spinner.service';
import { Location } from '@angular/common';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../common/format-datepicker/format-datepicker';
import { advertiserSlideInAnimation } from './advertiser-route-animation';

@Component({
  selector: 'app-advertiser',
  templateUrl: './advertiser.component.html',
  styleUrls: ['./advertiser.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter } , 
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ],
  animations: [advertiserSlideInAnimation]
})
export class AdvertiserComponent implements OnInit {
  private useremail: string = this.authenticationService.currentUserValue.email;
  public userPermissions: any;
  userRole = '';
  showBrandPanel = false;
  activeIcon: string;
  advertiserUser: Object;
  showProfilePanel: boolean;
  activeProfileIcon: boolean;
  showquestion = false;
  showProfileInfo: boolean;
  showBrandInfo: boolean;
  showQuestionInfo: boolean;
  showQuestion: boolean;
  showProfile: boolean;
  showBrand: boolean;
  showPaymentInfo: boolean;
  showNotificationInfo: boolean;
  hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
  rolePermissions: any;
  showNotification: boolean;
  unreadNotificationCount: any;
  immediateChildRoute: string;
  marketSelection: string;

  constructor(
    private router: Router,
    private eventEmitterService: EventEmitterService,
    private authenticationService: AuthenticationService,
    private advertiserProfileService: AdvertiserProfileService,
    private advertiserUserService: AdvertiserUserService,
    private notificationService: NotificationsService,
    private spinner: SpinnerService,
    private location: Location
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
      this.spinner.showSpinner.next(false);
      this.setPermissions();
      this.router.events.subscribe(event => {
        if (event.constructor.name === 'NavigationStart') {
          let routerURL: string = event["url"];
          this.immediateChildRoute = routerURL.split('/')[2];
        }
      });
  }
  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const visitedRoute = JSON.parse(localStorage.getItem('visitRoute'));
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    if (this.hasPersonalProfile) {
      this.getUnreadNotificationCount();
    }
    this.eventEmitterService.invokeSidebarComponentFunction.subscribe((name: string) => {
      this.hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
        this.setPermissions();
    });
    this.eventEmitterService.invokeTopBarIconFunction.subscribe((name: string) => {
      this.hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
       this.setIconActivation();
    });
    this.eventEmitterService.invokeNotificationBadgeFunction.subscribe((name: string) => {
       this.getUnreadNotificationCount();
    });

    if (this.hasPersonalProfile && localStorage.getItem('currentRoute') && !visitedRoute.advertiser) {
      this.router.navigate([localStorage.getItem('currentRoute')]);
      visitedRoute.advertiser = true;
      localStorage.setItem('visitRoute', JSON.stringify(visitedRoute));
      // localStorage.removeItem('currentRoute');
    } else if (this.hasPersonalProfile && !currentUser.visitedDashboard && !localStorage.getItem('currentRoute')) {
      this.router.navigate(['/advertiser/dashboard']);
      currentUser['visitedDashboard'] = true;
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
    } else if (this.hasPersonalProfile && (currentUser.visitedDashboard || (visitedRoute && visitedRoute.advertiser))) {
      this.router.navigate([this.location.path()]);
    } else {
      this.router.navigate(['/advertiser/advertiserProfile']);
    }
  }
  initRolesAndPermissions() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
  }
  setPermissions() {
    this.advertiserUserService.getAdvertiserUserPermissions(this.useremail).subscribe(
      permissions => {
        this.userPermissions = permissions['userPermissions'];
        this.userRole = permissions['userRole'];
        if (this.hasPersonalProfile && permissions['zone'] &&
        (localStorage.getItem('zone') === 'null' || localStorage.getItem('zone') === 'undefined'
        || permissions['zone'] !== localStorage.getItem('zone'))) {
          this.router.navigate(['/advertiser/advertiserProfile']);
          setTimeout(() => {
            localStorage.setItem('zone', permissions['zone']);
            this.marketSelection = localStorage.getItem('zone');
            this.router.navigate(['/advertiser/dashboard']);
          }, 10);
        }

        if (this.userRole !== '') {
          localStorage.setItem('userRole', JSON.stringify(this.userRole));
          localStorage.setItem('roleAndPermissions', JSON.stringify(this.userPermissions));
          this.initRolesAndPermissions();
        }
      }
    );
  }
  toggleBrandPanel() {
    this.showBrand = true;
    this.showProfile = false;
    this.showNotification = false;
    this.showQuestion = false;
  }
  toggleProfilePanel() {
    this.showBrand = false;
    this.showProfile = true;
    this.showNotification = false;
    this.showQuestion = false;
  }
  toggleAskAQuestion() {
    this.showBrand = false;
    this.showProfile = false;
    this.showNotification = false;
    this.showQuestion = true;
  }
  gotoPayments() {
    this.showPaymentInfo = true;
    this.router.navigate(['/advertiser/payments']);
  }
  toggleNotification() {
    this.showBrand = false;
    this.showProfile = false;
    this.showQuestion = false;
    this.showNotification = true;
    const preActiveIcon = JSON.parse(localStorage.getItem('activeIcon'));
    localStorage.setItem('preActiveIcon', JSON.stringify(preActiveIcon));
    localStorage.setItem('activeIcon', JSON.stringify('notifications'));
    this.setIconActivation();
  }
  setIconActivation() {
    if (JSON.parse(localStorage.getItem('activeIcon')) === 'question') {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showBrandInfo = false;
      this.showQuestionInfo = true;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'profile') {
      this.showPaymentInfo = false;
      this.showProfileInfo = true;
      this.showBrandInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'brand') {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showBrandInfo = true;
      this.showQuestionInfo = false;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'payment') {
      this.showPaymentInfo = true;
      this.showProfileInfo = false;
      this.showBrandInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'notifications') {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showBrandInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = true;
    } else {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showBrandInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = false;
    }
  }
  getUnreadNotificationCount() {
    this.notificationService.getUnreadNotificationCount().subscribe(
      res => {
        this.unreadNotificationCount = res;
      },
      err => {
      console.log('Error! getNotifications : ' + err);
    });
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }
}
