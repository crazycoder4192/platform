import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/_services/utils/util.service';
import { Observable } from 'rxjs/internal/Observable';
import { FormGroup, FormBuilder } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { AdvertiserUtilService } from 'src/app/_services/advertiser/advertiser-util.service';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { Advertiser } from 'src/app/_models/advertiser_model';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';
import { Router } from '@angular/router';
import { stringList } from 'aws-sdk/clients/datapipeline';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { MatSnackBar, MatDialog, MatAutocompleteSelectedEvent } from '@angular/material';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { USEROLESElement } from 'src/app/admin/configuration/dataModel/userRolesPermissionsModel';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { ConfirmPasswordDailogComponent } from 'src/app/common/confirm-password-dailog/confirm-password-dailog.component';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { ClientBrandMetadataService } from 'src/app/_services/advertiser/client-brand-metadata.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';


@Component({
  selector: 'app-account-creation',
  templateUrl: './account-creation.component.html',
  styleUrls: ['./account-creation.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AccountCreationComponent implements OnInit {

  public uniqeTherapeuticTypes: string[];
  public filteredTherapeuticAreas: Observable<string[]>;
  public addAccountForm: FormGroup;
  public addUserForm: FormGroup;
  public typeOfOrganizations: string[];
  public filteredTypeOfOrganizations: Observable<string[]>;
  public uniqeClients: string[];
  public advertiserBrandsDataSource: Brand[];
  public filteredClients: Observable<string[]>;
  private advertiserBrand: Brand;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  public advertiserProfile: Advertiser;
  public displayedColumns: string[] = ['brand', 'typeOfOrganization', 'therapeuticArea', 'market', 'action', 'details'];
  public actionState: string;
  brandId: any;
  advertiserUser: any;
  selectedBrands: any;
  selectedBrand: Brand;
  filteredUserClients: Observable<any>;
  filteredUserBrands: Observable<any>;
  public listOfExistedUserRolesPermissions: Array<USEROLESElement>;
  public filteredUserRolesPermissions: Observable<USEROLESElement[]>;
  adertiserUsers: any;
  public errMsg = '';
  selectedClientBrands: Brand[];
  userDeleteMsg: string;
  permissions: any;
  userPermissions: any;
  userRole = JSON.parse(localStorage.getItem('userRole'));
  changedBrand: any;
  isAdminBrand: any;
  advertiserUserDetails: Object;
  advertiser: any;
  userRoleFromBrandPanel: any;
  users: any;
  client: any;
  user: any;
  filterdAdvertiserBrandsDataSource: Brand[];
  brand: any;
  adertiserUserList: any;
  private isBrandNameAvailable = false;
  filterBrandClients: string[];
  isClientTypeReadOnly: boolean;
  rolePermissions: string[];
  clientList: any;
  brandList: any;
  filteredBrands: any;
  activeIcon: any;
  zone = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  clientBrandList: any;
  marketSelection = '1';
  showMarketColumn = 'table-cell';
  constructor(
    private utilService: UtilService,
    private formBuilder: FormBuilder,
    private advertiserUtilService: AdvertiserUtilService,
    private advertiserBrandService: AdvertiserBrandService,
    private advertiserProfileService: AdvertiserProfileService,
    private router: Router,
    private snackBar: MatSnackBar,
    private advertiserUserService: AdvertiserUserService,
    private configHTTPService: ConfigurationService,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private brandService: AdvertiserBrandService,
    private eventEmitterService: EventEmitterService,
    private clientBrandMetadataService: ClientBrandMetadataService
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
  }
  ngOnInit() {
    this.activeIcon = JSON.parse(localStorage.getItem('activeIcon'));
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.createAddAcountForm();
    this.createAddUserForm();
    this.initUniqueTherapeuticTypes();
    this.initAdvertiserAppTypes();
    if (this.activeIcon !== 'notifications') {
      this.actionState = 'addBrand';
      this.initClients();
    } else if (this.activeIcon === 'notifications') {
      this.actionState = 'addUser';
      this.getAllAdvertiserUsers();
    }
    this.adminRolesAndPermissions();
    this.getAdvertiserUserByEmail();
  }
  getAdvertiserUserByEmail(): any {
    this.advertiserUserService.getAdvertiserUser(this.userEmail).subscribe(
      res => {
        if ( res ) {
          this.advertiserUserDetails = res;
          this.advertiserProfile = ( res && res['advertiserDetails'] && res['advertiserDetails'].length > 0 ) ?
                                    this.advertiserUserDetails['advertiserDetails'][0] : null;
        }
      }
    );
  }
  initUniqueTherapeuticTypes() {
    this.utilService.getUniqueTherapeuticTypes().subscribe(
      res => {
        this.uniqeTherapeuticTypes = res;
        this.filteredTherapeuticAreas = this.addAccountForm.controls['therapeuticArea'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterTherapeuticType(name) : this.uniqeTherapeuticTypes.slice())
          );
      },
      err => {
        console.log(err);
      }
    );
  }

  initAdvertiserAppTypes() {
    this.brandService.getAllZonesByAdvertiserId().subscribe(
      result => {
        if (result && result.length > 0) {
          // Get total no of zones
          this.showMarketColumn = ([...new Set(result.map((x: any) => x.zone))].length > 1 ? 'table-cell' : 'none');
        }
    },
      error => {

      });
    this.advertiserUtilService.getAdvertiserAppTypes().subscribe(
      res => {
        this.typeOfOrganizations = res;
        this.filteredTypeOfOrganizations = this.addAccountForm.get('typeOfOrganization').valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterOrganization(name) : this.typeOfOrganizations.slice())
          );
      },
      err => {
        console.log(err);
      }
    );
  }
  initClients() {
    this.advertiserBrandService.getClientWisebrandsDetails().subscribe(
      res => {
        this.advertiserBrandsDataSource = res;
        if (res && res.length > 0) {
        this.advertiserBrandsDataSource = res.sort((a, b) => (a.clientName > b.clientName) ? 1 : -1);
        }
        this.filterdAdvertiserBrandsDataSource = this.advertiserBrandsDataSource;
        if (this.advertiserBrandsDataSource.length === 1) {
          this.selectedClientBrands = this.advertiserBrandsDataSource[0]['brands'];
        }
        this.uniqeClients = Array.from(new Set(this.advertiserBrandsDataSource.map((item: any) => item.clientName)));
        this.getClientsMetadata();
          this.selectedBrands = [];
          this.filteredUserClients = this.addUserForm.controls['client'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterUserClient(name) : this.uniqeClients.slice())
          );
          this.filterBrandClients = Array.from(new Set(this.advertiserBrandsDataSource.map((item: any) => item.clientName)));
          if (this.filterBrandClients.length) {
            this.filterBrandClients.unshift('All');
          }
          this.users = [];
          this.advertiserBrandsDataSource.forEach(element => {
            element['brands'].forEach(thisBrand => {
              thisBrand['filteredUsers'] = thisBrand.userList;
              thisBrand.userList.forEach(thisUser => {
                let userFullName: string;
                if (thisUser.firstName || thisUser.lastName) {
                  userFullName = thisUser.firstName + ' ' + thisUser.lastName;
                } else {
                  userFullName = thisUser.email;
                }
                this.users.push({
                  fullName: userFullName,
                  email: thisUser.email
                });
              });
            });
          });
          this.users = this.getUnique(this.users, 'fullName');
          if (this.users.length) {
            this.users.unshift({
              fullName: 'All',
              email: 'all_users'
            });
          }
          this.filterdAdvertiserBrandsDataSource = this.advertiserBrandsDataSource;
          this.filterdAdvertiserBrandsDataSource.forEach(element => {
            element['filteredBrands'] = element['brands'];
          });
      },
      err => {
        console.log(err);
    });
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }
  getClientsMetadata() {
    this.clientBrandMetadataService.getClientsMetadata(this.addAccountForm.get('marketSel').value).subscribe(
      res => {
        this.clientList = res;
        this.filteredClients = this.addAccountForm.controls['client'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterClient(name))
          );
          if (this.advertiserProfile && this.advertiserProfile.advertiserType === 'Client') {
            const clientName = this.advertiserBrandsDataSource[0]['clientName'];
            const findClientDetails = this.clientList.find(obj => obj.client === clientName);
            if (findClientDetails) {
              this.getBrandsMetadata(findClientDetails._id);
            }
          }
      },
      err => {
        console.log('Error!');
    });
  }

  onSelectedMarketPlace() {
    this.getClientsMetadata();
    this.addAccountForm.get('client').setValue(null);
    this.addAccountForm.get('typeOfOrganization').setValue(null);
    this.addAccountForm.get('brand').setValue(null);
  }
  getBrandsMetadata(clientId) {
    this.addAccountForm.get('brand').setValue('');
    this.clientBrandMetadataService.getBrandsMetadata(clientId, this.addAccountForm.get('marketSel').value).subscribe(
      res => {
        this.clientBrandList = res;
        this.brandList = this.clientBrandList.map((item => item.brand));
        this.filteredBrands = this.addAccountForm.controls['brand'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterBrand(name))
          );
      },
      err => {
        console.log('Error!');
    });
  }

  getTherapeuticData(event: MatAutocompleteSelectedEvent) {
    const zone  = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    if (zone === '2') {
      const therapyName = this.clientBrandList.filter((item) => item.brand === event.option.value)[0].therapyName;
      if (therapyName) { this.addAccountForm.get('therapeuticArea').setValue(therapyName); }
    } else { return null; }
  }

  private _filterClient(value: string): string[] {
    let filterValue: string;
    if (value) {
      filterValue = value.toLowerCase();
    }
    const filteredClients = this.clientList.filter(
      item =>
      item.client.toLowerCase().indexOf(filterValue) === 0);
      // for client type
      const clientName = this.addAccountForm.get('client').value;
      const selectedClientForType = this.advertiserBrandsDataSource.find(obj => obj.clientName === clientName);
      if (selectedClientForType) {
        this.isClientTypeReadOnly = true;
        this.addAccountForm.get('typeOfOrganization').setValue(selectedClientForType.clientType);
      } else {
        this.isClientTypeReadOnly = false;
      }
      // end
      return filteredClients;
  }
  private _filterBrand(value: string): string[] {
    if (value) {
        const filterValue = value.toLowerCase();
        return this.brandList.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }
  }
  _filterUserClient(name: string): any {
    // for brands
    this.addUserForm.get('brand').setValue('');
    const client = this.addUserForm.get('client').value;
    const selectedClient = this.advertiserBrandsDataSource.find(obj => obj.clientName === client);
    if (selectedClient) {
      this.selectedBrands = selectedClient['brands'];
    }
    // end

    // for client type
    const clientName = this.addAccountForm.get('client').value;
    const selectedClientForType = this.advertiserBrandsDataSource.find(obj => obj.clientName === clientName);
    if (selectedClientForType) {
      this.isClientTypeReadOnly = true;
      this.addAccountForm.get('typeOfOrganization').setValue(selectedClientForType.clientType);
    } else {
      this.isClientTypeReadOnly = false;
    }
    // end

    const filterValue = name.toLowerCase();
    return this.uniqeClients.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  _filterOrganization(name: string): any {
    const filterValue = name.toLowerCase();
    return this.typeOfOrganizations.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  _filterTherapeuticType(name: string): any {
    const filterValue = name.toLowerCase();
    return this.uniqeTherapeuticTypes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  createAddAcountForm() {
    this.addAccountForm = this.formBuilder.group({
      client: [''],
      typeOfOrganization: [''],
      brand: [''],
      therapeuticArea: [''],
      marketSel: [''],
    });
  }
  createAddUserForm() {
    this.addUserForm = this.formBuilder.group({
      email: [''],
      role: [''],
      client: [''],
      brand: ['']
    });
  }
  validateAddAccount(): boolean {
    const formGroup = this.addAccountForm;
    const clientCtrl = formGroup.controls['client'];
    const typeOfOrganizationCtrl = formGroup.controls['typeOfOrganization'];
    const brandCtrl = formGroup.controls['brand'];
    const therapeuticAreaCtrl = formGroup.controls['therapeuticArea'];

    let isValid = true;
    if (this.advertiserProfile.advertiserType !== 'Client' && !clientCtrl.value) {
      clientCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (this.advertiserProfile.advertiserType !== 'Client' && !typeOfOrganizationCtrl.value) {
      typeOfOrganizationCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!brandCtrl.value) {
      brandCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!therapeuticAreaCtrl.value) {
      therapeuticAreaCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }
  addAccount() {
    this.advertiserBrand = new Brand(
      '',
      this.addAccountForm.get('brand').value,
      this.addAccountForm.get('therapeuticArea').value,
      this.addAccountForm.get('client').value,
      this.addAccountForm.get('typeOfOrganization').value,
      this.advertiserProfile._id,
      this.userEmail,
      {}
    );
    if (this.advertiserProfile.advertiserType === 'Client') {
      this.advertiserBrand.clientName = this.advertiserProfile.businessName;
      this.advertiserBrand.clientType = this.advertiserProfile.organizationType;
    }
    if (this.validateAddAccount() && this.isBrandNameAvailable) {
      this.spinner.showSpinner.next(true);
      this.advertiserBrandService.saveAdvertiserBrand(this.advertiserBrand, this.addAccountForm.get('marketSel').value).subscribe(
        res => {
          const adminPermission = this.listOfExistedUserRolesPermissions.find((thisItem) =>
          thisItem.roleName.toLowerCase().trim() === 'Admin'.toLowerCase().trim());
          const advertiserUser = {
            email: this.userEmail,
            roleName: adminPermission.roleName,
            roleId: adminPermission._id,
            userPermissions: adminPermission.permissions,
            brand: res.brandName,
            brandId: res._id,
            advertiserId: this.advertiserProfile._id
          };
          this.advertiserUserService.addAdvertiserUser(advertiserUser, this.addAccountForm.get('marketSel').value).subscribe(
            advertiseUserRes => {
              this.spinner.showSpinner.next(false);
              res.status = true;
              this.addAccountForm.get('brand').setValue('');
              this.addAccountForm.get('therapeuticArea').setValue('');
              this.addAccountForm.get('client').setValue('');
              this.addAccountForm.get('typeOfOrganization').setValue('');
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: res.brandName + ' added successfully'
                }
              });
              this.brandService.getAllZonesByAdvertiserId().subscribe(
                result => {
                  if (result && result.length > 0) {
                    // Get total no of zones
                    this.showMarketColumn = ([...new Set(result.map((x: any) => x.zone))].length > 1 ? 'table-cell' : 'none');
                  }
              });
              this.initClients();
            },
            error => {
              console.log('Error!');
              this.spinner.showSpinner.next(false);
          });
        },
        error => {
          this.spinner.showSpinner.next(false);
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 2500,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: error.erroe['message']
            }
          });
      });
    }
  }

  deactivateBrand(brandId) {
    this.advertiserBrandService.deactivateAdvertiserBrand(brandId).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
        this.initClients();
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
    });
  }
  cancelEditBrand() {
    this.actionState = 'addBrand';
    this.addAccountForm.get('brand').setValue('');
    this.addAccountForm.get('therapeuticArea').setValue('');
    this.addAccountForm.get('client').setValue('');
    this.addAccountForm.get('typeOfOrganization').setValue('');
  }
  editBrand(clientName, brandDetails) {
    this.actionState = 'editBrand';
    this.brandId = brandDetails._id;
    if (this.advertiserProfile.advertiserType === 'Client') {
      this.addAccountForm.get('brand').setValue(brandDetails.brandName);
      this.addAccountForm.get('therapeuticArea').setValue(brandDetails.brandType);
    } else {
      this.addAccountForm.get('brand').setValue(brandDetails.brandName);
      this.addAccountForm.get('therapeuticArea').setValue(brandDetails.brandType);
      this.addAccountForm.get('client').setValue(clientName);
      this.addAccountForm.get('typeOfOrganization').setValue(brandDetails.clientType);
    }
  }
  updateBrand() {
    this.advertiserBrand = new Brand(
      this.brandId,
      this.addAccountForm.get('brand').value,
      this.addAccountForm.get('therapeuticArea').value,
      this.addAccountForm.get('client').value,
      this.addAccountForm.get('typeOfOrganization').value,
      this.advertiserProfile._id,
      this.userEmail,
      {}
    );
    if (this.advertiserProfile.advertiserType === 'Client') {
      this.advertiserBrand.clientName = this.advertiserProfile.businessName;
      this.advertiserBrand.clientType = this.advertiserProfile.organizationType;
    }
    if (this.validateAddAccount()) {
      this.advertiserBrandService.updateAdvertiserBrand(this.advertiserBrand).subscribe(
        res => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: res['message']
            }
          });
          this.cancelEditBrand();
          this.initClients();
        },
        error => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: error.error['message']
            }
          });
      });
    }
  }
  changeTab (type: string) {
    if (type === 'accounts') {
      this.actionState = 'addBrand';
      this.initClients();
    } else {
      this.actionState = 'addUser';
      this.getAllAdvertiserUsers();
    }
  }
  adminRolesAndPermissions() {
    this.configHTTPService.getalluserolesByModuleType('Advertiser').subscribe(
      res => {
        if (res) {
          this.listOfExistedUserRolesPermissions = <USEROLESElement[]>res;
          this.filteredUserRolesPermissions = this.addUserForm.get('role').valueChanges
          .pipe(
            startWith<string | USEROLESElement>(''),
            map(value => typeof value === 'string' ? value : value.roleName),
            map(userrolepermissions => userrolepermissions ?
              this._filterUserRolesPermissions(userrolepermissions) : this.listOfExistedUserRolesPermissions.slice())
          );
        }
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }

  private _filterUserRolesPermissions(value: string): USEROLESElement[] {
    const filterValue = value.toLowerCase();
    const filteredRolesPermissions = this.listOfExistedUserRolesPermissions.filter(
      userrolepermissions =>
      userrolepermissions.roleName.toLowerCase().indexOf(filterValue) === 0);
    return filteredRolesPermissions;
  }

  validateAddUser(): boolean {
    const formGroup = this.addUserForm;
    const emailCtrl = formGroup.controls['email'];
    const roleCtrl = formGroup.controls['role'];
    const brandCtrl = formGroup.controls['brand'];
    const isUserExist = this.adertiserUsers.find(obj => obj.email === emailCtrl.value);
    let isValid = true;
    // if (this.userEmail === emailCtrl.value) {
    //   this.errMsg = 'You are an admin, Please add brands from accounts tab !';
    //   isValid = false;
    // }
    if (isUserExist) {
      const isBrandExist = isUserExist.brands.find(obj => obj.brandName === brandCtrl.value);
      if (isBrandExist) {
        this.errMsg = `${brandCtrl.value} already exist for user ${emailCtrl.value} !`;
        isValid = false;
      }
    }
    if (!emailCtrl.value) {
      emailCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!roleCtrl.value) {
      roleCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!brandCtrl.value) {
      brandCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }

  checkBrandNameAvailablity() {
    const formGroup = this.addAccountForm;
    const brandCtrl = formGroup.controls['brand'];
    const value = brandCtrl.value.trim();
    if (value && value.length > 2) {
    this.advertiserBrandService.brandNameAvailability(value, this.advertiserProfile._id).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        brandCtrl.clearValidators();
        this.isBrandNameAvailable = true;
      },
      err => {
        this.spinner.showSpinner.next(false);
        brandCtrl.setErrors({ nameNotAvailable: true });
        this.isBrandNameAvailable = false;
      }
    );

    } else if (value && value.length <= 2) {
      brandCtrl.setErrors({ lessLength: true });
    }
  }

  addUser() {
    this.userDeleteMsg = '';
    this.errMsg = '';
    if (this.validateAddUser()) {
      if (this.advertiserProfile.advertiserType === 'Client') {
        this.selectedBrand = this.selectedClientBrands.find(obj =>
          obj.brandName === this.addUserForm.get('brand').value);
      } else {
        this.selectedBrand = this.selectedBrands.find(obj => obj.brandName === this.addUserForm.get('brand').value);
      }
      const selectedUserRoleAndPermissions = this.listOfExistedUserRolesPermissions.find(obj =>
        obj.roleName.toLowerCase().trim() === this.addUserForm.get('role').value.toLowerCase().trim()
      );
      this.advertiserUser = {
        email: this.addUserForm.get('email').value.toLowerCase(),
        roleName: selectedUserRoleAndPermissions.roleName,
        roleId: selectedUserRoleAndPermissions._id,
        userPermissions: selectedUserRoleAndPermissions.permissions,
        brand: this.selectedBrand.brandName,
        brandId: this.selectedBrand['brandId'],
        advertiserId: this.advertiserProfile._id
      };
      this.spinner.showSpinner.next(true);
      this.advertiserUserService.addAdvertiserUser(this.advertiserUser, this.selectedBrand['zone']).subscribe(
        res => {
          this.getAllAdvertiserUsers();
          this.addUserForm.get('email').setValue('');
          this.addUserForm.get('role').setValue('');
          this.addUserForm.get('client').setValue('');
          this.addUserForm.get('brand').setValue('');
          this.spinner.showSpinner.next(false);
          if (res['success']) {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: res['message']
              }
            });
          } else {
            this.errMsg = res['message'];
          }
          this.initClients();
        },
        error => {
          this.spinner.showSpinner.next(false);
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: error.error['message']
            }
          });
      });
    }
  }
  getAllAdvertiserUsers() {
    this.advertiserUserService.getUserDetaisVsBrandsByLoggedInUser().subscribe(
      res => {
        this.adertiserUsers = res;
        const findLoggedInUser =  this.adertiserUsers.find(obj => obj.email === this.userEmail);
        this.adertiserUsers = this.adertiserUsers.filter(obj => obj.email !== this.userEmail);
        this.adertiserUsers.unshift(findLoggedInUser);

        this.adertiserUserList = this.adertiserUsers.filter(obj => obj.email !== this.userEmail);
      },
      error => {

    });
  }
  checkBrandAdminUserRole (email, brandId, type) {
    let isDelete = true;
    this.userDeleteMsg = '';
    let brandNames = '';
    const brandsWithAdminRoleOfDeleteUser = [];
    this.adertiserUsers.forEach(thisItem => {
      thisItem.brands.forEach(thisBrand => {
        if (thisBrand.userRole === 'Admin' && thisItem.email === email) {
          brandsWithAdminRoleOfDeleteUser.push(thisBrand);
          brandNames = brandNames + ', ' + thisBrand.brandName;
        }
      });
    });
    if (!brandsWithAdminRoleOfDeleteUser.length) {
      isDelete = true;
    } else {
      const brandsWithAdminRoleOfOtherUser = [];
      this.adertiserUsers.forEach(thisItem => {
        thisItem.brands.forEach(thisBrand => {
          if (thisBrand.userRole === 'Admin' && thisItem.email !== email) {
            brandNames = '';
            return;
          }
        });
      });
      this.adertiserUsers.forEach(thisItem => {
        thisItem.brands.forEach(thisBrand => {
          if (thisBrand.userRole === 'Admin' && thisItem.email !== email) {
            brandsWithAdminRoleOfOtherUser.push(thisBrand);
            brandNames = brandNames + ', ' + thisBrand.brandName;
          }
        });
      });
      if (!brandsWithAdminRoleOfOtherUser.length) {
        isDelete = false;
      } else {
        brandNames = '';
        brandsWithAdminRoleOfDeleteUser.forEach(thisItem => {
          const isBrandAvailable = brandsWithAdminRoleOfOtherUser.find(obj => obj._id === thisItem._id);
            if (!isBrandAvailable) {
              isDelete = false;
              brandNames = brandNames + ' ' + thisItem.brandName;
            }
        });
      }
    }
    if (isDelete) {
      this.openDeleteCofirmDialog(email, brandId, type);
    } else {
      this.userDeleteMsg =  brandNames + ' is not associated with any admin user role,' +
      'So please assign brand to any admin before delete.';
    }
  }
  removeAdvertiserUser(email) {
    this.userDeleteMsg = '';
    this.advertiserUserService.deleteAdvertiserUser(email).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
        this.getAllAdvertiserUsers();
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }
  openDeleteCofirmDialog(email, brandId, type): void {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Are you sure you want to delete user from this brand?'
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const passwordConfirmDialogRef = this.dialog.open(ConfirmPasswordDailogComponent, {
          width: '50%',
          disableClose: true
        });
        passwordConfirmDialogRef.afterClosed().subscribe(passwordConfirmResult => {
          if (passwordConfirmResult) {
            if (result && type === 'advertiserUser') {
              this.removeAdvertiserUser(email);
            } else if (result && type === 'advertiserUserBrand') {
              this.deleteBrandFromAdvertiserUser(email, brandId);
            }
          }
        });
      }
    });
  }

  deleteBrandFromAdvertiserUser (email, brandId) {
    this.userDeleteMsg = '';
    const brandData = {
      email: email,
      brandId: brandId
    };
    this.advertiserUserService.deleteBrandFromAdvertiserUser(brandData).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
        this.getAllAdvertiserUsers();
        this.initClients();
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }
  updateBrandRoleOfAdvertiserUser(email, brandId, userRole): void {
    this.userDeleteMsg = '';
    const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Are you sure you want to update the role of this user?'
      },
      disableClose: true
    });

    confirmDialogRef.afterClosed().subscribe(confirmResult => {
      if (confirmResult) {
        const passwordConfirmDialogRef = this.dialog.open(ConfirmPasswordDailogComponent, {
          width: '50%',
          disableClose: true
        });
        passwordConfirmDialogRef.afterClosed().subscribe(passwordConfirmResult => {
          if (passwordConfirmResult) {
            const selectedUserRoleAndPermissions = this.listOfExistedUserRolesPermissions.find(obj =>
              obj.roleName.toLowerCase().trim() === userRole.toLowerCase().trim()
            );
            const brandData = {
              email: email,
              brandId: brandId,
              userRole: userRole,
              userPermissions: selectedUserRoleAndPermissions.permissions
            };
            this.advertiserUserService.updateBrandRoleOfAdvertiserUser(brandData).subscribe(
              data => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'success',
                    message: data['message']
                  }
                });
                this.getAllAdvertiserUsers();
               },
               err => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
               }
             );
          } else {
            this.getAllAdvertiserUsers();
          }
        });
      } else {
        this.getAllAdvertiserUsers();
      }
    });
  }
  reGenerateTokenOfUser(email: string) {
    const postActiveUserData = {
      email: email
    };
    this.spinner.showSpinner.next(true);
    this.advertiserUserService.reGenerateToken(postActiveUserData).subscribe(
      data => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
        this.getAllAdvertiserUsers();
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }
  switchToBrand(brandId, switchedZone) {
    if (this.changedBrand !== brandId) {
      this.changedBrand = brandId;
      const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
        width: '50%',
        data: {
          'title' : 'Are you sure you want to switch to this brand?'
        }
      });
      confirmDialogRef.afterClosed().subscribe(confirmResult => {
        if (confirmResult) {
          localStorage.setItem('zone', switchedZone);
          this.spinner.showSpinner.next(true);
          this.advertiserUserService.switchToBrandOfLoggedinUser(brandId).subscribe(
            data => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
              this.eventEmitterService.onSavedAdvertiserProfile();
              this.eventEmitterService.onReadNotification();
              this.router.navigate(['/advertiser/dashboard']);
            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
            }
          );
        }
      });
    }
  }
  searchBrand() {
    this.client = '';
    this.user = '';
    if (!this.brand) {
      this.filterdAdvertiserBrandsDataSource = this.advertiserBrandsDataSource;
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'];
      });
    } else {
      this.filterdAdvertiserBrandsDataSource = [];
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'].filter((option) =>
          option.brandName.toLowerCase().indexOf(this.brand.toLowerCase()) !== -1
        );
        if (element['filteredBrands'].length) {
          this.filterdAdvertiserBrandsDataSource.push(element);
        }
      });
    }
    return this.filterdAdvertiserBrandsDataSource;
  }
  callOnSelectUserOrClient() {
    this.brand = '';
    if (this.client && !this.user && this.client !== 'All') {
      this.filterdAdvertiserBrandsDataSource = [];
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'].filter((option) =>
          element.clientName.toLowerCase().indexOf(this.client.toLowerCase()) === 0
        );
        if (element['filteredBrands'].length) {
          this.filterdAdvertiserBrandsDataSource.push(element);
        }
      });
    } else if (this.client && !this.user && this.client === 'All') {
      this.filterdAdvertiserBrandsDataSource = this.advertiserBrandsDataSource;
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'];
      });
    } else if (!this.client && this.user && this.user.email !== 'all_users') {
      this.filterdAdvertiserBrandsDataSource = [];
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = [];
        element['brands'].forEach(thisBrand => {
          thisBrand['filteredUsers'] = thisBrand.userList.filter((thisUser) =>
          thisUser.email.toLowerCase().indexOf(this.user.email.toLowerCase()) === 0
        );
        if (thisBrand['filteredUsers'].length) {
          element['filteredBrands'].push (thisBrand);
          this.filterdAdvertiserBrandsDataSource.push(element);
        }
        });
      });
    } else if (!this.client && this.user && this.user.email === 'all_users') {
      this.filterdAdvertiserBrandsDataSource = this.advertiserBrandsDataSource;
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'];
        element['brands'].forEach(thisBrands => {
          thisBrands['filteredUsers'] = thisBrands.userList;
        });
      });
    } else if (this.client && this.user && this.client !== 'All' && this.user.email !== 'all_users') {
      this.filterdAdvertiserBrandsDataSource = [];
      this.advertiserBrandsDataSource.forEach(element => {
        let existList = [];
        existList = element['brands'].filter((option) =>
          element.clientName.toLowerCase().indexOf(this.client.toLowerCase()) === 0
        );
        element['filteredBrands'] = [];
        element['brands'].forEach(thisBrands => {
          thisBrands['filteredUsers'] = thisBrands.userList.filter((thisUser) =>
          thisUser.email.toLowerCase().indexOf(this.user.email.toLowerCase()) === 0
        );
        if (thisBrands['filteredUsers'].length) {
          element['filteredBrands'].push (thisBrands);
        }
        });
        if (element['filteredBrands'].length && existList.length) {
          this.filterdAdvertiserBrandsDataSource.push(element);
        }
      });
    } else if (this.client && this.user && this.client === 'All' && this.user.email === 'all_users') {
      this.filterdAdvertiserBrandsDataSource = this.advertiserBrandsDataSource;
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'];
        element['brands'].forEach(thisBrands => {
          thisBrands['filteredUsers'] = thisBrands.userList;
        });
      });
    } else if (this.client && this.user && this.client === 'All' && this.user.email !== 'all_users') {
      this.filterdAdvertiserBrandsDataSource = [];
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = [];
        element['brands'].forEach(thisBrand => {
          thisBrand['filteredUsers'] = thisBrand.userList.filter((thisUser) =>
          thisUser.email.toLowerCase().indexOf(this.user.email.toLowerCase()) === 0
        );
        if (thisBrand['filteredUsers'].length) {
          element['filteredBrands'].push (thisBrand);
          this.filterdAdvertiserBrandsDataSource.push(element);
        }
        });
      });
    } else if (this.client && this.user && this.client !== 'All' && this.user.email === 'all_users') {
      this.filterdAdvertiserBrandsDataSource = [];
      this.advertiserBrandsDataSource.forEach(element => {
        element['filteredBrands'] = element['brands'].filter((option) =>
          element.clientName.toLowerCase().indexOf(this.client.toLowerCase()) === 0
        );
        element['brands'].forEach(thisBrands => {
          thisBrands['filteredUsers'] = thisBrands.userList;
        });
        if (element['filteredBrands'].length) {
          this.filterdAdvertiserBrandsDataSource.push(element);
        }
      });
    }
    this.filterdAdvertiserBrandsDataSource = this.getUnique(this.filterdAdvertiserBrandsDataSource, 'clientName');
    return this.filterdAdvertiserBrandsDataSource;
  }
  getUnique(arr, comp) {
    const unique = arr
      .map(e => e[comp])
      .map((e, i, final) => final.indexOf(e) === i && i)
      .filter(e => arr[e]).map(e => arr[e]);
     return unique;
  }
}
