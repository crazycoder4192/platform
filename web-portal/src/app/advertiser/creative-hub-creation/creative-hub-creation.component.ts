import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UploadEvent, FileSystemFileEntry, UploadFile } from 'ngx-file-drop';
import config from 'appconfig.json';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { UploadToS3Service } from 'src/app/common/upload-to-s3/upload-to-s3.service';
import { Banner } from 'src/app/_models/utils';
import { UtilService } from 'src/app/_services/utils/util.service';
import { ImagedimensionsDirective } from 'src/app/common/directives/imagedimensions.directive';
import { BannerCropperComponent } from '../campaign-creation/banner-cropper/banner-cropper.component';
import { CreativeHubService } from 'src/app/_services/advertiser/creative-hub.service';
import { CreativeHub } from 'src/app/_models/advertise_creative_hub';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';
import {DomSanitizer} from '@angular/platform-browser';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
export enum FILE_SIZES {
  KB = 1024,
  MB = 1048576,
  GB = 1073741824
}

@Component({
  selector: 'app-creative-hub-creation',
  templateUrl: './creative-hub-creation.component.html',
  styleUrls: ['./creative-hub-creation.component.scss']
})

export class CreativeHubCreationComponent implements OnInit {
  @ViewChild('file') fileInput: ElementRef;
  @ViewChild(ImagedimensionsDirective) imageDimensions: ImagedimensionsDirective;
  public addCreativeForm: FormGroup;
  public videoURL: any;
  public videoFiles: UploadFile;
  public videoState = 'before';
  public creativeHub: CreativeHub;
  public webBanners: Banner[];
  public appBanners: Banner[];
  public selectedFiles: FileList;
  public selectedBannerToUpload: Banner;
  event: MouseEvent;
  public imagePath: any;
  imgURL: any;
  imageFile: File;
  imageData: FormData;
  postFormData: FormData;
  public imageFiles = [];
  public dataUrl: any;
  creativeType: any;
  existingCreativeHub: CreativeHub;
  userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  uploadedVideoUrl: string;
  isVideoUrl = false;
  progressValue = 0;
  hubReadOnly = false;
  allFilesHaveResolutionsMatch = true;
  bannerUploaded: Subject<uploadNextPointer>;
  creativeDetailsInitialSize: number;
  uniqueSectionNames = [];
  slideConfig = {
    'slidesToShow': 4,
    'slidesToScroll': 2,
    'speed': 300,
    'prevArrow': '<button type=\'button\' class=\'slick-prev pull-left\'><i class=\'fa fa-angle-left\' aria-hidden=\'true\'></i></button>',
    'nextArrow': '<button type=\'button\' class=\'slick-next pull-right\'><i class=\'fa fa-angle-right\' aria-hidden=\'true\'></i></button>',
    'dots': true,
    'cssEase': 'ease-in'
  };
  filesUploadTracker: FileUploadTracker[];
  progressBarInit = false;
  progressBarColor = 'primary';
  progressBarMode = 'determinate';
  progressBarValue = 0;
  progressCount = 0;
  atleastOneUploaded = false;
  atleastOneFailed = false;
  allFilesUploaded = false;
  /*Todo:
  Implement lazy loading so that images load only after coming in focus
  */

  slickInit(e: any) {
    //
  }

  configuredFileSizes = config['SIZES']['BANNER'];
  constructor(
    private dialog: MatDialog,
    public matDialogRef: MatDialogRef<CreativeHubCreationComponent>,
    @Inject(MAT_DIALOG_DATA) public creativeHubdata: any,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private sanitizer: DomSanitizer,
    private spinner: SpinnerService,
    private utilHTTPService: UtilService,
    private creativeHubService: CreativeHubService,
    private uploadService: UploadToS3Service,
    private advertiserService: AdvertiserProfileService,
    private _DomSanitizer: DomSanitizer
  ) {
    matDialogRef.disableClose = true;
    this.creativeHub = {
      _id: '',
      name: '',
      creativeType: '',
      creativeDetails: [
          { formatType: '', url: '', size: '', resolution: '', dimensions: '', styleName: '' }
      ],
      sectionName: 'Default',
      status: '',
      deletedUsers: [],
      advertiserId: '',
    };
    this.creativeHub.creativeDetails.pop();
  }

  ngOnInit() {
    this.creativeType = this.creativeHubdata.creativeType;
    this.existingCreativeHub = this.creativeHubdata.creativeHub;
    this.uniqueSectionNames = this.creativeHubdata.uniqueSectionNames;
    this.createForm();
    this.getBanners();
    this.getAdvertiser();
  }

  createForm() {
    if (this.existingCreativeHub !== undefined) {
      this.hubReadOnly = true;
      this.creativeHub = this.existingCreativeHub;
      this.addCreativeForm = this.formBuilder.group({
        creativeName: [this.existingCreativeHub.name, Validators.required],
        sectionName: this.creativeHub.sectionName
      });
      this.creativeDetailsInitialSize = this.creativeHub.creativeDetails.length;
    } else {
      this.addCreativeForm = this.formBuilder.group({
        creativeName: ['', Validators.required],
        sectionName: 'Default'
      });
    }
  }

  getAdvertiser(): void {
    /*     TODO--->Dependent on "AdvertiserUser-task",
     because for each advertiser multiple user will exist.
     So, based on the user need to get advertiserId */
    this.advertiserService.getAdvertiserByEmail(this.userEmail).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.creativeHub.advertiserId = res['_id'];
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAdvertisers : ' + err);
      });
  }

  getBanners(): void {
    this.utilHTTPService.getBanners().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        let banners = [...<Banner[] > res];
        let webBanners = [];
        let appBanners = [];
        banners.forEach((element: any) => {
          element.checked = false;
          if (element.platform === 'website') {
            element.type = 'web';
            webBanners.push(element);
          }
          if (element.platform === 'mobileApp') {
            element.type = 'app';
            appBanners.push(element);
          }
        });
        this.webBanners = webBanners;
        this.appBanners = appBanners;
        /*Todo : Publisher assets linkage.
        Based on assets banner sizes, campagin banner sizes selection should enable
        Till that linkage has done, will enable few campaign banner sizes */
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getBanners : ' + err);
      });
  }

  openFileSelectionDialog(banner: Banner) {
    /*
    if (banner.uploadStatus) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Already image has been uploaded for the selected banner : ' + banner.name
        }
      });
      this.selectedFiles = null;
      return;
    }
    */
    this.selectedFiles = null;
    this.selectedBannerToUpload = banner;
    /*banner.checked = (banner.checked) ? !banner.checked : true;*/
    this.event = new MouseEvent('click', {bubbles: false});
    this.fileInput.nativeElement.dispatchEvent(this.event);
  }

  selectFile(event: { target: { files: FileList; }; }) {
    this.filesUploadTracker = [];
    this.progressBarValue = 0;
    this.progressBarInit = true;
    let fileIndex = 0;
    this.atleastOneFailed = false;
    this.allFilesUploaded = false;
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.length === 0) {
      return;
    }

    let allFilesAreImages = true;
    let allFilesAreWithinSizeLimits = true;
    for(let i = 0; i < this.selectedFiles.length; i++){
      let fileTrackerObj: FileUploadTracker = {
        index: fileIndex++,
        name: this.selectedFiles[i].name,
        uploadStatus: 'pending',
        reason: ''
      };
      this.filesUploadTracker.push(fileTrackerObj);
      if (this.selectedFiles[i].type.match(/image\/*/) == null) {
        allFilesAreImages = false;
      }

      if (this.selectedFiles && this.selectedFiles[i] && this.selectedFiles[i].size
        && ((this.selectedFiles[i].size / +FILE_SIZES[config['SIZES']['BANNER'].split('~')[1]]) >
              +config['SIZES']['BANNER'].split('~')[0])) {
        allFilesAreWithinSizeLimits = false;
      }
    }

   // const mimeType = this.selectedFiles[0].type;
    if (!allFilesAreImages) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Only images are supported.'
        }
      });
      return;
    } else if (!allFilesAreWithinSizeLimits) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Selected image size is exceed, it should below ' + config['SIZES']['BANNER']
        }
      });
      return;
    }
    this.allFilesHaveResolutionsMatch = true;
    this.uploadFilesTillDone(0);

  }

  uploadFilesTillDone(fileIndex: number){
    this.matchImageResolution( this.selectedFiles[fileIndex], fileIndex );
    this.bannerUploaded.subscribe(
      uploadNextPointer => {
        if(uploadNextPointer.uploadedState === 'success') {
          this.imageFiles.push(this.selectedFiles[uploadNextPointer.fileIndex]);
        }
        if (uploadNextPointer.fileIndex < this.selectedFiles.length - 1){
          uploadNextPointer.fileIndex++;
          this.updateProgressBarColor(uploadNextPointer);
          this.uploadFilesTillDone(uploadNextPointer.fileIndex);
          if (uploadNextPointer.fileIndex === this.selectedFiles.length - 1) {
            this.allFilesUploaded = true;
            console.log('@all files uploaded', this.allFilesUploaded);
            this.updateProgressBarColor(uploadNextPointer);
            this.updateUploadProgressBar();
          }
        }
        if (uploadNextPointer.fileIndex === this.selectedFiles.length - 1) {
          this.allFilesUploaded = true;
          console.log('@all files uploaded', this.allFilesUploaded);
          this.updateProgressBarColor(uploadNextPointer);
          if( this.atleastOneUploaded ) {
            this.progressBarColor = 'primary';
          }
          this.updateUploadProgressBar();
        }
      }
    );
  }


  matchImageResolution(selectedFile: Blob | File, fileIndex: number){
    //let selectedFile= this.selectedFiles[fileIndex];
    this.bannerUploaded = new Subject<uploadNextPointer>();
    const reader = new FileReader();
    reader.readAsDataURL(selectedFile);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    const that = this;
    reader.onloadend = (_event) => {
      const invokeData = setInterval(() => {
        clearInterval(invokeData);
        if ((this.selectedBannerToUpload.size.split(' x ')[0] === this.imageDimensions.elementWidth.toString())
        && (this.selectedBannerToUpload.size.split(' x ')[1] === this.imageDimensions.elementHeight.toString())) {
          this.openBannerPreview({banner: this.selectedBannerToUpload, url: this.imgURL,
            width: this.imageDimensions.elementWidth, height: this.imageDimensions.elementHeight, previewOrUpload: 'upload'}, fileIndex);
            this.filesUploadTracker[fileIndex].uploadStatus = 'success';
        } else {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: 'Selected banner dimensions and browsed image dimensions are not matched'
            }
          });
          this.filesUploadTracker[fileIndex].uploadStatus = 'failed';
          this.filesUploadTracker[fileIndex].reason = 'Dimension mismatch';
          this.bannerUploaded.next({fileIndex: fileIndex,
             uploadedState: this.filesUploadTracker[fileIndex].uploadStatus});
        }
      }, 100);
    };
  }


  updateProgressBarColor( uploadNextPointer: uploadNextPointer ) {
    if (uploadNextPointer.uploadedState === 'failed' ) {
      this.progressBarColor = 'warn';
      this.atleastOneFailed = true;
    } else if ( uploadNextPointer.uploadedState === 'success' ) {
      this.progressBarColor = 'primary';
      this.atleastOneUploaded = true;
    }
  }

  updateUploadProgressBar() {
    this.progressCount = 0;
    this.filesUploadTracker.forEach(tracker => {
      if(tracker.uploadStatus === 'success' || tracker.uploadStatus === 'failed'){
        this.progressCount++;
      }
    });
    this.progressBarValue = Math.round(this.progressCount * 100 / this.filesUploadTracker.length);
    console.log('this progressBarValue: ', this.filesUploadTracker);
    console.log('this progressBarValue: ', this.progressBarValue);
  }

  openBannerPreview(selectedData: { banner: Banner; url: any; width: string; height: string; previewOrUpload: string; },
    fileIndex: number) {
    let dialogRef: any;
    if(this.selectedFiles.length > 1) {
      this.prepareCreativeHub(fileIndex);
      this.webBanners.forEach(element => {
        if (element._id === this.selectedBannerToUpload._id &&
            element.type === this.selectedBannerToUpload.type) {
              element.uploadStatus = true;
        }
      });
      this.appBanners.forEach(element => {
        if (element._id === this.selectedBannerToUpload._id &&
            element.type === this.selectedBannerToUpload.type) {
              element.uploadStatus = true;
        }
      });
    } else {
      dialogRef = this.dialog.open(BannerCropperComponent, {
        width: '75%',
        height: '75%',
        data: selectedData
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        //this.selectedBannerToUpload.uploadStatus = true;
        this.prepareCreativeHub(fileIndex);
        this.webBanners.forEach(element => {
          if (element._id === this.selectedBannerToUpload._id &&
              element.type === this.selectedBannerToUpload.type) {
                element.uploadStatus = true;
          }
        });
        this.appBanners.forEach(element => {
          if (element._id === this.selectedBannerToUpload._id &&
              element.type === this.selectedBannerToUpload.type) {
                element.uploadStatus = true;
          }
        });
      });
    }

    /*
    TODO: introduce a progress bar with S3 like upload status
    showing success and failures
    */
  }


  prepareCreativeHub(fileIndex: number) {
    const file = this.selectedFiles.item(fileIndex);
    this.creativeHub.creativeDetails.push({
      formatType: this.getFileExtension(this.selectedFiles[fileIndex].type),
      url: '',
      size: this.selectedBannerToUpload.size.toString(),
      resolution: this.selectedBannerToUpload.name.toString(),
      dimensions: '',
      styleName: ''
    });
    this.spinner.showSpinner.next(true);
    const that = this;
    const fileExtension = this.getFileExtension(this.selectedFiles[fileIndex].type);
    const bannerSizeTrim = this.selectedBannerToUpload.size.toString().replace(/\s/g, '');
    // we need to add a random number as we can have multiple files against a single dimension
    const fileName = 'ch-' + this.creativeHub._id + this.selectedBannerToUpload.type.toString() + 
    bannerSizeTrim + Math.round(Math.random() * 20000);
    this.uploadService.fetchBrandIdentifierS3();
    this.uploadService._brandIdentifySource.subscribe(
      brandIdentifier => {
        this.uploadService.uploadFileAndGetResult(file, fileName, brandIdentifier, fileExtension).send(function(err, data) {
          if (err) {
            that.spinner.showSpinner.next(false);
            that.updateUploadProgressBar();
            that.bannerUploaded.next({fileIndex: fileIndex,
              uploadedState: 'failed'});
            return false;
          }
          that.spinner.showSpinner.next(false);
          that.creativeHub.creativeDetails
            [(that.creativeHub.creativeDetails.length - 1)].url = data['Location'];
          that.updateUploadProgressBar();
          that.bannerUploaded.next({fileIndex: fileIndex,
            uploadedState: 'success'});
        });
      }
    );
  }

  saveCreativeHub() {
    // const formData = new FormData();
    // formData.append('creativeHub', that.creativeHub);
    if (this.validateCreativeForm()) {
      this.creativeHub.creativeType = this.creativeType;
      this.creativeHub.name = this.addCreativeForm.get('creativeName').value;
      this.creativeHub.status = 'uploaded';
      this.creativeHub.sectionName = this.addCreativeForm.get('sectionName').value;
      this.creativeHub.deletedUsers = [];

      if (this.existingCreativeHub !== undefined) {
        this.creativeHubService.updateCreativeHub(this.creativeHub._id , this.creativeHub).subscribe(
          res => {
            this.dialog.closeAll();
          },
          err => {

        });
      } else {
        this.creativeHubService.addCreativeHub(this.creativeHub).subscribe(
          res => {
            this.dialog.closeAll();
          },
          err => {

        });
      }
    }
  }

  validateCreativeForm() {
    const formGroup = this.addCreativeForm;
    const creativeNameCtrl = formGroup.controls['creativeName'];
    const sectionNameCtrl = formGroup.controls['sectionName'];
    let isValid = true;
    if (!creativeNameCtrl.value) {
      creativeNameCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (this.creativeType === 'image' && !this.imageFiles.length) {
      creativeNameCtrl.setErrors({ isAtLeastOneImage: true });
      isValid = false;
    }
    if (this.creativeType === 'video' && !this.uploadedVideoUrl) {
      this.isVideoUrl = true;
      isValid = false;
    }
    if (!sectionNameCtrl.value) {
      sectionNameCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }

  public dropped(event: UploadEvent) {
    this.videoURL = '';
    this.videoFiles = event.files[0];
    let validVideoFile = true;
    this.creativeHub.creativeDetails = [
      {formatType: '', url: '', size: '', resolution: '', dimensions: '', styleName: ''}];
    for (const droppedFile of event.files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        this.progressValue = 20;
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          if (file && file.type !== 'video/mp4') {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: 'Only mp4 videos will be supported. '
              }
            });
            validVideoFile = false;
            return;
          } else if (file && file.size &&
            ((file.size / +FILE_SIZES[config['SIZES']['VIDEO'].split('~')[1]]) > +config['SIZES']['VIDEO'].split('~')[0])) {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'error',
                  message: 'Selected video size is exceed, it should below ' + config['SIZES']['VIDEO']
                }
              });
            validVideoFile = false;
            return;
          }
          const blob = new Blob([file], {type: 'video/mp4'});
          const objectUrl: string = URL.createObjectURL(blob);
          this.videoURL = this.sanitizer.bypassSecurityTrustUrl(objectUrl);
          // Here you can access the real file
          // console.log('dropped : ' + droppedFile.relativePath, file);
          if (validVideoFile) {
            this.creativeHub.creativeDetails[0].size = file.size.toString();
            this.creativeHub.creativeDetails[0].formatType = file.type.toString();
           this.videoState = 'normal';
          }
        });
        this.uploadVideoToS3();
        // this.progressValue = 100;
      }
    }
  }

  public fileOver(event: string) {
    console.log('fileOver : ' + event);
  }

  public fileLeave(event: string) {
    console.log('fileLeave : ' + event);
  }

  getFileExtension(browsedFile: string): string {
    //browsedFile = browsedFile.toLowerCase();
    return ((browsedFile === 'image/jpg' ||
    browsedFile === 'image/jpeg' ||
    browsedFile === 'image/jpe' ||
    browsedFile === 'image/jif' ||
    browsedFile === 'image/jfif' ||
    browsedFile === 'image/jfi') ? 'JPEG' :
    ((browsedFile === 'image/png') ? 'PNG' :
    ((browsedFile === 'image/gif') ? 'GIF' : 'NotValidFormat')));
  }

  uploadVideoToS3() {
    if (this.videoFiles.fileEntry.isFile) {
      this.spinner.showSpinner.next(true);
      this.progressValue = 50;
      const fileEntry = this.videoFiles.fileEntry as FileSystemFileEntry;
      /*As we are providing single video upload, using [0] position */
      // this.creativeHub.creativeDetails[0].platformType = 'VideoPlatformType';
      fileEntry.file((file: File) => {
        const that = this;
        const fileExtension = this.getFileExtension(file.type);
        const bannerSizeTrim = file.size.toString().replace(/\s/g, '');
        const fileName = 'ch-' + file.name + bannerSizeTrim;
        this.uploadService.fetchBrandIdentifierS3();
        this.uploadService._brandIdentifySource.subscribe(
          brandIdentifier => {
            this.uploadService.uploadFileAndGetResult(file, fileName, brandIdentifier, fileExtension).send(function(err, data) {
              if (err) {
                that.spinner.showSpinner.next(false);
                return false;
              }
              that.isVideoUrl = false;
              that.uploadedVideoUrl = data['Location'];
              that.creativeHub.creativeDetails[0].url = data['Location'];
              that.progressValue = 100;
              that.spinner.showSpinner.next(false);
            });
          }
        );
      });
    }
  }
  cancelVideoToS3() {
    this.videoState = 'before';
    this.uploadedVideoUrl = '';
    this.creativeHub.creativeDetails[0].url = '';
  }

}

interface FileUploadTracker {
  index: number;
  name: string;
  uploadStatus: string;
  reason: string;
}

interface uploadNextPointer {
  fileIndex: number;
  uploadedState: string;
}
