import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreativeHubCreationComponent } from './creative-hub-creation.component';

describe('CreativeHubCreationComponent', () => {
  let component: CreativeHubCreationComponent;
  let fixture: ComponentFixture<CreativeHubCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreativeHubCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreativeHubCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
