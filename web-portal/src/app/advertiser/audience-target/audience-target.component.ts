import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, ValidatorFn, FormControl } from '@angular/forms';
import { TargetElement } from 'src/app/_models/advertiser_audience_target';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { DropDownWithTreeComponent } from 'src/app/common/drop-down-with-tree/drop-down-with-tree.component';
import { Observable } from 'rxjs';
import { UtilService } from 'src/app/_services/utils/util.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AudienceTargetService } from 'src/app/_services/advertiser/audience-target.service';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { UploadToS3Service } from 'src/app/common/upload-to-s3/upload-to-s3.service';
import { MatDialogConfig, MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { debounceTime } from 'rxjs/operators';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

export interface AudienceListElement {
  _id: string;
  name: string;
  created: any;
  audienceUploaded: boolean;
  npiSize: string;
  action: any;
}
const geoJsonUS = require('./../../../assets/geojson_US.json');
const geoJsonIN = require('./../../../assets/geojson_IN.json');
const VIEW_LIST: string[] = ['VIEW', 'CREATE', 'UPLOAD'];

@Component({
  selector: 'app-audience-target',
  templateUrl: './audience-target.component.html',
  styleUrls: ['./audience-target.component.scss']
})
export class AudienceTargetComponent implements OnInit {
  dropdownEnabled = true;
  items: TreeviewItem[];
  specializationItems: TreeviewItem[];
  // TODO: Hardcoded
  zone = localStorage.getItem('zone');
  values: number[];
  config = TreeviewConfig.create({
      hasAllCheckBox: true,
      hasFilter: true,
      hasCollapseExpand: true,
      decoupleChildFromParent: false,
      maxHeight: 100
  });
  @ViewChild('ddTreeLocations') ddTreeLocations: DropDownWithTreeComponent;
  @ViewChild('ddTreeSpecialization') ddTreeSpecilaziton: DropDownWithTreeComponent;
  @ViewChild('file') fileInput: ElementRef;
  displayedColumns: string[] = ['name', 'created', 'audienceUploaded', 'npiSize', 'action'];
  dataSource: AudienceListElement[];
  viewingstate = VIEW_LIST[0];
  uploadFormGroup: FormGroup;
  targetSectionFormGroup: FormGroup;
  audienceTarget = <TargetElement>{};
  lat = this.zone === '1' ? 41.4925 : this.zone === '2' ?  20.5937 : 41.4925 ;
  lng = this.zone === '1' ? -99.9018 : this.zone === '2' ?  78.9629 : -99.9018 ;
  zoom = this.zone === '1' ? 3 : this.zone === '2' ?  4 : 3;

  markers: Marker[] = [];
  backupMarkers: Marker[] = [];
  currentZoomLevel: number = this.zoom;
  geoJsonObject = this.zone === '1' ? geoJsonUS : this.zone === '2' ?  geoJsonIN : geoJsonUS;
  newGeoJson = {
    'type': 'FeatureCollection',
    'features': []
  };
  featureStyle = {
    fillColor: '#ff726f',
    fillOpacity: 0.2,
    strokeColor: '#ff726f',
    strokeWeight: 1
  };
  bounds = {
    northWestLat : 0,
    northWestLng : 0,
    southEastLat : 0,
    southEastLng : 0
  };
  defaultSelectionCreateHVA = false;
  zoomLevelReached = false;
  mapControls = false;
  locationList: any[];
  filteredLocationList: Observable<any[]>;
  specializationList: any[];
  isCityTagRemovable = true;
  locationsTreeList: any;
  specializationTreeList: any;
  selectedLocationZipCodes = [];
  selectedSpecializations = [];
  genders: string[] = ['Female', 'Male', 'Other'];
  behaviourList: string[] = [];
  potentialContent = false;
  potentialTotalHcps = 0;
  potentialReach = 0;
  prePotentialReach = 0;
  percentagePotential = 0;
  invalidFile = {status: false, message: ''};
  file: File;
  event: any;
  selectedFiles: FileList;
  selectedFileName: string;
  uploadSectionNameExists = false;
  fileUploadInitate = false;
  downloadCSV = false;
  toolTipKeyList = new Set([
    'advertiser_audience_location', 'advertiser_audience_demographic_specialization',
    'advertiser_audience_demographic_gender', 'advertiser_audience_demographic_behaviour'
  ]);
  marketSelection: string;

  constructor(private _formBuilder: FormBuilder, private uploadService: UploadToS3Service, private snackBar: MatSnackBar,
    private utilHTTPService: UtilService, private brandService: AdvertiserBrandService, private dialog: MatDialog,
    private spinner: SpinnerService,private toolTipService: ToolTipService, private audienceHTTPService: AudienceTargetService,
    private amplitudeService: AmplitudeService) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.audienceTarget.createAudienceTargetDetails = {
      geoLocation: { isInclude: false, locations: [] },
      demoGraphics: {
        specialization: { isInclude: false, areaOfSpecialization: [], groupOfSpecialization: [] },
        gender: [], behaviours: []
      }, websiteTypes: [], deviceTypes: [], keywords: []
    };
    this.getBrands();
    // this.getSavedAudienceList();
    this.getLocations();
    this.getSpecialization();
    this.getBehaviours();
  }

  getSavedAudienceList() {
    this.spinner.showSpinner.next(true);
    this.audienceHTTPService.getAllSavedAudience(this.audienceTarget.brandId).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.dataSource = <AudienceListElement[]>res;
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getSavedAudienceList : ' + err);
      });
  }

  ngOnInit() {
    this.uploadFormGroup = this._formBuilder.group({
      HVAName: ['', Validators.required]
    });
    this.targetSectionFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      locationsAdded: ['', Validators.required],
      gen: new FormArray([], this.minSelectedCheckboxes(1)),
      behaviourName: new FormArray([], this.minSelectedCheckboxes(1)),
      specializationName: ['', Validators.required]
    });
    this.targetSectionFormGroup.controls.locationsAdded.valueChanges.pipe(debounceTime(10)).subscribe(
      value => {
        if (this.defaultSelectionCreateHVA &&
          this.targetSectionFormGroup.controls.locationsAdded.value.length >= 0 &&
          this.targetSectionFormGroup.controls.specializationName.value.length >= 0 &&
          this.targetSectionFormGroup.controls.gen.value.length >= 0 &&
          this.targetSectionFormGroup.controls.behaviourName.value.length >= 0) {
          this.getPotentialReach();
        }
      }
    );
    this.targetSectionFormGroup.controls.specializationName.valueChanges.pipe(debounceTime(10)).subscribe(
      value => {
        if (this.defaultSelectionCreateHVA &&
          this.targetSectionFormGroup.controls.locationsAdded.value.length >= 0 &&
          this.targetSectionFormGroup.controls.specializationName.value.length >= 0 &&
          this.targetSectionFormGroup.controls.gen.value.length >= 0 &&
          this.targetSectionFormGroup.controls.behaviourName.value.length >= 0) {
          this.getPotentialReach();
        }
      }
    );
    this.targetSectionFormGroup.controls.gen.valueChanges.pipe(debounceTime(10)).subscribe(
      value => {
        if (this.defaultSelectionCreateHVA &&
          this.targetSectionFormGroup.controls.locationsAdded.value.length >= 0 &&
          this.targetSectionFormGroup.controls.specializationName.value.length >= 0 &&
          this.targetSectionFormGroup.controls.gen.value.length >= 0 &&
          this.targetSectionFormGroup.controls.behaviourName.value.length >= 0) {
          this.getPotentialReach();
        }
      }
    );
    this.targetSectionFormGroup.controls.behaviourName.valueChanges.pipe(debounceTime(10)).subscribe(
      value => {
        if (this.defaultSelectionCreateHVA  &&
          this.targetSectionFormGroup.controls.locationsAdded.value.length >= 0 &&
          this.targetSectionFormGroup.controls.specializationName.value.length >= 0 &&
          this.targetSectionFormGroup.controls.gen.value.length >= 0 &&
          this.targetSectionFormGroup.controls.behaviourName.value.length >= 0) {
          this.getPotentialReach();
        }
      }
    );
    this.audienceTarget.createAudienceTargetDetails.geoLocation.isInclude = 'true';
    this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.isInclude = 'true';
    this.addGenderControls();
    this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }

  changeState(updatedState) {
    this.items = [];
    this.viewingstate = updatedState;
    this.potentialContent = false;
    this.potentialTotalHcps = 0;
    this.potentialReach = 0;
    this.percentagePotential = 0;
    this.prePotentialReach = 0;
    this.audienceTarget.name = '';
    this.audienceTarget._id = '';
    if (updatedState === VIEW_LIST[0]) {
      this.getSavedAudienceList();
      this.defaultSelectionCreateHVA = false;
      this.targetSectionFormGroup.controls.gen.reset();
      this.targetSectionFormGroup.controls.behaviourName.reset();
    } else if (updatedState === VIEW_LIST[1]) {
      this.spinner.showSpinner.next(true);
      setTimeout(() => {
        if (updatedState === VIEW_LIST[1]) {
          this.items = [this.locationsTreeList];
        }
      }, 100);
      this.defaultSelectionCreateHVA = false;
      this.targetSectionFormGroup.controls.name.reset();
      this.audienceTarget.createAudienceTargetDetails = {
        geoLocation: { isInclude: false, locations: [] },
        demoGraphics: {
          specialization: { isInclude: false, areaOfSpecialization: [], groupOfSpecialization: [] },
          gender: [], behaviours: []
        }, websiteTypes: [], deviceTypes: [], keywords: []
      };
      this.potentialContent = true;
      this.spinner.showSpinner.next(true);
      setTimeout(() => {
        this.spinner.showSpinner.next(false);
        this.defaultEnableTargetSectionFields();
      }, 1000);
    } else if (updatedState === VIEW_LIST[2]) {
      this.selectedFileName = '';
      this.selectedFiles = null;
      this.invalidFile.message = '';
      this.downloadCSV = false;
      this.defaultSelectionCreateHVA = false;
      this.uploadFormGroup.controls.HVAName.reset();
      this.targetSectionFormGroup.controls.gen.reset();
      this.targetSectionFormGroup.controls.behaviourName.reset();
    }
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map(control => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => next ? prev + next : prev, 0);
      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };
    return validator;
  }

  removeLocationsLatest(zCode) {
    const pos = this.locationsTreeList.children.findIndex(code => code.value === zCode);
    this.audienceTarget.createAudienceTargetDetails.geoLocation.locations =
              this.audienceTarget.createAudienceTargetDetails.geoLocation.locations
      .filter(item => item.zipcode !== zCode);
      this.selectedLocationZipCodes = this.selectedLocationZipCodes
        .filter(item => item !== zCode);
    this.ddTreeLocations.onSelect(this.selectedLocationZipCodes);
    this.ddTreeLocations.updateItems(this.items, this.selectedLocationZipCodes);
    this.refreshGeoMarkers();
  }

  addMoreLoactionsLatest(events) {
    this.audienceTarget.createAudienceTargetDetails.geoLocation.locations = [];
    this.selectedLocationZipCodes = events;
    for (const selectedZipCode of events) {
      const selectedNode = this.treeSearch(this.locationList, selectedZipCode);
      this.audienceTarget.createAudienceTargetDetails.geoLocation.locations.push(
        {zipcode: selectedNode.value, city: selectedNode.text});
    }
    this.refreshGeoMarkers();
  }

  addSpecialization(events) {
    this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization = [];
    this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.groupOfSpecialization = [];
    this.selectedSpecializations = events;
    for (const selectedSpecialization of events) {
      const selectedNode = this.treeSearch(this.specializationList, selectedSpecialization);
      this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization.push(
        selectedNode.value);
      this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.groupOfSpecialization.push(
        {taxonamyID: selectedNode.value, taxonamyName: selectedNode.text});
    }
  }

  treeSearch(tree, value) {
    return tree.reduce(function f(acc, node) {
        return (node.value === value) ? node :
            (node.children && node.children.length) ? node.children.reduce(f, acc) : acc;
    });
  }
  addGenderControls() {
    this.genders.map((o, i) => {
      const existedValue = this.audienceTarget.createAudienceTargetDetails.demoGraphics.gender.includes(o);
      const control = new FormControl(existedValue);
      (this.targetSectionFormGroup.controls.gen as FormArray).push(control);
    });
  }
  onChange(selecteddValue: string, event: boolean, existedDBValues: string[], type: string) {
    let tempArray = [];
    if (existedDBValues.length > 0) {
      tempArray = tempArray.concat(existedDBValues);
    }
    if (event) {
      tempArray.push(selecteddValue);
    } else {
      const index = tempArray.indexOf(selecteddValue);
      tempArray.splice(index, 1);
    }
    if (type === 'genderType') {
      this.audienceTarget.createAudienceTargetDetails.demoGraphics.gender = tempArray;
    }
    if (type === 'behaviourType') {
      this.audienceTarget.createAudienceTargetDetails.demoGraphics.behaviours = tempArray;
    }
    /*if (type === 'deviceType') {
      this.audienceTarget.targetDetails.deviceTypes = tempArray;
    }
    if (type === 'webSiteType') {
      this.audienceTarget.targetDetails.websiteTypes = tempArray;
    }*/
  }
  updateGenderControls() {
    const prevSelectedItems = [];
    this.genders.map((o, i) => {
      const existedValue = this.audienceTarget.createAudienceTargetDetails.demoGraphics.gender.includes(o);
      prevSelectedItems.push(existedValue);
    });
    this.targetSectionFormGroup.controls.gen.setValue(prevSelectedItems);
  }

  addBehaviourControls() {
    if (this.behaviourList && this.behaviourList.length > 0) {
    for (const value of this.behaviourList) {
      const control = new FormControl(false);
      /*if (this.pageMode === 'Edit' && this.audienceTarget.targetDetails.demoGraphics.behaviours.includes(value)) {
        control = new FormControl(true);
      }*/
      (this.targetSectionFormGroup.controls.behaviourName as FormArray).push(control);
    }
    }
  }
  updateBehaviourControls() {
    const prevSelectedItems = [];
    this.behaviourList.map((o, i) => {
      const existedValue = this.audienceTarget.createAudienceTargetDetails.demoGraphics.behaviours.includes(o);
      prevSelectedItems.push(existedValue);
    });
    this.targetSectionFormGroup.controls.behaviourName.setValue(prevSelectedItems);
    if (this.defaultSelectionCreateHVA === false) {
      const invokePotentialReachForDefault = setInterval(() => {
        if (this.audienceTarget.createAudienceTargetDetails.geoLocation.locations.length > 0 &&
          this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization.length > 0 &&
          this.audienceTarget.createAudienceTargetDetails.demoGraphics.gender.length > 0 &&
          this.audienceTarget.createAudienceTargetDetails.demoGraphics.behaviours.length > 0 &&
          this.audienceTarget.createAudienceTargetDetails.geoLocation.locations.length ===
                           this.targetSectionFormGroup.controls.locationsAdded.value.length &&
          this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization.length ===
                            this.targetSectionFormGroup.controls.specializationName.value.length &&
          this.audienceTarget.createAudienceTargetDetails.demoGraphics.gender.length ===
                            this.targetSectionFormGroup.controls.gen.value.length &&
          this.audienceTarget.createAudienceTargetDetails.demoGraphics.behaviours.length ===
                            this.targetSectionFormGroup.controls.behaviourName.value.length) {
            clearInterval(invokePotentialReachForDefault);
            this.defaultSelectionCreateHVA = true;
            /*if (this.viewingstate === VIEW_LIST[1]) {
              this.getPotentialReach();
            }*/
        }
      }, 100);
    }
  }

  getBrands(): void {
    this.brandService.getAllBrands().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.audienceTarget.brandId = <any>res[0]._id;
        this.audienceTarget.advertiserId = <any>res[0].advertiser;
        this.getSavedAudienceList();
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAllBrands : ' + err);
      });
  }
  getLocations(): void {
    this.utilHTTPService.getValidHCPLocations().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.locationList = [];
        /* TreeSearch ignore's the first node, that's why inserted 'dummy data' item */
        this.locationList.push('dummy data');
        this.locationList.push(res);
        this.locationsTreeList = new TreeviewItem(<any>res);
        // this.items = [this.locationsTreeList];
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getLocations : ' + err);
      });
  }
  getSpecialization(): void {
    this.utilHTTPService.getValidHCPSpecialization().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.specializationList = [];
        /* TreeSearch ignore's the first node, that's why inserted 'dummy data' item
        specializationTreeList: any; */
        this.specializationList.push('dummy data');
        this.specializationList.push(res);
        this.specializationTreeList = new TreeviewItem(<any>res);
        this.specializationItems = [this.specializationTreeList];
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getSpecialization : ' + err);
      });
  }
  getBehaviours(): void {
    /*this.behaviourList = [
      'Pharmeity',
      'Toolboxers',
      'Analogous',
      'Curators',
      'Connectors',
      'Followers',
      'Pupils',
    ];*/
    this.utilHTTPService.getArchytypes().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.behaviourList = [];
         (<any[]>res).forEach(item => {
          this.behaviourList.push( item.name );
        });
        this.addBehaviourControls();
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getArchytypes : ' + err);
      });
  }

  getPotentialReach(): void {
    this.audienceHTTPService.getPotentialReach(this.audienceTarget).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        if (this.viewingstate === VIEW_LIST[1]) {
          this.potentialContent = true;
          this.potentialTotalHcps = +res['totalHcps'];
          this.potentialReach = +res['potentialReach'];
          if (this.prePotentialReach !== this.potentialReach) {
            this.percentagePotential =  Math.floor((this.potentialReach / this.potentialTotalHcps) * 100);
            this.prePotentialReach = this.potentialReach;
          }
        }
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.potentialContent = false;
        this.potentialTotalHcps = 0;
        this.potentialReach = 0;
        this.prePotentialReach = 0;
        this.percentagePotential = 0;
        console.log('Error! getPotentialReach : ' + err);
      });
  }

  getUploadedPotentialReach(): void {
    this.audienceHTTPService.getUploadedPotentialReach(this.audienceTarget._id).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        if (this.viewingstate === VIEW_LIST[2]) {
          this.potentialContent = true;
          this.potentialTotalHcps = +res['totalHcps'];
          this.potentialReach = +res['potentialReach'];
          if (this.prePotentialReach !== this.potentialReach) {
            this.percentagePotential = Math.floor((this.potentialReach / this.potentialTotalHcps) * 100);
            this.prePotentialReach = this.potentialReach;
          }
        }
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.potentialContent = false;
        this.potentialTotalHcps = 0;
        this.potentialReach = 0;
        this.percentagePotential = 0;
        this.prePotentialReach = 0;
        console.log('Error! getUploadedPotentialReach : ' + err);
      });
  }

  saveHVAForCreate() {
    if (this.targetSectionFormGroup.valid) {
      this.spinner.showSpinner.next(true);
      this.audienceTarget.isFileUpload = false;
      this.audienceHTTPService.createOrUploadHVA(this.audienceTarget).subscribe(
        res => {
          let eventProperties = {
            HVAName: this.audienceTarget.name
          }
          this.amplitudeService.logEvent('campaign_creation - create audience', eventProperties);
          this.spinner.showSpinner.next(false);
          this.audienceTarget.name = '';
          this.audienceTarget.createAudienceTargetDetails = {
            geoLocation: { isInclude: false, locations: [] },
            demoGraphics: {
              specialization: { isInclude: false, areaOfSpecialization: [], groupOfSpecialization: [] },
              gender: [], behaviours: []
            }, websiteTypes: [], deviceTypes: [], keywords: []
          };
          this.changeState(VIEW_LIST[0]);
          this.targetSectionFormGroup.controls.name.reset();
        },
        err => {
          if (err.status === 500) {
            const formGroup: FormGroup = this.targetSectionFormGroup;
            const createOREditHVA = formGroup.controls['name'];
            createOREditHVA.setErrors({ nameNotAvailable: true });
          }
          this.spinner.showSpinner.next(false);
          console.log('Error! saveHVA : ' + err);
      });
    }
  }

  updateHVAForCreate() {
    if (this.targetSectionFormGroup.valid) {
      this.spinner.showSpinner.next(true);
      this.audienceTarget.isFileUpload = false;
      this.audienceHTTPService.updateOrUploadHVA(this.audienceTarget).subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          this.audienceTarget.name = '';
          this.audienceTarget.createAudienceTargetDetails = {
            geoLocation: { isInclude: false, locations: [] },
            demoGraphics: {
              specialization: { isInclude: false, areaOfSpecialization: [], groupOfSpecialization: [] },
              gender: [], behaviours: []
            }, websiteTypes: [], deviceTypes: [], keywords: []
          };
          this.changeState(VIEW_LIST[0]);
          this.targetSectionFormGroup.controls.name.reset();
        },
        err => {
          if (err.status === 500 && this.viewingstate === VIEW_LIST[1]) {
            const formGroup: FormGroup = this.targetSectionFormGroup;
            const createOREditHVA = formGroup.controls['name'];
            createOREditHVA.setErrors({ nameNotAvailable: true });
          }
          this.spinner.showSpinner.next(false);
          console.log('Error! updateHVA : ' + err);
      });
    }
  }

  downloadSampleCsv() {
    this.downloadCSV = true;
    const headers: string[] = ['NPI'];
    const data: any[] = [
      [1821080359],
      [1437152394],
      [1083606297],
      [1821095266]
    ];

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: headers,
      nullToEmptyString: true
    };
    const csv = new Angular5Csv(data, 'HVASamplefile', options);
  }

  fileChangeListener($event: any): void {
    this.downloadCSV = false;
    if (this.uploadFormGroup.valid && this.audienceTarget && this.audienceTarget.name && this.audienceTarget.name.length > 0 ) {
      this.uploadSectionNameExists = true;
      this.fileUploadInitate = true;
      this.event = new MouseEvent('click', { bubbles: false });
      this.fileInput.nativeElement.dispatchEvent(this.event);
    } else {
      this.uploadSectionNameExists = false;
      this.fileUploadInitate = true;
    }
  }

  selectFile(event) {
    this.invalidFile.status = false;
    if (event && event.target && event.target.files && event.target.files.length > 0 &&
        event.target.files[0].name.toLocaleLowerCase().endsWith('.csv')) {
          const input = event.target;
          const reader = new FileReader();
          const that = this;
          reader.readAsText(input.files[0]);
          reader.onload = (data) => {
            const csvData = reader.result;
            const csvRecordsArray = csvData.toString().split(/\r\n|\n/);
            const validateData = this.validateHCPCSV(csvRecordsArray);
            if (validateData === true) {
              that.selectedFiles = event.target.files;
              that.selectedFileName = that.selectedFiles.item(0).name;
            } else {
              that.invalidFile.status = true;
              that.invalidFile.message = 'Uploaded CSV file contains invalid data, please provide a valid CSV file.';
            }
          };
          reader.onerror = function() {
            that.invalidFile.status = true;
            that.invalidFile.message = 'Uploaded CSV file contains invalid data, please provide a valid CSV file.';
          };
    } else {
      this.invalidFile.status = true;
      this.invalidFile.message = 'Invalid file selection, Only "CSV" file formats will be supported';
    }
  }

  validateHCPCSV(csvRecordsArr: any) {
    const headers = (<string>csvRecordsArr[0]).split(',');
    const headerArray = [];
    for (let i = 0; i < headers.length; i++) {
      if (headers[i] === 'NPI') {
        headerArray.push(headers[i]);
      }
    }
    if (headerArray.length === 1) {
      for (let i = 1; i < (csvRecordsArr.length - 1); i++) {
        const data = (<string>csvRecordsArr[i]).split(',');

        // Iterating data
        if (data.length === headerArray.length) {
          if (!data[0].trim().match(/^\d{10}$/)) {// US : NPI's has 10 numeric digits
            return false;
          }
        }
      }
    } else {
      return false;
    }
    return true;
  }

  saveHVAForUpload() {
    if (!this.selectedFiles || !this.selectedFiles.item(0)) {
      this.invalidFile.status = true;
      this.invalidFile.message = 'No file is selected';
      return;
    }

    const file = this.selectedFiles.item(0);
    // removing extension from file name
    const fileName = file.name.substring(0, file.name.lastIndexOf('.'));
    const that = this;
    let eventProperties = {
      HVAName: this.audienceTarget.name
    }
    that.amplitudeService.logEvent('campaign_creation - upload audience', eventProperties);
    this.uploadService.uploadFileAndGetResult(file, fileName,
      that.audienceTarget.brandId, 'csv').send(function (err, data) {
        if (err) {
          that.spinner.showSpinner.next(false);
          return false;
        }
        that.spinner.showSpinner.next(true);
        that.audienceTarget.isFileUpload = true;
        that.audienceTarget.uploadAudienceTargetDetails = {
          'actualFile': data['Location']
        };
        that.audienceHTTPService.createOrUploadHVA(that.audienceTarget).subscribe(
          resAud => {
            that.spinner.showSpinner.next(false);
            that.audienceTarget.name = '';
            that.audienceTarget.uploadAudienceTargetDetails = { };
            that.selectedFileName = '';
            that.changeState(VIEW_LIST[0]);
          },
          errAud => {
            if (errAud.status === 500) {
              that.invalidFile.status = true;
              that.invalidFile.message = errAud.error.message;
            } else {
              const formGroup: FormGroup = that.uploadFormGroup;
              const createHVACtrl = formGroup.controls['HVAName'];
              createHVACtrl.setErrors({ nameNotAvailable: true });
            }
            that.spinner.showSpinner.next(false);
            console.log('Error! uploadHVA create : ' + errAud);
          });
      });
  }

  updateHVAForUpload() {
    const that = this;
    if (this.selectedFiles && this.selectedFiles.item(0)) {
      const file = this.selectedFiles.item(0);
      this.uploadService.uploadFileAndGetResult(file, file.name,
        that.audienceTarget.brandId, 'csv').send(function (err, data) {
          if (err) {
            that.spinner.showSpinner.next(false);
            return false;
          }
          that.spinner.showSpinner.next(true);
          that.audienceTarget.isFileUpload = true;
          that.audienceTarget.uploadAudienceTargetDetails = {
            'actualFile': data['Location']
          };
          that.audienceHTTPService.updateOrUploadHVA(that.audienceTarget).subscribe(
            resAud => {
              that.spinner.showSpinner.next(false);
              that.audienceTarget.name = '';
              that.audienceTarget.uploadAudienceTargetDetails = { };
              that.selectedFileName = '';
              that.changeState(VIEW_LIST[0]);
            },
            errAud => {
              if (errAud.status === 500) {
                that.invalidFile.status = true;
                that.invalidFile.message = errAud.error.message;
              } else {
                const formGroup: FormGroup = that.uploadFormGroup;
                const createHVACtrl = formGroup.controls['HVAName'];
                createHVACtrl.setErrors({ nameNotAvailable: true });
              }
              that.spinner.showSpinner.next(false);
              console.log('Error! uploadHVA update : ' + errAud);
            });
        });
    } else {
      /*that.invalidFile.status = true;
      that.invalidFile.message = 'Warning ! No file has been changed to update';*/
      that.spinner.showSpinner.next(true);
      that.audienceTarget.isFileUpload = true;
      that.audienceHTTPService.updateOrUploadHVA(that.audienceTarget).subscribe(
        resAud => {
          that.spinner.showSpinner.next(false);
          that.audienceTarget.name = '';
          that.audienceTarget.uploadAudienceTargetDetails = { };
          that.selectedFileName = '';
          that.changeState(VIEW_LIST[0]);
        },
        errAud => {
          if (errAud.status === 500) {
            that.invalidFile.status = true;
            that.invalidFile.message = errAud.error.message;
          } else {
            const formGroup: FormGroup = that.uploadFormGroup;
            const createHVACtrl = formGroup.controls['HVAName'];
            createHVACtrl.setErrors({ nameNotAvailable: true });
          }
          that.spinner.showSpinner.next(false);
          console.log('Error! uploadHVA update : ' + errAud);
        });
    }
  }

  refreshGeoMarkers() {
    this.markers = [];
    this.backupMarkers = [];
    let zipArray: any = [];
    const locations = this.audienceTarget.createAudienceTargetDetails.geoLocation.locations;
    if (locations.length > 0) {
      zipArray = locations.map((x: any) => {
        return x.zipcode;
      });
    }

    if (zipArray.length > 0) {
      this.utilHTTPService.getCityDetailsByZipCodes(zipArray).subscribe(responseList => {
        for (let i = 0; i < responseList.length; i++) {
          const data: any = responseList[i];
          if (data != null) {

            const newLocation: Marker = {
              lat: data.latitude,
              lng: data.longitude,
              label: data.city,
              zipcode: data.zipcode,
              stateFullName: data.stateFullName
            };
            if (this.checkMarkerInBounds(newLocation)) {
              this.markers.push(newLocation);
            }
          }
        }
        this.backupMarkers = this.markers;
        this.updateStatesAndMarkers();
      });
    } else {
      this.updateStatesAndMarkers();
    }

  }

  checkMarkerInBounds(newLocation: Marker) {
    if ((this.bounds.southEastLat <= newLocation.lat)
     && (newLocation.lat <= this.bounds.northWestLat)
      && (this.bounds.southEastLng >= newLocation.lng)
       && (newLocation.lng >= this.bounds.northWestLng)) {
        return true;
       } else {
         return false;
       }
  }

  zoomChange(event) {
    this.currentZoomLevel = event;
    this.refreshGeoMarkers();
  }
  editAudience(selectedAudience: AudienceListElement) {
    this.spinner.showSpinner.next(true);
    this.audienceHTTPService.getSavedAudienceDetails(selectedAudience._id).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        if (!selectedAudience.audienceUploaded) {
          this.viewingstate = VIEW_LIST[1];
          this.items = [this.locationsTreeList];
          this.potentialContent = true;
          this.spinner.showSpinner.next(true);
          setTimeout(() => {
            this.audienceTarget = <TargetElement>res;
            this.audienceTarget.createAudienceTargetDetails.geoLocation.isInclude =
                            this.audienceTarget.createAudienceTargetDetails.geoLocation.isInclude.toString();
            this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.isInclude =
                            this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.isInclude.toString();
            this.spinner.showSpinner.next(false);
            if (this.audienceTarget.createAudienceTargetDetails.geoLocation.locations.length > 0) {
              const tempLocations = this.audienceTarget.createAudienceTargetDetails.geoLocation.locations;
                const temp = [];
                for (const obj of tempLocations) {
                  temp.push(obj.zipcode);
                }
                if ( this.locationList && this.locationList.length > 0) {
                  this.addMoreLoactionsLatest(temp);
                  this.ddTreeLocations.onSelect(temp);
                  this.ddTreeLocations.updateItems(this.items, temp);
                } else {
                  this.targetSectionFormGroup.controls.locationsAdded.valueChanges.pipe(debounceTime(1000)).subscribe(
                    value => {
                      if (value && value.length === 0 && this.locationList && this.locationList.length > 0) {
                        this.addMoreLoactionsLatest(temp);
                        this.ddTreeLocations.onSelect(temp);
                        this.ddTreeLocations.updateItems(this.items, temp);
                      }
                    }
                  );
                }
            }
            if (this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization.length > 0) {
              this.ddTreeSpecilaziton.onSelect(
                      this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization);
              this.ddTreeSpecilaziton.updateItems(this.specializationItems,
                this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization);
            }
            this.updateGenderControls();
            this.updateBehaviourControls();
            this.getPotentialReach();
          }, 1000);
        } else if (selectedAudience.audienceUploaded) {
          this.invalidFile.status = false;
          this.viewingstate = VIEW_LIST[2];
          this.selectedFiles = null;
          this.spinner.showSpinner.next(true);
          setTimeout(() => {
            this.audienceTarget = <TargetElement>res;
            if (this.audienceTarget.uploadAudienceTargetDetails && this.audienceTarget.uploadAudienceTargetDetails.actualFile) {
              let fileName: string = this.audienceTarget.uploadAudienceTargetDetails.actualFile;
              fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
              const ext = fileName.substring(fileName.lastIndexOf('.'));
              fileName = fileName.substring(0, fileName.lastIndexOf('-')) + ext;
              this.selectedFileName = fileName;
            }
            this.spinner.showSpinner.next(false);
            this.getUploadedPotentialReach();
          }, 1000);
        }
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error editAudience!');
      }
    );
  }
  deletAudience(selectedAudience: AudienceListElement) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete "' + selectedAudience.name + '" Audience',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.spinner.showSpinner.next(true);
        this.audienceHTTPService.deleteHVA(selectedAudience._id).subscribe(
          res => {
            this.spinner.showSpinner.next(false);
            this.getSavedAudienceList();
          },
          err => {
            this.spinner.showSpinner.next(false);
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 5000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err['error']['message']
              }
            });
            console.log('Error deletAudience!');
          }
        );
      } else {
        dialogRef.close();
      }
    });
  }
  updateStatesAndMarkers() {
    if(this.zone === '1') {
      if (this.currentZoomLevel > 4) {
        this.markers = this.backupMarkers;
      } else {
        this.markers = [];
      }
    }
    else if(this.zone ==='2') {
      if (this.currentZoomLevel > 6) {
        this.markers = this.backupMarkers;
      } else {
        this.markers = [];
      }
    }
    
    this.newGeoJson = {
      'type': 'FeatureCollection',
      'features': []
    };

    const statesGeoJSON = this.geoJsonObject.features;

    const stateCodeMap = new Map();

    for (let i = 0; i < statesGeoJSON.length; i++) {
      const stateObj = statesGeoJSON[i].properties;
      stateCodeMap.set(stateObj.NAME, i);
    }

    const stateIndexSet = new Set();
    this.backupMarkers.forEach((bmarker: any) => {
      const geoStateObj = statesGeoJSON[stateCodeMap.get(bmarker.stateFullName)];
      if (!stateIndexSet.has(stateCodeMap.get(bmarker.stateFullName))) {
        if (statesGeoJSON[stateCodeMap.get(bmarker.stateFullName)] !== undefined) {
          this.newGeoJson.features.push(statesGeoJSON[stateCodeMap.get(bmarker.stateFullName)]);
        }
        stateIndexSet.add(stateCodeMap.get(bmarker.stateFullName));
      }
    });
  }

  clickedDataLayer(event) {
    console.log('clicked region! :', event);
  }

  boundsChange(event) {
    this.bounds.southEastLng = event.Ta.i;  // event.Ta.g
    this.bounds.southEastLat = event.Ya.g;
    this.bounds.northWestLng = event.Ta.g; // event.Ya.i
    this.bounds.northWestLat = event.Ya.i;
    this.refreshGeoMarkers();
  }

  defaultEnableTargetSectionFields(): void {
    this.audienceTarget.createAudienceTargetDetails.geoLocation.isInclude = 'true';
    this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.isInclude = 'true';
    this.defaultSelectionCreateHVA = false;
    /*Location selection start*/
      if (this.locationList && this.locationList.length > 1 && this.locationList[1] &&
          this.locationList[1].children && this.locationList[1].children.length > 0) {
          const tempLocationList = [];
          for (const item of this.locationList[1].children) {
            tempLocationList.push(item.children); /*'item.children' is array list */
          }

          /*flattenedArray => each item/element has single/multiple('item.children') in 'tempLocationList'.
          To make single item for each array element we are concat the  tempLocationList to flattenedArray*/
          const flattenedArray = [].concat(...tempLocationList);

          /*Replacing 'value to zipcode' & 'text to city' and storing into this.su.tar.ge.locations*/
          this.audienceTarget.createAudienceTargetDetails.geoLocation.locations = flattenedArray.map(({ value: zipcode,
                                                  text: city, ...rest }) => ({ zipcode, city, ...rest }));

          /*selectedTempLocationList => we need only value from the 'flattenedArray' to pass treecomponent */
          const selectedTempLocationList = flattenedArray.map(key => key.value);

          this.ddTreeLocations.onSelect(selectedTempLocationList);
          this.ddTreeLocations.updateItems(this.items, selectedTempLocationList);
      }
    /*End of Location selection*/

    /*Specialization selection start*/
    if (this.specializationList && this.specializationList.length > 1 && this.specializationList[1] &&
      this.specializationList[1].children && this.specializationList[1].children.length > 0) {
        const tempspecializationList = [];
        for (const item of this.specializationList[1].children) {
          tempspecializationList.push((item.children && item.children.length > 0) ? 
          item.children : item); /*'item.children' is array list */
        }

        /*flattenedArray => each item/element has single/multiple('item.children') in 'tempspecializationList'.
        To make single item for each array element we are concat the  tempspecializationList to flattenedArray*/
        const flattenedArray = [].concat(...tempspecializationList);

        /*Replacing 'value to zipcode' & 'text to city' and storing into this.su.tar.de.sp.areaOfSpecialization*/
        this.audienceTarget.createAudienceTargetDetails.demoGraphics.specialization.areaOfSpecialization =
                                                                        flattenedArray.map(key => key.value);

        /*selectedTempLocationList => we need only value from the 'flattenedArray' to pass treecomponent */
        const selectedTempspecializationList = flattenedArray.map(key => key.value);

        this.ddTreeSpecilaziton.onSelect(selectedTempspecializationList);
        this.ddTreeSpecilaziton.updateItems(this.specializationItems, selectedTempspecializationList);

    }
    /*End of Specialization selection*/

    this.audienceTarget.createAudienceTargetDetails.demoGraphics.gender = this.genders;
    this.updateGenderControls();
    this.audienceTarget.createAudienceTargetDetails.demoGraphics.behaviours = this.behaviourList;
    this.updateBehaviourControls();
    // this.refreshGeoMarkers();
  }

}
interface Marker {
  lat: number;
  lng: number;
  label?: string;
  zipcode?: string;
  stateFullName?: string;
}
