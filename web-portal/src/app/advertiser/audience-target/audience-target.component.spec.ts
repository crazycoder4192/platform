import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudienceTargetComponent } from './audience-target.component';

describe('AudienceTargetComponent', () => {
  let component: AudienceTargetComponent;
  let fixture: ComponentFixture<AudienceTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudienceTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudienceTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
