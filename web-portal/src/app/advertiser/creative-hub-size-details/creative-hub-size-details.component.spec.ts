import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreativeHubSizeDetailsComponent } from './creative-hub-size-details.component';

describe('CreativeHubSizeDetailsComponent', () => {
  let component: CreativeHubSizeDetailsComponent;
  let fixture: ComponentFixture<CreativeHubSizeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreativeHubSizeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreativeHubSizeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
