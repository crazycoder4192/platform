import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-creative-hub-size-details',
  templateUrl: './creative-hub-size-details.component.html',
  styleUrls: ['./creative-hub-size-details.component.scss']
})
export class CreativeHubSizeDetailsComponent implements OnInit {
  creativeHub: any;

  constructor(
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public creativeHubData: any
    ) { }

  ngOnInit() {
    this.creativeHub = this.creativeHubData;
  }

}
