import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { PaypalService } from 'src/app/_services/advertiser/paypal.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-failure',
  templateUrl: './payment-failure.component.html',
  styleUrls: ['./payment-failure.component.scss']
})
export class PaymentFailureComponent implements OnInit {
  id: any;
  constructor(private spinner: SpinnerService, private snackBar: MatSnackBar,
     private paypalService: PaypalService,
     private _routeParams: ActivatedRoute) {
      this._routeParams.queryParams.subscribe(params => {
        this.id = params['token'];
      });
  }

  ngOnInit() {
    this.spinner.showSpinner.next(true);
    this.paypalService.paymentFailure(this.id).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
      },
      err => {
        this.spinner.showSpinner.next(false);
      });
  }

}
