import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { PaymentmethodService } from 'src/app/_services/advertiser/paymentmethod.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UtilService } from '../../../_services/utils/util.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class InvoicesComponent implements OnInit {
  invoiceList: any[] = [];
  filteredInvoiceList: any[] = [];
  show = false;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  displayedColumns: string[] = ['Date', 'Invoice number', 'Payment method', 'Total', 'Status'];
  expandedElement: null;
  totalPendingAmout = 0;
  private currencySymbol : string;
  loaded = false;
  constructor(private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private spinner: SpinnerService,
    private paymentMethodService: PaymentmethodService,
    private utilService: UtilService) { }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.renderListView();
  }

  toggleSearch() {
    this.show = !this.show;
  }

  renderListView() {
    this.paymentMethodService.getInvoices().subscribe(
      result => {
        this.loaded = true;
        this.invoiceList = <[]>result;
        this.filteredInvoiceList = this.invoiceList;
        this.filteredInvoiceList.forEach(item => {
          if (item.invoiceStatus === 'pending') {
            this.totalPendingAmout += item.amount;
          }
        }
        );
      },
      err => {
        console.log('Error! getAllInvoices : ' + err);
      });
  }

  downloadInvoce(id) {
    this.paymentMethodService.downloadInvoice(id).subscribe(
      data => {
        const file = new Blob([data], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(file);
          return;
        } else {
          const timestamp = new Date().valueOf();
          const url = window.URL.createObjectURL(file);
          const link = window.document.createElement('a');
          link.href = url;
          link.download = 'Invoice_' + timestamp;
          link.click();
          window.URL.revokeObjectURL(url);
          link.remove(); // remove the element
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  emailInvoice(id) {
    this.paymentMethodService.emailInvoice(id).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }

}
