import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { UploadToS3Service } from 'src/app/common/upload-to-s3/upload-to-s3.service';
import { CountryCurrenciesModel } from 'src/app/_models/CountryCurrenciesModel';
import { ZipcodesService } from 'src/app/_services/utils/zipcodes.service';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AdvertiserCreditLineService } from 'src/app/_services/advertiser/advertiser-credit-line.service';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { ToolTipService } from '../../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UtilService } from '../../../_services/utils/util.service';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
import { integer } from 'aws-sdk/clients/cloudfront';

@Component({
  selector: 'app-credit-line',
  templateUrl: './credit-line.component.html',
  styleUrls: ['./credit-line.component.scss']
})
export class CreditLineComponent implements OnInit {
  @ViewChild('file') fileInput: ElementRef;
  public creditLineForm: FormGroup;
  isSubmitted = false;
  public uniqueCities: string[];
  public uniqueStates: string[];
  public filteredStates: Observable<string[]>;
  public filteredCities: Observable<string[]>;
  public filteredCountries: any;
  public selectedFiles: FileList;
  public isValidCity: boolean;
  page: integer = 1;
  event: MouseEvent;
  documents: { name: string; type: string; uploadStatus: boolean; }[];
  index: any;
  countrySelected: any;
  creditLine: any;
  status: any;
  uniqueZipCodes: any;
  filteredZipCodes: Observable<any>;
  isValidZipCode: any;
  advertiserCreditLine: any;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['advertiser_manage_payments_credit_line_apply_for_credit_line']);
  docType: any;
  isValidDocument: boolean;
  countriesCurrencies:  CountryCurrenciesModel[];
  public cityStateDetails: any;
  public displayedColumns: string[] = [
    'askingCreditLimit', 'approvedCreditLimit', 'creditPeriod',
    'status', 'comments', 'applyNewCreditLine'];
  public pageView: string;
  creditLineDataSource: any[];
  isDateAvailabe: boolean;
  isDocumentHidden: boolean;
  minDate: Date;
  minStartDate: Date;
  latestApprovedDate: Date;
  latestApprovedAmount: any;
  private currencySymbol : string;
  constructor(
    private formBuilder: FormBuilder,
    private uploadService: UploadToS3Service,
    private zipcodesService: ZipcodesService,
    private utilService: UtilService,
    private configurationService: ConfigurationService,
    private spinner: SpinnerService,
    private advertiserCreditLineService: AdvertiserCreditLineService,
    private snackBar: MatSnackBar,
    private toolTipService: ToolTipService,
    private dialog: MatDialog,
    private amplitudeService: AmplitudeService
  ) {
    this.creditLine = {
      name: '',
      accept: '',
      email: '',
      companyName: '',
      companyAddress: '',
      streetAddress: '',
      phoneNumber:'',
      billingEmail:'',
      hasOrganizationRegistered: '',
      companyContactName:'',
      expectedIncome: '',
      dunsNumber: '',
      country:'',
      state:'',
      city:'',
      taxId: '',
      zipCode:'',
      billingContactName: '',
      startDate:'',
      documents: [
        {
          location: '',
          type: ''
        }
      ]
    };
    this.creditLine.documents.pop();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.countrySelected = 'null';
    this.createForm();
    this.minStartDate = new Date();
    this.initDocuments();
    this.getCriditLimit();
    this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }
  getCriditLimit() {
    this.pageView = 'listView';
    this.advertiserCreditLineService.getAdvertiserCreditLine().subscribe(
      res => {
        this.creditLineDataSource = res;
        this.minDate = new Date();
        this.latestApprovedDate = new Date();
        this.creditLineDataSource.forEach(element => {
          if (new Date(element.startDate) > this.minDate) {
            this.minDate = new Date(element.startDate);
          }
          if (element.status === 'Approved') {
            this.latestApprovedDate = new Date(element.endDate);
            this.latestApprovedAmount = element.approvedCreditLimit;
          }
        });
      },
      err => {
        console.log('Error!');
      });
  }
  renderAddView() {
    this.page = 1;
    this.initUniqueStates();
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = [];
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
        this.filteredCountries = this.creditLineForm.controls['country'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterCountry(name))
          );
      }
    );
    this.initUniqueCities();
    this.isSubmitted = false;
    if(this.creditLineDataSource.length){
      const approvedStatusesData = this.creditLineDataSource.filter((item)=> item.status === 'Approved')
      if(approvedStatusesData.length){
        const approvedStatusData = approvedStatusesData[approvedStatusesData.length -1];
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
          title: `Do you want to fetch the last approved application details?`,
          message: '',
          confirm: 'Yes',
          cancel: 'No'
        };
        dialogConfig.minWidth = 400;
        const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.setFormData(approvedStatusData);
            dialogRef.close();
          } else {
            dialogRef.close();
            this.resetFormData()
          }
        })
      }
      else {
        this.resetFormData();
      }
    }
    else {
      this.resetFormData();
    }
  }

  resetFormData(){
    this.pageView = 'addView';
    this.advertiserCreditLine = null;
    this.creditLineForm.get('comment').setValue(null);
    this.creditLineForm.get('hasOrganizationRegistered').setValue(null);
    this.creditLineForm.get('accept').setValue(null);
    this.creditLineForm.get('companyContactName').setValue(null);
    this.creditLineForm.get('taxId').setValue(null);
    this.creditLineForm.get('billingContactName').setValue(null);
    this.creditLineForm.get('billingEmail').setValue(null);
    this.creditLineForm.get('dunsNumber').setValue(null);
    this.creditLineForm.get('phoneNumber').setValue(null);
    this.creditLineForm.get('expectedIncome').setValue(null);
    this.creditLineForm.get('state').setValue(null);
    this.creditLineForm.get('city').setValue(null);
    this.creditLineForm.get('country').setValue(null);
    this.creditLineForm.get('zipCode').setValue(null);
    this.creditLineForm.get('name').setValue(null)
    this.creditLineForm.get('email').setValue(null)
    this.creditLineForm.get('companyName').setValue(null)
    this.creditLineForm.get('companyAddress').setValue(null)
    this.creditLineForm.get('streetAddress').setValue(null)
    this.creditLineForm.get('startDate').setValue(null);
    this.creditLine.documents = [];
    this.documents.forEach(doc => {
        doc.uploadStatus = false;
    });
    this.isDocumentHidden = true;
    this.amplitudeService.logEvent('credit_limit - click apply new', null)
  }

  setFormData(approvedStatusData){
    this.pageView = 'editView';
    this.advertiserCreditLine = null;
    this.creditLineForm.get('comment').setValue(approvedStatusData.comment);
    this.creditLineForm.get('hasOrganizationRegistered').setValue(approvedStatusData.hasOrganizationRegistered);
    this.creditLineForm.get('accept').setValue(approvedStatusData.accept);
    this.creditLineForm.get('companyContactName').setValue(approvedStatusData.companyContactName);
    this.creditLineForm.get('taxId').setValue(approvedStatusData.taxId);
    this.creditLineForm.get('billingContactName').setValue(approvedStatusData.billingContactName);
    this.creditLineForm.get('billingEmail').setValue(approvedStatusData.billingEmail);
    this.creditLineForm.get('phoneNumber').setValue(approvedStatusData.phoneNumber);
    this.creditLineForm.get('expectedIncome').setValue(approvedStatusData.expectedIncome);
    this.creditLineForm.get('dunsNumber').setValue(approvedStatusData.dunsNumber)
    this.creditLineForm.get('state').setValue(approvedStatusData.state);
    this.creditLineForm.get('city').setValue(approvedStatusData.city);
    this.creditLineForm.get('country').setValue(approvedStatusData.country);
    this.creditLineForm.get('zipCode').setValue(approvedStatusData.zipCode);
    this.creditLineForm.get('name').setValue(approvedStatusData.name);
    this.creditLineForm.get('email').setValue(approvedStatusData.email);
    this.creditLineForm.get('companyAddress').setValue(approvedStatusData.companyAddress);
    this.creditLineForm.get('companyName').setValue(approvedStatusData.companyName);
    this.creditLineForm.get('streetAddress').setValue(approvedStatusData.streetAddress);
    this.creditLineForm.get('startDate').setValue(null);
    this.creditLineForm.get('accept').setValue("true");
    this.creditLine.documents = approvedStatusData.documents;
    this.creditLine.documents.forEach(element => {
      this.documents.forEach(doc => {
        if (element.type === doc.type) {
          doc.uploadStatus = true;
        }
      });
    });
    this.isDocumentHidden = false;
  }

  renderEditView(creditLine) {
    this.pageView = 'editView';
    this.advertiserCreditLine = creditLine;
   
    this.page = 1;
    if (this.advertiserCreditLine) {
      this.creditLineForm.get('comment').setValue(this.advertiserCreditLine.comment);
      this.creditLineForm.get('hasOrganizationRegistered').setValue(this.advertiserCreditLine.hasOrganizationRegistered);
      this.creditLineForm.get('accept').setValue(this.advertiserCreditLine.accept);
      this.creditLineForm.get('companyContactName').setValue(this.advertiserCreditLine.companyContactName);
      this.creditLineForm.get('taxId').setValue(this.advertiserCreditLine.taxId);
      this.creditLineForm.get('billingContactName').setValue(this.advertiserCreditLine.billingContactName);
      this.creditLineForm.get('billingEmail').setValue(this.advertiserCreditLine.billingEmail);
      this.creditLineForm.get('phoneNumber').setValue(this.advertiserCreditLine.phoneNumber);
      this.creditLineForm.get('expectedIncome').setValue(this.advertiserCreditLine.expectedIncome);
      this.creditLineForm.get('dunsNumber').setValue(this.advertiserCreditLine.dunsNumber)
      this.creditLineForm.get('state').setValue(this.advertiserCreditLine.state);
      this.creditLineForm.get('city').setValue(this.advertiserCreditLine.city);
      this.creditLineForm.get('country').setValue(this.advertiserCreditLine.country);
      this.creditLineForm.get('zipCode').setValue(this.advertiserCreditLine.zipCode);
      this.creditLineForm.get('name').setValue(this.advertiserCreditLine.name);
      this.creditLineForm.get('email').setValue(this.advertiserCreditLine.email);
      this.creditLineForm.get('companyAddress').setValue(this.advertiserCreditLine.companyAddress);
      this.creditLineForm.get('companyName').setValue(this.advertiserCreditLine.companyName);
      this.creditLineForm.get('streetAddress').setValue(this.advertiserCreditLine.streetAddress);
      this.creditLineForm.get('startDate').setValue(this.advertiserCreditLine.startDate);
      this.creditLineForm.get('accept').setValue("true");
      this.creditLine.documents = this.advertiserCreditLine.documents;
      this.creditLine.documents.forEach(element => {
        this.documents.forEach(doc => {
          if (element.type === doc.type) {
            doc.uploadStatus = true;
          }
        });
      });
      this.isDocumentHidden = false;
    }
  }

  renderListView() {
    this.pageView = 'listView';
  }

  initDocuments() {
    this.documents = [{
      name: 'Document',
      type:  'pan',
      uploadStatus: false
    },
   ];
  }
  createForm() {
    this.creditLineForm = this.formBuilder.group({
      comment: ['', Validators.compose([Validators.required])],
      taxId: ['', Validators.compose([Validators.required])],
      accept: ['', Validators.compose([Validators.required])],
      companyContactName: ['', Validators.compose([Validators.required])],
      hasOrganizationRegistered: ['', Validators.compose([Validators.required])],
      billingContactName: ['', Validators.compose([Validators.required])],
      billingEmail: ['', Validators.compose([Validators.required, Validators.email])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      dunsNumber: ['', Validators.compose([Validators.required])],
      expectedIncome: ['', Validators.compose([Validators.required])],
      country: ['', Validators.compose([Validators.required])],
      state: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      zipCode: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      companyName: ['', Validators.compose([Validators.required])],
      companyAddress: ['', Validators.compose([Validators.required])],
      streetAddress: ['', Validators.compose([Validators.required])],
      startDate: ['', Validators.compose([Validators.required])],
    });
  }
  openFileSelectionDialog() {
    if (!this.advertiserCreditLine || (this.advertiserCreditLine && this.advertiserCreditLine.status !== 'Approved')) {
      if(this.documents[0].uploadStatus = true && !this.isDocumentHidden) {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: 'You have to remove the selected document first.'
          }
        });
      }
      else {
        this.docType = this.documents[0].type;
        this.selectedFiles = null;
        this.event = new MouseEvent('click', {bubbles: false});
        this.fileInput.nativeElement.dispatchEvent(this.event);
      }
    }
  }
  applyCreditLine() {
    if(this.advertiserCreditLine && this.advertiserCreditLine.status === 'Approved'){
      this.pageView = 'listView';
      return this.pageView;
    }
    this.isSubmitted = true;
    if (this.validateCreditLine() && this.creditLine) { /*&& this.creditLine.documents.length*/
      this.creditLine.comment = this.creditLineForm.get('comment').value;
      this.creditLine.hasOrganizationRegistered = this.creditLineForm.get('hasOrganizationRegistered').value
      this.creditLine.companyContactName = this.creditLineForm.get('companyContactName').value
      this.creditLine.taxId = this.creditLineForm.get('taxId').value
      this.creditLine.billingContactName = this.creditLineForm.get('billingContactName').value
      this.creditLine.billingEmail = this.creditLineForm.get('billingEmail').value
      this.creditLine.phoneNumber = this.creditLineForm.get('phoneNumber').value
      this.creditLine.dunsNumber = this.creditLineForm.get('dunsNumber').value
      this.creditLine.expectedIncome = this.creditLineForm.get('expectedIncome').value
      this.creditLine.state = this.creditLineForm.get('state').value
      this.creditLine.city = this.creditLineForm.get('city').value
      this.creditLine.country = this.creditLineForm.get('country').value
      this.creditLine.zipCode = this.creditLineForm.get('zipCode').value
      this.creditLine.email = this.creditLineForm.get('email').value
      this.creditLine.companyName = this.creditLineForm.get('companyName').value
      this.creditLine.companyAddress = this.creditLineForm.get('companyAddress').value
      this.creditLine.streetAddress = this.creditLineForm.get('streetAddress').value
      this.creditLine.name = this.creditLineForm.get('name').value
      this.creditLine.startDate = this.creditLineForm.get('startDate').value;
      this.creditLine.comment = this.creditLineForm.get('comment').value;
      if (this.advertiserCreditLine && this.advertiserCreditLine._id) {
        this.creditLine._id = this.advertiserCreditLine._id;
        this.advertiserCreditLineService.updateAdvertiserCreditLine(this.creditLine).subscribe(
          res => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: 'Updated successfully'
              }
            });
            this.getCriditLimit();
            this.amplitudeService.logEvent('credit_limit - applied', null);
          },
          err => {
            console.log('Error!');
          });
      } else {
        this.advertiserCreditLineService.saveAdvertiserCreditLine(this.creditLine).subscribe(
          res => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: 'Applied successfully'
              }
            });
            this.getCriditLimit();
            this.amplitudeService.logEvent('credit_limit - applied', null);
          },
          err => {
            console.log('Error!');
          });
      }
    }
  }
  selectFile(event: { target: { files: FileList; }; }) {
    this.selectedFiles = event.target.files;
    this.prepareCreditLineDocs(0);
    this.documents[0].uploadStatus = true;
    this.isDocumentHidden = false;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  removeFile(docType, index) {
    if(this.advertiserCreditLine && this.advertiserCreditLine.status === "Approved") {
      return null;
    }
    this.documents[index].uploadStatus = false;
    this.creditLine.documents = this.creditLine.documents.filter(obj => obj.type !== docType);
    this.snackBar.openFromComponent(CustomSnackbarComponent, {
      duration: 3000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass: ['snackbar-advertiser'],
      data: {
        icon: 'success',
        message: 'Document removed successfully'
      }
    });
    this.isDocumentHidden = true;
  }
  prepareCreditLineDocs(fileIndex) {
    const file = this.selectedFiles.item(fileIndex);
    this.spinner.showSpinner.next(true);
    const that = this;
    const fileName = file.name;
    const fileExtension = file.type;
    this.uploadService.fetchBrandIdentifierS3();
    this.uploadService._brandIdentifySource.subscribe(
      brandIdentifier => {
        this.uploadService.uploadFileAndGetResult(file, fileName, brandIdentifier, fileExtension).send(function(err, data) {
          if (err) {
            that.spinner.showSpinner.next(false);
            return false;
          }
          that.creditLine.documents.push({
            location: data['Location'],
            type: that.docType
          });
          that.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: 'Document uploaded successfully'
            }
          });
          that.spinner.showSpinner.next(false);
        });
      }
    );
  }

  validCreditLineDetail() :boolean {
    let isValid = true;
    const hasOrganizationRegistered = this.creditLineForm.controls['hasOrganizationRegistered']
    const state = this.creditLineForm.controls['state']
    const country = this.creditLineForm.controls['country']
    const zipCode = this.creditLineForm.controls['zipCode']
    const city = this.creditLineForm.controls['city']    
    const name = this.creditLineForm.controls['name']
    const email = this.creditLineForm.controls['email']
    const companyName = this.creditLineForm.controls['companyName']
    const companyAddress = this.creditLineForm.controls['companyAddress']
    const streetAddress = this.creditLineForm.controls['streetAddress']
    if (!city.value) {
      city.setErrors({ require: true });
      isValid = false;
    }
    if (!state.value) {
      state.setErrors({ require: true });
      isValid = false;
    }
    if (!country.value) {
      country.setErrors({ require: true });
      isValid = false;
    }
    if (!companyAddress.value) {
      companyAddress.setErrors({ require: true });
      isValid = false;
    }
    if (!zipCode.value) {
      zipCode.setErrors({ require: true });
      isValid = false;
    }
    if (hasOrganizationRegistered.value === null) {
      hasOrganizationRegistered.setErrors({ require: true });
      isValid = false;
    }
    if (!name.value) {
      name.setErrors({ require: true });
      isValid = false;
    }
    if(email.status === "INVALID"){
      isValid = false;
    }
    if (!email.value) {
      email.setErrors({ require: true });
      isValid = false;
    }
    if (!streetAddress.value) {
      streetAddress.setErrors({ require: true });
      isValid = false;
    }
    if (!companyName.value) {
      companyName.setErrors({ require: true });
      isValid = false;
    }
    if(!this.creditLine.documents.length) {
      this.isValidDocument = true;
      isValid = false;
    }
    return isValid

  }
  validateCreditLine(): boolean {
    const comment = this.creditLineForm.controls['comment'];
    const startDate = this.creditLineForm.controls['startDate'];
    let isValid = true;
    const accept = this.creditLineForm.controls['accept']
    const companyContactName = this.creditLineForm.controls['companyContactName']
    const taxId = this.creditLineForm.controls['taxId']
    const billingContactName = this.creditLineForm.controls['billingContactName']
    const billingEmail = this.creditLineForm.controls['billingEmail']
    const expectedIncome = this.creditLineForm.controls['expectedIncome']
    if (!comment.value) {
      comment.setErrors({ require: true });
      isValid = false;
    }
    if (!accept.value) {
      accept.setErrors({ require: true });
      isValid = false;
    }
    if (!billingContactName.value) {
      billingContactName.setErrors({ require: true });
      isValid = false;
    }
    if (!taxId.value) {
      taxId.setErrors({ require: true });
      isValid = false;
    }
    if(billingEmail.status === "INVALID"){
      isValid = false;
    }
    if (!billingEmail.value) {
      billingEmail.setErrors({ require: true });
      isValid = false;
    }
    if (!expectedIncome.value) {
      expectedIncome.setErrors({ require: true });
      isValid = false;
    }
    if (!startDate.value) {
      startDate.setErrors({ require: true });
      isValid = false;
    }
    if (!companyContactName.value) {
      companyContactName.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }

  initUniqueCities() {
    this.zipcodesService.getUniqueCities(this.countrySelected).subscribe(
      res => {
        this.uniqueCities = res;
        this.filteredCities = this.creditLineForm.controls['city'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
          );
      },
      err => {
        console.log('Error!', err);
      });
  }
  initUniqueZipCodes() {
    this.zipcodesService.getUniqueZipCodes(this.countrySelected).subscribe(
      res => {
        this.uniqueZipCodes = res;
        this.filteredZipCodes = this.creditLineForm.controls['zipCode'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
          );
      },
      err => {
        console.log('Error!');
      });
  }
  private _filterState(name: string): string[] {
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueStates.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }

  private _filterCountry(value: string): CountryCurrenciesModel[] {
    let filterValue: string;
    if (value) {
      filterValue = value.toLowerCase();
    }
    const filteredCountries = this.countriesCurrencies.filter(
      countrycurrency =>
      countrycurrency.countryName.toLowerCase().indexOf(filterValue) === 0);
      return filteredCountries;
  }
  onChangeCity() {
    if (this.uniqueCities.length) {
      const cityName = this.creditLineForm.controls['city'].value;
      this.isValidCity = this.uniqueCities.includes(cityName);
      if (this.isValidCity) {
        this.creditLineForm.get('state').setValue('');
        this.utilService.getCityDetailsByCityName(cityName, this.countrySelected).subscribe(
          res => {
            this.cityStateDetails = res;
            this.uniqueStates = [];
            this.uniqueStates = this.cityStateDetails.map(function(item) {
              return item['stateFullName'];
            });
            this.uniqueStates = Array.from(new Set(this.uniqueStates));
            this.filteredStates = this.creditLineForm.controls['state'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => this._filterState(name))
            );
            this.uniqueZipCodes = [];
            this.uniqueZipCodes = this.cityStateDetails.map(function(item) {
              return item['zipcode'];
            });
            if (!this.uniqueZipCodes.includes(this.creditLineForm.controls['zipCode'].value)) {
              this.creditLineForm.get('zipCode').setValue('');
            }
            this.filteredZipCodes = this.creditLineForm.controls['zipCode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
          },
          err => {
            console.log(err);
          }
        );
        // get
      }
    }
  }
  onChangeCityString() {
    if (this.creditLineForm.controls['city'].value.length > 1) {
      this.zipcodesService.getUniqueCitiesOnChangeCity(this.creditLineForm.controls['city'].value, this.countrySelected).subscribe(
        res => {
          this.uniqueCities = res;
          this.filteredCities = this.creditLineForm.controls['city'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueCities();
    }
  }

  private _filterCity(name: string): string[] {
    this.onChangeCityString();
    this.onChangeCity();
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueCities.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }

  private _filterZipCode(name: string): any {
    this.onChangeZipCodeString();
    this.onChangeZipCode();
    const filterValue = name.toLowerCase();
    return this.uniqueZipCodes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  onChangeZipCodeString() {
    if (this.creditLineForm.controls['city'].value && this.creditLineForm.controls['city'].value.length > 1) {
      this.zipcodesService.getUniqueZipCodesOnChangeZipCode(this.creditLineForm.controls['zipCode'].value, this.countrySelected).subscribe(
        res => {
          this.uniqueZipCodes = res;
          this.filteredZipCodes = this.creditLineForm.controls['zipCode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueZipCodes();
    }
  }
  validatePincode() {
    const zipCodeCtrl = this.creditLineForm.controls['zipCode'];
    if (this.countrySelected === '1' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 5) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    } else if (this.countrySelected === '2' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 6) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    }
  }
  next(selectedPage) {
    if (selectedPage === 1) {
      this.page = 1;
    }
    else {
      if(this.validCreditLineDetail()) {
        this.page = 2;
      }
      else {
        this.page = 1;
      }
    }
  }
  onChangeZipCode() {
    if (this.uniqueZipCodes.length) {
      const zipCode = this.creditLineForm.controls['zipCode'].value;
      this.isValidZipCode = this.uniqueZipCodes.includes(zipCode);
      if (this.isValidZipCode) {
        this.creditLineForm.get('city').setValue('');
        this.creditLineForm.get('state').setValue('');
        // this.creditLineForm.get('country').setValue('');
        this.utilService.getCityDetailsByZipCode(zipCode, this.countrySelected).subscribe(
          res => {
            this.cityStateDetails = res;
            this.creditLineForm.get('city').setValue(this.cityStateDetails.city);
            this.creditLineForm.get('state').setValue(this.cityStateDetails.stateFullName);
            // this.creditLineForm.get('country').setValue(this.cityStateDetails.country);
          },
          err => {
            console.log(err);
          }
        );
        // get
      }
    }
  }

  initUniqueStates() {
    this.zipcodesService.getUniqueStates(this.countrySelected).subscribe(
      res => {
        this.uniqueStates = res;
        this.filteredStates = this.creditLineForm.controls['state'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterState(name))
          );
      });
  }

  selectedCountry(countryISO2Code) {
    const isValid = this.creditLineForm.get('city').value
    || this.creditLineForm.get('state').value || this.creditLineForm.get('zipCode').value ;
    if (isValid) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        title: `Are you sure you want to re-enter the address details?`,
        message: '',
        confirm: 'Yes',
        cancel: 'No'
      };
      dialogConfig.minWidth = 400;
      const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.getCountryDetails(countryISO2Code);
          if (this.advertiserCreditLine) {
            this.advertiserCreditLine.zipcode = null;
            this.advertiserCreditLine.state = null;
            this.advertiserCreditLine.city = null;
          }
          this.creditLineForm.get('city').setValue(null);
          this.creditLineForm.get('state').setValue(null);
          this.creditLineForm.get('zipCode').setValue(null);
          dialogRef.close();
        } else {
          const countryValue = this.countrySelected === '1' ? 'United States' : 'India';
          this.advertiserCreditLine.country = countryValue;
          this.creditLineForm.get('country').setValue(countryValue);
          dialogRef.close();
        }
      });
    } else {
      this.getCountryDetails(countryISO2Code);
    }

  }
  getCountryDetails(countryISO2Code) {
    this.countrySelected = countryISO2Code === 'US' ? '1' : '2';
    this.initUniqueZipCodes();
    this.initUniqueCities();
    this.initUniqueStates();
  }
}
