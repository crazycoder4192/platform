import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSecureContentComponent } from './payment-secure-content.component';

describe('PaymentSecureContentComponent', () => {
  let component: PaymentSecureContentComponent;
  let fixture: ComponentFixture<PaymentSecureContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentSecureContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSecureContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
