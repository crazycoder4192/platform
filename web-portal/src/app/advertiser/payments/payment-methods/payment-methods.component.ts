
import { Component, OnInit, ComponentRef, ViewContainerRef, ComponentFactoryResolver, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmPasswordDailogComponent } from 'src/app/common/confirm-password-dailog/confirm-password-dailog.component';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';
import { PaymentmethodService } from 'src/app/_services/advertiser/paymentmethod.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { PaymentMethod } from '../../../_models/advertiser/payment_method_model';
import { Brand } from '../../../_models/advertiser_brand_model';
import { CountryCurrenciesModel } from '../../../_models/CountryCurrenciesModel';
import { ConfigurationService } from '../../../_services/admin/configuration.service';
import { checkExpiryDate } from 'src/app/common/validators/validator';
import { ToolTipService } from '../../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { PaymentSecureContentComponent } from '../payment-secure-content/payment-secure-content.component';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
import { OptValidationComponent } from 'src/app/common/opt-validation/opt-validation.component';
import { SMSService } from 'src/app/_services/utils/sms.service';
import { ZipcodesService } from 'src/app/_services/utils/zipcodes.service';
import { startWith, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.component.html',
  styleUrls: ['./payment-methods.component.scss']
})
export class PaymentMethodsComponent implements OnInit {
  @Output() editModeOn: EventEmitter<any> = new EventEmitter();
  @Output() editModeOff: EventEmitter<any> = new EventEmitter();
  public paymentMethodList: PaymentMethod[] = [];
  public show = false;
  public submitted = false;
  public addPaymentMethodForm: FormGroup;
  public message;
  public status;
  public advertiserDetails;
  public advertiserDataObj;
  public pageView = 'listView';
  public editId;
  public editData;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  public id: string;
  public brandsList: Brand[];
  public Profile;
  public countriesCurrencies = [];
  public filteredcountriesCurrencies = [];
  public filteredCountries;
  public minDate: Date;
  public months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  public years = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028,
    2029, 2030, 2031, 2032, 2033, 2034, 2035];
  loaded = false;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['advertiser_manage_payments_payment_methods','advertiser_manage_payments_payment_methods_select_brands']);
  public windowReference: any;
  private zone;
  isPhoneNumberVerified = false;
  verifiedPhNumber: any;
  public countryCodes = ['+1', '+91'];
  isSameAsProfile = false;
  radioType = '';
  countryCodePhNumber: any;
  phoneNumber: any;
  uniqueCities: string[];
  filteredCities: any;
  countrySelected: string;
  uniqueZipCodes: any;
  filteredZipCodes: Observable<any>;
  constructor(
    private formBuilder: FormBuilder,
    private toolTipService: ToolTipService,
    private paymentMethodService: PaymentmethodService,
    private advertiserProfileService: AdvertiserProfileService,
    private advertiserBrandService: AdvertiserBrandService,
    private configurationService: ConfigurationService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router,
    private spinner: SpinnerService,
    private r: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
    private amplitudeService: AmplitudeService,
    private smsService: SMSService,
    private zipcodesService: ZipcodesService
  ) {
    // this.months = this.months.filter(item => item > new Date().getMonth());
    this.years = this.years.filter(item => item >= new Date().getFullYear());
  }

  ngOnInit() {
    this.radioType = 'no';
    this.zone = localStorage.getItem('zone');
    this.renderListView();
    this.advertiserProfileService.getAdvertiserByEmail(JSON.parse(localStorage.getItem('currentUser')).email).subscribe(
      res => {
        this.Profile = res;
        this.id = this.Profile._id;
      },
      err => {
        console.log('Error!');
      });
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
      }
    );
    this.createFormGroup();
    this.fetchTooltips(this.toolTipKeyList);
    this.initUniqueZipCodes();
  }
  initUniqueCities() {
    this.addPaymentMethodForm.controls['city'].setValue('');
    this.addPaymentMethodForm.controls['zipcode'].setValue('');
    this.addPaymentMethodForm.controls['phoneNumber'].setValue('');
    this.countrySelected = this.addPaymentMethodForm.controls['country'].value === 'India' ? '2' : '1';
    if ( this.countrySelected === '1' ) {
      this.addPaymentMethodForm.controls['countryCodePhNumber'].setValue('+1');
    } else if ( this.countrySelected === '2' ) {
      this.addPaymentMethodForm.controls['countryCodePhNumber'].setValue('+91');
    }
    this.zipcodesService.getUniqueCities(this.countrySelected).subscribe(
      res => {
        this.uniqueCities = res;
        this.uniqueCities = this.uniqueCities.filter((el, i, a) => i === a.indexOf(el));
        this.filteredCities = this.addPaymentMethodForm.controls['city'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
          );
      },
      err => {
        console.log('Error!');
      });
  }
  private _filterCity(name: string): string[] {
    this.onChangeCityString();
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueCities.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }
  onChangeCityString() {
    this.addPaymentMethodForm.controls['zipcode'].setValue('');
    if (this.addPaymentMethodForm.controls['city'].value.length > 1) {
      this.zipcodesService.getUniqueCitiesOnChangeCity(this.addPaymentMethodForm.controls['city'].value, this.countrySelected).subscribe(
        res => {
          this.uniqueCities = res;
          this.filteredCities = this.addPaymentMethodForm.controls['city'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueCities();
    }
    this.initUniqueZipCodes();
  }
  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }
  redirectSecure() {
    this.dialog.open(PaymentSecureContentComponent, {
      width: '50%',
      data: {
        'title': 'Payment Secure Content !'
      },
      panelClass: ['paymentSecureContent']
    });
  }
  renderListView() {
    this.pageView = 'listView';
    this.loaded = false;
    this.editModeOff.emit(null);
    if (this.addPaymentMethodForm) {
      this.addPaymentMethodForm.reset();
    }
    this.paymentMethodService.getPaymentMethods().subscribe(
      result => {
        this.paymentMethodList = <PaymentMethod[]>result;
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        console.log('Error! getAllPaymentMethods : ' + err);
      });
  }
  payNow() {
    this.spinner.showSpinner.next(true);
    this.paymentMethodService.payNow().subscribe(
      result => {
        this.spinner.showSpinner.next(false);
        window.location.href = result['message'];
      },
      err => {
        console.log('Error! getAllPaymentMethods : ' + err);
      });
  }
  public noWhitespaceValidator(control: FormControl) {
    let isWhitespace = false;
    if (control.value && control.value.trim(' ').length < 3 ) {
      isWhitespace = true;
    }
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  createFormGroup() {
    this.addPaymentMethodForm = this.formBuilder.group({
      // billingCountry: ['United States', Validators.required],
      // billingCurrency: ['USD', Validators.required],
      cardNumber: ['', Validators.compose([Validators.required, Validators.minLength(13),
      Validators.maxLength(16), Validators.pattern('^[0-9]*$')])],
      nameOnCard: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]+$'), this.noWhitespaceValidator])],
      expiryMonth: ['', Validators.required],
      expiryYear: ['', Validators.required],
      cvv: ['', Validators.compose([Validators.required, Validators.minLength(3),
        Validators.maxLength(4), Validators.pattern('^[0-9]*$')])],
      brands: ['', Validators.required],
      addressLine1: [''],
      addressLine2: [''],
      country: [''],
      city: [''],
      zipcode: [''],
      phoneNumber: [''],
      countryCodePhNumber: ['']
    }, {
      validator: [checkExpiryDate('expiryMonth', 'expiryYear')]
    });

    this.advertiserBrandService.getAllBrandsForCards().subscribe(
      advertiserBrandDetails => {
        this.brandsList = <Brand[]>advertiserBrandDetails;
      },
      err => {
        console.log('Error!');
      });
    this.editData = this.addPaymentMethodForm;
    this.amplitudeService.logEvent('payment_setup - click add', null);
  }
  onSelectionCountry(event: MatAutocompleteSelectedEvent) {
    this.filteredcountriesCurrencies = [];
    if (event.option.value) {
      const selected = this.countriesCurrencies.find(item => item.countryName === event.option.value);
      this.filteredcountriesCurrencies.push(selected);
      this.editData.billingCurrency = selected.currencyCode;
    }
  }
  onSelectionCurrency(event: MatAutocompleteSelectedEvent) {
    if (event.option.value) {
      const selected = this.countriesCurrencies.find(item => item.currencyCode === event.option.value);
      this.editData.billingCountry = selected.countryName;
    }
  }
  togglePaymentMethod(pageType) {
    this.pageView = pageType;
    this.show = !this.show;
    this.submitted = false;
    if (pageType === 'addView' || pageType === 'listView') {
      this.addPaymentMethodForm.reset();
    }
  }
  onSubmit() {
    this.submitted = true;
    if (this.addPaymentMethodForm.invalid) {
      if (this.radioType && this.radioType === 'yes') {
        this.validateAddress();
      }
      return;
    }
   if (this.radioType && this.radioType === 'yes') {
      if (!this.validateAddress()) {
        return;
      }
    }
    const addPaymentMethodRequest: PaymentMethod = Object.assign({}, this.addPaymentMethodForm.value);
    addPaymentMethodRequest.expiryDate = new Date(addPaymentMethodRequest.expiryDate);
    if (this.zone === '1') {
      addPaymentMethodRequest.billingCountry = 'United States';
      addPaymentMethodRequest.billingCurrency = 'USD';
    } else if (this.zone === '2') {
      addPaymentMethodRequest.billingCountry = 'India';
      addPaymentMethodRequest.billingCurrency = 'INR';
    }
    if (this.radioType && this.radioType === 'yes') {
      addPaymentMethodRequest.hasBillingAddress = true;
      addPaymentMethodRequest.isPhNumberVerified = this.isPhoneNumberVerified;
    } else {
      addPaymentMethodRequest.hasBillingAddress = false;
    }
    this.spinner.showSpinner.next(true);
    this.paymentMethodService.addPaymentMethod(addPaymentMethodRequest).subscribe(
      data => {
        this.message = data['message'];
        this.status = data['success'];
        if (this.status === true) {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: data['message']
            }
          });
          this.renderListView();
        }
        this.spinner.showSpinner.next(false);
        this.amplitudeService.logEvent('payment_setup - save card', null);
      },
      error => {
        this.spinner.showSpinner.next(false);
        this.message = error.error['message'];
        this.status = error.error['success'];
      }
    );
  }
  validateAddress() {
    const formGroup = this.addPaymentMethodForm;
    const address1Ctrl = formGroup.controls['addressLine1'];
    const countryCtrl = formGroup.controls['country'];
    const cityCtrl = formGroup.controls['city'];
    const zipCodeCtrl = formGroup.controls['zipcode'];
    const countryCodePhNumberCtrl = formGroup.controls['countryCodePhNumber'];
    const phoneNumberCtrl = formGroup.controls['phoneNumber'];
    let isValid = true;
    if (!address1Ctrl.value) {
      address1Ctrl.setErrors({ require: true });
      isValid = false;
    }
    if (!countryCtrl.value) {
      countryCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!cityCtrl.value) {
      cityCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!zipCodeCtrl.value) {
      zipCodeCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!countryCodePhNumberCtrl.value) {
      countryCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!phoneNumberCtrl.value) {
      phoneNumberCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }
  editPaymentMethodView(id) {
    this.initUniqueZipCodes();
    this.initUniqueCities();
    this.togglePaymentMethod('editView');
    this.editModeOn.emit(null);
    this.submitted = false;
    this.paymentMethodService.getPaymentMethodById(id).subscribe(account => {
      this.editData = <PaymentMethod>account[0];
      this.editData.billingCountry = 'United States';
      this.editData.billingCurrency = 'USD';
      this.advertiserBrandService.getAllBrandsForCards(account[0]._id).subscribe(
        advertiserBrandDetails => {
          this.brandsList = <Brand[]>advertiserBrandDetails;
        },
        err => {
          console.log('Error!');
        });
      this.editData.expiryMonth = this.months[this.months.indexOf(+this.editData.expiryDate.split('-')[1])];
      this.editData.expiryYear = this.years[this.years.indexOf(+this.editData.expiryDate.split('-')[0])];
      this.editId = account[0]._id;
      this.addPaymentMethodForm.get('addressLine1').setValue(this.editData.addressLine1);
      this.addPaymentMethodForm.get('addressLine2').setValue(this.editData.addressLine2);
      this.addPaymentMethodForm.get('country').setValue(this.editData.country);
      this.addPaymentMethodForm.get('city').setValue(this.editData.city);
      this.addPaymentMethodForm.get('zipcode').setValue(this.editData.zipcode);
      if (this.editData.phoneNumber && this.editData.phoneNumber.startsWith('+91')) {
        this.addPaymentMethodForm.get('countryCodePhNumber').setValue('+91');
        this.addPaymentMethodForm.get('phoneNumber').setValue(this.editData.phoneNumber.replace('+91', ''));
      } else if (this.editData.phoneNumber && this.editData.phoneNumber.startsWith('+1')) {
        this.addPaymentMethodForm.get('countryCodePhNumber').setValue('+1');
        this.addPaymentMethodForm.get('phoneNumber').setValue(this.editData.phoneNumber.replace('+1', ''));
      }
      if (this.editData.hasBillingAddress) {
        this.radioType = 'yes';
        this.isSameAsProfile = true;
      } else {
        this.radioType = 'no';
        this.isSameAsProfile = false;
      }
    });
  }
  updatePaymentMethod(id) {
    if (this.radioType && this.radioType === 'yes') {
      if (!this.validateAddress()) {
        return;
      }
    }
    const passwordConfirmDialogRef = this.dialog.open(ConfirmPasswordDailogComponent, {
      width: '50%'
    });
    passwordConfirmDialogRef.afterClosed().subscribe(passwordConfirmResult => {
      if (passwordConfirmResult) {
        if (this.pageView === 'editView') {
          this.addPaymentMethodForm.get('cardNumber').clearValidators();
          this.addPaymentMethodForm.get('cardNumber').updateValueAndValidity();
          this.addPaymentMethodForm.get('cvv').clearValidators();
          this.addPaymentMethodForm.get('cvv').updateValueAndValidity();
        }
        this.submitted = true;
        if (this.addPaymentMethodForm.invalid) {
          return;
        }
        const editPaymentMethodRequest: PaymentMethod = Object.assign({}, this.addPaymentMethodForm.value);
        if (this.radioType && this.radioType === 'yes') {
          editPaymentMethodRequest.hasBillingAddress = true;
          editPaymentMethodRequest.isPhNumberVerified = this.isPhoneNumberVerified;
        } else {
          editPaymentMethodRequest.hasBillingAddress = false;
        }
        if (this.addPaymentMethodForm.valid) {
          this.spinner.showSpinner.next(true);
          this.paymentMethodService.updatePaymentMethodById(id, editPaymentMethodRequest).subscribe(
            data => {
              this.message = data['message'];
              this.status = data['success'];
              this.editModeOff.emit(null);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
              this.paymentMethodList = [];
              this.paymentMethodService.getPaymentMethods().subscribe(
              result => {
                this.paymentMethodList = <PaymentMethod[]>result;
                this.loaded = true;
                this.togglePaymentMethod('listView');
                this.spinner.showSpinner.next(false);
              },
              err => {
                this.loaded = true;
                console.log('Error! getAllPaymentMethods in editPaymentMethodView: ' + err);
              });
            },
            error => {
              this.message = error['message'];
              this.status = error['success'];
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'error',
                  message: error.error['message']
                }
              });
            }
          );
        }
      }
    });
  }
  deletePaymentMethod(id) {
    const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title': 'Are you sure you want to delete the payment method?'
      }
    });
    confirmDialogRef.afterClosed().subscribe(confirmResult => {
      if (confirmResult) {
        this.paymentMethodService.deletePaymentMethod(id).subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          error => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: error.error['message']
              }
            });
          }
        );
      }
    });
  }

  generateInvoice() {
    this.spinner.showSpinner.next(true);
    this.togglePaymentMethod('listView');
    this.paymentMethodService.generateInvoice().subscribe(
      data => {
        this.message = data['message'];
        this.status = data['success'];
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
      },
      error => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
      }
    );
  }

  removeBrand(selectedBrand: any) {
    const temp = this.editData.brands;
    this.editData.brands = [];
    for (const brand of temp) {
      if (brand !== selectedBrand) {
        this.editData.brands.push(brand);
      }
    }
  }

  getBrandName(selectedBrand: any) {
    for (const brand of this.brandsList) {
      if (brand._id === selectedBrand) {
        return brand.brandName;
      }
    }
  }
  validatePhoneNumber() {
    const phoneNumberCtrl = this.addPaymentMethodForm.controls['phoneNumber'];
    const countryCodephNumCtrl = this.addPaymentMethodForm.controls['countryCodePhNumber'];
    if (!phoneNumberCtrl.value) {
      if (!this.id) {
        this.isPhoneNumberVerified = null;
      }
    } else if (phoneNumberCtrl.value.trim()) {
      const phNumber = countryCodephNumCtrl.value.trim() + phoneNumberCtrl.value.trim();
      let validatePhNUmber = phNumber.replace('+', '');
      validatePhNUmber = (validatePhNUmber.match(/[(), ,+,-]/g) != null) ? validatePhNUmber.replace(/[(), ,+,-]/g, '') : validatePhNUmber;
      if (!validatePhNUmber.match(/^\d+$/)) {
        this.isPhoneNumberVerified = null;
      } else {
        const cleaned = ('' + phoneNumberCtrl.value.trim()).replace(/\D/g, '');
        if (countryCodephNumCtrl.value.startsWith('+1')) {
          if (phNumber.startsWith('+1')) {
            const trimmedNumber = phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '');
            const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
            if (match) {
            //  const intlCode = (match[1] ? '+1 ' : '');
              phoneNumberCtrl.setValue(['(', match[2], ') ', match[3], '-', match[4]].join(''));
              if (!this.id && !this.isPhoneNumberVerified && trimmedNumber.length === 10) {
                this.isPhoneNumberVerified = false;
              }  else if (trimmedNumber.length === 10) {
                this.checkChangeInPhoneNumber('+1' + phoneNumberCtrl.value);
              } else {
                this.isPhoneNumberVerified = null;
              }
              if (trimmedNumber.length !== 10 || !trimmedNumber.match(/^\d+$/)) {
                // phoneNumberCtrl.setErrors({ invalidPhNumber: true });
                this.isPhoneNumberVerified = null;
              }
            }
          }
        } else if (countryCodephNumCtrl.value.startsWith('+91')) {
          if (phNumber.startsWith('+91')) {
            const indianNumber = phoneNumberCtrl.value.match(/[(), ,+,-]/g) != null ? phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '') :
            phoneNumberCtrl.value;
            phoneNumberCtrl.setValue(indianNumber);
            const phoneNumber = phNumber.replace(/[(), ,-]/g, '');
            if (!this.id && !this.isPhoneNumberVerified && phoneNumber.length === 13) {
              this.isPhoneNumberVerified = false;
            } else if (phoneNumber.length === 13) {
              this.checkChangeInPhoneNumber(countryCodephNumCtrl.value + phoneNumberCtrl.value);
            } else {
              this.isPhoneNumberVerified = null;
            }
            if (indianNumber.length !== 10 || !indianNumber.match(/^\d+$/)) {
             // phoneNumberCtrl.setErrors({ invalidPhNumber: true });
              this.isPhoneNumberVerified = null;
            }
          }
        } else if (cleaned.length === 10 && !phNumber.startsWith('+91') && !phNumber.startsWith('+1')) {
          this.isPhoneNumberVerified = null;
        } else {
          this.isPhoneNumberVerified = null;
        }
      }
    }
  }
  checkChangeInPhoneNumber(phNumber: string) {
    if (this.verifiedPhNumber) {
      if (this.verifiedPhNumber !== phNumber) {
        this.isPhoneNumberVerified = false;
      } else {
        this.isPhoneNumberVerified = this.addPaymentMethodForm.get('isPhNumberVerified').value;
      }
    } else {
      this.isPhoneNumberVerified = false;
    }
  }
  radioChange(radioType) {
    this.radioType = radioType;
    if (radioType === 'yes') {
      this.isSameAsProfile = true;
      if (this.editId) {
        this.addPaymentMethodForm.get('addressLine1').setValue(this.editData.addressLine1);
        this.addPaymentMethodForm.get('addressLine2').setValue(this.editData.addressLine2);
        this.addPaymentMethodForm.get('country').setValue(this.editData.country);
        this.addPaymentMethodForm.get('city').setValue(this.editData.city);
        this.addPaymentMethodForm.get('zipcode').setValue(this.editData.zipcode);
        if (this.editData.phoneNumber && this.editData.phoneNumber.startsWith('+91')) {
          this.addPaymentMethodForm.get('countryCodePhNumber').setValue('+91');
          this.addPaymentMethodForm.get('phoneNumber').setValue(this.editData.phoneNumber.replace('+91', ''));
        } else if (this.editData.phoneNumber && this.editData.phoneNumber.startsWith('+1')) {
          this.addPaymentMethodForm.get('countryCodePhNumber').setValue('+1');
          this.addPaymentMethodForm.get('phoneNumber').setValue(this.editData.phoneNumber.replace('+1', ''));
        }
      }
    } else {
      this.isSameAsProfile = false;
      this.addPaymentMethodForm.controls['addressLine1'].setValue('');
      this.addPaymentMethodForm.controls['addressLine2'].setValue('');
      this.addPaymentMethodForm.controls['country'].setValue('');
      this.addPaymentMethodForm.controls['city'].setValue('');
      this.addPaymentMethodForm.controls['zipcode'].setValue('');
      this.addPaymentMethodForm.controls['countryCodePhNumber'].setValue('');
      this.addPaymentMethodForm.controls['phoneNumber'].setValue('');
    }
  }
  validatePincode() {
    const zipCodeCtrl = this.addPaymentMethodForm.controls['zipcode'];
    if (this.countrySelected === '1' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 5) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    } else if (this.countrySelected === '2' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 6) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    }
  }
  requestOTP() {
    this.spinner.showSpinner.next(true);
    const phoneNumber = this.addPaymentMethodForm.get('countryCodePhNumber').value + this.addPaymentMethodForm.get('phoneNumber').value;
    this.smsService.verifyPhoneNumber(phoneNumber).
    subscribe( res => {
      this.spinner.showSpinner.next(false);
      this.openPhoneNumberValidationDialog(res['otpLimitMsg']);
    },
    err => {
      this.spinner.showSpinner.next(false);
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: err.error['message']
        }
      });
    });
  }
  openPhoneNumberValidationDialog(limitMsg: string) {
    const phoneNumber = this.addPaymentMethodForm.get('countryCodePhNumber').value + this.addPaymentMethodForm.get('phoneNumber').value;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = '50%';
    dialogConfig.data = {
      phNumber: phoneNumber,
      limitMsg: limitMsg,
      userType: 'Advertiser'
    };
      const dialogRef = this.dialog.open(OptValidationComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(res => {
        if ( res && res.status === 'success') {
            this.verifiedPhNumber = res.phNumber;
            this.isPhoneNumberVerified = true;
          }
      });
  }
  initUniqueZipCodes() {
    this.countrySelected = this.addPaymentMethodForm.controls['country'].value === 'India' ? '2' : '1';
    this.zipcodesService.getUniqueZipCodes(this.countrySelected).subscribe(
        res => {
          this.uniqueZipCodes = res;
          this.filteredZipCodes = this.addPaymentMethodForm.controls['zipcode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
        },
        err => {
          console.log('Error!');
        });
    }
    private _filterZipCode(name: string): any {
      this.onChangeZipCodeString();
      const filterValue = name.toLowerCase();
      return this.uniqueZipCodes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }
    onChangeZipCodeString() {
      if (this.addPaymentMethodForm.controls['city'].value && this.addPaymentMethodForm.controls['city'].value.length > 1) {
        this.zipcodesService.getUniqueZipCodesOnChangeZipCode(
          this.addPaymentMethodForm.controls['zipcode'].value, this.countrySelected).subscribe(
          res => {
            this.uniqueZipCodes = res;
            this.filteredZipCodes = this.addPaymentMethodForm.controls['zipcode'].valueChanges
              .pipe(
                startWith<string>(''),
                map(value => typeof value === 'string' ? value : value),
                map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
              );
          },
          err => {
            console.log('Error!');
        });
      } else {
        this.initUniqueZipCodes();
      }
    }
}
