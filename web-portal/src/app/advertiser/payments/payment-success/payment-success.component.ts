import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { PaypalService } from 'src/app/_services/advertiser/paypal.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.scss']
})
export class PaymentSuccessComponent implements OnInit {
  id: any;
  paymentId: any;
  PayerID: any;
  constructor(private spinner: SpinnerService,
     private snackBar: MatSnackBar,
     private paypalService: PaypalService,
     private _routeParams: ActivatedRoute) {
      this._routeParams.queryParams.subscribe(params => {
        this.id = params['token'];
        this.paymentId = params['paymentId'];
        this.PayerID = params['PayerID'];
      });
  }

  ngOnInit() {
    this.spinner.showSpinner.next(true);
    this.paypalService.paymentSuccess(this.id, this.paymentId, this.PayerID).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        console.log(res);
      },
      err => {
        this.spinner.showSpinner.next(false);
      });
  }

}
