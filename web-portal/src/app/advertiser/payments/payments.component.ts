
import { Component, OnInit, ViewChild } from '@angular/core';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { PaymentmethodService } from 'src/app/_services/advertiser/paymentmethod.service';
import { PaymentMethod } from 'src/app/_models/advertiser/payment_method_model';
import { AdvertiserDashboardService } from 'src/app/_services/advertiser/advertiser-dashboard.service';
import config from 'appconfig.json';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';
import { UtilService } from '../../_services/utils/util.service';
import { InvoicesComponent } from './invoices/invoices.component';
import { PaymentMethodsComponent } from './payment-methods/payment-methods.component';
import { CreditLineComponent } from './credit-line/credit-line.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';


@Component({
  selector: 'app-payment',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  @ViewChild('invoicesComponent') invoicesComponent: InvoicesComponent;
  @ViewChild('paymentMethodsComponent') paymentMethodsComponent: PaymentMethodsComponent;
  @ViewChild('creditLineComponent') creditLineComponent: CreditLineComponent;
  invoiceList: any[] = [];
  filteredInvoiceList: any[] = [];
  totalPendingAmout: Number = 0;
  public paymentMethodList: PaymentMethod[] = [];
  public selectedTab = 0;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  public billGenerationdate = new Date();
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['advertiser_manage_payments_next_bill']);
  activeIcon: any;
  private currencySymbol: string;
  marketSelectionDisplay = false;
  marketSelection = '1';
  public editMode = false;
  constructor(
    private eventEmitterService: EventEmitterService,
    private paymentMethodService: PaymentmethodService,
    private dashboardService: AdvertiserDashboardService,
    private toolTipService: ToolTipService,
    private utilService: UtilService,
    private spinner: SpinnerService,
    private advertiserBrandService: AdvertiserBrandService
  ) {
    this.activeIcon = JSON.parse(localStorage.getItem('activeIcon'));
    if (this.activeIcon !== 'notifications') {
      this.selectedTab = 0;
    } else if (this.activeIcon === 'notifications') {
      this.selectedTab = 1;
    }
    let yearOfBillGeneration: number = (config['INVOICE_GENERATION_DATE']['YEAR'] > 0 ?
                                  config['INVOICE_GENERATION_DATE']['YEAR'] : (new Date).getFullYear());
    let monthOfBillGeneration: number  = (config['INVOICE_GENERATION_DATE']['MONTH'] > 0 ?
                                   config['INVOICE_GENERATION_DATE']['MONTH'] : ((new Date).getMonth()));
    const dayOfBillGeneration: number = (config['INVOICE_GENERATION_DATE']['DAY'] > 0 ?
                                  config['INVOICE_GENERATION_DATE']['DAY'] : (new Date).getDate());
    if (dayOfBillGeneration < (new Date).getDate()) {
      monthOfBillGeneration = (monthOfBillGeneration + 1);
    }
    if (monthOfBillGeneration > 12) {
      monthOfBillGeneration = 1;
      yearOfBillGeneration = (yearOfBillGeneration + 1);
    }
    this.billGenerationdate = new Date(yearOfBillGeneration, monthOfBillGeneration, dayOfBillGeneration);
    localStorage.setItem('activeIcon', JSON.stringify('payment'));
    this.eventEmitterService.onClickNavigation();
    this.paymentMethodService.getPaymentMethods().subscribe(
      result => {
        this.paymentMethodList = <PaymentMethod[]>result;
        if (!this.paymentMethodList.length) {
          this.selectedTab = 1;
        }
      },
      err => {
        console.log('Error! getAllPaymentMethods : ' + err);
      });
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.paymentMethodService.getInvoices().subscribe(
      result => {
        this.invoiceList = <[]>result;
        this.filteredInvoiceList = this.invoiceList;
        /*this.filteredInvoiceList.forEach(item => {
          if (item.invoiceStatus === 'pending') {
            this.totalPendingAmout += item.amount;
          }
        }
        );*/
      },
      err => {
        console.log('Error! getAllInvoices : ' + err);
      });
      this.dashboardService.getOutStandingAmount().subscribe(
        outStandingRes => {
          this.totalPendingAmout = outStandingRes['outstandingAmount'];
        },
        outStandingErr => {
          console.log('Error! getOutStandingAmount : ' + outStandingErr);
        },
      );
      this.fetchTooltips(this.toolTipKeyList);
      this.loadAllZonesByAdvertiserId();
   }

   fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  } 

  selectTab(event) {
    this.editMode = false;
    this.selectedTab = event;
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
    this.ngOnInit();
    this.currencySymbol = this.utilService.getCurrencySymbol();
    if (this.invoicesComponent) {
      this.invoicesComponent.ngOnInit();
    } else if (this.creditLineComponent) {
      this.creditLineComponent.ngOnInit();
    } else if (this.paymentMethodsComponent) {
      this.paymentMethodsComponent.ngOnInit();
    }
  }
  loadAllZonesByAdvertiserId() {
    this.advertiserBrandService.getAllZonesByAdvertiserId().subscribe(
      result => {
        if (result && result.length > 0) {
          // Display Market Selection dropdown if we have more than one zone
          this.marketSelectionDisplay = (([...new Set(result.map((x: any) => x.zone))].length) > 1);
        }
    });
  }
}
