import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AdvertiserDashboardService } from 'src/app/_services/advertiser/advertiser-dashboard.service';


@Component({
  selector: 'app-dashboard-recent-campaigns',
  templateUrl: './dashboard-recent-campaigns.component.html',
  styleUrls: ['./dashboard-recent-campaigns.component.scss']
})
export class DashboardRecentCampaignsComponent implements OnInit {

  recentCampaignsList: any[] = [];
  pRecent = 1;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();

  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  rolePermissions: any;
  constructor(private dashboardService: AdvertiserDashboardService) {

  }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.dashboardService.getActiveCampaignLifeTimeData().subscribe(
      result => {
        this.recentCampaignsList = <[]>result;
      },
      err => {
        console.log('Error! getAllBrands : ' + err);
      });
  }

}
