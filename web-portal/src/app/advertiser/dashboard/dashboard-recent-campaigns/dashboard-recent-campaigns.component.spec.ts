import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRecentCampaignsComponent } from './dashboard-recent-campaigns.component';

describe('DashboardRecentCampaignsComponent', () => {
  let component: DashboardRecentCampaignsComponent;
  let fixture: ComponentFixture<DashboardRecentCampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRecentCampaignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRecentCampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
