import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AdvertiserDashboardService } from 'src/app/_services/advertiser/advertiser-dashboard.service';
import { UtilService } from '../../../_services/utils/util.service';

@Component({
  selector: 'app-dashboard-active-campaigns',
  templateUrl: './dashboard-active-campaigns.component.html',
  styleUrls: ['./dashboard-active-campaigns.component.scss'],
})
export class DashboardActiveCampaignsComponent implements OnInit {

  p = 1;
  activeCampaignsList: any[] = [];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();
  private currencySymbol : string;
  constructor(private dashboardService: AdvertiserDashboardService, private utilService: UtilService) {

  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.dashboardService.getActiveCampaignLifeTimeData().subscribe(
      result => {
        this.activeCampaignsList = <[]>result;
        this.activeCampaignsList.forEach(element => {
          const imageUrls = [];
          element.creativeSpecifications.creativeDetails.forEach(item => {
            imageUrls.push(item.url);
          });
          element['imageUrls'] = imageUrls;
        });
      },
      err => {
        console.log('Error! getAllBrands : ' + err);
      });
  }

}
