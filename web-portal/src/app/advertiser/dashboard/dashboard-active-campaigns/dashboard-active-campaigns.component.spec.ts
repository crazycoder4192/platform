import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardActiveCampaignsComponent } from './dashboard-active-campaigns.component';


describe('DashboardActiveCampaignsComponent', () => {
  let component: DashboardActiveCampaignsComponent;
  let fixture: ComponentFixture<DashboardActiveCampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardActiveCampaignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardActiveCampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
