import { Component, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AdvertiserDashboardService } from 'src/app/_services/advertiser/advertiser-dashboard.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { UserService } from 'src/app/_services/user.service';
import { interval, Subscription, Subject } from 'rxjs';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { MatDialog, MatSnackBar } from '@angular/material';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UtilService } from '../../_services/utils/util.service';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  data: any;
  cardsData: any = Object;
  clientsData: any;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();
  userDetailsTemplate; hideme = {};
  activeId = 1;
  skipId = 10;
  topBarElements = [1, 4, 8, 9];
  sideBarElements = [2, 3, 5, 6, 7];

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  rolePermissions: any;

  private updateSubscription: Subscription;
  private nullSubscription: Subscription;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['advertiser_dashboard_ctr', 'advertiser_dashboard_spends',
  'advertiser_dashboard_cpc', 'advertiser_dashboard_impressions',
  'advertiser_dashboard_reach', 'advertiser_dashboard_campaigns',
  'advertiser_dashboard_cpm', 'advertiser_dashboard_clicks',
  'advertiser_dashboard_outstanding_amt']);
  private currencySymbol: string;
  marketSelectionDisplay = false;
  marketSelection = '1';

  constructor(private dashboardService: AdvertiserDashboardService,
    private advertiserUserService: AdvertiserUserService,
    public router: Router, private eventEmitterService: EventEmitterService,
    private userService: UserService, public dialog: MatDialog, private snackBar: MatSnackBar,
    private toolTipService: ToolTipService, private spinner: SpinnerService, private utilService: UtilService,
    private advertiserBrandService: AdvertiserBrandService
  ) {
    this.spinner.showSpinner.next(true);
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
    this.eventEmitterService.onClickNavigation();
    this.doActivity();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.hideme = {};
    this.updateSubscription = interval(10000).subscribe(
      (val) => {
        this.doActivity();
      }
    );
    this.fetchTooltips(this.toolTipKeyList);
    this.loadAllZonesByAdvertiserId();
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
    if (this.nullSubscription  && (localStorage.getItem('zone') === '1' || localStorage.getItem('zone') === '2')) {
      this.nullSubscription.unsubscribe();
    }
  }

  // TODO: need to redirect to subcampaign view page
  editSubCampaign(bId, Id, subId) {
    this.router.navigate(['/advertiser/campaignCreation'],
      { state: { 'brandId': JSON.stringify(bId), 'campaignId': JSON.stringify(Id), 'subCampaignId': JSON.stringify(subId) } });
  }

  filterResults(brand: any) {
    if (!brand.isWatching) {
      const brandId = brand.brandId;
      const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
        width: '50%',
        data: {
          'title' :  'Are you sure you want to switch to "' + brand.brandName + '" ?'
        }
      });
      confirmDialogRef.afterClosed().subscribe(confirmResult => {
        if (confirmResult) {
          this.spinner.showSpinner.next(true);
          this.advertiserUserService.switchToBrandOfLoggedinUser(brandId).subscribe(
            data => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
              this.router.navigate(['/advertiser/brandDashboard'], { state: { 'brandId': JSON.stringify(brandId) } });
            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
            }
          );
        }
      });
    } else {
      this.router.navigate(['/advertiser/brandDashboard'], { state: { 'brandId': JSON.stringify(brand.brandId) } });
    }
  }

  scrollToNext() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    if (skipEl) {
      skipEl.style.display = 'none';
    }
    this.activeId++;
    this.finalizeNextStep();
  }

  scrollToSkip() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    if (!currentEl) {
      this.skip();
    }
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    skipEl.style.display = 'block';
    skipEl.style.position = 'fixed';
    skipEl.style.zIndex = '999';

    const skipButton = document.getElementById('skipButton');
    const skipButtonLocation = skipButton.getBoundingClientRect();
    skipEl.style.left = (skipButtonLocation.left) + 'px';
    skipEl.style.top =  '20px'; // (skipButtonLocation.top)
  }

  skip() {
    const overLay = document.getElementById('overlay_bg');
    overLay.style.display = 'none';
    if (this.userDetails.isVisitedDashboard === false) {
      this.userService.setDashboardVisitedStatusByEmail(this.userDetails.email, true).subscribe(
        result => {
          console.log('setDashboardVisitedStatusByEmail : SUCCESS');
        },
        error => {
          console.log('setDashboardVisitedStatusByEmail : ERROR');
        }
      );
    }
  }

  finalizeNextStep() {
    const m = document.
      getElementById('siteclick_' + this.activeId);
    if (!m) {
      this.skip();
      return;
    }
    const mainElement = m.getBoundingClientRect();
    let x = mainElement.right;
    let y = mainElement.bottom;

    const nextEl = document.getElementById('overlay_' + this.activeId);
    nextEl.style.display = 'block';
    nextEl.style.position = 'fixed';

    nextEl.style.zIndex = '999';

    if (this.topBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the topbar
      x = x - (mainElement.width);
      y = mainElement.bottom + 20;
      // y remains same
    } else if (this.sideBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the sidebar
      x = mainElement.right;
      y = y - (mainElement.height / 2);
      // x remains same
    }
    nextEl.style.left = x + 'px';
    nextEl.style.top = y + 'px';
  }

  private doActivity() {
    if (localStorage.getItem('zone') !== 'null') {
      if (this.nullSubscription) {
        this.nullSubscription.unsubscribe();
      }
      this.currencySymbol = this.utilService.getCurrencySymbol();
      this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
      this.userService.getUserByEmail(this.userDetails.email).subscribe(
        res => {
          this.userDetails.first_name = res.firstName;
          this.userDetails.last_name = res.lastName;
          this.userDetails.isVisitedDashboard = res.isVisitedDashboard;
          this.spinner.showSpinner.next(false);
          /*if (res.isVisitedDashboard === false) {
            this.userService.setDashboardVisitedStatusByEmail(this.userDetails.email, true).subscribe(
              result => {
                console.log('setDashboardVisitedStatusByEmail : SUCCESS');
              },
              error => {
                console.log('setDashboardVisitedStatusByEmail : ERROR');
              }
            );
          }*/
        }
      );
      this.userDetailsTemplate = this.userDetails;
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();
      if (this.userDetails.hasPersonalProfile) {
        this.dashboardService.getClientWiseBrandsLifeTimeData().subscribe(
          result => {
            this.data = result;
            if (this.rolePermissions && this.rolePermissions.includes('view_payment_method')) {
              this.dashboardService.getOutStandingAmount().subscribe(
                outStandingRes => {
                  this.cardsData = this.data.cardsData;
                  this.cardsData.outstandingAmt.total = outStandingRes['outstandingAmount'];
                },
                outStandingErr => {
                  console.log('Error! getOutStandingAmount : ' + outStandingErr);
                },
              );
            }
            this.clientsData = this.data.clientBrands.sort((a, b) => (a.clientName > b.clientName) ? 1 : -1);
            if (this.userDetails.hasPersonalProfile) {
              if (this.clientsData.length === 1 &&
                this.clientsData[0]['brandData'].length === 1) {
                this.router.navigate(['/advertiser/brandDashboard'],
                  { state: { 'brandId': JSON.stringify(this.clientsData[0]['brandData'][0]['brandId']) } });
              }
            } else {
              this.router.navigate(['/advertiser/advertiserProfile']);
            }
          },
          err => {
            console.log('Error! getAllBrands : ' + err);
          });
      }
    } else if ((this.nullSubscription === undefined) && localStorage.getItem('zone') === 'null' ) {
      this.nullSubscription = interval(500).subscribe(
        (val) => {
          this.doActivity();
        }
      );
    }
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }

  updateMarketSelection() {
    this.spinner.showSpinner.next(true);
    this.setZone();
    this.doActivity();
  }
  loadAllZonesByAdvertiserId() {
    this.advertiserBrandService.getAllZonesByAdvertiserId().subscribe(
      result => {
        if (result && result.length > 0) {
          // Display Market Selection dropdown if we have more than one zone
          this.marketSelectionDisplay = (([...new Set(result.map((x: any) => x.zone))].length) > 1);
        }
    });
  }
}
