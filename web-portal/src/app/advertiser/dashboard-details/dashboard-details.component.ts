import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { AdvertiserDashboardService } from 'src/app/_services/advertiser/advertiser-dashboard.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDatepickerInputEvent, MatSnackBar } from '@angular/material';
import { Observable, interval, Subscription, Subject } from 'rxjs';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { UserService } from 'src/app/_services/user.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UtilService } from '../../_services/utils/util.service';

@Component({
  selector: 'app-dashboard-details',
  templateUrl: './dashboard-details.component.html',
  styleUrls: ['./dashboard-details.component.scss']
})
export class DashboardDetailsComponent implements OnInit, OnDestroy {
  id;
  campaignsList: any[] = [];
  recentCampaignsList: any[] = [];
  p = 1;
  activeCampaignsList: any[] = [];
  pRecent = 1;
  showCampaign = false;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();
  activeId = 1;
  skipId = 10;
  topBarElements = [1, 4, 8, 9];
  sideBarElements = [2, 3, 5, 6, 7];

  cardsData: any = { increasedSpends: 0, totalSpends: 0 };
  totalSpends = 0; totalImpressions = 0; totalClicks = 0; totalViews = 0;
  totalCPC = 0; totalCPM = 0; totalCPV = 0; totalCTR = 0; totalActiveCampaigns = 0;
  totalAfterLogoutSpends = 0; totalAfterLogoutImpressions = 0; totalAfterLogoutClicks = 0; totalAfterLogoutViews = 0;
  totalAfterLogoutCPC = 0; totalAfterLogoutCPM = 0; totalAfterLogoutCPV = 0; totalAfterLogoutCTR = 0; totalAfterLogoutActiveCampaigns = 0;
  lastLoginSpends = 0; percentageOfSpends = 0;

  userDetailsTemplate; hideme = {};
  filteredRecentList: any[] = [];
  filteredActiveList: any[] = [];
  saveFilterForm: FormGroup;
  filteredDuration: { value: string; name: string; }[];
  performanceTrackingResults: any;
  startDate: any;
  endDate: any;
  endDateMin: any;
  todayDate = new Date();
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  rolePermissions: any;
  private updateSubscription: Subscription;
  private nullSubscription: Subscription;

  public toggleSpends: any = 'Spends';
  public toggleCPM: any = undefined;
  public toggleCTR: any = 'CTR';
  public toggleCPC: any = undefined;
  public activeToggleArray: string[] = ['Spends', 'CTR'];
  public toggleStateMap: Map<string, string> = new Map<string, string>();
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['advertiser_dashboard_spends', 'advertiser_dashboard_impressions',
  'advertiser_dashboard_campaigns', 'advertiser_dashboard_cpm',
  'advertiser_dashboard_reach', 'advertiser_dashboard_ctr',
  'advertiser_dashboard_clicks', 'advertiser_dashboard_cpc',
  'advertiser_dashboard_active_campaigns_spends', 'advertiser_dashboard_active_campaigns_impressions',
  'advertiser_dashboard_active_campaigns_clicks', 'advertiser_dashboard_active_campaigns_ctr',
  'advertiser_dashboard_cpm', 'advertiser_dashboard_performance_tracking',
  'advertiser_dashboard_outstanding_amt']);
  private loadGraph;
  watchingBrand: any;
  private currencySymbol: string;
  marketSelectionDisplay = false;
  marketSelection = '1';

  constructor(private brandService: AdvertiserBrandService,
    private dashboardService: AdvertiserDashboardService,
    private toolTipService: ToolTipService,
    public router: Router, private formBuilder: FormBuilder,
    private advertiserUserService: AdvertiserUserService,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private spinner: SpinnerService,
    private utilService: UtilService,
    private advertiserBrandService: AdvertiserBrandService
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
    this.doActivity();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.loadGraph = true;
    this.saveFilterForm = this.formBuilder.group({
      startDate: [Validators.required],
      endDate: [Validators.required],
      duration: ['Custom Duration', Validators.required]
    });
    this.filteredDuration = [
      { value: 'Custom Duration', name: 'Custom Duration' },
      { value: 'Last 24 hours', name: 'Last 24 hours' },
      { value: 'Last 48 hours', name: 'Last 48 hours' },
      { value: 'Last 1 week', name: 'Last 1 week' },
      { value: 'Last 1 month', name: 'Last 1 month' },
      { value: 'Last 6 months', name: 'Last 6 months' }
    ];

    this.toggleStateMap.set('Spends', 'Spends');
    this.toggleStateMap.set('CPM', undefined);
    this.toggleStateMap.set('CTR', 'CTR');
    this.toggleStateMap.set('CPC', undefined);
    this.getWatchingBrand();
    this.updateSubscription = interval(10000).subscribe(
      (val) => {
        this.doActivity();
      }
    );
    this.fetchTooltips(this.toolTipKeyList);
    this.loadAllZonesByAdvertiserId();
  }
  getWatchingBrand() {
    this.advertiserUserService.getAdvertiserUserBrands().subscribe(
      res => {
        this.watchingBrand = res['brands'].find(obj => obj.isWatching);
      },
      err => {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
          icon: 'error',
          message: err.error['message']
          }
      });
      }
    );
  }
  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
    this.loadGraph = null;
    if (this.nullSubscription && (localStorage.getItem('zone') === '1' || localStorage.getItem('zone') === '2')) {
      this.nullSubscription.unsubscribe();
    }
  }

  private doActivity() {
    if (localStorage.getItem('zone') !== 'null') {
      if (this.nullSubscription) {
        this.nullSubscription.unsubscribe();
      }
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.showCampaign = true;

    // initialize id
    if (!this.id && !this.router.getCurrentNavigation()) {
      this.id = this.watchingBrand.brandId;
      this.hideme = {};
    } else if (!this.id && this.router.getCurrentNavigation() && this.router.getCurrentNavigation().extras.state) {
      this.id = JSON.parse(this.router.getCurrentNavigation().extras.state.brandId);
      this.hideme = {};
    }

    if (this.id) {
      this.userService.getUserByEmail(this.userDetails.email).subscribe(
        res => {
          this.userDetails.first_name = res.firstName;
          this.userDetails.last_name = res.lastName;
          this.userDetails.isVisitedDashboard = res.isVisitedDashboard;
          this.spinner.showSpinner.next(false);
          /*if (res.isVisitedDashboard === false) {
            this.userService.setDashboardVisitedStatusByEmail(this.userDetails.email, true).subscribe(
              result => {
                console.log('setDashboardVisitedStatusByEmail : SUCCESS');
              },
              error => {
                console.log('setDashboardVisitedStatusByEmail : ERROR');
              }
            );
          }*/
        }
      );
      this.userDetailsTemplate = this.userDetails;
      this.dashboardService.getLastCheckInData(this.id).subscribe(
        result => {
          this.cardsData = result;
          if (this.rolePermissions && this.rolePermissions.includes('view_payment_method')) {
            this.dashboardService.getOutStandingBrandAmount(this.id).subscribe(
              outStandingRes => {
                this.cardsData.outstandingAmt = { total: outStandingRes['outstandingAmount'] };
              },
              outStandingErr => {
                console.log('Error! getOutStandingAmount : ' + outStandingErr);
              },
            );
          }
        },
        err => {
          console.log('Error! getAllBrands : ' + err);
        }
      );

      this.dashboardService.getActiveCampaignLifeTimeData(this.id).subscribe(
        result => {
          this.recentCampaignsList = <[]>result;
          this.filteredRecentList = this.recentCampaignsList;
          this.filteredRecentList.forEach(element => {
            const imageUrls = [];
            element.creativeSpecifications.creativeDetails.forEach(item => {
              imageUrls.push(item.url);
            });
            element['imageUrls'] = imageUrls;
          });
        },
        err => {
          console.log('Error! getAllBrands : ' + err);
        }
      );

      this.filteredRecentList = this.recentCampaignsList.filter(item => {
        return item.brandId === this.id;
      });

      this.brandService.getBrandById(this.id).subscribe(response => {
        if (!this.checkIfValidDate(this.saveFilterForm.get('startDate').value)) {
          this.saveFilterForm.get('startDate').setValue(response.created.at);
        }
        if (!this.checkIfValidDate(this.saveFilterForm.get('endDate').value)) {
          this.saveFilterForm.get('endDate').setValue(this.todayDate);
        }
        this.loadPerformanceTracking();
      });
    }
    } else if ((this.nullSubscription === undefined) && localStorage.getItem('zone') === 'null' ) {
      this.nullSubscription = interval(500).subscribe(
        (val) => {
          this.doActivity();
        }
      );
    }
  }

  private checkIfValidDate(d) {
    return d instanceof Date;
  }

  loadPerformanceTracking() {
    let startDate = new Date(this.saveFilterForm.get('startDate').value);
    let endDate = new Date(this.saveFilterForm.get('endDate').value);

    if (this.saveFilterForm.get('duration').value === 'Custom Duration') {
      startDate = new Date(new Date(this.saveFilterForm.get('startDate').value).setHours(0o0, 0o0, 0o0));
      endDate = new Date(new Date(this.saveFilterForm.get('endDate').value).setHours(23, 59, 59));
    }
    const sDate = startDate.toString();
    const eDate = endDate.toString();
    const performanancePostData = {
      startDate: sDate,
      endDate: eDate,
      brandId: this.id,
      timezone: new Date().getTimezoneOffset()
    };
    this.dashboardService.getPerformananceAnalyticsData(performanancePostData).subscribe(
      response => {
        this.performanceTrackingResults = response;
        if (this.loadGraph && this.performanceTrackingResults.data.length) {
          this.drawConversionResultLineChart(this.performanceTrackingResults);
        } else {
          this.resetGraph();
        }
      },
      (err: Error) => {
        console.log(err);
    });
  }

  scrollToNext() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    if (skipEl) {
      skipEl.style.display = 'none';
    }
    this.activeId++;
    this.finalizeNextStep();
  }

  scrollToSkip() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    if (!currentEl) {
      this.skip();
    }
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    skipEl.style.display = 'block';
    skipEl.style.position = 'fixed';
    skipEl.style.zIndex = '999';

    const skipButton = document.getElementById('skipButton');
    const skipButtonLocation = skipButton.getBoundingClientRect();
    skipEl.style.left = (skipButtonLocation.left) + 'px';
    skipEl.style.top =  '20px'; // (skipButtonLocation.top)
  }

  skip() {
    const overLay = document.getElementById('overlay_bg');
    overLay.style.display = 'none';
    if (this.userDetails.isVisitedDashboard === false) {
      this.userService.setDashboardVisitedStatusByEmail(this.userDetails.email, true).subscribe(
        result => {
          console.log('setDashboardVisitedStatusByEmail : SUCCESS');
        },
        error => {
          console.log('setDashboardVisitedStatusByEmail : ERROR');
        }
      );
    }
  }

  finalizeNextStep() {
    const m = document.
      getElementById('siteclick_' + this.activeId);
    if (!m) {
      this.skip();
      return;
    }
    const mainElement = m.getBoundingClientRect();
    let x = mainElement.right;
    let y = mainElement.bottom;

    const nextEl = document.getElementById('overlay_' + this.activeId);
    nextEl.style.display = 'block';
    nextEl.style.position = 'fixed';

    nextEl.style.zIndex = '999';

    if (this.topBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the topbar
      x = x - (mainElement.width);
      y = mainElement.bottom + 20;
      // y remains same
    } else if (this.sideBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the sidebar
      x = mainElement.right;
      y = y - (mainElement.height / 2);
      // x remains same
    }
    nextEl.style.left = x + 'px';
    if (this.activeId !== 4) {
      nextEl.style.top = y + 'px';
    }
  }


  drawConversionResultLineChart(performanceTrackingResults: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');

    const toggleValues = Array.from( this.toggleStateMap.values() );
    let dataRows = [];

    toggleValues.forEach(toggleElement => {
      if (this.toggleStateMap.get(toggleElement) !== undefined) {
        data.addColumn('number', this.toggleStateMap.get(toggleElement));
      }
    });

    const chartData: any[] = performanceTrackingResults.data;
    for (const c of chartData) {
      dataRows = [];
      dataRows.push(c.durationInstance);

      toggleValues.forEach(toggleElement => {
        if ( this.toggleStateMap.get(toggleElement) !== undefined ) {
          if (this.toggleStateMap.get(toggleElement) === 'Spends') {
            dataRows.push(c.spends);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPM') {
            dataRows.push(c.avgCPM);
          } else if (this.toggleStateMap.get(toggleElement) === 'CTR') {
            dataRows.push(c.ctr);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPC') {
            dataRows.push(c.avgCPC);
          }
        }
      });
      data.addRow(dataRows);
    }

    const r: any = this.getSeriesAndVAxes();
    const options: any = {
      colors: ['#5287c6', '#78c068', 'f37a6e', '#9a67ab'],
      hAxis: { titleTextStyle: { color: '#ccc' } },
      vAxis: { viewWindow: { min: 0 } },
      chartArea: { 'width': '90%', 'height': '80%' },
      vAxes: r.vAxes,
      series: r.series,
      width: 973,
      height: 350,
      legend: { position: 'none'}
    };

    const chart = new google.visualization.LineChart(document.getElementById('performanceTrackingLineChart'));
    chart.draw(data, options);
  }

  onSelectDuration(duration) {
    let date = new Date();
    this.saveFilterForm.get('endDate').setValue(date);
    if (duration === 'Custom Duration') {
      this.saveFilterForm.get('startDate').setValue(date);
      this.saveFilterForm.get('startDate').enable();
      this.saveFilterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      date = new Date(new Date().setHours(new Date().getHours() - 24));
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 48 hours') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setHours(new Date().getHours() - 48));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 week') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setDate(new Date().getDate() - 7));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 month') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 1));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 6 months') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 6));
      this.saveFilterForm.get('startDate').setValue(date);
    }
    this.startDate = this.saveFilterForm.get('startDate').value;
    this.endDate = this.saveFilterForm.get('endDate').value;
    this.loadPerformanceTracking();
  }
  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    this.endDateMin = this.startDate;

    this.loadPerformanceTracking();
  }
  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDate = new Date(this.saveFilterForm.get('endDate').value);
    this.endDateMin = this.endDate;

    this.loadPerformanceTracking();
  }

  viewCampaignAnalytics(subcampaign) {
    this.router.navigate(['/advertiser/analytics'],
      { state: { 'brandId': JSON.stringify(subcampaign.brandId), 'campaignId': JSON.stringify(subcampaign.campaignId),
         'subCampaignId': JSON.stringify(subcampaign.subCampaignId) } });
  }

  public twoToggle(toggleValue: any) {
    if (this.toggleStateMap.get(toggleValue) === undefined) {
      this.toggleStateMap.set(toggleValue, toggleValue);
      this.activeToggleArray.push(toggleValue);
      if (this.activeToggleArray.length > 2) {
        this['toggle' + this.activeToggleArray[0]] = undefined;
        this.toggleStateMap.set(this.activeToggleArray[0], undefined);
        this.activeToggleArray.shift();
      }
    } else if (this.toggleStateMap.get(toggleValue) === toggleValue) {
      this.toggleStateMap.set(toggleValue, undefined);
      this['toggle' + toggleValue] = undefined;
      this.activeToggleArray = this.activeToggleArray.filter(item => item !== toggleValue);
    }
    if (this.performanceTrackingResults && this.performanceTrackingResults.data.length) {
      this.drawConversionResultLineChart(this.performanceTrackingResults);
    }
  }

  public getCDNPath(s3url: string) {
    console.log('getCDPath : ' + s3url);
    return s3url;
  }

  private getSeriesAndVAxes(): any {

    const spendsColor = '#5287c6';
    const cpmColor = '#78c068';
    const cpcColor = '#f37a6e';
    const ctrColor = '#9a67ab';

    let series: any = {};
    let vAxes: any = {};

    let toggleValues = Array.from(this.toggleStateMap.values());
    toggleValues = toggleValues.filter(( element ) => {
      return element !== undefined;
    });

    if (toggleValues && toggleValues.length === 1) {
      if (toggleValues.includes('Spends')) {
        series = {
          0: { color: spendsColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('$#', null);
      }
      if (toggleValues.includes('CPM')) {
        series = {
          0: { color: cpmColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('$#', null);
      }
      if (toggleValues.includes('CPC')) {
        series = {
          0: { color: cpcColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('$#', null);
      }
      if (toggleValues.includes('CTR')) {
        series = {
          0: { color: ctrColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('#\'%\'', null);
      }
    }

    if (toggleValues && toggleValues.length === 2) {
      if (toggleValues.includes('Spends') && toggleValues.includes('CPM')) {
        if (toggleValues.indexOf('Spends') < toggleValues.indexOf('CPM')) {
          series = {
            0: { color: spendsColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: spendsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes('$#', '$#');
      }

      if (toggleValues.includes('Spends') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('Spends') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: spendsColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('$#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: spendsColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', '$#');
        }
      }

      if (toggleValues.includes('Spends') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('Spends') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: spendsColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: spendsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes('$#', '$#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes('$#', '$#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('$#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', '$#');
        }
      }

      if (toggleValues.includes('CPC') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPC') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('$#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', '$#');
        }
      }
    }
    return {series, vAxes};
  }

  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }

  updateMarketSelection() {
    this.spinner.showSpinner.next(true);
    this.setZone();
    this.doActivity();
    this.loadAllZonesByAdvertiserId();
  }

  loadAllZonesByAdvertiserId() {
    this.advertiserBrandService.getAllZonesByAdvertiserId().subscribe(
      result => {
        if (result && result.length > 0) {
          // Display Market Selection dropdown if we have more than one zone
          this.marketSelectionDisplay = (([...new Set(result.map((x: any) => x.zone))].length) > 1);
        }
    });
  }

  resetGraph() {
    const node = document.getElementById('performanceTrackingLineChart');
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
      const a = document.createElement('span');
        a.innerHTML =
        `<img src="../assets/images/graph/adv-conversion-result.jpg"
        alt="loading" style="width:973px; height:350px; margin-left: 15px;">
        <p style="position: relative; word-break: break-all; top:-213px;width: 400px; margin: auto !important;text-align: center;">
        You will start seeing analytics data once your campaign starts receiving views, impressions and clicks.</p>`;
      document.getElementById('performanceTrackingLineChart').appendChild(a);
    }
  }
  populateVAxes(yAxis1Format: string, yAxis2Format: string): any {
    let a: any = {};

    if (yAxis2Format) {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: '$#'
        },
        1: {
          minValue: 0,
          max: 100,
          format: '#\'%\''
        }
      };
      a['0']['format'] = yAxis1Format;
      a['1']['format'] = yAxis2Format;
    } else {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: '$#'
        }
      };
      a['0']['format'] = yAxis1Format;
    }
    return a;
  }
}
