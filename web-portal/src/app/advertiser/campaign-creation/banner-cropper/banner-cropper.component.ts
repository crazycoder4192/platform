import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { Banner } from 'src/app/_models/utils';

@Component({
  selector: 'app-banner-cropper',
  templateUrl: './banner-cropper.component.html',
  styleUrls: ['./banner-cropper.component.scss']
})
export class BannerCropperComponent implements OnInit {
  banners: Banner;
  resizeWidth = 200;
  imgUrl = '';
  imgWidth = '';
  imgHeight = '';
  previewOrUpload = '';
  buttonText = 'Upload';
  constructor(public dialogRef: MatDialogRef<BannerCropperComponent>, private spinner: SpinnerService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.banners = this.data.banner;
      this.imgUrl = this.data.url;
      this.imgWidth = this.data.width;
      this.imgHeight = this.data.height;
      this.previewOrUpload = this.data.previewOrUpload;
    }

  ngOnInit() {
    if (this.previewOrUpload === 'preview') {
      console.log('chaning button text');
      this.buttonText = 'OK';
    }
  }
  onSubmit(): void {
      this.dialogRef.close(this.data);
  }

}
