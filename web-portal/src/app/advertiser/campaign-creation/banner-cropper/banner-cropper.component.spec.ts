import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerCropperComponent } from './banner-cropper.component';

describe('BannerCropperComponent', () => {
  let component: BannerCropperComponent;
  let fixture: ComponentFixture<BannerCropperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerCropperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerCropperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
