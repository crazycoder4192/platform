import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { CampaignElement } from 'src/app/_models/advertiser_campaign';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';

@Component({
  selector: 'app-add-campaign',
  templateUrl: './add-campaign.component.html',
  styleUrls: ['./add-campaign.component.scss']
})
export class AddCampaignComponent implements OnInit {

  advertiserList: any;
  brandList: Brand[];
  formValid: Boolean = true;
  userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  public isCampaignNameAvailable = true;
  public editingCampaignName = '';

  constructor(public dialogRef: MatDialogRef<AddCampaignComponent>, private spinner: SpinnerService,
    @Inject(MAT_DIALOG_DATA) public data: CampaignElement, private brandService: AdvertiserBrandService,
    private advertiserService: AdvertiserProfileService,
    private campaignService: CampaignService) {
      this.editingCampaignName = this.data.name;
      this.getAdvertiser();
      this.getBrands();
  }

  ngOnInit() {
  }

  getBrands(): void {
    this.brandService.getAllBrands().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.brandList = <Brand[]>res;
        this.data.brandId = this.brandList[0]._id;
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAllBrands : ' + err);
      });
  }

  getAdvertiser(): void {
    /*     TODO--->Dependent on "AdvertiserUser-task",
     because for each advertiser multiple user will exist.
     So, based on the user need to get advertiserId */
    this.advertiserService.getAdvertiserByEmail(this.userEmail).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.advertiserList = res;
        this.data.advertiserId = res['_id'];
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAdvertisers : ' + err);
      });
  }

  checkCampaignNameAvailablity() {
    const value = this.data.name.trim();
    this.spinner.showSpinner.next(true);
    if (value && value.length > 2) {
      this.campaignService.campaignNameAvailability(value, this.data.brandId, this.data.advertiserId).subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          this.isCampaignNameAvailable = true;
          this.dialogRef.close(this.data);
        },
        err => {
          this.spinner.showSpinner.next(false);
          this.isCampaignNameAvailable = false;
        }
      );

    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit(): void {
    this.data.name = (this.data && this.data.name) ? this.data.name.trim() : '';
    console.log(this.data.page);
    if (this.data.page === 'Edit') {
      if (this.data.brandId && this.data.name && this.data.name.trim().length > 2  &&
          this.editingCampaignName && this.editingCampaignName.toLowerCase() !== this.data.name.toLowerCase()) {
        this.formValid = true;
        this.checkCampaignNameAvailablity();
      } else {
        this.formValid = false;
        this.dialogRef.close(this.data);
      }
    } else {
      if (this.data.brandId && this.data.name && this.data.name.trim().length > 2) {
        this.formValid = true;
        this.checkCampaignNameAvailablity();
      } else {
        this.formValid = false;
       // this.dialogRef.close(this.data);
      }
    }
  }

}
