import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { UtilService } from 'src/app/_services/utils/util.service';
import { Banner } from 'src/app/_models/utils';
import { MatDialog, MatSnackBar, MatStepper } from '@angular/material';
import { AddCampaignComponent } from './add-campaign/add-campaign.component';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { SubCampaignElement, SubcampaignStatus } from 'src/app/_models/advertiser_sub_campaign';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { ObjectiveElement } from 'src/app/_models/advertiser_objectives.model';
import { UploadToS3Service } from 'src/app/common/upload-to-s3/upload-to-s3.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { BannerCropperComponent } from './banner-cropper/banner-cropper.component';
import { CampaignElement } from 'src/app/_models/advertiser_campaign';
import { DomSanitizer } from '@angular/platform-browser';
import { ImagedimensionsDirective } from 'src/app/common/directives/imagedimensions.directive';
import { CheckBids } from 'src/app/common/validators/validator';
import config from 'appconfig.json';
import { AdvertiserProfileService } from 'src/app/_services/advertiser/advertiser-profile.service';
import { Observable, of, Subject } from 'rxjs';
import {  Router } from '@angular/router';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { UploadCampaignCreativePopupComponent } from '../upload-campaign-creative-popup/upload-campaign-creative-popup.component';
import { CreativeHubService } from 'src/app/_services/advertiser/creative-hub.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { debounceTime } from 'rxjs/operators';
import { TreeviewItem,  TreeviewConfig } from 'ngx-treeview';
import { DropDownWithTreeComponent } from 'src/app/common/drop-down-with-tree/drop-down-with-tree.component';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AudienceTargetService } from 'src/app/_services/advertiser/audience-target.service';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
const geoJsonUS = require('./../../../assets/geojson_US.json');

export enum FILE_SIZES {
  KB = 1024,
  MB = 1048576,
  GB = 1073741824
}
@Component({
  selector: 'app-campaign-creation',
  templateUrl: './campaign-creation.component.html',
  styleUrls: ['./campaign-creation.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false },
  }]
})
export class CampaignCreationComponent implements OnInit, OnDestroy {
  SubcampaignStatus = SubcampaignStatus;
  dropdownEnabled = true;
  items: TreeviewItem[];
  specializationItems: TreeviewItem[];
  values: number[];
  config = TreeviewConfig.create({
      hasAllCheckBox: true,
      hasFilter: true,
      hasCollapseExpand: true,
      decoupleChildFromParent: false,
      maxHeight: 100
  });

  @ViewChild('ddTreeLocations') ddTreeLocations: DropDownWithTreeComponent;
  @ViewChild('ddTreeSpecialization') ddTreeSpecilaziton: DropDownWithTreeComponent;
  @ViewChild('pauseBtn') pauseBtn: ElementRef;
  @ViewChild('resumeBtn') resumeBtn: ElementRef;
  @ViewChild('deleteBtn') deleteBtn: ElementRef;
  @ViewChild('saveAsDraftBtn') saveAsDraftBtn: ElementRef;
  @ViewChild('saveAndPublishBtn') saveAndPublishBtn: ElementRef;
  @ViewChild('publishBtn') publishBtn: ElementRef;
  @ViewChild('file') fileInput: ElementRef;
  @ViewChild('stepper') campaignStepper: MatStepper;
  @ViewChild(ImagedimensionsDirective) imageDimensions: ImagedimensionsDirective;
  basicSectionFormGroup: FormGroup;
  targetSectionFormGroup: FormGroup;
  operationalFormGroup: FormGroup;
  creativeFormGroup: FormGroup;
  brandList: Brand[];
  subCampDetails: SubCampaignElement;
  publishedSubCampDetails: SubCampaignElement;
  campaignList: CampaignElement[];
  campaignListByBrandId: CampaignElement[];
  objectiveList: ObjectiveElement[];
  webBanners: Banner[];
  appBanners: Banner[];
  basicTabs: Array<object> = [{ name: 'Banner', size: 'KB' }];
  selectedFiles: FileList;
  behaviourList: string[] = [];
  websiteList: string[] = [];
  deviceList: string[] = ['Desktop', 'Tablet', 'Mobile'];
  bidList: object[] = [];
  bidLimitList: object[] = [];
  newCampaignId = null;
  selectedBannerToUpload: Banner;
  event: any;
  public imagePath;
  imgURL: any;
  startMinRageDatePicker: Date;
  startDatePicker: Date;
  endDatePicker: Date;
  endMinRageDatePicker: Date;
  userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  brandName;
  pageMode;
  potentialContnet = false;
  potentialTotalHcps = 0;
  potentialReach = 0;
  percentagePotential = 0;
  isFromLocal: any;
  selectedCreative: any;
  creativeHub: any;
  progressValue = 0;
  imgList = [];
  isCreateCreativeHub: boolean;
  creativeHubPostData: any;
  isSectionFormValid = false;
  clearingBanner: boolean;
  ctaLinkState = {'valid': 'VALID', 'inProgress': 'IN_PROGRESS', 'notValid': 'NOT_VALID'};
  verifyCTALink = '';
  geoJsonObject = geoJsonUS;
  newGeoJson = {
    'type': 'FeatureCollection',
    'features': []
  };
  featureStyle = {
    fillColor: '#ff726f',
    fillOpacity: 0.2,
    strokeColor: '#ff726f',
    strokeWeight: 1
  };
  bounds = {
    northEastLat : 0,
    northEastLng : 0,
    southWestLat : 0,
    southWestLng : 0
  };
  zoomLevelReached = false;
  editingSubCamName = '';
  rolePermissions: any;
  bannerDetails: any;
  minBidAmount: any;
  maxBidAmount: any;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toppings = new FormControl();
  hvaList: string[] = [];
  campaignStatus: any;
  toolTipKeyList = new Set([
    'advertiser_newCampaign_basics_brand',
    'advertiser_newCampaign_basics_campaign', 'advertiser_newCampaign_basics_sub_campaign',
    'advertiser_newCampaign_basics_objective', 'advertiser_newCampaign_basics_type',
    'advertiser_newCampaign_targeting_location', 'advertiser_newCampaign_targeting_display',
    'advertiser_newCampaign_targeting_demographic_specialization', 'advertiser_newCampaign_targeting_demographic_gender',
    'advertiser_newCampaign_targeting_demographic_behaviour', 'advertiser_newCampaign_operational_details_date',
    'advertiser_newCampaign_operational_details_bid_amount', 'advertiser_newCampaign_operational_details_total_budget',
    'advertiser_newCampaign_operational_details_limit_to', 'advertiser_newCampaign_upload_creative_creative_sizes',
    'advertiser_newCampaign_upload_creative_cta_links', 'advertiser_newCampaign_targeting_demographic',
    'advertiser_newCampaign_targeting_type_of_websites', 'advertiser_newCampaign_targeting_devices']);
    private currencySymbol : string;

  constructor(
    private _formBuilder: FormBuilder,
    private utilHTTPService: UtilService,
    private snackBar: MatSnackBar,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private campaignService: CampaignService,
    private brandService: AdvertiserBrandService,
    private uploadService: UploadToS3Service,
    private assetService: AssetService,
    private sanitizer: DomSanitizer,
    private advertiserService: AdvertiserProfileService,
    private router: Router,
    private creativeHubService: CreativeHubService,
    private eventEmitterService: EventEmitterService,
    private toolTipService: ToolTipService, private audienceHTTPService: AudienceTargetService,
    private amplitudeService: AmplitudeService
    ) {
    this.subCampDetails = <SubCampaignElement>{};
    this.subCampDetails._id = '';
    this.subCampDetails.creativeType = 'Banner';
    this.potentialContnet = false;
    this.potentialTotalHcps = 0;
    this.potentialReach = 0;
    this.percentagePotential = 0;
    this.isSectionFormValid = true;
    this.subCampDetails.targetDetails = {
      geoLocation: { locations: [] },
      demoGraphics: {
        specialization: { areaOfSpecialization: [], groupOfSpecialization: [] },
        gender: [], behaviours: []
      }
    };
    this.subCampDetails.audienceTargetIds =  [],
    this.subCampDetails.displayTargetDetails = { websiteTypes: [], deviceTypes: [] };
    this.subCampDetails.operationalDetails = {
      currency: '',
      bidSpecifications: [], totalBudget: '',
      bidLimits: { budget: '', type: '' }
    };
    this.subCampDetails.creativeSpecifications = {
      ctaLink: '',
      creativeDetails: [
        { formatType: '', url: '', weight: '', resolution: '', size: '', platformType: '', style: '' }
      ]
    };
    this.subCampDetails.creativeSpecifications.creativeDetails.pop();
    this.spinner.showSpinner.next(true);
    this.getAdvertiser();
    this.getBrands();
    this.getCampaigns();
    this.getObjectives();
    this.getBanners();
    this.getWebsiteNames();
    this.getBidTypes();
    this.getBidLimitTypes();
    
    /*this.uploadService.getListofObjectsInAnBucket();*/
    if (this.router.getCurrentNavigation().extras.state) {
      this.pageMode = 'Edit';
      if (this.router.getCurrentNavigation().extras.state.subCampaignId) {
        this.subCampDetails._id = JSON.parse(this.router.getCurrentNavigation().extras.state.subCampaignId);
        of(this.campaignService.getOneSubCampaign(this.subCampDetails._id).subscribe(
          response => {
            this.spinner.showSpinner.next(false);
            this.subCampDetails = <SubCampaignElement>response;
            if (this.subCampDetails.status === SubcampaignStatus.approved ||
                this.subCampDetails.status === SubcampaignStatus.paused ||
                this.subCampDetails.status === SubcampaignStatus.recentlyCompleted) {
                  this.publishedSubCampDetails = JSON.parse(JSON.stringify(this.subCampDetails));
            }
            if (this.subCampDetails.operationalDetails.startDate === this.subCampDetails.operationalDetails.endDate) {
              this.subCampDetails.operationalDetails.startDate = '';
              this.subCampDetails.operationalDetails.endDate = '';
            }
            this.editingSubCamName = this.subCampDetails.name;
            // this.subCampDetails.targetDetails.geoLocation.isInclude = this.subCampDetails.targetDetails.geoLocation.isInclude.toString();
            /*this.subCampDetails.targetDetails.demoGraphics.specialization.isInclude =
                                this.subCampDetails.targetDetails.demoGraphics.specialization.isInclude.toString();
            */
           /*this.brandName = this.brandList.find(items => items._id === this.subCampDetails.brandId).brandName;
            if (this.subCampDetails.targetDetails.geoLocation.locations.length > 0) {
              const tempLocations = this.subCampDetails.targetDetails.geoLocation.locations;
                const temp = [];
                for (const obj of tempLocations) {
                  temp.push(obj.zipcode);
                }
                if ( this.locationList && this.locationList.length > 0) {
                  this.addMoreLoactionsLatest(temp);
                  this.ddTreeLocations.onSelect(temp);
                  this.ddTreeLocations.updateItems(this.items, temp);
                } else {
                  this.targetSectionFormGroup.controls.locationsAdded.valueChanges.pipe(debounceTime(1000)).subscribe(
                    value => {
                      if (value && value.length === 0 && this.locationList && this.locationList.length > 0) {
                        this.addMoreLoactionsLatest(temp);
                        this.ddTreeLocations.onSelect(temp);
                        this.ddTreeLocations.updateItems(this.items, temp);
                      }
                    }
                  );
                }
            }*/
            /*if (this.subCampDetails.targetDetails.demoGraphics.specialization.areaOfSpecialization.length > 0) {
              this.ddTreeSpecilaziton.onSelect(this.subCampDetails.targetDetails.demoGraphics.specialization.areaOfSpecialization);
              this.ddTreeSpecilaziton.updateItems(this.specializationItems,
                this.subCampDetails.targetDetails.demoGraphics.specialization.areaOfSpecialization);
            }*/
            /*if (this.subCampDetails.targetDetails.demoGraphics.gender.length > 0 ) {
              this.updateGenderControls();
            }
            if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0 ) {
              this.updateBehaviourControls();
            }*/
            if (this.subCampDetails.displayTargetDetails.websiteTypes.length > 0 ) {
              this.updateWebSiteControls();
            }
            if (this.subCampDetails.displayTargetDetails.deviceTypes.length > 0 ) {
              this.updateDeviceControls();
            }
            if (this.subCampDetails.audienceTargetIds.length > 0 ) {
              this.getPotentialReach();
            }
            if (this.subCampDetails.audienceTargetIds.length === 0 &&
              this.subCampDetails.displayTargetDetails.deviceTypes.length === 0 &&
              this.subCampDetails.displayTargetDetails.websiteTypes.length === 0) {
              this.defaultEnableTargetSectionFields();
            }
            if (this.subCampDetails.creativeType === 'Banner' && this.subCampDetails.creativeSpecifications.creativeDetails.length) {
              this.subCampDetails.creativeSpecifications.creativeDetails.forEach( creative => {
                this.imgList.push(creative.url);
              });
            }
            if (this.subCampDetails.creativeSpecifications.ctaLink && this.subCampDetails.creativeSpecifications.ctaLink.length > 0) {
              this.spinner.showSpinner.next(true);
              this.creativeFormGroup.controls.ctaLink.valueChanges.pipe(debounceTime(10)).subscribe(
                value => {
                  this.verifyCTALink = this.ctaLinkState.inProgress;
                }
              );
            }
          },
          error => {
            console.log('Error while getting subcampaign');
          }
        ));
      }
    }
    this.creativeHubPostData = {
      _id: '',
      name: '',
      creativeType: '',
      creativeDetails: [
          { formatType: '', url: '', size: '', resolution: '', dimensions: '', styleName: '' }
      ],
      sectionName: '',
      status: '',
      deletedUsers: [],
      advertiserId: '',
    };
    this.creativeHubPostData.creativeDetails.pop();
    localStorage.setItem('activeIcon', JSON.stringify(''));
    this.eventEmitterService.onClickNavigation();
    this.clearingBanner = false;
  }

  ngOnInit() {
    this.currencySymbol = this.utilHTTPService.getCurrencySymbol();
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.createForm();
    this.fetchTooltips(this.toolTipKeyList);
    this.amplitudeService.logEvent('campaign_creation - click new campaign', null);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }

  ngOnDestroy() {
    this.dialog.closeAll();
  }


  createForm() {
    this.basicSectionFormGroup = this._formBuilder.group({
      brandName: ['', Validators.required],
      name: ['', Validators.required],
      subCampaignName: ['', Validators.required],
      objectiveName: ['', Validators.required]
    });
    this.targetSectionFormGroup = this._formBuilder.group({
      hvaIds: ['', Validators.required],
      webSiteName: new FormArray([], this.minSelectedCheckboxes(1)),
      deviceName: new FormArray([], this.minSelectedCheckboxes(1))
    });
    this.targetSectionFormGroup.controls.hvaIds.valueChanges.pipe(debounceTime(10)).subscribe(
      value => {
        // if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0) {
          this.getPotentialReach();
        // }
      }
    );
   /* this.targetSectionFormGroup.controls.gen.valueChanges.pipe(debounceTime(1000)).subscribe(
      value => {
        if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0) {
          this.getPotentialReach();
        }
      }
    );
    this.targetSectionFormGroup.controls.behaviourName.valueChanges.pipe(debounceTime(1000)).subscribe(
      value => {
        if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0) {
          this.getPotentialReach();
        }
      }
    );
    this.targetSectionFormGroup.controls.specializationName.valueChanges.pipe(debounceTime(1000)).subscribe(
      value => {
        if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0) {
          this.getPotentialReach();
        }
      }
    );*/
    this.targetSectionFormGroup.controls.webSiteName.valueChanges.pipe(debounceTime(1000)).subscribe(
      value => {
        // if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0) {
          this.getPotentialReach();
        // }
      }
    );
    this.targetSectionFormGroup.controls.deviceName.valueChanges.pipe(debounceTime(1000)).subscribe(
      value => {
        // if (this.subCampDetails.targetDetails.demoGraphics.behaviours.length > 0) {
          this.getPotentialReach();
        // }
      }
    );
    this.operationalFormGroup = this._formBuilder.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      bidAmountType: ['', Validators.required],
      bidAmountValue: ['', Validators.compose([Validators.required, Validators.min(this.minBidAmount), Validators.max(this.maxBidAmount)])],
      totalBudget: ['', Validators.required],
      limitToValue: ['', Validators.required],
      limitToType: ['', Validators.required]
    }, {
        validator: [CheckBids('bidAmountValue', 'limitToValue', 'totalBudget')]
      });
    this.creativeFormGroup = this._formBuilder.group({
      ctaLink: ['', Validators.required]
    });
    this.creativeFormGroup.controls.ctaLink.valueChanges.pipe(debounceTime(10)).subscribe(
      value => {
          this.verifyCTALink = this.ctaLinkState.inProgress;
      }
    );
    /*this.addGenderControls();*/
    this.addWebSiteControls();
    this.addDeviceControls();
  }
  onChangeByBrand(item) {
    this.campaignListByBrandId = this.campaignList.filter( items => items.brandId === item._id);
    this.brandName = item.brandName;
  }
  addWebSiteControls() {
    for (const value of this.websiteList) {
      let control = new FormControl(false);
      if (this.pageMode === 'Edit' && this.subCampDetails.displayTargetDetails.websiteTypes.includes(value)) {
        control = new FormControl(true);
      }
      (this.targetSectionFormGroup.controls.webSiteName as FormArray).push(control);
    }
  }
  updateWebSiteControls() {
    const prevSelectedItems = [];
    this.websiteList.map((o, i) => {
      const existedValue = this.subCampDetails.displayTargetDetails.websiteTypes.includes(o);
      prevSelectedItems.push(existedValue);
    });
    this.targetSectionFormGroup.controls.webSiteName.setValue(prevSelectedItems);
  }
  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map(control => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => next ? prev + next : prev, 0);
      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };
    return validator;
  }
  addDeviceControls() {
    for (const value of this.deviceList) {
      let control = new FormControl(false);
      if (this.pageMode === 'Edit' && this.subCampDetails.displayTargetDetails.deviceTypes.includes(value)) {
        control = new FormControl(true);
      }
      (this.targetSectionFormGroup.controls.deviceName as FormArray).push(control);
    }
  }
  updateDeviceControls() {
    const prevSelectedItems = [];
    this.deviceList.map((o, i) => {
      const existedValue = this.subCampDetails.displayTargetDetails.deviceTypes.includes(o);
      prevSelectedItems.push(existedValue);
    });
    this.targetSectionFormGroup.controls.deviceName.setValue(prevSelectedItems);
  }
  onSelect(brandId) {
      this.brandName = null;
        for (let i = 0; i < this.brandList.length; i++) {
          if (this.brandList[i]._id === brandId) {
            this.brandName = this.brandList[i].brandName;
         }
        }
  }
  getAdvertiser(): void {
    /*     TODO--->Dependent on "AdvertiserUser-task",
     because for each advertiser multiple user will exist.
     So, based on the user need to get advertiserId */
    this.advertiserService.getAdvertiserByEmail(this.userEmail).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.subCampDetails.advertiserId = res['_id'];
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAdvertisers : ' + err);
      });
  }
  getBrands(): void {
    this.brandService.getAllBrands().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.brandList = <Brand[]>res;
        this.subCampDetails.brandId = this.brandList[0]._id;
        this.brandName = this.brandList[0].brandName;
        this.getHVAList();
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAllBrands : ' + err);
      });
  }
  getHVAList(): void {
    this.spinner.showSpinner.next(true);
    this.audienceHTTPService.getAllSavedAudience(this.subCampDetails.brandId).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.hvaList = <string[]>res;
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAllBrands : ' + err);
      });
  }

  displayNull(value) {
    return null;
  }

  getCampaigns(): void {
    this.campaignService.getAllCampaigns().subscribe(
      res => {
        this.spinner.showSpinner.next(true);
        this.campaignList = <CampaignElement[]>res;
        this.campaignListByBrandId = this.campaignList;
        if (this.newCampaignId) {
          this.subCampDetails.campaignId = null;
          setTimeout(() => {
            this.spinner.showSpinner.next(false);
            this.subCampDetails.campaignId = this.newCampaignId;
            this.basicSectionFormGroup.controls.name.setValue(this.newCampaignId);
          }, 500);
        } else {
          this.spinner.showSpinner.next(false);
        }
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAllCampaigns : ' + err);
      });
  }

  getObjectives(): void {
    this.utilHTTPService.getObjectives().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.objectiveList = <ObjectiveElement[]>res;
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getObjectives : ' + err);
      });
  }



  getBanners(): void {
    this.utilHTTPService.getBanners().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.appBanners = [];
        this.webBanners = [];
        (<Banner[]>res).forEach((element: any) => {
          if (element.platform.toLowerCase() === 'mobileapp') {
            element.checked = false;
            element.type = 'app';
            this.appBanners.push(element);
          }
          if (element.platform.toLowerCase() === 'website') {
            element.checked = false;
            element.type = 'web';
            this.webBanners.push(element);
          }
        });
        /*Todo : Publisher assets linkage.
        Based on assets banner sizes, campagin banner sizes selection should enable
        Till that linkage has done, will enable few campaign banner sizes */
       /*We can remove this method later,
        this.setBannerSizeEnableTimeBeing();*/
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getBanners : ' + err);
      });
  }
  setBannerSizeEnableTimeBeing() {
    /*Existed sizes
      250 x 250, 200 x 200, 468 x 60,
      728 x 90, 300 x 250, 336 x 280,
      120 x 600, 160 x 600, 300 x 600, 970 x 90 */
    this.assetService.getBannerSizesForExistedAssets().subscribe(
      (res: any[]) => {
        const that = this;
        for (const item of res) {
          if (item['platformType'] === 'Website' && item.assetDimensions.trim()) {
            that.webBanners.find(x => x.size === item.assetDimensions).checked = true;
          } else if (item.assetDimensions.trim()) {
            that.appBanners.find(x => x.size === item.assetDimensions).checked = true;
          }
        }
      },
      err => {
        console.log('getBannerSizesForExistedAssets : Error ' + err);
      }
    );
  }
  getWebsiteNames(): void {
    this.utilHTTPService.getAppTypes().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        res.forEach(appEelement => {
          this.websiteList.push(appEelement['appType']);
        });
        this.addWebSiteControls();
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAppTypes : ' + err);
      });
  }
  getPotentialReach(): void {
    this.spinner.showSpinner.next(true);
    if (this.subCampDetails && this.subCampDetails.audienceTargetIds &&
        this.subCampDetails.audienceTargetIds.length > 0) {
      this.campaignService.getPotentialBanners({subcampaignId: this.subCampDetails._id,
                        audienceTargetIds: this.subCampDetails.audienceTargetIds}).subscribe(
        bannersRes => {
          this.spinner.showSpinner.next(false);
          this.potentialContnet = true;
          this.potentialTotalHcps = +bannersRes['totalHcps'];
          this.potentialReach = +bannersRes['potentialReach'];
          this.percentagePotential = ((this.potentialReach / this.potentialTotalHcps) * 100);
          this.bannerDetails = bannersRes['bidAndBannerDetails'];
          if (bannersRes && bannersRes['bidAndBannerDetails']
                  && bannersRes['bidAndBannerDetails']['bannerSizes']) {
              const that = this;
              if (bannersRes['bidAndBannerDetails']['bannerSizes'].length === 0) {
                that.webBanners.forEach(webElement => {
                  webElement.checked = false;
                  webElement.type = 'web';
                });
                that.appBanners.forEach(appBanners => {
                  appBanners.checked = false;
                  appBanners.type = 'app';
                });
              } else if (bannersRes['bidAndBannerDetails']['bannerSizes'].length > 0) {
                for (const item of bannersRes['bidAndBannerDetails']['bannerSizes']) {
                  if (((typeof item.size === 'string') && (typeof item['platformType']  === 'string' )) &&
                  item['platformType'] === 'Website' && item.size.trim()) {
                    that.webBanners.find(x => x.size === item.size).checked = true;
                    if (this.pageMode === 'Edit' &&
                    that.subCampDetails.creativeSpecifications.creativeDetails.find(obj => obj.size === item.size 
                      && obj.platformType === 'web')) {
                      that.webBanners.find(x => x.size === item.size).uploadStatus = true;
                    }
                  } else if ((typeof item.size === 'string') && item['platformType'] === 'MobileApp' && item.size.trim()) {
                    that.appBanners.find(x => x.size === item.size).checked = true;
                    if (this.pageMode === 'Edit' &&
                    that.subCampDetails.creativeSpecifications.creativeDetails.find(obj => obj.size === item.size 
                      && obj.platformType === 'app')) {
                      that.appBanners.find(x => x.size === item.size).uploadStatus = true;
                    }
                  }
                }
              }
          }
        },
        bannersErr => {
          this.spinner.showSpinner.next(false);
          this.potentialContnet = false;
          this.potentialTotalHcps = 0;
          this.potentialReach = 0;
          this.percentagePotential = 0;
          console.log('Error! getPotentialBanners : ' + bannersErr);
        });
    } else {
      this.spinner.showSpinner.next(false);
      this.potentialTotalHcps = 0;
      this.potentialReach = 0;
      this.percentagePotential = 0;
    }
  }
  callOnSelectBidAmountType(event) {
    if (event.value === 'CPM') {
      this.minBidAmount = this.bannerDetails.cpm.minCpmBidRange;
      this.maxBidAmount = this.bannerDetails.cpm.maxCpmBidRange;
    } else if (event.value === 'CPC') {
      this.minBidAmount = this.bannerDetails.cpc.minCpcBidRange;
      this.maxBidAmount = this.bannerDetails.cpc.maxCpcBidRange;
    }
    this.operationalFormGroup = this._formBuilder.group({
      startDate: [this.operationalFormGroup.get('startDate').value, Validators.required],
      endDate: [this.operationalFormGroup.get('endDate').value, Validators.required],
      bidAmountType: [this.operationalFormGroup.get('bidAmountType').value, Validators.required],
      bidAmountValue: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(1000)])],
      totalBudget: [this.operationalFormGroup.get('totalBudget').value, Validators.required],
      limitToValue: [this.operationalFormGroup.get('limitToValue').value, Validators.required],
      limitToType: [this.operationalFormGroup.get('limitToType').value, Validators.required]
    }, {
        validator: [CheckBids('bidAmountValue', 'limitToValue', 'totalBudget')]
      });
  }
  onChange(selecteddValue: string, event: boolean, existedDBValues: string[], type: string) {
    let tempArray = [];
    if (existedDBValues.length > 0) {
      tempArray = tempArray.concat(existedDBValues);
    }
    if (event) {
      tempArray.push(selecteddValue);
    } else {
      const index = tempArray.indexOf(selecteddValue);
      tempArray.splice(index, 1);
    }
    if (type === 'genderType') {
      this.subCampDetails.targetDetails.demoGraphics.gender = tempArray;
    }
    if (type === 'behaviourType') {
      this.subCampDetails.targetDetails.demoGraphics.behaviours = tempArray;
    }
    if (type === 'deviceType') {
      this.subCampDetails.displayTargetDetails.deviceTypes = tempArray;
    }
    if (type === 'webSiteType') {
      this.subCampDetails.displayTargetDetails.websiteTypes = tempArray;
    }
  }

  getBidTypes() {
    this.bidList = [
      { 'id': 1, 'value': 'CPM', 'displayName': 'CPM' },
      { 'id': 2, 'value': 'CPC', 'displayName': 'CPC' }];
      /*Removed as part of PLAT-594, actually its existed from long back,
      { 'id': 3, 'value': 'CPV', 'displayName': 'CPV' }]; */
  }
  getBidLimitTypes() {
    this.bidLimitList = [
      { 'id': 1, 'value': 'Per day', 'displayName': 'Per day' },
      { 'id': 2, 'value': 'Per week', 'displayName': 'Per week' },
      { 'id': 3, 'value': 'Per month', 'displayName': 'Per month' }];
  }
  basicTabChange(selTabName) {
    this.subCampDetails.creativeType = selTabName['name'];
  }
  addCampaign(): void {
    const dialogRef = this.dialog.open(AddCampaignComponent, {
      width: '40%',
      data: { category: 'single' }
    });
    dialogRef.afterClosed().subscribe(result => {
     if (result) {
      this.campaignService.storeCampaign(result).subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          this.getCampaigns();
          this.newCampaignId = res['id'];
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: res['message']
            }
          });
        },
        err => {
          this.spinner.showSpinner.next(false);
          console.log('Error! storeCampaign : ' + err);
        });
     }
    });
  }
  saveSubCampaign(stepper: MatStepper): void {
    if (this.subCampDetails._id.length > 1) {/*This condition is to valid whether the user clicks on first time or not*/
      this.spinner.showSpinner.next(true);
      if (this.targetSectionFormGroup.valid) {
        this.getPotentialReach();
      }
      this.campaignService.updateSubCampaign(this.subCampDetails._id, this.subCampDetails).subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          stepper.next();
        },
        err => {
          if ( err.error.message === 'Sub Campaign Name not available') {
            const formGroup: FormGroup = this.basicSectionFormGroup;
            const subCampCtrl = formGroup.controls['subCampaignName'];
            subCampCtrl.setErrors({ nameNotAvailable: true });
          }
          this.spinner.showSpinner.next(false);
          console.log('Error! updateSubCampaign : ' + err);
        });
    } else if (this.basicSectionFormGroup.valid && this.checkSubCampaignName()) {
      this.spinner.showSpinner.next(true);
      this.campaignService.storeSubCampaign(this.subCampDetails).subscribe(
        res => {
          this.defaultEnableTargetSectionFields();
          this.spinner.showSpinner.next(false);
          this.subCampDetails._id = res['id'];
          
          let campaignId = this.basicSectionFormGroup.get('name').value;
          let selectedCampaignName = this.campaignListByBrandId.find(x => x._id == campaignId).name;
          let eventProperties = {
            campaignName: selectedCampaignName
          }
          this.amplitudeService.logEvent('campaign_creation - create basic details', eventProperties);
          stepper.next();
        },
        err => {
          if ( err.error.message === 'Sub Campaign Name not available') {
            const formGroup: FormGroup = this.basicSectionFormGroup;
            const subCampCtrl = formGroup.controls['subCampaignName'];
            subCampCtrl.setErrors({ nameNotAvailable: true });
          }
          this.spinner.showSpinner.next(false);
          console.log('Error! saveSubCampaign : ' + err.error.message);
        });
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'success',
          message: 'Please fill required fields'
        }
      });
    }
  }

  defaultEnableTargetSectionFields(): void {
    // this.subCampDetails.audienceTargetIds = this.hvaList.map(x => x['_id']);
    this.subCampDetails.displayTargetDetails.websiteTypes = this.websiteList;
    this.updateWebSiteControls();
    this.subCampDetails.displayTargetDetails.deviceTypes = this.deviceList;
    this.updateDeviceControls();
  }

  nextStepper(event: KeyboardEvent, stepper: MatStepper, currentSection: FormGroup) {
    /*To make feasible keywords enter functionality, passing event.
    event[screenX] will be greater only user clicks on next button*/

    // TODO: Commenting this till the time we have a decent reach
    if (/*this.potentialReach  > 0 &&*/ currentSection.valid && (event['screenX'] > 0)) {
      this.isSectionFormValid = true;
      stepper.next();
      this.updateSubCampaign(currentSection);
      
      let hvaIds: [] = currentSection.get('hvaIds').value;
      let hvaNames = [];
      hvaIds.forEach(_id => {
        hvaNames.push(this.getHVAName(_id))
      }); 
      let eventProperties = {
        hvaListNames: hvaNames
      }
      this.amplitudeService.logEvent('campaign_creation - select audience', eventProperties);
    } else if (currentSection.valid && this.potentialReach  <= 0) {
      this.isSectionFormValid = false;
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Potential should be greater than zero'
        }
      });
    } else {
      this.isSectionFormValid = false;
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Please fill required fields'
        }
      });
    }
  }
  backToPreviousStepper(currentSection: FormGroup) {
    this.isSectionFormValid = true;
  }
  collapseStepper() {
    if (this.verifyCTALink !== this.ctaLinkState.valid) {
      this.validateExternalURL();
    } else if (this.imgList && this.imgList.length > 0) {
      const nodes = document.querySelectorAll('.mat-vertical-stepper-content');
      const last = nodes[nodes.length - 1];
      (last as HTMLElement).style.visibility  = 'hidden';
      (last as HTMLElement).style.height  = '10px';
      document.querySelectorAll('.mat-step-icon-state-noNumber')[0].className = 'mat-step-icon-state-done mat-step-icon';
      if (this.publishBtn) {
        this.updateSubCampaign(this.operationalFormGroup);
        this.publishBtn.nativeElement.focus();
      } else if (this.saveAndPublishBtn && this.publishedSubCampDetails &&
        (JSON.stringify(this.subCampDetails.creativeSpecifications.creativeDetails) !==
         JSON.stringify(this.publishedSubCampDetails.creativeSpecifications.creativeDetails)) &&
         (this.subCampDetails.status === SubcampaignStatus.approved || this.subCampDetails.status === SubcampaignStatus.paused)) {
        this.subCampDetails.status = SubcampaignStatus.underReview;
        this.updateSubCampaign(this.creativeFormGroup);
      } else if (this.deleteBtn) {
        this.updateSubCampaign(this.operationalFormGroup);
        this.deleteBtn.nativeElement.focus();
      } else if (this.pauseBtn) {
        this.pauseBtn.nativeElement.focus();
      } else if (this.saveAsDraftBtn) {
        this.saveAsDraftBtn.nativeElement.focus();
      } else if (this.resumeBtn) {
        this.resumeBtn.nativeElement.focus();
      }
    } else {
      this.isSectionFormValid = false;
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Atleast one banner has to select & upload in "UPLOAD CREATIVE tab"'
        }
      });
    }
  }
  saveSubCampaignAsDraft(): void {
    if (this.subCampDetails && this.subCampDetails._id) {
      this.spinner.showSpinner.next(true);
      this.campaignService.updateSubCampaign(this.subCampDetails._id, this.subCampDetails).subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: res['message']
            }
          });
          if (this.isCreateCreativeHub) {
            // create creative hub.
            if (this.subCampDetails.creativeType === 'Banner') {
              this.creativeHubPostData.creativeType = 'image';
            }
            this.creativeHubPostData.name = this.subCampDetails.name;
            this.creativeHubPostData.status = 'uploaded';
            this.creativeHubPostData.sectionName = 'Default';
            this.creativeHubPostData.deletedUsers = [];
            // this.creativeHubPostData.creativeDetails = this.subCampDetails.creativeSpecifications.creativeDetails;
            /*this.creativeHubService.addCreativeHub(this.creativeHubPostData).subscribe(
              creativeRes => {

              },
              err => {

            });*/
          }
        },
        err => {
          if ( err.error.message === 'Sub Campaign Name not available') {
           const formGroup: FormGroup = this.basicSectionFormGroup;
           const subCampCtrl = formGroup.controls['subCampaignName'];
           subCampCtrl.setErrors({ nameNotAvailable: true });
          }
          this.spinner.showSpinner.next(false);
          console.log('Error! updateSubCampaign : ' + err);
      });
    }
  }
  updateSubCampaign(currenSection: FormGroup): void {
    this.campaignStatus = {};
    if (this.subCampDetails && this.subCampDetails._id && currenSection.valid &&
      (currenSection !== this.creativeFormGroup || (currenSection === this.creativeFormGroup && this.imgList && this.imgList.length > 0))) {
      this.isSectionFormValid = true;
      this.spinner.showSpinner.next(true);
      if (currenSection === this.creativeFormGroup) {
          this.subCampDetails.isPublished = true;
      }
      if (currenSection === this.creativeFormGroup &&
          this.subCampDetails.status === SubcampaignStatus.recentlyCompleted) {
          this.subCampDetails.status = SubcampaignStatus.underReview;
      }
      this.campaignService.updateSubCampaign(this.subCampDetails._id, this.subCampDetails).subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          this.amplitudeService.logEvent('campaign_creation - create operational details', null);
          if (this.publishedSubCampDetails &&
              (JSON.stringify(this.subCampDetails.operationalDetails) !==
               JSON.stringify(this.publishedSubCampDetails.operationalDetails)) &&
              (this.subCampDetails.status === SubcampaignStatus.approved ||
               this.subCampDetails.status === SubcampaignStatus.paused ||
               this.subCampDetails.status === SubcampaignStatus.recentlyCompleted)) {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-advertiser'],
                  data: {
                    icon: 'success',
                    message: 'It will take upto 4hrs to reflect your "Operational Details" changes'
                  }
                });
          } else if (currenSection === this.creativeFormGroup) {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: 'Your campaign has been published, its in review state,' +
                'and will be active post approval of Doceree Admin'
              }
            });
            this.amplitudeService.logEvent('campaign_creation - campaign published', null);

            this.router.navigate(['/advertiser/dashboard']);
          } else if (this.campaignStatus && this.campaignStatus.name &&
                      this.campaignStatus.name && this.campaignStatus.status &&
                      this.campaignStatus.status === SubcampaignStatus.recentlyCompleted) {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: 'Sub-campaign "' + this.campaignStatus.name +
                        '" will remain active for next 7 days(though no ads will be pulished); '
              }
            });
          } else {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: res['message']
              }
            });
          }
          if (this.isCreateCreativeHub) {
            // create creative hub.
            if (this.subCampDetails.creativeType === 'Banner') {
              this.creativeHubPostData.creativeType = 'image';
            }
            this.creativeHubPostData.name = this.subCampDetails.name;
            this.creativeHubPostData.status = 'uploaded';
            this.creativeHubPostData.sectionName = 'Default';
            this.creativeHubPostData.deletedUsers = [];
            // this.creativeHubPostData.creativeDetails = this.subCampDetails.creativeSpecifications.creativeDetails;
            /*this.creativeHubService.addCreativeHub(this.creativeHubPostData).subscribe(
              creativeRes => {

              },
              err => {

            });*/
          }
        },
        err => {
          if ( err.error.message === 'Sub Campaign Name not available') {
           const formGroup: FormGroup = this.basicSectionFormGroup;
           const subCampCtrl = formGroup.controls['subCampaignName'];
           subCampCtrl.setErrors({ nameNotAvailable: true });
          }
          if ( err.error.message === 'No payment method linked with the brand​' &&
          this.rolePermissions && this.rolePermissions.includes('view_payment_method')) {
            const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
              width: '40%',
              data: {
                'title' : 'No payment method linked to your brand. Do you want to add it now?'
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate(['/advertiser/payments']);
              }
            });
          }
          this.spinner.showSpinner.next(false);
          console.log('Error! updateSubCampaign : ' + err);
          this.subCampDetails.isPublished = false;
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
        });
    } else {
      this.isSectionFormValid = false;
      if (currenSection === this.creativeFormGroup && this.imgList && this.imgList.length <= 0) {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: 'Atleast one banner has to select & upload in "UPLOAD CREATIVE tab"'
          }
        });
      } else {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: 'Please fill required fields'
          }
        });
      }
    }
  }
  setStartDate() {
      this.startMinRageDatePicker = new Date();
      if (this.subCampDetails.operationalDetails.endDate) {
        this.endDatePicker = new Date(this.subCampDetails.operationalDetails.endDate);
        this.endDatePicker.setHours(23, 59, 59, 59);
      }
  }
  setEndDate() {
    const todaysDate = new Date();
    const selectedStartDate = new Date(this.subCampDetails.operationalDetails.startDate);
    this.endMinRageDatePicker = todaysDate;
    if (this.subCampDetails.operationalDetails.startDate && (selectedStartDate > todaysDate)) {
      this.endMinRageDatePicker = selectedStartDate;
    }
  }
  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDatePicker = new Date(event.value);
    this.subCampDetails.operationalDetails.startDate = event.value;
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDatePicker = new Date(event.value);
    this.endDatePicker.setHours(23, 59, 59, 59);
    this.subCampDetails.operationalDetails.endDate = this.endDatePicker;
  }

  clearBanner(banner) {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '40%',
      data: {
        'title' : 'Do you want to delete creative : ' + banner['description']
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.clearingBanner = true;
        this.webBanners.forEach(webElement => {
          if (webElement._id === banner._id &&
            webElement.type === banner.type) {
            webElement.uploadStatus = false;
            webElement.type = 'web';
            webElement.checked = true;
            this.deleteUploadedFileFromS3(banner);
          }
        });
        this.appBanners.forEach(appElement => {
          if (appElement._id === banner._id &&
            appElement.type === banner.type) {
            appElement.uploadStatus = false;
            appElement.checked = true;
            appElement.type = 'app';
          }
        });
      }
    });
  }
  getHVAName(audTargetId) {
    const filteredHVA: any = this.hvaList.filter((x: any) => x._id === audTargetId);
    return ((filteredHVA && filteredHVA.length > 0) ? filteredHVA[0].name : '');
}
  deleteUploadedFileFromS3(banner) {
    this.uploadService.fetchBrandIdentifierS3();
    this.uploadService._brandIdentifySource.subscribe(
      brandIdentifier => {
        const s3path = 'https://' + config['AWS-S3']['BUCKET'] + '.s3.amazonaws.com/' +
          config['AWS-S3']['FOLDER'] + '/' + brandIdentifier + '/';
        const fileDetails: any = this.subCampDetails.creativeSpecifications.creativeDetails.find(o => (o.size === banner.size));
        this.subCampDetails.creativeSpecifications.creativeDetails =
          this.subCampDetails.creativeSpecifications.creativeDetails.filter(o => (o.size !== banner.size));
        this.imgList = [];
        this.subCampDetails.creativeSpecifications.creativeDetails.forEach(creative => {
          this.imgList.push(creative.url);
        });
        this.campaignService.updateSubCampaign(this.subCampDetails._id, this.subCampDetails).subscribe();
        if (fileDetails) {
          const fileName = fileDetails.url.replace(s3path, '');
          this.uploadService.deletefile(fileName, brandIdentifier);
        } else {
          // TODO: need to handle this scenario
        }
      }
    );
  }

  openFileSelectionDialog(banner) {
    if (this.clearingBanner === true) {
      this.clearingBanner = false;
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'success',
          message: 'Banner upload cleared'
        }
      });
      return;
    }
    if (!banner.checked) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Please click on highlited banner, to upload an image'
        }
      });
      this.selectedFiles = null;
      return;
    }
    if (banner.uploadStatus) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Already image has been uploaded for the selected banner : ' + banner.name
        }
      });
      this.selectedFiles = null;
      return;
    }
    this.selectedBannerToUpload = banner;
    /*banner.checked = (banner.checked) ? !banner.checked : true;*/
    const confirmDialogRef = this.dialog.open(UploadCampaignCreativePopupComponent, {
      width: '40%',
      data: {
        creativeDetails: this.selectedBannerToUpload,
        creativeType: 'image'
      }
    });
    confirmDialogRef.beforeClosed().subscribe(confirmResultBC => {
      if (confirmResultBC.isFromLocal && !confirmResultBC.isClose) {
        this.event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(this.event);
        this.isFromLocal = confirmResultBC.isFromLocal;
      }
    });
    confirmDialogRef.afterClosed().subscribe(confirmResult => {
      if (!confirmResult.isFromLocal && !confirmResult.isClose) {
        this.isFromLocal = confirmResult.isFromLocal;
        const extension = ((confirmResult.creative.formatType.toUpperCase() === 'JPG' ||
                            confirmResult.creative.formatType.toUpperCase() === 'JPEG' ||
                            confirmResult.creative.formatType.toUpperCase() === 'PNG' ||
                            confirmResult.creative.formatType.toUpperCase() === 'GIF') ?
                              confirmResult.creative.formatType : this.getFileExtension(confirmResult.creative.formatType));
        this.selectedCreative = {
          formatType: extension,
          url: confirmResult.creative.url, weight: '', resolution: confirmResult.creative.resolution,
          size: confirmResult.creative.size,
          platformType: 'web',
          style: ''
        };
        this.uploadFile();
        this.webBanners.forEach(webElement => {
          if (webElement._id === this.selectedBannerToUpload._id &&
            webElement.type === this.selectedBannerToUpload.type) {
            webElement.uploadStatus = true;
          }
        });
        this.appBanners.forEach(appElement => {
          if (appElement._id === this.selectedBannerToUpload._id &&
            appElement.type === this.selectedBannerToUpload.type) {
            appElement.uploadStatus = true;
          }
        });
      }
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.length === 0) {
      return;
    }
    const mimeType = this.selectedFiles[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: 'Only images are supported.'
        }
      });
      return;
    } else if (this.selectedFiles && this.selectedFiles[0] && this.selectedFiles[0].size
      && ((this.selectedFiles[0].size / +FILE_SIZES[config['SIZES']['BANNER'].split('~')[1]]) >
        +config['SIZES']['BANNER'].split('~')[0])) {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: 'Selected image size is exceed, it should below ' + config['SIZES']['BANNER']
          }
        });
      return;
    }

    const reader = new FileReader();
    this.imagePath = this.selectedFiles;
    reader.readAsDataURL(this.selectedFiles[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    const that = this;
    reader.onloadend = (_event) => {
      const invokeData = setInterval(() => {
        clearInterval(invokeData);
        if ((this.selectedBannerToUpload.size.split(' x ')[0] === this.imageDimensions.elementWidth.toString())
          && (this.selectedBannerToUpload.size.split(' x ')[1] === this.imageDimensions.elementHeight.toString())) {
          this.openBannerPreview({
            banner: this.selectedBannerToUpload, url: this.imgURL,
            width: this.imageDimensions.elementWidth, height: this.imageDimensions.elementHeight
          });
        } else {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'error',
              message: 'Selected banner dimensions and browsed image dimensions are not matched'
            }
          });
          this.selectedFiles = null;
          return;
        }
      }, 100);
    };
  }

  openBannerPreview(selectedData) {
    let dialogRef: any;
    dialogRef = this.dialog.open(BannerCropperComponent, {
      width: '75%',
      height: '75%',
      data: selectedData
    });
    dialogRef.afterClosed().subscribe(result => {
      this.uploadFile();
      /*this.selectedBannerToUpload.uploadStatus = true;*/
      this.webBanners.forEach(webElement => {
        if (webElement._id === this.selectedBannerToUpload._id &&
          webElement.type === this.selectedBannerToUpload.type) {
          webElement.uploadStatus = true;
        }
      });
      this.appBanners.forEach(appElement => {
        if (appElement._id === this.selectedBannerToUpload._id &&
          appElement.type === this.selectedBannerToUpload.type) {
          appElement.uploadStatus = true;
        }
      });
    });
  }
  checkSubCampaignName() {
    const formGroup: FormGroup = this.basicSectionFormGroup;
    const subCampCtrl = formGroup.controls['subCampaignName'];
    const subCamp: string = subCampCtrl.value;
    const campId: string = formGroup.get('name').value;
    const subCamEditingName = this.editingSubCamName.toLowerCase().trim();

    if (!subCamp) {
      subCampCtrl.setErrors({ require: true });
      return false;
    } else if (subCamp && subCamp.trim().length < 3 ) {
      subCampCtrl.setErrors({ lessLength: true });
      return false;
    }
    return true;
  }

  getFileExtension(browsedFile: string): string {
    return ((browsedFile === 'image/jpg' ||
    browsedFile === 'image/jpeg' ||
    browsedFile === 'image/jpe' ||
    browsedFile === 'image/jif' ||
    browsedFile === 'image/jfif' ||
    browsedFile === 'image/jfi') ? 'JPEG' :
    ((browsedFile === 'image/png') ? 'PNG' :
    ((browsedFile === 'image/gif') ? 'GIF' : 'NotValidFormat')));
  }

  uploadFile() {
    this.spinner.showSpinner.next(true);
    if (this.isFromLocal) {
      const file = this.selectedFiles.item(0);
      this.subCampDetails.creativeSpecifications.creativeDetails.push({
        formatType: this.getFileExtension(this.selectedFiles[0].type),
        url: '', weight: '', resolution: this.selectedBannerToUpload.name.toString(),
        size: this.selectedBannerToUpload.size.toString(),
        platformType: this.selectedBannerToUpload.type.toString(),
        style: ''
      });
      const that = this;
      const fileExtension = this.getFileExtension(this.selectedFiles[0].type);
      const bannerSizeTrim = this.selectedBannerToUpload.size.toString().replace(/\s/g, '');
      const fileName = this.subCampDetails._id + this.selectedBannerToUpload.type.toString() + bannerSizeTrim;
      const resolution = this.selectedBannerToUpload.name.toString();
      const size = this.selectedBannerToUpload.size.toString();

      this.uploadService.fetchBrandIdentifierS3();
      this.uploadService._brandIdentifySource.subscribe(
        brandIdentifier => {
          // uploading the file for the first time - this is for campaign
          this.uploadService.uploadFileAndGetResult(file, fileName, brandIdentifier, fileExtension).send(function (err, data) {
            if (err) {
              that.spinner.showSpinner.next(false);
              return false;
            }
            that.subCampDetails.creativeSpecifications.creativeDetails
            [(that.subCampDetails.creativeSpecifications.creativeDetails.length - 1)].url = data['Location'];
            that.imgList = [];
            that.subCampDetails.creativeSpecifications.creativeDetails.forEach(creative => {
              that.imgList.push(creative.url);
            });
            that.spinner.showSpinner.next(false);
            that.imgList = [];
            that.subCampDetails.creativeSpecifications.creativeDetails.forEach(creative => {
              that.imgList.push(creative.url);
            });
            that.campaignService.updateSubCampaign(that.subCampDetails._id, that.subCampDetails).subscribe();
            });

          // uploading the same file for the second time - this time its for creative hub
          const creativeHubFileName = 'ch-' + this.subCampDetails._id + this.selectedBannerToUpload.type.toString() + bannerSizeTrim;
          this.uploadService.uploadFileAndGetResult(file, creativeHubFileName, brandIdentifier, fileExtension).send(function (err, data) {
            if (err) {
              that.spinner.showSpinner.next(false);
              return false;
            }

            that.creativeHubPostData.creativeDetails.push({
              formatType: fileExtension,
              url: data['Location'],
              size: size,
              resolution: resolution,
              styleName: '',
            });
            that.isCreateCreativeHub = true;
            if (that.subCampDetails.creativeType === 'Banner') {
              that.creativeHubPostData.creativeType = 'image';
            }
            that.creativeHubPostData.name = that.subCampDetails.name;
            that.creativeHubPostData.status = 'uploaded';
            that.creativeHubPostData.sectionName = 'Default';
            that.creativeHubPostData.deletedUsers = [];
            that.creativeHubService.addCreativeHub(that.creativeHubPostData).subscribe();
            that.spinner.showSpinner.next(false);
            let eventProperties = {
              size: size,
              resolution: resolution
            };
            that.amplitudeService.logEvent('campaign_creation - upload creative', eventProperties);

          });
        });
    } else {
      this.isCreateCreativeHub = false;
      this.subCampDetails.creativeSpecifications.creativeDetails.push(this.selectedCreative);
      this.campaignService.updateSubCampaign(this.subCampDetails._id, this.subCampDetails).subscribe();
      this.imgList = [];
      this.subCampDetails.creativeSpecifications.creativeDetails.forEach(creative => {
        this.imgList.push(creative.url);
      });
      this.spinner.showSpinner.next(false);
    }
    this.selectedFiles = null;
  }

  public fileOver(event) {
    console.log('fileOver : ' + event);
  }

  public fileLeave(event) {
    console.log('fileLeave : ' + event);
  }

  deleteSubCampaign(id) {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '40%',
      data: {
        'title' : 'Do you want to delete this sub campaign, This action cannot be undone !'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.campaignService.deleteSubCampaign(id).subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.router.navigate(['/advertiser/campaignManage']);
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
      }
    });
  }
  changeSubcampaignStatus(campaign: any, status: string) {
    this.campaignStatus = {'name': campaign.name, 'status': status};
    let message = '';
    switch (status) {
      case SubcampaignStatus.paused: {
        message = 'Are you sure you want to pause "' + campaign.name + '" sub campaign ?';
        break;
      }
      case SubcampaignStatus.approved: {
        message = 'Do you want to resume "' + campaign.name + '" sub campaign ?';
        break;
      }
      case SubcampaignStatus.recentlyCompleted: {
        message = 'Are you sure you want to end "' + campaign.name + '" sub campaign ?';
        break;
      }
    }
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '40%',
      data: {
        'title' : message
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const campainDetails = {
          id: campaign._id,
          status: status,
          name: campaign.name,
          brandId: campaign.brandId
        };
        this.campaignService.approveCampaign(campainDetails).subscribe(
          data => {
            if (this.campaignStatus.status === SubcampaignStatus.recentlyCompleted) {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-advertiser'],
                data: {
                  icon: 'success',
                  message: 'Sub-campaign "' + this.campaignStatus.name +
                  '" will remain active for next 7 days(though no ads will be pulished); '
                }
            });
          } else {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-advertiser'],
            data: {
              icon: 'success',
              message: data['message']
            }
          });
          }
            
            this.eventEmitterService.onReadNotification();
            this.router.navigate(['/advertiser/campaignManage']);
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          });
      }
    });
  }

  validateExternalURL() {
    if (this.creativeFormGroup.valid) {
      this.verifyCTALink = this.ctaLinkState.inProgress;
      this.spinner.showSpinner.next(true);
      this.utilHTTPService.validateExternalURL(this.subCampDetails.creativeSpecifications.ctaLink)
      .subscribe(
        res => {
          this.spinner.showSpinner.next(false);
          if (res && res.toLowerCase() === 'true') {
          // if (res && res['statusCode'] !== undefined && res['statusCode'] === 200) {
            this.verifyCTALink = this.ctaLinkState.valid;
            const nodes = document.querySelectorAll('.mat-vertical-stepper-content');
            const last: any = nodes[nodes.length - 1];
            if (last && last.style && last.style.visibility !== 'hidden') {
              this.collapseStepper();
            }
          } else {
            this.verifyCTALink = this.ctaLinkState.notValid;
          }
        },
        err => {
          this.verifyCTALink = this.ctaLinkState.notValid;
          this.spinner.showSpinner.next(false);
          console.log('Error validateExternalURL');
        }
      );
    }
  }

}
