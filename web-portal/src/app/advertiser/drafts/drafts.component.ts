import { Component, OnInit, Injectable, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatSnackBar, MatTableDataSource, MatDialogConfig, MatDialog, MatOptionModule, MatSort} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { AddCampaignComponent } from '../campaign-creation/add-campaign/add-campaign.component';
import { CountryCurrencyComponent } from 'src/app/admin/configuration/country-currency/country-currency.component';
import { Router, ActivatedRoute } from '@angular/router';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import {Sort} from '@angular/material/sort';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-drafts',
  templateUrl: './drafts.component.html',
  styleUrls: ['./drafts.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DraftsComponent implements OnInit {
  campaignList: any[];
  displayedColumns: string[] = ['campaign', 'dateCreated', 'lastSaved', 'createdBy', 'editdelete'];
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any[];
  filteredCampaigns: any[];
  expandedElement: null;
  campaign = '';
  show = false;
  videoCheck = false;
  imageCheck = false;
  filteredAdvertisers: any[];
  typeList: string[];
  type: string;
  createdBy: any;
  rolePermissions: string[];
  constructor(private spinner: SpinnerService, private campaignService: CampaignService,
    private snackBar: MatSnackBar, private dialog: MatDialog, private router: Router,
    private eventEmitterService: EventEmitterService) {
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();
    }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.renderListView();
  }

  renderListView() {
    this.campaignService.getDraftCampaigns().subscribe(
      resposne => {
        this.spinner.showSpinner.next(false);
        this.dataSource = <[]>resposne;
        this.filteredCampaigns = this.dataSource;
        const users = this.dataSource.map(data => {
          return data.createdBy;
        });
        this.filteredCampaigns.forEach(element => {
          element['showExpand'] = false;
        });
        this.filteredAdvertisers = users.filter((x, i, a) => x && a.indexOf(x) === i);
        // this.typeList = ['All', 'Banner', 'Video'];
        this.typeList = ['Banner'];
        this.searchCampaign('');
        this.searchSubCampaign();
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getAllCampaigns : ' + err);
      });
  }
  toggleSearch() {
    this.show = !this.show;
  }
  editCampaign(id, branddId, campaignName) {
    const dialogRef = this.dialog.open(AddCampaignComponent, {
      width: '50%',
      data: { category: 'single', brandId: branddId, name: campaignName, page: 'Edit' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.name === null) {
          return;
        }
        this.campaignService.updateCampaign(id, result).subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }
  deleteCampaign(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the campaign',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.campaignService.deleteCampaign(id, 'draft').subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }
  deleteSubCampaign(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the sub campaign',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.campaignService.deleteSubCampaign(id).subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-advertiser'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }

  editSubCampaign(bId, Id, subId) {
    this.router.navigate(['/advertiser/campaignCreation'],
      { state: { 'brandId': JSON.stringify(bId), 'campaignId': JSON.stringify(Id), 'subCampaignId': JSON.stringify(subId) } });
  }
  searchCampaign(name) {
    if (!name) {
      this.filteredCampaigns = this.dataSource;
      this.dataSource.forEach(thisCampaign => {
        thisCampaign.filteredSubCampaign = thisCampaign.subDomainDetails;
      });
    } else {
      this.filteredCampaigns = [];
      this.filteredCampaigns = this.dataSource.filter((option) =>
          option.campaign.toLowerCase().indexOf(name.toLowerCase()) !== -1
      );
      if (this.filteredCampaigns.length) {
        this.filteredCampaigns.forEach(thisCampaign => {
          thisCampaign.filteredSubCampaign = thisCampaign.subDomainDetails;
        });
      } else {
        this.dataSource.forEach(thisCampaign => {
          thisCampaign.filteredSubCampaign = thisCampaign.subDomainDetails.filter((option) =>
            option.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
          );
          if (thisCampaign.filteredSubCampaign.length) {
            this.filteredCampaigns.push(thisCampaign);
          }
        });
      }
    }
    return this.filteredCampaigns;
  }
  searchSubCampaign() {
    this.filteredCampaigns = [];
      this.dataSource.forEach(thisCampaign => {
        thisCampaign.filteredSubCampaign = thisCampaign.subDomainDetails.filter(item => {
          if (this.createdBy && this.createdBy !== 'All' && !item.created.by.includes(this.createdBy)) {
            return false;
          }
          if (this.type && this.type !== 'All' && !item.creativeType.includes(this.type)) {
            return false;
          }
          return true;
        });
        if (thisCampaign.filteredSubCampaign.length) {
          this.filteredCampaigns.push(thisCampaign);
        }
      });
  }
  clearFilters() {
    this.createdBy = '';
    this.type = '';
    this.campaign = '';
    this.searchSubCampaign();
  }
  toggleExpand(campaign) {
    this.filteredCampaigns.forEach(element => {
      if (element.id === campaign.id) {
        element['showExpand'] = !element['showExpand'];
      } else {
        element['showExpand'] = false;
      }
    });
  }
  sortData(sort: Sort) {
    const data: any = this.filteredCampaigns.slice();
    if (!sort.active || sort.direction === '') {
      this.filteredCampaigns = data;
      return;
    }
    this.filteredCampaigns = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'createdDate': return compare(a.dateCreated, b.dateCreated, isAsc);
        case 'lastSaved': return compare(a.lastSaved, b.lastSaved, isAsc);
        case 'createdBy': return compare(a.createdBy, b.createdBy, isAsc);
        default: return 0;
      }
    });
  }
  sortSubCampaignData(sort: Sort, position: number) {
    const data: any = this.filteredCampaigns[position].filteredSubCampaign.slice();
    if (!sort.active || sort.direction === '') {
      this.filteredCampaigns[position].filteredSubCampaign = data;
      return;
    }

    this.filteredCampaigns[position].filteredSubCampaign = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'creativeType': return compare(a.creativeType, b.creativeType, isAsc);
        case 'name': return compare(a.name, b.name, isAsc);
        case 'createdDate': return compare(a.dateCreated, b.dateCreated, isAsc);
        case 'lastSaved': return compare(a.lastSaved, b.lastSaved, isAsc);
        case 'createdBy': return compare(a.created.by, b.created.by, isAsc);
        default: return 0;
      }
    });
  }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
