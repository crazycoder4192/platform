import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, CanLoad, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../_services/authentication.service';
import { Location } from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private router: Router, private authService: AuthenticationService, private location: Location) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.authService.currentUserValue;
    if (currentUser) {
      // logged in so return true
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.authService.currentUserValue;
    if (currentUser) {
      // localStorage.removeItem('currentRoute');
      // logged in so return true
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.authService.currentUserValue;
    console.log('this.location.path():',this.location.path());
    const url = `/${route.path}`;
    if (currentUser && currentUser.usertype === `${route.path}`) {
      // logged in so return true
      return true;
    }
    if(this.location.path() !== '/login'){
      localStorage.setItem('currentRoute', this.location.path());
      localStorage.setItem('visitRoute', JSON.stringify({publisher: false, admin: false, advertiser: false}));
    }
    // not logged in so redirect to login page with the return url
    if(currentUser){
      this.authService.logout('')
      .pipe()
      .subscribe(
        data => {
          console.log('Logout successfull : ' + data);
        },
        error => {
          console.log('Logout successfull : ' + error);
      });
    }else{
      this.router.navigate(['/login']);
    }
    return false;
  }
}
