import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreditVoucherService {

  constructor(private http: HttpClient) { }

  addCreditVoucher(data: any) {
    return this.http.post(environment.base_url + 'admin/creditvouchers', data).pipe(map(response => response));
  }

  getAllCreditVouchers() {
    return this.http.get(environment.base_url + 'admin/creditvouchers')
    .pipe(map(response => response));
  }

  getCreditVoucherById(_id: any) {
    return this.http.get(environment.base_url + `admin/creditvouchers/${_id}`).pipe(map(response => response));
  }

  deleteCreditVoucher(_id: any) {
    return this.http.delete(environment.base_url + `admin/creditvouchers/${_id}`).pipe(map(response => response));
  }

  updateCreditVoucher(data: any) {
    return this.http.put(environment.base_url + `admin/creditvouchers/${data._id}`, data).pipe(map(response => response));
  }

}
