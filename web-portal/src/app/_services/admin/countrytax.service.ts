import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CountryTaxService {

  constructor(private http: HttpClient) { }

  getAllCountryTaxes() {
    return this.http.get(environment.base_url + 'admin/countrytaxes')
    .pipe(map(response => response));
  }

  getCountryTaxById(_id: any) {
    return this.http.get(environment.base_url + `admin/countrytaxes/${_id}`).pipe(map(response => response));
  }

  addCountryTax(data: any) {
    return this.http.post(environment.base_url + 'admin/countrytaxes', data).pipe(map(response => response));
  }

  updateCountryTax(data: any) {
    return this.http.put(environment.base_url + `admin/countrytaxes/${data._id}`, data).pipe(map(response => response));
  }

  deleteCountryTax(_id: any) {
    return this.http.delete(environment.base_url + `admin/countrytaxes/${_id}`).pipe(map(response => response));
  }

}
