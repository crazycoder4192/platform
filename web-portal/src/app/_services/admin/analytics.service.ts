import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminAnalyticsService {

  constructor(private http: HttpClient) { }
  
  getAnalytics(data: any) {
    return this.http.post(environment.base_url + 'admin/analytics', data).pipe(map(response => response));
  }
}