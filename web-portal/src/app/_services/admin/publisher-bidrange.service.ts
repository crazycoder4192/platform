import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PublisherBidRangeService {

  constructor(private http: HttpClient) { }

  addPublisherBidrange(data: any) {
    return this.http.post(environment.base_url + 'admin/publisherbidranges', data).pipe(map(response => response));
  }

  getAllPublisherBidranges() {
    return this.http.get(environment.base_url + 'admin/publisherbidranges')
      .pipe(map(response => response));
  }

  getPublisherBidrangeById(_id: any) {
    return this.http.get(environment.base_url + `admin/publisherbidranges/${_id}`).pipe(map(response => response));
  }

  deletePublisherBidrange(_id: any) {
    return this.http.delete(environment.base_url + `admin/publisherbidranges/${_id}`).pipe(map(response => response));
  }

  updatePublisherBidrange(data: any) {
    return this.http.put(environment.base_url + `admin/publisherbidranges/${data._id}`, data).pipe(map(response => response));
  }

}
