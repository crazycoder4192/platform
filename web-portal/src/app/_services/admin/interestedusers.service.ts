import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminInterestedUsersService {

    constructor(private http: HttpClient) { }

    getAdminInterestedUsers() {
        return this.http.get(environment.base_url + `admin/interestedusers`).pipe(map(response => response));
    }

    updateStatusToEmailSent(id: string) {
      return this.http.get(environment.base_url + `admin/interestedusers/emailsent/${id}`).pipe(map(response => response));
  }
}