import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { log } from 'util';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private http: HttpClient) { }

// Begin SMTP services
  getallsmtpdetails() {
    return this.http.get(environment.base_url + `admin/smtpdetails`).pipe(map(response => response));
  }

  getonesmtpdetails(_id: any) {
    return this.http.get(environment.base_url + `admin/smtpdetails`, {params: {id: _id}}).pipe(map(response => response));
  }

  storesmtpvalues(data: any) {
    return this.http.post(environment.base_url + `admin/smtpdetails`, data).pipe(map(response => response));
  }

  updatesmtpvalues(data: any) {
    return this.http.put(environment.base_url + `admin/smtpdetails`, data).pipe(map(response => response));
  }
  // End of SMTP services

  // Begin User Roles & Permissions services
  getallpermissions() {
    return this.http.get(environment.base_url + `admin/userpermissions`).pipe(map(response => response));
  }

  getonepermissions(_id: any) {
    return this.http.get(environment.base_url + `admin/userpermissions`, {params: {id: _id}}).pipe(map(response => response));
  }

  getallpermissionsbyMType(mName) {
    return this.http.get(environment.base_url + `admin/userpermissions`, {params: {moduleName: mName}}).pipe(map(response => response));
  }

  getalluseroles() {
    return this.http.get(environment.base_url + `admin/userroles`).pipe(map(response => response));
  }

  getoneuseroles(_id: any) {
    return this.http.get(environment.base_url + `admin/userroles`, {params: {id: _id}}).pipe(map(response => response));
  }

  getalluserolesByModuleType(mType: any) {
    return this.http.get(environment.base_url + `admin/userroles`, {params: {modulename: mType}}).pipe(map(response => response));
  }

  storeuserroles(data: any) {
    return this.http.post(environment.base_url + `admin/userroles`, data).pipe(map(response => response));
  }

  updateuserroles(data: any) {
    return this.http.put(environment.base_url + `admin/userroles`, data).pipe(map(response => response));
  }
  // End of User Roles & Permissions services

  // Begin Country & Currency services
  // get all
  getAllCountriesCourrencies() {
    return this.http.get(environment.base_url + `admin/countriescurrencies`).pipe(map(response => response));
  }
  // add
  addCountryCurrency(data: any) {
    return this.http.post(environment.base_url + `admin/countriescurrencies`, data).pipe(map(response => response));
  }
  // get by id
  getCountryCurrencyById(_id: any) {
    return this.http.get(environment.base_url + `admin/countriescurrencies/${_id}`).pipe(map(response => response));
  }
  // update
  updateCountryCurrency(data: any) {
    return this.http.put(environment.base_url + `admin/countriescurrencies/${data._id}`, data).pipe(map(response => response));
  }
  // delete
  deleteCountryCurrency(_id: any) {
    return this.http.delete(environment.base_url + `admin/countriescurrencies/${_id}`).pipe(map(response => response));
  }
  // End of Country & Currency services
}
