import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminPublisherBillService {
    constructor(
        private http: HttpClient
      ) { }

    getPublisherBills(type: string) {
        return this.http.get(environment.base_url + `admin/publisher/bills/${type}`)
        .pipe(map(response => response));
      }
    updatePublisherBillAsPaid(billId: string, details: any) {
      return this.http.put(environment.base_url + `admin/publisher/bills`, {billId: billId,
        transactionNumber: details.transactionNumber,
        transactionDate: details.transactionDate
      })
      .pipe(map(response => response));
    }
}