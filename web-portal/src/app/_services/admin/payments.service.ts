import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(private http: HttpClient) { }

  getInvoiceById(_id: any) {
    return this.http.get(environment.base_url + `publisher/invoice/${_id}`).pipe(map(response => response));
  }

  updateInvoice(data: any) {
    return this.http.put(environment.base_url + `publisher/invoice/${data._id}`, data).pipe(map(response => response));
  }

  updateQBInvoice(data: any[]) {
    return this.http.put(environment.base_url + `admin/invoices/updateqbinvoice/`, {invoiceIds: data}).pipe(map(response => response));
  }

  updateQBPayment(data: any[]) {
    return this.http.put(environment.base_url + `admin/invoices/updateqbpayment/`, {invoiceIds: data}).pipe(map(response => response));
  }

}
