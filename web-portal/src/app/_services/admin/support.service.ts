import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SupportService {

  constructor(
    private http: HttpClient
  ) { }
  getTickets() {
    return this.http.get(environment.base_url + 'admin/supportticket')
    .pipe(map(response => response));
  }
  getTicket(id: string) {
    return this.http.get(environment.base_url + `admin/supportticket/${id}`)
    .pipe(map(response => response));
  }
  addTicket(data: any) {
    return this.http.post(environment.base_url + 'admin/supportticket', data).pipe(map(response => response));
  }
  updateTicket(data: any) {
    return this.http.put(environment.base_url + 'admin/supportticket', data).pipe(map(response => response));
  }
  getTicketsByEmail() {
    return this.http.get(environment.base_url + 'admin/supportticket')
    .pipe(map(response => response));
  }
  addComment(data: any) {
    return this.http.post(environment.base_url + 'admin/supportticket/comment', data).pipe(map(response => response));
  }
  getSupportArticles() {
    return this.http.get(environment.base_url + 'admin/supportarticles')
    .pipe(map(response => response));
  }
}
