import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminPublisherService {
  constructor(
      private http: HttpClient
    ) { }
  getPublishers() {
      return this.http.get(environment.base_url + `admin/publisher`)
      .pipe(map(response => response));
  }
  getPlatformsByPublisherIds(data: any) {
    return this.http.post(environment.base_url + `admin/publisher/platform`, data)
    .pipe(map(response => response));
  }

  notifyPublisherForHCPScript(platformId: string) {
    return this.http.get(environment.base_url + `admin/hcp/notifystatus/${platformId}`)
    .pipe(map(response => response));
  }
}