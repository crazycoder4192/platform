import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
export class InvoiceService {

    constructor(
      private http: HttpClient
    ) { }
    processInvoices(invoiceIds: any[]) {
      return this.http.post(environment.base_url + 'admin/invoice/processpending', {invoiceIds : invoiceIds})
      .pipe(map(response => response));
    }
  }