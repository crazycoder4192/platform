import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SchedulePaymentService {

  constructor(private http: HttpClient) { }

  addSchedulePayment(data: any) {
    return this.http.post(environment.base_url + 'admin/schedulepayments', data).pipe(map(response => response));
  }

  getAllSchedulePayments() {
    return this.http.get(environment.base_url + 'admin/schedulepayments')
      .pipe(map(response => response));
  }

  getSchedulePaymentById(_id: any) {
    return this.http.get(environment.base_url + `admin/schedulepayments/${_id}`).pipe(map(response => response));
  }

  deleteSchedulePayment(_id: any) {
    return this.http.delete(environment.base_url + `admin/schedulepayments/${_id}`).pipe(map(response => response));
  }

  updateSchedulePayment(data: any) {
    return this.http.put(environment.base_url + `admin/schedulepayments/${data._id}`, data).pipe(map(response => response));
  }

}
