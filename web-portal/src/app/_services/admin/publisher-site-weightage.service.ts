import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PublisherSiteWeightageService {

  constructor(private http: HttpClient) { }

  addPublisherSiteWeightage(data: any) {
    return this.http.post(environment.base_url + 'admin/publishersiteweightages', data).pipe(map(response => response));
  }

  getAllPublisherSiteWeightages() {
    return this.http.get(environment.base_url + 'admin/publishersiteweightages')
      .pipe(map(response => response));
  }

  getPublisherSiteWeightageById(_id: any) {
    return this.http.get(environment.base_url + `admin/publishersiteweightages/${_id}`).pipe(map(response => response));
  }

  deletePublisherSiteWeightage(_id: any) {
    return this.http.delete(environment.base_url + `admin/publishersiteweightages/${_id}`).pipe(map(response => response));
  }

  updatePublisherSiteWeightage(data: any) {
    return this.http.put(environment.base_url + `admin/publishersiteweightages/${data._id}`, data).pipe(map(response => response));
  }

}
