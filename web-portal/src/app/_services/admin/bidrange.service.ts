import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BidRangeService {

  constructor(private http: HttpClient) { }

  getAllBidRanges() {
    return this.http.get(environment.base_url + 'admin/bidranges')
    .pipe(map(response => response));
  }

  getBidRangeById(_id: any) {
    return this.http.get(environment.base_url + `admin/bidranges/${_id}`).pipe(map(response => response));
  }

  addBidRange(data: any) {
    return this.http.post(environment.base_url + 'admin/bidranges', data).pipe(map(response => response));
  }

  updateBidRange(data: any) {
    return this.http.put(environment.base_url + `admin/bidranges/${data._id}`, data).pipe(map(response => response));
  }

  deleteBidRange(_id: any) {
    return this.http.delete(environment.base_url + `admin/bidranges/${_id}`).pipe(map(response => response));
  }

}
