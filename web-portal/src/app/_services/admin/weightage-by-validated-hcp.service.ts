import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeightageByValidatedHcpService {

  constructor(private http: HttpClient) { }

  addWeightageByValidatedHcp(data: any) {
    return this.http.post(environment.base_url + 'admin/weightagebyvalidatedhcps', data).pipe(map(response => response));
  }

  getAllWeightageByValidatedHcps() {
    return this.http.get(environment.base_url + 'admin/weightagebyvalidatedhcps')
      .pipe(map(response => response));
  }

  getWeightageByValidatedHcpById(_id: any) {
    return this.http.get(environment.base_url + `admin/weightagebyvalidatedhcps/${_id}`).pipe(map(response => response));
  }

  deleteWeightageByValidatedHcp(_id: any) {
    return this.http.delete(environment.base_url + `admin/weightagebyvalidatedhcps/${_id}`).pipe(map(response => response));
  }

  updateWeightageByValidatedHcp(data: any) {
    return this.http.put(environment.base_url + `admin/weightagebyvalidatedhcps/${data._id}`, data).pipe(map(response => response));
  }

}
