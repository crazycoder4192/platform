import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminDashboardService {

  constructor(private http: HttpClient) { }

  getTop5Advertisers() {
    return this.http.get(environment.base_url + 'admin/dashboard/topfiveadvertisers').pipe(map(response => response));
  }

  getTop5Publishers() {
    return this.http.get(environment.base_url + 'admin/dashboard/topfivePublishers').pipe(map(response => response));
  }
  getTopAdvertisers() {
    return this.http.get(environment.base_url + 'admin/dashboard/topadvertisers').pipe(map(response => response));
  }

  getTopPublishers() {
    return this.http.get(environment.base_url + 'admin/dashboard/topPublishers').pipe(map(response => response));
  }

  getTop5Assets() {
    return this.http.get(environment.base_url + 'admin/dashboard/topassets').pipe(map(response => response));
  }

  getTop5Campaigns() {
    return this.http.get(environment.base_url + 'admin/dashboard/topcampaigns').pipe(map(response => response));
  }
}