import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminAdvertiserService {
  constructor(
      private http: HttpClient
    ) { }
  /*getPubl() {
      return this.http.get(environment.base_url + `admin/advertiser/`)
      .pipe(map(response => response));
  } */
  getAdvertiserInvoices(type: string) {
    return this.http.get(environment.base_url + `admin/provider/invoices/${type}`)
    .pipe(map(response => response));
  }
}