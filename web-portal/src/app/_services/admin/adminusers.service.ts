import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminUsersService {

  constructor(private http: HttpClient) { }

  getAdminUsers() {
    return this.http.get(environment.base_url + 'admin/users').pipe(map(response => response));
  }

  addAdminUser(data: any) {
    return this.http.post(environment.base_url + `admin/addUser`, data).pipe(map(response => response));
  }

  getAdminUserById(_id: any) {
    return this.http.get(environment.base_url + `admin/users/${_id}`).pipe(map(response => response));
  }

  getUserByEmail(_id: any) {
    return this.http.get(environment.base_url + `admin/userDetails/${_id}`).pipe(map(response => response));
  }

  deleteUserByEmail(_id: any) {
    return this.http.delete(environment.base_url + `admin/userDetails/${_id}`).pipe(map(response => response));
  }

  updateAdminUser(data: any) {
    return this.http.put(environment.base_url + `admin/users/${data._id}`, data).pipe(map(response => response));
  }

  deleteAdminUser(_id: any) {
    return this.http.delete(environment.base_url + `admin/users/${_id}`).pipe(map(response => response));
  }

}
