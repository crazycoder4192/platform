import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../_models/users';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UserService {

    constructor(private http: HttpClient) { }

    register(user: User) {
        // return this.http.post(environment.base_url + `user/register`, user);
        return this.http.post<any>(environment.base_url + `user/register`, user)
      .pipe(map(userData => {
          return userData;
      }));
    }

    activateUser(_id, tokenType) {
      return this.http.get(environment.base_url + `user/verifytoken?token=${_id}&tokenType=${tokenType}`);
    }

    unsubscribeUser(_id) {
      return this.http.get(environment.base_url + `user/unsubscribeUser?token=${_id}`);
    }

    resetPwd(_id) {
      return this.http.get(environment.base_url + `user/reset`, {params: {id: _id}});
    }

    updatePwd(data) {
      return this.http.post(environment.base_url + `user/updatePassword`, data);
    }

    submittedFeedback(_id, _tokenType, _feedback) {
      return this.http.get(environment.base_url + `user/submittedFeedback?token=${_id}&tokenType=${_tokenType}&feedback=${_feedback}`).pipe(map(response => 
        {return response}));
    }

    updateAddUserPassword(data) {
      return this.http.post(environment.base_url + `user/updateAddUserPassword`, data);
    }

    getUserByEmail(email) {
      const req = { email: email };
      return this.http.post<User>(environment.base_url + `user/email`, req).pipe(map(user => {
        return user;
      }));
    }

    setDashboardVisitedStatusByEmail(email, dashboardVisitedStatus) {
      const req = { email: email, isVisitedDashboard: dashboardVisitedStatus};
      return this.http.post<User>(environment.base_url + `user/email`, req).pipe(map(user => {
        return user;
      }));
    }

    changePwd(data) {
      return this.http.post(environment.base_url + `user/changepassword`, data);
    }

    createQuestions(data) {
      return this.http.post(environment.base_url + `user/savequestions`, data);
    }

    extendToken(emailId, tokenOrg) {
      const req = { email: emailId, token: tokenOrg };
      return this.http.post(environment.base_url + `user/extendToken`, req);
    }

    forgotPwd(data) {
      return this.http.post(environment.base_url + `user/forgotpassword`, data);
    }

    passwordExpiryReset(data) {
      return this.http.post(environment.base_url + `user/password_expiry_reset`, data);
    }
    // update publisher first and last name only
    updateFirstAndLastName(userDetails: User) {
      return this.http.put<any>(environment.base_url + `user/updatefirstlastname`, userDetails)
        .pipe(map(user => {
          return user;
        }));
    }
    // end

    updateUser(userDetails) {
       return this.http.put<any>(environment.base_url + `user/updateUser`, userDetails)
        .pipe(map(user => {
          return user;
        }));
    }
    checkPassword(data) {
      return this.http.post(environment.base_url + `user/checkpassword`, data);
    }
    deleteUser() {
      return this.http.delete(environment.base_url + `user/delete`).pipe(map(user => user));
    }
    notificationSettings(notification: any) {
      return this.http.put(environment.base_url + `user/notificationsettings`, notification).pipe(map(user => user));
    }
    reGenerateToken(data: any) {
      return this.http.post<any>(environment.base_url + `user/regeneratetoken`, data)
      .pipe(map(user => user));
    }

    validateCode(code: string) {
      const input: any = {invitationCode: code};
      return this.http.post<any>(environment.base_url + `user/validateinvitationcode`, input);
    }

    saveRequestingEmail(email: string) {
      const input: any = {interestedUser: email};
      return this.http.post<any>(environment.base_url + `user/addinteresteduser`, input);
    }

}
