import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DialogTemplateComponent } from '../common/dialog-template/dialog-template.component';
import { UserService } from '../_services/user.service';

@Injectable({ providedIn: 'root' })
export class TimeoutService {
  idleTime = 84000;
  timeout;

  constructor(private dialog: MatDialog, private router: Router,
    private userService: UserService) { }

  handleInactivity() {
    return new Observable(observer => {
      const setInactive = () => {
        observer.next(true);
      };

      const resetTimer = () => {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(setInactive, this.idleTime);
      };

      window.onload = resetTimer;
      document.onmousemove = resetTimer;
      document.onmousedown = resetTimer;
      document.onkeydown = resetTimer;
      document.onscroll = resetTimer;
      document.ontouchstart = resetTimer;
      document.ontouchmove = resetTimer;
    });
  }
  clearTime() {
    clearTimeout(this.timeout);
    window.onload = null;
    document.onmousemove = null;
    document.onmousedown = null;
    document.onkeydown = null;
    document.onscroll = null;
    document.ontouchstart = null;
    document.ontouchmove = null;
  }
  setIdleTime(idleTime: number) {
    this.idleTime = idleTime;
  }

  openModal(title: string, message: string, yes: Function = null, no: Function = null) {
    this.dialog.closeAll();
    const dialogConfig = new MatDialogConfig();

    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.data = {
        title: title,
        message: message
    };
    dialogConfig.minWidth = 400;

    const dialogRef = this.dialog.open(DialogTemplateComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.clearTime();
        this.userService.extendToken(
          JSON.parse(localStorage.getItem('currentUser')).email,
          JSON.parse(localStorage.getItem('currentUser')).token
        ).subscribe(observe => {
         // console.log(observe);
        });
        dialogRef.close();
      } else {
        this.clearTime();
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
        dialogRef.close();
      }
    });
  }

  handleAPIInactivity(reset) {
    return new Observable(observer => {
      const setInactive = () => {
        observer.next(true);
      };

      const resetTimer = () => {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(setInactive, this.idleTime);
      };
      if (reset) {
        resetTimer();
      }
    });
  }

}
