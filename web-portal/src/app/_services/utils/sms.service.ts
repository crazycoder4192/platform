import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SMSService {

  constructor(private http: HttpClient) { }

  public verifyPhoneNumber(phnumber: string) {
    return this.http.post<any>(environment.base_url + `sms/verifyphonenumber`, {phNumber: phnumber})
    .pipe(map(sms => sms));
  }

  public verifyotp(otp: string) {
    return this.http.post<any>(environment.base_url + `sms/verifyotp`, {otpValue: otp})
    .pipe(map(sms => sms));
  }
}