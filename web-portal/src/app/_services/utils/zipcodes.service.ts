import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';
import {CityStateDetails} from 'src/app/_models/city_state_details_model';
import { log } from 'util';

@Injectable({
  providedIn: 'root'
})
export class ZipcodesService {

  constructor(private http: HttpClient) { }

  getCities(): Observable<CityStateDetails[]> {

    return this.http.get<CityStateDetails[]>(environment.base_url + `util/cities`);
  }

  getUniqueCities(zone: string): Observable<string[]> {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url + `util/cities/unique`, {headers: customHeader});
  }

  getUniqueStates(zone: string): Observable<string[]> {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url + `util/states/unique`, {headers: customHeader});
  }

  getUniqueCountries(): Observable<string[]> {
    return this.http.get<string[]>(environment.base_url + `util/country/unique`);
  }
  // searching cities by prefix
  getUniqueCitiesOnChangeCity(citystring: string, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url + `util/citiessearch/${citystring}`, {headers: customHeader}).pipe(map(response => response));
  }
  // end
  getUniqueZipCodes(zone: string): Observable<string[]> {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url + `util/zipcodes/unique`, {headers: customHeader});
  }
  // searching cities by prefix
  getUniqueZipCodesOnChangeZipCode(zipCodeString: string, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url + `util/zipcodessearch/${zipCodeString}`, {headers: customHeader}).pipe(map(response => response));
  }
  // end
}
