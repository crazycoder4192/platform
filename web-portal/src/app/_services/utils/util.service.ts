import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { first, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private http: HttpClient) { }

  getUniqueTherapeuticTypes() {
    return this.http.get<any>(environment.base_url + `util/therapeuticTypes/unique`)
      .pipe(map(therapeuticTypes => therapeuticTypes));
  }

  getTherapeuticTypes() {
    return this.http.get<any>(environment.base_url + `util/therapeuticTypes`)
      .pipe(map(therapeuticTypes => therapeuticTypes));
  }

  getBanners() {
    return this.http.get(environment.base_url + `util/banners`).pipe(map(response => response));
  }

  getArchytypes() {
    return this.http.get(environment.base_url + `util/archetypes`)
    .pipe(map(response => response));
  }

  getCityDetailsByCityName(cityName: string, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url +
      `util/citydetailsbycityname/${cityName}`, {headers: customHeader}).pipe(map(response => response));
  }
  getCityDetailsByZipCode(zipCode: string, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<string[]>(environment.base_url +
      `util/citydetailsbyzipcode/${zipCode}`, {headers: customHeader}).pipe(map(response => response));
  }

  getCityDetailsByZipCodes(zipCodes: string[]) {
    const payload: any = {zipCodes: zipCodes};
    return this.http.post<string[]>(environment.base_url + `util/citydetailsbyzipcodes/`, payload).pipe(map(response => response));
  }

  getObjectives() {
    return this.http.get(environment.base_url + `util/objectives`).pipe(map(response => response));
  }

  getAppTypes() {
    return this.http.get<any>(environment.base_url + `publisher/appTypes`)
      .pipe(map(appTypes => {
        return appTypes;
      }));
  }

  getValidHCPLocations() {
    return this.http.get(environment.base_url + `util/validHCPLocations`).pipe(map(response => response));
  }
  getValidHCPSpecialization() {
    return this.http.get(environment.base_url + `util/validHCPSpecialization`).pipe(map(response => response));
  }

  getValidHCPLocationsByTypeAndId(type: string, id: string) {
    return this.http.get(environment.base_url + `util/validHCPLocations/${type}/${id}`).pipe(map(response => response));
  }
  getValidHCPLocationListByTypeAndId(type: string, id: string) {
    return this.http.get(environment.base_url + `util/validHCPLocationList/${type}/${id}`).pipe(map(response => response));
  }
  getValidHCPSpecializationByTypeAndId(type: string, id: string) {
    return this.http.get(environment.base_url + `util/validHCPSpecialization/${type}/${id}`).pipe(map(response => response));
  }
  getValidHCPSpecializationListByTypeAndId(type: string, id: string) {
    return this.http.get(environment.base_url + `util/validHCPSpecializationList/${type}/${id}`).pipe(map(response => response));
  }

  getValidHCPGenderByTypeAndId(type: string, id: string) {
    return this.http.get(environment.base_url + `util/validHCPGender/${type}/${id}`).pipe(map(response => response));
  }

  saveFilter(data: any) {
    return this.http.post(environment.base_url + `util/filter`, data).pipe(map(response => response));
  }

  getFiltersByEmail(email: string) {
    return this.http.get(environment.base_url + `util/filter/email/${email}`).pipe(map(response => response));
  }

  checkFilterNameExists(details: any) {
    return this.http.post(environment.base_url + `util/filter/checkfilternameexists`, details).pipe(map(response => response));
  }

  validateExternalURL(extURL: string) {
    return this.http.post<string>(environment.base_url + `util/urlExists`, {urlToValidate: extURL}).pipe(map(response => response));
  }

  getCurrencySymbol() {
    let symbol: string;
    if (localStorage.getItem('zone') === '1') {
      symbol = '$';
    }
    if (localStorage.getItem('zone') === '2') {
      symbol = '₹';
    }
    return symbol;
  }

}
