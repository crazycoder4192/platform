import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ToolTip } from '../../_models/tooltip_model';

@Injectable({
  providedIn: 'root'
})
export class ToolTipService {

  constructor(private http: HttpClient) { }

  fetchToolTipUniversal(toolTipKeyList) {
    let selectedTooltips = [];
    let toolTipSubject = new Subject<any[]>();
    this.getTooltips().subscribe(
      res => {
        let toolList = res;
        if (toolList.length > 0) {
          toolList.forEach(toolTip => {
            if (toolTipKeyList.has(toolTip.key)) {
              selectedTooltips.push(toolTip);
            }
          });
          toolTipSubject.next(selectedTooltips);
        }
      },
      err => {
        console.log('Error! getToolTips : ' + err);
      });
    return toolTipSubject;
  }

  getTooltips() {
    return this.http.get(environment.base_url + `util/tooltips`).pipe(map(response => {
      const toolList = <ToolTip[]>response;
      return toolList
    }));
  }

}
