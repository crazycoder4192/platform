import { Injectable } from '@angular/core';
import * as amplitude from 'amplitude-js';
import config from 'appconfig.json';

@Injectable({
  providedIn: 'root'
})
export class AmplitudeService {
  amplitudeEnabled = config['AMPLITUDE']['ENABLED'];
  constructor() { }

  setUserProperties(email: string, type: string) {
    if(this.amplitudeEnabled) { 
      amplitude.setUserId(email);
      amplitude.setUserProperties({
        'type': type,
        'email': email
      });
    }
  }

  logEvent(event: string, eventProperties: Object) {
    if(this.amplitudeEnabled) { 
      amplitude.getInstance().logEvent(event, eventProperties);
    }
  }

}