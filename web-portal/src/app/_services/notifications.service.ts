import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private http: HttpClient) { }
  getNotifications() {
    return this.http.get(environment.base_url + `notification/all`).pipe(map(response => response));
  }

  markNotificationAsRead(id: string) {
    return this.http.put(environment.base_url + `notification/markasread/${id}`, {}).pipe(map(response => response));
  }

  removeNotification(id: string) {
    return this.http.delete(environment.base_url + `notification/${id}`).pipe(map(response => response));
  }

  getUnreadNotificationCount() {
    return this.http.get(environment.base_url + `notification/unreadNotificationCount`).pipe(map(response => response));
  }
}
