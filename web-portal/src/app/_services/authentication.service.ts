import { HttpClient } from '@angular/common/http';
import { Injectable, Compiler  } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoginUserDetails } from '../_models/users';
import { TimeoutService } from './timeout.service';


@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<LoginUserDetails>;
    public currentUser: Observable<LoginUserDetails>;

    constructor(private http: HttpClient, private router: Router,
        private timeoutService: TimeoutService, private compiler: Compiler) {
        this.currentUserSubject = new BehaviorSubject<LoginUserDetails>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): LoginUserDetails {
        return this.currentUserSubject.value;
    }

    login(data: LoginUserDetails) {
        return this.http.post<any>(environment.base_url + `user/login`, data)
            .pipe(map(user => {
                // login successful if there's a token in the response
                if (user && user.token) {
                    // store user details and token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                      this.currentUserSubject.next(user);
                  //  this.timeoutService.setIdleTime(environment.idle_timeout);
                  /*  this.timeoutService.handleInactivity()
                        .subscribe(inactive => {
                            this.timeoutService.openModal('title', 'message');
                        });*/
                    this.router.navigate(['/' + user.usertype]);
                }
                return user;
            }));
    }

    isLoggedIn() {
        if (this.currentUserSubject.value  && this.currentUserValue && this.currentUserValue.token) {
            return this.http.get(environment.base_url + `user/isLoggedIn`, { params: { token: this.currentUserValue.token } })
                .pipe(map(tokenStatus => {
                    if (tokenStatus['success'] === false && (tokenStatus['message'] === 'INVALID_TOKEN')) {
                        if (this.currentUserValue.usertype === 'publisher') {
                            this.clearStorage('publisher');
                        } else {
                            this.clearStorage('advertiser');
                        }
                    }
                    return (tokenStatus['success'] && (tokenStatus['message'] === 'VALID_TOKEN') ? true : false);
                }));
        }
    }

    logout(userRole: string) {
        return this.http.get(environment.base_url + `user/logout`, { params: { token: this.currentUserValue.token } })
            .pipe(map(user => {
                this.clearStorage(userRole);
            }));
    }

    clearStorage(userRole: string) {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('visitRoute');
        localStorage.removeItem('currentRoute');
        localStorage.removeItem('zone');
        this.currentUserSubject.next(null);
        this.timeoutService.clearTime();
        // clear all cache and local storage.
        this.compiler.clearCache();
        localStorage.clear();
        
        this.router.navigate(['/login']);
    }
    /*
    clearStorageForPublisher() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.timeoutService.clearTime();
    } */
}
