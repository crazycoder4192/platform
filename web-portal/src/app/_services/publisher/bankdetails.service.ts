import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Bankdetails } from '../../_models/bankdetails.model';

@Injectable({
  providedIn: 'root'
})
export class BankdetailsService {
  constructor(private http: HttpClient) {}

  getAccountDetails() {
    return this.http
      .get<Bankdetails>(environment.base_url + `publisher/bank-details/`)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  getAccountById(_id: any) {
    return this.http
      .get(environment.base_url + `publisher/bank-details/${_id}`)
      .pipe(map(response => response));
  }
  getAccountDetailsByPlatformId(platformId: any) {
    return this.http
      .get(environment.base_url + `publisher/getPlatformBankDetails/${platformId}`)
      .pipe(map(response => response));
  }
  addAccount(bankFormData: Bankdetails) {
    return this.http
      .post<Bankdetails>(
        environment.base_url + `publisher/bank-details/`,
        bankFormData
      )
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  updateAccountById(_id, data) {
    return this.http
      .put(environment.base_url + `publisher/bank-details/${_id}`, data)
      .pipe(map(response => response));
  }

  deleteAccount(_id: any) {
    return this.http
      .delete(environment.base_url + `publisher/bank-details/${_id}`)
      .pipe(map(response => response));
  }

  getBills() { // by publisher
    return this.http.get(environment.base_url + `publisher/bills`).pipe(map(response => response));
  }
  getBill(_id: any) { // by id invdividual
    return this.http.get(environment.base_url + `publisher/bill/` + `${_id}`).pipe(map(response => response));
  }
  updateBill(data: any) {
    return this.http
      .put(environment.base_url + `publisher/bill/${data._id}`, data)
      .pipe(map(response => response));
  }
  getPendingBills() {
    return this.http.get(environment.base_url + `publisher/pendingBills`).pipe(map(response => response));
  }
  downloadBill(_id: any) {
    return this.http.get(environment.base_url + `publisher/downloadBill/` + `${_id}`,
    {responseType: 'blob'}).pipe(map(response => response));
  }
  emailBill(_id: any) {
    return this.http.get(environment.base_url + `publisher/emailBill/` + `${_id}`).pipe(map(response => response));
  }

  getPublisherBankDetails(data: any) {
    return this.http.post(environment.base_url + `admin/getBankDetails`, data).pipe(map(response => response));
  }

}
