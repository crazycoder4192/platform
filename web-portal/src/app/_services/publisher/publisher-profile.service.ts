import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Publisher } from 'src/app/_models/publisher_model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PublisherProfileService {

  constructor(private http: HttpClient) { }

  addProfile(publisherModel: Publisher) {
    return this.http.post<Publisher>(environment.base_url + `publisher/profile`, publisherModel)
      .pipe(map(publisher => {
        return publisher;
      }));
  }

  updateProfile(publisherModel: Publisher) {
    return this.http.put<any>(environment.base_url + `publisher/profile`, publisherModel)
      .pipe(map(publisher => {
        return publisher;
      }));
  }

  getProfileByEmail(email: string) {
    const req = {
      email: email
    };
    return this.http.post<Publisher>(environment.base_url + `publisher/profile/email`, req)
      .pipe(map(publisher => {
        return publisher;
      }));
  }
}
