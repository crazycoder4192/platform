import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AssetService {

  constructor(private http: HttpClient) { }

  getPlatforms() {
    return this.http.get(environment.base_url + `publisher/platform`).pipe(map(response => response));
  }

  getAssets() {
    return this.http.get(environment.base_url + `publisher/asset`).pipe(map(response => response));
  }

  getAssetsByPlatformId(platformId: any) {
    return this.http.get(environment.base_url + `publisher/asset/platform/${platformId}`).pipe(map(response => response));
  }

  getActiveAssetsByPlatformId(platformId: any) {
    return this.http.get(environment.base_url + `publisher/asset/platform/active/${platformId}`).pipe(map(response => response));
  }

  getSelectedAssetDetails(_id: any) {
    return this.http.get(environment.base_url + `publisher/asset`, {params: {id: _id}}).pipe(map(response => response));
  }

  storeAsset(data: any) {
    return this.http.post(environment.base_url + `publisher/asset`, data).pipe(map(response => response));
  }

  updateAsset(data: any) {
    return this.http.put(environment.base_url + `publisher/asset`, data).pipe(map(response => response));
  }

  deleteAsset(_id: string) {
    return this.http.delete(environment.base_url + `publisher/asset/${_id}`)
    .pipe(map(response => response));
  }

  getBannerSizesForExistedAssets() {
    return this.http.get(environment.base_url + `publisher/getBannerSizesForExistedAssets`).pipe(map(response => response));
  }

  checkAssetNameAvailablity(data: any) {
    return this.http.post<any>(environment.base_url + `publisher/asset/checkAssetNameAvailablity`, data)
    .pipe(map(publisherHCP => {
      return publisherHCP;
    }));
  }
  getSuggestedBidRange(platformId: any) {
    return this.http.get(environment.base_url + `publisher/asset/getSuggestedBidRange/${platformId}`).pipe(map(response => response));
  }

  sendInvitationAndAdIntegrationCode(postData: any) {
    return this.http.post<any>(environment.base_url + `publisher/asset/sendInvitationAndAdIntegrationCodeMail`, postData);
  }
  sendAdIntegrationCode(postData: any) {
    return this.http.post<any>(environment.base_url + `publisher/asset/sendAdIntegrationCodeMail`, postData);
  }
}
