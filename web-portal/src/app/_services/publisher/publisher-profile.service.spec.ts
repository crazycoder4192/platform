import { TestBed } from '@angular/core/testing';

import { PublisherProfileService } from './publisher-profile.service';

describe('PublisherProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublisherProfileService = TestBed.get(PublisherProfileService);
    expect(service).toBeTruthy();
  });
});
