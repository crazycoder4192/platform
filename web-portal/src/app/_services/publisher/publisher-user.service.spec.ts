import { TestBed } from '@angular/core/testing';

import { PublisherUserService } from './publisher-user.service';

describe('PublisherUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublisherUserService = TestBed.get(PublisherUserService);
    expect(service).toBeTruthy();
  });
});
