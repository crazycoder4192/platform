import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Publisher } from 'src/app/_models/publisher_model';

@Injectable({
  providedIn: 'root'
})
export class PublisherUserService {

  constructor(
    private http: HttpClient
  ) { }
  addPublisherUser(publisherUSerModel: any, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `publisher/publisheruser`, publisherUSerModel, {headers: customHeader})
      .pipe(map(publisherUser => {
        return publisherUser;
      }));
  }
  getPublisherUsers(publisherId: string) {
    return this.http.get(environment.base_url + `publisher/publisheruserdetails/${publisherId}`)
    .pipe(map(publisherUsers => publisherUsers));
  }
  getPublisherDetails(email: string) {
    return this.http.get<Publisher>(environment.base_url + `publisher/publisherdetails/${email}`)
    .pipe(map(publisher => publisher));
  }
  updatePlatformRoleOfPublisherUser(data: any) {
    return this.http.put(environment.base_url + `publisher/publisheruserplatformrole`, data).pipe(map(publisherUser => publisherUser));
  }
  reGenerateToken(data: any) {
    return this.http.post<any>(environment.base_url + `publisher/regeneratetoken`, data)
    .pipe(map(publisherUser => publisherUser));
  }
  deletePublisherUser(email: string) {
    return this.http.delete(environment.base_url + `publisher/publisheruser/${email}`).pipe(map(publisherUser => publisherUser));
  }
  deletePlatformFromPublisherUser(data: any) {
    return this.http.put(environment.base_url + `publisher/publisheruserplatform`, data).pipe(map(publisherUser => publisherUser));
  }
  getPublisherUserPermissions(email: string) {
    return this.http.get(environment.base_url + `publisher/publisheruserpermissions/${email}`)
    .pipe(map(publisherUserPermissions => publisherUserPermissions));
  }
  sendPublisherSubUserMails(data: any) {
    return this.http.post(environment.base_url + `publisher/publisheruser/sendSubUserMails`, data)
    .pipe(map(response => response));
  }
}
