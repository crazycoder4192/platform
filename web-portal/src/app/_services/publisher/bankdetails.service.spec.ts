import { TestBed } from '@angular/core/testing';

import { BankdetailsService } from './bankdetails.service';

describe('BankdetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BankdetailsService = TestBed.get(BankdetailsService);
    expect(service).toBeTruthy();
  });
});
