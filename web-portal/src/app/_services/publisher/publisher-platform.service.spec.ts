import { TestBed } from '@angular/core/testing';

import { PublisherPlatformService } from './publisher-platform.service';

describe('PublisherPlatformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublisherPlatformService = TestBed.get(PublisherPlatformService);
    expect(service).toBeTruthy();
  });
});
