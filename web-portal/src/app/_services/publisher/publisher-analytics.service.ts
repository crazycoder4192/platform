import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AnalyticsFilter } from 'src/app/_models/analytics_filter.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PublisherAnalyticsService {

  constructor(private http: HttpClient) { }

  getAnalyticsData(analyticsFilter: AnalyticsFilter) {
    return this.http.post<any>(environment.base_url + `publisher/analytics`, analyticsFilter)
      .pipe(map(publisher => publisher));
  }
  getAnalyticsCardsData(analyticsFilter: AnalyticsFilter) {
    return this.http.post<any>(environment.base_url + `publisher/analyticsCards`, analyticsFilter)
    .pipe(map(publisher => publisher));
  }
  getAnalyticsMetaData(platformId: AnalyticsFilter) {
    return this.http.get<any>(environment.base_url + `publisher/analytics/metadata/${platformId}`)
      .pipe(map(metadata => metadata));
  }
}
