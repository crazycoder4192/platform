import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getPlatformDetailsForDashboard() {
    return this.http.get(environment.base_url + `publisher/dashboard`).pipe(map(response => response));
  }
  getViewingPlatformDetailForDashboard(platformId: string) {
    return this.http.get(environment.base_url + `publisher/dashboard/${platformId}`).pipe(map(response => response));
  }
  getPerformananceAnalyticsData(analyticsFilter: any) {
    return this.http.post<any>(environment.base_url + `publisher/dashboardperformanceanalytics`, analyticsFilter)
      .pipe(map(performanceAnalytics => performanceAnalytics));
  }

  getOutStandingAmount() {
    return this.http.get(environment.base_url + `publisher/getOutstandingAmount`).pipe(map(response => response));
  }

}
