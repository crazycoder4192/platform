import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PublisherPlatform } from 'src/app/_models/publisher_platform_model';
import { map, publish } from 'rxjs/operators';
import { PublisherHCPDetails } from 'src/app/_models/publisher-hcp';

@Injectable({
  providedIn: 'root'
})
export class PublisherPlatformService {
  constructor(private http: HttpClient) { }

  addPlatform(platform: PublisherPlatform, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `publisher/platform`, platform, {headers: customHeader})
      .pipe(map(platformRes => {
        return platformRes;
    }));
  }

  addPlatformDetails(platform: FormData, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `publisher/platform`, platform, {headers: customHeader})
      .pipe(map(platformRes => {
        return platformRes;
    }));
  }

  getPlatformsByPublisherId(publisher: string) {
    return this.http.get<any>(environment.base_url + `publisher/platform`, { params: {publisherId: publisher}})
      .pipe(map(platform => {
        return platform;
      }));
  }

  getAllZonesByPublisherId() {
    return this.http.get<any>(environment.base_url + `publisher/allzoneplatform`)
      .pipe(map(platform => {
        return platform;
      }));
  }
  getPlatformsByPlatformTypeAndPublisherId(platformType: string) {
    return this.http.get<any>(environment.base_url + `publisher/websiteandmobileappsplatforms`, { params: {platformType: platformType}})
      .pipe(map(platform => {
        return platform;
      }));
  }

  getUniquePublisherAppTypes() {
    return this.http.get<any>(environment.base_url + `publisher/unique/appTypes`)
      .pipe(map(appTypes => {
        return appTypes;
      }));
  }

  getPlatformHeaderScript() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getHJS`);
  }

  getDocereeLoginSnippetScript() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getDocereeLoginSnippet`);
  }

  getDocereeLogoutSnippetScript() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getDocereeLogoutSnippet`);
  }

  getMobilePISnippet0() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getMobilePISnippet0`);
  }

  getMobilePISnippet1() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getMobilePISnippet1`);
  }

  getMobilePISnippet2() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getMobilePISnippet2`);
  }

  getMobilePISnippet3() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getMobilePISnippet3`);
  }

  getMobileAISnippet1() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getMobileAISnippet1`);
  }

  getMobileAISnippet2() {
    return this.http.get<any>(environment.base_url + `publisher/platform/getMobileAISnippet2`);
  }

  getPublisherAppTypes() {
    return this.http.get<any>(environment.base_url + `publisher/appTypes`)
      .pipe(map(appTypes => {
        return appTypes;
      }));
  }

  getUniquePublisherSiteTypes() {
    return this.http.get<any>(environment.base_url + `publisher/unique/siteTypes`)
      .pipe(map(siteTypes => {
        return siteTypes;
      }));
  }

  getPublisherSiteTypes() {
    return this.http.get<any>(environment.base_url + `publisher/siteTypes`)
      .pipe(map(siteTypes => {
        return siteTypes;
      }));
  }

  createPublisherHCP(publisherHCPDetails: any) {
    return this.http.post<any>(environment.base_url + `publisher/hcp-details`, publisherHCPDetails)
    .pipe(map(publisherHCP => {
      return publisherHCP;
  }));
  }

  getPlatformById(_id: any) {
    return this.http
      .get(environment.base_url + `publisher/platform/${_id}`)
      .pipe(map(response => response));
  }

  getPlatformByIdZone(_id: any, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http
      .get(environment.base_url + `publisher/platform/${_id}`, {headers: customHeader})
      .pipe(map(response => response));
  }

  updatePlatform(platform: any, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.put<any>(environment.base_url + `publisher/platform`, platform, {headers: customHeader})
      .pipe(map(platformRes => {
        return platformRes;
    }));
  }
  validatPublisherHCP() {
    return this.http.get<any>(environment.base_url + `publisher/pulisherhcpvalidation`)
      .pipe(map(platformRes => {
        return platformRes;
    }));
  }
  getPublisherHCPMaster() {
    return this.http.get<any>(environment.base_url + `publisher/pulisherhcpmaster`)
      .pipe(map(publisherMasterData => {
        return publisherMasterData;
    }));
  }
  getViewingPlatform(publisherId: string) {
    return this.http.get<any>(environment.base_url + `publisher/platform/viewing/${publisherId}`)
      .pipe(map(publisherMasterData => {
        return publisherMasterData;
    }));
  }

  getPublisherHCPFields() {
    return this.http.get<any>(environment.base_url + `publisher/docereehcpfields`)
      .pipe(map(publisherHCPFieldsData => {
        return publisherHCPFieldsData;
    }));
  }

  switchToPlatform(id: any) {
    //return this.http.put<any>(environment.base_url + `publisher/switchtoplatform/${id}`, {})
    return this.http.put<any>(environment.base_url + `publisher/publisheruserswitchplatform/${id}`, {})
    .pipe(map(publisherHCP => {
      return publisherHCP;
    }));
  }
  checkPlatformNameAvailablity(data: any) {
    return this.http.post<any>(environment.base_url + `publisher/platform/checkPlatformNameAvailablity`, data)
    .pipe(map(publisherHCP => {
      return publisherHCP;
    }));
  }
  checkPlatformURLAvailablity(data: any) {
    return this.http.post<any>(environment.base_url + `publisher/platform/checkPlatformURLAvailablity`, data)
    .pipe(map(publisherHCP => {
      return publisherHCP;
    }));
  }
  checkHCPUploadDataStatusByPlatformId(platformId: string) {
    return this.http.get<any>(environment.base_url + `publisher/platform/checkHCPStatus/${platformId}`)
    .pipe(map(publisherHCP => {
      return publisherHCP;
    }));
  }

  sendInvitationAndHeaderCode(postData: any, zone: string, platformType: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `publisher/platform/sendInvitationAndHeaderCodeMail`,
    postData, {headers: customHeader, params : {platformType : platformType}});
  }
  sendHeaderCode(postData: any, platformType: string) {
    return this.http.post<any>(environment.base_url + `publisher/platform/sendHeaderCodeMail`, postData, 
    { params : {platformType : platformType}});
  }
  sendInvitationAndInvocationCode(postData: any, platformType: string) {
    return this.http.post<any>(environment.base_url + `publisher/platform/sendInvitationAndInvocationCodeMail`, postData,
    { params : {platformType : platformType}});
  }
  sendInvocationCode(postData: any, zone: string, platformType: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `publisher/platform/sendInvocationCodeMail`, postData, 
    {headers: customHeader, params : {platformType : platformType}}
    );
  }
}
