import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InvitationCodeService {

  constructor(private http: HttpClient) { }

  getInvitationCodeDetails() {
    return this.http.get(environment.base_url + `user/my/invites`).pipe(map(response => response));
  }

  sendInvitation(invitation) {
    return this.http.post(environment.base_url + `user/send/invitation`, invitation).pipe(map(response => response));
  }
}