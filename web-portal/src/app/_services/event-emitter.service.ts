import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  invokeSidebarComponentFunction = new EventEmitter();
  // invokeAccountComponentFunction = new EventEmitter();
  invokeTopBarIconFunction = new EventEmitter();
  invokeAccountSetting = new EventEmitter();
  invokeNotificationBadgeFunction = new EventEmitter();

  constructor() { }
  onSavedAdvertiserProfile() {
    this.invokeSidebarComponentFunction.emit();
  }
  // onChangeBrandFromBrandPanel() {
  //   this.invokeAccountComponentFunction.emit();
  // }
  onClickNavigation() {
    this.invokeTopBarIconFunction.emit();
  }
  onClickAccountSetting() {
    this.invokeAccountSetting.emit();
  }

  onReadNotification() {
    this.invokeNotificationBadgeFunction.emit();
  }
  onSavedPublisherProfile() {
    this.invokeSidebarComponentFunction.emit();
  }
}
