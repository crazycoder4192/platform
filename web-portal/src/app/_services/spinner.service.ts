import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  public showSpinner: BehaviorSubject<Boolean>;
  constructor() {
    this.showSpinner = new BehaviorSubject<Boolean>(false);
   }

   public get spinnerEnabled(): Boolean {
    return this.showSpinner.value;
  }
}
