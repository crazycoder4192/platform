import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientBrandMetadataService {

  constructor(private http: HttpClient) { }
  getClientsMetadata(zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<any>(environment.base_url + `provider/clientbrand/metadata/allclients`, {headers: customHeader})
      .pipe(map(clients => clients));
  }
  getBrandsMetadata(clientId, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.get<any>(environment.base_url + `provider/clientbrand/metadata/allbrands/${clientId}`, {headers: customHeader})
      .pipe(map(brands => brands));
  }
}
