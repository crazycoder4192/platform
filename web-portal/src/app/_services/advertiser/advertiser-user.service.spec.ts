import { TestBed } from '@angular/core/testing';

import { AdvertiserUserService } from './advertiser-user.service';

describe('AdvertiserUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserUserService = TestBed.get(AdvertiserUserService);
    expect(service).toBeTruthy();
  });
});
