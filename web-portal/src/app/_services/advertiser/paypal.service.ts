import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PaypalService {

  constructor(private http: HttpClient) { }

  payNow() {
    const headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET, POST');
    headers.append('Content-Type', 'application/json');

    const options = { headers: headers };
    return this.http.post(environment.base_url + `provider/payNow/`, {}, options).pipe(map(response => response));
  }

  paymentSuccess(_id: any, paymentId: any, PayerID: any) {
    return this.http.get(environment.base_url + `provider/paymentSuccess/` + `${_id}/` + `${paymentId}/` + `${PayerID}`)
    .pipe(map(response => response));
  }

  paymentFailure(_id: any) {
    return this.http.get(environment.base_url + `provider/paymentFailure/` + `${_id}`).pipe(map(response => response));
  }
}
