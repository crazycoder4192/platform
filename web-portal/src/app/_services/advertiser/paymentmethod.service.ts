import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { PaymentMethod } from '../../_models/advertiser/payment_method_model';

@Injectable({
  providedIn: 'root'
})
export class PaymentmethodService {

  constructor(private http: HttpClient) { }

  public url = environment.base_url + `provider/paymentmethod/`;

  getPaymentMethods() {
    return this.http.get<PaymentMethod[]>(this.url).pipe(map(result => result));
  }

  getPaymentMethodById(_id: any) {
    return this.http.get<PaymentMethod>(this.url + `${_id}`).pipe(map(response => response));
  }

  addPaymentMethod(bankFormData: PaymentMethod) {
    return this.http.post<PaymentMethod>(this.url, bankFormData).pipe(map(result => result));
  }

  updatePaymentMethodById(_id, data) {
    return this.http.put(this.url + `${_id}`, data).pipe(map(response => response));
  }

  deletePaymentMethod(_id: any) {
    return this.http.delete(this.url + `${_id}`).pipe(map(response => response));
  }

  generateInvoice() {
    return this.http.get(environment.base_url + `provider/generateInvoices/`).pipe(map(response => response));
  }

  getInvoices() {
    return this.http.get(environment.base_url + `provider/invoice`).pipe(map(response => response));
  }

  downloadInvoice(_id: any) {
    return this.http.get(environment.base_url + `provider/downloadInvoice/` + `${_id}`,
    {responseType: 'blob'}).pipe(map(response => response));
  }
  emailInvoice(_id: any) {
    return this.http.get(environment.base_url + `provider/emailInvoice/` + `${_id}`).pipe(map(response => response));
  }
  payNow() {
    const headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET, POST');
    headers.append('Content-Type', 'application/json');

    const options = { headers: headers };
    return this.http.post(environment.base_url + `provider/payNow/`, {}, options).pipe(map(response => response));
  }
}
