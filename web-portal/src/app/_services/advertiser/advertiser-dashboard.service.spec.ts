import { TestBed } from '@angular/core/testing';

import { AdvertiserDashboardService } from './advertiser-dashboard.service';

describe('AdvertiserDashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserDashboardService = TestBed.get(AdvertiserDashboardService);
    expect(service).toBeTruthy();
  });
});
