import { TestBed } from '@angular/core/testing';

import { AdvertiserBrandService } from './advertiser-brand.service';

describe('AdvertiserBrandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserBrandService = TestBed.get(AdvertiserBrandService);
    expect(service).toBeTruthy();
  });
});
