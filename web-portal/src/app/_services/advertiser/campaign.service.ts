import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  
  constructor(private http: HttpClient) { }

  getAllCampaigns() {
    return this.http.get(environment.base_url + `provider/campaigns`).pipe(map(response => response));
  }

  storeCampaign(data: any) {
    return this.http.post(environment.base_url + `provider/campaigns`, data).pipe(map(response => response));
  }

  updateCampaign(_id, data) {
    return this.http.put(environment.base_url + `provider/campaigns/${_id}`, data).pipe(map(response => response));
  }

  deleteCampaign(_id: any, type) {
    return this.http.delete(environment.base_url + `provider/campaigns/${_id}/${type}`).pipe(map(response => response));
  }

  deleteSubCampaign(_id: any) {
    return this.http.delete(environment.base_url + `provider/subcampaigns/${_id}`).pipe(map(response => response));
  }

  getAllSubCampaignsForSpecificCampaign(_id: any) {
    return this.http.get(environment.base_url + `provider/subcampaignsbycampaignid/${_id}`).pipe(map(response => response));
  }

  getOneSubCampaign(_id: any) {
    return this.http.get(environment.base_url + `provider/subcampaign/${_id}`).pipe(map(response => response));
  }

  storeSubCampaign(data: any) {
    return this.http.post(environment.base_url + `provider/subcampaigns`, data).pipe(map(response => response));
  }

  updateSubCampaign(_id, data: any) {
    return this.http.put(environment.base_url + `provider/subcampaigns`, data).pipe(map(response => response));
  }

  getDraftCampaigns() {
    return this.http.get(environment.base_url + `provider/drafts`).pipe(map(response => response));
  }
  getPendingCampaigns() {
    return this.http.get(environment.base_url + `provider/pendingsubcampaign`).pipe(map(response => response));
  }
  getManageCampaings() {
    return this.http.get(environment.base_url + `provider/manageCampaigns`).pipe(map(response => response));
  }
  approveCampaign(campaignDetails: any) {
    return this.http.put(environment.base_url + `provider/changestatusSubCampaign`, campaignDetails).pipe(map(response => response));
  }
  // list admins tab advetiser campaigns
  getProviders() {
    return this.http.get(environment.base_url + `provider/providerList`).pipe(map(response => response));
  }
  getCampaignByUserEmailId(email: string) {
    return this.http.get(environment.base_url + `provider/campaign/useremail/${email}`).pipe(map(response => response));
  }

  getPotentialReach(data: any) {
    return this.http.post(environment.base_url + `provider/subcampaigns/getPotentialReach`,
    { campaignDetails: data }).pipe(map(response => response));
  }

  getPotentialBanners(data: any) {
    return this.http.put(environment.base_url + `provider/subcampaigns/potentialreach/bannersize/bidrange`, data)
    .pipe(map(response => response));
  }

  getCampaignByBrandId(brandId: any) {
    return this.http.get(environment.base_url + `provider/campaigns/by/brand/${brandId}`).pipe(map(response => response));
  }

  getSubcampaignByBrandId(brandId: any) {
    return this.http.get(environment.base_url + `provider/subcampaigns/brand/${brandId}`).pipe(map(response => response));
  }

  campaignNameAvailability(campaignName: string, brandId: string, advertiserId: string) {
    return this.http.post<any>(environment.base_url + `provider/campaigns/campaignNameAvailability`,
                              { name: campaignName, brandId: brandId, advId: advertiserId })
      .pipe(map(brand => brand));
  }

  subcampaignNameAvailability(subcampaignName: string, campId: string, advertiserId: string) {
    return this.http.post<any>(environment.base_url + `provider/subcampaign/subCampaignNameAvailability`,
                              { name: subcampaignName, campaignId: campId, advId: advertiserId })
      .pipe(map(brand => brand));
  }

  getCreativesUrl(id: any) {
    return this.http.get(environment.base_url + `provider/subcampaign/creative/urls/${id}`).pipe(map(response => response));
  }
}
