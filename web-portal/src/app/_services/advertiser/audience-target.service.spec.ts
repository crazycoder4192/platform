import { TestBed } from '@angular/core/testing';

import { AudienceTargetService } from './audience-target.service';

describe('AudienceTargetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AudienceTargetService = TestBed.get(AudienceTargetService);
    expect(service).toBeTruthy();
  });
});
