import { TestBed } from '@angular/core/testing';

import { AdvertiserUtilService } from './advertiser-util.service';

describe('AdvertiserUtilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserUtilService = TestBed.get(AdvertiserUtilService);
    expect(service).toBeTruthy();
  });
});
