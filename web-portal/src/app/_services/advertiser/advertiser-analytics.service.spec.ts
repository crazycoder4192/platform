import { TestBed } from '@angular/core/testing';

import { AdvertiserAnalyticsService } from './advertiser-analytics.service';

describe('AdvertiserAnalyticsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserAnalyticsService = TestBed.get(AdvertiserAnalyticsService);
    expect(service).toBeTruthy();
  });
});
