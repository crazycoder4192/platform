import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserDashboardService {

  constructor(private http: HttpClient) { }

  getActiveCampaignLifeTimeData(id?: any) {
    if (id === undefined) {
      id = 'All';
    }
    return this.http.get(environment.base_url + `provider/activeCampaignLifeTimeData/` + `${id}`)
      .pipe(map(data => data));
  }

  getClientWiseBrandsLifeTimeData() {
    return this.http.get(environment.base_url + `provider/dashboardLifeTimeData`)
      .pipe(map(data => data));
  }

  getLastCheckInData(id?: any) {
    if (id === undefined) {
      id = 'All';
    }
    return this.http.get(environment.base_url + `provider/lastCheckInData/` + `${id}`)
      .pipe(map(data => data));
  }

  getPerformananceAnalyticsData(analyticsFilter: any) {
    return this.http.post<any>(environment.base_url + `provider/dashboardperformanceanalytics`, analyticsFilter)
      .pipe(map(performanceAnalytics => performanceAnalytics));
  }

  getOutStandingAmount() {
    return this.http.get(environment.base_url + `provider/getOutstandingAmount`).pipe(map(response => response));
  }

  getOutStandingBrandAmount(brandId: string) {
    return this.http.get(environment.base_url + `provider/getOutstandingAmount/` + brandId).pipe(map(response => response));
  }

}
