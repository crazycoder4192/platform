import { TestBed } from '@angular/core/testing';

import { AdvertiserProfileService } from './advertiser-profile.service';

describe('AdvertiserProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserProfileService = TestBed.get(AdvertiserProfileService);
    expect(service).toBeTruthy();
  });
});
