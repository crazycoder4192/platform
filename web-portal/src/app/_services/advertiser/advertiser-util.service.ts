import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AdvertiserUtilService {

  constructor(private http: HttpClient) { }

  getUniqueAdvertiserAppTypes() {
    return this.http.get<any>(environment.base_url + `provider/providerTypes`);
  }

  getAdvertiserAppTypes() {
    return this.http.get<any>(environment.base_url + `provider/unique/providerTypes`);
  }

  getUniqueAdvertiserAgencyTypes() {
    return this.http.get<any>(environment.base_url + `provider/unique/providerAgencyTypes`);
  }
}
