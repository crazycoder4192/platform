import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AudienceTargetService {

  constructor(private http: HttpClient) { }

  getAllSavedAudience(brandId) {
    return this.http.get(environment.base_url + `provider/audience/target/list/${brandId}`)
    .pipe(map(response => response));
  }

  getSavedAudienceDetails(audienceTargetId) {
    return this.http.get(environment.base_url + `provider/audience/target/detail/${audienceTargetId}`)
    .pipe(map(response => response));
  }

  getUploadedPotentialReach(audienceTargetId: any) {
    return this.http.get(environment.base_url + `provider/audience/target/potentialreach/${audienceTargetId}`)
    .pipe(map(response => response));
  }

  getPotentialReach(data: any) {
    return this.http.put(environment.base_url + `provider/audience/target/potentialreach/forcreate`, data)
    .pipe(map(response => response));
  }

  createOrUploadHVA(data: any) {
    return this.http.post(environment.base_url + `provider/audience/target`, data).pipe(map(response => response));
  }

  updateOrUploadHVA(data: any) {
    return this.http.put(environment.base_url + `provider/audience/target`, data).pipe(map(response => response));
  }

  deleteHVA(hvaId: any) {
    return this.http.delete(environment.base_url + `provider/audience/target/${hvaId}`).pipe(map(response => response));
  }
}
