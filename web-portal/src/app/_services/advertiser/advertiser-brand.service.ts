import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserBrandService {

  constructor(private http: HttpClient) { }

  saveAdvertiserBrand(advertiserModel: Brand, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `provider/brand`, advertiserModel, {headers: customHeader})
      .pipe(map(advertiser => advertiser));
  }

  updateAdvertiserBrand(advertiserModel: Brand) {
    return this.http.put<any>(environment.base_url + `provider/brand`, advertiserModel)
      .pipe(map(advertiser => advertiser));
  }

  getAllBrands() {
    return this.http.get<any>(environment.base_url + `provider/brand`)
      .pipe(map(advertiser => advertiser));
  }

  getAllZonesByAdvertiserId() {
    return this.http.get<any>(environment.base_url + `provider/allzonebrand`)
      .pipe(map(platform => {
        return platform;
      }));
  }

  getAllBrandsForCards(brandId?: any) {
    return this.http.get<any>(environment.base_url + `provider/brandforCards`, { params: { id: brandId } })
      .pipe(map(advertiser => advertiser));
  }

  getAllClientBrands() {
    return this.http.get<any>(environment.base_url + `provider/clientbrands`)
      .pipe(map(advertiser => advertiser));
  }
  getClientWisebrandsDetails() {
    return this.http.get<any>(environment.base_url + `provider/brandDetailsForLoggedInUser`)
      .pipe(map(advertiser => advertiser));
  }

  getAdvertiserBrandByAdvertiserId(advertiserId: string) {
    return this.http.get<any>(environment.base_url + `provider/brand`, { params: { id: advertiserId } })
      .pipe(map(advertiser => advertiser));
  }

  deactivateAdvertiserBrand(_id: any) {
    return this.http.delete(environment.base_url + `provider/brand/${_id}`).pipe(map(advertiserBrand => advertiserBrand));
  }

  getBrandById(brandId: string) {
    return this.http.get<any>(environment.base_url + `provider/brand/${brandId}`)
      .pipe(map(brand => brand));
  }

  brandNameAvailability(brandName: string, advertiserId: string) {
    return this.http.post<any>(environment.base_url + `provider/brand/brandNameAvailability`, { name: brandName, advId: advertiserId })
      .pipe(map(brand => brand));
  }

}
