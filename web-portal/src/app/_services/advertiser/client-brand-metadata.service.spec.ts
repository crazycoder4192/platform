import { TestBed } from '@angular/core/testing';

import { ClientBrandMetadataService } from './client-brand-metadata.service';

describe('ClientBrandMetadataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientBrandMetadataService = TestBed.get(ClientBrandMetadataService);
    expect(service).toBeTruthy();
  });
});
