import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Advertiser } from 'src/app/_models/advertiser_model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserProfileService {

  constructor(private http: HttpClient) { }

  saveAdvertiser(advertiserModel: Advertiser) {
    return this.http.post<any>(environment.base_url + `provider/profile`, advertiserModel)
      .pipe(map(advertiser => advertiser));
  }

  updateAdvertiser(advertiserModel: Advertiser) {
    return this.http.put<any>(environment.base_url + `provider/profile`, advertiserModel)
      .pipe(map(advertiser => advertiser));
  }
  getAdvertiserByEmail(email: string) {
    return this.http.get<any>(environment.base_url + `provider/profile/email`, { params: {email: email}})
      .pipe(map(advertiser => advertiser));
  }

}
