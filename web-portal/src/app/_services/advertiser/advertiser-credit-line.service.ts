import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserCreditLineService {

  constructor(private http: HttpClient) {
  }
  saveAdvertiserCreditLine(creditLine: any) {
    return this.http.post<any>(environment.base_url + `provider/creditline`, creditLine)
      .pipe(map(creditlineRes => creditlineRes));
  }
  getAdvertiserCreditLine() {
    return this.http.get<any>(environment.base_url + `provider/creditline`)
      .pipe(map(creditlineRes => creditlineRes));
  }
  updateAdvertiserCreditLine(creditLine: any) {
    return this.http.put<any>(environment.base_url + `provider/creditline`, creditLine)
      .pipe(map(creditlineRes => creditlineRes));
  }
  getCreditLinesByStatus(status) {
    return this.http.get<any>(environment.base_url + `provider/creditlines/${status}`)
      .pipe(map(creditlineRes => creditlineRes));
  }
  approveCreditLine(creditLine: any) {
    return this.http.put<any>(environment.base_url + `provider/approveCreditline`, creditLine)
    .pipe(map(creditline => creditline));
  }
  disproveCreditLine(creditLine: any) {
    return this.http.put<any>(environment.base_url + `provider/disproveCreditLine`, creditLine)
    .pipe(map(creditline => creditline));
  }
}
