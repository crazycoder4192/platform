import { TestBed } from '@angular/core/testing';

import { CreativeHubService } from './creative-hub.service';

describe('CreativeHubService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreativeHubService = TestBed.get(CreativeHubService);
    expect(service).toBeTruthy();
  });
});
