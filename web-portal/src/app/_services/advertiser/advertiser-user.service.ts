import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserUserService {

  constructor(
    private http: HttpClient
  ) { }
  addAdvertiserUser(data: any, zone: string) {
    const header = new HttpHeaders();
    const customHeader = header.append('overwriteLocalStorageZone', zone);
    return this.http.post<any>(environment.base_url + `provider/advertiseruser`, data, {headers: customHeader})
      .pipe(map(creativeHub => creativeHub));
  }
  getAdvertiserUsers() {
    return this.http.get<any>(environment.base_url + `provider/advertiseruser`)
      .pipe(map(advertiserUsers => advertiserUsers));
  }
  getUserDetaisVsBrandsByLoggedInUser() {
    return this.http.get<any>(environment.base_url + `provider/userDetailsVsBrandsByLoggedInUser`)
      .pipe(map(advertiserUsers => advertiserUsers));
  }
  deleteAdvertiserUser(email: string) {
    return this.http.delete(environment.base_url + `provider/advertiseruser/${email}`).pipe(map(advertiserUser => advertiserUser));
  }
  deleteBrandFromAdvertiserUser(data: any) {
    return this.http.put(environment.base_url + `provider/advertiseruserbrand`, data).pipe(map(advertiserUser => advertiserUser));
  }
  updateBrandRoleOfAdvertiserUser(data: any) {
    return this.http.put(environment.base_url + `provider/advertiseruserbrandrole`, data).pipe(map(advertiserUser => advertiserUser));
  }
  reGenerateToken(data: any) {
    return this.http.post<any>(environment.base_url + `provider/regeneratetoken`, data)
    .pipe(map(advertiserUser => advertiserUser));
  }
  getAdvertiserUserPermissions(email: string) {
    return this.http.get(environment.base_url + `provider/advertiseruserpermissions/${email}`)
    .pipe(map(advertiserUserPermissions => advertiserUserPermissions));
  }
  getAdvertiserUser(email: string) {
    return this.http.get(environment.base_url + `provider/advertiseruser/${email}`)
    .pipe(map(advertiserUser => advertiserUser));
  }
  switchToBrandOfLoggedinUser(id: any) {
    return this.http.put(environment.base_url + `provider/advertiseruserswitchbrand`,
    {brandId: id}).pipe(map(advertiserUser => advertiserUser));
  }
  getAdvertiserUserBrands() {
    return this.http.get(environment.base_url + `provider/advertiseruserbrands`).pipe(map(advertiserUserBrands => advertiserUserBrands));
  }
  getAdvertiserDetails(email: string) {
    return this.http.get(environment.base_url + `provider/advertiseruserdetails/${email}`)
    .pipe(map(advertiserUser => advertiserUser));
  }
}
