import { TestBed } from '@angular/core/testing';

import { AdvertiserCreditLineService } from './advertiser-credit-line.service';

describe('AdvertiserCreditLineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiserCreditLineService = TestBed.get(AdvertiserCreditLineService);
    expect(service).toBeTruthy();
  });
});
