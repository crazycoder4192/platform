import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AnalyticsFilter } from 'src/app/_models/analytics_filter.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdvertiserAnalyticsService {

  constructor(private http: HttpClient) { }

  getAnalyticsData(analyticsFilter: AnalyticsFilter) {
    return this.http.post<any>(environment.base_url + `provider/analytics`, analyticsFilter)
      .pipe(map(advertiser => advertiser));
  }
  getAnalyticsCardsData(analyticsFilter: AnalyticsFilter) {
    return this.http.post<any>(environment.base_url + `provider/analyticsCardsData`, analyticsFilter)
      .pipe(map(advertiser => advertiser));
  }
}
