import { response } from 'express';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreativeHubService {
  constructor(
    private http: HttpClient
  ) { }
  addCreativeHub(data: any) {
    return this.http.post<any>(environment.base_url + `provider/creatives`, data)
      .pipe(map(creativeHub => creativeHub));
  }
  getAllCreativeHubs(type: string) {
    return this.http.get<any>(environment.base_url + `provider/creatives`, { params: { creativeType: type } })
      .pipe(map(creativeHubs => creativeHubs));
  }
  updateCreativeHub(_id, data: any){
    console.log('sending request');
    return this.http.put(environment.base_url + `provider/creatives/${_id}`, data).pipe(map(updateResponse => updateResponse));
  }
  deleteCreativeHub(data: any) {
    return this.http.put(environment.base_url + `provider/deletemanycreatives`, data)
    .pipe(map(deleteCreatvicResponse => deleteCreatvicResponse));
  }
  addCreativeHubsToSection(data: any) {
    return this.http.put(environment.base_url + `provider/addtosection`, data)
    .pipe(map(addCreatvicResponse => addCreatvicResponse));
  }
  getAllCreativeHubsByBannerSize(size: string) {
    return this.http.get<any>(environment.base_url + `provider/creativesbybannersize`, { params: { bannerSize: size } })
      .pipe(map(creativeHubs => creativeHubs));
  }
  getAllVideoCreativeHubsByAdvertiser() {
    return this.http.get<any>(environment.base_url + `provider/creativesvideosbyuser`)
      .pipe(map(creativeHubs => creativeHubs));
  }
  getCreativesUrl(creativeHubId: any) {
    return this.http.get<any>(environment.base_url + `provider/creatives/urls/${creativeHubId}`)
      .pipe(map(urls => urls));
  }

  downloadAllBanners(name: any) {
    return this.http.get(environment.base_url + `provider/creatives/download/` + `${name}`,
    {responseType: 'blob'}).pipe(map(response => response));
  }
}
