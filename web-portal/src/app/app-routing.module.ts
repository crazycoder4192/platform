import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/auth-guard.guard';

const routes: Routes = [
  { path: '',    redirectTo: 'login',    pathMatch: 'full' },
  { path: '', loadChildren: './registration/user-reg-activate-login-resetpass.module#UserRegActLoginResetPassModule'},
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canLoad: [AuthGuard]},
  { path: 'advertiser', loadChildren: './advertiser/advertiser.module#AdvertiserModule', canLoad: [AuthGuard]},
  { path: 'publisher', loadChildren: './publisher/publisher.module#PublisherModule', canLoad: [AuthGuard]},
  { path: '**', redirectTo: '/login'}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
