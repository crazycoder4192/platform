import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from './_services/authentication.service';
import { SpinnerService } from './_services/spinner.service';
import { TimeoutService } from './_services/timeout.service';
import * as amplitude from 'amplitude-js';
import config from 'appconfig.json';

const APP_TITLE = 'Doceree - ';
const SEPARATOR = ' > ';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  isAdvertiser: boolean;
  isPublisher: boolean;
  amplitudeEnabled = config['AMPLITUDE']['ENABLED'];
  public constructor(private titleService: Title, private router: Router,
    private activatedRoute: ActivatedRoute,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private authService: AuthenticationService,
    private timeoutService: TimeoutService, public spinner: SpinnerService) {
    this.spinner.showSpinner.next(true);
    const userDetails = this.authService.currentUserValue;
    if (userDetails && userDetails.token) {
      this.authService.isLoggedIn()
        .pipe()
        .subscribe(
          data => {
            this.spinner.showSpinner.next(false);
            // if (data === true) {
            //   this.timeoutService.setIdleTime(environment.idle_timeout);
            //   this.timeoutService.handleInactivity()
            //     .subscribe(inactive => {
            //       this.timeoutService.openModal('title', 'message');
            //     });
            // }
            console.log('AppComponent isLoggedIn : ' + data);
          },
          error => {
            this.spinner.showSpinner.next(false);
            if (userDetails.usertype === 'publisher') {
              this.authService.clearStorage('publisher');
            } else {
              this.authService.clearStorage('advertiser');
            }
            console.log('AppComponent isLoggedIn : ' + error);
          });
    } else {
      this.spinner.showSpinner.next(false);
    }
  }
  title = 'Doceree Application';
  static ucFirst(string) {
    if (!string) { return string; }
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  ngOnInit(): void {
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd))
      .pipe(map(() => {
        let route = this.activatedRoute;
        while (route.firstChild) { route = route.firstChild; }
        return route;
      }))
      .pipe(filter((route) => route.outlet === 'primary'))
      .pipe(mergeMap((route) => route.data))
      .pipe(map((data) => {
        if (data.title) {
          // If a route has a title set (e.g. data: {title: "Foo"}) then we use it
          return data.title;
        } else {
          // If not, we do a some work on the url to create an approximation
          return this.router.url.split('/').reduce((acc, frag) => {
            if (acc && frag) { acc += SEPARATOR; }
            return acc + AppComponent.ucFirst(frag);
          });
        }
      }))
      .subscribe((pathString) => this.setTitle(`${APP_TITLE}`, `${pathString}`));
      if(this.amplitudeEnabled) {
        let amplitudeKey = config['AMPLITUDE']['KEY'];
        amplitude.getInstance().init(amplitudeKey, null, {
          saveEvents: true,
          includeReferrer: true,
          deviceIdFromUrlParam: true,
          includeUtm: true});
        amplitude.setVersionName('v1.0');
      }
  }
  public setTitle(appName: string, newTitle: string) {
    this.setUserSpinnerColor();
    this.titleService.setTitle(appName + newTitle);
    newTitle = (newTitle.indexOf('Admin') !== - 1) ? 'advertiser-favicon' :
      ((newTitle.indexOf('Publisher') !== - 1) ? 'publisher-favicon' :
        ((newTitle.indexOf('Advertiser') !== -1) ? 'advertiser-favicon' : 'advertiser-favicon'));
    this._document.getElementById('appFavIcon').setAttribute('href', 'assets/' + newTitle + '.png');
  }
  public setUserSpinnerColor() {
    let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUserData) {
      let userType = currentUserData['usertype'];
      if( userType === 'advertiser') {
        this.isAdvertiser = true;
        this.isPublisher = false;
      } else if (userType === 'publisher') {
        this.isAdvertiser = false;
        this.isPublisher = true;
      }
    }
  }

}
