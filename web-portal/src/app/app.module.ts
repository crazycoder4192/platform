import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppInterceptorService } from './app-interceptor.service';
import { DialogTemplateComponent } from './common/dialog-template/dialog-template.component';
import { CommonModulesModule } from './common/common-modules/common-modules.module';
import { ConfirmdialogComponent } from './common/confirmdialog/confirmdialog.component';
import { CustomSnackbarComponent } from './common/custom-snackbar/custom-snackbar.component';
import { OptValidationComponent } from './common/opt-validation/opt-validation.component';
import { OtpValidationDirective } from './common/directives/otp.directive';
import { WalkthroughComponent } from './common/walkthrough/walkthrough.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  declarations: [
    AppComponent, DialogTemplateComponent, ConfirmdialogComponent, OptValidationComponent, WalkthroughComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModulesModule,
    SlickCarouselModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AppInterceptorService, multi: true }],
  bootstrap: [AppComponent],
  entryComponents: [DialogTemplateComponent, ConfirmdialogComponent, CustomSnackbarComponent, OptValidationComponent, WalkthroughComponent]
})
export class AppModule { }
