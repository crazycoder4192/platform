import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptValidationComponent } from './opt-validation.component';

describe('OptValidationComponent', () => {
  let component: OptValidationComponent;
  let fixture: ComponentFixture<OptValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
