import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SMSService } from 'src/app/_services/utils/sms.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-opt-validation',
  templateUrl: './opt-validation.component.html',
  styleUrls: ['./opt-validation.component.scss']
})
export class OptValidationComponent implements OnInit, OnDestroy {

  public otpValidationForm: FormGroup;
  public phoneNumber: string;
  public errMessage: string;
  public limitMsg: string;
  public isAdvertiser: Boolean;
  public isPublisher: Boolean;
  public disableResend = false;
  constructor(private formBuilder: FormBuilder,
              private dialog: MatDialog,
              private smsService: SMSService,
              private snackBar: MatSnackBar,
              private spinner: SpinnerService,
              private dialogRef: MatDialogRef<OptValidationComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
         this.phoneNumber = data.phNumber;
         this.limitMsg = data.limitMsg;
        }

  ngOnInit() {
    this.otpValidationForm = this.formBuilder.group({
      OTP1: ['', Validators.required],
      OTP2: ['', Validators.required],
      OTP3: ['', Validators.required],
      OTP4: ['', Validators.required],
      OTP5: ['', Validators.required],
      OTP6: ['', Validators.required]
    });
    const currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    const userType = currentUserData['usertype'];
    if ( userType === 'advertiser') {
      this.isAdvertiser = true;
      this.isPublisher = false;
    } else if (userType === 'publisher') {
      this.isAdvertiser = false;
      this.isPublisher = true;
    }
  }

  ngOnDestroy() {

  }
  get f() { return this.otpValidationForm.controls; }
  onClose() {
    this.dialog.closeAll();
    window.onload = null;
    document.onmousemove = null;
    document.onmousedown = null;
    document.onkeydown = null;
    document.onscroll = null;
    document.ontouchstart = null;
    document.ontouchmove = null;
    this.dialog.closeAll();
  }
  onSubmit() {
    this.errMessage = null;
    if (this.otpValidationForm.valid) {
      const otp = this.otpValidationForm.get('OTP1').value +
                  this.otpValidationForm.get('OTP2').value +
                  this.otpValidationForm.get('OTP3').value +
                  this.otpValidationForm.get('OTP4').value +
                  this.otpValidationForm.get('OTP5').value +
                  this.otpValidationForm.get('OTP6').value;
      this.smsService.verifyotp(otp).subscribe (
        res => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'success',
              message: res['message']
            }
          });
          const result = {
            status: 'success',
            phNumber: this.phoneNumber
          };
          this.dialogRef.close(result);
        },
        err => {
          this.otpValidationForm.reset();
          this.errMessage = err.error['message'];
          this.limitMsg = err.error['otpLimitMsg'];
          if (!this.limitMsg) {
            this.dialogRef.close();
            this.callErrorToaster(err.error['message']);
          }
        });
      //this.dialogRef.close(otp);
    }
  }

  resendOTP() {
    if (!this.disableResend) {
      this.smsService.verifyPhoneNumber(this.phoneNumber).
      subscribe( res => {
        this.spinner.showSpinner.next(false);
        this.limitMsg = res['otpLimitMsg'];
        this.disableResend = true;
        this.callSuccessToaster('OTP Resent Successfully');
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
        this.onClose();
      });
    } else {
      if (this.isAdvertiser) {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-advertiser'],
          data: {
            icon: 'error',
            message: 'OTP Limit Exhausted. Please try again tomorrow'
          }
        });
      } else {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: 'OTP Limit Exhausted. Please try again tomorrow'
          }
        });
      }
    }
  }

  callErrorToaster(msg: string) {
    if (this.isAdvertiser) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'error',
          message: msg
        }
      });
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: msg
        }
      });
    }
  }
  callSuccessToaster(msg: string) {
    if (this.isAdvertiser) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-advertiser'],
        data: {
          icon: 'success',
          message: msg
        }
      });
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'success',
          message: msg
        }
      });
    }
  }
}
