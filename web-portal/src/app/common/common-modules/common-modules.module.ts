import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordStrengthBarComponent } from '../passwordstrengthBar/passwordStrengthBar';
import { MatStepperModule } from '@angular/material/stepper';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material_module';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { FieldErrorDisplayComponent } from '../field-error-display/field-error-display.component';
import { ChangepasswordComponent } from '../changepassword/changepassword.component';
import { TrimPipe } from '../pipes/trim.pipe';
import { NumberMaskPipe } from '../pipes/mask.pipe';
import { NumbersOnlyDirective } from '../directives/number.directive';
import { ImagedimensionsDirective } from '../directives/imagedimensions.directive';
import { InputkeysDirective } from '../directives/inputkeys.directive';
import { DeleteConfirmPopupComponent } from '../delete-confirm-popup/delete-confirm-popup.component';
import { AssetCreationInstructionsComponent } from '../asset-creation-instructions/asset-creation-instructions.component';
import { ConfirmPasswordDailogComponent } from '../confirm-password-dailog/confirm-password-dailog.component';
import { NgCircleProgressModule, CircleProgressComponent } from 'ng-circle-progress';
import { NgxPaginationModule } from 'ngx-pagination';
import { TreeviewModule, DropdownTreeviewComponent } from 'ngx-treeview';
import { DropDownWithTreeComponent } from '../drop-down-with-tree/drop-down-with-tree.component';
import { TermsAndConditionsComponent } from '../terms-and-conditions/terms-and-conditions.component';
import { OtpValidationDirective } from '../directives/otp.directive';
import { NotificationsPanelComponent } from '../notifications-panel/notifications-panel.component';
import { StringtransformPipe } from '../pipes/stringtransform.pipe';
import { CustomSnackbarComponent } from '../custom-snackbar/custom-snackbar.component';
import { AdvUserTermsAndConditionsComponent } from '../../advertiser/adv-user-terms-and-conditions/adv-user-terms-and-conditions.component';
import { SlideshowModule } from 'ng-simple-slideshow';
import { CarouselshowComponent } from '../carouselshow/carouselshow.component';

@NgModule({
  declarations: [
    PasswordStrengthBarComponent,
    FieldErrorDisplayComponent,
    ChangepasswordComponent,
    TrimPipe,
    NumberMaskPipe,
    NumbersOnlyDirective,
    ImagedimensionsDirective,
    InputkeysDirective,
    DeleteConfirmPopupComponent,
    ConfirmPasswordDailogComponent,
    DropDownWithTreeComponent,
    AssetCreationInstructionsComponent,
    TermsAndConditionsComponent,
    NotificationsPanelComponent,
    StringtransformPipe,
    CustomSnackbarComponent,
    AdvUserTermsAndConditionsComponent,
    OtpValidationDirective,
    CarouselshowComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxPasswordToggleModule,
    MatStepperModule,
    NgCircleProgressModule.forRoot({
      'radius': 60,
      'space': 2,
      'outerStrokeGradient': true,
      'outerStrokeWidth': 10,
      'outerStrokeColor': '#4882c2',
      'outerStrokeGradientStopColor': '#53a9ff',
      'innerStrokeColor': '#e7e8ea',
      'innerStrokeWidth': 10,
      'title': 'Defined Audience',
      'animateTitle': false,
      'animationDuration': 1000,
      'titleFontSize': '12',
      'showUnits': false,
      'showBackground': false,
      'clockwise': false,
      'startFromZero': true,
      'lazy': false
    }),
    NgxPaginationModule,
    TreeviewModule.forRoot(),
    SlideshowModule
  ],
  exports: [
    ReactiveFormsModule,
    PasswordStrengthBarComponent,
    MaterialModule,
    PasswordStrengthBarComponent,
    FieldErrorDisplayComponent,
    ChangepasswordComponent,
    TrimPipe,
    NumberMaskPipe,
    NumbersOnlyDirective,
    ImagedimensionsDirective,
    InputkeysDirective,
    DeleteConfirmPopupComponent,
    ConfirmPasswordDailogComponent,
    CircleProgressComponent,
    DropdownTreeviewComponent,
    DropDownWithTreeComponent,
    AssetCreationInstructionsComponent,
    TermsAndConditionsComponent,
    NotificationsPanelComponent,
    StringtransformPipe,
    CustomSnackbarComponent,
    OtpValidationDirective,
    CarouselshowComponent
  ]
})
export class CommonModulesModule {}
