import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetCreationInstructionsComponent } from './asset-creation-instructions.component';

describe('AssetCreationInstructionsComponent', () => {
  let component: AssetCreationInstructionsComponent;
  let fixture: ComponentFixture<AssetCreationInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetCreationInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetCreationInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
