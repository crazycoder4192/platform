import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-asset-creation-instructions',
  templateUrl: './asset-creation-instructions.component.html',
  styleUrls: ['./asset-creation-instructions.component.scss']
})
export class AssetCreationInstructionsComponent implements OnInit {

  public title: string;
  constructor(
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public entity: any
  ) { }

  ngOnInit() {
    this.title = this.entity.title;
  }
  onClose() {
    this.dialog.closeAll();
  }

}
