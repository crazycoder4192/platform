import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-captcha-comp',
  template: `
  <div>
    <!--<label>{{labelText}}  </label>-->
      <div style="background-color:none; width:100px; height:30px;padding:0 0px;">
        <span id="captcha"></span>
        <a style="float:right; margin:0px 0;position: absolute; top: 15px; right:0px;" (click)="drawCaptcha() ">
          <img src="./assets/images/refresh.png" width="24" height="24" alt="" />
        </a>
      </div>
  </div>
  `,
  styles: []
})
export class CaptchaCompComponent implements OnInit {
  @Output() captchatxt = new EventEmitter<String>();
  public captchaCode: any;
  public canv: any;
  public labelText: String;

//  constructor() {
//    this.labelText = 'Enter Captcha ';
//  }

  ngOnInit() {
    this.drawCaptcha();
  }

  drawCaptcha() {
    const min = 1; const max = 4;
    const currentRmValue = Math.floor(Math.random() * (max - min + 1) + min);
    if (currentRmValue === 1) {
      this.captchaCode = Math.ceil(Math.random() * 9)
      + '' + ' ' + Math.ceil(Math.random() * 9) + '' + ' '
      + ' ' + Math.ceil(Math.random() * 9) + '' + ' '
      + Math.ceil(Math.random() * 9) + '';
      this.captchatxt.emit(this.captchaCode.split(' ').join(''));
      // this.labelText = 'Enter numbers ';
    } else if (currentRmValue === 2) {
      this.captchaCode = '';
      const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      for (let i = 0; i < 5; i++) {
        this.captchaCode += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      this.captchatxt.emit(this.captchaCode);
      // this.labelText = 'Enter text ';
    } else if (currentRmValue === 3) {
      this.captchaCode = '';
      const rand_num1 = Math.floor(Math.random() * 10) + 1;
      const rand_num2 = Math.floor(Math.random() * 10) + 1;
      const rand_num3 = Math.floor(Math.random() * 10) + 1;
      const sum = rand_num1 + rand_num2 + rand_num3;
      this.captchaCode = rand_num1 + '+' + rand_num2 + '+' + rand_num3 + '= ?';
      this.captchatxt.emit(sum.toString());
      // this.labelText = 'Add the numbers and type the results';
    } else if (currentRmValue === 4) {
      this.captchaCode = '';
      const rand_num1 = Math.floor(Math.random() * 10) + 1;
      const rand_num2 = Math.floor(Math.random() * 10) + 1;
      const sum = rand_num1 * rand_num2;
      this.captchaCode = rand_num1 + '  *  ' + rand_num2 + ' = ?';
      this.captchatxt.emit(sum.toString());
      // this.labelText = 'Multiply the numbers and type the results';
    }
// else if (currentRmValue === 5) {
//      this.captchaCode = '';
//      const a = Math.ceil(Math.random() * 9) + '';
//      const b = Math.ceil(Math.random() * 9) + '';
//      const c = Math.ceil(Math.random() * 9) + '';
//      const d = Math.ceil(Math.random() * 9) + '';
//      const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
//      for (let i = 0; i < 5; i++) {
//        this.captchaCode = possible.charAt(Math.floor(Math.random() * possible.length))
//        + ' ' + a + ' ' + possible.charAt(Math.floor(Math.random() * possible.length)) + ' ' + b + ' '
//        + ' ' + possible.charAt(Math.floor(Math.random() * possible.length)) + ' ' + c + ' '
//        + possible.charAt(Math.floor(Math.random() * possible.length)) + ' ' + d;
//      }
//      this.captchatxt.emit((a + ' ' + b + ' ' + ' ' + c + ' ' + d).split(' ').join(''));
//      this.labelText = 'Enter only numbers ';
//    }

    if (document.getElementById('captcha').childElementCount > 0) {
      document.getElementById('captcha').removeChild(this.canv);
    }
    this.canv = document.createElement('canvas');
    this.canv.id = 'captcha';
    this.canv.width = 200;
    this.canv.height = 35;
    const ctx = this.canv.getContext('2d');
    ctx.font = '25px Georgia';
    ctx.strokeText(this.captchaCode, 0, 30);
    document.getElementById('captcha').appendChild(this.canv); // adds the canvas to the body element
 }

}
