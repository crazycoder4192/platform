import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptchaCompComponent } from './captcha-comp.component';

describe('CaptchaCompComponent', () => {
  let component: CaptchaCompComponent;
  let fixture: ComponentFixture<CaptchaCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaptchaCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptchaCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
