import { Component, OnInit, Output, Input, EventEmitter, HostListener } from '@angular/core';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { NotificationsService } from 'src/app/_services/notifications.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/authentication.service';

@Component({
  selector: 'app-notifications-panel',
  templateUrl: './notifications-panel.component.html',
  styleUrls: ['./notifications-panel.component.scss']
})
export class NotificationsPanelComponent implements OnInit {
  public clickState = false;
  @Output() private toggleNotification = new EventEmitter<boolean>();
  preActiveIcon: any;
  activeIcon: any;
  notificationData: any;
  public usertype = this.authenticationService.currentUserValue.usertype;
  rolePermissions: any;
  constructor(
    private authenticationService: AuthenticationService,
    private eventEmitterService: EventEmitterService,
    private spinner: SpinnerService,
    private notificationService: NotificationsService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.preActiveIcon = JSON.parse(localStorage.getItem('preActiveIcon'));
    this.activeIcon = JSON.parse(localStorage.getItem('activeIcon'));
    this.getNotifications();
  }
  @HostListener('click')
  clickInside() {
    this.clickState = false;
  }
  @HostListener('document:click')
  clickout() {
    if (this.clickState) {
      if (this.usertype === 'publisher') {
        localStorage.setItem('activeIcon', JSON.stringify(''));
      } else {
        if (this.activeIcon && this.activeIcon === 'notification') {
          localStorage.setItem('activeIcon', JSON.stringify('this.preActiveIcon'));
        } else if (this.activeIcon && this.activeIcon !== 'notification') {
          localStorage.setItem('activeIcon', JSON.stringify(this.preActiveIcon));
        }
      }
      this.toggleNotification.emit(false);
      this.eventEmitterService.onClickNavigation();
    }
    this.clickState = true;
  }
  getNotifications() {
    this.notificationService.getNotifications().subscribe(
      res => {
        this.notificationData = res;
        this.notificationData['notifications'].sort((a, b) => new Date(b.created.at).getTime() - new Date(a.created.at).getTime());
      },
      err => {
      console.log('Error! getNotifications : ' + err);
    });
  }
  readNotification(id) {
    this.notificationService.markNotificationAsRead(id).subscribe(
      res => {
        this.getNotifications();
        this.eventEmitterService.onReadNotification();
      },
      err => {
      console.log('Error! removeNotification : ' + err);
    });
  }
  removeNotification(id) {
    this.notificationService.removeNotification(id).subscribe(
      res => {
        this.getNotifications();
        this.eventEmitterService.onReadNotification();
      },
      err => {
      console.log('Error! removeNotification : ' + err);
    });
  }
  reviewNotification(typeDetails, id) {
    this.readNotification(id);
    this.toggleNotification.emit(false);
    if (typeDetails.routeTo === 'campaign') {
      this.router.navigate(['/advertiser/campaignManage']);
    } else if (typeDetails.routeTo === 'manage-account' && this.usertype === 'advertiser') {
      this.router.navigate(['/advertiser/accountCreation']);
    } else if (typeDetails.routeTo === 'manage-account' && this.usertype === 'publisher') {
      this.router.navigate(['/publisher/platformManage']);
    } else if (typeDetails.routeTo === 'payment' && this.usertype === 'advertiser') {
      this.router.navigate(['/advertiser/payments']);
    } else if (typeDetails.routeTo === 'payment' && this.usertype === 'publisher') {
      this.router.navigate(['/publisher/payments']);
    }
  }

}
