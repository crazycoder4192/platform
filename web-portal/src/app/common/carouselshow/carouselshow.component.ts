import { Component, OnInit, Input } from '@angular/core';
import config from 'appconfig.json';

const awsS3Path = config['AWS-S3']['BUCKET'] + '.' + config['AWS-S3']['PATH'];
const cloudFrontPath = config['CLOUND-FRONT']['URL'];

@Component( {
  selector: 'app-carouselshow',
  template: `<slideshow [minHeight]="height" [autoPlay]="false"
    [showArrows]="imageUrls.length > 1" [arrowSize]="'10px'"
    [imageUrls]="imageUrls"
    [lazyLoad]="imageUrls.length > 1" [autoPlayWaitForLazyLoad]="false"
    [backgroundSize]="'contain'">
  </slideshow>`
} )

export class CarouselshowComponent implements OnInit {
  private _imageUrls: string[];
  @Input() height: string;

  get imageUrls() {
    return this._imageUrls;
  }

  @Input('imageUrls')
  set imageUrls(s3url: string[]) {
    this._imageUrls = s3url.map( item => item.replace(awsS3Path, cloudFrontPath));
  }

  constructor() { }

  ngOnInit() {
  }

}
