import { FormGroup, AbstractControl, Validators } from '@angular/forms';

export function ValidatePassword(control: AbstractControl) {
  let valid = false;

  if (control.value && control.value.length > 5) {
    valid = /^(?=.*\d)(?=.*[~`!@#$%^&*()_=+}{|;:'"<>,.\\/[\]\?-])(?=.*[a-z])(?=.*[A-Z])[A-Za-z\d~`!@#$%^&*()_=+}{|;:'"<>,.\\/[\]\?-]{6,}$/.test(control.value);
    if (!valid) {
      return { validPassword: true };
    }
  }
  return null;
}

export function MustMatch(firstName: string, lastName: string, oldPassword: string, currentPassword: string, confirmPassword: string) {
  return (formGroup: FormGroup) => {

    const firstNameval = formGroup.controls[firstName].value;
    const lastNameval = formGroup.controls[lastName].value;
    const control = formGroup.controls[currentPassword];
    const oldPasswordCtrl = formGroup.controls[oldPassword];

    if (control.errors && !control.errors.mustMatch) {
      return;
    }

    let fristNameExists = false;
    let lastNameExists = false;
    let firstName4CharsExists = false;
    let lastName4CharsExists = false;

    if (firstNameval) {
      fristNameExists = control.value.toLowerCase().includes(firstNameval.toLowerCase());
      firstName4CharsExists = control.value.toLowerCase().includes(firstNameval.toLowerCase().substring(0, 4).toLowerCase());
    }

    if (lastNameval) {
      lastNameExists = control.value.toLowerCase().includes(lastNameval.toLowerCase());
      lastName4CharsExists = control.value.toLowerCase().includes(lastNameval.toLowerCase().substring(0, 4));
    }

    if (fristNameExists || lastNameExists) {
      control.setErrors({ firstNameOrLastExists: true });
    }

    if (firstName4CharsExists || lastName4CharsExists) {
      control.setErrors({ first4CharsExists: true });
    }

    if (oldPasswordCtrl) {
      if (oldPasswordCtrl.errors && !oldPasswordCtrl.errors.mustMatch) {
        return;
      }
      if (control.value.toLowerCase() === oldPasswordCtrl.value.toLowerCase()) {
        formGroup.controls[currentPassword].setErrors({ samePassword: true });
      }
    }

    const matchingControl = formGroup.controls[confirmPassword];
    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }

  };

}

export function CheckPasswords(password, confirmPassword) { // here we have the 'passwords' group
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[password];
    const matchingControl = formGroup.controls[confirmPassword];
    if (control.errors && !control.errors.mustMatch) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function Number(control: AbstractControl) {
  let valid = false;

  if (control.value && control.value.length > 5) {
    valid = /^[0-9]*$/.test(control.value);
    if (!valid) {
      return { validNumber: true };
    }
  }
  return null;
}

export function ValidateDomain(domainName) {
  const reg = '([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'; /*Actual domain reg expression*/
  return (formGroup: FormGroup) => {
    const domainNameValuectrl = formGroup.controls[domainName];
    if (domainNameValuectrl.value && (domainNameValuectrl.value.toLowerCase().indexOf('http://') !== -1 ||
        domainNameValuectrl.value.toLowerCase().indexOf('https://') !== -1 ||
        domainNameValuectrl.value.toLowerCase().indexOf('://') !== -1)) {
      domainNameValuectrl.setErrors({ containsHttpOrHtttps: true });
    }
  };
}

export function CheckBids(bidAmountValue, limitToValue, totalBudget) {
  return (formGroup: FormGroup) => {
    const bidAmountValuectrl = formGroup.controls[bidAmountValue];
    const limitToValuectrl = formGroup.controls[limitToValue];
    const totalBudgetctrl = formGroup.controls[totalBudget];
    if (+limitToValuectrl.value >= +totalBudgetctrl.value) {
      limitToValuectrl.setErrors({ greaterThan: true });
    }
    if (+limitToValuectrl.value <= +bidAmountValuectrl.value) {
      limitToValuectrl.setErrors({ lessThan: true });
    }
    if ((+limitToValuectrl.value < +totalBudgetctrl.value) &&
        (+limitToValuectrl.value > +bidAmountValuectrl.value)) {
      limitToValuectrl.setErrors(null);
    }
    // if (+bidAmountValuectrl.value >= +limitToValuectrl.value) {
    //   bidAmountValuectrl.setErrors({ less: true });
    // }
  };
}

export function checkExpiryDate(monthValue, yearValue) {
  return (formGroup: FormGroup) => {
    const expiryMonthCtrl = formGroup.controls[monthValue];
    const yearCtrl = formGroup.controls[yearValue];
    if (yearCtrl.value === new Date().getFullYear()) {
      if (expiryMonthCtrl.value < new Date().getMonth() ) {
        expiryMonthCtrl.setErrors({ monthInvalid: true});
      }
    } else {
      expiryMonthCtrl.setErrors(null);
    }
  };
}
