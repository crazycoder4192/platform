import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-dialog-template',
  templateUrl: `./dialog-template.component.html`,
  styleUrls: ['./dialog-template.component.css']
})
export class DialogTemplateComponent implements OnInit, OnDestroy {

  public timer = 60;
  private timerRef: any;
  public isAdvertiser: Boolean;
  public isPublisher: Boolean;

  title: string;
  messaege: string;

  constructor(private dialog: MatDialog) {  }

  ngOnInit() {
    this.timerRef = setInterval(() => {
      if (this.timer > 0) {
        this.timer--;
      } else {
        this.onClose();
        this.stopInterval();
      }
    }, 1000);
    let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    let userType = currentUserData['usertype'];
    if( userType === 'advertiser') {
      this.isAdvertiser = true;
      this.isPublisher = false;
    } else if (userType === 'publisher') {
      this.isAdvertiser = false;
      this.isPublisher = true;
    }
  }

  stopInterval() {
    clearInterval(this.timerRef);
  }

  onClose() {
    this.dialog.closeAll();
    window.onload = null;
    document.onmousemove = null;
    document.onmousedown = null;
    document.onkeydown = null;
    document.onscroll = null;
    document.ontouchstart = null;
    document.ontouchmove = null;
    this.dialog.closeAll();
  }

  ngOnDestroy() {
    this.timer = 0;
  }

}
