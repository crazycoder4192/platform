import { ChangeDetectionStrategy, Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';
import { isEqual } from 'lodash';

export const MULTI_SELECT_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef( () => DropDownWithTreeComponent ),
  multi: true
};

const updateTreeLeaf = ( item: TreeviewItem, selected: string[]) => {
  const shouldBeChecked = selected.indexOf( item.value ) !== -1;

  // do not mutate if there are no changes
  if ( item.checked === shouldBeChecked ) {
    return item;
  }

  const modifiedItem = {
    ...item,
    checked: shouldBeChecked
  };

  return new TreeviewItem( modifiedItem );

};

const updateTreeParent = ( item: TreeviewItem, selected: string[]) => {

  const children = item.children.map( child => mapItem( child, selected ) );

  // do not mutate parent if there are no changes
  if ( isEqual(children, item.children) ) {
    return item;
  }

  const modifiedParent = {
    ...item,
    children
  };

  return new TreeviewItem( modifiedParent, true );

};

/**
 * Recursively map items and set checked property without mutating the original items
 * @param {TreeviewItem} item
 * @param {string[]} selected
 * @returns {TreeviewItem}
 */
const mapItem = ( item: TreeviewItem, selected: string[] ) => {

  if ( item.children && item.children.length > 0 ) {
    return updateTreeParent(item, selected);
  }

  //update TreeLeave item
  return updateTreeLeaf( item, selected);
};

@Component( {
  selector: 'app-drop-down-with-tree',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ngx-dropdown-treeview
      [items]="items"
      [config]="config"
      (selectedChange)="onSelect($event)"
    >
    </ngx-dropdown-treeview>`,
  providers: [
    MULTI_SELECT_INPUT_CONTROL_VALUE_ACCESSOR,
  ]
} )
export class DropDownWithTreeComponent implements ControlValueAccessor {

  @Input() config: TreeviewConfig = TreeviewConfig.create( {
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: false,
    maxHeight: 350,
  } );

  @Output() selectedChange = new EventEmitter<String>();
  @Output() filterChange = new EventEmitter<String>();
  private _items: TreeviewItem[];
  private selected: string[] = [];

  // internal functions to call by ControlValueAccessor
  private onTouched: Function;
  private onModelChange: Function;

  /*ngOnInit(){
    this.checkedList.subscribe(v => {
       console.log('value is changing', v);
       this.onSelect(v);
       this.updateItems(this.items, v);
    });
  }*/
  @Input( 'items' )
  set items( items: TreeviewItem[] ) {

    //modify items so selected values are checked ( and all others will be unchecked )
    this.updateItems( items, this.selected );

  }

  get items() {
    return this._items;
  }

  public onSelect( items ) {
    //sort values so isEqual comparison works
    const selected = [ ...items ].sort();
    this.selected = selected;

    //trigger change on form
    this.onModelChange( selected );
    this.selectedChange.emit(items);
  }

  /**
   * set the checked property on all Treeview items
   * @param {TreeviewItem[]} items
   * @param {string[]} selected
   */
  public updateItems( items: TreeviewItem[], selected: string[] ) {

    // recursively walk through all treeview items and set the checked value
    this._items = items.map( item => mapItem( item, selected ) );
    this.selected = selected;
  }

  /********************************
   * ControlValue methods
   ********************************/

  /**
   * This is called from a form input to set the internal value
   * @param {string[]} selected
   */
  public writeValue( selected: string[] ): void {
    if (selected.length > 0) {
    /*sort values so isEqual comparison works*/
    selected = [ ...selected ].sort();

    if ( isEqual( this.selected, selected ) ) {
      return;
    }
  }
    this.updateItems( this.items, selected );
  }

  registerOnChange( fn: Function ) {
    this.onModelChange = fn;
  }

  registerOnTouched( fn: () => any ): void {
    this.onTouched = fn;
  }

  setDisabledState?( isDisabled: boolean ): void {
    throw new Error( 'Method not implemented.' );
  }

}