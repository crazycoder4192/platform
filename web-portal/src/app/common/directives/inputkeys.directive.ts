import { Directive, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  ENTER = 13
}

@Directive({
  selector: '[appInputkeys]'
})
export class InputkeysDirective {

  @Output()
  tagValue: EventEmitter<string> = new EventEmitter<string>();

  constructor(private el: ElementRef) { }
  @HostListener('keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {

    if (event.keyCode === KEY_CODE.ENTER) {
      this.tagValue.emit(this.el.nativeElement.value);
      this.el.nativeElement.value = '';
    }
  }


}
