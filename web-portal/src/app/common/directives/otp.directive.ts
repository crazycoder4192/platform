import { Directive, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
    selector: '[appOtpValidation]'
  })
  export class OtpValidationDirective {

    @Output()
    tagValue: EventEmitter<string> = new EventEmitter<string>();

    constructor(private el: ElementRef, private control: NgControl) {
      if (el && control && control.control) {
        (<any>control.control).nativeElement = el.nativeElement;
      }
     }
    @HostListener('keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
      if (this.el.nativeElement.value.match(/[^0-9]/g)) {
        this.el.nativeElement.value = '';
      }
      if (this.el.nativeElement.value.length > 1) {
       const temp = this.el.nativeElement.value.substring(1, 2);
        if (this.control && this.control.control) {
          this.control.control.setValue(temp);
        }
      }
      if (this.el.nativeElement.value.length === 1) {
        const element = document.getElementById(this.el.nativeElement.getAttribute('nextFocus'));
        if (element) {
            element.focus();
        }
      }
    }
}