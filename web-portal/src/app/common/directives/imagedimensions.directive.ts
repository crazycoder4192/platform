import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[getDimensions]'
})
export class ImagedimensionsDirective {
  public elementWidth = '';
  public elementHeight = '';
  constructor(private el: ElementRef) { }
  @HostListener('load', ['$event'])
  load() {
    this.elementWidth = this.el.nativeElement.width;
    this.elementHeight = this.el.nativeElement.height;
  }

}
