import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mask'
})
export class NumberMaskPipe implements PipeTransform {
  transform(number: string, visibleDigits: number = 2): string {
    const visibleSection = number.slice(0, visibleDigits);
    const maskedSection = number.slice(visibleDigits);
    return (
      visibleSection + ' ' + maskedSection.replace(/./g, 'X').replace(/(.{4})/g, '$1 ')
    );
  }
}
