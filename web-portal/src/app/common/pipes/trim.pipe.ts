import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim'
})
export class TrimPipe implements PipeTransform {

  transform(value: string, args?: any): any {
     if (!value) {
      return '';
    }
    return value.toString().replace(/\s/g, '');
  }

}
