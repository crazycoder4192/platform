import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringtransform'
})
export class StringtransformPipe implements PipeTransform {

  transform(value: string, args?: any): any {
     if (!value) {
      return '';
    }
    return value.toString().replace(/./g, args);
  }

}
