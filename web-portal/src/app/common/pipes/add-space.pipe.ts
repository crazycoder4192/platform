import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addspace'
})
export class AddSpacePipe implements PipeTransform {

  transform(value: string, args?: any): any {
     if (!value) {
      return '';
    }
    return value.toString().replace(/,/g, ', ');
  }

}
