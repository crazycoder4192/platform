import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})
export class TermsAndConditionsComponent implements OnInit {

  public title: string;
  constructor(
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public entity: any
  ) { }

  ngOnInit() {
    this.title = 'Terms and Conditions';
  }
  onClose() {
    this.dialog.closeAll();
  }

}
