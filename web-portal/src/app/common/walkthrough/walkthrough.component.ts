import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.component.html',
  styleUrls: ['./walkthrough.component.scss']
})
export class WalkthroughComponent implements OnInit {
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    infinite: false,
    speed: 200,
    dotsClass: 'slick-dots',
    'prevArrow': "<button type='button' class='slick-prev pull-left'></button>",
    'nextArrow': "<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
    'cssEase': 'ease-in'
  };

  private advertWalkthrough: Object[] = [];
  private publisherWalkthrough: Object[] = [];
  private pubHeaderMessages: Object[] = [ {
    h5 : 'Welcome to ',
    h6 : 'DOCEREE FOR PUBLISHERS'
  },
    'BECOME A PUBLISHER PARTNER',
    'EARN PREMIUM PRICING',
    'OPTIMIZE YOUR INVENTORY UTILIZATION',
    'COMPLETE TRANSPARENCY AND NO ACCOUNTING HASSLES'
  ];

  private pubSmallMessages: String[] = [
    'Physician marketing is now precision marketing',
    'A publisher network where you can register multiple platforms and multiple advertising inventories.',
    'Every click, impression and view recorded on your platform gets you higher CPM/CPC.',
    'Monetize your unutilized ad inventory with specialty and generic pharmaceutical advertisers’ who are looking to reach engaged physician audiences on your platform.',
    'Paid through a transparent billing process, credited in your account every month'
  ];

  private pubImgSrcList: String[] = [
    '../../../assets/images/walkthrough/whiteImage.png',
    '../../../assets/images/walkthrough/pubWalkthrough2.jpg',
    '../../../assets/images/walkthrough/pubWalkthrough3.jpg',
    '../../../assets/images/walkthrough/pubWalkthrough4.jpg',
    '../../../assets/images/walkthrough/pubWalkthrough5.jpg',
  ];

  private advHeaderMessages: Object[] = [ {
    h5 : 'Welcome to ',
    h6 : 'DOCEREE FOR ADVERTISERS'
  },
    'BECOME AN ADVERTISER',
    'PRECISION TARGET YOUR PHYSICIAN AUDIENCE',
    'MONITOR AND OPTIMIZE YOUR CAMPAIGNS',
    'MEASURE THE IMPACT'
  ];

  private advSmallMessages: String[] = [
    'Physician marketing is now precision marketing',
    'An integrated interface, which gives you access to a huge publisher network where you can easily manage multiple brands, multiple campaigns',
    'Use intelligent targeting tools to advertise to your audiences on multiple platforms.',
    'Pause, stop or edit campaigns in real-time with ability to change the messaging and budgets on the go, based on audience response.',
    'Integrated singular dashboard for all brands, to review impact of every dollar spent.'
  ];

  private advImgSrcList: String[] = [
    '../../../assets/images/walkthrough/whiteImage.png',
    '../../../assets/images/walkthrough/advWalkthrough2.jpg',
    '../../../assets/images/walkthrough/advWalkthrough3.jpg',
    '../../../assets/images/walkthrough/advWalkthrough4.jpg',
    '../../../assets/images/walkthrough/advWalkthrough5.jpg',
  ];

  private usertype: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private amplitudeService: AmplitudeService
  ) {
    this.usertype = data.context;
    for (var index in this.pubHeaderMessages) {
      this.advertWalkthrough.push({
        msgHeader: this.advHeaderMessages[index],
        msgSmall: this.advSmallMessages[index],
        src: this.advImgSrcList[index]
      });

      this.publisherWalkthrough.push({
        msgHeader: this.pubHeaderMessages[index],
        msgSmall: this.pubSmallMessages[index],
        src: this.pubImgSrcList[index]
      });
    }

   }

  ngOnInit() {
  }

  slickInit(e: any) {
  }

  afterChange(e: any) {
    if(JSON.parse(localStorage.getItem('currentUser'))['usertype'] === 'publisher') {
      if(e['currentSlide']) {
        if (e['currentSlide'] === 1) {
          this.amplitudeService.logEvent('onboarding - user views become a publisher', null);
        } else if (e['currentSlide'] === 2) {
          this.amplitudeService.logEvent('onboarding - user views earn premium', null);
        } else if (e['currentSlide'] === 3) {
          this.amplitudeService.logEvent('onboarding - user views optimise inventory', null);
        } else if (e['currentSlide'] === 4) {
          this.amplitudeService.logEvent('onboarding - user views complete transparency', null);
        }
      }
    } else if (JSON.parse(localStorage.getItem('currentUser'))['usertype'] === 'advertiser') {
      if(e['currentSlide']) {
        if (e['currentSlide'] === 1) {
          this.amplitudeService.logEvent('onboarding - user views become an advertiser', null);
        } else if (e['currentSlide'] === 2) {
          this.amplitudeService.logEvent('onboarding - user views precision target', null);
        } else if (e['currentSlide'] === 3) {
          this.amplitudeService.logEvent('onboarding - user views monitor and optimise', null);
        } else if (e['currentSlide'] === 4) {
          this.amplitudeService.logEvent('onboarding - user views measure the impact', null);
        }
      }
    }


  }
  
}
