import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';
import { UtilService } from '../../_services/utils/util.service';
import config from 'appconfig.json';
import { ManagedUpload } from 'aws-sdk/clients/s3';
import { AdvertiserBrandService } from 'src/app/_services/advertiser/advertiser-brand.service';
import { Brand } from 'src/app/_models/advertiser_brand_model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadToS3Service {
  params: any;
  brandList: Brand[];
  brandIdentifier: string;
  _brandIdentifySource: Subject<string>;
  constructor(private utilService: UtilService,
    private brandService: AdvertiserBrandService) {
    // Set up credentials
      AWS.config.credentials = new AWS.Credentials({
        accessKeyId: config['AWS-S3']['ACCESSID'],
        secretAccessKey: config['AWS-S3']['SECRETACCESSKEY']
      });
    this.params = {
      Bucket: config['AWS-S3']['BUCKET'],
      region: config['AWS-S3']['REGION'],
      Key: config['AWS-S3']['FOLDER'] + '/'
    };
   }

  uploadfile(file) {
    let randomNum = Date.now() + Math.floor(Math.random() * 1000) + 1;
    this.params.Key = config['AWS-S3']['FOLDER'] + '/' + this.brandIdentifier + '/' +file.name + '-' +randomNum;
    console.log(this.params.key);
    this.params['Body'] = file;

    const s3 = new AWS.S3();
    s3.upload(this.params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        return false;
      }

      console.log('Successfully uploaded file.', data);
      return true;
    });
  }

  deletefile(fileName: string, brandIdentifier: string) {
    const deleteParams = {
      Bucket: config['AWS-S3']['BUCKET'],
      Key: config['AWS-S3']['FOLDER'] + '/' + brandIdentifier + '/' + fileName
    };
    console.log(deleteParams);
    const s3 = new AWS.S3();
    s3.getObject(deleteParams).on('success', function(response) {
      s3.deleteObject(deleteParams, function (err, data) {
        if (err) {
          console.log('There was an error while deleting your file: ', err);
          return false;
        }
        console.log('Successfully deleted file.', data);
        return true;
      });
    }).on('error', function(error) {
      console.log('Error while checking file exist or not.', error);
    }).send();

  }

 fetchBrandIdentifierS3(){
  this._brandIdentifySource = new Subject<string>();
  this.brandService.getAllBrands().subscribe(
    res => {
      this.brandList = <Brand[]>res;
      this.brandIdentifier = this.brandList[0].brandName + '_' + this.brandList[0]._id;
      this._brandIdentifySource.next(this.brandIdentifier);
      this._brandIdentifySource.unsubscribe();
    },
    err => {
      console.log('Error! getAllBrands : ' + err);
    });
  }

uploadFileAndGetResult(file: any, fileName: String, brandIdentifier: String, fileExtension: String): ManagedUpload {
    let randomNum = Date.now() + Math.floor(Math.random() * 10000) + 1;
    this.params.Key = config['AWS-S3']['FOLDER'] + '/' + brandIdentifier + '/' + fileName + '-' + randomNum + '.' + fileExtension;
    this.params['Body'] = file;
    return new AWS.S3.ManagedUpload({params: this.params});
  }

  getListofObjectsInAnBucket() {
  const params = {
    Bucket: config['AWS-S3']['BUCKET'],
    Prefix: config['AWS-S3']['FOLDER'] + '/'
  };
  const s3 = new AWS.S3();
  s3.listObjects(params, function(err, data) {
    if (err) {
      console.error(err); // an error occurred
    } else {
      /*const string = new TextDecoder('utf-8').decode(data.Body.toString());*/
      console.log(data['Contents']);
    }
  });
  }

}
