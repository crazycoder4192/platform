import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { ValidatePassword, MustMatch } from '../validators/validator';
import { DeleteConfirmPopupComponent } from '../delete-confirm-popup/delete-confirm-popup.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { User } from 'src/app/_models/users';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
  @ViewChild('f') myNgForm;
  chngpwdForm: FormGroup;
  secQsForm: FormGroup;
  barLabel: String = 'Password Strength';
  submitted = false;
  quesFormSubmitted = false;
  user: User;
  message;
  status;
  messageQs;
  statusQs;
  checkQuestions;
  pwdHide = true;
  confirmPwdHide = true;
  confirmHide = true;
  @Input() param1: any;

  security_questions = [
    'What school did you attend for fifth grade?',
    'What was the name of your first pet?',
    'What was your dream job as a child?',
    'What is the name of your favorite childhood friend?',
    'What is your oldest cousin\'s first and last name?',
    'In what city did you meet your spouse/significant other?',
    'In what city was your first job?'
  ];
  question1;
  question2;
  sameQuestionMsg: string;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['publisher_settings_account_delete_account', 'advertiser_settings_account_security_questions']);

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog,
    private toolTipService: ToolTipService,
    private snackBar: MatSnackBar
  ) {  }

  ngOnInit() {
    this.createForm();
    this.checkQuestions = JSON.parse(localStorage.getItem('currentUser')).security;
    this.userService.getUserByEmail(this.authenticationService.currentUserValue.email).subscribe(
      res => {
        this.user = res;
        this.chngpwdForm.get('firstName').setValue(this.user.firstName);
        this.chngpwdForm.get('lastName').setValue(this.user.lastName);
      }
    );
    this.createSecQsForm();
    this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  } 
  get f() { return this.chngpwdForm.controls; }
  get secQsf() { return this.secQsForm.controls; }

  random_item(items)  {
    return items[Math.floor(Math.random() * items.length)];
  }

  createSecQsForm() {
    // this.question1 = this.random_item(this.security_questions);
    // this.question2 = this.random_item(this.security_questions);
    // if (this.question1 === this.question2) {
    //   this.question2 = this.random_item(this.security_questions);
    // }
    this.secQsForm = this.formBuilder.group({
      securityQuestion1: ['', !this.checkQuestions ? Validators.required : ''],
      securityAnswer1: ['', !this.checkQuestions ? Validators.required : ''],
      securityQuestion2: ['', !this.checkQuestions ? Validators.required : ''],
      securityAnswer2: ['', !this.checkQuestions ? Validators.required : '']
    });
  }

  createForm() {
    this.chngpwdForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      curPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      confirmPassword: ['', Validators.required]
    }, {validator: MustMatch('firstName', 'lastName', 'curPassword', 'newPassword', 'confirmPassword')});

  }
  reset() {
    this.myNgForm.resetForm();
  }
  onSubmit() {
    this.submitted = true;
    if (this.chngpwdForm.invalid) {
      return;
    }

    this.userService.changePwd({
      'currentPwd': this.chngpwdForm.value.curPassword,
      'newPassword': this.chngpwdForm.value.newPassword
    }).subscribe(res => {
      this.message = res['message'];
      this.status = res['success'];
      if (this.status === true) {
        this.reset();
        this.submitted = false;
      }
    },
    err => {
      this.reset();
    });
  }

  onSaveSecQs() {
    this.quesFormSubmitted = true;
    if (this.secQsForm.invalid) {
      return;
    }

    if (this.secQsForm.value.securityQuestion1 === this.secQsForm.value.securityQuestion2) {
      this.secQsForm.controls['securityQuestion2'].setErrors({same: true});
      return;
    }

    this.userService.createQuestions({
      'securityQuestion1': this.secQsForm.value.securityQuestion1 ? this.secQsForm.value.securityQuestion1 : '',
      'securityAnswer1': this.secQsForm.value.securityAnswer1 ? this.secQsForm.value.securityAnswer1 : '',
      'securityQuestion2': this.secQsForm.value.securityQuestion2 ? this.secQsForm.value.securityQuestion2 : '',
      'securityAnswer2': this.secQsForm.value.securityAnswer2 ? this.secQsForm.value.securityAnswer2 : '',
    }).subscribe(res => {
      this.messageQs = res['message'];
      this.statusQs = res['success'];
      this.reset();
    },
    err => {
      console.log('Security Questions updated Error!');
      this.reset();
    });
  }
  deleteAccount() {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'This action will permanently delete your account. Are you sure you want to proceed?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const email = this.authenticationService.currentUserValue.email;
        this.userService.deleteUser().subscribe(
          res => {
           this.snackBar.open(res['message'], '', {
             duration: 3000,
             verticalPosition: 'top',
             horizontalPosition: 'right',
             panelClass: ['success-campaign-notify']
           });
           this.logout();
          },
          err => {
           this.snackBar.open(err.error['message'], '', {
             duration: 3000,
             verticalPosition: 'top',
             horizontalPosition: 'right',
             panelClass: ['success-campaign-notify']
           });
          }
        );
      }
    });
  }
  logout() {
    this.authenticationService.logout(this.user.usertype)
      .pipe()
      .subscribe(
        data => {
          console.log('Logout successfull : ' + data);
      },
      error => {
        console.log('Logout successfull : ' + error);
      });
  }
  onSelectQuestion() {
    this.sameQuestionMsg = '';
    if (this.secQsForm.value.securityQuestion1 === this.secQsForm.value.securityQuestion2) {
      this.sameQuestionMsg = 'Please choose different question';
    }
  }
}
