import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-confirm-password-dailog',
  templateUrl: './confirm-password-dailog.component.html',
  styleUrls: ['./confirm-password-dailog.component.scss']
})
export class ConfirmPasswordDailogComponent implements OnInit {
  public email: string;
  public passwordMatched: boolean;
  public confirmPasswordForm: FormGroup;
  passErrMsg: string;
  constructor(
    private dialogRef: MatDialogRef<ConfirmPasswordDailogComponent>,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.confirmPasswordForm = this.formBuilder.group({
      password: [''],
    });
  }
  checkPassword() {
    this.passErrMsg = '';
    const formGroup = this.confirmPasswordForm;
    const passwordCtrl = formGroup.controls['password'];
    if (!passwordCtrl.value) {
      passwordCtrl.setErrors({ require: true });
    } else {
      this.passwordMatched = false;
      const checkPassData = {
        password:  this.confirmPasswordForm.get('password').value,
      };
      this.userService.checkPassword(checkPassData).subscribe(
        data => {
          if (data['success']) {
            this.passwordMatched = true;
            this.dialogRef.close(this.passwordMatched);
          } else {
           this.passErrMsg = 'Password incorrect please varify once !';
          }
        },
        err => {
        this.snackBar.open(err.error['message'], '', {
          duration: 5000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        }
      );
    }
  }
}
