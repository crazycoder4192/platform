import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmPasswordDailogComponent } from './confirm-password-dailog.component';

describe('ConfirmPasswordDailogComponent', () => {
  let component: ConfirmPasswordDailogComponent;
  let fixture: ComponentFixture<ConfirmPasswordDailogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmPasswordDailogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPasswordDailogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
