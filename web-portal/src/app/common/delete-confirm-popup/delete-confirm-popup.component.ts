import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-confirm-popup',
  templateUrl: './delete-confirm-popup.component.html',
  styleUrls: ['./delete-confirm-popup.component.scss']
})
export class DeleteConfirmPopupComponent implements OnInit {
  public title: string;
  constructor(
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public entity: any
  ) { }

  ngOnInit() {
    this.title = this.entity.title;
    let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    let userType = currentUserData['usertype'];
    if( userType === 'advertiser') {
      this.isAdvertiser = true;
      this.isPublisher = false;
    } else if (userType === 'publisher') {
      this.isAdvertiser = false;
      this.isPublisher = true;
    }
  }
  onClose() {
    this.dialog.closeAll();
  }
  public isAdvertiser: Boolean;
  public isPublisher: Boolean;

}
