export class PublisherDashboardMetrics {
    _id: { 'pubId': string, 'platformId': string };
    'assetDetails': [
        {
            'assetId': string,
            'assetName': string,
            'snippetId': string,
            'assetType': string
        }
    ];
    count: number;
    openState: boolean;
    platformDetails: [{
        '_id': string,
        'name': string,
        'platformType': string,
        'domainUrl': string,
        'typeOfSite': string,
        'description': string,
        'publisherId': string
    }];
    totalImpressions: number;
    totalEarnings: number;
    totalReach: number;
    increasedImpressions: number;
    increasedEarnings: number;
    increasedReach: number;
    earnings: {total: number, percentage: number};
    impressions: {total: number, percentage: number};
    reach: {total: number, percentage: number};
    Clicks = '-';
    CTR = '-'; /* Clicks dividedby impression i.e., CPC/CPM */
    CPC = '-';
    CPV = '-';
    CPM = '-';
}
