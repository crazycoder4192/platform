export class User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  usertype: string;
  userStatus: string;
  hasSecurityQuestions?: boolean;
  isVisitedDashboard?: boolean;
  createAt: Date;
}

export class PasswordData {
  password: string;
  confirmPassword: string;
  token: string;
}

export class LoginUserDetails {
  status: string;
  message: string;
  token: string;
  usertype: string;
  email: string;
  hasPersonalProfile: boolean;
}

export class AdminUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  userType: string;
  userStatus: string;
  userRole: string;
  roleName: string;
}
