export interface ObjectiveElement {
    _id: string;
    name: string;
    description: string;
}

export interface LocationElement {
    _id: string;
    city: string;
    country: string;
    county: string;
    zipcode: string;
    zipcodeType: string;
    timezone: string;
    stateFullName: string;
    stateCode: string;
    latitude: string;
    longitude: string;
    decommissioned: string;
}

export interface SpecializationElement {
    _id: string;
    Classification: string;
    Grouping: string;
    Specialization: string;
    Taxonomy: string;
    createdAt: string;
    updatedAt: string;
}
