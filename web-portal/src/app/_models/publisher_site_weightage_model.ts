import { Resource } from 'src/app/_models/resource';
export interface PublisherSiteWeightageModel extends Resource {
    weightage: string;
    deleted: boolean;
    created: {
        by: string;
        at: Date
    };
    modified: {
        by: string;
        at: Date
    };
}
