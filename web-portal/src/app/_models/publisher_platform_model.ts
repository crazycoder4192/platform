export class PublisherPlatform {

    constructor(id: String,
        platform_type: String,
        domainUrl: String,
        name: String,
        type_of_site: String,
        description: String,
        keywords: String[],
        exclude_advertiser_types: String[],
        ga_api_key: String,
        ga_analytics_status: String,
        publisherId: String,
        hcp_overall_status: String,
        webrank: Number, isDeleted, isDeactivated, isHCPUploaded: boolean, isHeadScriptAdded: boolean,
        trustedSites: String,
        appType: String) {
        this._id = id;
        this.platformType = platform_type;
        this.domainUrl = domainUrl;
        this.name = name;
        this.typeOfSite = type_of_site;
        this.description = description;
        this.keywords = keywords;
        this.excludeAdvertiserTypes = exclude_advertiser_types;
        this.gaApiKey = ga_api_key;
        this.gaAnalyticsStatus = ga_analytics_status;
        this.publisherId = publisherId;
        this.hcpOverallStatus = hcp_overall_status;
        this.webrank = webrank;
        this.isDeleted = isDeleted;
        this.isDeactivated = isDeactivated;
        this.isHCPUploaded = isHCPUploaded;
        this.isHeadScriptAdded = isHeadScriptAdded;
        this.trustedSites = trustedSites;
        this.appType = appType;
    }

    _id: String;
    platformType: String;
    domainUrl: String;
    name: String;
    typeOfSite: String;
    description: String;
    keywords: String[];
    excludeAdvertiserTypes: String[];
    gaApiKey: String;
    gaAnalyticsStatus: String;
    publisherId: String;
    hcpOverallStatus: String;
    webrank: Number;
    isDeleted: boolean;
    isDeactivated: boolean;
    isHCPUploaded: boolean;
    hcpMappings: {
        mappedHeaders: string[];
        mapping: Object[];
    };
    isHeadScriptAdded: boolean;
    trustedSites: String;
    appType: String;
}
