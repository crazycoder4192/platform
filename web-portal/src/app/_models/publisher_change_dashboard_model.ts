export class PublisherDashboard {
    publisherId: string;
    earnings: { total: number, performanceChange: string };
    impressions: { total: number, performanceChange: string };
    reach: { total: number, performanceChange: string };
    platforms: [{
        platformId: string,
        platformName: string,
        openState: boolean,
        assets: [{
            assetId: string,
            assetName: string,
            assetType: string,
            clicks: number,
            CPC: number,
            CPM: number,
            CPV: number,
            CTR: number,
            earnings: number,
            impressions: number,
            views: number
        }]
    }];
}
