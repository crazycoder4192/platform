export interface CreativeHub {
    _id: string;
    name: string;
    creativeType: string;
    creativeDetails: [
        {
            formatType: string;
            url: string;
            size: string;
            resolution: string;
            dimensions: string;
            styleName: string;
        }
    ];
    sectionName: string;
    status: string;
    deletedUsers: string[];
    advertiserId: string;
}
