export interface NotificationModel {
    _id: string;
    typeDetails: { type: string; typeId: string };
    userDetails: { email:string; isRead: boolean };
    icon: string;
    message: string;
    created: { at: Date; };
    modified: { at: Date; };
    notified: { at: Date; };
  }
