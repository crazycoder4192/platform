export class Banner {
    constructor(_id: String,
        name: String,
        description: String,
        size: String,
        type: String,
        deleted: Boolean,
        uploadStatus: Boolean = false,
        checked: Boolean = false) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.size = size;
        this.deleted = deleted;
        this.checked = checked;
        this.type = type;
        this.uploadStatus = uploadStatus;
    }

    _id: String;
    name: String;
    description: String;
    size: String;
    type: String;
    deleted: Boolean;
    checked: Boolean;
    uploadStatus: Boolean;
}
