export class CreditVoucher {

    constructor(_id: string,
        country: string,
        voucherName: string,
        voucherCode: string,
        amount: string,
        expiryDate: Date,
        active: boolean,
        deleted: boolean) {
        this._id = _id;
        this.country = country;
        this.voucherName = voucherName;
        this.voucherCode = voucherCode;
        this.amount = amount;
        this.expiryDate = expiryDate;
        this.active = active;
        this.deleted = deleted;
    }

    _id: string;
    country: string;
    voucherName: string;
    voucherCode: string;
    amount: string;
    expiryDate: Date;
    active: boolean;
    deleted: boolean;
}
