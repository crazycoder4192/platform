import { Resource } from 'src/app/_models/resource';
export interface PublisherBidRangeModel extends Resource {
    type: string;
    max: number;
    min: number,
    deleted: boolean;
    created: {
        by: string;
        at: Date
    };
    modified: {
        by: string;
        at: Date
    };
}
