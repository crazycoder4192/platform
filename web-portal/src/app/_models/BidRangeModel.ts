import { Resource } from 'src/app/_models/resource';

export interface BidRangeModel extends Resource {
  country: string;
  currencyCode: string;
  currencySymbol: string;
  cpm: {
    min: Number;
    max: Number;
  };
  cpc: {
    min: Number;
    max: Number;
  };
  cpv: {
    min: Number;
    max: Number;
  };
  deleted: boolean;
  created: {
    by: string;
    timeAt: Date
  };
  modified: {
    by: string;
    timeAt: Date
  };
}
