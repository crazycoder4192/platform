import { Resource } from 'src/app/_models/resource';
export interface SchedulePaymentModel extends Resource {
    scheduleDayofMonth: string;
    enableSchedule: boolean;
    deleted: boolean;
    created: {
        by: string;
        at: Date
    };
    modified: {
        by: string;
        at: Date
    };
}
