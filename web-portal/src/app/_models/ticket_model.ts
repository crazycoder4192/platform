export class Ticket {
    _id: string;
    ticketId: number;
    email: string;
    orgName: string;
    userType: string;
    subject: string;
    comment: string;
    status: string;
    severity: string;
    priority: string;
    comments: [{
        commentTime: Date;
        commentBy: string;
        comment: string;
    }];
    latestAssignedUser: {
        email: string;
        at: Date;
    };
    assignedUserHistory: [{
        email: string;
        at: Date;
    }];
    constructor(
        email: string,
        orgName: string,
        userType: string,
        subject: string,
        comment: string,
        status: string,
        severity: string,
        priority: string,
        comments: [{
            commentTime: Date;
            commentBy: string;
            comment: string;
        }],
        latestAssignedUser: {
            email: string;
            at: Date;
        },
        assignedUserHistory: [{
            email: string;
            at: Date;
        }]
    ) {
        this.email = email;
        this.orgName = orgName;
        this.userType = userType;
        this.subject = subject;
        this.comment = comment;
        this.status = status;
        this.severity = severity;
        this.priority = priority;
        this.comments = comments;
        this.latestAssignedUser = latestAssignedUser;
        this.assignedUserHistory = assignedUserHistory;
        }
}
