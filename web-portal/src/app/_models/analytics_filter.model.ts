export interface AnalyticsFilter {
    _id: string;
    startDate: Date;
    endDate: Date;
    reach: string;
    specialization: string;
    gender: string;
    location: string;
    site: string;
    banner: string;
  }