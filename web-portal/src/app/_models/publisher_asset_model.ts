export class PublisherAsset {
    _id: string;
    assetDimensions: string;
    assetSize: number;
    assetUnit: string;
    assetStatus: string;
    assetType: string;
    codeSnippet: string;
    fullUrl: string;
    name: string;
    url: string;
    publisherId: string;
    platformId: string;
    platformType: string;
    fileFormats: [];
    deleted: boolean;
    isActive: boolean;
    isVerified: boolean;
    created: Object;
    bidRange: {
        cpc: {
            min: number;
            max: number;
        },
        cpm: {
            min: number;
            max: number;
        }
    };
    addedSlotScriptOption: string;
}
