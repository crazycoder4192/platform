import { TargetElement } from './advertiser_audience_target';
export interface SubCampaignElement {
    _id: string;
    name: string;
    advertiserId: string;
    brandId: string;
    campaignId: string;
    objective: string;
    creativeType: string;
    isPublished: boolean;
    status: string;
    targetDetails: {
        geoLocation: { locations: any[] };
        demoGraphics: {
            specialization: { areaOfSpecialization: string[]; groupOfSpecialization: any[] };
            gender: string[];
            behaviours: string[]
        };
    };
    audienceTargetIds: string[];
    displayTargetDetails: {
        websiteTypes: string[];
        deviceTypes: string[];
    }
    operationalDetails: {
        startDate?;
        endDate ?;
        currency: string;
        bidSpecifications: string[];
        totalBudget: string;
        bidLimits: { budget: string; type: string; }
    };
    creativeSpecifications: {
        ctaLink: string;
        creativeDetails: { formatType: string;
              url: string;
              weight: string;
              resolution: string;
              size: string;
              platformType: string;
              style: string;
            }[]
    };
}
export enum SubcampaignStatus {
    draft = 'Draft',
    publish = 'Publish',
    underReview = 'Under review',
    error = 'Error',
    approved = 'Active',
    paused = 'Paused',
    completed = 'Completed',
    recentlyCompleted = 'Recently Completed'
}
