export interface TargetElement {
    _id: string;
    name: string;
    brandId: string;
    advertiserId: string;
    isFileUpload: boolean;
    createAudienceTargetDetails: {
        geoLocation: { isInclude: any; locations: any[] };
        demoGraphics: {
            specialization: { isInclude: any; areaOfSpecialization: string[]; groupOfSpecialization: any[] };
            gender: string[];
            behaviours: string[]
        };
        websiteTypes: string[];
        deviceTypes: string[];
        keywords: string[]
    };
    uploadAudienceTargetDetails: any;
}
