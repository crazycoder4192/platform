import { Resource } from 'src/app/_models/resource';
export interface WeightageByValidatedHcpModel extends Resource {
    weightage: string;
    max: number;
    min: number,
    deleted: boolean;
    created: {
        by: string;
        at: Date
    };
    modified: {
        by: string;
        at: Date
    };
}
