export interface ToolTip {
    _id: string;
    key: string;
    value: string;
    created: any;
}