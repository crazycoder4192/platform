export class PaymentMethod {
    public _id: string;
    public billingCountry: string;
    public billingCurrency: string;
    public cardNumber: string;
    public nameOnCard: string;
    public expiryDate: Date;
    public cvv: string;
    public brands: [];
    public amount: string;
    public deleted: boolean;
    public customerId: string;
    public customerProfileId: string;
    public cardType: string;
    public hasBillingAddress: boolean;
    public isPhNumberVerified: boolean;
}
