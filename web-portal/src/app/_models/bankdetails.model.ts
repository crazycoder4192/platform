export interface Bankdetails {
  bank_name: string;
  account_holder_name: string;
  account_number: string;
  swift_code: string;
  tax_number: string;
  publisher_id: number;
  platforms: [];
  created: Date;
  modified: Date;
}


