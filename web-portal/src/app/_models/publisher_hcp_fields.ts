export class PublisherHCPFields {
    identifier: string;
    fieldName: string;
    isActive: boolean;
    isMandatory: boolean;
}
