export class Brand {

    constructor(_id: string,
        brandName: string,
        brandType: string,
        clientName: string,
        clientType: string,
        advertiser: string,
        loggedInEmail: string,
        created: any) {
        this._id = _id;
        this.brandName = brandName;
        this.brandType = brandType;
        this.clientName = clientName;
        this.clientType = clientType;
        this.advertiser = advertiser;
        this.loggedInEmail = loggedInEmail;
        this.created = created;
    }

    _id: string;
    brandName: string;
    brandType: string;
    clientName: string;
    clientType: string;
    advertiser: string;
    brandStatus: boolean;
    loggedInEmail: string;
    created: any;
}
