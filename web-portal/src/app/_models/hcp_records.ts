export class HCPRecord {
    NPINumber: string;
    firstName: string;
    lastName: string;
    emailId: string;
    phoneNumber: string;
    specialty: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    country: string;
    zipcode: string;
    adsConsent: string;
    gender: string;
    registrationDate: string;
    organizationName: string;
    isValidated: boolean;
    isValid: boolean;
    errMessege: string;
}
