import { Resource } from 'src/app/_models/resource';

export interface CountryTaxModel extends Resource {
  country: string;
  address: {
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    zipcode: string;
  };
  typeOfTax: string;
  percentageOfTax: string;
  taxRegistrationNumber: string;
}
