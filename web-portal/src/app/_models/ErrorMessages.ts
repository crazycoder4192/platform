export const errorMessages: { [key: string]: string } = {
  fullName: 'Full name must be between 1 and 128 characters',
  email: 'Email must be a valid email address (username@domain)',
  confirmEmail: 'Email addresses must match',
  password: 'Password must be between 7 and 15 characters, and contain at least one number and special character',
  confirmPassword: 'Passwords must match'
};

export const requiredErrorMessages: { [key: string]: string } = {
  email: 'Email is required',
  addressLine1: 'Address Line 1 is required',
  addressLine2: 'Address Line 2 is required',
  city: 'City is required',
  state: 'State is required',
  zipcode: 'Zipcode is required',
  country: 'Country is required',
  password: 'Password is required',
  confirmPassword: 'Passwords must match'
};

