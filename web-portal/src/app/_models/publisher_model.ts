import { Address } from './publisher_address_model';
export class Publisher {
    constructor(
    _id: string,
    firstName: string,
    lastName: string,
    phoneNumber: string,
    acceptTerms: boolean,
    organization: string,
    address: Address,
    ) {
      this._id = _id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.acceptTerms = acceptTerms;
      this.organizationName = organization;
      this.phoneNumber = phoneNumber;
      this.address = address;
    }

    _id: string;
    firstName: string;
    lastName: string;
    acceptTerms: boolean;
    organizationName: string;
    phoneNumber: string;
    address: Address;
    isPhNumberVerified: boolean;
  }
