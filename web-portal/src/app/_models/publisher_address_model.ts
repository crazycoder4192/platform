export class Address {
    constructor(
        addressLine1: string,
        addressLine2: string,
        city: string,
        country: string,
        state: string,
        zipcode: string
        ) {
          this.addressLine1 = addressLine1;
          this.addressLine2 = addressLine2;
          this.city = city;
          this.country = country;
          this.state = state;
          this.zipcode = zipcode;
        }
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: string;
    state: string;
    zipcode: string;
}
