import { Resource } from 'src/app/_models/resource';

export interface CountryCurrenciesModel extends Resource {
  countryName: string;
  countryISO3Code: string;
  countryISO2Code: string;
  currencyCode: string;
  currencyName: string;
  symbol: string;
  active: boolean;
  deleted: boolean;
  created: {
    by: string;
    timeAt: Date
  };
  modified: {
    by: string;
    timeAt: Date
  };
}
