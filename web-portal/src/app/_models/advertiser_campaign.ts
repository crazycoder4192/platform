export interface CampaignElement {
    _id: string;
    advertiserId: string;
    brandId: string;
    name: string;
    page: string;
    created: any;
}
