export class Advertiser {

    constructor(_id: string,
        businessName: string,
        advertiserType: string,
        organizationType: string,
        acceptTerms: boolean,
        addressLine1: string,
        addressLine2: string,
        city: string,
        state: string,
        zipcode: string,
        country: string,
        email: string,
        phoneNumber: string,
        isPhNumberVerified: boolean) {
        this._id = _id;
        this.businessName = businessName;
        this.advertiserType = advertiserType;
        this.organizationType = organizationType;
        this.acceptTerms = acceptTerms;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.country = country;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.isPhNumberVerified = isPhNumberVerified;
    }
    _id: string;
    businessName: string;
    advertiserType: string;
    organizationType: string;
    acceptTerms: boolean;
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    zipcode: string;
    country: string;
    email: string;
    phoneNumber: string;
    isPhNumberVerified: boolean;
}
