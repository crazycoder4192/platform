export class PublisherHCPDetails {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    address: string;
}
