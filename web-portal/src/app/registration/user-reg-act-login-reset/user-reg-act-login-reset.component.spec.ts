import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRegActLoginResetComponent } from './user-reg-act-login-reset.component';

describe('UserRegActLoginResetComponent', () => {
  let component: UserRegActLoginResetComponent;
  let fixture: ComponentFixture<UserRegActLoginResetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRegActLoginResetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRegActLoginResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
