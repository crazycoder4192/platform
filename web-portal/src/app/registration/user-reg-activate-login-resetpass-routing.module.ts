import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../_guards/auth-guard.guard';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { LoginComponent } from './login/login.component';
import { ActivateuserComponent } from './activateuser/activateuser.component';
import { RegisterComponent } from './register/register.component';
import { RegistrationcnfrmemailComponent } from './register/registrationcnfrmemail/registrationcnfrmemail.component';
import { UserRegActLoginResetComponent } from './user-reg-act-login-reset/user-reg-act-login-reset.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ForgotpasswordcnfrmemailComponent } from './forgotpassword/forgotpasswordcnfrmemail/forgotpasswordcnfrmemail.component';
import { PasswordExpiryComponent } from './password-expiry/password-expiry.component';
import { RegisterViaCodeComponent } from './register-via-code/register-via-code.component';
import { unsubscribeUserComponent } from '../../app/registration/unsubscribeuser/unsubscribeuser.component'

const routes: Routes = [
  {
    path: '', component: UserRegActLoginResetComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'register-via-code', component: RegisterViaCodeComponent },
          { path: 'register/:invitationcode', component: RegisterComponent },
          { path: 'verifytoken', component: ActivateuserComponent },
          { path: 'unsubscribeUser', component: unsubscribeUserComponent },
          { path: 'login', component: LoginComponent },
          { path: 'forgotpassword', component: ForgotpasswordComponent },
          { path: 'resetpassword/:id', component: ResetpasswordComponent },
          { path: 'passwordexpiry/:id', component: PasswordExpiryComponent }
        ]
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRegActLoginResetPassRoutingModule { }
