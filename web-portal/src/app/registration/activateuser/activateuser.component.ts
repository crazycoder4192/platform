import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CheckPasswords, ValidatePassword } from 'src/app/common/validators/validator';
import { UserService } from '../../_services/user.service';
import { CaptchaCompComponent } from 'src/app/common/captcha-comp/captcha-comp.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { DISABLED } from '@angular/forms/src/model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-activateuser',
  templateUrl: './activateuser.component.html',
  styleUrls: ['./activateuser.component.scss']
})
export class ActivateuserComponent implements OnInit, OnDestroy {
  @ViewChild(CaptchaCompComponent) child;

  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  userType = 'advertiser';
  id: any;
  tokenType: any;
  activateUserForm: FormGroup;
  profileSetUp = false;
  submitted = false;
  errorMessage = '';
  formErrorMsg = '';
  isTokenValid = false;
  pwdHide = true;
  captchaValue: String = '';
  barLabel: String = 'Password Strength:';
  formSubmitSuccess = false;
  activateUser: Subscription;

  constructor(public router: Router, private userService: UserService,
    private spinner: SpinnerService,  private _routeParams: ActivatedRoute, private formBuilder: FormBuilder) {
      this.spinner.showSpinner.next(true);
      this._routeParams.queryParams.subscribe(params => {
        this.id = params['token'];
        this.tokenType = params['tokenType'];
      });

  }

  ngOnInit() {
    this.verifyToken(this.id, this.tokenType);
  }

  verifyToken(id, tokenType) {
    this.userService
      .activateUser(id, tokenType)
      .pipe(first())
      .subscribe(
        data => {
          this.isTokenValid = true;
          if (this.tokenType === 'addUser') {
            this.createActivateUserForm(data['email'], data['userType']);
          }
          this.spinner.showSpinner.next(false);
        },
        error => {
          this.isTokenValid = false;
          this.errorMessage = error.error['message'];
          this.spinner.showSpinner.next(false);
        }
      );
  }

  createActivateUserForm(email: string, userType: string) {
    this.activateUserForm = this.formBuilder.group({
      email: [email, Validators.compose([Validators.required, Validators.pattern(this.emailPattern), Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      captchaInput: ['', Validators.required],
      userType: [userType]
    });
    this.activateUser = this.activateUserForm.valueChanges.subscribe(val => {
      this.formErrorMsg = '';
      this.submitted = false;
    });
  }

  ngOnDestroy() {
    this.activateUser.unsubscribe();
  }

  getCaptchaValue(value: String) {
    this.captchaValue = value;
  }

  updatePwd() {
    this.submitted = true;
    if (this.activateUserForm.invalid) {
      this.child.drawCaptcha();
      return;
    }
    this.spinner.showSpinner.next(true);
    this.userService.updateAddUserPassword({
      'token' : this.id,
      'email': this.activateUserForm.value.email,
      'password': this.activateUserForm.value.password,
      'userType': this.activateUserForm.value.userType
    })
    .pipe(first())
    .subscribe(
    data => {
      this.formSubmitSuccess = true;
      this.spinner.showSpinner.next(false);
    },
    error => {
      this.formSubmitSuccess = false;
      this.spinner.showSpinner.next(false);
      this.formErrorMsg = error.error['message'];
    });
  }
}
