import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, map, startWith } from 'rxjs/operators';
import { CheckPasswords, ValidatePassword } from 'src/app/common/validators/validator';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { UserService } from '../../_services/user.service';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit, OnDestroy {

  resetForm: FormGroup;
  isTokenValid = false;
  submitted = false;
  message = '';
  errorMessage = '';
  id = '';
  pwdHide = true;
  confirmPwdHide = true;
  hasSecurityQuestions = false;
  barLabel: String = 'Password Strength:';
  formSubmitSuccess = false;
  securityQuestionObservable: Subscription;

  security_questions = [
    'What school did you attend for fifth grade?',
    'What was the name of your first pet?',
    'What was your dream job as a child?',
    'What is the name of your favorite childhood friend?',
    'What is your oldest cousin\'s first and last name?',
    'In what city did you meet your spouse/significant other?',
    'In what city was your first job?'
  ];

  security_questions2 = [
    'What school did you attend for fifth grade?',
    'What was the name of your first pet?',
    'What was your dream job as a child?',
    'What is the name of your favorite childhood friend?',
    'What is your oldest cousin\'s first and last name?',
    'In what city did you meet your spouse/significant other?',
    'In what city was your first job?'
  ];

  security_questions3 = [
    'What school did you attend for fifth grade?',
    'What was the name of your first pet?',
    'What was your dream job as a child?',
    'What is the name of your favorite childhood friend?',
    'What is your oldest cousin\'s first and last name?',
    'In what city did you meet your spouse/significant other?',
    'In what city was your first job?'
  ];

  constructor(private formBuilder: FormBuilder, private userService: UserService,
     public router: Router, private spinner: SpinnerService, private _routeParams: ActivatedRoute) {
      this._routeParams.params.subscribe(params => {
        this.id = params['id'];
      });
      this.onSubmit();
  }

  ngOnInit() {
      this.resetForm = this.formBuilder.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      confirmPassword: ['', Validators.required],
      securityQuestion1: ['',  Validators.required],
      securityAnswer1: ['', Validators.required],
      securityQuestion2: ['', Validators.required],
      securityAnswer2: ['',  Validators.required]
      }, {validator: CheckPasswords('password', 'confirmPassword')});

    this.securityQuestionObservable = this.resetForm.controls['securityQuestion1'].valueChanges
        .pipe().subscribe(
          value => {
            this.security_questions2 = this.security_questions.slice(0);
            this.security_questions2.reverse();
            const index = this.security_questions2.indexOf(value);
            this.security_questions2.splice(index, 1);
          }
        );
  }

  ngOnDestroy() {
    this.securityQuestionObservable.unsubscribe();
  }

  get f() { return this.resetForm.controls; }

  onSubmit() {
    this.userService.resetPwd(this.id)
    .pipe(first())
    .subscribe(
      data => {
          this.isTokenValid = true;
          this.message = data['message'];
          this.hasSecurityQuestions = data['hasSecurityQuestions'];
      },
      error => {
        this.isTokenValid = false;
        this.errorMessage = error.error['message'];
      }
    );
  }

  updatePwd() {
    this.submitted = true;
    this.spinner.showSpinner.next(true);
    if (this.hasSecurityQuestions) {
      this.resetForm.get('securityQuestion1').clearValidators();
      this.resetForm.get('securityQuestion1').updateValueAndValidity();
      this.resetForm.get('securityAnswer1').clearValidators();
      this.resetForm.get('securityAnswer1').updateValueAndValidity();
      this.resetForm.get('securityQuestion2').clearValidators();
      this.resetForm.get('securityQuestion2').updateValueAndValidity();
      this.resetForm.get('securityAnswer2').clearValidators();
      this.resetForm.get('securityAnswer2').updateValueAndValidity();
    }
    if (this.resetForm.invalid) {
      this.spinner.showSpinner.next(false);
      return;
    }
    this.userService.updatePwd({
      'token' : this.id,
      'password': this.resetForm.value.password,
      'confirmPassword': this.resetForm.value.confirmPassword,
      'securityQuestion1': !this.hasSecurityQuestions ? this.resetForm.value.securityQuestion1 : '',
      'securityAnswer1': !this.hasSecurityQuestions ? this.resetForm.value.securityAnswer1 : '',
      'securityQuestion2': !this.hasSecurityQuestions ? this.resetForm.value.securityQuestion2 : '',
      'securityAnswer2': !this.hasSecurityQuestions ? this.resetForm.value.securityAnswer2 : '',
    })
    .pipe(first())
    .subscribe(
    data => {
      this.formSubmitSuccess = true;
      this.spinner.showSpinner.next(false);
    },
    error => {
      this.formSubmitSuccess = false;
      this.errorMessage = error.error['message'];
      this.spinner.showSpinner.next(false);
    });
  }

}
