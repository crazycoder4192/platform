import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { UserRegActLoginResetPassRoutingModule } from './user-reg-activate-login-resetpass-routing.module';

import { UserRegActLoginResetComponent } from './user-reg-act-login-reset/user-reg-act-login-reset.component';
import { ActivateuserComponent } from './activateuser/activateuser.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { CaptchaCompComponent } from '../common/captcha-comp/captcha-comp.component';
import { CommonModulesModule } from '../common/common-modules/common-modules.module';
import { MaterialModule } from '../common/common-modules/material_module';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { PasswordExpiryComponent } from './password-expiry/password-expiry.component';
import { ForgotpasswordcnfrmemailComponent } from './forgotpassword/forgotpasswordcnfrmemail/forgotpasswordcnfrmemail.component';
import { RegistrationcnfrmemailComponent } from './register/registrationcnfrmemail/registrationcnfrmemail.component';
import { RegisterViaCodeComponent } from './register-via-code/register-via-code.component';
import { unsubscribeUserComponent } from '../../app/registration/unsubscribeuser/unsubscribeuser.component';

@NgModule({
  declarations: [
    UserRegActLoginResetComponent,
    LoginComponent,
    RegisterViaCodeComponent,
    RegisterComponent,
    ActivateuserComponent,
    unsubscribeUserComponent,
    CaptchaCompComponent,
    ResetpasswordComponent,
    ForgotpasswordComponent,
    PasswordExpiryComponent,
    ForgotpasswordcnfrmemailComponent,
    RegistrationcnfrmemailComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    UserRegActLoginResetPassRoutingModule,
    CommonModulesModule,
    MaterialModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class UserRegActLoginResetPassModule { }
