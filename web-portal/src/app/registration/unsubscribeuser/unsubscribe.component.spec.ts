import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { unsubscribeUserComponent } from './unsubscribeuser.component';

describe('unsubscribeUserComponent', () => {
  let component: unsubscribeUserComponent;
  let fixture: ComponentFixture<unsubscribeUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ unsubscribeUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(unsubscribeUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
