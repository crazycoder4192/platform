import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { UserService } from '../../_services/user.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { Subscription, } from 'rxjs';

@Component({
  selector: 'app-unsubscribeuser',
  templateUrl: './unsubscribeuser.component.html',
  styleUrls: ['./unsubscribeuser.component.scss']
})
export class unsubscribeUserComponent implements OnInit {

  @ViewChild('f') myNgForm;
  id: any;
  tokenType: any;
  errorMessage = '';
  message;
  isTokenValid = false;
  userEmail;
  unSubscribeUser: Subscription;
  feedbacks=['I no longer want to recieve these emails','The emails are inappropriate',
  'The emails are spam and should be reported', 'I never signed up for this mailing list','Other'];
  SignupForm: FormGroup;
  status;

  constructor(public router: Router, private userService: UserService,
    private spinner: SpinnerService,  private _routeParams: ActivatedRoute,) {
      this.spinner.showSpinner.next(true);
      this._routeParams.queryParams.subscribe(params => {
        this.id = params['token'];
        this.tokenType = params['tokenType'];
      });

  }

  ngOnInit() {
    this.unsubscribeUser(this.id);
    this.SignupForm = new FormGroup({
      'feedback':new FormControl('Other')
    });

    this.SignupForm.setValue({
      'feedback':'Other'
    });
  }

  onSubmit(){
    this.userService.submittedFeedback( this.id, 'unsubscribe' , this.SignupForm.value.feedback).subscribe(
      res => {
        console.log('feedback response!');
      }
    );
    this.router.navigate(['/login']);
  }

  unsubscribeUser(id) {
    this.userService
      .unsubscribeUser(id)
      .pipe(first())
      .subscribe(
        data => {
          this.isTokenValid = true
          this.spinner.showSpinner.next(false);
          this.userEmail = data['email'];
        },
        error => {
          this.isTokenValid = false;
          this.errorMessage = error.error['message'];
          this.spinner.showSpinner.next(false);
        }
      );
  }
}
