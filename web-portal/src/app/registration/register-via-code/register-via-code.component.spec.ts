import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterViaCodeComponent } from './register-via-code.component';

describe('RegisterViaCodeComponent', () => {
  let component: RegisterViaCodeComponent;
  let fixture: ComponentFixture<RegisterViaCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterViaCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterViaCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
