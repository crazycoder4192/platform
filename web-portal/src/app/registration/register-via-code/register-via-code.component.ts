import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../_services/user.service';
import { first } from 'rxjs/operators';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatSnackBar } from '@angular/material';
import { debounceTime } from 'rxjs/operators';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-register-via-code',
  templateUrl: './register-via-code.component.html',
  styleUrls: ['./register-via-code.component.scss']
})
export class RegisterViaCodeComponent implements OnInit {

  registerViaCodeForm: FormGroup;
  validCode = 0;
  emailAdded = 0;
  submitted = false;
  errorMsg = '';
  successMessage = '';
  emailPattern = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';

  constructor(private formBuilder: FormBuilder,
    public router: Router, private spinner: SpinnerService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private amplitudeService: AmplitudeService
  ) { }

  ngOnInit() {
    this.amplitudeService.logEvent('onboarding - click invitation link', null);
    this.registerViaCodeForm = this.formBuilder.group({
      code: [''],
      requestingEmail: ['', Validators.compose([Validators.pattern(this.emailPattern), Validators.email])]
    });

    this.registerViaCodeForm.valueChanges.subscribe(val => {
      this.errorMsg = '';
      this.submitted = false;
    });

    this.validCode = 0;
    this.emailAdded = 0;

    this.registerViaCodeForm.controls.code.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(data => this.checkInvitationCode(data));
  }

  checkInvitationCode(code: any) {
    console.log('Check invitation code invoked');
    this.spinner.showSpinner.next(true);
    this.userService.validateCode(this.registerViaCodeForm.value['code'])
    .pipe(first())
    .subscribe(
      data => {
        this.validCode = 1;
        this.spinner.showSpinner.next(false);
        this.toRegister();
      },
      error => {
        this.validCode = 2;
        console.log(error);
        if (error.error['status'] !== 409) {
          this.validCode = 3;
          this.errorMsg = error.error['message'];
        }
        this.spinner.showSpinner.next(false);
      }
    );
  }

  get f() { return this.registerViaCodeForm.controls; }

  onSubmit() {
    this.submitted = true;
    //convert email in lowercase
    this.registerViaCodeForm.get('requestingEmail').setValue(this.registerViaCodeForm.get('requestingEmail').value.toLowerCase());
    if (this.registerViaCodeForm.invalid) {
      return;
    }
    console.log(this.registerViaCodeForm.value['requestingEmail']);
    if (this.registerViaCodeForm.value['requestingEmail'] && this.registerViaCodeForm.value['requestingEmail'].length > 0) {
      this.spinner.showSpinner.next(true);
      this.userService.saveRequestingEmail(this.registerViaCodeForm.value['requestingEmail'])
      .pipe(first())
      .subscribe(
      data => {
        this.emailAdded = 1;
        this.amplitudeService.setUserProperties(this.registerViaCodeForm.value['requestingEmail'], null);
        this.amplitudeService.logEvent('lead_generation - reg via code email drop', null);
        this.spinner.showSpinner.next(false);
      },
      error => {
        this.emailAdded = 3;
        this.errorMsg = error.error['message'];
        this.spinner.showSpinner.next(false);
      });
    }
  }

  toRegister() {
    if (this.registerViaCodeForm.value['code']) {  
      this.amplitudeService.logEvent('onboarding - enter code', null);
      this.router.navigate(['/register/' + this.registerViaCodeForm.value['code']]);
    }
  }
}
