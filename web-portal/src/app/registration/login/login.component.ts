import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../_services/user.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  loginForm: FormGroup;
  submitted = false;
  message = '';
  errorMsg = '';
  pwdHide = true;

  constructor(private formBuilder: FormBuilder, private userService: UserService,
    private authService: AuthenticationService,
    public router: Router, private spinner: SpinnerService, private amplitudeService: AmplitudeService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailPattern), Validators.email])],
      password: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.errorMsg = '';
    this.message = '';
    this.loginForm.get('email').setValue(this.loginForm.get('email').value.toLowerCase());
    if (this.loginForm.invalid) {
      return;
    }
    this.spinner.showSpinner.next(true);
    this.authService.login(this.loginForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.message = data['message'];
          this.amplitudeService.setUserProperties(data['email'], data['usertype']);
          this.amplitudeService.logEvent('Logged in', null);
          if (data['status'] === false && data['message'] === 'password_expiry_reset') {
            this.spinner.showSpinner.next(false);
            this.router.navigate(['/passwordexpiry/' + data['id']]);
          }
        },
        error => {
          this.spinner.showSpinner.next(false);
          this.errorMsg = error.error['message'];
          this.submitted = false;
        }
      );
  }

}
