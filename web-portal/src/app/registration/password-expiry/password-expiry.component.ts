import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { ValidatePassword, CheckPasswords } from 'src/app/common/validators/validator';
import { first } from 'rxjs/operators';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-password-expiry',
  templateUrl: './password-expiry.component.html',
  styleUrls: ['./password-expiry.component.scss']
})
export class PasswordExpiryComponent implements OnInit {
  resetForm: FormGroup;
  loading = false;
  submitted = false;
  message = '';
  status = false;
  obj = {};
  id = '';
  hide = true;
  confirmHide = true;

  constructor(private formBuilder: FormBuilder, private userService: UserService,
     private authService: AuthenticationService,
     public router: Router,
     private _routeParams: ActivatedRoute) {
      this._routeParams.params.subscribe(params => {
        this.id = params['id'];
      });
  }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      currentpassword: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
       confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])]
    },
    {
      validator: CheckPasswords('password', 'confirmPassword')
    });
  }
  updatePwd() {
    this.submitted = true;
    this.obj['currentPwd'] =  this.resetForm.value['currentpassword'];
    this.obj['password'] =  this.resetForm.value['password'];
    this.obj['confirmPassword'] =  this.resetForm.value['confirmPassword'];
    this.obj['id'] =  this.id;
    if (this.resetForm.invalid) {
        return;
    }
    this.loading = true;
    this.userService.passwordExpiryReset(this.obj)
    .pipe(first())
    .subscribe(
    data => {
      this.message = data['message'];
      this.status = data['status'];
      if (data['status'] === true) {
        this.router.navigate(['/login']);
      }
    },
    error => {
        this.loading = false;
        this.message = error.error['message'];
        this.status = false;
    });
  }
}
