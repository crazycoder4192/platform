import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotpasswordcnfrmemailComponent } from './forgotpasswordcnfrmemail.component';

describe('ForgotpasswordcnfrmemailComponent', () => {
  let component: ForgotpasswordcnfrmemailComponent;
  let fixture: ComponentFixture<ForgotpasswordcnfrmemailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotpasswordcnfrmemailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotpasswordcnfrmemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
