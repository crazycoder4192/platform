import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpasswordcnfrmemail',
  templateUrl: './forgotpasswordcnfrmemail.component.html',
  styleUrls: ['./forgotpasswordcnfrmemail.component.scss']
})
export class ForgotpasswordcnfrmemailComponent implements OnInit {

  userType = 'advertiser';
  publisherURL = '/publisher/forgotpasswordconfirmemail';
  constructor(public router: Router) {
    if (this.router.url === this.publisherURL) {
      this.userType = 'publisher';
    }
  }

  ngOnInit() {
  }

}
