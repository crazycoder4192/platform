import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, RequiredValidator } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from 'src/app/_services/user.service';
import { ValidatePassword, MustMatch } from '../../common/validators/validator';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {

  forgotForm: FormGroup;
  submitted = false;
  status = false;
  forgotPwdSuccess = false;
  errorMsg = '';
  message = '';
  questions: string[] = [];
  hasSecurityQuestions = false;

  constructor(private userService: UserService, private formBuilder: FormBuilder,
    private spinner: SpinnerService, public router: Router) { }

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      securityQuestion: [''],
      securityQuesAnswer: ['']
    });
    this.forgotForm.valueChanges.subscribe(val => {
      this.errorMsg = '';
      this.submitted = false;
    });
  }

  get f() { return this.forgotForm.controls; }

  onSubmit() {
    this.forgotForm.get('email').setValue(this.forgotForm.get('email').value.toLowerCase());
    this.submitted = true;
    if (!this.hasSecurityQuestions) {
      this.forgotForm.get('securityQuestion').clearValidators();
      this.forgotForm.get('securityQuestion').setValidators(null);
      this.forgotForm.get('securityQuesAnswer').clearValidators();
      this.forgotForm.get('securityQuesAnswer').setValidators(null);
    } else {
      this.forgotForm.get('securityQuestion').setValidators([Validators.required]);
      this.forgotForm.get('securityQuesAnswer').setValidators([Validators.required]);
    }
    this.forgotForm.get('securityQuestion').updateValueAndValidity();
    this.forgotForm.get('securityQuesAnswer').updateValueAndValidity();
    if (this.forgotForm.invalid) {
      return;
    }
    this.spinner.showSpinner.next(true);
    this.userService.forgotPwd(this.forgotForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.message = data['message'];
          this.status = data['success'];
          this.hasSecurityQuestions = data['hasSecurityQuestions'];
          if (this.hasSecurityQuestions) {
            this.questions = data['questions'];
            this.forgotPwdSuccess = false;
          } else {
            this.forgotPwdSuccess = true;
          }
          this.spinner.showSpinner.next(false);
        },
        error => {
          this.errorMsg = error.error['message'];
          this.status = false;
          this.forgotPwdSuccess = false;
          this.spinner.showSpinner.next(false);
        }
      );
  }

}
