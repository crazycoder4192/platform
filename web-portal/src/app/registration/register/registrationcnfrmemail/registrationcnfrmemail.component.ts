import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrationcnfrmemail',
  templateUrl: './registrationcnfrmemail.component.html',
  styleUrls: ['./registrationcnfrmemail.component.scss']
})
export class RegistrationcnfrmemailComponent implements OnInit {

  advertiserURL = '/register/confirmemail';
  publisherURL = '/publisher/register/confirmemail';
  constructor(public router: Router) { }

  ngOnInit() {
  }

}
