import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationcnfrmemailComponent } from './registrationcnfrmemail.component';

describe('RegistrationcnfrmemailComponent', () => {
  let component: RegistrationcnfrmemailComponent;
  let fixture: ComponentFixture<RegistrationcnfrmemailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationcnfrmemailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationcnfrmemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
