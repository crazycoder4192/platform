import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services/user.service';
import { first } from 'rxjs/operators';
import { ValidatePassword } from '../../common/validators/validator';

import { CaptchaCompComponent } from '../../common/captcha-comp/captcha-comp.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatSnackBar } from '@angular/material';
import { AdvertiserUserService } from 'src/app/_services/advertiser/advertiser-user.service';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild(CaptchaCompComponent) child;

  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  registerForm: FormGroup;
  submitted = false;
  captchaValue: String = '';
  barLabel: String = 'Password Strength:';
  errorMsg = '';
  passwordMismatchMsg = '';
  captchaMismatchMsg = '';
  pwdHide = true;
  registrationSuccess = false;
  successMessage = '';
  invitationCode = '';
  iconSelected: boolean;

  constructor(private formBuilder: FormBuilder,
    public router: Router, private spinner: SpinnerService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private advertiserUserService: AdvertiserUserService,
    private _routeParams: ActivatedRoute,
    private amplitudeService: AmplitudeService
  ) {
    this._routeParams.params.subscribe(params => {
      this.invitationCode = params['invitationcode'];
    });
  }

  ngOnInit() {
    this.iconSelected = false;
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailPattern), Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      confirmPassword: ['', Validators.compose([Validators.required])],
      captchaInput: ['', Validators.required],
      userType: ['', Validators.required]
    });
    this.registerForm.valueChanges.subscribe(val => {
      this.errorMsg = '';
      this.passwordMismatchMsg = '';
      this.captchaMismatchMsg = '';
      this.submitted = false;
    });

  }

  get f() { return this.registerForm.controls; }

  getCaptchaValue(value: String) {
    this.captchaValue = value;
  }

  onSubmit() {
    this.registerForm.get('email').setValue(this.registerForm.get('email').value.toLowerCase());
    this.submitted = true;
    if (this.registerForm.invalid && !this.iconSelected) {
      this.child.drawCaptcha();
      return;
    }

    if (this.registerForm.value['password'] !== this.registerForm.value['confirmPassword']) {
      this.passwordMismatchMsg = 'Password and confirm password does not match';
      this.child.drawCaptcha();
      return;
    }

    if (this.captchaValue !== this.registerForm.controls['captchaInput'].value) {
      this.captchaMismatchMsg = 'Incorrect captcha';
      this.child.drawCaptcha();
      return;
    }
    this.spinner.showSpinner.next(true);
    const payload: any = this.registerForm.value;
    payload.invitationCode = this.invitationCode;

    // TODO: Save this value in DB in later stage
    if (payload.userType === 'media-agency' || payload.userType === 'brand') {
      payload.userType = 'advertiser';
    }
    this.userService.register(payload)
      .pipe(first())
      .subscribe(
        data => {
          this.amplitudeService.setUserProperties(this.registerForm.get('email').value.toLowerCase(), payload.userType);
          this.amplitudeService.logEvent('onboarding - user clicks on register', null);
          this.registrationSuccess = true;
          this.successMessage = data['message'];
          this.spinner.showSpinner.next(false);
        },
        error => {
          this.registrationSuccess = false;
          this.child.drawCaptcha();
          this.spinner.showSpinner.next(false);
          this.errorMsg = error.error['message'];
        }
      );
  }

  reGenerateTokenOfUser(email: string) {
    const postActiveUserData = {
      email: email
    };
    this.spinner.showSpinner.next(true);
    this.userService.reGenerateToken(postActiveUserData).subscribe(
      data => {
        this.spinner.showSpinner.next(false);
        if (data['success']) {
          this.snackBar.open(data['message'], '', {
            duration: 5000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
        } else if (!data['success']) {
          this.snackBar.open(data['message'], '', {
            duration: 5000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
        }
       },
       err => {
        this.snackBar.open(err.error['message'], '', {
          duration: 5000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
       }
     );
  }
  selectIcon() {
    this.iconSelected = true;
  }

}
