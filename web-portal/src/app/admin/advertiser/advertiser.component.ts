import { Component, OnInit } from '@angular/core';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-advertiser',
  templateUrl: './advertiser.component.html',
  styleUrls: ['./advertiser.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AdvertiserComponent implements OnInit {
  show = false;
  dataSource = [];
  displayedColumns: string[] = ['Company Name', 'Type of user', 'Member since', 'Brands',
    'Active campaigns', 'Spends', 'Accounts Linked'];
  filteredCampaigns: any[];
  expandedElement: null;
  typeOfUser = 'Filter by type';
  brand = 'Filter by brand';
  sDate = new Date();
  campaign: any;

  constructor(private campaignService: CampaignService, private snackBar: MatSnackBar, private dialog: MatDialog) { }

  ngOnInit() {
    this.renderListView();
  }
  toggleSearch() {
    this.show = !this.show;
  }
  searchSubCampaign(typeOfUser, brand, startDate) {
    this.filteredCampaigns = this.dataSource;
    this.filteredCampaigns = this.dataSource.filter(item => {
      if (typeOfUser && typeOfUser !== 'Filter by type' && !item.advertiserType.includes(typeOfUser)) {
        return false;
      }
      const sdate = new Date(startDate);
      const sstartDate = (sdate.getMonth() + 1) + '-' + sdate.getDate() + '-' + sdate.getFullYear();

      const dbendDate = new Date(item.created.at);
      const dbcompareDate = (dbendDate.getMonth() + 1) + '-' + dbendDate.getDate() + '-' + dbendDate.getFullYear();

      if (startDate && dbcompareDate > sstartDate) {
        return false;
      }
      return true;
    });
    return this.filteredCampaigns;
  }
  searchCampaign(name) {
    return name;
  }
  renderListView() {
    this.campaignService.getProviders().subscribe(
      resposne => {
        this.dataSource = <[]>resposne;
        this.filteredCampaigns = this.dataSource;
        console.log(resposne);
      },
      err => {
        console.log('Error! getAllCampaigns : ' + err);
      });
  }
}
