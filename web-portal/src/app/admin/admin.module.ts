import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { AdvertiserComponent } from './advertiser/advertiser.component';
import { PublisherComponent } from './publisher/publisher.component';
import { PublisherHcpScriptComponent } from './publisher/publisher-hcp-script/publisher-hcp-script.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { SupportComponent } from './support/support.component';
import { PaymentsComponent } from './payments/payments.component';
import { SettingsComponent } from './settings/settings.component';
import { CreditlineComponent } from './creditline/creditline.component';
import { PopupEditSMTPComponent } from './configuration/popup-edit-smtp/popup-edit-smtp.component';
import { CommonModulesModule } from '../common/common-modules/common-modules.module';
import { UseRolePermissionsComponent } from './configuration/use-role-permissions/use-role-permissions.component';
import { CountrytaxesComponent } from './settings/countrytaxes/countrytaxes.component';
import { BidrangesComponent } from './settings/bidranges/bidranges.component';
import { AdminusersComponent } from './settings/adminusers/adminusers.component';
import { PermissionDetailsComponent } from './configuration/use-role-permissions/permission-details/permission-details.component';
import { CountryCurrencyComponent } from './configuration/country-currency/country-currency.component';
import { SchedulepaymentsComponent } from './configuration/schedulepayments/schedulepayments.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { PendingCampaignsComponent } from './campaigns/pending/pending-campaigns.component';
import {SlideshowModule} from 'ng-simple-slideshow';
import { ApprovedComponent } from './campaigns/approved/approved.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateTicketComponent } from './support/create-ticket/create-ticket.component';
import { TicketDetailsComponent } from './support/ticket-details/ticket-details.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { TreeviewModule } from 'ngx-treeview';
import { ApproveComponent } from './creditline/approve/approve.component';
import { SuspendComponent } from './creditline/suspend/suspend.component';
import { ReviewComponent } from './creditline/review/review.component';
import { PublisherBillDialogComponent } from './payments/publisher-bill-dialog/publisher-bill-dialog.component';
import { InterestedUsersComponent } from './interested-users/interested-users.component';
import { TopAdvertisersComponent } from './dashboard/top-advertisers/top-advertisers.component';
import { TopPublishersComponent } from './dashboard/top-publishers/top-publishers.component';
import { PublisherBidrangeComponent } from './configuration/publisher-bidrange/publisher-bidrange.component';

@NgModule({
  declarations: [
    AdminComponent,
    ConfigurationComponent,
    AdvertiserComponent,
    PublisherComponent,
    AnalyticsComponent,
    SupportComponent,
    PaymentsComponent,
    SettingsComponent,
    CreditlineComponent,
    PopupEditSMTPComponent,
    UseRolePermissionsComponent,
    CountrytaxesComponent,
    BidrangesComponent,
    AdminusersComponent,
    PermissionDetailsComponent,
    CountryCurrencyComponent,
    SchedulepaymentsComponent,
    CampaignsComponent,
    PendingCampaignsComponent,
    ApprovedComponent,
    DashboardComponent,
    CreateTicketComponent,
    TicketDetailsComponent,
    ApproveComponent,
    SuspendComponent,
    ReviewComponent,
    PublisherHcpScriptComponent,
    PublisherBillDialogComponent,
    InterestedUsersComponent,
    TopAdvertisersComponent,
    TopPublishersComponent,
    PublisherBidrangeComponent,
  ],
  imports: [
    CommonModule,
    SlideshowModule,
    AdminRoutingModule,
    FormsModule,
    CommonModulesModule,
    TreeviewModule,
    TooltipModule],
  entryComponents: [
    PopupEditSMTPComponent,
    CreateTicketComponent,
    ApproveComponent,
    SuspendComponent,
    ReviewComponent,
    PublisherBillDialogComponent
  ]
})

export class AdminModule { }
