import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { PublisherBidRangeModel } from 'src/app/_models/publisher_bid_range_model';
import { PublisherBidRangeService } from 'src/app/_services/admin/publisher-bidrange.service';
import { PublisherSiteWeightageModel } from 'src/app/_models/publisher_site_weightage_model';
import { PublisherSiteWeightageService } from 'src/app/_services/admin/publisher-site-weightage.service';
import { WeightageByValidatedHcpModel } from 'src/app/_models/weightage_by_validated_hcp_model';
import { WeightageByValidatedHcpService } from 'src/app/_services/admin/weightage-by-validated-hcp.service';
import { MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';

@Component({
  selector: 'app-publisher-bidrange',
  templateUrl: './publisher-bidrange.component.html',
  styleUrls: ['./publisher-bidrange.component.scss']
})
export class PublisherBidrangeComponent implements OnInit {

  publisherBidRangeForm: FormGroup;
  publisherSiteWeightageForm: FormGroup;
  WeightageByValidatedHcpForm: FormGroup;
  pageView: String = 'listView';
  isPublisher:  boolean= false;;
  isPublisherSiteWeightage: boolean= false;
  isWeightageByValidatedHcp:  boolean= false;
  formSubmitted;
  message;
  status;
  publisherBidRangesList = [];
  publisherSiteWeightagesList = [];
  weightageByValidatedHcpsList =[];
  id;
  constructor(private formBuilder: FormBuilder, private publisherBidrangeService: PublisherBidRangeService,
    private publisherSiteWeightageService: PublisherSiteWeightageService,
    private weightageByValidatedHcpService:  WeightageByValidatedHcpService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) { }


  ngOnInit() {
    this.renderListView();
    this.renderPublisherSiteWeightageListView();
    this.renderWeightageByValidatedHcpListView();
  }

  createFormGroup() {
    this.publisherBidRangeForm = this.formBuilder.group({
      min: ['', Validators.required],
      max: ['', Validators.required],
      type: ['', Validators.required]
    });
  }

  createPublisherSiteWeightageFormGroup() {
    this.publisherSiteWeightageForm = this.formBuilder.group({
      weightage: ['', Validators.required]
    });
  }

  createWeightageByValidatedHcpFormGroup() {
    this.WeightageByValidatedHcpForm = this.formBuilder.group({
      min: ['', Validators.required],
      max: ['', Validators.required],
      weightage: ['', Validators.required]
    });
  }

  renderPublisherSiteWeightageListView() {
    this.pageView = 'listView';
    this.isPublisherSiteWeightage = false;
    this.publisherSiteWeightageService.getAllPublisherSiteWeightages().subscribe(
      res => {
        this.publisherSiteWeightagesList = [];
        this.publisherSiteWeightagesList = <PublisherSiteWeightageModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }

  renderListView() {
    console.log('ddddddddd');
    this.pageView = 'listView';
    this.isPublisher = false;
    this.publisherBidrangeService.getAllPublisherBidranges().subscribe(
      res => {
        this.publisherBidRangesList = [];
        this.publisherBidRangesList = <PublisherBidRangeModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }
  renderAddView() {
    this.pageView = 'addView';
    this.formSubmitted = false;
    this.isPublisher = true;
    this.isWeightageByValidatedHcp = false;
    this.isPublisherSiteWeightage = false
    this.message = '';
    this.createFormGroup();
  }

  renderEditView(id: any) {
    this.pageView = 'editView';
    this.isPublisher = true;
    this.message = '';
    this.createFormGroup();
    this.formSubmitted = false;
    this.publisherBidrangeService.getPublisherBidrangeById(id).subscribe((publisherBidRange: PublisherBidRangeModel) => {
      this.id = publisherBidRange._id;
      this.publisherBidRangeForm.patchValue(publisherBidRange);
    });
  }

  isFieldValid(field: string) {
    return (
      (!this.publisherBidRangeForm.get(field).valid && this.publisherBidRangeForm.get(field).touched) ||
      ((this.publisherBidRangeForm.get(field).untouched && this.formSubmitted))
    );
  }

  onSubmit() {
    this.formSubmitted = true;
    if (this.publisherBidRangeForm.invalid) {
      return;
    }

    if (this.publisherBidRangeForm.valid) {
      const publisherBidRange: PublisherBidRangeModel = Object.assign({}, this.publisherBidRangeForm.value);
      this.publisherBidrangeService.addPublisherBidrange(publisherBidRange)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error['success'];
          });
    }
  }
  onEditSubmit() {
    this.formSubmitted = true;
    if (this.publisherBidRangeForm.valid) {
      const publisherBidRange: PublisherBidRangeModel = Object.assign({}, this.publisherBidRangeForm.value);
      publisherBidRange._id = this.id;
      this.publisherBidrangeService.updatePublisherBidrange(publisherBidRange)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error.error['success'];
          });
    }
  }

  removePublisherBidRange(id: any, name: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the schedule task',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.publisherBidrangeService.deletePublisherBidrange(id).subscribe(
          data => {
            this.snackBar.open(data['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderListView();
          },
          error => {
            this.snackBar.open(error.error['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }

  renderPublisherSiteWeightageAddView() {
    this.pageView = 'addView';
    this.formSubmitted = false;
    this.isPublisherSiteWeightage = true;
    this.message = '';
    this.createPublisherSiteWeightageFormGroup();
  }

  isPublisherSiteWeightageFieldValid(field: string) {
    return (
      (!this.publisherSiteWeightageForm.get(field).valid && this.publisherSiteWeightageForm.get(field).touched) ||
      ((this.publisherSiteWeightageForm.get(field).untouched && this.formSubmitted))
    );
  }


  renderPublisherSiteWeightageEditView(id: any) {
    this.pageView = 'editView';
    this.message = '';
    this.isPublisherSiteWeightage = true;
    this.createPublisherSiteWeightageFormGroup();
    this.formSubmitted = false;
    this.publisherSiteWeightageService.getPublisherSiteWeightageById(id).subscribe((publisherSiteWeightage: PublisherSiteWeightageModel) => {
      this.id = publisherSiteWeightage._id;
      this.publisherSiteWeightageForm.patchValue(publisherSiteWeightage);
    });
  }
  onPublisherSiteWeightageSubmit() {
    this.formSubmitted = true;
    if (this.publisherSiteWeightageForm.invalid) {
      return;
    }

    if (this.publisherSiteWeightageForm.valid) {
      const publisherSiteWeightage: PublisherSiteWeightageModel = Object.assign({}, this.publisherSiteWeightageForm.value);
      this.publisherSiteWeightageService.addPublisherSiteWeightage(publisherSiteWeightage)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderPublisherSiteWeightageListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error['success'];
          });
    }
  }
  onPublisherSiteWeightageEditSubmit() {
    this.formSubmitted = true;
    if (this.publisherSiteWeightageForm.valid) {
      const publisherSiteWeightage: PublisherSiteWeightageModel = Object.assign({}, this.publisherSiteWeightageForm.value);
      publisherSiteWeightage._id = this.id;
      this.publisherSiteWeightageService.updatePublisherSiteWeightage(publisherSiteWeightage)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderPublisherSiteWeightageListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error.error['success'];
          });
    }
  }

  removePublisherSiteWeightage(id: any, name: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the schedule task',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.publisherSiteWeightageService.deletePublisherSiteWeightage(id).subscribe(
          data => {
            this.snackBar.open(data['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderPublisherSiteWeightageListView();
          },
          error => {
            this.snackBar.open(error.error['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }

  renderWeightageByValidatedHcpListView() {
    this.pageView = 'listView';
    this.isWeightageByValidatedHcp = false;
    this.weightageByValidatedHcpService.getAllWeightageByValidatedHcps().subscribe(
      res => {
        this.weightageByValidatedHcpsList = [];
        this.weightageByValidatedHcpsList = <WeightageByValidatedHcpModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }

  renderWeightageByValidatedHcpAddView() {
    this.pageView = 'addView';
    this.isWeightageByValidatedHcp = true;
    this.formSubmitted = false;
    console.log(this.isPublisher,
      this.isWeightageByValidatedHcp,
      this.isPublisherSiteWeightage)
    this.message = '';
    this.createWeightageByValidatedHcpFormGroup();
  }

  renderWeightageByValidatedHcpEditView(id: any) {
    this.pageView = 'editView';
    this.isWeightageByValidatedHcp = true;
    this.message = '';
    this.createWeightageByValidatedHcpFormGroup();
    this.formSubmitted = false;
    this.weightageByValidatedHcpService.getWeightageByValidatedHcpById(id).subscribe((weightageByValidatedHcp: WeightageByValidatedHcpModel) => {
      this.id = weightageByValidatedHcp._id;
      this.WeightageByValidatedHcpForm.patchValue(weightageByValidatedHcp);
    });
  }

  isWeightageByValidatedHcpFieldValid(field: string) {
    return (
      (!this.WeightageByValidatedHcpForm.get(field).valid && this.WeightageByValidatedHcpForm.get(field).touched) ||
      ((this.WeightageByValidatedHcpForm.get(field).untouched && this.formSubmitted))
    );
  }

  onWeightageByValidatedHcpSubmit() {
    this.formSubmitted = true;
    if (this.WeightageByValidatedHcpForm.invalid) {
      return;
    }

    if (this.WeightageByValidatedHcpForm.valid) {
      const weightageByValidatedHcp: WeightageByValidatedHcpModel = Object.assign({}, this.WeightageByValidatedHcpForm.value);
      this.weightageByValidatedHcpService.addWeightageByValidatedHcp(weightageByValidatedHcp)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderWeightageByValidatedHcpListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error['success'];
          });
    }
  }
  onWeightageByValidatedHcpEditSubmit() {
    this.formSubmitted = true;
    if (this.WeightageByValidatedHcpForm.valid) {
      const weightageByValidatedHcp: WeightageByValidatedHcpModel = Object.assign({}, this.WeightageByValidatedHcpForm.value);
      weightageByValidatedHcp._id = this.id;
      this.weightageByValidatedHcpService.updateWeightageByValidatedHcp(weightageByValidatedHcp)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderWeightageByValidatedHcpListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error.error['success'];
          });
    }
  }

  removeWeightageByValidatedHcp(id: any, name: any) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the schedule task',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.weightageByValidatedHcpService.deleteWeightageByValidatedHcp(id).subscribe(
          data => {
            this.snackBar.open(data['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderWeightageByValidatedHcpListView();
          },
          error => {
            this.snackBar.open(error.error['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }

}
