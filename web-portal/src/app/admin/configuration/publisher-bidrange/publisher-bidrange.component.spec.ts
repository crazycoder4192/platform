import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherBidrangeComponent } from './publisher-bidrange.component';

describe('PublisherBidrangeComponent', () => {
  let component: PublisherBidrangeComponent;
  let fixture: ComponentFixture<PublisherBidrangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherBidrangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherBidrangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
