import { Component, OnInit, Input } from '@angular/core';
import { PERMISSIONSElement } from '../../dataModel/userRolesPermissionsModel';

@Component({
  selector: 'app-permission-details',
  templateUrl: './permission-details.component.html',
  styleUrls: ['./permission-details.component.scss']
})
export class PermissionDetailsComponent implements OnInit {
  @Input() listOfPermissions: Array<PERMISSIONSElement>;
  public permissionSelectedElements = [];

  constructor() {
   }

  ngOnInit() {
    this.permissionSelectedElements = [];
    const temp = this.groupBy(this.listOfPermissions, 'tabName', []);
    for (const key in temp) {
      if (temp.hasOwnProperty(key)) {
        this.permissionSelectedElements.push({'key': key, 'values': temp[key]});
      }
     }
  }

   groupBy(xs, prop, existedlist) {
    const grouped = {};
    for (let i = 0; i < xs.length; i++) {
      const p = xs[i][prop];
      if (!grouped[p]) { grouped[p] = []; }
      // console.log("groupBy -> "+xs[i]["_id"] +", : "+existedlist.includes(xs[i]["_id"]));
      xs[i]['checked'] = existedlist.includes(xs[i]['_id']);
      grouped[p].push(xs[i]);
    }
      return grouped;
  }
}
