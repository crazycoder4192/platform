import { Component, OnInit, Injectable, ViewChild } from '@angular/core';
import { USEROLESElement, PERMISSIONSElement } from '../dataModel/userRolesPermissionsModel';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSnackBar } from '@angular/material';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-use-role-permissions',
  templateUrl: './use-role-permissions.component.html',
  styleUrls: ['./use-role-permissions.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UseRolePermissionsComponent implements OnInit {

  pageView = 'ListView';
  formSubmitted: boolean;
  errorMessage: string;
  displayedColumns: string[] = ['moduleType', 'roleName', 'permissions', 'addrolesandpermission'];
  dataSource: USEROLESElement[] = [];
  moduleTypes = ['Admin', 'Advertiser', 'Publisher'];
  rolesAndPermisionFormGroup: FormGroup;
  permissionsMetadata: any[] = [];
  values: string[];
  treeViewDisabled =  true;
  selectedId: string;
  viewConfig = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 450
  });
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: false,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 450
  });

  constructor(
    private configHTTPService: ConfigurationService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
  ) {
    this.dataSource = [];
  }

  ngOnInit() {
    this.renderListView();
  }
  renderListView() {
    this.pageView = 'ListView';
    this.dataSource = [];
    this.selectedId = '';
    this.configHTTPService.getalluseroles().subscribe(
      res => {
        this.dataSource = [];
        this.dataSource = <USEROLESElement[]>res;
      },
      err => {
        console.log('Error! getalluseroles');
      }
    );
  }

  renderAddView() {
    this.pageView = 'AddView';
    this.createForm();
    this.getAllPermissionsByModuleType(this.rolesAndPermisionFormGroup.get('moduleType').value);
  }

  createForm() {
    this.rolesAndPermisionFormGroup = this.formBuilder.group({
      moduleType: ['Admin', Validators.required],
      roleName: ['', Validators.required],
      description: ['']
    });
  }

  getAllPermissionsByModuleType(mType: String) {
    this.formSubmitted = false;
    this.configHTTPService.getallpermissionsbyMType(mType).subscribe(
      res => {
        this.prepareTreeViewStructureForAddScenario(<any[]>res);
      },
      err => {
        console.log('Error! getallpermissions');
      }
    );
  }

  editRolesAndPerDetails(selectedElement) {
    this.pageView = 'EditView';
    this.createForm();
    this.configHTTPService.getoneuseroles(selectedElement).subscribe(
      res => {
        const result = <USEROLESElement>res;
        this.configHTTPService.getallpermissionsbyMType(result.moduleType).subscribe(
          response => {
            this.prepareTreeViewStructureForEditScenario(<any[]>response, result.permissions);
            this.selectedId = selectedElement;
            this.rolesAndPermisionFormGroup.get('moduleType').setValue(result.moduleType);
            this.rolesAndPermisionFormGroup.get('roleName').setValue(result.roleName);
            this.rolesAndPermisionFormGroup.get('description').setValue(result.description);
          },
          err => {
            console.log('Error! getallpermissions of module type');
          }
        );
      },
      error => {
        this.errorMessage = error.error['message'];
        console.log('Error! get permissions based on selected role');
      }
    );
  }

  prepareTreeViewStructureForAddScenario(permissions: any[]) {
    this.permissionsMetadata = [];
    for (const permission of permissions) {
      const children: any[] = [];
      for (const child of permission.items) {
        children.push({ text: child.actionName, checked: false, value: child.identifier});
      }
      if (permission.items && permission.items.length === 1 && permission._id === permission.items[0].actionName) {
        const treeViewItem = new TreeviewItem({
          text: permission._id, value: permission.items[0].identifier,  checked: false, collapsed: true
        });
        this.permissionsMetadata.push(treeViewItem);
      } else {
        const treeViewItem = new TreeviewItem({
          text: permission._id, value: permission._id,  collapsed: true,
          children: children
        });
        this.permissionsMetadata.push(treeViewItem);
      }
    }
  }

  prepareTreeViewStructureForEditScenario(permissionsOfModule: any[], selectedPermissions: any[]) {
    this.permissionsMetadata = [];
    for (const permission of permissionsOfModule) {
      const children: any[] = [];
      for (const child of permission.items) {
        if (selectedPermissions.indexOf(child.identifier) === -1) {
          children.push({ text: child.actionName, checked: false, value: child.identifier});
        } else {
          children.push({ text: child.actionName, checked: true, value: child.identifier});
        }
      }
      if (permission.items && permission.items.length === 1 && permission._id === permission.items[0].actionName) {
        if (selectedPermissions.indexOf(permission.items[0].identifier) === -1) {
          const treeViewItem = new TreeviewItem({
            text: permission._id, value: permission.items[0].identifier,  checked: false
          });
          this.permissionsMetadata.push(treeViewItem);
        } else {
          const treeViewItem = new TreeviewItem({
            text: permission._id, value: permission.items[0].identifier,  checked: true
          });
          this.permissionsMetadata.push(treeViewItem);
        }
      } else {
        const treeViewItem = new TreeviewItem({
          text: permission._id, value: permission._id,  collapsed: true,
          children: children
        });
        this.permissionsMetadata.push(treeViewItem);
      }
    }
  }

  saveRolesAndPermissions() {
    this.formSubmitted = true;
    if (this.rolesAndPermisionFormGroup.valid && this.values && this.values.length > 0) {
      const postPermissionsData = {
        'moduleType': this.rolesAndPermisionFormGroup.get('moduleType').value,
        'roleName': this.rolesAndPermisionFormGroup.get('roleName').value,
        'description': this.rolesAndPermisionFormGroup.get('description').value,
        'permissions': this.values
      };
      this.configHTTPService.storeuserroles(postPermissionsData).subscribe(
        data => {
          this.snackBar.open(data['message'], data['success'], {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
          this.renderListView();
        },
        error => {
          this.errorMessage = error.error['message'];
        }
      );
    }
  }

  updateRolesAndPermissions(selectedId) {
    this.formSubmitted = true;
    if (this.rolesAndPermisionFormGroup.valid && this.values && this.values.length > 0) {
      const postPermissionsData = {
        'id': selectedId,
        'moduleType': this.rolesAndPermisionFormGroup.get('moduleType').value,
        'roleName': this.rolesAndPermisionFormGroup.get('roleName').value,
        'description': this.rolesAndPermisionFormGroup.get('description').value,
        'permissions': this.values
      };
      this.configHTTPService.updateuserroles(postPermissionsData).subscribe(
        data => {
          this.snackBar.open(data['message'], data['success'], {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
          this.renderListView();
        },
        error => {
          this.errorMessage = error.error['message'];
        }
      );
    }
  }


}
