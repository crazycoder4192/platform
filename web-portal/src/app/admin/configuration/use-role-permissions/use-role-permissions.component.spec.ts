import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseRolePermissionsComponent } from './use-role-permissions.component';

describe('UseRolePermissionsComponent', () => {
  let component: UseRolePermissionsComponent;
  let fixture: ComponentFixture<UseRolePermissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseRolePermissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseRolePermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
