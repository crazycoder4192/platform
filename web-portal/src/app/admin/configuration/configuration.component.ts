import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupEditSMTPComponent } from './popup-edit-smtp/popup-edit-smtp.component';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { SMTPElement } from './dataModel/smtpModel';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  public displayedColumns: string[] = ['category', 'host', 'port', 'secure', 'user', 'createDate', 'addsmtp'];
  public dataSource: SMTPElement[] = [];

  constructor(public dialog: MatDialog, private configHTTPService: ConfigurationService) {
    this.configHTTPService.getallsmtpdetails().subscribe(
      res => {
        this.dataSource = [];
        this.dataSource = <SMTPElement[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PopupEditSMTPComponent, {
      width: '50%',
      data: {category: 'single'}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.saveSMTPDetails(result);
    });
  }

  editSMTPDetails(selectedId: any) {
    this.configHTTPService.getonesmtpdetails(selectedId).subscribe(
      res => {
        const dialogRef = this.dialog.open(PopupEditSMTPComponent, {
          width: '50%',
          data: <SMTPElement>res
        });

        dialogRef.afterClosed().subscribe(result => {
          this.updateSMTPDetails(result);
        });
      },
      err => {
        console.log('Error!');
      }
    );

  }

  saveSMTPDetails(data: SMTPElement) {
    this.configHTTPService.storesmtpvalues(data).subscribe(
      res => {
        this.configHTTPService.getallsmtpdetails().subscribe(
          result => {
            this.dataSource = [];
            this.dataSource = <SMTPElement[]>result;
          },
          err => {
            console.log('Error!');
          }
        );
      },
      err => {
        console.log('Error!');
      }
    );
  }

  updateSMTPDetails(data: SMTPElement) {
    this.configHTTPService.updatesmtpvalues(data).subscribe(
      res => {
        this.configHTTPService.getallsmtpdetails().subscribe(
          result => {
            this.dataSource = [];
            this.dataSource = <SMTPElement[]>result;
          },
          err => {
            console.log('Error!');
          }
        );
      },
      err => {
        console.log('Error!');
      }
    );
  }

  deleteSMTPDetails(data: SMTPElement) {
    data.activestate = false;
    this.updateSMTPDetails(data);
  }



}
