import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupEditSMTPComponent } from './popup-edit-smtp.component';

describe('PopupEditSMTPComponent', () => {
  let component: PopupEditSMTPComponent;
  let fixture: ComponentFixture<PopupEditSMTPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupEditSMTPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupEditSMTPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
