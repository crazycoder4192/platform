import { Component, OnInit, Inject, ViewEncapsulation} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { SMTPElement } from '../dataModel/smtpModel';

@Component({
  selector: 'app-popup-edit-smtp',
  templateUrl: './popup-edit-smtp.component.html',
  styleUrls: ['./popup-edit-smtp.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupEditSMTPComponent implements OnInit {
  formValid: Boolean = false;
  constructor(public dialogRef: MatDialogRef<PopupEditSMTPComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SMTPElement, private snackBar: MatSnackBar) {
      data.activestate = (data.activestate ? data.activestate : false);
     }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit(): void {
    if (!this.data.host || !this.data.port || !this.data.secure || !this.data.user || !this.data.pass) {
      this.formValid = true;
      this.snackBar.open('Please fill required fields', '', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['success-campaign-notify']
      });
    } else {
      this.formValid = false;
      this.dialogRef.close(this.data);
    }
  }
}
