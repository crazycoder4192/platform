export interface USEROLESElement {
  _id: string;
  moduleType: string;
  roleName: string;
  description: string;
  permissions: Array<string>;
  moduleAllPermissions: Array<any>;
}

export interface PERMISSIONSElement {
  _id: string;
  moduleType: string;
  tabName: string;
  actionName: string;
  identifier: string;
}
