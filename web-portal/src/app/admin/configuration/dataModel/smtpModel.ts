export interface SMTPElement {
  category: string;
  createdAt: string;
  host: string;
  pass: string;
  port: string;
  secure: string;
  user: string;
  activestate: boolean;
}
