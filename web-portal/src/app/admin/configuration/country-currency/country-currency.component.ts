import { Component, OnInit } from '@angular/core';
import {CountryCurrenciesModel} from 'src/app/_models/CountryCurrenciesModel';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {ConfigurationService} from 'src/app/_services/admin/configuration.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-country-currency',
  templateUrl: './country-currency.component.html',
  styleUrls: ['./country-currency.component.scss']
})
export class CountryCurrencyComponent implements OnInit {

  displayedColumns: string[] = [
    'countryName',
    'countryISO3Code',
    'countryISO2Code',
    'currencyCode',
    'currencyName',
    'symbol',
    'addCountryCurrency'
  ];
  dataSource: any = [];
  pageView: String = 'listView';
  countryCurrencyForm: FormGroup;
  private formSubmitted: boolean;
  errMessage: any;
  countryCurrencies: CountryCurrenciesModel[] = [];
  message: String;
  status: String;
  id: number;

  constructor(private formBuilder: FormBuilder, private configurationService: ConfigurationService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.renderListView();
  }

  createFormGroup() {
    this.countryCurrencyForm = this.formBuilder.group({
      countryName: ['', Validators.required],
      countryISO3Code: ['', Validators.required],
      countryISO2Code: ['', Validators.required],
      currencyCode: ['', Validators.required],
      currencyName: ['', Validators.required],
      symbol: ['', Validators.required]
    });
  }

  renderListView() {
    this.pageView = 'listView';
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countryCurrencies = [];
        this.countryCurrencies = <CountryCurrenciesModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }

  renderAddView() {
    this.pageView = 'addView';
    this.formSubmitted = false;
    this.errMessage = '';
    this.createFormGroup();
  }
  isFieldValid(field: string) {
    return (
      (!this.countryCurrencyForm.get(field).valid && this.countryCurrencyForm.get(field).touched) ||
      ((this.countryCurrencyForm.get(field).untouched && this.formSubmitted))
    );
  }
  onSubmit() {
    this.formSubmitted = true;
    if (this.countryCurrencyForm.valid) {
      const countryCurrency: CountryCurrenciesModel = Object.assign({}, this.countryCurrencyForm.value);

      this.configurationService.addCountryCurrency(countryCurrency)
      .subscribe(
      data => {
        this.message = data['message'];
        this.status = data['success'];
        this.renderListView();
      },
      error => {
        this.errMessage = error.error['message'];
        this.status = error['success'];
      });
    }
  }
  renderEditView(id: any) {
    this.pageView = 'editView';
    this.errMessage = '';
    this.createFormGroup();
    this.formSubmitted = false;
    this.configurationService.getCountryCurrencyById(id).subscribe((countryCurrency: CountryCurrenciesModel) => {
      this.id = countryCurrency._id;
      this.countryCurrencyForm.patchValue(countryCurrency);
    });
  }
  onEditSubmit() {
    this.formSubmitted = true;
    if (!this.countryCurrencyForm.valid) {
      this.validateAllFormFields(this.countryCurrencyForm);
    }
    if (this.countryCurrencyForm.valid) {
      const countryCurrency: CountryCurrenciesModel = Object.assign({}, this.countryCurrencyForm.value);
      countryCurrency._id = this.id;
      this.configurationService.updateCountryCurrency(countryCurrency)
      .subscribe(
      data => {
        this.message = data['message'];
        this.status = data['success'];
        this.renderListView();
      },
      error => {
        this.message = error['message'];
        this.status = error['success'];
      });
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);

      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  removeCountryCurrency(id: any, name: any) {
    this.configurationService.deleteCountryCurrency(id).subscribe(
      data => {
        this.snackBar.open(data['message'], '', {
          duration: 10000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.snackBar.open(error.error['message'], '', {
          duration: 10000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
      }
    );
  }
}
