import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulepaymentsComponent } from './schedulepayments.component';

describe('SchedulepaymentsComponent', () => {
  let component: SchedulepaymentsComponent;
  let fixture: ComponentFixture<SchedulepaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulepaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulepaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
