import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SchedulePaymentModel } from 'src/app/_models/schedule_payments.model';
import { SchedulePaymentService } from 'src/app/_services/admin/schedulepayment.service';
import { MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';


@Component({
  selector: 'app-schedulepayments',
  templateUrl: './schedulepayments.component.html',
  styleUrls: ['./schedulepayments.component.scss']
})
export class SchedulepaymentsComponent implements OnInit {
  schedulePaymentForm: FormGroup;
  pageView: String = 'listView';
  formSubmitted;
  message;
  status;
  scheduledPaymentList = [];
  days = [];
  schedulePaymentsList = [];
  id;
  constructor(private formBuilder: FormBuilder, private schedulePaymentService: SchedulePaymentService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.days = Array.from(Array(28), (x, index) => index + 1);
    this.renderListView();
  }

  createFormGroup() {
    this.schedulePaymentForm = this.formBuilder.group({
      scheduleDayofMonth: ['', Validators.required],
      enableSchedule: ['', Validators.required],
    });
  }

  renderListView() {
    this.pageView = 'listView';
    this.schedulePaymentService.getAllSchedulePayments().subscribe(
      res => {
        this.schedulePaymentsList = [];
        this.schedulePaymentsList = <SchedulePaymentModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }
  renderAddView() {
    this.pageView = 'addView';
    this.formSubmitted = false;
    this.message = '';
    this.createFormGroup();
  }

  renderEditView(id: any) {
    this.pageView = 'editView';
    this.message = '';
    this.createFormGroup();
    this.formSubmitted = false;
    this.schedulePaymentService.getSchedulePaymentById(id).subscribe((schedulePayment: SchedulePaymentModel) => {
      this.id = schedulePayment._id;
      this.schedulePaymentForm.patchValue(schedulePayment);
    });
  }


  onSubmit() {
    this.formSubmitted = true;
    if (this.schedulePaymentForm.invalid) {
      return;
    }

    if (this.schedulePaymentForm.valid) {
      const schedulePayment: SchedulePaymentModel = Object.assign({}, this.schedulePaymentForm.value);
      this.schedulePaymentService.addSchedulePayment(schedulePayment)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error['success'];
          });
    }
  }
  onEditSubmit() {
    this.formSubmitted = true;
    if (this.schedulePaymentForm.valid) {
      const schedulePayment: SchedulePaymentModel = Object.assign({}, this.schedulePaymentForm.value);
      schedulePayment._id = this.id;
      this.schedulePaymentService.updateSchedulePayment(schedulePayment)
        .subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.renderListView();
          },
          error => {
            this.message = error.error['message'];
            this.status = error.error['success'];
          });
    }
  }

  removeSchedulePayment(id: any, name: any) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to delete the schedule task',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.schedulePaymentService.deleteSchedulePayment(id).subscribe(
          data => {
            this.snackBar.open(data['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderListView();
          },
          error => {
            this.snackBar.open(error.error['message'], '', {
              duration: 10000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }
}
