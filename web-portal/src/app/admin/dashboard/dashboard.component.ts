import { Component, OnInit } from '@angular/core';
import { AdminDashboardService } from '../../_services/admin/admin-dashboard.service';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public top5AdvertisersList: any[] = [];
  public top5PublishersList: any[] = [];
  public top5AssetsList: any[] = [];
  public top5CampaignsList: any[] = [];
  constructor(private adminDashboardService: AdminDashboardService) { }

  public top5AdvColumns: string[] = ['Advertiser', 'Spends', 'Active campaigns', 'This Week'];
  public top5PubColumns: string[] = ['Publisher', 'Earnings', 'Active Assets', 'This Week'];
  public top5AssetColumns: string[] = ['Name', 'Type', 'Platform', 'Earnings', 'Utilized', 'This Week'];
  public topCampaignsColumns: string[] = ['Campaign Name', 'Type', 'Spends', 'Cost', 'Reach', 'This Week'];

  ngOnInit() {
    this.adminDashboardService.getTop5Advertisers().subscribe(
      res => {
        this.top5AdvertisersList = <[]>res;
      },
      err => {

      });
    this.adminDashboardService.getTop5Publishers().subscribe(
      res => {
        this.top5PublishersList = <[]>res;
      },
      err => {

      });
    this.adminDashboardService.getTop5Assets().subscribe(
      res => {
        this.top5AssetsList = <[]>res;
      },
      err => {

      });

    this.adminDashboardService.getTop5Campaigns().subscribe(
      res => {
        this.top5CampaignsList = <[]>res;
      },
      err => {

      });
  }

  sortTopAdvertisers(sort: Sort) {
    const data: any = this.top5AdvertisersList.slice();
    if (!sort.active || sort.direction === '') {
      this.top5AdvertisersList = data;
      return;
    }

    this.top5AdvertisersList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Advertiser': return compare(a.advertiser, b.advertiser, isAsc);
        case 'Spends': return compare(a.totalAmount, b.totalAmount, isAsc);
        case 'Active campaigns': return compare(a.activeCampaigns, b.activeCampaigns, isAsc);
        case 'This Week': return compare(a.thisWeek, b.thisWeek, isAsc);
        default: return 0;
      }
    });
  }
  sortTopPublishers(sort: Sort) {
    const data: any = this.top5PublishersList.slice();
    if (!sort.active || sort.direction === '') {
      this.top5PublishersList = data;
      return;
    }

    this.top5PublishersList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Publisher': return compare(a.advertiser, b.advertiser, isAsc);
        case 'Earnings': return compare(a.totalAmount, b.totalAmount, isAsc);
        case 'Active Assets': return compare(a.activeCampaigns, b.activeCampaigns, isAsc);
        case 'This Week': return compare(a.thisWeek, b.thisWeek, isAsc);
        default: return 0;
      }
    });
  }
  sortTopAssets(sort: Sort) {
    const data: any = this.top5AssetsList.slice();
    if (!sort.active || sort.direction === '') {
      this.top5AssetsList = data;
      return;
    }

    this.top5AssetsList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Name': return compare(a.assetName, b.assetName, isAsc);
        case 'Type': return compare(a.assetType, b.assetType, isAsc);
        case 'Platform': return compare(a.platformEarnings, b.platformEarnings, isAsc);
        case 'Earnings': return compare(a.assetEarnings, b.assetEarnings, isAsc);
        case 'Utilized': return compare(a.utilized, b.utilized, isAsc);
        case 'This Week': return compare(a.thisWeek, b.thisWeek, isAsc);
        default: return 0;
      }
    });
  }

  sortTopCampaigns(sort: Sort) {
    const data: any = this.top5CampaignsList.slice();
    if (!sort.active || sort.direction === '') {
      this.top5CampaignsList = data;
      return;
    }

    this.top5CampaignsList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Campaign Name': return compare(a.subcampaignName, b.subcampaignName, isAsc);
        case 'Type': return compare(a.type, b.type, isAsc);
        case 'Spends': return compare(a.spends, b.spends, isAsc);
        case 'Cost': return compare(a.assetEarnings, b.assetEarnings, isAsc);
        case 'Reach': return compare(a.reach, b.reach, isAsc);
        case 'This Week': return compare(a.thisWeek, b.thisWeek, isAsc);
        default: return 0;
      }
    });
  }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
