import { Component, OnInit } from '@angular/core';
import { AdminDashboardService } from '../../../_services/admin/admin-dashboard.service';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-top-advertisers',
  templateUrl: './top-advertisers.component.html',
  styleUrls: ['./top-advertisers.component.scss']
})
export class TopAdvertisersComponent implements OnInit {

  public topAdvertisersList: any[] = [];
  public topAdvertisersListTemp: any[] = [];
  public topAdvColumns: string[] = ['Advertiser', 'Spends', 'Active campaigns', 'This Week'];

  constructor(private adminDashboardService: AdminDashboardService) { }

  ngOnInit() {
    this.adminDashboardService.getTopAdvertisers().subscribe(
      res => {
        this.topAdvertisersList = <[]>res;
        this.topAdvertisersListTemp = <[]>res;
      },
      err => {

      });
  }

  sortTopAdvertisers(sort: Sort) {
    const data: any = this.topAdvertisersList.slice();
    if (!sort.active || sort.direction === '') {
      this.topAdvertisersList = data;
      return;
    }

    this.topAdvertisersList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Advertiser': return compare(a.advertiser, b.advertiser, isAsc);
        case 'Spends': return compare(a.totalAmount, b.totalAmount, isAsc);
        case 'Active campaigns': return compare(a.activeCampaigns, b.activeCampaigns, isAsc);
        case 'This Week': return compare(a.thisWeek, b.thisWeek, isAsc);
        default: return 0;
      }
    });
  }

  searchAdvertiser(name) {
    if (!name) {
      this.topAdvertisersList = this.topAdvertisersListTemp;
    } else {
      this.topAdvertisersList = this.topAdvertisersListTemp.filter(option =>
        option.advertiser.toLowerCase().indexOf( name.toLowerCase()) !== -1
      );
    }
  }

}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}