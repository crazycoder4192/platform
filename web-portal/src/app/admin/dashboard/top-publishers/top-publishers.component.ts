import { Component, OnInit } from '@angular/core';
import { AdminDashboardService } from '../../../_services/admin/admin-dashboard.service';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-top-publishers',
  templateUrl: './top-publishers.component.html',
  styleUrls: ['./top-publishers.component.scss']
})
export class TopPublishersComponent implements OnInit {

  public topPublishersList: any[] = [];
  public topPublishersListTemp: any[] = [];
  public topPubColumns: string[] = ['Publisher', 'Earnings', 'Active Assets', 'This Week'];
  constructor(private adminDashboardService: AdminDashboardService) { }

  ngOnInit() {
    this.adminDashboardService.getTopPublishers().subscribe(
      res => {
        this.topPublishersList = <[]>res;
        this.topPublishersListTemp = <[]>res;
      },
      err => {

      }
    );
  }
  sortTopPublishers(sort: Sort) {
    const data: any = this.topPublishersList.slice();
    if (!sort.active || sort.direction === '') {
      this.topPublishersList = data;
      return;
    }

    this.topPublishersList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Publisher': return compare(a.advertiser, b.advertiser, isAsc);
        case 'Earnings': return compare(a.totalAmount, b.totalAmount, isAsc);
        case 'Active Assets': return compare(a.activeCampaigns, b.activeCampaigns, isAsc);
        case 'This Week': return compare(a.thisWeek, b.thisWeek, isAsc);
        default: return 0;
      }
    });
  }

  searchPublisher(name) {
    if (!name) {
      this.topPublishersList = this.topPublishersListTemp;
    } else {
      this.topPublishersList = this.topPublishersListTemp.filter(option =>
        option.publisher.toLowerCase().indexOf( name.toLowerCase()) !== -1
      );
    }
  }

}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
