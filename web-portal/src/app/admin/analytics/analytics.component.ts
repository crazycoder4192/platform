import {  Component, OnInit } from '@angular/core';
import { AdminDashboardService } from '../../_services/admin/admin-dashboard.service';
import { AdminAnalyticsService } from '../../_services/admin/analytics.service';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  public topAdvertisers: any[] = [];
  public showfilters = false;
  public todayDate: Date = new Date(new Date().setHours(23, 59, 59));
  public startDate: Date = new Date(2019, 5, 5);
  public startDateMin: Date = new Date(2019, 1, 1);
  public startDateMax: Date = this.todayDate;
  public endDateMax: Date = this.todayDate;
  public filterForm: FormGroup;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  public analyticsGraphsDataTemp: any = null;
  public showAdvertiserTopSubcampaignTableData =  false;
  public advertiserTopSubcampaignTableData1 =  [];
  public advertiserTopSubcampaignTableData2 =  [];

  public filteredDuration = [
    { value: 'Custom Duration', name: 'Custom Duration' },
    { value: 'Last 24 hours', name: 'Last 24 hours' },
    { value: 'Last 48 hours', name: 'Last 48 hours' },
    { value: 'Last 1 week', name: 'Last 1 week' },
    { value: 'Last 1 month', name: 'Last 1 month' },
    { value: 'Last 6 months', name: 'Last 6 months' }
  ];

  constructor(private adminAnalyticsService: AdminAnalyticsService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private formBuilder: FormBuilder ) {
  }

  get f() { return this.filterForm.controls; }

  ngOnInit() {
    this.filterForm = this.formBuilder.group({
      _id: [''],
      startDate: [new Date(), Validators.required],
      endDate: [new Date(), Validators.required],
      duration: ['Custom Duration', Validators.required],
      email: this.userDetails.email,
      timezone: new Date().getTimezoneOffset(),
      tabType: ''
    });

    this.adminAnalyticsService.getAnalytics(this.filterForm.value).subscribe( res => {
      this.analyticsGraphsDataTemp = res;
    },
    err => {

    });

  }

  getAnalytics() {
    this.analyticsGraphsDataTemp = null;
    this.adminAnalyticsService.getAnalytics(this.filterForm.value).subscribe( res => {
      this.analyticsGraphsDataTemp = res;
      this.drawAdvertiserBrandAndTherapeuticStackedBarChart(this.analyticsGraphsDataTemp.brandAndTherapeuticData);
      this.drawAdvertiserTopBrandsBubbleChart(this.analyticsGraphsDataTemp.topBrands);
      this.drawAdvertiserTopSubCampaignsScatterChart(this.analyticsGraphsDataTemp.topSubCampaigns);

      this.drawTableAdvertiserTopSubCampaigns(this.analyticsGraphsDataTemp.topSubCampaigns);
    },
    err => {

    });
  }

  drawAdvertiserBrandAndTherapeuticStackedBarChart(therapeuticData: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'BrandType');
    data.addColumn('number', 'Active');
    data.addColumn('number', 'Inactive');


    for (const ele of therapeuticData) {
      data.addRows([[ele.brandType, ele.active, ele.inactive]]);  
    }

    const options: any = {
      vAxis: { viewWindow: {min: 0}},
      colors: ['#EA694D', '#4A68E0'],
      chartArea: { 'width': '90%', 'height': '80%' },
      legend: {
        position: 'top',
        alignment: 'end'
      },
      bar: { groupWidth: '40%' },
      'height': 300,
      isStacked: true
    };

    const chart = new google.visualization.ColumnChart(
    document.getElementById('advertiserBrandAndTherapeuticStackedBarChart'));
    const columns = [
      { title: 'BrandType', dataKey: 'brandType' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
   
    chart.draw(data, options);

  }

  drawAdvertiserTopBrandsBubbleChart(topbrands) {

    const data = new google.visualization.DataTable();
    data.addColumn('string', 'BrandType');
    data.addColumn('number', 'Reach');
    data.addColumn('number', 'Spends');
    data.addColumn('string', 'Size');
    data.addColumn('number', 'Subcampains Count');


    for (const ele of topbrands) {
      data.addRows([['', ele.reach, ele.spends, 'Size indicates no. of campaigns', ele.subcampaigncount]]);  
    }


    var options = {
     
      hAxis: {title: '------ Reach ------>'},
      vAxis: {title: '------ Spends ------>'},
      'height': 300,
      'width': 1200
    };

    var chart = new google.visualization.BubbleChart(document.getElementById('advertiserTopBrandsBubbleChart'));
    chart.draw(data, options);

  }

  drawAdvertiserTopSubCampaignsScatterChart(topSubCampaigns) {
    if (topSubCampaigns.length > 0) {
      const data = new google.visualization.DataTable();
      data.addColumn('number', 'Spends');
      data.addColumn('number', 'Reach');

      for (const ele of topSubCampaigns) {
        for (const el of ele.subcamps) {
          if (ele.adType === 'Banner') {
            /*TODO: unable to get custom tooltip
          // let tooltip: string = `${r.assetName}, reach: ${r.reach}, earnings: ${r.totalEarnings}`*/
            data.addRow([el.spends, el.reach]);
          }
        }
      }

      const options = {
        hAxis: {title: '------ Reach ------>'},
        vAxis: {title: '------ Spends ------>'},
        'height': 300,
        'width': 1200
      };
      setTimeout(() => {
        const chart = new google.visualization.ScatterChart(document.getElementById('advertiserTopSubCampaignsScatterChart'));
        chart.draw(data, options);
      }, 200);
    }

  }
  drawTableAdvertiserTopSubCampaigns(topSubCampaigns) {

    const totalRows: number = topSubCampaigns[0].subcamps.length;
    console.log(totalRows);

    if (totalRows > 0 ) {
      for (let i = 0; i <  totalRows / 2; i++) {
        this.advertiserTopSubcampaignTableData1.push(topSubCampaigns[0].subcamps[i]);
      }
      for (let i =  Math.round(totalRows / 2); i <  totalRows; i++) {
        this.advertiserTopSubcampaignTableData2.push(topSubCampaigns[0].subcamps[i]);
      }
    }
  }

  public displayFilters() {
    this.showfilters = !this.showfilters;
  }

  public showAdvertiserTopSubCampaignsData() {
    this.showAdvertiserTopSubcampaignTableData = !this.showAdvertiserTopSubcampaignTableData;
    if (!this.showAdvertiserTopSubcampaignTableData) {
      setTimeout(() => {
          this.drawAdvertiserTopSubCampaignsScatterChart(this.analyticsGraphsDataTemp.topSubCampaigns);
      }, 500);
    }
  }

  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.filterForm.get('startDate').value);
    this.startDateMax = new Date(this.filterForm.get('endDate').value);
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDateMax = new Date(this.filterForm.get('endDate').value);
  }

  onSelectDuration(duration) {
    const endDate = new Date();
    this.filterForm.get('endDate').setValue(endDate);
    const startDate = new Date();
    if (duration === 'Custom Duration') {
     // this.filterForm.get('startDate').setValue(this.brandCreateDate);
      this.filterForm.get('startDate').enable();
      this.filterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      startDate.setHours(startDate.getHours() - 24);
      this.filterForm.get('startDate').setValue(startDate);
      this.filterForm.get('startDate').disable();
      this.filterForm.get('endDate').disable();
    } else if (duration === 'Last 48 hours') {
      startDate.setHours(startDate.getHours() - 48);
      this.filterForm.get('startDate').setValue(startDate);
      this.filterForm.get('startDate').disable();
      this.filterForm.get('endDate').disable();
    } else if (duration === 'Last 1 week') {
      startDate.setHours(startDate.getDay() - 7 * 24);
      this.filterForm.get('startDate').setValue(startDate);
      this.filterForm.get('startDate').disable();
      this.filterForm.get('endDate').disable();
    } else if (duration === 'Last 1 month') {
      startDate.setMonth(startDate.getMonth() - 1);
      this.filterForm.get('startDate').setValue(startDate);
      this.filterForm.get('startDate').disable();
      this.filterForm.get('endDate').disable();
    } else if (duration === 'Last 6 months') {
      startDate.setMonth(startDate.getMonth() - 6);
      this.filterForm.get('startDate').setValue(startDate);
      this.filterForm.get('startDate').disable();
      this.filterForm.get('endDate').disable();
    }
  }

  }
