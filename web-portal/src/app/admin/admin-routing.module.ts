import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdvertiserComponent } from './advertiser/advertiser.component';
import { PublisherComponent } from './publisher/publisher.component';
import { PublisherHcpScriptComponent } from './publisher/publisher-hcp-script/publisher-hcp-script.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { CreditlineComponent } from './creditline/creditline.component';
import { PaymentsComponent } from './payments/payments.component';
import { SettingsComponent } from './settings/settings.component';
import { SupportComponent } from './support/support.component';
import { AuthGuard } from '../_guards/auth-guard.guard';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TopAdvertisersComponent } from './dashboard/top-advertisers/top-advertisers.component';
import { TopPublishersComponent } from './dashboard/top-publishers/top-publishers.component';
import { InterestedUsersComponent } from './interested-users/interested-users.component';
import { TicketDetailsComponent } from './support/ticket-details/ticket-details.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AuthGuard],
    children: [
      {
        path: '', canActivateChild: [AuthGuard],
        children: [
          { path: 'config', component: ConfigurationComponent},
          { path: 'advertiser', component: AdvertiserComponent},
          { path: 'publisher', component: PublisherComponent},
          { path: 'publisher/hcpscript', component: PublisherHcpScriptComponent},
          { path: 'analytics', component: AnalyticsComponent},
          { path: 'support', component: SupportComponent},
          { path: 'campaigns', component: CampaignsComponent},
          { path: 'payments', component: PaymentsComponent},
          { path: 'settings', component: SettingsComponent},
          { path: 'creditline', component: CreditlineComponent},
          { path: 'dashboard', component: DashboardComponent},
          { path: 'dashboard/topadvertisers', component: TopAdvertisersComponent},
          { path: 'dashboard/toppublishers', component: TopPublishersComponent},
          { path: 'ticketDetails/:id', component: TicketDetailsComponent},
          { path: 'interestedusers', component: InterestedUsersComponent}
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
