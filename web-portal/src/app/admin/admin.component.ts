import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { SpinnerService } from '../_services/spinner.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(private authService: AuthenticationService , private router: Router,
              private spinner: SpinnerService, private location: Location) {
    this.spinner.showSpinner.next(false);
   }
   marketSelection = '1';
   marketSelectionDisplay = false;

  ngOnInit() {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    const visitedRoute = JSON.parse(localStorage.getItem('visitRoute'));
    if (localStorage.getItem('currentRoute') && !visitedRoute.admin) {
      this.router.navigate([localStorage.getItem('currentRoute')]);
      visitedRoute.admin = true;
      localStorage.setItem('visitRoute', JSON.stringify(visitedRoute));
    } else if (visitedRoute && visitedRoute.admin) {
      this.router.navigate([this.location.path()]);
    } else {
      this.router.navigate(['/admin/dashboard']);
    }
  }
  updateMarketSelection() {
    localStorage.setItem('zone', this.marketSelection);
  }

  logout() {
    this.authService.logout('admin')
    .pipe()
    .subscribe(
    data => {
      console.log('Logout successfull : ' + data);
    },
    error => {
      console.log('Logout successfull : ' + error);
    });
  }

}
