import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingCampaignsComponent } from './pending-campaigns.component';

describe('PendingCampaingsComponent', () => {
  let component: PendingCampaignsComponent;
  let fixture: ComponentFixture<PendingCampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingCampaignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingCampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
