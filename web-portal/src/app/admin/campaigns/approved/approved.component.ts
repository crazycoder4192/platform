import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CampaignService } from 'src/app/_services/advertiser/campaign.service';
import { MatSnackBar, MatTableDataSource, MatDialogConfig, MatDialog, MatOptionModule } from '@angular/material';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';

export interface PendingCampaigns {
  _id: string;
  name: string;
  creativeType: string;
  creativeSpecifications: string;
  modified: string;
  created: string;
  campaignName: string;
  brandName: string;
  Action: string;
}

@Component({
  selector: 'app-approved-campaigns',
  templateUrl: './approved.component.html',
  styleUrls: ['./approved.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ApprovedComponent implements OnInit {

  dataSource = [];
  tempDataList = [];
  columnsToDisplay;
  expandedElement: PendingCampaigns;
  constructor(private campaignService: CampaignService, private snackBar: MatSnackBar, private dialog: MatDialog) {
    this.renderListView();
  }

  ngOnInit() {
    this.columnsToDisplay = ['name', 'brandName', 'campaignName', 'creativeType', 'created.at', 'modified.at', 'Action'];
    this.renderListView();
  }
  renderListView() {
    this.campaignService.getPendingCampaigns().subscribe(
      resposne => {
        this.tempDataList = <PendingCampaigns[]>resposne;
        // TODO: filtering now only active campaigns
        this.dataSource = this.tempDataList.filter(subcampaign => subcampaign.isApproved === true
           && subcampaign.status === 'Active');
        this.dataSource.forEach(element => {
          const imageUrls = [];
          element.creativeSpecifications.creativeDetails.forEach(item => {
            imageUrls.push(item.url);
          });
          element['imageUrls'] = imageUrls;
        });
      },
      err => {
        console.log('Error! getAllCampaigns : ' + err);
      });
  }

  changedStatusToApprove(campaign: any, status: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: `Are you sure you want to ${status} the campaign`,
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const campainDetails = {
          id: campaign._id,
          status: status,
          name: campaign.name,
          brandId: campaign.brandId
        };
        const zone = campaign.zone ? campaign.zone : '1';
        localStorage.setItem('zone', zone);
        this.campaignService.approveCampaign(campainDetails).subscribe(
          resposne => {
            this.renderListView();
          },
          err => {
            console.log('Error! getAllCampaigns : ' + err);
          this.snackBar.open(err.error['message'], '', {
            duration: 2500,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
        });
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
    this.renderListView();
  }


}
