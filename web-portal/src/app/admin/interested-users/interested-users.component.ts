import { Component, OnInit } from '@angular/core';
import { AdminInterestedUsersService } from '../../_services/admin/interestedusers.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-interested-users',
  templateUrl: './interested-users.component.html',
  styleUrls: ['./interested-users.component.scss']
})
export class InterestedUsersComponent implements OnInit {

  public interestedUsers: any[];
  public displayedColumns: string[] = ['email', 'Requested Date', 'Last Updated Date', 'Status', 'actions'];
  constructor(private interestedUsersService: AdminInterestedUsersService,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService ) { }

  ngOnInit() {
    this.intializeGrid();
  }

  intializeGrid() {
    this.interestedUsersService.getAdminInterestedUsers().subscribe(
      res => {
        this.interestedUsers = <[]> res;
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });

      }
    );
  }

  updateStatus(id: string) {
    this.interestedUsersService.updateStatusToEmailSent(id).subscribe( res => {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'success',
          message:  res['message']
        }
      });
      this.intializeGrid();
    }, err => {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: err.error['message']
        }
      });
    })
  }

}
