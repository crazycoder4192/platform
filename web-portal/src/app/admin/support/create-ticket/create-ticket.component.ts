import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SupportService } from 'src/app/_services/admin/support.service';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Ticket } from 'src/app/_models/ticket_model';
import { AdminUsersService } from 'src/app/_services/admin/adminusers.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.scss']
})
export class CreateTicketComponent implements OnInit {
  public createTicketForm: FormGroup;
  adminUsers: Object[];
  assignToUsers: any;
  issueTypes: any;
  filteredAssignToUsers: any;
  filteredIssueTypes: any;
  constructor(
    private formBuilder: FormBuilder,
    private supportService: SupportService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private adminUsersService: AdminUsersService,
    @Inject(MAT_DIALOG_DATA) public entity: any
  ) { }

  ngOnInit() {
    this.initCreateTicketForm();
    this.issueTypes = [];
    this.entity.tickets.map((elem) => this.issueTypes.push(elem.ticketType));
    this.filteredIssueTypes = this.createTicketForm.controls['typeOfIssue'].valueChanges
    .pipe(
      startWith<string>(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => this._issueTypes(name))
    );
    this.loadAdminUsers();
  }
  loadAdminUsers() {
    this.assignToUsers = [];
    this.adminUsersService.getAdminUsers().subscribe(
      resposne => {
        this.adminUsers = <Object[]>resposne;
        this.adminUsers.map((elem) => this.assignToUsers.push(elem['email']));
        this.filteredAssignToUsers = this.createTicketForm.controls['assignTo'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._assignToUsers(name))
          );
      },
      err => {
        console.log('Error! loadAdminUsers.adminUsersService.getAdminUsers : ' + err);
    });
  }
  initCreateTicketForm() {
    this.createTicketForm = this.formBuilder.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      typeOfIssue: ['', Validators.required],
      message: ['', Validators.required],
      assignTo: ['', Validators.required]
    });
  }
  private _assignToUsers(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.assignToUsers.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }
  private _issueTypes(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.issueTypes.filter(option => (option && option.toLowerCase().indexOf(filterValue) === 0));
  }
  createTicket() {
    if (this.validateCreateTicket()) {
      const ticket = new Ticket(
        this.createTicketForm.controls['email'].value,
        this.createTicketForm.controls['name'].value,
        '',
        this.createTicketForm.controls['message'].value,
        this.createTicketForm.controls['typeOfIssue'].value,
        'New',
        '',
        '',
        [{
          commentTime: new Date(),
          commentBy: this.createTicketForm.controls['email'].value,
          comment: this.createTicketForm.controls['message'].value
        }],
        {
          email: this.createTicketForm.controls['assignTo'].value,
          at: new Date()
        },
        [{
          email: this.createTicketForm.controls['assignTo'].value,
          at: new Date()
        }]
      );
      this.supportService.addTicket(ticket).subscribe(
        resposne => {
          if (resposne['success']) {
            this.dialog.closeAll();
            this.snackBar.open(resposne['message'], resposne['success'], {
              duration: 7000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
          } else {
            this.snackBar.open(resposne['message'], resposne['success'], {
              duration: 7000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
          }
        },
        err => {
          console.log('Error! createTicket.supportService.addTicket : ' + err);
      });
    }
  }
  validateCreateTicket(): boolean {
    const formGroup = this.createTicketForm;

    const emailCtrl = formGroup.controls['email'];
    const nameCtrl = formGroup.controls['name'];
    const typeOfIssueCtrl = formGroup.controls['typeOfIssue'];
    const messageCtrl = formGroup.controls['message'];
    const assignToCtrl = formGroup.controls['assignTo'];

    let isValid = true;
    if (!emailCtrl.value) {
      emailCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!nameCtrl.value) {
      nameCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!typeOfIssueCtrl.value) {
      typeOfIssueCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!messageCtrl.value) {
      messageCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!assignToCtrl.value) {
      assignToCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }

}
