import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CreateTicketComponent } from './create-ticket/create-ticket.component';
import { SupportService } from 'src/app/_services/admin/support.service';
import { Ticket } from 'src/app/_models/ticket_model';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  displayedColumns: string[] = [
    'ticketId', 'dateAndTime', 'sentBy', 'typeOfUser', 'typeOfIssue', 'message', 'lastRepliedBy', 'status', 'actions'
  ];
  tickets: Ticket[];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;

  constructor(
    private dialog: MatDialog,
    private supportService: SupportService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadAllTickets();
  }
  createTicket(): void {
    const dialogRef = this.dialog.open(CreateTicketComponent, {
      width: '50%',
      data: {
        tickets: this.tickets
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.loadAllTickets();
    });
  }
  loadAllTickets() {
    this.supportService.getTickets().subscribe(
      resposne => {
        this.tickets = <Ticket[]>resposne;
      },
      err => {
        console.log('Error! createTicket.supportService.addTicket : ' + err);
    });
  }

}
