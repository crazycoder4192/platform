import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SupportService } from 'src/app/_services/admin/support.service';
import { Ticket } from 'src/app/_models/ticket_model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { interval } from 'rxjs';

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.scss']
})
export class TicketDetailsComponent implements OnInit {
  ticket: Ticket;
  public editTicketForm: FormGroup;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  userDetails: any;
  commentMsg: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private supportService: SupportService,
    private snackBar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.getSuportTicket();
    this.initEditTicketForm();
  }
  initEditTicketForm() {
    this.editTicketForm = this.formBuilder.group({
      comment: ['']
    });
  }
  getSuportTicket () {
    this.route.params.subscribe(params => {
      const source = interval(10000);
      this.supportService.getTicket(params.id).subscribe(
        resposne => {
          this.ticket = <Ticket>resposne['ticketDetails'];
          this.userDetails = resposne['userDetails'];
        },
        err => {
          console.log('Error! createTicket.supportService.addTicket : ' + err);
      });
      source.subscribe( () => {
        this.supportService.getTicket(params.id).subscribe(
          resposne => {
            this.ticket = <Ticket>resposne['ticketDetails'];
            this.userDetails = resposne['userDetails'];
          },
          err => {
            console.log('Error! createTicket.supportService.addTicket : ' + err);
        });
      });
    });
  }
  updateSuportTicket (type: string) {
    if (type === 'reply') {
      this.commentMsg = '';
      if (!this.editTicketForm.controls['comment'].value) {
        this.commentMsg = 'Comment is required.';
        return false;
      }
      const comment = {
        comment: this.editTicketForm.controls['comment'].value,
        commentBy: this.userEmail,
        commentTime: new Date()
      };
      this.ticket.comments.push(comment);
      this.ticket.status = 'In Progress';
    } else if (type === 'status') {
      this.ticket.status = 'Resolved';
    } else if (type === 'priority') {
      this.ticket.priority = 'High';
    }
    this.supportService.updateTicket(this.ticket).subscribe(
      resposne => {
        this.snackBar.open(resposne['message'], resposne['success'], {
          duration: 7000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.getSuportTicket();
      },
      err => {
        console.log('Error! createTicket.supportService.addTicket : ' + err);
    });
  }
}
