import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CreditVoucher } from 'src/app/_models/credit_voucher_model';
import { CreditVoucherService } from 'src/app/_services/admin/credit-voucher.service';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { CountryCurrenciesModel } from 'src/app/_models/CountryCurrenciesModel';
import { startWith, map } from 'rxjs/operators';
import { Sort } from '@angular/material/sort';
import { BankdetailsService } from 'src/app/_services/publisher/bankdetails.service';
import { AdminPublisherBillService } from 'src/app/_services/admin/publisher-bill.service';
import { AdminAdvertiserService } from 'src/app/_services/admin/advertiser.service';
import { InvoiceService } from 'src/app/_services/admin/invoices.service';
import { PaymentsService } from 'src/app/_services/admin/payments.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { PublisherBillDialogComponent } from './publisher-bill-dialog/publisher-bill-dialog.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  pageView = 'ListView';
  creditVoucherForm: FormGroup;
  invoiceForm: FormGroup;
  creditVouchers: CreditVoucher[] = [];
  countryList: any = [{'id': '1', name: 'United States'}, {'id': '2', name: 'India'}]
  private formSubmitted: boolean;
  message: any;
  status: any;
  errMessage: any;
  id: any;
  minDate: Date;
  countriesCurrencies: any[];
  filteredCountries: any;
  currencySymbol: any;
  publisherBillsList: any[];
  publisherBillsListTemp: any[];
  publisherPendingBillsList: any[];
  publisherPendingBillsListTemp: any[];
  advertiserInvoiceList: any[];
  advertiserInvoiceListTemp: any[];
  transaction: any;
  advSelectedFilter = 'pendingInvoices';
  advertiserInvoiceView = true;
  advertiserQBInvoiceView = false;
  advertiserQBPaymentView = false;
  showAdvertiserFilters = false;

  public displayedColumns: string[] = ['Advertiser', 'Amount invoiced', 'Amount received', 'Amount outstanding', 'DropDown'];
  public upcomingPaymentsAdvColumns: string[] = ['Receivables', 'Due Date', 'Amount'];
  public upcomingPaymentsPubColumns: string[] = ['Payables', 'Due Date', 'Amount'];
  public publisherDisplayedColumns: string[] = ['Publisher', 'Amount Billed',
  'Amount Paid', 'Amount outstanding', 'DropDown'];
  public bankDetails: any = [];

  constructor(private formBuilder: FormBuilder, private creditVoucherService: CreditVoucherService,
    private snackBar: MatSnackBar, private configurationService: ConfigurationService,
    private bankDetailsService: BankdetailsService,
    private adminAdvertiserService: AdminAdvertiserService,
    private invoiceService: InvoiceService,
    private paymentsService: PaymentsService,
    private adminPublisherBillService: AdminPublisherBillService,
    private dialog: MatDialog,
    private spinner: SpinnerService) {
      this.intializeAdvertiserInvoices('pending');
      this.renderPublisherBills();
      this.renderPublisherPendingBills();
    }

  ngOnInit() {

  }
  // *************************************** UPCOMING PAYMENTS START ***********************************
  sortUpcomingPublisherPaymentsData(sort: Sort) {
    const data: any = this.publisherPendingBillsList.slice();
    if (!sort.active || sort.direction === '') {
      this.publisherPendingBillsList = data;
      return;
    }

    this.publisherPendingBillsList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Payables': return compare(a.publisherName, b.publisherName, isAsc);
        case 'Amount': return compare(a.amountBilled, b.amountBilled, isAsc);
        case 'Due Date': return compare(a.platformDetails[0].billDate, b.platformDetails[0].billDate, isAsc);
        default: return 0;
      }
    });
  }

  sortUpcomingAdvertiserPaymentsData(sort: Sort) {
    const data: any = this.advertiserInvoiceList.slice();
    if (!sort.active || sort.direction === '') {
      this.advertiserInvoiceList = data;
      return;
    }

    this.advertiserInvoiceList = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Receivables': return compare(a.advertiserName, b.advertiserName, isAsc);
        case 'Amount': return compare(a.amountInvoiced, b.amountInvoiced, isAsc);
        case 'Due Date': return compare(a.amountInvoiced, b.amountInvoiced, isAsc);
        default: return 0;
      }
    });
  }

  searchUpcomingPayments(name) {
    if (!name) {
      this.publisherPendingBillsList = this.publisherPendingBillsListTemp;
      this.advertiserInvoiceList = this.advertiserInvoiceListTemp;
    } else {
      this.publisherPendingBillsList = this.publisherPendingBillsListTemp.filter(option =>
        option.publisherName.toLowerCase().indexOf( name.toLowerCase()) !== -1
      );
      this.advertiserInvoiceList = this.advertiserInvoiceListTemp.filter(option =>
        option.advertiserName.toLowerCase().indexOf( name.toLowerCase()) !== -1
      );
    }
  }

  sortByEarliestDateForPublisherPendingBills() {
    this.publisherPendingBillsList.forEach(ele => {
      ele.platformDetails.sort((a, b) => {
        return compare(a.billDate, b.billDate, true);
      });
    });
  }
  sortByEarliestDateForAdvertiserPendingInvoices() {
    this.advertiserInvoiceList.forEach(ele => {
      ele.brandDetails.sort((a, b) => {
        return compare(a.invoiceDate, b.invoiceDate, true);
      });
    });
  }
  
  // *************************************** UPCOMING PAYMENTS END ***********************************

  createFormGroup() {
    this.creditVoucherForm = this.formBuilder.group({
      country: ['', Validators.required],
      voucherName: ['', Validators.required],
      voucherCode: ['', Validators.required],
      amount: ['', Validators.required],
      expiryDate: ['', Validators.required]
    });
  }
  invoiceFormGroup() {
    this.invoiceForm = this.formBuilder.group({
      transactionNumber: ['', Validators.required],
      expiryDate: ['', Validators.required]
    });
  }
  renderPublisherBills() {
    this.pageView = 'invoiceListView';
    this.adminPublisherBillService.getPublisherBills('All').subscribe(
      result => {
        this.publisherBillsList = <[]>result;
        this.publisherBillsListTemp = <[]>result;
      },
      err => {
        console.log('Error! getAllBills : ' + err);
      });
  }
  renderPublisherPendingBills() {
    this.pageView = 'invoiceListView';
    this.adminPublisherBillService.getPublisherBills('Pending').subscribe(
      result => {
        this.publisherPendingBillsList = <[]>result;
        this.publisherPendingBillsListTemp = <[]>result;
        this.sortByEarliestDateForPublisherPendingBills();
      },
      err => {
        console.log('Error! getPendingBills : ' + err);
      });
  }

  togglePublisherExpand(element) {
    this.publisherBillsList.forEach(ele => {
      if (ele.publisherId === element.publisherId) {
        element['showPlatform'] = !element['showPlatform'];
      } else {
        ele['showPlatform'] = false;
      }
    });
  }

  markPublisherBillAsPaid(element) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = '50%';
    dialogConfig.data = {
      billId: element
    };
    const dialogRef = this.dialog.open(PublisherBillDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    this.renderPublisherBills();
    });
  }


  renderListView() {
    this.pageView = 'ListView';
    this.creditVouchers = [];
    this.creditVoucherService.getAllCreditVouchers().subscribe(
      res => {
        this.creditVouchers = [];
        this.creditVouchers = <CreditVoucher[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }
  renderAddView() {
    this.pageView = 'AddView';
    this.formSubmitted = false;
    this.errMessage = '';
    this.createFormGroup();
    this.minDate = new Date();
    this.countriesCurrenciesList();
  }
  countriesCurrenciesList() {
    this.countriesCurrencies = [];
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
        this.filteredCountries = this.creditVoucherForm.get('country').valueChanges
          .pipe(
            startWith(''),
            map(countrycurrency => countrycurrency ? this._filterCountries(countrycurrency) : this.countriesCurrencies.slice())
          );
      },
      err => {
        console.log('Error!');
      }
    );
  }
  onSubmit() {
    this.formSubmitted = true;
    if (this.creditVoucherForm.valid) {
      const creditVoucher: CreditVoucher = Object.assign({}, this.creditVoucherForm.value);
      creditVoucher.expiryDate = new Date(creditVoucher.expiryDate);
      this.creditVoucherService.addCreditVoucher(creditVoucher)
        .subscribe(
          data => {
            this.snackBar.open(data['message'], data['success'], {
              duration: 5000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderListView();
          },
          error => {
            this.errMessage = error.error['message'];
            this.status = error['success'];
          });
    }
  }
  renderEditView(id: any) {
    this.pageView = 'EditView';
    this.errMessage = '';
    this.createFormGroup();
    this.minDate = new Date();
    this.formSubmitted = false;
    this.countriesCurrenciesList();
    this.creditVoucherService.getCreditVoucherById(id).subscribe((creditVoucher: CreditVoucher) => {
      this.id = creditVoucher._id;
      this.creditVoucherForm.patchValue(creditVoucher);
    });
  }
  invoiceEditView(id) {
    this.pageView = 'invoiceEditView';
    this.errMessage = '';
    this.invoiceFormGroup();
    this.minDate = new Date();
    this.formSubmitted = false;
    this.id = id;
    this.bankDetailsService.getBill(id).subscribe(
      result => {
        this.invoiceForm.get('transactionNumber').setValue(result['transactionNumber']);
        this.invoiceForm.get('expiryDate').setValue(result['modified.at']);
      },
      err => {
        console.log('Error! getAllInvoices : ' + err);
      });
  }
  onEditSubmit() {
    this.formSubmitted = true;
    if (!this.creditVoucherForm.valid) {
      this.validateAllFormFields(this.creditVoucherForm);
    }
    if (this.creditVoucherForm.valid) {
      const creditVoucher: CreditVoucher = Object.assign({}, this.creditVoucherForm.value);
      creditVoucher._id = this.id;
      creditVoucher.expiryDate = new Date(creditVoucher.expiryDate);
      this.creditVoucherService.updateCreditVoucher(creditVoucher)
        .subscribe(
          data => {
            this.snackBar.open(data['message'], data['success'], {
              duration: 5000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderListView();
          },
          error => {
            this.message = error['message'];
            this.status = error['success'];
          });
    }
  }
  onEditInvoiceSubmit() {
    this.formSubmitted = true;
    if (this.invoiceForm.valid) {
      const invoice = Object.assign({}, this.invoiceForm.value);
      invoice._id = this.id;
      invoice.expiryDate = new Date(invoice.expiryDate);
      this.bankDetailsService.updateBill(invoice)
      .subscribe(
        data => {
          this.snackBar.open(data['message'], '', {
            duration: 5000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
          // this.renderPublisherPendingInvoices();
        },
        error => {
          this.message = error['message'];
          this.status = error['success'];
        });
    }
  }
  isFieldValid(field: string) {
    return (
      (!this.creditVoucherForm.get(field).valid && this.creditVoucherForm.get(field).touched) ||
      ((this.creditVoucherForm.get(field).untouched && this.formSubmitted))
    );
  }
  isInvoiceFieldValid(field: string) {
    return (
      (!this.invoiceForm.get(field).valid && this.invoiceForm.get(field).touched) ||
      ((this.invoiceForm.get(field).untouched && this.formSubmitted))
    );
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);

      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  removeCreditVoucher(id: any, name: any) {
    this.creditVoucherService.deleteCreditVoucher(id).subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 5000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.snackBar.open(error.error['message'], error.error['success'], {
          duration: 10000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
      }
    );
  }
  // helper function for country and currencies
  private _filterCountries(value: string): CountryCurrenciesModel[] {
    const filterValue = value.toLowerCase();
    const filteredCountries = this.countriesCurrencies.filter(
      countrycurrency =>
        countrycurrency.countryName.toLowerCase().indexOf(filterValue) === 0);
    this.currencySymbol = filteredCountries[0].symbol;
    return filteredCountries;
  }
  // end

  // *************************************** ADVERTISER PAYMENTS START ***********************************
  intializeAdvertiserInvoices(type: string) {
    this.adminAdvertiserService.getAdvertiserInvoices(type).subscribe (res => {
      this.advertiserInvoiceList = <[]>res;
      this.advertiserInvoiceListTemp = <[]>res;
      this.sortByEarliestDateForAdvertiserPendingInvoices();
    },
    err => {

    }
    );
  }

  toggleExpand(element) {
    this.advertiserInvoiceList.forEach(ele => {
      if (ele.advertiserId === element.advertiserId) {
        element['showBrand'] = !element['showBrand'];
      } else {
        ele['showBrand'] = false;
      }
    });
  }

  submitPayment(invoiceId) {
    const invoiceIds: string[] = [];
    invoiceIds.push(invoiceId);
    this.invoiceService.processInvoices(invoiceIds).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res
          }
        });
      },
      err => {

      }
    );
  }

  changeAdvertiserInvoiceView() {
    this.intializeAdvertiserInvoices(this.advSelectedFilter);
  }

  updateQBPayment(invoiceId) {
    const invoiceIds: string[] = [];
    invoiceIds.push(invoiceId);
    this.paymentsService.updateQBPayment(invoiceIds).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res
          }
        });
      },
      err => {

      }
    );
  }
  updateQBInvoice(invoiceId) {
    const invoiceIds: string[] = [];
    invoiceIds.push(invoiceId);
    this.paymentsService.updateQBInvoice(invoiceIds).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res
          }
        });
      },
      err => {

      }
    );
  }
  displayAdvertiserFilters() {
    this.showAdvertiserFilters = !this.showAdvertiserFilters;
  }

  getBankDetailsListForPublisher(zone: any){
    zone = (!zone) ? '0': zone;
    this.bankDetailsService.getPublisherBankDetails({"zone": zone}).subscribe(
      (res: any) => {
        this.bankDetails = res;
      },
      (error: any) => {

      }
    )
  }

  selectCountry(event){
    this.getBankDetailsListForPublisher(event.value);
  }
  // *************************************** ADVERTISER PAYMENTS END ***********************************
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
