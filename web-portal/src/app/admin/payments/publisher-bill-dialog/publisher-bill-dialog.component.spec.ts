import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherBillDialogComponent } from './publisher-bill-dialog.component';

describe('PublisherBillDialogComponent', () => {
  let component: PublisherBillDialogComponent;
  let fixture: ComponentFixture<PublisherBillDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherBillDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherBillDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
