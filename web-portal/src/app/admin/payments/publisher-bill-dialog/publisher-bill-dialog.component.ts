import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AdminPublisherBillService } from 'src/app/_services/admin/publisher-bill.service';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-publisher-bill-dialog',
  templateUrl: './publisher-bill-dialog.component.html',
  styleUrls: ['./publisher-bill-dialog.component.scss']
})
export class PublisherBillDialogComponent implements OnInit {

  public publisherBillForm: FormGroup;
  public billId;
  public transactionDateMax = new Date();

  constructor(private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private spinner: SpinnerService,
    private adminPublisherBillService: AdminPublisherBillService,
    private dialogRef: MatDialogRef<PublisherBillDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.billId = data.billId;
  }

  get f() { return this.publisherBillForm.controls; }
  ngOnInit() {
    this.publisherBillForm = this.formBuilder.group({
      transactionNumber: ['', Validators.required],
      transactionDate: ['', Validators.required]
    });
  }

  onSubmit() {
    this.adminPublisherBillService.updatePublisherBillAsPaid(this.billId, this.publisherBillForm.value).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
        this.dialogRef.close();
      }
    );
  }

}
