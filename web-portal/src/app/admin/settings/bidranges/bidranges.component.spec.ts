import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidrangesComponent } from './bidranges.component';

describe('BidrangeComponent', () => {
  let component: BidrangesComponent;
  let fixture: ComponentFixture<BidrangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidrangesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidrangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
