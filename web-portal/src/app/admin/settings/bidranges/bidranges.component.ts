import { Component, OnInit } from '@angular/core';
import { BidRangeService } from 'src/app/_services/admin/bidrange.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { BidRangeModel } from '../../../_models/BidRangeModel';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { CountryCurrenciesModel } from '../../../_models/CountryCurrenciesModel';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-settings-bidranges',
  templateUrl: './bidranges.component.html',
  styleUrls: ['./bidranges.component.scss']
})
export class BidrangesComponent implements OnInit {

  pageView = 'ListView';
  displayedColumns: string[] = ['country', 'cpm', 'cpc', 'cpv', 'addbidrange'];
  bidRangeDataSource = [];
  countriesCurrencies: CountryCurrenciesModel[] = [];
  countries: string[] = [];
  currencySymbols: string[] = [];
  bidRangeForm: FormGroup;
  formSubmitted: Boolean = false;
  filteredCountries: Observable<CountryCurrenciesModel[]>;
  message: string;
  status: string;
  errorMessage: any;
  currencySymbol: string;

  constructor(private bidRangeService: BidRangeService, private configurationService: ConfigurationService,
    private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.bidRangeDataSource = [];
    this.countriesCurrencies = [];
  }

  ngOnInit() {
    this.renderListView();
    this.createFormGroup();
  }

  get f() { return this.bidRangeForm.controls; }

  createFormGroup() {
    this.bidRangeForm = this.formBuilder.group({
        _id: [null],
        country: ['', Validators.required],
        currencyCode: ['', Validators.required],
        currencySymbol: ['', Validators.required],
        cpm: this.formBuilder.group({
          min: ['', Validators.required],
          max: ['', Validators.required]
        }),
        cpc: this.formBuilder.group({
          min: ['', Validators.required],
          max: ['', Validators.required]
        }),
        cpv: this.formBuilder.group({
          min: ['', Validators.required],
          max: ['', Validators.required]
        })
    });
  }

  isFieldValid(field: string) {
    return (
      (!this.bidRangeForm.get(field).valid && this.bidRangeForm.get(field).touched) ||
      ((this.bidRangeForm.get(field).untouched && this.formSubmitted))
    );
  }

  countriesCurrenciesList() {
    this.countriesCurrencies = [];
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
        this.filteredCountries = this.bidRangeForm.get('country').valueChanges
          .pipe(
            startWith(''),
            map(countrycurrency => countrycurrency ? this._filterCountries(countrycurrency) : this.countriesCurrencies.slice())
          );
      },
      err => {
        console.log('Error!');
      }
    );
  }

  private _filterCountries(value: string): CountryCurrenciesModel[] {
    const filterValue = value.toLowerCase();
    const filteredCountries = this.countriesCurrencies.filter(
      countrycurrency =>
      countrycurrency.countryName.toLowerCase().indexOf(filterValue) === 0);
    if (filteredCountries.length === 1) {
      this.currencySymbol = filteredCountries[0].symbol;
      this.bidRangeForm.get('currencyCode').setValue(filteredCountries[0].currencyCode);
      this.bidRangeForm.get('currencySymbol').setValue(filteredCountries[0].symbol);
    }
    return filteredCountries;
  }

  renderListView() {
    this.pageView = 'ListView';
    this.bidRangeDataSource = [];
    this.bidRangeService.getAllBidRanges().subscribe(
      res => {
        this.bidRangeDataSource = [];
        this.bidRangeDataSource = <BidRangeModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }

  renderAddView() {
    this.pageView = 'AddView';
    this.formSubmitted = false;
    this.createFormGroup();
    this.countriesCurrenciesList();
  }

  renderEditView(id: any) {
    this.pageView = 'EditView';
    this.formSubmitted = false;
    this.countriesCurrenciesList();
    this.bidRangeService.getBidRangeById(id).subscribe((bidRange: BidRangeModel) => {
      this.currencySymbol = bidRange.currencySymbol;
      this.bidRangeForm.patchValue(bidRange);
    });
  }

  removeBidRange(id: any) {
    this.bidRangeService.deleteBidRange(id).subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.snackBar.open(error.error['message'], error.error['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
      }
    );
  }

  onSubmit() {
    this.formSubmitted = true;
    if (this.bidRangeForm.valid) {
      const bidRange: BidRangeModel = Object.assign({}, this.bidRangeForm.value);
      this.bidRangeService.addBidRange(bidRange)
      .subscribe(
          data => {
            this.snackBar.open(data['message'], data['success'], {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.renderListView();
          },
          error => {
            this.errorMessage = error['message'];
            this.status = error['success'];
          }
      );
    }
  }

  onEditSubmit() {
    this.formSubmitted = true;
    if (!this.bidRangeForm.valid) {
      this.validateAllFormFields(this.bidRangeForm);
    }
    if (this.bidRangeForm.valid) {
      const bidRange: BidRangeModel = Object.assign({}, this.bidRangeForm.value);
      console.log(bidRange);
      this.bidRangeService.updateBidRange(bidRange)
      .subscribe(
        data => {
          this.snackBar.open(data['message'], data['success'], {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['success-campaign-notify']
          });
          this.renderListView();
        },
        error => {
          this.errorMessage = error['message'];
          this.status = error['success'];
        }
      );
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);

      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
