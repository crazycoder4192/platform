import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { CountryTaxModel } from '../../../_models/CountryTaxModel';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CountryTaxService } from 'src/app/_services/admin/countrytax.service';
import { MatSnackBar } from '@angular/material';
import { ZipcodesService } from 'src/app/_services/utils/zipcodes.service';
import { map, startWith } from 'rxjs/operators';
import { UtilService } from 'src/app/_services/utils/util.service';
import { CountryCurrenciesModel } from 'src/app/_models/CountryCurrenciesModel';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';

@Component({
  selector: 'app-settings-countrytaxes',
  templateUrl: './countrytaxes.component.html',
  styleUrls: ['./countrytaxes.component.scss']
})
export class CountrytaxesComponent implements OnInit {
  @ViewChild('alert') alert: ElementRef;

  countryTaxForm: FormGroup;
  private formSubmitted: boolean;
  pageView = 'ListView';
  displayedColumns: string[] = ['country', 'address', 'typeOfTax', 'percentageOfTax', 'taxNumber', 'addcountry'];
  dataSource: CountryTaxModel[] = [];
  message: string;
  status: string;
  uniqueCities: string[];
  filteredCities: any;
  isValidCity: boolean;
  cityStateDetails: string[];
  uniqueStates: any[];
  uniqueCountries: any[];
  filteredStates: any;
  errorMessage: any;
  countriesCurrencies: any;
  filteredCountries: any;
  countrySelected: any;

  constructor(private countryTaxService: CountryTaxService,
    private formBuilder: FormBuilder, private snackBar: MatSnackBar, private zipcodesService: ZipcodesService,
    private utilService: UtilService, private configurationService: ConfigurationService) {
    this.dataSource = [];
    this.countrySelected = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  }

  ngOnInit() {
    this.renderListView();
    this.createFormGroup();
    this.initUniqueCities();
    this.initUniqeStates();
  }

  renderListView() {
    this.pageView = 'ListView';
    this.dataSource = [];
    this.countryTaxService.getAllCountryTaxes().subscribe(
      res => {
        this.dataSource = [];
        this.dataSource = <CountryTaxModel[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }
  createFormGroup() {
    this.countryTaxForm = this.formBuilder.group({
        _id: [null],
        country: ['', Validators.required],
        address: this.formBuilder.group({
          addressLine1: ['', Validators.required],
          addressLine2: ['', Validators.required],
          city: ['', Validators.required],
          state: ['', Validators.required],
          zipcode: ['', Validators.required],
        }),
        typeOfTax: ['', Validators.required],
        percentageOfTax: ['', Validators.required],
        taxRegistrationNumber: ['', Validators.required]
    });
  }

  get f() { return this.countryTaxForm.controls; }

  onSubmit() {
    this.formSubmitted = true;
    // if (this.countryTaxForm.untouched && this.countryTaxForm.pristine) {
     // return;
    // }
    if (this.countryTaxForm.valid) {
      const countryTax: CountryTaxModel = Object.assign({}, this.countryTaxForm.value);
      this.countryTaxService.addCountryTax(countryTax)
      .subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.errorMessage = error['message'];
        this.status = error['success'];
      });
    }
  }

  onEditSubmit() {
    this.formSubmitted = true;
    if (!this.countryTaxForm.valid) {
      this.validateAllFormFields(this.countryTaxForm);
    }
    if (this.countryTaxForm.valid) {
      const countryTax: CountryTaxModel = Object.assign({}, this.countryTaxForm.value);
      this.countryTaxService.updateCountryTax(countryTax)
      .subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.errorMessage = error['message'];
        this.status = error['success'];
      });
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);

      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return (
      (!this.countryTaxForm.get(field).valid && this.countryTaxForm.get(field).touched) ||
      ((this.countryTaxForm.get(field).untouched && this.formSubmitted))
    );
  }

  renderAddView() {
    this.pageView = 'AddView';
    this.formSubmitted = false;
    this.createFormGroup();
    this.countriesCurrenciesList();
  }

  renderEditView(id: any) {
    this.pageView = 'EditView';
    this.formSubmitted = false;
    this.countriesCurrenciesList();
    this.countryTaxService.getCountryTaxById(id).subscribe((countryTax: CountryTaxModel) => {
      this.countryTaxForm.patchValue(countryTax);
    });
  }

  removeCountryTax(id: any, name: any) {
    this.countryTaxService.deleteCountryTax(id).subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.snackBar.open(error.error['message'], error.error['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
      }
    );
  }
  // get cities for city dropdowns.
  initUniqueCities() {
    this.zipcodesService.getUniqueCities(this.countrySelected).subscribe(
      res => {
        this.uniqueCities = res;
        this.filteredCities = this.countryTaxForm.get('address.city').valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
          );
      },
      err => {
        console.log('Error!');
      });
  }
  // end
  initUniqeStates() {
    this.zipcodesService.getUniqueStates(this.countrySelected).subscribe(
      res => {
        this.uniqueStates = res;
        this.filteredStates = this.countryTaxForm.get('address.state').valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterState(name))
          );
      });
  }
  countriesCurrenciesList() {
    this.countriesCurrencies = [];
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
        this.filteredCountries = this.countryTaxForm.get('country').valueChanges
          .pipe(
            startWith(''),
            map(countrycurrency => countrycurrency ? this._filterCountries(countrycurrency) : this.countriesCurrencies.slice())
          );
      },
      err => {
        console.log('Error!');
      }
    );
  }
  // helper function for city
  private _filterCity(name: string): string[] {
    const filterValue = name;
    return this.uniqueCities.filter(option => option.indexOf(filterValue) === 0);
  }
  private _filterState(name: string): string[] {
    const filterValue = name;
    return this.uniqueStates.filter(option => option.indexOf(filterValue) === 0);
  }
  // helper function for country and currencies
  private _filterCountries(value: string): CountryCurrenciesModel[] {
    const filterValue = value.toLowerCase();
    const filteredCountries = this.countriesCurrencies.filter(
      countrycurrency =>
      countrycurrency.countryName.toLowerCase().indexOf(filterValue) === 0);
      return filteredCountries;
  }
  // end
  // end
  // searching cities by prefix.
  onChangeCityString() {
    if (this.countryTaxForm.get('address.city').value.length > 1) {
      this.countryTaxForm.get('address.state').setValue('');
      this.countryTaxForm.get('address.zipcode').setValue('');
      this.zipcodesService.getUniqueCitiesOnChangeCity(this.countryTaxForm.get('address.city').value, this.countrySelected).subscribe(
        res => {
          this.uniqueCities = res;
          this.filteredCities = this.countryTaxForm.get('address.city').valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueCities();
    }
  }
  // end
  // call on city change and change the country state and pin.
  onChangeCity() {
    if (this.uniqueCities.length) {
      const cityName = this.countryTaxForm.get('address.city').value;
      this.isValidCity = this.uniqueCities.includes(cityName);
      if (this.isValidCity) {
        this.countryTaxForm.get('address.state').setValue('');
        this.countryTaxForm.get('address.zipcode').setValue('');
        this.utilService.getCityDetailsByCityName(cityName, this.countrySelected).subscribe(
          res => {
            this.cityStateDetails = res;
            this.uniqueStates = [];
            this.uniqueCountries = [];
            this.uniqueStates = this.cityStateDetails.map(function(item) {
              return item['state_full_name'];
            });
            this.uniqueStates = Array.from(new Set(this.uniqueStates));
            this.filteredStates = this.countryTaxForm.get(['address', 'city']).valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => this._filterState(name))
            );
          },
          err => {
            console.log(err);
          }
        );
      }
    }
  }
  // end

}
