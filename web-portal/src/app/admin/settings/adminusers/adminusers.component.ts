import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { AdminUsersService } from 'src/app/_services/admin/adminusers.service';
import { USEROLESElement } from '../../configuration/dataModel/userRolesPermissionsModel';
import { MatSnackBar } from '@angular/material';
import { User, AdminUser } from 'src/app/_models/users';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-settings-adminusers',
  templateUrl: './adminusers.component.html',
  styleUrls: ['./adminusers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminusersComponent implements OnInit {
  displayedColumns: string[] = ['firstname', 'lastname', 'email', 'status', 'role', 'adduser'];
  pageView = 'ListView';
  adminUserList: AdminUser[] = [];
  adminUserForm: FormGroup;
  deleteUserForm: FormGroup;
  private formSubmitted: boolean;
  public listOfExistedUserRolesPermissions: Array<USEROLESElement>;
  filteredUserRolesPermissions: Observable<USEROLESElement[]>;
  public selectedRole: USEROLESElement;
  public userEmail = '';
  public usertype = 'admin';
  errorMessage: any;
  status: string;
  deleteUserDetails: any;

  constructor(private configHTTPService: ConfigurationService,
    private adminUsersService: AdminUsersService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,private spinner: SpinnerService) {
      this.adminUserList = [];
      this.listOfExistedUserRolesPermissions = [];
      this.adminRolesAndPermissions();
  }

  ngOnInit() {
    this.renderListView();
  }

  get f() { return this.adminUserForm.controls; }

  createFormGroup() {
    this.adminUserForm = this.formBuilder.group({
        _id: [null],
        email: ['', Validators.required],
        userType: ['admin'],
        userRole: ['', Validators.required]
    });
    this.deleteUserForm = this.formBuilder.group({
        _id: [null],
        email: ['', Validators.required]
    });
  }

  renderListView() {
    this.pageView = 'ListView';
    this.adminUserList = [];
    this.adminUsersService.getAdminUsers().subscribe(
      res => {
        this.adminUserList = <AdminUser[]>res;
      },
      err => {
        console.log('Error!');
      }
    );
  }

  adminRolesAndPermissions() {
    this.configHTTPService.getalluserolesByModuleType('Admin').subscribe(
      res => {
        if (res) {
          this.listOfExistedUserRolesPermissions = <USEROLESElement[]>res;
        }
      },
      err => {
        this.snackBar.open('Error!', err, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['success-campaign-notify']
      });
      }
    );
  }

  displayFn(userRolePermission?: USEROLESElement): string | undefined {
    return userRolePermission ? userRolePermission.roleName : undefined;
  }

  filterUserRoles() {
    this.filteredUserRolesPermissions = this.adminUserForm.get('userRole').valueChanges
    .pipe(
      startWith<string | USEROLESElement>(''),
      map(value => typeof value === 'string' ? value : value.roleName),
      map(userrolepermissions => userrolepermissions ?
        this._filterUserRolesPermissions(userrolepermissions) : this.listOfExistedUserRolesPermissions.slice())
    );
  }

  private _filterUserRolesPermissions(value: string): USEROLESElement[] {
    const filterValue = value.toLowerCase();
    const filteredRolesPermissions = this.listOfExistedUserRolesPermissions.filter(
      userrolepermissions =>
      userrolepermissions.roleName.toLowerCase().indexOf(filterValue) === 0);
    return filteredRolesPermissions;
  }

  private _filterUserRolesPermissionsById(value: string): USEROLESElement[] {
    return this.listOfExistedUserRolesPermissions.filter( userrolepermissions => userrolepermissions._id.indexOf(value) === 0);
  }

  isFieldValid(field: string) {
    return (
      (!this.adminUserForm.get(field).valid && this.adminUserForm.get(field).touched) ||
      ((this.adminUserForm.get(field).untouched && this.formSubmitted))
    );
  }

  renderAddView() {
    this.pageView = 'AddView';
    this.formSubmitted = false;
    this.createFormGroup();
    this.adminRolesAndPermissions();
    this.filterUserRoles();
  }

  renderDeleteView() {
    this.pageView = 'DeleteView';
    this.createFormGroup();
  }

  renderEditView(id: any) {
    this.pageView = 'EditView';
    this.formSubmitted = false;
    this.createFormGroup();
    this.adminRolesAndPermissions();
    this.filterUserRoles();
    this.adminUsersService.getAdminUserById(id).subscribe((adminUser: AdminUser) => {
      this.adminUserForm.patchValue(adminUser);
      const role = this._filterUserRolesPermissionsById(adminUser.userRole);
      this.adminUserForm.patchValue({
        userRole:  role[0]
      });
    });
  }
  onSearch() {
    this.spinner.showSpinner.next(true);
    this.adminUsersService.getUserByEmail(this.deleteUserForm.get('email').value).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.pageView = 'DeleteDetailsView';
        this.deleteUserDetails = res;
      },
      error => {
        this.spinner.showSpinner.next(false);
        console.log('Error in onSearch adminUsersService.getAdminUserById');
      }
    );
  }

  onSubmit() {
    this.formSubmitted = true;
    if (this.adminUserForm.valid) {
      const adminUser = {
        'email': this.adminUserForm.get('email').value,
        'userType': this.adminUserForm.get('userType').value,
        'userRole': this.adminUserForm.get('userRole').value._id
      };
      this.adminUsersService.addAdminUser(adminUser)
      .subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.errorMessage = error.error['message'];
        this.status = error.error['success'];
      });
    }
  }

  deletUser() {
    this.spinner.showSpinner.next(true);
    this.adminUsersService.deleteUserByEmail(this.deleteUserForm.get('email').value).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        console.log('Success in deletUser adminUsersService.deleteUserByEmail');
      },
      error => {
        this.spinner.showSpinner.next(false);
        console.log('Error in deletUser adminUsersService.deleteUserByEmail');
      }
    );
  }

  onEditSubmit() {
    this.formSubmitted = true;
    if (this.adminUserForm.valid) {
      const adminUser = {
        '_id': this.adminUserForm.get('_id').value,
        'email': this.adminUserForm.get('email').value,
        'userType': this.adminUserForm.get('userType').value,
        'userRole': this.adminUserForm.get('userRole').value._id
      };
      this.adminUsersService.updateAdminUser(adminUser)
      .subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.errorMessage = error.error['message'];
        this.status = error.error['success'];
      });
    }
  }

  removeUser(id: any) {
    this.adminUsersService.deleteAdminUser(id).subscribe(
      data => {
        this.snackBar.open(data['message'], data['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
        this.renderListView();
      },
      error => {
        this.snackBar.open(error.error['message'], error.error['success'], {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['success-campaign-notify']
        });
      }
    );
  }

}
