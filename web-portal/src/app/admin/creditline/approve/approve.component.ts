import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdvertiserCreditLineService } from 'src/app/_services/advertiser/advertiser-credit-line.service';
import { MatSnackBar, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: ['./approve.component.scss']
})
export class ApproveComponent implements OnInit {
  approvedCreditLineForm: FormGroup;
  isSubmitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ApproveComponent>
  ) { }

  ngOnInit() {
    this.approvedCreditLineForm = this.formBuilder.group({
      approvedCreditLimit: ['', Validators.compose([Validators.required])]
    });
  }
  approveCreditLine() {
    this.isSubmitted = true;
    if (this.approvedCreditLineForm.valid) {
      const approvedCreditLimit = this.approvedCreditLineForm.get('approvedCreditLimit').value;
      this.dialogRef.close(approvedCreditLimit);
    }
  }

}
