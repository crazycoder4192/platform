import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-suspend',
  templateUrl: './suspend.component.html',
  styleUrls: ['./suspend.component.scss']
})
export class SuspendComponent implements OnInit {
  rejectionCreditLineForm: FormGroup;
  isSubmitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<SuspendComponent>
  ) { }

  ngOnInit() {
    this.rejectionCreditLineForm = this.formBuilder.group({
      rejectionReason: ['', Validators.compose([Validators.required])]
    });
  }
  suspendCreditLine() {
    this.isSubmitted = true;
    if (this.rejectionCreditLineForm.valid) {
      const rejectionReason = this.rejectionCreditLineForm.get('rejectionReason').value;
      this.dialogRef.close(rejectionReason);
    }
  }
}
