import { Component, OnInit } from '@angular/core';
import { AdvertiserCreditLineService } from 'src/app/_services/advertiser/advertiser-credit-line.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ApproveComponent } from './approve/approve.component';
import { SuspendComponent } from './suspend/suspend.component';
import { ReviewComponent } from './review/review.component';

@Component({
  selector: 'app-creditline',
  templateUrl: './creditline.component.html',
  styleUrls: ['./creditline.component.scss']
})
export class CreditlineComponent implements OnInit {
  public creditLines: any;
  status: any;

  constructor(
    private advertiserCreditLineService: AdvertiserCreditLineService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) {

  }

  ngOnInit() {
    this.getCreditLines('Approved');
  }
  getCreditLines(status) {
    this.status = status;
    this.advertiserCreditLineService.getCreditLinesByStatus(status).subscribe(
      res => {
        this.creditLines = res;
      },
      err => {
        console.log('Error!');
    });
  }

  preview(data){
    data.documents.map((item)=>{
      window.open(item.location);
    })
  }
  reviewCreditLine(creditLine) {
    const dialogRef = this.dialog.open(ReviewComponent, {
      width: '50%',
      data: {creditLine},
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Review Component closed!');
      }
    });
  }
  approveCreditLine(creditLine) {
    const dialogRef = this.dialog.open(ApproveComponent, {
      width: '50%',
      data: {},
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        creditLine.approvedCreditLimit = result;
        this.advertiserCreditLineService.approveCreditLine(creditLine).subscribe(
          res => {
            this.snackBar.open(res['message'], '', {
              duration: 2500,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.getCreditLines(this.status);
          },
          err => {
            console.log('Error!');
        });
      }
    });
  }
  disproveCreditLine(creditLine) {
    const dialogRef = this.dialog.open(SuspendComponent, {
      width: '50%',
      data: {},
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        creditLine.rejectionReason = result;
        this.advertiserCreditLineService.disproveCreditLine(creditLine).subscribe(
          res => {
            this.snackBar.open(res['message'], '', {
              duration: 2500,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['success-campaign-notify']
            });
            this.getCreditLines(this.status);
          },
          err => {
            console.log('Error!');
        });
      }
    });
  }

}
