import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  public uploadedDocuments: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.uploadedDocuments = (data && data.creditLine && data.creditLine.documents.length > 0) ?
                              data.creditLine.documents : [];
   }

  ngOnInit() {
  }

}
