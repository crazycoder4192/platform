import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminPublisherService } from '../../_services/admin/publisher.service';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class PublisherComponent implements OnInit {
  show = false;
  publisher;
  public displayedColumns: string[] = ['Organization Name', 'Country', 'Platforms', 'ActiveAssets', 'Earnings', 'actions', 'DropDown'];
  months = 'Filter by period';
  sDate = new Date();
  eDate = new Date();
  public publisherList: any = [];
  public platformList;
  constructor(private router: Router,
    private adminPublisherService: AdminPublisherService) {
    this.adminPublisherService.getPublishers().subscribe(
      res => {
        this.publisherList = res;
      },
      err => {

      }
    );
   }


  ngOnInit() {
  }

  toggleSearch() {
    this.show = !this.show;
  }
  searchPublisher(name) {
    return name;
  }
  searchPublisherList(startDate, endDate, period) {
    console.log(startDate, endDate);
  }
  toggleExpand(element) {
    this.publisherList.forEach(ele => {
      if (ele.publisherId === element.publisherId) {
        element['showPlatform'] = !element['showPlatform'];
      } else {
        ele['showPlatform'] = false;
      }
    });
  }

  editPlatform(platformData) {
    localStorage.setItem('zone', platformData.zone);
    this.router.navigate(['/admin/publisher/hcpscript'],
    { state: { 'platformId':  JSON.stringify(platformData.platformId)} });
  }
}
