import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublisherPlatformService } from '../../../_services/publisher/publisher-platform.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ValidateDomain } from 'src/app/common/validators/validator';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AdminPublisherService } from 'src/app/_services/admin/publisher.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-publisher-hcp-script',
  templateUrl: './publisher-hcp-script.component.html',
  styleUrls: ['./publisher-hcp-script.component.scss']
})
export class PublisherHcpScriptComponent implements OnInit {
  public platformId;
  public platformFormGroup: FormGroup;
  private platform: any;
  countrySelected: any;
  constructor(private router: Router,
    private platformService: PublisherPlatformService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private spinner: SpinnerService,
    private adminPublisherService: AdminPublisherService) {
    this.countrySelected = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    if (this.router.getCurrentNavigation().extras.state) {
      if (this.router.getCurrentNavigation().extras.state.platformId) {
        this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.platformId);
      }
    }
   }

  ngOnInit() {
    this.platformFormGroup = this.formBuilder.group({
      _id: [this.platformId],
      platformType: [],
      name: ['', Validators.required],
      domainUrl: ['', [Validators.required]],
      trustedSites: [''],
      appType: [''],
      typeOfSite: ['', Validators.required],
      description: ['', Validators.required],
      excludeAdvertisers: [],
      gaAPIKey: [''],
      fileType: ['CSV'],
      isHCPUploaded: [],
      isDeleted: [false],
      isDeactivated: [false],
      publisherId: [],
      file: [''],
      hcpScript: ['']
    },
    {
      validator: [ValidateDomain('domainUrl')]
    });

    this.platformService.getPlatformById(this.platformId).subscribe(
      res => {
        this.platform = res;
        this.fillTheForm();
      },
      err => {
          console.log(err);
      });
  }

  fillTheForm() {
    this.platformFormGroup.get('_id').setValue(this.platform._id);
    this.platformFormGroup.get('name').setValue(this.platform.name);
    this.platformFormGroup.get('platformType').setValue(this.platform.platformType);
    this.platformFormGroup.get('domainUrl').setValue(this.platform.domainUrl);
    this.platformFormGroup.get('trustedSites').setValue(this.platform.trustedSites);
    this.platformFormGroup.get('appType').setValue(this.platform.appType);
    this.platformFormGroup.get('typeOfSite').setValue(this.platform.typeOfSite);
    this.platformFormGroup.get('description').setValue(this.platform.description);
    // this.platformFormGroup.get('keywords').setValue(this.platform.keywords);
    this.platformFormGroup.get('excludeAdvertisers').setValue(this.platform.excludeAdvertiserTypes);
    this.platformFormGroup.get('gaAPIKey').setValue(this.platform.gaApiKey);
    this.platformFormGroup.get('isHCPUploaded').setValue(this.platform.isHCPUploaded);
    this.platformFormGroup.get('isDeactivated').setValue(this.platform.isDeactivated);
    this.platformFormGroup.get('isDeleted').setValue(this.platform.isDeleted);
    this.platformFormGroup.get('publisherId').setValue(this.platform.publisherId);
    this.platformFormGroup.get('hcpScript').setValue(this.platform.hcpScript);
  }

  updatePlatform () {
    this.platformService.updatePlatform(this.platformFormGroup.value, this.countrySelected).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: 'Platform Updated Successfully'
          }
        });
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      });
  }

  sendNotification() {
    this.adminPublisherService.notifyPublisherForHCPScript(this.platform._id).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }
}
