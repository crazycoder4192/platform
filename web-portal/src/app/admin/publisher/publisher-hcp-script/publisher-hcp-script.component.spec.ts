import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherHcpScriptComponent } from './publisher-hcp-script.component';

describe('PublisherHcpScriptComponent', () => {
  let component: PublisherHcpScriptComponent;
  let fixture: ComponentFixture<PublisherHcpScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherHcpScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherHcpScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
