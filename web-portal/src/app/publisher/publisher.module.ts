import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PublisherRoutingModule } from './publisher-routing.module';
import { PublisherComponent } from './publisher.component';
import { AssetCreationComponent } from './asset-creation/asset-creation.component';
import { AssetManageComponent } from './asset-manage/asset-manage.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { PaymentsComponent } from './payments/payments.component';
import { SettingsComponent } from './settings/settings.component';
import { PlatformCreationComponent } from './platform-creation/platform-creation.component';
import { PublisherProfileComponent } from './publisher-profile/publisher-profile.component';
import { CommonModulesModule } from '../common/common-modules/common-modules.module';
import { PublisherProfileThanksComponent } from './publisher-profile-thanks/publisher-profile-thanks.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { PublisherPlatformThanksComponent } from './publisher-platform-thanks/publisher-platform-thanks.component';
import { PopupCsvHcpComponent } from './popup-csv-hcp/popup-csv-hcp.component';
import { PlatformManageComponent } from './platform-manage/platform-manage.component';
import { DeleteConfirmPopupComponent } from '../common/delete-confirm-popup/delete-confirm-popup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PlatformPanelComponent } from './platform-panel/platform-panel.component';
import { AskAQuestionComponent } from './ask-a-question/ask-a-question.component';
import { PublisherSupportComponent } from './publisher-support/publisher-support.component';
import { PublisherTicketDetailsComponent } from './publisher-support/publisher-ticket-details/publisher-ticket-details.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { BankDetailsComponent } from './payments/bank-details/bank-details.component';
import { PaymentsReceivedComponent } from './payments/payments-received/payments-received.component';
import { AssetCreationInstructionsComponent } from '../common/asset-creation-instructions/asset-creation-instructions.component';
import { TermsAndConditionsComponent } from '../common/terms-and-conditions/terms-and-conditions.component';
//import { OptValidationComponent } from '../common/opt-validation/opt-validation.component';
import { ProfilePanelComponent } from './profile-panel/profile-panel.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { AssetThanksComponent } from './asset-creation/asset-thanks/asset-thanks.component';
import { PlatformThanksFileUploadComponent } from './platform-creation/platform-thanks-file-upload/platform-thanks-file-upload.component';
import { InvitationCodeComponent } from './invitation-code/invitation-code.component';
import { DeveloperShareComponent } from './developer-share/developer-share.component';
import { ConfirmPasswordDailogComponent } from '../common/confirm-password-dailog/confirm-password-dailog.component';

@NgModule({
  declarations: [PublisherComponent,
     AssetCreationComponent,
      AssetManageComponent,
       AnalyticsComponent,
        PaymentsComponent,
         SettingsComponent,
          PlatformCreationComponent,
           PublisherProfileComponent,
            PublisherProfileThanksComponent,
            PublisherPlatformThanksComponent,
            PopupCsvHcpComponent,
            PlatformManageComponent,
            DashboardComponent,
            PlatformPanelComponent,
            AskAQuestionComponent,
            PublisherSupportComponent,
            PublisherTicketDetailsComponent,
            BankDetailsComponent,
            PaymentsReceivedComponent,
            ProfilePanelComponent,
            DashboardDetailsComponent,
            AssetThanksComponent,
            PlatformThanksFileUploadComponent,
            InvitationCodeComponent,
            DeveloperShareComponent
            ],
  imports: [
    CommonModule,
    PublisherRoutingModule,
    FormsModule,
    CommonModulesModule,
    NgMultiSelectDropDownModule.forRoot(),
    TooltipModule
  ],
  entryComponents: [
    PopupCsvHcpComponent,
    DeleteConfirmPopupComponent,
    AssetCreationInstructionsComponent,
    TermsAndConditionsComponent,
    DeveloperShareComponent,
    ConfirmPasswordDailogComponent
 //   OptValidationComponent
  ]
})
export class PublisherModule { }
