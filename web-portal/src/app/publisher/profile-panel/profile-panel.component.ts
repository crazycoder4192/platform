import { Component, OnInit, EventEmitter, Output, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';

@Component({
  selector: 'app-profile-panel',
  templateUrl: './profile-panel.component.html',
  styleUrls: ['./profile-panel.component.scss']
})
export class ProfilePanelComponent implements OnInit {
  @Output() private toggleProfile = new EventEmitter<boolean>();
  @Output() private toggleProfileIcon = new EventEmitter<boolean>();
  clickState: boolean;
  rolePermissions: any;
  public accountsTab = false;
  public hasPersonalProfile = this.authService.currentUserValue.hasPersonalProfile;
  constructor(
    private router: Router,
    private eventEmitterService: EventEmitterService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.setPermissions();
  }
  @HostListener('click')
  clickInside() {
    this.clickState = false;
  }
  @HostListener('document:click')
  clickout() {
    if (this.clickState) {
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();
    }
    this.clickState = true;
  }
  gotoClickedLink(linkType: string) {
    if (linkType === 'profile') {
      localStorage.setItem('activeIcon', JSON.stringify('profile'));
      this.eventEmitterService.onClickNavigation();
      this.router.navigate(['/publisher/publisherProfile']);
      this.toggleProfile.emit(false);
    }
    if (linkType === 'email_settings') {
      localStorage.setItem('settingType', JSON.stringify('email_setings'));
      this.router.navigate(['/publisher/settings']);
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      this.eventEmitterService.onClickAccountSetting();
    }
    if (linkType === 'change_password') {
      localStorage.setItem('settingType', JSON.stringify('change_password'));
      this.router.navigate(['/publisher/settings']);
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      this.eventEmitterService.onClickAccountSetting();
    }
    if (linkType === 'invitation_code') {
      localStorage.setItem('settingType', JSON.stringify('invitation_code'));
      this.router.navigate(['/publisher/invitationCode']);
      this.toggleProfile.emit(false);
      this.toggleProfileIcon.emit(false);
      this.eventEmitterService.onClickAccountSetting();
    }
  }
  setPermissions() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    if (this.rolePermissions && this.rolePermissions.length) {
      this.rolePermissions.forEach(element => {
        if (
          element.identifier === 'add_edit_account' ||
          element.identifier === 'add_edit_user' ||
          element.identifier === 'remove_account' ||
          element.identifier === 'remove_user' ||
          element.identifier === 'switch_user'
        ) {
          this.accountsTab = true;
        }
      });
    }
  }
  logout() {
    this.authService.logout('publisher').pipe().subscribe( data => {
      localStorage.removeItem('roleAndPermissions');
      localStorage.removeItem('userRole');
      console.log('Logout successfull : ' + data);
    },
    error => {
      console.log('Logout successfull : ' + error);
    });
  }
}
