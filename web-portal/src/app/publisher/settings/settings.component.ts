import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../_services/user.service';
import { first } from 'rxjs/operators';
import { ValidatePassword, MustMatch } from '../../common/validators/validator';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { MatSnackBar } from '@angular/material';
import { PublisherProfileService } from 'src/app/_services/publisher/publisher-profile.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';


export interface User {
  firstName: String;
  lastName: String;
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  @ViewChild('f') myNgForm;

  chngpwdForm: FormGroup;
  secQsForm: FormGroup;
  barLabel: String = 'Password Strength';
  firstName: String;
  lastName: String;
  user: User;
  submitted = false;
  message;
  status;
  messageQs;
  statusQs;
  checkQuestions;

  security_questions = [
    'What school did you attend for fifth grade?',
    'What was the name of your first pet?',
    'What was your dream job as a child?',
    'What is the name of your favorite childhood friend?',
    'What is your oldest cousin\'s first and last name?',
    'In what city did you meet your spouse/significant other?',
    'In what city was your first job?'
  ];

  public billingAlerts: boolean;
  public campaignMaintainance: boolean;
  public newsLetters: boolean;
  public customizedHelpPerformance: boolean;
  public disapprovedAdsPolicy: boolean;
  public reports: boolean;
  public specialOffers: boolean;
  public userEmail: string;
  public settingType = JSON.parse(localStorage.getItem('settingType'));
  hasSecurityQuestions;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private eventEmitterService: EventEmitterService,
    private snackBar: MatSnackBar,
    private publisherProfileService: PublisherProfileService,
    private publisherUserService: PublisherUserService
  ) {
    localStorage.setItem('activeIcon', JSON.stringify('profile'));
    this.eventEmitterService.onClickNavigation();
  }

  ngOnInit() {
    this.eventEmitterService.invokeAccountSetting.subscribe((name: string) => {
      this.settingType = JSON.parse(localStorage.getItem('settingType'));
    });
    this.userEmail = this.authenticationService.currentUserValue.email;
    this.getPublisherUser();
    this.checkQuestions = JSON.parse(localStorage.getItem('currentUser')).security;
    this.userService.getUserByEmail(this.userEmail).subscribe(
      res => {
        this.user = res;
        this.hasSecurityQuestions = res.hasSecurityQuestions ? res.hasSecurityQuestions : false;
        this.chngpwdForm.get('firstName').setValue(this.user.firstName);
        this.chngpwdForm.get('lastName').setValue(this.user.lastName);
      }
    );
    this.createForm();
    this.createSecQsForm();
  }

  get f() { return this.chngpwdForm.controls; }
  get secQsf() { return this.secQsForm.controls; }

  createSecQsForm() {
    this.secQsForm = this.formBuilder.group({
      security_question1: ['', !this.checkQuestions ? Validators.required : ''],
      security_answer1: ['', !this.checkQuestions ? Validators.required : ''],
      security_question2: ['', !this.checkQuestions ? Validators.required : ''],
      security_answer2: ['', !this.checkQuestions ? Validators.required : '']
    });
  }

  createForm() {
    this.chngpwdForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      curPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), ValidatePassword])],
      confirmPassword: ['', Validators.required]
    }, {validator: MustMatch('firstName', 'lastName', 'curPassword', 'newPassword', 'confirmPassword')});

  }

  reset() {
    this.myNgForm.resetForm();
  }

  onSubmit() {
    if (this.chngpwdForm.invalid) {
      return;
    }
    this.userService.changePwd({
      'currentPwd': this.chngpwdForm.value.curPassword,
      'newPassword': this.chngpwdForm.value.newPassword
    }).subscribe(res => {
      console.log('Password :' + res);
      this.message = res['message'];
      this.status = res['success'];
      this.reset();
    },
    err => {
      console.log('Password updated Error!');
      this.reset();
    });
  }

  onSaveSecQs() {

    if (this.secQsForm.invalid) {
      return;
    }

    this.userService.createQuestions({
      'security_question1': this.secQsForm.value.security_question1 ? this.secQsForm.value.security_question1 : '',
      'security_answer1': this.secQsForm.value.security_answer1 ? this.secQsForm.value.security_answer1 : '',
      'security_question2': this.secQsForm.value.security_question2 ? this.secQsForm.value.security_question2 : '',
      'security_answer2': this.secQsForm.value.security_answer2 ? this.secQsForm.value.security_answer2 : '',
    }).subscribe(res => {
      this.messageQs = res['message'];
      this.statusQs = res['success'];
      this.reset();
    },
    err => {
      console.log('Password updated Error!');
      this.reset();
    });
  }
  getPublisherUser() {
    this.publisherUserService.getPublisherDetails(this.userEmail).subscribe(
      publisher => {
        this.billingAlerts = publisher['notificationSettings'].billingAlerts;
        this.campaignMaintainance = publisher['notificationSettings'].campaignMaintainance;
        this.newsLetters = publisher['notificationSettings'].newsLetters;
        this.customizedHelpPerformance = publisher['notificationSettings'].customizedHelpPerformance;
        this.disapprovedAdsPolicy = publisher['notificationSettings'].disapprovedAdsPolicy;
        this.reports = publisher['notificationSettings'].reports;
        this.specialOffers = publisher['notificationSettings'].specialOffers;
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
    });
  }
  callOnNotificationChange() {
    const notification = {
      billingAlerts: this.billingAlerts,
      campaignMaintainance: this.campaignMaintainance,
      newsLetters: this.newsLetters,
      customizedHelpPerformance: this.customizedHelpPerformance,
      disapprovedAdsPolicy: this.disapprovedAdsPolicy,
      reports: this.reports,
      specialOffers: this.specialOffers,
    };
    this.userService.notificationSettings(notification).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
    });
  }

}
