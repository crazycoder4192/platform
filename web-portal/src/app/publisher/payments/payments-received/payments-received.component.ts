import { Component, OnInit } from '@angular/core';
import { BankdetailsService } from 'src/app/_services/publisher/bankdetails.service';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UtilService } from 'src/app/_services/utils/util.service';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-payments-received',
  templateUrl: './payments-received.component.html',
  styleUrls: ['./payments-received.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PaymentsReceivedComponent implements OnInit {
  private currencySymbol : string;
  constructor(private bankDetailsService: BankdetailsService, private snackBar: MatSnackBar,
    private dialog: MatDialog,private spinner: SpinnerService,
    private utilService: UtilService) { }
  invoiceList: any[] = [];
  filteredInvoiceList: any[] = [];
  totalPendingAmout;
  displayedColumns: string[] = ['date', 'receiptNumber',  'platform', 'accountNumber', 'earnings', 'status', 'actions'];
  expandedElement: null;
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  ngOnInit() {
    this.spinner.showSpinner.next(false);
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.bankDetailsService.getBills().subscribe(
      result => {
        this.invoiceList = <[]>result;
        this.filteredInvoiceList = this.invoiceList;
        this.totalPendingAmout = 0;
        this.invoiceList.forEach(item => {
          if (item.invoiceStatus === 'pending') {
            this.totalPendingAmout += +item.amount;
          }
        }
        );
      },
      err => {
        console.log('Error! getAllBills : ' + err);
      });
  }

  downloadInvoce(id) {
    this.bankDetailsService.downloadBill(id).subscribe(
      data => {
        const file = new Blob([data], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(file);
          return;
        } else {
          const timestamp = new Date().valueOf();
          const url = window.URL.createObjectURL(file);
          const link = window.document.createElement('a');
          link.href = url;
          link.download = 'Bill_' + timestamp;
          link.click();
          window.URL.revokeObjectURL(url);
          link.remove(); // remove the element
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  emailInvoice(id) {
    this.bankDetailsService.emailBill(id).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
      }
    );
  }

}
