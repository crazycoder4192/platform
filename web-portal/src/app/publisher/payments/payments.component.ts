import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BankdetailsService } from '../../_services/publisher/bankdetails.service';
import config from 'appconfig.json';
import { DashboardService } from 'src/app/_services/publisher/dashboard.service';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';
import { UtilService } from 'src/app/_services/utils/util.service';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { PaymentsReceivedComponent } from './payments-received/payments-received.component';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PaymentsComponent implements OnInit {
  @ViewChild('bankDetialsComponent') bankDetialsComponent: BankDetailsComponent;
  @ViewChild('paymentsComponent') paymentsComponent: PaymentsReceivedComponent;
  public dataSource: any = [];
  totalPendingAmout;
  invoiceList: any[] = [];
  filteredInvoiceList: any[] = [];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  displayedColumns: string[] = ['Date', 'Invoice number', 'Payment method', 'Total', 'Status'];
  expandedElement: null;
  public selectedTab = 0;
  public paymentCreditedOn = new Date();
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set([
    'publisher_payments_your_earnings'
  ]);
  private currencySymbol : string;
  public editMode = false;
  marketSelection = '1';
  marketSelectionDisplay = false;
  constructor(
    private bankDetailsService: BankdetailsService,
    private dashboardService: DashboardService,
    private toolTipService: ToolTipService,
    private utilService: UtilService,
    private publisherPlatformService: PublisherPlatformService,
    private spinner: SpinnerService
  ) {
    let yearOfBillGeneration = (config['BILL_GENERATION_DATE']['YEAR'] > 0 ?
                                  config['BILL_GENERATION_DATE']['YEAR'] : (new Date).getFullYear());
    let monthOfBillGeneration = (config['BILL_GENERATION_DATE']['MONTH'] > 0 ?
                                   config['BILL_GENERATION_DATE']['MONTH'] : ((new Date).getMonth()));
    // DAY of bill generation of UI config file should be match to rest config file.
    const dayOfBillGeneration = (config['BILL_GENERATION_DATE']['DAY'] > 0 ?
                                  config['BILL_GENERATION_DATE']['DAY'] : (new Date).getDate());
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
    if (dayOfBillGeneration < (new Date).getDate()) {
      monthOfBillGeneration = (monthOfBillGeneration + 1);
    }
    if (monthOfBillGeneration > 12) {
      monthOfBillGeneration = 1;
      yearOfBillGeneration = (yearOfBillGeneration + 1);
    }
    this.paymentCreditedOn = new Date(yearOfBillGeneration, monthOfBillGeneration, dayOfBillGeneration);
    this.publisherPlatformService.getAllZonesByPublisherId().subscribe(
      result => {
        if (result && result.length > 0) {
          // Display Market Selection dropdown if we have more than one zone
          this.marketSelectionDisplay = (([...new Set(result.map((x: any) => x.zone))].length) > 1);
        }
    });
    }

  ngOnInit() {
    this.spinner.showSpinner.next(true);
    this.currencySymbol = this.utilService.getCurrencySymbol();
    if (localStorage.getItem('zone') === '1' || localStorage.getItem('zone') === '2') {
      this.bankDetailsService.getBills().subscribe(
        result => {
          this.invoiceList = <[]>result;
          this.filteredInvoiceList = this.invoiceList;
          /*this.totalPendingAmout = 0;
          this.invoiceList.forEach(item => {
            if (item.invoiceStatus === 'pending') {
              this.totalPendingAmout += +item.amount;
            }
          }
          );*/
        },
        err => {
          this.spinner.showSpinner.next(false);
          console.log('Error! getAllInvoices : ' + err);
        });
    }
      this.dashboardService.getOutStandingAmount().subscribe(
        outStandingRes => {
          this.spinner.showSpinner.next(false);
          this.totalPendingAmout = 0;
          this.totalPendingAmout = outStandingRes['outstandingAmount'];
        },
        outStandingErr => {
          this.spinner.showSpinner.next(false);
          console.log('Error! getOutStandingAmount : ' + outStandingErr);
        },
      );
      this.bankDetailsService.getAccountDetails().subscribe(
      result => {
        this.dataSource = result;
        if (this.dataSource.length) {
          this.selectedTab = 1;
        }
      },
      err => {
          console.log('Error! getAllInvoices : ' + err);
      });
      this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }

  selectTab(event) {
    this.editMode = false;
    this.selectedTab = event;
  }

  setZone() {
    this.totalPendingAmout = -1;
    this.spinner.showSpinner.next(true);
    localStorage.setItem('zone', this.marketSelection);
    this.ngOnInit();
    this.currencySymbol = this.utilService.getCurrencySymbol();
    if (this.paymentsComponent) {
      this.spinner.showSpinner.next(true);
      this.paymentsComponent.ngOnInit();
    }
    if (this.bankDetialsComponent) {
      this.spinner.showSpinner.next(true);
      this.bankDetialsComponent.show = false;
      this.bankDetialsComponent.ngOnInit();
    }
  }
}
