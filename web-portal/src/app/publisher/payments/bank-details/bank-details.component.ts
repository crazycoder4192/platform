import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BankdetailsService } from 'src/app/_services/publisher/bankdetails.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PublisherPlatform } from 'src/app/_models/publisher_platform_model';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { startWith, map } from 'rxjs/operators';
import { ToolTipService } from '../../../_services/utils/tooltip.service';
import { Observable } from 'rxjs';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class BankDetailsComponent implements OnInit {
  @Output() editModeOn: EventEmitter<any> = new EventEmitter();
  @Output() editModeOff: EventEmitter<any> = new EventEmitter();
  public dataSource: any = [];
  public show = false;
  public submitted = false;
  public addAccountForm: FormGroup;
  public message;
  public status;
  public publisherDetails;
  public publisherDataObj;
  public pageView = 'pageView';
  public editId;
  public editData;
  unamePattern = '^[a-z0-9_-]{8,15}$';
  platForms: PublisherPlatform[];
  totalPendingAmout;
  invoiceList: any[] = [];
  filteredInvoiceList: any[] = [];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  displayedColumns: string[] = ['Date', 'Invoice number', 'Payment method', 'Total', 'Status'];
  expandedElement: null;
  uniqeBankCountries: string[];
  filteredBankCountries: Observable<any>;
  uniqeAccountTypes: string[];
  filteredAccountTypes: Observable<any>;
  toolTipKeyList = new Set([
    'publisher_payments_bank_details_entity_name'
  ]);
  private zone: string;
  constructor(
    private bankDetailsService: BankdetailsService,
    private formBuilder: FormBuilder,
    private assetHTTPService: AssetService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private spinner: SpinnerService,
    private toolTipService: ToolTipService,
    private amplitudeService: AmplitudeService
  ) { }

  ngOnInit() {
    this.zone = localStorage.getItem('zone');
    this.assetHTTPService.getPlatforms().subscribe(
      res => {
        this.platForms = [];
        this.platForms = <PublisherPlatform[]>res;
      },
      err => {
        console.log('Error! : ' + err);
      }
    );
    this.bankDetailsService.getBills().subscribe(
      result => {
        this.invoiceList = <[]>result;
        this.filteredInvoiceList = this.invoiceList;
        this.totalPendingAmout = 0;
        this.invoiceList.forEach(item => {
          if (item.invoiceStatus === 'pending') {
            this.totalPendingAmout += +item.amount;
          }
        }
        );
      },
      err => {
        console.log('Error! getAllInvoices : ' + err);
      });
    this.renderListView();
    this.createFormGroup();
    this.getCountryList();
    this.getAccountTypeList();
    this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }

  getCountryList() {
    this.uniqeBankCountries = ['US', 'India'];
    this.filteredBankCountries = this.addAccountForm.controls['bankCountry'].valueChanges
      .pipe(
        startWith<string>(''),
        map(value => typeof value === 'string' ? value : value),
        map(name => this._filterBankCountry(name))
      );
  }
  getAccountTypeList() {
    this.uniqeAccountTypes = (this.zone === "1") ? ['Business', 'Checking'] : ['Savings', 'Current'];
    this.filteredAccountTypes = this.addAccountForm.controls['accountType'].valueChanges
      .pipe(
        startWith<string>(''),
        map(value => typeof value === 'string' ? value : value),
        map(name => this._filterAccountType(name))
      );
  }
  private _filterBankCountry(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.uniqeBankCountries.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterAccountType(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.uniqeAccountTypes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  renderListView() {
    this.bankDetailsService.getAccountDetails().subscribe(
      result => {
        this.spinner.showSpinner.next(false);
        this.dataSource = result;
        this.toggleAddAccount('listView');
      },
      err => {
        this.spinner.showSpinner.next(false);
          console.log('Error! getAllInvoices : ' + err);
      });
  }
  public noWhitespaceValidator(control: FormControl) {
    let isWhitespace = false;
    if (control.value && control.value.trim(' ').length < 3 ) {
      isWhitespace = true;
    }
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  createFormGroup() {
    if(this.zone === "1"){
      this.addAccountForm = this.formBuilder.group({
        // tslint:disable-next-line: max-line-length
        accountHolderName: ['', Validators.compose([Validators.required])],
        bankName: ['', Validators.compose([Validators.required])],
        bankCountry: ['United States', Validators.compose([Validators.required])],
        accountNumber: ['', Validators.compose([Validators.required])],
        accountType: ['', Validators.compose([Validators.required])],
        swiftCode: ['', Validators.compose([Validators.required])],
        taxNumber: ['', Validators.compose([Validators.required])],
        platforms: ['', Validators.compose([Validators.required])]
      });
    }
    
    if(this.zone === "2"){
      this.addAccountForm = this.formBuilder.group({
        accountHolderName: ['', Validators.compose([Validators.required])],
        bankName: ['', Validators.compose([Validators.required])],
        bankCountry: ['India', Validators.compose([Validators.required])],
        accountNumber: ['', Validators.compose([Validators.required])],
        accountType: ['', Validators.compose([Validators.required])],
        swiftCode: ['', Validators.compose([Validators.required])],
        taxNumber: ['', Validators.compose([Validators.required])],
        platforms: ['', Validators.compose([Validators.required])],
        accountHolderAddress: ['', Validators.compose([Validators.required])],
        accountHolderCIN : ['', Validators.compose([Validators.required])],
        accountHolderGSTIN: ['', Validators.compose([Validators.required])],
        bankAddress: ['', Validators.compose([Validators.required])],
        ifsc: ['', Validators.compose([Validators.required])],
      });
    }
    this.assetHTTPService.getPlatforms().subscribe(
      res => {
        this.platForms = [];
        this.platForms = <PublisherPlatform[]>res;
      },
      err => {
        console.log('Error! : ' + err);
      }
    );
    this.editData = this.addAccountForm;
  }
  toggleAddAccount(pageType) {
    this.pageView = pageType;
    this.submitted = false;
    if (pageType === 'addView') {
      this.editModeOn.emit(true);
      this.addAccountForm.reset();
      if(this.zone === "1"){
        this.addAccountForm = this.formBuilder.group({
          // tslint:disable-next-line: max-line-length
          accountHolderName: ['', Validators.compose([Validators.required])],
          bankName: ['', Validators.compose([Validators.required])],
          bankCountry: ['United States', Validators.compose([Validators.required])],
          accountNumber: ['', Validators.compose([Validators.required])],
          accountType: ['', Validators.compose([Validators.required])],
          swiftCode: ['', Validators.compose([Validators.required])],
          taxNumber: ['', Validators.compose([Validators.required])],
          platforms: ['', Validators.compose([Validators.required])]
        });
      }
      if(this.zone === "2"){
        this.addAccountForm = this.formBuilder.group({
          accountHolderName: ['', Validators.compose([Validators.required])],
          bankName: ['', Validators.compose([Validators.required])],
          bankCountry: ['India', Validators.compose([Validators.required])],
          accountNumber: ['', Validators.compose([Validators.required])],
          accountType: ['', Validators.compose([Validators.required])],
          swiftCode: ['', Validators.compose([Validators.required])],
          taxNumber: ['', Validators.compose([Validators.required])],
          platforms: ['', Validators.compose([Validators.required])],
          accountHolderAddress: ['', Validators.compose([Validators.required])],
          accountHolderCIN : ['', Validators.compose([Validators.required])],
          accountHolderGSTIN: ['', Validators.compose([Validators.required])],
          bankAddress: ['', Validators.compose([Validators.required])],
          ifsc: ['', Validators.compose([Validators.required])],
        });
      }
      this.editData = this.addAccountForm;
    }else{
      this.editModeOff.emit(true);
    }
    this.message = '';
    this.status = '';
    this.show = !this.show;
    this.amplitudeService.logEvent('payment_setup - click add account', null);
  }

  onSubmit() {
    this.submitted = true;
    if (this.addAccountForm.invalid) {
      return;
    }
    this.spinner.showSpinner.next(true);
    this.bankDetailsService.addAccount(this.addAccountForm.value).subscribe(
      data => {
        this.spinner.showSpinner.next(false);
        this.message = data['message'];
        this.status = data['success'];
        if (this.status) {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'success',
              message: data['message']
            }
          });
          this.renderListView();
        } else {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'success',
              message: data['message']
            }
          });
        }
        this.amplitudeService.logEvent('payment_setup - save details', null);
      },
      error => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
      }
    );
  }

  editAccountView(id) {
    this.toggleAddAccount('editView');
    this.editModeOn.emit(null);
    this.submitted = false;
    this.bankDetailsService.getAccountById(id).subscribe(account => {
      this.editData = account;
      this.addAccountForm.get('bankCountry').setValue(this.editData.bankCountry);
      this.addAccountForm.get('accountType').setValue(this.editData.accountType);
      this.addAccountForm.get('platforms').setValue(this.editData.platforms);
      this.editId = account['_id'];
    });
  }

  updateAccount(id) {
    this.submitted = true;
    let valid = false;
    if ((this.zone === "1" && this.addAccountForm.controls['accountHolderName'].valid && this.addAccountForm.controls['bankName'].valid &&
        this.addAccountForm.controls['swiftCode'].valid && this.addAccountForm.controls['taxNumber'].valid &&
        this.addAccountForm.controls['platforms'].valid || (this.zone === "2" && this.addAccountForm.controls['accountHolderName'].valid && this.addAccountForm.controls['bankName'].valid &&
        this.addAccountForm.controls['swiftCode'].valid && this.addAccountForm.controls['taxNumber'].valid && this.addAccountForm.controls['platforms'].valid && 
        this.addAccountForm.controls['accountHolderAddress'].valid && this.addAccountForm.controls['accountHolderCIN'].valid && this.addAccountForm.controls['accountHolderGSTIN'].valid && 
        this.addAccountForm.controls['bankAddress'].valid && this.addAccountForm.controls['ifsc'].valid))) {
      valid = true;
    }
    if (!valid) {
      return;
    }
    if (valid) {
      this.spinner.showSpinner.next(true);
      this.bankDetailsService
        .updateAccountById(id, this.addAccountForm.value)
        .subscribe(
          data => {
            this.spinner.showSpinner.next(false);
            this.message = data['message'];
            this.status = data['success'];
            this.editModeOff.emit(null);
            if (this.status) {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
              this.renderListView();
            } else {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: data['message']
                }
              });
            }
          },
          error => {
            this.spinner.showSpinner.next(false);
            this.message = error['message'];
            this.status = error['success'];
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: error.error['message']
              }
            });
          }
        );
    }
  }

  deleteAccount(id) {
    this.toggleAddAccount('listView');
    const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title': 'Are you sure you want to delete your bank account?'
      }
    });
    confirmDialogRef.afterClosed().subscribe(confirmResult => {
      if (confirmResult) {
        this.bankDetailsService.deleteAccount(id).subscribe(
          data => {
            this.message = data['message'];
            this.status = data['success'];
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.renderListView();
          },
          error => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: error.error['message']
              }
            });
          }
        );
      }
    });

  }

  downloadInvoce(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are you sure you want to download pdf',
      message: '',
      confirm: 'Yes',
      cancel: 'No'
    };
    dialogConfig.minWidth = 400;
    const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.bankDetailsService.downloadBill(id).subscribe(
          data => {
            const file = new Blob([data], { type: 'application/pdf' });
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(file);
              return;
            } else {
              const timestamp = new Date().valueOf();
              const url = window.URL.createObjectURL(file);
              const link = window.document.createElement('a');
              link.href = url;
              link.download = 'Bill_' + timestamp;
              link.click();
              window.URL.revokeObjectURL(url);
              link.remove(); // remove the element
            }
          },
          error => {
            console.log(error);
          }
        );
        dialogRef.close();
      } else {
        dialogRef.close();
      }
    });
  }
  emailInvoice(id) {
    this.bankDetailsService.emailBill(id).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
      }
    );
  }

  redirectSecure() {

  }
  removePlatform(selectedPlatform: any) {
    const temp = this.editData.platforms;
    this.editData.platforms = [];
    for (const platForm of temp) {
      if (platForm !== selectedPlatform) {
        this.editData.platforms.push(platForm);
      }
    }
    this.addAccountForm.get('platforms').setValue(this.editData.platforms);
  }
  getPlatformName(selectedPlatform: any) {
    for (const platForm of this.platForms) {
      if (platForm._id === selectedPlatform) {
        return platForm.name;
      }
    }
  }
  callOnPlatformChange(selectedPlatforms) {
    if (!this.editData) {
      this.editData = {
        platforms: []
      };
    }
    this.editData.platforms = selectedPlatforms;
  }
}
