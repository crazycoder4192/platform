import { Component, OnInit, HostListener, ElementRef, Input, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { PublisherProfileService } from 'src/app/_services/publisher/publisher-profile.service';
import { EventEmitterService } from '../_services/event-emitter.service';
import { NotificationsService } from '../_services/notifications.service';
import { BehaviorSubject } from 'rxjs';
import { SpinnerService } from '../_services/spinner.service';
import { Location } from '@angular/common';
import { PublisherUserService } from '../_services/publisher/publisher-user.service';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../common/format-datepicker/format-datepicker';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter } , 
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class PublisherComponent implements OnInit {
  show = false;
  showquestion = false;
  platformResults: any[];
  websites: any[];
  apps: any[];
  id;
  private useremail: string = this.authenticationService.currentUserValue.email;
  activeIcon: string;
  showPaymentInfo: boolean;
  showPlatformInfo: boolean;
  showProfileInfo: boolean;
  showQuestionInfo: boolean;
  showProfile = false;
  hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
  showNotificationInfo: boolean;
  unreadNotificationCount: Object;
  showNotification: boolean;
  immediateChildRoute: string;  
  public enableAddAsset: BehaviorSubject<boolean>;
  userPermissions: any;
  userRole: any;
  rolePermissions: any;
  marketSelection = '1';
  constructor(private authService: AuthenticationService,
    private router: Router,
    private publisherPlatformService: PublisherPlatformService,
    private publisherProfileService: PublisherProfileService,
    private authenticationService: AuthenticationService,
    private eventEmitterService: EventEmitterService,
    private notificationService: NotificationsService,
    private spinner: SpinnerService,
    private location: Location,
    private publisherUserService: PublisherUserService
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
    this.spinner.showSpinner.next(false);
    this.setPermissions();
    this.enableAddAsset = new BehaviorSubject<boolean>(false); 
    this.router.events.subscribe(event => {
      if (event.constructor.name === 'NavigationStart') {
        const routerURL: string = event['url'];
        this.immediateChildRoute = routerURL.split('/')[2];
      }
    });
  }
  
  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const visitedRoute = JSON.parse(localStorage.getItem('visitRoute'));
    if (this.hasPersonalProfile) {
      this.getUnreadNotificationCount();
    }
    this.eventEmitterService.invokeSidebarComponentFunction.subscribe((name: string) => {
      this.hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
        this.setPermissions();
    });
    this.eventEmitterService.invokeTopBarIconFunction.subscribe((name: string) => {
      this.hasPersonalProfile = this.authenticationService.currentUserValue.hasPersonalProfile;
      this.setIconActivation();
    });
    if (localStorage.getItem('currentRoute') && !visitedRoute.publisher) {
      this.router.navigate([localStorage.getItem('currentRoute')]);
      visitedRoute.publisher = true;
      localStorage.setItem('visitRoute', JSON.stringify(visitedRoute));
    } else if (this.hasPersonalProfile && !currentUser.visitedDashboard && !localStorage.getItem('currentRoute')) {
      this.router.navigate(['/publisher/dashboard']);
      currentUser['visitedDashboard'] = true;
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
    } else if (this.hasPersonalProfile && (currentUser.visitedDashboard || (visitedRoute && visitedRoute.publisher))) {
      this.router.navigate([this.location.path()]);
    } else {
      this.router.navigate(['/publisher/publisherProfile']);
    }
    this.publisherProfileService.getProfileByEmail(this.useremail).subscribe(
      res => {
        if (res) {
          this.id = res._id;
          this.publisherPlatformService.getPlatformsByPublisherId(this.id).subscribe(
            result => {
              this.platformResults = <[]>result;
              if (this.platformResults && this.platformResults.length > 0) {
                this.enableAddAsset.next(true);
              }
              this.websites = this.platformResults.filter(platform => platform.platformType === 'Website');
              this.apps = this.platformResults.filter(platform => platform.platformType === 'MobileApp');
            }
          );
        }
      });
      
  }

  addPlatform(platformType) {
    this.router.navigate(['/publisher/platformCreation'], { state: { platformType: JSON.stringify(platformType) } });
  }  
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }
  togglePlatform() {
    this.showPlatformInfo = true;
    this.show = !this.show;
    this.showPaymentInfo = false;
    this.showProfileInfo = false;
    this.showQuestionInfo = false;
    this.showNotificationInfo = false;
    this.showNotification = false;
  }
  toggleAskAQuestion() {
    this.showquestion = !this.showquestion;
    this.showPaymentInfo = false;
    this.showPlatformInfo = false;
    this.showProfileInfo = false;
    this.showQuestionInfo = true;
    this.showNotificationInfo = false;
    this.showNotification = false;
  }
  toggleProfilePanel() {
    this.showProfile = !this.showProfile;
    this.showPaymentInfo = false;
    this.showPlatformInfo = false;
    this.showProfileInfo = true;
    this.showQuestionInfo = false;
    this.showNotificationInfo = false;
    this.showNotification = false;
  }
  toggleNotification() {
    this.showNotificationInfo = true;
    this.showNotification = true;
    this.showPaymentInfo = false;
    this.showPlatformInfo = false;
    this.showProfileInfo = false;
    this.showQuestionInfo = false;
    this.showProfileInfo = false;
  }
  gotoProfile() {
    this.showProfileInfo = true;
    this.showPaymentInfo = false;
    this.showPlatformInfo = false;
    this.showQuestionInfo = false;
    this.showNotificationInfo = false;
    this.showNotification = false;
  }
  gotoPayments() {
    this.showPaymentInfo = true;
    this.showPlatformInfo = false;
    this.showProfileInfo = false;
    this.showQuestionInfo = false;
    this.showNotificationInfo = false;
    this.showNotification = false;
    this.router.navigate(['/publisher/payments']);
  }
  setIconActivation() {
    if (JSON.parse(localStorage.getItem('activeIcon')) === 'platform') {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showQuestionInfo = false;
      this.showPlatformInfo = true;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'payment') {
      this.showPaymentInfo = true;
      this.showProfileInfo = false;
      this.showQuestionInfo = false;
      this.showPlatformInfo = false;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'question') {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showPlatformInfo = false;
      this.showQuestionInfo = true;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'profile') {
      this.showPaymentInfo = false;
      this.showProfileInfo = true;
      this.showPlatformInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = false;
    } else if (JSON.parse(localStorage.getItem('activeIcon')) === 'notification') {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showPlatformInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = true;
    } else {
      this.showPaymentInfo = false;
      this.showProfileInfo = false;
      this.showPlatformInfo = false;
      this.showQuestionInfo = false;
      this.showNotificationInfo = false;
    }
  }
  getUnreadNotificationCount() {
    this.notificationService.getUnreadNotificationCount().subscribe(
      res => {
        this.unreadNotificationCount = res;
      },
      err => {
      console.log('Error! getNotifications : ' + err);
    });
  }
  setPermissions() {
    this.publisherUserService.getPublisherUserPermissions(this.useremail).subscribe(
      permissions => {
        this.userPermissions = permissions['userPermissions'];
        this.userRole = permissions['userRole'];
        if (this.hasPersonalProfile && permissions['zone'] &&
        (localStorage.getItem('zone') === 'null' || localStorage.getItem('zone') === 'undefined'
        || permissions['zone'] !== localStorage.getItem('zone'))) {
          this.router.navigate(['/publisher/publisherProfile']);
          setTimeout(() => {
            localStorage.setItem('zone', permissions['zone']);
            this.router.navigate(['/publisher/dashboard']);
          }, 10);
        }
        this.marketSelection = localStorage.getItem('zone');
        if (this.userRole !== '') {
          localStorage.setItem('userRole', JSON.stringify(this.userRole));
          localStorage.setItem('roleAndPermissions', JSON.stringify(this.userPermissions));
          this.initRolesAndPermissions();
        }
    });
  }
  initRolesAndPermissions() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
  }
}
