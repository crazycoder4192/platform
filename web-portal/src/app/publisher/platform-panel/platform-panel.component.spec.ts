import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformPanelComponent } from './platform-panel.component';

describe('PlatformPanelComponent', () => {
  let component: PlatformPanelComponent;
  let fixture: ComponentFixture<PlatformPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
