import { Component, OnInit, HostListener, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { PublisherProfileService } from 'src/app/_services/publisher/publisher-profile.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { PublisherComponent } from '../publisher.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';
import {FlexLayoutModule, BREAKPOINT} from '@angular/flex-layout';

@Component({
  selector: 'app-platform-panel',
  templateUrl: './platform-panel.component.html',
  styleUrls: ['./platform-panel.component.scss']
})
export class PlatformPanelComponent implements OnInit {
  clickState = false;
  addAssetState = false;
  platformResults: any[] = [];
  websitesUS: any[] = [];
  websitesInd: any[] = [];
  apps: any[] = [];
  id;

  @Output() private togglePlatform = new EventEmitter<boolean>();
  @Input() useremail: string;
  selectedPlatformId: any;
  watchingPlatform: any;
  selectedMarketPlace = '1';
  totalZones = 1;
  maxShowingPlatforms: number = 13; // in combinations of (10 + 1) or (1 + 10)
  singlePlatformHeight = 34; // in pixels
  indiaFlexPart: string;
  usFlexPart: string;
  indiaOverlayHt: number;
  usOverlayHt: number;
  scrWidth: number;

  constructor(private eRef: ElementRef,
    private authenticationService: AuthenticationService,
    private publisherProfileService: PublisherProfileService,
    private publisherPlatformService: PublisherPlatformService,
    private publisherCompontent: PublisherComponent,
    private router: Router,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private eventEmitterService: EventEmitterService,
    private publisherUserService: PublisherUserService
  ) { 
    this.selectedMarketPlace = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  }

  ngOnInit() {
    this.getScreenSize();
    this.loadPlatformsByPublisherId();
  }
  loadPlatformsByPublisherId () {
    this.publisherUserService.getPublisherDetails(this.useremail).subscribe(
      res => {
        this.id = res._id;
        this.publisherPlatformService.getAllZonesByPublisherId().subscribe(
          result => {
            this.platformResults = <[]>result;
            if (result && result.length > 0) {
              // Get total no of zones
              this.totalZones = ([...new Set(result.map((x: any) => x.zone))].length);
            }
            this.watchingPlatform = this.platformResults.find( (thisPlatform) =>
            (thisPlatform.isWatching === true && this.selectedMarketPlace === thisPlatform.zone));
            if (this.watchingPlatform) {
              this.selectedPlatformId = this.watchingPlatform._id;
            }
            this.websitesUS = this.platformResults.filter(platform => (platform.zone === '1'));
            this.websitesInd = this.platformResults.filter(platform => (platform.zone === '2'));
            this.apps = this.platformResults.filter(platform => platform.platformType === 'MobileApp');
            
            let indiaSitesLength = this.websitesInd.length;
            let usSitesLength = this.websitesUS.length;
            let usFlex: number;
            let indiaFlex: number;
            usFlex = usSitesLength / (usSitesLength + indiaSitesLength);
            indiaFlex = indiaSitesLength / (usSitesLength + indiaSitesLength);
            if( indiaSitesLength + usSitesLength > this.maxShowingPlatforms) {
              if( indiaSitesLength > 0 && usSitesLength > 0) {
                    if(usFlex > indiaFlex) {
                      if (indiaSitesLength > 6) {
                        indiaFlex = (6 / this.maxShowingPlatforms) * 100;
                        usFlex = 100 - indiaFlex;
                        this.indiaFlexPart = indiaFlex + '%';
                        this.usFlexPart = usFlex + '%';
                      } else {
                        indiaFlex = (indiaSitesLength / this.maxShowingPlatforms) * 100;
                        usFlex = 100 - indiaFlex;
                        this.indiaFlexPart = indiaFlex + '%';
                        this.usFlexPart = usFlex + '%';
                      }

                    } else if (indiaFlex >= usFlex) {
                      if (usSitesLength > 6) {
                        usFlex = (6 / this.maxShowingPlatforms) * 100;
                        indiaFlex = 100 - usFlex;
                        this.indiaFlexPart = indiaFlex + '%';
                        this.usFlexPart = usFlex + '%';
                      } else {
                        usFlex = (usSitesLength / this.maxShowingPlatforms) * 100;
                        indiaFlex = 100 - usFlex;
                        this.indiaFlexPart = indiaFlex + '%';
                        this.usFlexPart = usFlex + '%';
                      }
                    }
              } else if(indiaSitesLength === 0) {
                indiaFlex = 0;
                usFlex = 100;
                this.indiaFlexPart = 0 + '%';
                this.usFlexPart = 100 + '%';
              } else if(usSitesLength === 0) {
                indiaFlex = 100;
                usFlex = 0;
                this.indiaFlexPart = 100 + '%';
                this.usFlexPart = 0 + '%';
              }

              this.indiaOverlayHt = Math.floor((indiaFlex * this.maxShowingPlatforms) / 100) * this.singlePlatformHeight;
              this.usOverlayHt = Math.floor((usFlex * this.maxShowingPlatforms) / 100) * this.singlePlatformHeight;
            } else {
              usFlex = usSitesLength / (usSitesLength + indiaSitesLength);
              indiaFlex = indiaSitesLength / (usSitesLength + indiaSitesLength);
              this.indiaFlexPart = indiaFlex + '%';
              this.usFlexPart = usFlex + '%';
              this.indiaOverlayHt = indiaSitesLength * this.singlePlatformHeight;
              this.usOverlayHt = usSitesLength * this.singlePlatformHeight;
            }

          }
        );
      }
    );
  }

  setSinglePlatformHeight() {
    if(this.scrWidth < 1167) {
      this.singlePlatformHeight = 16;
    } else if (this.checkBetweenWidth(1167,1250,this.scrWidth)) {
      this.singlePlatformHeight = 17;
    } else if (this.checkBetweenWidth(1250,1377,this.scrWidth)) {
      this.singlePlatformHeight = 18;
    } else if (this.checkBetweenWidth(1377,1426,this.scrWidth)) {
      this.singlePlatformHeight = 19;
    } else if (this.checkBetweenWidth(1426,1477,this.scrWidth)) {
      this.singlePlatformHeight = 20;
    } else if (this.checkBetweenWidth(1477,1544,this.scrWidth)) {
      this.singlePlatformHeight = 21;
    } else if (this.checkBetweenWidth(1544,1576,this.scrWidth)) {
      this.singlePlatformHeight = 22;
    } else if (this.checkBetweenWidth(1576,1626,this.scrWidth)) {
      this.singlePlatformHeight = 23;
    } else if (this.checkBetweenWidth(1626,1675,this.scrWidth)) {
      this.singlePlatformHeight = 24;
    } else if (this.checkBetweenWidth(1675,1725,this.scrWidth)) {
      this.singlePlatformHeight = 25;
    } else if (this.checkBetweenWidth(1725,1788,this.scrWidth)) {
      this.singlePlatformHeight = 26;
    } else if (this.checkBetweenWidth(1788,1827,this.scrWidth)) {
      this.singlePlatformHeight = 27;
    } else if (this.checkBetweenWidth(1827,1875,this.scrWidth)) {
      this.singlePlatformHeight = 29;
    } else if (this.checkBetweenWidth(1875,1925,this.scrWidth)) {
      this.singlePlatformHeight = 30;
    } else if (this.checkBetweenWidth(1925,1975,this.scrWidth)) {
      this.singlePlatformHeight = 31;
    } else if (this.checkBetweenWidth(1975,2025,this.scrWidth)) {
      this.singlePlatformHeight = 32;
    } else {
      this.singlePlatformHeight = 32;
    }
    this.singlePlatformHeight = this.singlePlatformHeight + 16;

    this.maxShowingPlatforms = 13;
  }

  checkBetweenWidth(lowerW: number, higherW: number, currentW: number) {
    if (lowerW <= currentW && currentW <= higherW) {
      return true;
    } else {
      return false;
    }
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.scrWidth = window.innerWidth;
        this.setSinglePlatformHeight();
  }

  @HostListener('click')
  clickInside() {
    this.clickState = false;
  }

  @HostListener('document:click')
  clickout() {
    if (this.clickState) {
      this.togglePlatform.emit(false);
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();
    }
    this.clickState = true;
  }
  gotoManagePlatform() {
    this.togglePlatform.emit(false);
    this.router.navigate(['/publisher/platformManage']);
  }
  switchPlatform(platform, isWatching) {
    const platformId = platform._id;
    this.addAssetState = ((platform.hcpScript.length > 1) ? true : false);
    if (isWatching) {
      this.router.navigate(['/publisher/platformDashboard'],
      { state: { 'platformId': JSON.stringify(platformId) } });
    } else if (this.selectedPlatformId !== platformId) {
      const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
        width: '50%',
        data: {
          'title' : 'Are you sure you want to switch to "' + platform.name + '" ?'
        }
      });
      confirmDialogRef.afterClosed().subscribe(confirmResult => {
        if (confirmResult) {
          this.router.navigate(['/publisher/dashboard']);
          this.selectedPlatformId = platformId;
          localStorage.setItem('zone', platform.zone);
          this.publisherPlatformService.switchToPlatform(platformId).subscribe(
            res => {
              this.publisherCompontent.enableAddAsset.next(this.addAssetState);
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: res.message
                }
              });
            // this.loadPlatformsByPublisherId();
            this.router.navigate(['/publisher/platformDashboard'],
                              { state: { 'platformId': JSON.stringify(platformId) } });
            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
            });
        }
      });
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'success',
          message: 'Platform already watching'
        }
      });
    }
  }
}
