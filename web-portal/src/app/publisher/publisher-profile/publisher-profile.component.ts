import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PublisherProfileService } from 'src/app/_services/publisher/publisher-profile.service';
import { ZipcodesService } from 'src/app/_services/utils/zipcodes.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { UserService } from 'src/app/_services/user.service';
import { User, LoginUserDetails } from 'src/app/_models/users';
import { Publisher } from 'src/app/_models/publisher_model';
import { PublisherPlatform } from 'src/app/_models/publisher_platform_model';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { SMSService } from 'src/app/_services/utils/sms.service';
import { log } from 'util';
import { UtilService } from 'src/app/_services/utils/util.service';
import { CountryCurrenciesModel } from 'src/app/_models/CountryCurrenciesModel';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { TermsAndConditionsComponent } from 'src/app/common/terms-and-conditions/terms-and-conditions.component';
import { OptValidationComponent } from 'src/app/common/opt-validation/opt-validation.component';
import { ValidateDomain } from 'src/app/common/validators/validator';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { OtpValidationDirective } from '../../common/directives/otp.directive';
import { WalkthroughComponent } from 'src/app/common/walkthrough/walkthrough.component';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
import { USEROLESElement } from 'src/app/admin/configuration/dataModel/userRolesPermissionsModel';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';

@Component({
  selector: 'app-publisher-profile',
  templateUrl: './publisher-profile.component.html',
  styleUrls: ['./publisher-profile.component.scss']
})
export class PublisherProfileComponent implements OnInit {

  public filteredStates: Observable<string[]>;
  public filteredCities: Observable<string[]>;
  public filteredCountries: any;
  public currentTab = 1;
  public publisherProfileForm: FormGroup;
  public publisherApp: FormGroup;
  public publisherSite: FormGroup;
  public cityControl = new FormControl();
  public stateControl = new FormControl();
  public countryControl = new FormControl();
  private sec1 = new FormControl();
  private sec2 = new FormControl();
  private usr: User;
  private useremail: string = this.authenticationService.currentUserValue.email;
  private publisherProfile: Publisher;
  public id: string;
  private uniqueCities: string[];
  private uniqueStates: string[];
  private uniqueSities: string[];
  private uniqueApps: string[];
  public filteredSities: Observable<string[]>;
  public filteredApps: Observable<string[]>;
  public appCategoryControl = new FormControl();
  public appTypeControl = new FormControl();
  private publisherPlatform: PublisherPlatform;
  public typeOfAccount = 'Website';
  public platformStatus = 'add';
  public cityStateDetails: any;
  public isValidCity: boolean;
  countriesCurrencies: any[];
  public editPersonalInfo =  false;
  public editBusinessAddress = false;
  uniqueZipCodes: any;
  filteredZipCodes: Observable<any>;
  isValidZipCode: any;
  userDetails: LoginUserDetails;
  countrySelected: string;
  hasPersonalProfile: boolean;
  public showTagLine;
  public isPhoneNumberVerified;
  private verifiedPhNumber;
  public defaultCountryCode = '+1';
  public countryCodes = ['+1', '+91'];
  listOfExistedUserRolesPermissions: any[];
  isNotRegiteredUser: boolean;
  publisherDetails: any;
  marketSelection: string;
  selectedMarketPlace: string;
  countryISO2Code: any;
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private publisherProfileService: PublisherProfileService,
    private zipcodesService: ZipcodesService,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private utilService: UtilService,
    private configurationService: ConfigurationService,
    private publisherPlatformService: PublisherPlatformService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private spinner: SpinnerService,
    private smsService: SMSService,
    private eventEmitterService: EventEmitterService,
    private amplitudeService: AmplitudeService,
    private publisherUserService: PublisherUserService
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.countrySelected = 'null';
    this.spinner.showSpinner.next(false);
    // this.initUniqueCities();
    this.setZone();
    this.configurationService.getAllCountriesCourrencies().subscribe(
      res => {
        this.countriesCurrencies = [];
        this.countriesCurrencies = <CountryCurrenciesModel[]>res;
        this.filteredCountries = this.countryControl.valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterCountry(name))
          );
      }
    );
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }
  onMarketChange(){
    this.marketSelection = this.selectedMarketPlace ;
    this.setZone();
  }
  initUniqueStates() {
    this.zipcodesService.getUniqueStates(this.countrySelected).subscribe(
      res => {
        this.uniqueStates = res;
        this.filteredStates = this.stateControl.valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => this._filterState(name))
          );
      }
    );
  }
  initUniqueCities() {
    this.zipcodesService.getUniqueCities(this.countrySelected).subscribe(
      res => {
        this.uniqueCities = res;
        this.uniqueCities = this.uniqueCities.filter((el, i, a) => i === a.indexOf(el));
        this.filteredCities = this.cityControl.valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
          );
      },
      err => {
        console.log('Error!');
      });

  }

  get f() { return this.publisherProfileForm.controls; }


  nextView(n: number) {
    if (n === 1) {
      if (this.validatePersonalInformation()) {
        this.publisherProfileForm.get('acceptTerms').disable();
        this.currentTab = this.currentTab + n;
        this.amplitudeService.logEvent('profile_creation - user enters basic information', null);
      }
    } else if (n === 2) {
      if (this.validateBusinessAddress()) {
        this.currentTab = this.currentTab + 1;
        this.amplitudeService.logEvent('profile_creation - user enters business address', null);
      }
    }
  }

  prevView(n: number) {
    this.currentTab = this.currentTab + n;
    if (this.currentTab === 1) {
      this.publisherProfileForm.get('acceptTerms').enable();
    }
  }

  ngOnInit() {
    this.publisherProfileForm = this.formBuilder.group({
      id: [this.id],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      acceptTerms: [false, Validators.required],
      phoneNumber: ['', Validators.required],
      countryCodePhNumber: ['+1', Validators.required],
      isPhNumberVerified: [false, Validators.required],
      email: this.authenticationService.currentUserValue.email,
      organization: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      zipCode: ['', Validators.required]
    });

    this.publisherApp = this.formBuilder.group({
      typeOfAccount: [this.typeOfAccount, Validators.required],
      appName: ['', Validators.required],
      appLink: ['', Validators.required],
      appType: ['', Validators.required],
      marketSel: ['', Validators.required],
      appCategory: ['', Validators.required]
    },
    {
      validator: [ValidateDomain('appLink')]
    });

    this.publisherSite = this.formBuilder.group({
      typeOfAccount: [this.typeOfAccount, Validators.required],
      siteName: ['', Validators.required],
      siteLink: ['', Validators.required],
      marketSel: ['', Validators.required],
      siteType: ['', Validators.required]
    },
    {
      validator: [ValidateDomain('siteLink')]
    });
    this.userService.getUserByEmail(this.useremail).subscribe(
      res => {
        this.usr = res;
        this.publisherProfileForm.get('firstName').setValue(this.usr.firstName);
        this.publisherProfileForm.get('lastName').setValue(this.usr.lastName);
      }
    );
    this.publisherProfileService.getProfileByEmail(this.useremail).subscribe(
      res => {
        this.publisherProfile = res;
        if (!this.publisherProfile) {
          if (!this.userDetails.hasPersonalProfile) {
            this.showWalkthrough();
          }
          this.editPersonalInfo = true;
          this.editBusinessAddress = true;
           this.publisherUserService.getPublisherDetails(this.useremail).subscribe(
             publisherDetails => {
               this.publisherDetails = publisherDetails;
               this.showTagLine = true;
              if (this.publisherDetails) {
                this.isNotRegiteredUser = true;
                this.editBusinessAddress = false;
                this.platformStatus = 'added';
                 this.currentTab = 0;
                 this.id = this.publisherDetails._id;
                 this.publisherProfileForm.get('phoneNumber').setValue(this.publisherDetails.phoneNumber);
                 this.publisherProfileForm.get('organization').setValue(this.publisherDetails.organizationName);
                 this.publisherProfileForm.get('acceptTerms').setValue(this.publisherDetails.acceptTerms);
                 this.publisherProfileForm.get('addressLine1').setValue(this.publisherDetails.address.addressLine1);
                 this.publisherProfileForm.get('addressLine2').setValue(this.publisherDetails.address.addressLine2);
                 this.publisherProfileForm.get('city').setValue(this.publisherDetails.address.city);
                 this.publisherProfileForm.get('state').setValue(this.publisherDetails.address.state);
                 this.publisherProfileForm.get('country').setValue(this.publisherDetails.address.country);
                 this.cityControl.setValue(this.publisherDetails.address.city);
                 this.stateControl.setValue(this.publisherDetails.address.state);
                 this.publisherProfileForm.get('zipCode').setValue(this.publisherDetails.address.zipcode);
                 this.publisherProfileForm.get('isPhNumberVerified').setValue(this.publisherDetails.isPhNumberVerified);
                this.countryControl.setValue(this.publisherDetails.address.country);
                 if (!this.publisherDetails.isPhNumberVerified) {
                   this.isPhoneNumberVerified = false;
                 } else {
                   this.isPhoneNumberVerified = true;
                   this.verifiedPhNumber = this.publisherDetails.phoneNumber;
                 }
                 if (this.publisherDetails.phoneNumber.startsWith('+91')) {
                   this.publisherProfileForm.get('countryCodePhNumber').setValue('+91');
                   this.publisherProfileForm.get('phoneNumber').setValue(this.publisherDetails.phoneNumber.replace('+91', ''));
                 } else {
                   this.publisherProfileForm.get('countryCodePhNumber').setValue('+1');
                   this.publisherProfileForm.get('phoneNumber').setValue(this.publisherDetails.phoneNumber.replace('+1', ''));
                 }
               } else {
                this.isNotRegiteredUser = false;
                this.platformStatus = 'add';
              }
             }
           );
        } else if (this.publisherProfile) {
          this.countrySelected = this.publisherProfile.address.country === 'US' ? '1' : '2';
          this.isNotRegiteredUser = false;
          this.showTagLine = true;
          this.publisherProfile = res;
          // this.showWalkthrough();
          this.editPersonalInfo = true;
          this.editBusinessAddress = false;
          this.id = this.publisherProfile._id;
          this.publisherProfileForm.get('phoneNumber').setValue(this.publisherProfile.phoneNumber);
          this.publisherProfileForm.get('organization').setValue(this.publisherProfile.organizationName);
          this.publisherProfileForm.get('acceptTerms').setValue(this.publisherProfile.acceptTerms);
          this.publisherProfileForm.get('addressLine1').setValue(this.publisherProfile.address.addressLine1);
          this.publisherProfileForm.get('addressLine2').setValue(this.publisherProfile.address.addressLine2);
          this.publisherProfileForm.get('city').setValue(this.publisherProfile.address.city);
          this.publisherProfileForm.get('state').setValue(this.publisherProfile.address.state);
          this.publisherProfileForm.get('country').setValue(this.publisherProfile.address.country);
          this.cityControl.setValue(this.publisherProfile.address.city);
          this.stateControl.setValue(this.publisherProfile.address.state);
          this.publisherProfileForm.get('zipCode').setValue(this.publisherProfile.address.zipcode);
          this.publisherProfileForm.get('isPhNumberVerified').setValue(this.publisherProfile.isPhNumberVerified);
          this.countryControl.setValue(this.publisherProfile.address.country);
          if (!this.publisherProfile.isPhNumberVerified) {
            this.isPhoneNumberVerified = false;
          } else {
            this.isPhoneNumberVerified = true;
            this.verifiedPhNumber = this.publisherProfile.phoneNumber;
          }
          if (this.publisherProfile.phoneNumber.startsWith('+91')) {
            this.publisherProfileForm.get('countryCodePhNumber').setValue('+91');
            this.publisherProfileForm.get('phoneNumber').setValue(this.publisherProfile.phoneNumber.replace('+91', ''));
          } else {
            this.publisherProfileForm.get('countryCodePhNumber').setValue('+1');
            this.publisherProfileForm.get('phoneNumber').setValue(this.publisherProfile.phoneNumber.replace('+1', ''));
          }

          if (this.id) {
            this.publisherPlatformService.getPlatformsByPublisherId(this.id).subscribe(
              result => {
                if (result) {
                  this.platformStatus = 'added';
                } else {
                  this.platformStatus = 'add';
                }
              }
            );
          }
        } else {
          this.showWalkthrough();
        }
        this.initUniqueZipCodes();
        this.initUniqueCities();
        this.initUniqueStates();
    });

    this.publisherPlatformService.getUniquePublisherSiteTypes().subscribe(
      res => {
        this.uniqueSities = res;
      },
      err => {
        console.log('Error!');
      });

    this.publisherPlatformService.getUniquePublisherAppTypes().subscribe(
      res => {
        this.uniqueApps = res;
        this.filteredApps = this.appCategoryControl.valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterAppType(name) : this.uniqueSities.slice())
          );
      },
      err => {
        console.log('Error!');
      });
      
      this.userDetails = this.authenticationService.currentUserValue;
      this.adminRolesAndPermissions();
  }

  showWalkthrough() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    let context = JSON.parse(localStorage.getItem('currentUser'))['usertype'];
    dialogConfig.data = {
      context
    };
    const dialogRef = this.dialog.open(WalkthroughComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
        dialogRef.close();
        let eventProperties = {
          type: JSON.parse(localStorage.getItem('currentUser'))['usertype']
        }
        this.amplitudeService.logEvent('onboarding - user clicks on get started', eventProperties);
    });
  }

  initUniqueZipCodes() {
    this.zipcodesService.getUniqueZipCodes(this.countrySelected).subscribe(
      res => {
        this.uniqueZipCodes = res;
        this.filteredZipCodes = this.publisherProfileForm.controls['zipCode'].valueChanges
          .pipe(
            startWith<string>(''),
            map(value => typeof value === 'string' ? value : value),
            map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
          );
      },
      err => {
        console.log('Error!');
      });
  }
  editSection(sectionType: any) {
    if (sectionType === 'personalInfoSection') {
      this.editPersonalInfo = !this.editPersonalInfo;
    }
    if (sectionType === 'businessAddress') {
      this.editBusinessAddress = !this.editBusinessAddress;
      if (this.editBusinessAddress) {
        this.currentTab = 2;
      } else {
        this.currentTab = 0;
      }
    }
  }
  selectAppType(value: string) {
    if (this.currentTab === 3) {
      this.typeOfAccount = value;
      if (value === 'Website') {
        this.publisherApp.reset();
      } else {
        this.publisherSite.reset();
      }
    }
  }

  private _filterCity(name: string): string[] {
    this.onChangeCityString();
    this.onChangeCity();
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueCities.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterState(name: string): string[] {
    let filterValue: string;
    if (name) {
      filterValue = name.toLowerCase();
    }
    return this.uniqueStates.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterCountry(value: string): CountryCurrenciesModel[] {
    let filterValue: string;
    if (value) {
      filterValue = value.toLowerCase();
    }
    const filteredCountries = this.countriesCurrencies.filter(
      countrycurrency =>
      countrycurrency.countryName.toLowerCase().indexOf(filterValue) === 0);
      return filteredCountries;
  }

  private _filterAppType(name: string): string[] {
    const filterValue = name.toLowerCase();
    return this.uniqueApps.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterZipCode(name: string): any {
    this.onChangeZipCodeString();
    this.onChangeZipCode();
    const filterValue = name.toLowerCase();
    return this.uniqueZipCodes.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  // searching cities by prefix with atleast 4 charecters
  onChangeCityString() {
    if (this.cityControl.value.length > 1) {
      this.zipcodesService.getUniqueCitiesOnChangeCity(this.cityControl.value, this.countrySelected).subscribe(
        res => {
          this.uniqueCities = res;
          this.filteredCities = this.cityControl.valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterCity(name) : this.uniqueCities.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueCities();
    }
  }
  // end
   // searching zipcodes by prefix with atleast 4 charecters
   onChangeZipCodeString() {
    if (this.cityControl && this.cityControl.value && this.cityControl.value.length > 1) {
      this.zipcodesService.getUniqueZipCodesOnChangeZipCode(this.publisherProfileForm.controls['zipCode'].value, this.countrySelected).subscribe(
        res => {
          this.uniqueZipCodes = res;
          this.filteredZipCodes = this.publisherProfileForm.controls['zipCode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
        },
        err => {
          console.log('Error!');
      });
    } else {
      this.initUniqueZipCodes();
    }
  }
  onCountryChange(countryISO2Code) {
    this.stateControl.setValue('');
    this.cityControl.setValue('');
    this.publisherProfileForm.get('zipCode').setValue('');
    this.countrySelected = countryISO2Code === 'US' ? '1' : '2';
    this.defaultCountryCode = countryISO2Code === 'US' ? '+1' : countryISO2Code === 'IN' ? '+91': '+1';
    this.publisherProfileForm.get('countryCodePhNumber').setValue(this.defaultCountryCode);
    this.initUniqueZipCodes();
    this.initUniqueCities();
    this.initUniqueStates();
  }

  // call on city change and change the country state and pin.
  onChangeCity() {
    if (this.uniqueCities.length) {
      const cityName = this.cityControl.value;
      this.isValidCity = this.uniqueCities.includes(cityName);
      if (this.isValidCity) {
        this.stateControl.setValue('');
        this.utilService.getCityDetailsByCityName(cityName, this.countrySelected).subscribe(
          res => {
            this.cityStateDetails = res;
            this.uniqueStates = [];
            this.uniqueStates = this.cityStateDetails.map(function(item) {
              return item['stateFullName'];
            });
            this.uniqueStates = Array.from(new Set(this.uniqueStates));
            this.filteredStates = this.stateControl.valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => this._filterState(name))
            );
            this.uniqueZipCodes = [];
            this.uniqueZipCodes = this.cityStateDetails.map(function(item) {
              return item['zipcode'];
            });
            if (!this.uniqueZipCodes.includes(this.publisherProfileForm.controls['zipCode'].value)) {
              this.publisherProfileForm.get('zipCode').setValue('');
            }
            this.filteredZipCodes = this.publisherProfileForm.controls['zipCode'].valueChanges
            .pipe(
              startWith<string>(''),
              map(value => typeof value === 'string' ? value : value),
              map(name => name ? this._filterZipCode(name) : this.uniqueZipCodes.slice())
            );
          },
          err => {
            console.log(err);
          }
        );
      }
    }
  }
  // call on city change and change the country state and pin.
  onChangeZipCode() {
  if (this.uniqueZipCodes.length) {
    const zipCode = this.publisherProfileForm.controls['zipCode'].value;
    this.isValidZipCode = this.uniqueZipCodes.includes(zipCode);
    if (this.isValidZipCode) {
      this.cityControl.setValue('');
      this.stateControl.setValue('');
      // this.countryControl.setValue('');
      this.utilService.getCityDetailsByZipCode(zipCode, this.countrySelected).subscribe(
        res => {
          this.cityStateDetails = res;
          this.cityControl.setValue(this.cityStateDetails.city);
          this.stateControl.setValue(this.cityStateDetails.stateFullName);
          // this.countryControl.setValue(this.cityStateDetails.country);
        },
        err => {
          console.log(err);
        }
      );
      // get
    }
  }
  }
// end
  // end
  savePublisher() {
    this.spinner.showSpinner.next(true);
    let valid = true;
      this.publisherProfileForm.get('city').setValue(this.cityControl.value);
      this.publisherProfileForm.get('state').setValue(this.stateControl.value);
      this.publisherProfileForm.get('country').setValue(this.countryControl.value);
      this.publisherProfileForm.get('isPhNumberVerified').setValue(this.isPhoneNumberVerified);
      this.publisherProfileForm.get('acceptTerms').enable();
      if (!this.id) {
        if (this.typeOfAccount === 'MobileApp') {
          valid = this.validateManagePlatformApp(this.publisherApp, 'appLink', 'appName', 'appCategory', 'appType', 'marketSel');
        } else {
          valid = this.validateManagePlatformSite(this.publisherSite, 'siteLink', 'siteName', 'siteType', 'marketSel');
        }
        
        if (valid) {
         this.publisherProfileService.addProfile(this.publisherProfileForm.value).subscribe(
            res => {
              this.addPublisherPersonalInfo();
              const platform: any = this.savePlatform(res._id, res);
              this.spinner.showSpinner.next(false);
            },
            err => {
              this.spinner.showSpinner.next(false);
              console.log('Error!');
          });
        }
      } else {
        if (this.validateBusinessAddress()) {
          this.spinner.showSpinner.next(true);
          this.publisherProfileForm.get('id').setValue(this.id);
          this.publisherProfileService.updateProfile(this.publisherProfileForm.value).subscribe(
            res => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: 'Business Address Updated Successfully'
                }
              });
              this.editSection('businessAddress');
              this.spinner.showSpinner.next(false);
            },
            err => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
              console.log('Error!');
          });
        }
      }
      this.spinner.showSpinner.next(false);
  }

  addPublisherPersonalInfo() {
    if (this.validatePersonalInformation()) {
      this.usr.firstName = this.publisherProfileForm.get('firstName').value;
      this.usr.lastName = this.publisherProfileForm.get('lastName').value;
      this.usr.email = this.useremail;
      this.userService.updateFirstAndLastName(this.usr).subscribe(
        res => {
          if (!this.userDetails.hasPersonalProfile) {
            this.userDetails.hasPersonalProfile = true;
            localStorage.setItem('currentUser', JSON.stringify(this.userDetails));
            this.eventEmitterService.onClickNavigation();
          }
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
      });
    }
  }
  validatePlatformDetails() {
    let valid = false;
    if (this.typeOfAccount === 'MobileApp') {
      valid = this.validateManagePlatformApp(this.publisherApp, 'appLink', 'appName', 'appCategory', 'appType', 'marketSel');
      if (valid) {
      this.spinner.showSpinner.next(true);
      this.checkPlatformURLAvailablity(this.publisherApp.get('appLink').value);
      }
    } else {
      valid = this.validateManagePlatformSite(this.publisherSite, 'siteLink', 'siteName', 'siteType', 'marketSel');
      if (valid) {
        this.spinner.showSpinner.next(true);
        this.checkPlatformURLAvailablity(this.publisherSite.get('siteLink').value);
      }
    }
  //  this.spinner.showSpinner.next(false);
  }
  // update only first and last name of publisher.
  updatePublisherPersonalInfo() {
    if (this.validatePersonalInformation() && this.checkChangeInName()) {
      this.usr.firstName = this.publisherProfileForm.get('firstName').value;
      this.usr.lastName = this.publisherProfileForm.get('lastName').value;
      this.usr.email = this.useremail;
      this.publisherUserService.getPublisherUserPermissions(this.useremail).subscribe(
        permissions => {
          if (!this.userDetails.hasPersonalProfile && permissions['zone'] &&
        (localStorage.getItem('zone') === 'null' || localStorage.getItem('zone') === 'undefined'
        || permissions['zone'] !== localStorage.getItem('zone'))) {
          localStorage.setItem('zone', permissions['zone']);
        }
        
      this.userService.updateFirstAndLastName(this.usr).subscribe(
        res => {
          if (!this.userDetails.hasPersonalProfile) {
            this.userDetails.hasPersonalProfile = true;
            localStorage.setItem('currentUser', JSON.stringify(this.userDetails));
            this.eventEmitterService.onClickNavigation();
          }
          if ( !this.checkChangeInOrganisation()) {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'success',
                message: 'Personal Information Updated Successfully.'
              }
            });
            this.editSection('personalInfoSection');
          }

        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
      });
        });
    }

    if (this.validatePersonalInformation() && this.checkChangeInOrganisation()) {
      this.publisherProfileForm.get('id').setValue(this.id);
      this.publisherProfileForm.get('city').setValue(this.cityControl.value);
      this.publisherProfileForm.get('state').setValue(this.stateControl.value);
      this.publisherProfileForm.get('country').setValue(this.countryControl.value);
      this.publisherProfileForm.get('acceptTerms').enable();
      this.publisherProfileService.updateProfile(this.publisherProfileForm.value).subscribe(
        res => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'success',
              message: 'Personal Information Updated Successfully'
            }
          });
          this.editSection('personalInfoSection');
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
          console.log('Error!');
      });
    }
  }
  // end
  checkChangeInName(): boolean {
    if (this.usr.firstName !== this.publisherProfileForm.get('firstName').value.trim()) {
      return true;
    } else if (this.usr.lastName !== this.publisherProfileForm.get('lastName').value.trim()) {
      return true;
    }
    return false;
  }
  checkChangeInOrganisation(): boolean {
    if (
      this.publisherProfile && this.publisherProfile.organizationName.trim() !== this.publisherProfileForm.get('organization').value.trim()
    ) {
      return true;
    }
    return false;
  }
  savePlatform(id: String, publisher: any) {

    let m;
    if (this.typeOfAccount === 'MobileApp') {
      m = this.publisherApp.get('marketSel').value;
      this.publisherPlatform = new PublisherPlatform('', this.typeOfAccount, this.publisherApp.get('appLink').value,
        this.publisherApp.get('appName').value, this.publisherApp.get('appCategory').value, '', [], null, '', 
        'pending', id, 'N/A', 0, false, false, false, false, '', this.publisherApp.get('appType').value);
    } else {
      m = this.publisherSite.get('marketSel').value;
      this.publisherPlatform = new PublisherPlatform(
      '', this.typeOfAccount, this.publisherSite.get('siteLink').value,
      this.publisherSite.get('siteName').value, this.publisherSite.get('siteType').value, '', [], null, '',
      'pending', id, 'N/A', 0, false, false, false, false, '', '');
    }

    this.publisherPlatformService.addPlatform(this.publisherPlatform, m).subscribe(
      res => {
        if (res) {
          this.amplitudeService.logEvent('profile_creation - user enters new platform', null);
          const adminPermission = this.listOfExistedUserRolesPermissions.find((thisItem) =>
          thisItem.roleName.toLowerCase().trim() === 'Admin'.toLowerCase().trim());
          const publisherUser = {
            email: publisher['user'].email,
            userRoleId: adminPermission._id,
            platformId: res._id,
            publisherId: publisher._id
          };
          this.publisherUserService.addPublisherUser(publisherUser, m).subscribe(
            userResponse => {
              this.spinner.showSpinner.next(false);
              this.spinner.showSpinner.next(false);
              this.eventEmitterService.onSavedPublisherProfile();
              this.router.navigate(['/publisher/publisherProfileThanks'],  
              { 'state': {'addPlatformId':  JSON.stringify(res._id), 
              'addPlatformType': JSON.stringify(this.typeOfAccount) }});
            },
            error => {
              console.log('Error!');
            });
        }
      }
    );
  }


  validatePersonalInformation(): boolean {
    const formGroup = this.publisherProfileForm;
    const firstNameCtrl = formGroup.controls['firstName'];
    const lastNameCtrl = formGroup.controls['lastName'];
    const organizationCtrl = formGroup.controls['organization'];
    const acceptTermsCtrl = formGroup.controls['acceptTerms'];

    let isValid = true;

    if (!firstNameCtrl.value) {
      firstNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(this.publisherProfileForm, 'firstName')) {
      isValid = false;
    }
    if (!lastNameCtrl.value) {
      lastNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(this.publisherProfileForm, 'lastName')) {
      isValid = false;
    }
    if (!organizationCtrl.value) {
      organizationCtrl.setErrors({ require: true });
      isValid = false;
    }
    // else if (!this.checkForSpecialCharactersAndNumber(this.publisherProfileForm, 'organization')) {
    //   isValid = false;
    // }

    if (!this.id  && !acceptTermsCtrl.value) {
      acceptTermsCtrl.setErrors({ require: true });
      isValid = false;
    }

    return isValid;
  }


  validateManagePlatformApp(formGroup: FormGroup, appLink: string, appName: string, appCategory: string, 
    appType: string, marketSel: string): boolean {

    const appLinkCtrl = formGroup.controls[appLink];
    const appNameCtrl = formGroup.controls[appName];
    const marketCtrl = formGroup.controls[marketSel];
    const appCategoryCtrl = formGroup.controls[appCategory];
    const appTypeCtrl = formGroup.controls[appType];
    let isValid = true;

    if (!appLinkCtrl.value) {
      appLinkCtrl.setErrors({ require: true });
      isValid = false;
    } else if (appLinkCtrl.value.toLowerCase().indexOf('http://') !== -1 ||
      appLinkCtrl.value.toLowerCase().indexOf('https://') !== -1 ||
      appLinkCtrl.value.toLowerCase().indexOf('://') !== -1) {
        appLinkCtrl.setErrors({ containsHttpOrHtttps: true });
      isValid = false;
    }
    // else if (!this.checkForSpecialCharactersAndNumber(formGroup, siteName)) {
    //   isValid = false;
    // }

    if (!appNameCtrl.value) {
      appNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (appNameCtrl.value.trim().length < 3) {
      appNameCtrl.setErrors({ minlength: true });
      isValid = false;
    }

    if (!appCategoryCtrl.value) {
      appCategoryCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!appTypeCtrl.value) {
      appTypeCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!marketCtrl.value) {
      marketCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }

  checkPlatformURLAvailablity(value: string) {
    this.spinner.showSpinner.next(true);
    let siteLinkCtrl = this.publisherSite.controls['siteLink'];
    if (this.typeOfAccount === 'MobileApp') {
      siteLinkCtrl = this.publisherApp.controls['appLink'];
    }
    this.publisherPlatformService.checkPlatformURLAvailablity({ url: value}).subscribe(
      res => {
        value = 'http://' + value;
        this.utilService.validateExternalURL(value).subscribe(
          result => {
            if (result && result.toLowerCase() === 'true') {
              this.savePublisher();
            } else {
              this.spinner.showSpinner.next(false);
              siteLinkCtrl.setErrors({ invalidURL: true });
              return false;
            }
          },
          error => {
            this.spinner.showSpinner.next(false);
            siteLinkCtrl.setErrors({ invalidURL: true });
            return false;
          });
      },
      err => {
        this.spinner.showSpinner.next(false);
        siteLinkCtrl.setErrors({ urlNotAvailable: true });
        return false;
      });

  }

  validateManagePlatformSite(formGroup: FormGroup, siteLink: string, siteName: string, siteType: string, marketSel: string) {

    const siteLinkCtrl = formGroup.controls[siteLink];
    const siteNameCtrl = formGroup.controls[siteName];
    const marketCtrl = formGroup.controls[marketSel];
    const siteTypeCtrl = formGroup.controls[siteType];
    let isValid = true;

    if (!siteLinkCtrl.value) {
      siteLinkCtrl.setErrors({ require: true });
      isValid = false;
    } else if (siteLinkCtrl.value.toLowerCase().indexOf('http://') !== -1 ||
      siteLinkCtrl.value.toLowerCase().indexOf('https://') !== -1 ||
      siteLinkCtrl.value.toLowerCase().indexOf('://') !== -1) {
        siteLinkCtrl.setErrors({ containsHttpOrHtttps: true });
      isValid = false;
    }
    // else if (!this.checkForSpecialCharactersAndNumber(formGroup, siteName)) {
    //   isValid = false;
    // }

    if (!siteNameCtrl.value) {
      siteNameCtrl.setErrors({ require: true });
      isValid = false;
    } else if (siteNameCtrl.value.trim().length < 3) {
      siteNameCtrl.setErrors({ minlength: true });
      isValid = false;
    }

    if (!siteTypeCtrl.value) {
      siteTypeCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!marketCtrl.value) {
      marketCtrl.setErrors({ require: true });
      isValid = false;
    }
    return isValid;
  }

  validateBusinessAddress(): boolean {
    const formGroup = this.publisherProfileForm;
    const addr1Ctrl = formGroup.controls['addressLine1'];
    this.publisherProfileForm.get('city').setValue(this.cityControl.value);
    this.publisherProfileForm.get('state').setValue(this.stateControl.value);
    this.publisherProfileForm.get('country').setValue(this.countryControl.value);
    const phoneNumberCtrl = formGroup.controls['phoneNumber'];
    const countryCodephNumCtrl = formGroup.controls['countryCodePhNumber'];


    const cityCtrl = formGroup.controls['city'];
    const stateCtrl = formGroup.controls['state'];
    const countryCtrl = formGroup.controls['country'];
    const zipCodeCtrl = formGroup.controls['zipCode'];
    let isValid = true;

    if (!addr1Ctrl.value) {
      addr1Ctrl.setErrors({ require: true });
      isValid = false;
    }
    if (!cityCtrl.value) {
      cityCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'city')) {
      isValid = false;
    }
    if (!stateCtrl.value) {
      stateCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'state')) {
      isValid = false;
    }
    if (!countryCtrl.value) {
      countryCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!this.checkForSpecialCharactersAndNumber(formGroup, 'country')) {
      isValid = false;
    }
    if (!zipCodeCtrl.value) {
      zipCodeCtrl.setErrors({ require: true });
      isValid = false;
    } else if (!Number(zipCodeCtrl.value.trim())) {
      zipCodeCtrl.setErrors({ invalidCharacters: true });
      isValid = false;
    } else if (this.countrySelected === '1' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 5) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
      isValid = false;
    } else if (this.countrySelected === '2' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 6) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    }
    if (!phoneNumberCtrl.value) {
      phoneNumberCtrl.setErrors({ require: true });
      isValid = false;
    } else if (phoneNumberCtrl.value.trim()) {
      const phNumber = countryCodephNumCtrl.value.trim() + phoneNumberCtrl.value.trim();
      // phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '').;
      let validatePhNUmber = phNumber.replace('+', '');
      validatePhNUmber = (validatePhNUmber.match(/[(), ,+,-]/g) != null) ? validatePhNUmber.replace(/[(), ,+,-]/g, '') : validatePhNUmber;
      if (!validatePhNUmber.match(/^\d+$/)) {
        phoneNumberCtrl.setErrors({ invalidPhNumber: true });
        isValid = false;
      } else {
        const cleaned = ('' + phoneNumberCtrl.value.trim()).replace(/\D/g, '');
        if (countryCodephNumCtrl.value.startsWith('+1')) {
            const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
            if (match) {
           //   const intlCode = (match[1] ? '+1 ' : '');
              phoneNumberCtrl.setValue(['(', match[2], ') ', match[3], '-', match[4]].join(''));
            }
            const trimmedNumber = phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '');
            if (trimmedNumber.length !== 10 || !trimmedNumber.match(/^\d+$/)) {
              phoneNumberCtrl.setErrors({ invalidPhNumber: true });
              isValid = false;
              this.isPhoneNumberVerified = null;
            }
        } else if (countryCodephNumCtrl.value.startsWith('+91')) {
          const indianNumber = phoneNumberCtrl.value.match(/[(), ,+,-]/g) != null ? phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '') :
          phoneNumberCtrl.value;
          phoneNumberCtrl.setValue(indianNumber);
          if (indianNumber.length !== 10 || !indianNumber.match(/^\d+$/)) {
            phoneNumberCtrl.setErrors({ invalidPhNumber: true });
            isValid = false;
            this.isPhoneNumberVerified = null;
          }
        }
      }
    }
    return isValid;
  }
  validatePhoneNumber() {
    const phoneNumberCtrl = this.publisherProfileForm.controls['phoneNumber'];
    const countryCodephNumCtrl = this.publisherProfileForm.controls['countryCodePhNumber'];
    if (!phoneNumberCtrl.value) {
      this.isPhoneNumberVerified = null;
    } else if (phoneNumberCtrl.value.trim()) {
      const phNumber = countryCodephNumCtrl.value.trim() + phoneNumberCtrl.value.trim();
      let validatePhNUmber = phNumber.replace('+', '');
      validatePhNUmber = (validatePhNUmber.match(/[(), ,+,-]/g) != null) ? validatePhNUmber.replace(/[(), ,+,-]/g, '')
      : validatePhNUmber;
      if (!validatePhNUmber.match(/^\d+$/)) {
        this.isPhoneNumberVerified = null;
      } else {
        const cleaned = ('' + phoneNumberCtrl.value.trim()).replace(/\D/g, '');
        if (countryCodephNumCtrl.value.startsWith('+1')) {
          if (phNumber.startsWith('+1')) {
            const trimmedNumber = phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '');
            const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
            if (match) {
              phoneNumberCtrl.setValue(['(', match[2], ') ', match[3], '-', match[4]].join(''));
              if (!this.id && !this.isPhoneNumberVerified && trimmedNumber.length === 10) {
                this.isPhoneNumberVerified = false;
              } else if (trimmedNumber.length === 10) {
                this.checkChangeInPhoneNumber('+1' + phoneNumberCtrl.value);
              }
            }
            if (trimmedNumber.length !== 10 || !trimmedNumber.match(/^\d+$/)) {
              // phoneNumberCtrl.setErrors({ invalidPhNumber: true });
               this.isPhoneNumberVerified = null;
             }
          } else if (!phNumber.startsWith('+91')) {
            this.isPhoneNumberVerified = null;
          } else {
            this.isPhoneNumberVerified = null;
          }
        } else if (countryCodephNumCtrl.value.startsWith('+91')) {
          if (phNumber.startsWith('+91')) {
            const indianNumber = phoneNumberCtrl.value.match(/[(), ,+,-]/g) != null ? phoneNumberCtrl.value.replace(/[(), ,+,-]/g, '') :
            phoneNumberCtrl.value;
            phoneNumberCtrl.setValue(indianNumber);
            const phoneNumber = phNumber.replace(/[(), ,-]/g, '');
            if (!this.id && !this.isPhoneNumberVerified && phoneNumber.length === 13) {
              this.isPhoneNumberVerified = false;
            } else if (phoneNumber.length === 13) {
              this.checkChangeInPhoneNumber(countryCodephNumCtrl.value + phoneNumberCtrl.value);
            } else {
              this.isPhoneNumberVerified = null;
            }
          }
        } else if (cleaned.length === 10 && !phNumber.startsWith('+91') && !phNumber.startsWith('+1')) {
          this.isPhoneNumberVerified = null;
        }
      }
    }
  }
  checkChangeInPhoneNumber(phNumber: string) {
    if (this.verifiedPhNumber) {
      if (this.verifiedPhNumber !== phNumber) {
        this.isPhoneNumberVerified = false;
      } else {
        this.isPhoneNumberVerified = this.publisherProfileForm.get('isPhNumberVerified').value;
      }
    } else {
      this.isPhoneNumberVerified = false;
    }
  }

  validatePincode() {
    const zipCodeCtrl = this.publisherProfileForm.controls['zipCode'];
    if (this.countrySelected === '1' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 5 ) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    } else if (this.countrySelected === '2' && zipCodeCtrl.value && zipCodeCtrl.value.length !== 6) {
      zipCodeCtrl.setErrors({invalidCharacters: true});
    }
  }
  openTermsAndConditionsDialog() {
    if ( this.currentTab === 1) {
      const dialogRef = this.dialog.open(TermsAndConditionsComponent, {
        width: '50%'
      });
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }
  openPhoneNumberValidationDialog(limitMsg: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = '50%';
    const phoneNumber = this.publisherProfileForm.get('countryCodePhNumber').value + this.publisherProfileForm.get('phoneNumber').value;
    dialogConfig.data = {
      phNumber: phoneNumber,
      limitMsg: limitMsg,
      userType: 'Publisher'
    };
      const dialogRef = this.dialog.open(OptValidationComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(res => {
        if ( res && res.status === 'success') {
            this.verifiedPhNumber = res.phNumber;
            this.isPhoneNumberVerified = true;
            if (this.id) {
              this.spinner.showSpinner.next(true);
            //  this.publisherProfileService.getProfileByEmail(this.useremail).subscribe( res => {
                this.publisherProfileForm.get('isPhNumberVerified').setValue(true);
                this.publisherProfileForm.get('id').setValue(this.id);
                this.publisherProfileService.updateProfile(this.publisherProfileForm.value).subscribe(rt => {
                  this.spinner.showSpinner.next(false);
                  this.publisherProfile = rt;
                },
                err => {
                  this.spinner.showSpinner.next(false);
                });
            }
        }
      });
  }
  adminRolesAndPermissions() {
    this.configurationService.getalluserolesByModuleType('Publisher').subscribe(
      res => {
        if (res) {
          this.listOfExistedUserRolesPermissions = <USEROLESElement[]>res;
        }
      },
      err => {
        console.log(err);
      });
  }

  checkForSpecialCharactersAndNumber(formGroup: FormGroup, ctrlName: string) {
    let isValid = true;
    if (!formGroup.controls[ctrlName].value.trim().replace(/\s/g, '').match(/^[A-Za-z]*$/)) {
      formGroup.controls[ctrlName].setErrors({ invalidCharacters: true });
      isValid = false;
    }
    return isValid;
  }

  requestOTP() {
    if (this.currentTab === 2 || this.editBusinessAddress) {
      this.spinner.showSpinner.next(true);
      const phoneNumber = this.publisherProfileForm.get('countryCodePhNumber').value + this.publisherProfileForm.get('phoneNumber').value;
      this.smsService.verifyPhoneNumber(phoneNumber).
      subscribe( res => {
        this.spinner.showSpinner.next(false);
        this.openPhoneNumberValidationDialog(res['otpLimitMsg']);
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      });
    }
  }

}
