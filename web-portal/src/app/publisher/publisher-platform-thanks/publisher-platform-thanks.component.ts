import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-publisher-platform-thanks',
  templateUrl: './publisher-platform-thanks.component.html',
  styleUrls: ['./publisher-platform-thanks.component.scss']
})
export class PublisherPlatformThanksComponent implements OnInit {

  public platformName: string;
  public platformLink: string;
  public platformType: string;
  public platformId: string;
  public platformZOne: string;

  constructor(public router: Router,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private publisherPlatformService: PublisherPlatformService,
    private amplitudeService: AmplitudeService) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.platformName = JSON.parse(this.router.getCurrentNavigation().extras.state.platformName);
      this.platformLink = JSON.parse(this.router.getCurrentNavigation().extras.state.platformLink);
      this.platformType = JSON.parse(this.router.getCurrentNavigation().extras.state.platformType);
      this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.platformId);
      this.platformZOne = JSON.parse(this.router.getCurrentNavigation().extras.state.platformZOne);
      }

   }

  ngOnInit() {
  }

  navigateTo() {

  }
  switchPlatform() {
    this.spinner.showSpinner.next(true);
    this.publisherPlatformService.getPlatformByIdZone(this.platformId, this.platformZOne).subscribe ( res => {
      this.spinner.showSpinner.next(false);
      if (res['isWatching']) {
        this.router.navigate(['/publisher/platformManage']);
      } else {
        const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
          width: '50%',
          data: {
            'title' : 'Do you want to switch "' + res['name'] + '" platform'
          }
        });
        confirmDialogRef.afterClosed().subscribe(confirmResult => {
          if (confirmResult) {
            this.spinner.showSpinner.next(true);
            this.publisherPlatformService.switchToPlatform(this.platformId).subscribe(
              result => {
                this.spinner.showSpinner.next(false);
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'success',
                    message: result['message']
                  }
                });
                this.router.navigate(['/publisher/platformManage']);
              },
              err => {
                this.spinner.showSpinner.next(false);
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
              });
            } else {
            this.router.navigate(['/publisher/platformManage']);
          }
        });
      }
    },
    err => {
      this.spinner.showSpinner.next(false);
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: err.error['message']
        }
      });
    });
  }
  goToAddAsset() {
    this.amplitudeService.logEvent('platform_addition - user clicks on add first slot', null);
    this.spinner.showSpinner.next(true);
    this.publisherPlatformService.getPlatformByIdZone(this.platformId, this.platformZOne).subscribe ( res => {
      if (res['isWatching']) {
        this.router.navigate(['/publisher/assetCreation']);
      } else {
        this.spinner.showSpinner.next(false);
        const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
          width: '50%',
          data: {
            'title' : 'Do you want to switch "' + res['name'] + '" platform to add Slots for this platform'
          }
        });
        confirmDialogRef.afterClosed().subscribe(confirmResult => {
          if (confirmResult) {
            localStorage.setItem('zone', this.platformZOne)
            this.spinner.showSpinner.next(true);
            this.publisherPlatformService.switchToPlatform(this.platformId).subscribe(
              result => {
                this.spinner.showSpinner.next(false);
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'success',
                    message: result['message']
                  }
                });
                this.router.navigate(['/publisher/assetCreation']);
              },
              err => {
                this.spinner.showSpinner.next(false);
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
              });
            }
          });
        }
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      });
  }
}
