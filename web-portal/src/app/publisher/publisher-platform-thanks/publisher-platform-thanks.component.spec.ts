import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherPlatformThanksComponent } from './publisher-platform-thanks.component';

describe('PublisherPlatformThanksComponent', () => {
  let component: PublisherPlatformThanksComponent;
  let fixture: ComponentFixture<PublisherPlatformThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherPlatformThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherPlatformThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
