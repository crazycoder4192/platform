import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherSupportComponent } from './publisher-support.component';

describe('PublisherSupportComponent', () => {
  let component: PublisherSupportComponent;
  let fixture: ComponentFixture<PublisherSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherSupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
