import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherTicketDetailsComponent } from './publisher-ticket-details.component';

describe('PublisherTicketDetailsComponent', () => {
  let component: PublisherTicketDetailsComponent;
  let fixture: ComponentFixture<PublisherTicketDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherTicketDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherTicketDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
