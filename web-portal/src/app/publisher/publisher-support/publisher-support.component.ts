import { Component, OnInit } from '@angular/core';
import { SupportService } from 'src/app/_services/admin/support.service';
import { Ticket } from 'src/app/_models/ticket_model';
import { SpinnerService } from 'src/app/_services/spinner.service';

@Component({
  selector: 'app-publisher-support',
  templateUrl: './publisher-support.component.html',
  styleUrls: ['./publisher-support.component.scss']
})
export class PublisherSupportComponent implements OnInit {
  displayedColumns: string[] = ['id', 'subject', 'priority', 'status', 'createdOn', 'updatedOn', 'actions'];
  tickets: Ticket[];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;

  constructor(
    private supportService: SupportService,
    private spinner: SpinnerService
  ) { }

  ngOnInit() {
    this.loadAllTickets();
  }
  loadAllTickets() {
    this.supportService.getTicketsByEmail().subscribe(
      resposne => {
        this.tickets = <Ticket[]>resposne;
      },
      err => {
        console.log('Error! createTicket.supportService.addTicket : ' + err);
    });
  }

}
