import { Component, OnInit } from '@angular/core';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { PublisherPlatform } from 'src/app/_models/publisher_platform_model';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { Publisher } from 'src/app/_models/publisher_model';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { PublisherComponent } from '../publisher.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { USEROLESElement } from 'src/app/admin/configuration/dataModel/userRolesPermissionsModel';
import { ConfirmPasswordDailogComponent } from 'src/app/common/confirm-password-dailog/confirm-password-dailog.component';

@Component({
  selector: 'app-platform-manage',
  templateUrl: './platform-manage.component.html',
  styleUrls: ['./platform-manage.component.scss']
})
export class PlatformManageComponent implements OnInit {

  public publisherPlatform: PublisherPlatform;
  public plateformsDataSource: PublisherPlatform[];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  private publisherProfile: Publisher;
  public displayedColumns: string[] = [
    'name', 'typeOfSite', 'platformStatus', 'activeAssets', 'market', 'actions'
  ];
  public displayedUsersColumns: string[] = ['name', 'email', 'userPlatform', 'role', 'actions'];
  public selectedPlatformId: any;
  public platform: any;
  plateformsData: any;
  public addUserForm: FormGroup;
  selectedPlatforms: string[];
  addUserSubmitted: boolean;
  selectedPlatformIdForAddUser: any;
  listOfExistedUserRolesPermissions: any[];
  filteredUserRolesPermissions: any;
  selectedRoleIdForAddUser: any;
  publisherUsers: any;
  userPlatforms: any;
  userDeleteMsg: string;
  errMsg: string;
  rolePermissions: string[];
  selectedRole: string;
  selectedMarketPlace = '1';
  filteredUserPlatforms: any;
  showMarketColumn = 'table-cell';
  allZonePlatforms: any;
  androidApps: any;
  iosApps: any;
  constructor(
    private publisherPlatformService: PublisherPlatformService,
    private router: Router,
    private snackBar: MatSnackBar,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    public assetService: AssetService,
    private formBuilder: FormBuilder,
    private publisherUserService: PublisherUserService,
    private configHTTPService: ConfigurationService
    ) {
      this.selectedMarketPlace = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
     }

  ngOnInit() {
    this.addUserSubmitted = false;
    this.loadUserByEmail();
    this.loadPlatformsByPublisherId('Website');
    this.createAddUserForm();
    this.adminRolesAndPermissions();
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
  }
  loadPublisherUsers() {
    this.publisherUserService.getPublisherUsers(this.publisherProfile._id).subscribe(
      res => {
        this.publisherUsers = res;
        this.publisherUsers.forEach(element => {
          if (element.firstName) {
            element.userName = element.firstName +  ' ' + element.lastName;
          } else {
            element.userName = '-';
          }
        });
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }
  createAddUserForm() {
    this.addUserForm = this.formBuilder.group({
      role: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      userPlatform: ['', Validators.compose([Validators.required])]
    });
  }
  loadUserByEmail(): any {
    this.publisherUserService.getPublisherDetails(this.userEmail).subscribe(
      res => {
        this.publisherProfile = res;
        this.loadPlatforms();
        this.loadPublisherUsers();
      }
    );
  }
  adminRolesAndPermissions() {
    this.configHTTPService.getalluserolesByModuleType('Publisher').subscribe(
      res => {
        if (res) {
          this.filteredUserRolesPermissions = <USEROLESElement[]>res;
        }
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }
  loadPlatformsByPublisherId(platformType) {
    this.androidApps = [];
    this.iosApps = [];
    this.publisherPlatformService.getAllZonesByPublisherId().subscribe(
      result => {
        this.allZonePlatforms = result;
        if (result && result.length > 0) {
          // Get total no of zones
          this.showMarketColumn = ([...new Set(result.map((x: any) => x.zone))].length > 1 ? 'table-cell' : 'none');
        }
      },
      error => { });
    this.publisherPlatformService.getPlatformsByPlatformTypeAndPublisherId(platformType).subscribe(
      res => {
        if (res && res.length > 0) {
          res = res.sort((a, b) => (a.name > b.name) ? 1 : -1);
        }
        this.plateformsDataSource = res;
        this.plateformsData = res;
        this.plateformsDataSource.forEach(element => {
          if (element['platforms'][0].appType === 'Android') {
            this.androidApps.push(element);
          }
          if (element['platforms'][0].appType === 'IOS') {
            this.iosApps.push(element);
          }
          this.assetService.getActiveAssetsByPlatformId(element['platforms'][0]._id).subscribe(
            rev => {
              const response = <Array<any>> rev;
              element['activeAssets'] = response.length;
            });
          // const isPlatformWatching = element.isWatching;
          if (element['isWatching'] && element['platforms'][0].zone === this.selectedMarketPlace) {
            this.selectedPlatformId = element['platforms'][0]._id;
          }
        });
        this.searchPlatform(this.platform);
      },
      err => {
        console.log('Error!');
      });
  }
  addNewPlatform( platformType ) {
    this.router.navigate(['/publisher/platformCreation'], { state: { platformType: JSON.stringify(platformType) }});
  }
  editPlatform( platformType,  platformId, platformZone) {
    // localStorage.setItem('platformType', JSON.stringify(platformType));
    // localStorage.setItem('platformId', JSON.stringify(platformId));
    if (this.selectedPlatformId !== platformId) {
      const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
        width: '50%',
        data: {
          'title' : 'Are you sure you want to switch to this platform?'
        }
      });
      confirmDialogRef.afterClosed().subscribe(confirmResult => {
        if (confirmResult) {
          this.selectedPlatformId = platformId;
          localStorage.setItem('zone', platformZone);
          this.publisherPlatformService.switchToPlatform(platformId).subscribe(
            res => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: res.message
                }
              });
              this.router.navigate(['/publisher/platformCreation'],
              { state: { 'platformType': JSON.stringify(platformType), 'platformId':  JSON.stringify(platformId) } });
            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
            });
        }
      });
    } else {
      this.router.navigate(['/publisher/platformCreation'],
      { state: { 'platformType': JSON.stringify(platformType), 'platformId':  JSON.stringify(platformId) } });
    }
  }
  switchPlatform(platformType, platformId, platformZone) {
    if (this.selectedPlatformId !== platformId) {
      const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
        width: '50%',
        data: {
          'title' : 'Are you sure you want to switch to this platform?'
        }
      });
      confirmDialogRef.afterClosed().subscribe(confirmResult => {
        if (confirmResult) {
          this.selectedPlatformId = platformId;
          localStorage.setItem('zone', platformZone);
          this.publisherPlatformService.switchToPlatform(platformId).subscribe(
            res => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: res.message
                }
              });
            this.loadPlatformsByPublisherId(platformType);
            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
            });
        }
      });
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'success',
          message: 'Platform already watching'
        }
      });
    }
  }
  searchPlatform(name) {
    if (!name) {
      this.plateformsDataSource = this.plateformsData;
    } else {
      this.plateformsDataSource = this.plateformsData.filter(option =>
        option.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
      );
    }
    return this.plateformsDataSource;
  }
  loadPlatforms() {
    this.publisherPlatformService.getAllZonesByPublisherId().subscribe(
      res => {
        this.userPlatforms = res;
        this.filteredUserPlatforms = this.userPlatforms.filter(option =>
          option.zone === this.selectedMarketPlace
        );
      }
    );
  }
  addUser() {
    this.errMsg = '';
    this.addUserSubmitted = true;
    const findedRole = this.filteredUserRolesPermissions.find(item => item.roleName === this.addUserForm.get('role').value);
    this.selectedRoleIdForAddUser = findedRole._id;
    const findedPlatform = this.userPlatforms.find(item => item.name === this.addUserForm.get('userPlatform').value);
    this.selectedPlatformIdForAddUser = findedPlatform._id;
    if (this.addUserForm.invalid) {
      return;
    }
    const emailToAdd = this.addUserForm.get('email').value.toLowerCase();
    const publisherUser = {
      publisherId: this.publisherProfile._id,
      email: emailToAdd,
      platformId: this.selectedPlatformIdForAddUser,
      userRoleId: this.selectedRoleIdForAddUser,
    };
    const userExist = this.publisherUsers.find(obj => obj.platformId === publisherUser.platformId && obj.email === emailToAdd);
    if (userExist) {
      this.errMsg = 'This user is already present in the selected platform';
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: this.errMsg
        }
      });
      return;
    }
    this.spinner.showSpinner.next(true);
    this.publisherUserService.addPublisherUser(publisherUser, findedPlatform.zone).subscribe(
      res => {
        this.loadPublisherUsers();
        if (res.success) {
          this.addUserSubmitted = false;
          this.addUserForm.get('role').setValue('');
          this.addUserForm.get('email').setValue('');
          this.addUserForm.get('userPlatform').setValue('');
        }
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }
  reGenerateTokenOfUser(email) {
    const postActiveUserData = {
      email: email
    };
    this.publisherUserService.reGenerateToken(postActiveUserData).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
        this.loadPublisherUsers();
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }
  deletePlatformFromPublisherUser(email, platformId) {
    this.userDeleteMsg = '';
    const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Are you sure you want to delete this user?'
      },
      disableClose: true
    });

    confirmDialogRef.afterClosed().subscribe(confirmResult => {
      if (confirmResult) {
        const removePlatformPostData = {
          email: email,
          platformId: platformId
        };
        this.publisherUserService.deletePlatformFromPublisherUser(removePlatformPostData).subscribe(
          data => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'success',
                message: data['message']
              }
            });
            this.loadPublisherUsers();
          },
          err => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
          }
        );
      }
    });
  }
  updatePlatformRoleOfPublisherUser(email, platformId, userRole): void {
    this.userDeleteMsg = '';
    const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Are you sure you want to update the role of this user?'
      },
      disableClose: true
    });

    confirmDialogRef.afterClosed().subscribe(confirmResult => {
      if (confirmResult) {
        const passwordConfirmDialogRef = this.dialog.open(ConfirmPasswordDailogComponent, {
          width: '50%',
          disableClose: true
        });
        passwordConfirmDialogRef.afterClosed().subscribe(passwordConfirmResult => {
          if (passwordConfirmResult) {
            const selectedUserRole = this.filteredUserRolesPermissions.find(obj =>
              obj.roleName.toLowerCase().trim() === userRole.toLowerCase().trim()
            );
            const platformData = {
              email: email,
              platformId: platformId,
              userRole: userRole,
            };
            this.publisherUserService.updatePlatformRoleOfPublisherUser(platformData).subscribe(
              data => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'success',
                    message: data['message']
                  }
                });
                this.loadPublisherUsers();
               },
               err => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
               }
             );
          } else {
            this.loadPublisherUsers();
          }
        });
      } else {
        this.loadPublisherUsers();
      }
    });
  }

  setCurrentRole(roleName: string) {
    this.selectedRole = roleName;
  }
}
