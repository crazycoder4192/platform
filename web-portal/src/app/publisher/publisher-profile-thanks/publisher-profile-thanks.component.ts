import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AmplitudeService } from 'src/app/_services/amplitude.service';

@Component({
  selector: 'app-publisher-profile-thanks',
  templateUrl: './publisher-profile-thanks.component.html',
  styleUrls: ['./publisher-profile-thanks.component.scss']
})
export class PublisherProfileThanksComponent implements OnInit {

  private platformId: string;
  private platformType: string;
  activeId = 1;
  skipId = 10;
  topBarElements = [1, 3, 7, 8];
  sideBarElements = [2, 4, 5, 6];

  constructor(private router: Router, private amplitudeService: AmplitudeService) {
    if (this.router.getCurrentNavigation().extras) {
      if (this.router.getCurrentNavigation().extras.state) {
        this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.addPlatformId);
        this.platformType = JSON.parse(this.router.getCurrentNavigation().extras.state.addPlatformType);
        console.log('this.platformType', this.platformType);
      }
    }
  }

  navigate() {
    this.router.navigate(['/publisher/platformCreation'],
    { 'state': {'addPlatformId':  JSON.stringify(this.platformId), 
    'addPlatformType':  JSON.stringify(this.platformType) }});
    let eventProperties = {
      type: JSON.parse(localStorage.getItem('currentUser'))['usertype']
    };
    this.amplitudeService.logEvent('profile_creation - user clicks on add your first platform', eventProperties);
  }

  ngOnInit() {
  }
  
  scrollToNext() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    console.log('currentEl: ', currentEl);
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    if (skipEl) {
      console.log('skipEl: ', skipEl);
      skipEl.style.display = 'none';
    }
    this.activeId++;
    this.finalizeNextStep();
  }

  scrollToSkip() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    if (!currentEl) {
      this.skip();
    }
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    skipEl.style.display = 'block';
    skipEl.style.position = 'fixed';
    skipEl.style.zIndex = '999';

    const skipButton = document.getElementById('skipButton');
    const skipButtonLocation = skipButton.getBoundingClientRect();
    skipEl.style.left = (skipButtonLocation.left) + 'px';
    skipEl.style.top = (skipButtonLocation.top) + 'px';
  }

  skip() {
    console.log('skipping');
    const overLay = document.getElementById('overlay_bg');
    overLay.style.display = 'none';
    console.log('overlay', overLay);
  }

  finalizeNextStep() {
    let m = document.getElementById('siteclick_' + this.activeId);
    if (this.activeId === 7) {
      m = document.getElementById('siteclick_' + 1);
    }

    if (!m) {
      this.skip();
    }
    const mainElement = m.getBoundingClientRect();
    let x = mainElement.right;
    let y = mainElement.bottom;

    const nextEl = document.getElementById('overlay_' + this.activeId);
    nextEl.style.display = 'block';
    nextEl.style.position = 'fixed';

    nextEl.style.zIndex = '999';

    if (this.topBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the topbar
      x = x - (mainElement.width);
      y = mainElement.bottom + 20;
      // y remains same 
    } else if (this.sideBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the sidebar
      x = mainElement.right;
      y = y - (mainElement.height / 2);
      // x remains same
    }
    nextEl.style.left = x + 'px';
    nextEl.style.top = y + 'px';
  }

}
