import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherProfileThanksComponent } from './publisher-profile-thanks.component';

describe('PublisherProfileThanksComponent', () => {
  let component: PublisherProfileThanksComponent;
  let fixture: ComponentFixture<PublisherProfileThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublisherProfileThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherProfileThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
