import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { USEROLESElement } from 'src/app/admin/configuration/dataModel/userRolesPermissionsModel';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { Publisher } from 'src/app/_models/publisher_model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { AssetService } from 'src/app/_services/publisher/asset.service';

@Component({
  selector: 'app-developer-share',
  templateUrl: './developer-share.component.html',
  styleUrls: ['./developer-share.component.scss']
})
export class DeveloperShareComponent implements OnInit  {
  public action: string;
  public platformId: string;
  public platformName: string;
  public platformType: string;
  public marketSelection: string;
  public assetId: string;
  public addUsers: any[];
  filteredUserRolesPermissions: any[];
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  private publisherProfile: Publisher;
  userPlatforms: any;
  publisherUsers: any;
  public addUserForm: FormGroup;
  errMsg: any;
  addUserSubmitted: boolean;
  selectedPlatformIdForAddUser: any;
  selectedRoleIdForAddUser: any;
  selectedUserEmail: any;
  public displayedUsersColumns: string[] = ['radioAction', 'email', 'userPlatform', 'role'];
  watchingPlatform: any;
  constructor(
    private dialog: MatDialog,
    private configHTTPService: ConfigurationService,
    private snackBar: MatSnackBar,
    private publisherUserService: PublisherUserService,
    private publisherPlatformService: PublisherPlatformService,
    private assetService: AssetService,
    private formBuilder: FormBuilder,
    private spinner: SpinnerService,
    @Inject(MAT_DIALOG_DATA) public entity: any
  ) { }

  ngOnInit() {
    this.action = this.entity.action;
    this.platformId = this.entity.platformId;
    this.assetId = this.entity.assetId;
    this.platformName = this.entity.platformName;
    this.platformType = this.entity.platformType;
    this.marketSelection = this.entity.marketSelection;
    this.addUsers = [];
    console.log('this.title', this.action);
    this.createAddUserForm();
    this.loadUserByEmail();
    this.adminRolesAndPermissions();
  }
  createAddUserForm() {
    this.addUserForm = this.formBuilder.group({
      role: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      userPlatform: ['', Validators.compose([Validators.required])]
    });
  }
  loadUserByEmail(): any {
    this.publisherUserService.getPublisherDetails(this.userEmail).subscribe(
      res => {
        this.publisherProfile = res;
        this.loadPlatforms();
        this.loadPublisherUsers();
      }
    );
  }
  loadPlatforms() {
    this.publisherPlatformService.getAllZonesByPublisherId().subscribe(
      res => {
        this.userPlatforms = res;
        if (this.platformId) {
          this.watchingPlatform = this.userPlatforms.find(item => item._id.toString() === this.platformId.toString());
          this.addUserForm.get('userPlatform').setValue(this.watchingPlatform.name);
        }
      }
    );
  }
  loadPublisherUsers() {
    this.publisherUserService.getPublisherUsers(this.publisherProfile._id).subscribe(
      res => {
        this.publisherUsers = res;
        this.publisherUsers = this.publisherUsers.filter(item => item.email !== this.userEmail && item.roleName !== 'Analyst');
        this.publisherUsers = this.publisherUsers.filter(item => item.platformId === this.platformId);
        this.publisherUsers.forEach(element => {
          element.selected = false;
        });
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }
  adminRolesAndPermissions() {
    this.configHTTPService.getalluserolesByModuleType('Publisher').subscribe(
      res => {
        if (res) {
          this.filteredUserRolesPermissions = <USEROLESElement[]>res;
          this.filteredUserRolesPermissions = this.filteredUserRolesPermissions.filter(item => item.roleName === 'Developer');
        }
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      }
    );
  }
  callOnChangeRole(roleId) {
    this.selectedRoleIdForAddUser = roleId;
  }
  radioChange (email) {
    this.selectedUserEmail = email;
  }
  addUser() {
    this.errMsg = '';
    this.addUserSubmitted = true;
    const findedRole = this.filteredUserRolesPermissions.find(item => item.roleName === this.addUserForm.get('role').value);
    this.selectedRoleIdForAddUser = findedRole._id;
    if (this.addUserForm.invalid) {
      return;
    }
    const emailToAdd = this.addUserForm.get('email').value.toLowerCase();
    const publisherUser = {
      publisherId: this.publisherProfile._id,
      email: emailToAdd,
      platformId: this.watchingPlatform._id,
      userRoleId: this.selectedRoleIdForAddUser,
      dontSendMail: true
    };
    this.addUsers.push(publisherUser);
    const userExist = this.publisherUsers.find(obj => obj.platformId === publisherUser.platformId && obj.email === emailToAdd);
    if (userExist) {
      this.errMsg = 'This user is already present in the selected platform';
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: this.errMsg
        }
      });
      return;
    }
    this.spinner.showSpinner.next(true);
    this.publisherUserService.addPublisherUser(publisherUser, this.marketSelection).subscribe(
      res => {
        this.loadPublisherUsers();
        if (res.success) {
          this.addUserSubmitted = false;
          this.addUserForm.get('role').setValue('');
          this.addUserForm.get('email').setValue('');
        }
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err['message']
          }
        });
      }
    );
  }

  private processSuccessMail(data) {
    this.snackBar.openFromComponent(CustomSnackbarComponent, {
      duration: 3000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass: ['snackbar-publisher'],
      data: {
        icon: 'success',
        message: data['message']
      }
    });
  }

  private processErrorMail(err) {
    this.snackBar.openFromComponent(CustomSnackbarComponent, {
      duration: 3000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass: ['snackbar-publisher'],
      data: {
        icon: 'error',
        message: err['message']
      }
    });
  }

  sendMail() {
    // the cases could be
    // 1. new user added, and same user has been sent code
    // 2. new user added, but that user is not selected but another existing one
    // 3. existing user is sent a code
    if (!this.selectedUserEmail) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: 'No developer user selected'
        }
      });
      return;
    }
    const selectedEmail = this.selectedUserEmail;
    const newUsersAndNotSelectedUser: any[] = this.addUsers.filter(x => x.email !== selectedEmail);
    console.log(JSON.stringify(newUsersAndNotSelectedUser));
    const newUserAndSelectedUser: any = this.addUsers.find(x => x.email === selectedEmail);
    let existingUser = false;
    if (this.addUsers.length === 0 || !this.addUsers.find(x => x.email === selectedEmail)) {
      existingUser = true;
    }
    if (existingUser) {
      if (this.action === 'headerCode') {
        const postData = {
          email: selectedEmail,
          platformName: this.platformName
        };
        this.publisherPlatformService.sendHeaderCode(postData, this.platformType).subscribe(
          data => {
            this.processSuccessMail(data);
           },
           err => {
            this.processErrorMail(err);
           }
         );
      } else if (this.action === 'invocationCode') {
        const postData = {
          email: selectedEmail,
          platformName: this.platformName
        };
        this.publisherPlatformService.sendInvocationCode(postData, this.marketSelection, this.platformType).subscribe(
          data => {
            this.processSuccessMail(data);
           },
           err => {
            this.processErrorMail(err);
           }
         );
      } else if (this.action === 'adIntegrationCode') {
        const postData = {
          email: selectedEmail,
          assetId: this.assetId
        };
        this.assetService.sendAdIntegrationCode(postData).subscribe(
          data => {
            this.processSuccessMail(data);
           },
           err => {
            this.processErrorMail(err);
           }
         );
      }
    }

    if (newUserAndSelectedUser) {
      if (this.action === 'headerCode') {
        const postData = {
          email: selectedEmail,
          platformName: this.platformName
        };
        this.publisherPlatformService.sendInvitationAndHeaderCode(postData, this.marketSelection, this.platformType).subscribe(
          data => {
            this.processSuccessMail(data);
            },
            err => {
            this.processErrorMail(err);
            }
          );
      } else if (this.action === 'invocationCode') {
        const postData = {
          email: selectedEmail,
          platformName: this.platformName
        };
        this.publisherPlatformService.sendInvitationAndInvocationCode(postData, this.platformType).subscribe(
          data => {
            this.processSuccessMail(data);
            },
            err => {
            this.processErrorMail(err);
            }
          );
      } else if (this.action === 'adIntegrationCode') {
        const postData = {
          email: selectedEmail,
          assetId: this.assetId
        };
        this.assetService.sendInvitationAndAdIntegrationCode(postData).subscribe(
          data => {
            this.processSuccessMail(data);
            },
            err => {
            this.processErrorMail(err);
            }
          );
      }
    }

    if (newUsersAndNotSelectedUser && newUsersAndNotSelectedUser.length > 0) {
      for (const n of newUsersAndNotSelectedUser) {
        this.publisherUserService.sendPublisherSubUserMails(n).subscribe(
          data => {
            this.processSuccessMail(data);
            },
            err => {
            this.processErrorMail(err);
            }
        );
      }
    }
  }

  sendVerificationMail() {
    if (this.addUsers && !this.addUsers.length) {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: 'No new developer user add'
        }
      });
      return;
    }
    for (const index in this.addUsers) {
      const postActiveUserData = {
        email: this.addUsers[index].email
      };
      this.publisherUserService.reGenerateToken(postActiveUserData).subscribe(
        data => {
          this.processSuccessMail(data);
        },
        err => {
          this.processErrorMail(err);
        }
      );
    }
  }

}
