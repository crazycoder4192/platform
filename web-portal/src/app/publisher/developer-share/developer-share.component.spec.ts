import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeveloperShareComponent } from './developer-share.component';

describe('DeveloperShareComponent', () => {
  let component: DeveloperShareComponent;
  let fixture: ComponentFixture<DeveloperShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeveloperShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
