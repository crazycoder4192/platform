import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { Router } from '@angular/router';
import { PublisherAsset } from 'src/app/_models/publisher_asset_model';
import { UtilService } from 'src/app/_services/utils/util.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { Banner } from 'src/app/_models/utils';
import { MatSnackBar, MatDialog } from '@angular/material';
import {Sort} from '@angular/material/sort';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';

import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-asset-manage',
  templateUrl: './asset-manage.component.html',
  styleUrls: ['./asset-manage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AssetManageComponent implements OnInit {
  public displayedColumns: string[] = ['asset', 'status', 'fromDate', 'toDate', 'percentageUtilized',
  'lifetimeImpressions', 'lifetimeClicks', 'avgCPC', 'avgCPM', 'actions'];
  public assetsData: PublisherAsset[];
  public assetsDataSource: PublisherAsset[];
  public grid = false;
  public showFilters = false;
  public statusList: string[];
  public typeList: string[];
  public asset: any;
  banners: Banner[];
  todaysDate = new Date();
  status: string;
  type: string;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['publisher_manage_assets_status','publisher_manage_assets_from_date', 'publisher_manage_assets_to_date',
  'publisher_manage_assets_percentage_utilized', 'publisher_manage_assets_lifetime_impressions',
  'publisher_manage_assets_lifetime_clicks', 'publisher_manage_assets_average_cpc',
  'publisher_manage_assets_average_cpm']);
  private currencySymbol : string;
  rolePermissions: any;
  constructor(
    private assetService: AssetService, private utilHTTPService: UtilService,
    private spinner: SpinnerService, private router: Router, public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private toolTipService: ToolTipService,
    private eventEmitterService: EventEmitterService,
    private utilService: UtilService,
    ) {
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();
    }

  ngOnInit() {
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.loadBanners();
    this.initAssets();
    this.fetchTooltips(this.toolTipKeyList);
    this.currencySymbol = this.utilService.getCurrencySymbol();
  }
  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }
  loadBanners() {
    this.utilHTTPService.getBanners().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.banners = <Banner[]>res;
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getBanners : ' + err);
      }
    );
  }
  initAssets() {
    this.assetService.getAssets().subscribe(
       res => {
        this.assetsData = <PublisherAsset[]>res;
        if (this.assetsData) {
          this.assetsData.forEach((assetItem: any) => {
            if (this.banners && this.banners.length) {
              assetItem.bannerDetails = this.banners.find((item) => item.size === assetItem.assetDimensions);
            }
          });
          this.assetsDataSource = this.assetsData;

          this.statusList = ['All', 'Active', 'Integration Pending', 'Verification Pending', 'Inactive'];
          this.typeList = ['Banner'];
        }
       },
       err => {
        console.log('Error! initAssets.getAssets : ' + err);
       }
     );
  }
  searchAsset(name) {
    if (!name) {
      this.assetsDataSource = this.assetsData;
    } else {
      this.assetsDataSource = this.assetsData.filter(option =>
        option.name.toLowerCase().indexOf( name.toLowerCase()) !== -1
      );
    }
    return this.assetsDataSource;
  }

  sortData(sort: Sort) {
    const data: any = this.assetsDataSource.slice();
    if (!sort.active || sort.direction === '') {
      this.assetsDataSource = data;
      return;
    }

    this.assetsDataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'assetDimensions': return compare(a.assetDimensions, b.assetDimensions, isAsc);
        case 'assetStatus': return compare(a.assetStatus, b.assetStatus, isAsc);
        case 'created.at': return compare(a.created.at, b.created.at, isAsc);
        case 'modified.at': return compare(a.modified.at, b.modified.at, isAsc);
        case 'utilization[0].utilized': return compare(a.utilization[0].utilized, b.utilization[0].utilized, isAsc);
        case 'transactionalData[0].impressions': return compare(a.transactionalData[0].impressions,
                                                        b.transactionalData[0].impressions, isAsc);
        case 'transactionalData[0].clicks': return compare(a.transactionalData[0].clicks,
                                                        b.transactionalData[0].clicks, isAsc);
        case 'transactionalData[0].CPC': return compare(a.transactionalData[0].CPC,
                                                        b.transactionalData[0].CPC, isAsc);
        case 'transactionalData[0].CPV': return compare(a.transactionalData[0].CPV,
                                                        b.transactionalData[0].CPV, isAsc);
        case 'transactionalData[0].CPM': return compare(a.transactionalData[0].CPM,
                                                        b.transactionalData[0].CPM, isAsc);
        default: return 0;
      }
    });
  }

  editAsset(assetType: string, assetId: string) {
    this.router.navigate(['/publisher/assetCreation'],
    { state: { 'assetType':  JSON.stringify(assetType), 'assetId':  JSON.stringify(assetId) } });
  }

  viewAssetAnalytics(assetId: string) {
    this.router.navigate(['/publisher/analytics'],
    { state: {'assetId':  JSON.stringify(assetId) } });
  }

  openDeleteCofirmDialog(_id: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Are you sure you want to delete this slot?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
      this.deleteAsset(_id);
     }
    });
  }
  deleteAsset(_id: string) {
    this.assetService.deleteAsset(_id).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: data['message']
          }
        });
        this.initAssets();
      //  this.router.navigate(['/publisher/assetManage']);
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }

  displayViewToggle() {
    this.grid = !this.grid;
  }

  displayFilters() {
    this.showFilters = !this.showFilters;
    this.status = '';
    this.type = '';
    this.asset = '';
    this.filterAsset('All', 'All' );
  }

  filterAsset(status: string, type: string ) {
    if (this.assetsData) {
      this.assetsDataSource = this.assetsData.filter(item => {
        if (status && status !== 'All' && !item.assetStatus.includes(status === 'Integration Pending' ? 'New' : status)) {
          return false;
        }
        if (type && type !== 'All' && !item.assetType.includes(type)) {
          return false;
        }
        return true;
      });
    }
  }


}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
