import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent, MatDialog, MatSnackBar } from '@angular/material';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { BankdetailsService } from 'src/app/_services/publisher/bankdetails.service';
import { DashboardService } from 'src/app/_services/publisher/dashboard.service';
import { PublisherAnalyticsService } from 'src/app/_services/publisher/publisher-analytics.service';
import { UserService } from 'src/app/_services/user.service';
import { PublisherDashboard } from '../../_models/publisher_change_dashboard_model';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { interval, Subscription, Subject } from 'rxjs';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Router } from '@angular/router';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { UtilService } from 'src/app/_services/utils/util.service';
import { SpinnerService } from 'src/app/_services/spinner.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  platformResults: PublisherDashboard;
  userDetailsTemplate;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  invoiceList: any[] = [];
  public filteredDuration = [];
  public saveFilterForm: FormGroup;
  startDate: Date;
  endDateMin: Date;
  performanceTrackingResults: any;
  endDate: Date;
  publisherId: string;
  public startDateMin = new Date(2019, 1, 1);
  public todayDate = new Date(new Date().setHours(23, 59, 59));
  private updateSubscription: Subscription;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set([
    'publisher_dashboard_earnings', 'publisher_dashboard_impressions',
    'publisher_dashboard_reach', 'publisher_dashboard_performance_tracking'
  ]);

  public toggleEarnings: any = 'Earnings';
  public toggleCPM: any = undefined;
  public toggleCTR: any = 'CTR';
  public toggleCPC: any = undefined;
  public activeToggleArray: string[] = ['Earnings', 'CTR'];
  public toggleStateMap: Map<string, string> = new Map<string, string>();
  private loadGraph;
  selectedIndex: any;
  activeId = 1;
  skipId = 10;
  topBarElements = [1, 3, 7, 8];
  sideBarElements = [2, 4, 5, 6];
  rolePermissions: any;
  marketSelection = '1';
  private currencySymbol : string;
  marketSelectionDisplay = false;
  constructor(
    private dashboardService: DashboardService,
    private bankDetailsService: BankdetailsService,
    private formBuilder: FormBuilder,
    private eventEmitterService: EventEmitterService,
    private userService: UserService,
    private toolTipService: ToolTipService,
    private publisherPlatformService: PublisherPlatformService,
    public router: Router,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private utilService: UtilService,
    public spinner: SpinnerService
  ) {
    localStorage.setItem('activeIcon', JSON.stringify(''));
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
    this.eventEmitterService.onClickNavigation();
    this.userService.getUserByEmail(this.userEmail).subscribe(
      res => {
        this.userDetails.first_name = res.firstName;
        this.userDetails.last_name = res.lastName;
        this.userDetails.isVisitedDashboard = res.isVisitedDashboard;
      }
    );
    this.userDetailsTemplate = this.userDetails;
    if (this.userDetails.hasPersonalProfile &&
      (localStorage.getItem('zone') === '1' || localStorage.getItem('zone') === '2')) {
      this.bankDetailsService.getBills().subscribe(
        result => {
          this.invoiceList = <[]>result;
        },
        err => {
          console.log('Error! getAllInvoices : ' + err);
        });
    }
    this.doActivity();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.selectedIndex = 0;
    this.saveFilterForm = this.formBuilder.group({
      startDate: [Validators.required],
      endDate: [Validators.required],
      duration: ['Custom Duration', Validators.required]
    });
    this.filteredDuration = [
      { value: 'Custom Duration', name: 'Custom Duration' },
      { value: 'Last 24 hours', name: 'Last 24 hours' },
      { value: 'Last 48 hours', name: 'Last 48 hours' },
      { value: 'Last 1 week', name: 'Last 1 week' },
      { value: 'Last 1 month', name: 'Last 1 month' },
      { value: 'Last 6 months', name: 'Last 6 months' }
    ];

    this.toggleStateMap.set('Earnings', 'Earnings');
    this.toggleStateMap.set('CPM', undefined);
    this.toggleStateMap.set('CTR', 'CTR');
    this.toggleStateMap.set('CPC', undefined);
    this.loadGraph = true;
    this.updateSubscription = interval(10000).subscribe(
      (val) => {
        this.doActivity();
      }
    );
    this.fetchTooltips(this.toolTipKeyList);
  }

  scrollToNext() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    console.log('currentEl: ', currentEl);
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    if (skipEl) {
      console.log('skipEl: ', skipEl);
      skipEl.style.display = 'none';
    }
    this.activeId++;
    this.finalizeNextStep();
  }

  scrollToSkip() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    if (!currentEl) {
      this.skip();
    }
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    skipEl.style.display = 'block';
    skipEl.style.position = 'fixed';
    skipEl.style.zIndex = '999';

    const skipButton = document.getElementById('skipButton');
    const skipButtonLocation = skipButton.getBoundingClientRect();
    skipEl.style.left = (skipButtonLocation.left) + 'px';
    skipEl.style.top = (skipButtonLocation.top) + 'px';
  }

  skip() {
    const overLay = document.getElementById('overlay_bg');
    overLay.style.display = 'none';
    if (this.userDetails.isVisitedDashboard === false) {
      this.userService.setDashboardVisitedStatusByEmail(this.userDetails.email, true).subscribe(
        result => {
          console.log('setDashboardVisitedStatusByEmail : SUCCESS');
        },
        error => {
          console.log('setDashboardVisitedStatusByEmail : ERROR');
        }
      );
    }
  }

  finalizeNextStep() {
    let m = document.getElementById('siteclick_' + this.activeId);
    if (this.activeId === 7) {
      m = document.getElementById('siteclick_' + 1);
    }

    if (!m) {
      this.skip();
    }
    const mainElement = m.getBoundingClientRect();
    let x = mainElement.right;
    let y = mainElement.bottom;

    const nextEl = document.getElementById('overlay_' + this.activeId);
    nextEl.style.display = 'block';
    nextEl.style.position = 'fixed';

    nextEl.style.zIndex = '999';

    if (this.topBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the topbar
      x = x - (mainElement.width);
      y = mainElement.bottom + 20;
      // y remains same 
    } else if (this.sideBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the sidebar
      x = mainElement.right;
      y = y - (mainElement.height / 2);
      // x remains same
    }
    nextEl.style.left = x + 'px';
    nextEl.style.top = y + 'px';
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
    this.loadGraph = null;
  }

  private doActivity() {
    if (this.userDetails.hasPersonalProfile) {
      this.getDashboardData();
    }
  }

  getDashboardData () {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.dashboardService.getPlatformDetailsForDashboard().subscribe(
      res => {
        this.platformResults = <PublisherDashboard>res;
        if (this.platformResults.platforms && this.platformResults.platforms.length > 0) {
          this.platformResults.platforms[this.selectedIndex].openState = true;
        }
        this.publisherId = this.platformResults.publisherId;
        this.publisherPlatformService.getAllZonesByPublisherId().subscribe(/*getting the platform id*/
          result => {
            if (result && result.length > 0) {
              // Display Market Selection dropdown if we have more than one zone
              this.marketSelectionDisplay = (([...new Set(result.map((x: any) => x.zone))].length) > 1);
            }
            if (!this.checkIfValidDate(this.saveFilterForm.get('startDate').value)) {
              let startDate = new Date();
              if (result && result.length > 0) {
                startDate = this.orderDates(result);
              }
              this.saveFilterForm.get('startDate').setValue(startDate);
            }
            if (!this.checkIfValidDate(this.saveFilterForm.get('endDate').value)) {
              this.saveFilterForm.get('endDate').setValue(this.todayDate);
            }
            this.loadPerformanceTracking();
          });
      },
      err => {
        console.log('Error DashboardComponent : ' + err);
      }
    );
  }

  private checkNoOfMarketPlaces(platforms: any) {
    return [...new Set(platforms.map((x: any) => x.zone))];
  }

  private checkIfValidDate(d) {
    return d instanceof Date;
  }

  private orderDates(platforms: any[]) {
    const orderedPlatforms = platforms.sort((a, b) => {
      return (Date.parse(a.created.at) > Date.parse(b.created.at)) ? 0 : 1;
    });
    return orderedPlatforms[0].created.at;
  }

  openSelectedPlatform(selectedPlatform: any, index: any) {
    this.selectedIndex = index;
    this.platformResults.platforms.forEach(item => {
      item.openState = false;
      if (item.platformId === selectedPlatform.platformId) {
        item.openState = true;
      }
    });
  }

  getMetricsValue(metricsObj: any[], type: string) {
    if (metricsObj && metricsObj !== undefined) {
      console.log('Metrics Object : ' + metricsObj.find(x => x.event === type));
      return (metricsObj.find(x => x.event === type));
    }
    return '-';
  }

  loadPerformanceTracking() {
    let startDate = new Date(this.saveFilterForm.get('startDate').value);
    let endDate = new Date(this.saveFilterForm.get('endDate').value);

    if (this.saveFilterForm.get('duration').value === 'Custom Duration') {
      startDate = new Date(new Date(this.saveFilterForm.get('startDate').value).setHours(0o0, 0o0, 0o0));
      endDate = new Date(new Date(this.saveFilterForm.get('endDate').value).setHours(23, 59, 59));
    }
    const sDate = startDate.toString();
    const eDate = endDate.toString();
    const performanancePostData = {
      startDate: sDate,
      endDate: eDate,
      publisherId: this.publisherId,
      timezone: new Date().getTimezoneOffset(),
      marketSelection: this.marketSelection
    };
    this.dashboardService.getPerformananceAnalyticsData(performanancePostData).subscribe(
      response => {
        this.spinner.showSpinner.next(false);
        this.performanceTrackingResults = response;
        if (this.loadGraph && this.performanceTrackingResults.data.length) {
          this.drawConversionResultLineChart(this.performanceTrackingResults);
        } else {
          this.resetGraph();
        }
      },
      (err: Error) => {
        this.spinner.showSpinner.next(false);
        console.log(err);
    });
  }

  drawConversionResultLineChart(performanceTrackingResults: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    const toggleValues = Array.from( this.toggleStateMap.values() );
    let dataRows = [];

    toggleValues.forEach(toggleElement => {
      if (this.toggleStateMap.get(toggleElement) !== undefined) {
        data.addColumn('number', this.toggleStateMap.get(toggleElement));
      }
    });
    const chartData: any[] = performanceTrackingResults.data;
    for (const c of chartData) {
      dataRows = [];
      dataRows.push(c.durationInstance);
      toggleValues.forEach(toggleElement => {
        if ( this.toggleStateMap.get(toggleElement) !== undefined ) {
          if (this.toggleStateMap.get(toggleElement) === 'Earnings') {
            dataRows.push(c.totalEarnings);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPM') {
            dataRows.push(c.avgCPM);
          } else if (this.toggleStateMap.get(toggleElement) === 'CTR') {
            dataRows.push(c.ctr);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPC') {
            dataRows.push(c.avgCPC);
          }
        }
      });
      data.addRow(dataRows);
    }

    const r = this.getSeriesAndVAxes();
    const options: any = {
      colors: ['#eb7871', '#81d4f2', '#8292ca', '#3dc3d6'],
      hAxis: { titleTextStyle: { color: '#333' } },
      vAxis: { viewWindowMode: 'explicit', viewWindow: { min: 0} },
      chartArea: { 'width': '90%', 'height': '80%' },
      vAxes: r.vAxes,
      series: r.series,
      width: 973,
      height: 350,
      legend: { position: 'none'}
    };

    const chart = new google.visualization.LineChart(document.getElementById('performanceTrackingLineChart'));
    chart.draw(data, options);
  }
  onSelectDuration(duration) {
    let date = new Date();
    this.saveFilterForm.get('endDate').setValue(date);
    if (duration === 'Custom Duration') {
      this.saveFilterForm.get('startDate').setValue(date);
      this.saveFilterForm.get('startDate').enable();
      this.saveFilterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      date = new Date(new Date().setHours(new Date().getHours() - 24));
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 48 hours') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setHours(new Date().getHours() - 48));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 week') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setDate(new Date().getDate() - 7));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 month') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 1));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 6 months') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 6));
      this.saveFilterForm.get('startDate').setValue(date);
    }
    this.startDate = this.saveFilterForm.get('startDate').value;
    this.endDate = this.saveFilterForm.get('endDate').value;
    this.loadPerformanceTracking();
  }
  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    this.endDateMin = this.startDate;

    this.loadPerformanceTracking();
  }
  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDate = new Date(this.saveFilterForm.get('endDate').value);
    this.endDateMin = this.endDate;

    this.loadPerformanceTracking();
  }

  public twoToggle(toggleValue: any) {
    if (this.toggleStateMap.get(toggleValue) === undefined) {
      this.toggleStateMap.set(toggleValue, toggleValue);
      this.activeToggleArray.push(toggleValue);
      if (this.activeToggleArray.length > 2) {
        this['toggle' + this.activeToggleArray[0]] = undefined;
        this.toggleStateMap.set(this.activeToggleArray[0], undefined);
        this.activeToggleArray.shift();
      }
    } else if (this.toggleStateMap.get(toggleValue) === toggleValue) {
      this.toggleStateMap.set(toggleValue, undefined);
      this['toggle' + toggleValue] = undefined;
      this.activeToggleArray = this.activeToggleArray.filter(item => item !== toggleValue);
    }
    if (this.performanceTrackingResults.data.length) {
      this.drawConversionResultLineChart(this.performanceTrackingResults);
    }
  }

  private getSeriesAndVAxes(): any {

    const earningsColor = '#5287c6';
    const cpmColor = '#78c068';
    const cpcColor = '#f37a6e';
    const ctrColor = '#9a67ab';

    let series: any = {};
    let vAxes: any = {};

    let toggleValues = Array.from(this.toggleStateMap.values());
    toggleValues = toggleValues.filter(( element ) => {
      return element !== undefined;
    });

    if (toggleValues && toggleValues.length === 1) {
      if (toggleValues.includes('Earnings')) {
        series = {
          0: { color: earningsColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CPM')) {
        series = {
          0: { color: cpmColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CPC')) {
        series = {
          0: { color: cpcColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CTR')) {
        series = {
          0: { color: ctrColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes("#'%'", null);
      }
    }

    if (toggleValues && toggleValues.length === 2) {
      if (toggleValues.includes('Earnings') && toggleValues.includes('CPM')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CPM')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('Earnings') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', "#'%'");
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes("#'%'", this.currencySymbol+'#');
        }
      }

      if (toggleValues.includes('Earnings') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', "#'%'");
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes("#'%'", this.currencySymbol+'#');
        }
      }

      if (toggleValues.includes('CPC') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPC') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', "#'%'");
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes("#'%'", this.currencySymbol+'#');
        }
      }
    }
    return {series, vAxes};
  }

  populateVAxes(yAxis1Format: string, yAxis2Format: string): any {
    let a: any = {};

    if (yAxis2Format) {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: this.currencySymbol+'#'
        },
        1: {
          minValue: 0,
          max: 100,
          format: "#'%'"
        }
      };
      a['0']['format'] = yAxis1Format;
      a['1']['format'] = yAxis2Format;
    } else {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: this.currencySymbol+'#'
        }
      };
      a['0']['format'] = yAxis1Format;
    }
    return a;
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }
  goToManageAssets(platformId) {
    this.publisherPlatformService.getPlatformById(platformId).subscribe ( res => {
      if (res['isWatching']) {
        this.router.navigate(['/publisher/assetManage']);
      } else {
        const confirmDialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
          width: '50%',
          data: {
            'title' : 'Do you want to switch "' + res['name'] + '" platform'
          }
        });
        confirmDialogRef.afterClosed().subscribe(confirmResult => {
          if (confirmResult) {
            this.publisherPlatformService.switchToPlatform(platformId).subscribe(
              result => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'success',
                    message: result['message']
                  }
                });
                this.router.navigate(['/publisher/assetManage']);
              },
              err => {
                this.snackBar.openFromComponent(CustomSnackbarComponent, {
                  duration: 3000,
                  verticalPosition: 'top',
                  horizontalPosition: 'right',
                  panelClass: ['snackbar-publisher'],
                  data: {
                    icon: 'error',
                    message: err.error['message']
                  }
                });
              });
            }
          });
        }
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
      });
  }
  updateMarketSelection() {
    this.spinner.showSpinner.next(true);
    this.setZone();
    this.doActivity();
  }
  resetGraph() {
    const node = document.getElementById('performanceTrackingLineChart');
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
      const a = document.createElement('span');
        a.innerHTML =
        `<img src="../assets/images/graph/pub-conversion-result.jpg"
        alt="loading" style="width:1100px; height:352px">
        <p style="position: relative;  top:-211px;width: 400px; margin: auto !important;text-align: center;">
        You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      document.getElementById('performanceTrackingLineChart').appendChild(a);
    }
  }
}
