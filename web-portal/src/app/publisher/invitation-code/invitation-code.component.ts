import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { InvitationCodeService } from 'src/app/_services/invitation-code.service';
import { MatSnackBar } from '@angular/material';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-invitation-code',
  templateUrl: './invitation-code.component.html',
  styleUrls: ['./invitation-code.component.scss']
})
export class InvitationCodeComponent implements OnInit {
  isEmail1: boolean;
  isEmail2: boolean;
  isEmail3: boolean;
  isEmail4: boolean;
  isEmail5: boolean;
  email1: any;
  email: string;
  email2: any;
  email3: any;
  email4: any;
  email5: any;
  sent1: boolean;
  sent5: boolean;
  sent4: boolean;
  sent3: boolean;
  sent2: boolean;
  invitations: Object[];

  constructor(
    private eventEmitterService: EventEmitterService,
    private invitationsService: InvitationCodeService,
    private snackBar: MatSnackBar
  ) {
    localStorage.setItem('activeIcon', JSON.stringify('profile'));
    this.eventEmitterService.onClickNavigation();
  }

  ngOnInit() {
    this.invitationsService.getInvitationCodeDetails().subscribe(
      res => {
        this.invitations = res['results'];
        this.invitations.forEach((element , i) => {
          if (i === 0) {
            this.email1 = element['invitedUser'];
            this.sent1 = element['invited'];
          } else if (i === 1) {
            this.email2 = element['invitedUser'];
            this.sent2 = element['invited'];
          } else if (i === 2) {
            this.email3 = element['invitedUser'];
            this.sent3 = element['invited'];
          } else if (i === 3) {
            this.email4 = element['invitedUser'];
            this.sent4 = element['invited'];
          } else if (i === 4) {
            this.email5 = element['invitedUser'];
            this.sent5 = element['invited'];
          }
        });
      },
      err => {
        console.log(err);
      }
    );
    this.isEmail1 = false;
    this.isEmail2 = false;
    this.isEmail3 = false;
    this.isEmail4 = false;
    this.isEmail5 = false;
    this.sent1 = false;
    this.sent2 = false;
    this.sent3 = false;
    this.sent4 = false;
    this.sent5 = false;
  }
  sendInvitation(emailNo) {
      this.isEmail1 = false;
      this.isEmail2 = false;
      this.isEmail3 = false;
      this.isEmail4 = false;
      this.isEmail5 = false;
      this.email = null;
    if (emailNo === 'email1' && !this.email1) {
      this.isEmail1 = true;
      this.isEmail2 = false;
      this.isEmail3 = false;
      this.isEmail4 = false;
      this.isEmail5 = false;
      return;
    } else if (emailNo === 'email1' && this.email1) {
      this.email = this.email1;
    } else if (emailNo === 'email2' && !this.email2) {
      this.isEmail1 = false;
      this.isEmail2 = true;
      this.isEmail3 = false;
      this.isEmail4 = false;
      this.isEmail5 = false;
      return;
    } else if (emailNo === 'email2' && this.email2) {
      this.email = this.email2;
    } else if (emailNo === 'email3' && !this.email3) {
      this.isEmail1 = false;
      this.isEmail2 = false;
      this.isEmail3 = true;
      this.isEmail4 = false;
      this.isEmail5 = false;
      return;
    } else if (emailNo === 'email3' && this.email3) {
      this.email = this.email3;
    } else if (emailNo === 'email4' && !this.email4) {
      this.isEmail1 = false;
      this.isEmail2 = false;
      this.isEmail3 = false;
      this.isEmail4 = true;
      this.isEmail5 = false;
      return;
    } else if (emailNo === 'email4' && this.email4) {
      this.email = this.email4;
    } else if (emailNo === 'email5' && !this.email5) {
      this.isEmail1 = false;
      this.isEmail2 = false;
      this.isEmail3 = false;
      this.isEmail4 = false;
      this.isEmail5 = true;
      return;
    } else if (emailNo === 'email5' && this.email5) {
      this.email = this.email5;
    }
    if (this.email && this.isValid(this.email)) {
      const postInvitation = {
        email: this.email.toLowerCase()
      };
      this.invitationsService.sendInvitation(postInvitation).subscribe(
        res => {

          if (res['success']) {
            if (emailNo === 'email1' && this.email1) {
              this.sent1 = true;
            } else if (emailNo === 'email2' && this.email2) {
              this.sent2 = true;
            } else if (emailNo === 'email3' && this.email3) {
              this.sent3 = true;
            } else if (emailNo === 'email4' && this.email4) {
              this.sent4 = true;
            } else if (emailNo === 'email5' && this.email5) {
              this.sent5 = true;
            }
          }

          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'success',
              message: res['message']
            }
          });
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
        }
      );
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'error',
          message: 'Email is not valid'
        }
      });
    }
  }
  isValid(email) {
    const regexp = new RegExp(
      // tslint:disable-next-line:max-line-length
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    return regexp.test(email);
  }

}
