import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PublisherAnalyticsService } from '../../../app/_services/publisher/publisher-analytics.service';
import { PublisherPlatformService } from '../../_services/publisher/publisher-platform.service';
import { PublisherProfileService } from '../../_services/publisher/publisher-profile.service';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { AssetService } from '../../../app/_services/publisher/asset.service';
import { UtilService } from 'src/app/_services/utils/util.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import canvg from 'canvg';
import { Router } from '@angular/router';
import { DropDownWithTreeComponent } from 'src/app/common/drop-down-with-tree/drop-down-with-tree.component';
import { TreeviewItem } from 'ngx-treeview';
import { ConfirmdialogComponent } from 'src/app/common/confirmdialog/confirmdialog.component';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Subject, interval, Subscription } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';
import config from 'appconfig.json';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit, OnDestroy {

  private platformId: string;
  private assetId: string;
  private currentlyViewingPlatformId: string;
  public isHcpFilterSelected = false;

  private viewingAsset: boolean;
  public assetList;
  public saveFilterForm: FormGroup;
  public viewingPlatform: string;
  public assetName: string;
  public assetDimensions: string;
  public assetSize: string;
  public assetFormats: string;
  public savedFilters;
  public showSiteAnalytics = true;
  public selectedAsset: string;
  public viewingSiteName: string;
  public viewingSiteType: string;
  public viewingSiteCreateDate: Date;
  public activeAssets = ' Active slots';
  public endDateMin: Date;
  public showfilters = false;
  public todayDate: Date = new Date(new Date().setHours(23, 59, 59));
  public startDate: Date = new Date(2019, 1, 1);
  public startDateMin: Date = new Date(2019, 1, 1);
  public startDateMax: Date = this.todayDate;
  public endDateMax: Date = this.todayDate;
  public locationData = [];
  public platformResults;

  public filteredReach;
  public analyticsGraphsData: any;

  public engagementImpressions = 0;
  public engagementReach = 0;
  public engagementClicks = 0;
  public engagementViews = 0;

  public resultEarnings = 0;
  public resultCPM = 0;
  public resultCTR = 0;
  public resultCPC = 0;
  public resultCPV = 0;

  public toggleEarnings: any = 'Earnings';
  public toggleCPM: any = undefined;
  public toggleCTR: any = 'CTR';
  public toggleCPC: any = undefined;
  public activeToggleArray: string[] = ['Earnings', 'CTR'];
  public toggleStateMap: Map<string, string> = new Map<string, string>();
  private conversionResultData: any;

  public navigatedAssetId;
  public loadGraphs;
  private updateSubscription: Subscription;
  @ViewChild('ddTreeLocation') ddTreeLocation: DropDownWithTreeComponent;
  public locationList: any[];
  public locationTreeList: any;
  public allLocationTreeItems: TreeviewItem[];
  private selectedLocation: any[] = [];
  private persistSelectedLocation = [];

  @ViewChild('ddTreeSpecialization') ddTreeSpecialization: DropDownWithTreeComponent;
  public specializationList: any[];
  public specializationTreeList: any;
  public allSpecializationTreeItems: TreeviewItem[];
  private selectedSpecialization: any[] = [];
  private persistSelectedSpecialization = [];

  @ViewChild('ddTreeGender') ddTreeGender: DropDownWithTreeComponent;
  public genderList: any[];
  public genderTreeList: any;
  public allGenderTreeItems: TreeviewItem[];
  private selectedGender: any[] = [];
  private persistSelectedGender = [];

  public filteredDuration = [
    { value: 'Custom Duration', name: 'Custom Duration' },
    { value: 'Last 24 hours', name: 'Last 24 hours' },
    { value: 'Last 48 hours', name: 'Last 48 hours' },
    { value: 'Last 1 week', name: 'Last 1 week' },
    { value: 'Last 1 month', name: 'Last 1 month' },
    { value: 'Last 6 months', name: 'Last 6 months' }
  ];

  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private imagesForPdfs: any[] = [];
  isApply: any;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set([
  'publisher_analytics_earnings', 'publisher_analytics_impressions',
  'publisher_analytics_reach', 'publisher_analytics_clicks',
  'publisher_analytics_ctr', 'publisher_analytics_cpc',
  'publisher_analytics_audience_specializations', 'publisher_analytics_audience_location',
  'publisher_analytics_audience_hcp_archetypes', 'publisher_analytics_behavior_time_of_the_day',
  'publisher_analytics_behavior_devices', 'publisher_analytics_conversion_engagement',
  'publisher_analytics_conversion_results', 'publisher_analytics_overview_top_ad_assets',
  'publisher_analytics_overview_asset_types_utilization', 'publisher_analytics_overview_site_frequency'    
  ]);
  activeTab: any;
  zone = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
  analyticsGraphsDataTemp: any;
  startAnalyticsDate: any;
  endAnalyticsDate: any;
  enableConversionTab: boolean;
  enableBehaviorTab: boolean;
  enableAudienceTab: boolean;
  enableOverviewTab: boolean;
  hideExtraGraph: boolean;
  locationListForPdf: any;
  specializationListForPdf: any;
  private currencySymbol : string;

  constructor(private formBuilder: FormBuilder,
    private publisherAnalyticsService: PublisherAnalyticsService,
    private assetService: AssetService,
    private publisherPlatformService: PublisherPlatformService,
    private publisherProfileService: PublisherProfileService,
    private utilService: UtilService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private toolTipService: ToolTipService,
    private eventEmitterService: EventEmitterService,
    private spinner: SpinnerService,
    private publisherUserService: PublisherUserService
  ) {
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();

      if (this.router.getCurrentNavigation().extras.state) {
        if (this.router.getCurrentNavigation().extras.state.assetId) {
          this.navigatedAssetId = JSON.parse(this.router.getCurrentNavigation().extras.state.assetId);
        }
      }
  }

  public displayFilters() {
    this.showfilters = !this.showfilters;
  }

  get f() { return this.saveFilterForm.controls; }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.enableOverviewTab = false;
    this.enableConversionTab = false;
    this.enableBehaviorTab = false;
    this.enableAudienceTab = false;
    this.analyticsGraphsData = {
      overviewTopAdAssets: [],
      overviewAssetTypeAndUtilization: [],
      overviewValidatedHcps: null,
      audienceGenderExperience: null,
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceAssetType: [],
      behaviorTimeOfDay: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResults: null
    };
    this.analyticsGraphsDataTemp = {
      overviewTopAdAssets: [],
      overviewAssetTypeAndUtilization: [],
      overviewValidatedHcps: null,
      audienceGenderExperience: null,
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceAssetType: [],
      behaviorTimeOfDay: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResults: null
    };
    this.activeTab = 'overviewTab';
    this.hideExtraGraph = true;
    this.saveFilterForm = this.formBuilder.group({
      _id: [''],
      startDate: [new Date(), Validators.required],
      endDate: [new Date(), Validators.required],
      duration: ['Custom Duration', Validators.required],
      reach: [''],
      specialization: ['', Validators.required],
      filter: [''],
      gender: ['', Validators.required],
      location: ['', Validators.required],
      platformId: this.platformId,
      filterName: [''],
      assetId: this.selectedAsset,
      email: this.userDetails.email,
      isHcpFilterSelected: false,
      timezone: '',
      tabType: ''
    });

    this.toggleStateMap.set('Earnings', 'Earnings');
    this.toggleStateMap.set('CPM', undefined);
    this.toggleStateMap.set('CTR', 'CTR');
    this.toggleStateMap.set('CPC', undefined);
    this.loadGraphs = true;

    this.utilService.getFiltersByEmail(this.userDetails.email).subscribe(
      response => {
        this.savedFilters = response;
      },
      err => {

      });

    this.selectedAsset = null;
    this.publisherUserService.getPublisherDetails(this.userDetails.email).subscribe(/*getting publisher id*/
      res => {
        this.publisherPlatformService.getViewingPlatform(res._id).subscribe(/*getting the platform id*/
          result => {
            this.platformId = result._id;
            this.currentlyViewingPlatformId = result._id;
            this.viewingPlatform = result;
            this.viewingSiteType = result.typeOfSite;
            this.viewingSiteName = result.name;
            this.viewingSiteCreateDate = result.created.at;
            this.assetService.getAssetsByPlatformId(result._id).subscribe(
              assets => {
                this.assetList = assets;
                this.isApply = false;
              //  if (result.isHCPUploaded) {
               // }
                 if (this.navigatedAssetId) {
                  this.onSelectAsset(this.navigatedAssetId, 'asset');
                 } else {
                  this.populateValuesInFilters();
                  this.getAnalyticsData(this.activeTab);
                 }
              });
          });
      });
      this.fetchTooltips(this.toolTipKeyList);
  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  } 

  ngOnDestroy() {
    this.loadGraphs = null;
  }
  resetDropdownFilters() {
    this.isHcpFilterSelected = false;
    this.saveFilterForm.get('specialization').setValue('');
    this.saveFilterForm.get('gender').setValue('');
    this.saveFilterForm.get('location').setValue('');
    this.saveFilterForm.get('isHcpFilterSelected').setValue('');
  }

  onSelectFilter(id) {
    const filterData = this.savedFilters.find((x) => x._id === id);
    this.ddTreeLocation.onSelect(filterData.location);
    this.ddTreeLocation.updateItems(this.allLocationTreeItems, filterData.location);

    this.ddTreeSpecialization.onSelect(filterData.specialization);
    this.ddTreeSpecialization.updateItems(this.allSpecializationTreeItems, filterData.specialization);

    this.ddTreeGender.onSelect(filterData.gender);
    this.ddTreeGender.updateItems(this.allGenderTreeItems, filterData.gender);
  }
  saveFilter() {
    const filterName: string = this.saveFilterForm.get('filterName').value;
    this.utilService.checkFilterNameExists({'filterName': filterName}).subscribe(
      response => {
        if (response['filterExist']) {
          /*indicates filter name already exist*/
          const dialogConfig = new MatDialogConfig();
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            title: 'Overwrite Filter?',
            message: `A filter with the name already exists. Do you want to overwrite it?`,
            confirm: 'Yes',
            cancel: 'No'
          };
          dialogConfig.minWidth = 400;
          const dialogRef = this.dialog.open(ConfirmdialogComponent, dialogConfig);
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              this.invokeSaveFilter();
              dialogRef.close();
            } else {
              dialogRef.close();
            }
          });
        } else {
          this.invokeSaveFilter();
        }
      },
      error => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: error.error['message']
          }
        });
      }
    );
  }

  private invokeSaveFilter() {
    const filterData = {
      'filterName': this.saveFilterForm.get('filterName').value,
      'specialization': this.selectedSpecialization,
      'gender': this.selectedGender,
      'location': this.selectedLocation
    };
    this.utilService.saveFilter(filterData).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res['message']
          }
        });
      },
      err => {

      }
    );
  }

  private populateValuesInFilters() {
    /*this.resetDropdownFilters();*/

    let type = '';
    let id = '';
    if (this.platformId) {
      type = 'platform';
      id = this.platformId;
    } else if (this.assetId) {
      type = 'asset';
      id = this.assetId;
    } else {
      console.error('Invalid type: at least one type (platform or asset) expected');
    }

    this.utilService.getValidHCPLocationsByTypeAndId(type, id).subscribe(response => {
      /* TreeSearch ignore's the first node, that's why inserted 'dummy data' item in the case of location
      as the first item in our case is US and we want to show that*/
      this.locationList = [];
      this.locationList.push('dummy data');
      this.locationList.push(response);
      this.locationTreeList = new TreeviewItem(<any>response);
      this.allLocationTreeItems = [this.locationTreeList];
    });
    this.utilService.getValidHCPLocationListByTypeAndId(type, id).subscribe(response => {
      this.locationListForPdf = response;
    });
    this.utilService.getValidHCPSpecializationByTypeAndId(type, id).subscribe(response => {
      this.specializationList = [];
      this.specializationList.push(response);
      this.specializationTreeList = new TreeviewItem(<any>response);
      this.allSpecializationTreeItems = [this.specializationTreeList];
    });
    this.utilService.getValidHCPSpecializationListByTypeAndId(type, id).subscribe(response => {
      this.specializationListForPdf = response;
    });
    this.utilService.getValidHCPGenderByTypeAndId(type, id).subscribe(response => {
      this.genderList = [];
      this.genderList.push(response);
      this.genderTreeList = new TreeviewItem(<any>response);
      this.allGenderTreeItems = [this.genderTreeList];
    });
  }

  private unwindFilter() {

    this.persistSelectedLocation = JSON.parse(JSON.stringify(this.selectedLocation));
    this.saveFilterForm.get('location').setValue(this.selectedLocation);

    this.persistSelectedSpecialization = JSON.parse(JSON.stringify(this.selectedSpecialization));
    this.saveFilterForm.get('specialization').setValue(this.selectedSpecialization);

    this.persistSelectedGender = JSON.parse(JSON.stringify(this.selectedGender));
    this.saveFilterForm.get('gender').setValue(this.selectedGender);

    this.saveFilterForm.get('isHcpFilterSelected').setValue(true);
  }

  treeSearch(tree, value) {
    return tree.reduce(function f(acc, node) {
        return (node.value === value) ? node :
            (node.children && node.children.length) ? node.children.reduce(f, acc) : acc;
    });
  }

  onLocationSelectedChange(events) {
    this.selectedLocation = events;
  }

  onSpecializationSelectedChange(events) {
    this.selectedSpecialization = events;
  }

  onGenderSelectedChange(events) {
    this.selectedGender = events;
  }

  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    this.endDateMin = new Date(this.startDate);
    this.startDateMax = new Date(this.saveFilterForm.get('endDate').value);
  }

  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDateMin = this.startDate;
    this.endDateMin = this.startDate;
    this.startDateMax = new Date(this.saveFilterForm.get('endDate').value);
  }

  onSelectDuration(duration) {
    const endDate = new Date();
    this.saveFilterForm.get('endDate').setValue(endDate);
    const startDate = new Date();
    if (duration === 'Custom Duration') {
      this.saveFilterForm.get('startDate').setValue(this.viewingSiteCreateDate);
      this.saveFilterForm.get('startDate').enable();
      this.saveFilterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      startDate.setHours(startDate.getHours() - 24);
      this.saveFilterForm.get('startDate').setValue(startDate);
    } else if (duration === 'Last 48 hours') {
      startDate.setHours(startDate.getHours() - 48);
      this.saveFilterForm.get('startDate').setValue(startDate);
    } else if (duration === 'Last 1 week') {
      startDate.setHours(startDate.getDay() - 7 * 24);
      this.saveFilterForm.get('startDate').setValue(startDate);
    } else if (duration === 'Last 1 month') {
      startDate.setMonth(startDate.getMonth() - 1);
      this.saveFilterForm.get('startDate').setValue(startDate);
    } else if (duration === 'Last 6 months') {
      startDate.setMonth(startDate.getMonth() - 6);
      this.saveFilterForm.get('startDate').setValue(startDate);
    }
  }

  onSelectAsset(id: string, assetType: string) {
    if (this.activeTab === 'overviewTab') {
      this.activeTab = 'audienceTab';
    }
    this.hideExtraGraph = true;
    this.enableOverviewTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.analyticsGraphsDataTemp = {
      overviewTopAdAssets: [],
      overviewAssetTypeAndUtilization: [],
      overviewValidatedHcps: null,
      audienceGenderExperience: null,
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceAssetType: [],
      behaviorTimeOfDay: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResults: null
    };
    this.assetId = id;
    this.platformId = null;
    this.selectedAsset = id;
    const asset = this.assetList.find((x: any) => x._id === id);
    this.saveFilterForm.get('startDate').setValue(asset.created.at);
    this.saveFilterForm.get('endDate').setValue(new Date());
    this.assetName = asset.name;
    this.assetDimensions = asset.assetDimensions + ' | ';
    this.assetSize = asset.assetSize + ' ' + asset.assetUnit + ' | ';
    this.assetFormats = asset.fileFormats;
    this.populateValuesInFilters();
    this.getAnalyticsData(this.activeTab);
    this.showSiteAnalytics = false;
  }

  changeView() {
    this.activeTab = 'overviewTab';
    this.hideExtraGraph = true;
    this.enableOverviewTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.assetId = null;
    this.platformId = this.currentlyViewingPlatformId;
    this.populateValuesInFilters();
    this.getAnalyticsData(this.activeTab);
    this.showSiteAnalytics = true;
    this.selectedAsset = null;
  }

  drawOverviewTopAdAssetsScatterChart(overviewTopAdAssets: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('number', 'Reach');
    data.addColumn('number', 'Earning');

    for (const r of overviewTopAdAssets) {
      if (r.assetType === 'Banner') {
        /*TODO: unable to get custom tooltip
       // let tooltip: string = `${r.assetName}, reach: ${r.reach}, earnings: ${r.totalEarnings}`*/
        data.addRow([r.reach, r.totalEarnings]);
      }
      if (r.assetType === 'Video') {
        data.addRow([r.reach, r.totalEarnings]);
      }
    }

    const options: any = {
      legend: 'none',
      colors: ['#5fbdea', '#ecb057','#a569d6'],
      chartArea: { 'width': '90%', 'height': '80%' },
      sizeAxis: { maxSize: 7, minSize: 7 },
      width: 973,
      height: 300
    };

    const chart = new google.visualization.ScatterChart(document.getElementById('overviewTopAdAssetsScatterChart'));
    const columns = [
      { title: 'Banner Name', dataKey: 'bannerName' },
      { title: 'Reach', dataKey: 'reach' },
      { title: 'Earning', dataKey: 'earning' }
    ];

    const rows: any[] = [];
    for (const r of overviewTopAdAssets) {
      if (r.assetType === 'Banner') {
        const row: any = { bannerName: r.assetName, reach: r.reach, earning: r.totalEarnings.toFixed(2) };
        rows.push(row);
      }
    }
    this.pushToPDF(chart, 'Top Slots', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawOverviewAssetTypeAndUtilizationDonutChart(overviewAssetTypeAndUtilization: any) {
    for (const o of overviewAssetTypeAndUtilization) {
      if (o.assetType === 'Banner') {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'name');
        data.addColumn('number', 'count');
        data.addRow(['Sucess', o.success]);
        data.addRow(['Failure', o.failed]);

        const options: any = {
          pieHole: 0.5,
          colors: ['#5fbdea', '#ecb057','#a569d6'],
          chartArea: { 'width': '90%', 'height': '80%' },
          /*pieSliceTextStyle: {
            color: 'black',
            fontSize: 23,
            bold: true
          }, */
          legend: {
            position: 'bottom'
          },
          width: 310,
          height: 300
        };

        const chart = new google.visualization.PieChart(document.getElementById('overviewBannerAssetTypeAndUtilizationDonutChart'));
        const columns = [
          { title: 'Slot Type', dataKey: 'assetType' },
          { title: 'Success', dataKey: 'successCount' },
          { title: 'Failure', dataKey: 'failureCount' }
        ];

        const rows: any[] = [];
        for (const r of overviewAssetTypeAndUtilization) {
          const row: any = { assetType: r.assetType, successCount: r.success, failureCount: r.failed };
          rows.push(row);
        }
        this.pushToPDF(chart, 'Slot Type & Utilization', {columns: columns, rows: rows});
        chart.draw(data, options);
      }
      /*
      if (o.assetType === 'Video') {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'name');
        data.addColumn('number', 'count');
        data.addRow(['sucess', o.success]);
        data.addRow(['failure', o.failure]);

        const options: any = {
          pieHole: 0.5,
          colors: ['#81e0f5', '#ab75f5', '#f37f89'],
          chartArea: { 'width': '95%', 'height': '80%' },
          pieSliceTextStyle: {
            color: 'black',
            fontSize: 23,
            bold: true
          },
          legend: {
            position: 'none'
          }
        };

        const chart = new google.visualization.PieChart(document.getElementById('overviewVideoAssetTypeAndUtilizationDonutChart'));
        //this.addToPdfChartQueue(chart, 'Video asset type and utilization', 200, 200);
        chart.draw(data, options);
      }*/
    }

  }

  drawOverviewValidatedHcpsDonutChart(overviewValidatedHcps: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'name');
    data.addColumn('number', 'count');
    data.addRow(['Validated', overviewValidatedHcps.validatedHcpCount]);
    data.addRow(['Unvalidated', overviewValidatedHcps.unvalidatedHcpCount]);

    const options: any = {
      pieHole: 0.5,
      colors: ['#5fbdea', '#ecb057','#a569d6'],
     /* pieSliceTextStyle: {
        color: 'black',
        fontSize: 23,
        bold: true
      }, */
      legend: {
        position: 'bottom'
      },
      chartArea: { 'width': '90%', 'height': '80%' },
      width: 310,
      height: 300
    };

    const chart = new google.visualization.PieChart(document.getElementById('overviewValidatedHcpsDonutChart'));
    const columns = [
      { title: 'Validated HCP', dataKey: 'validatedHcpCount' },
      { title: 'Non Validated HCP', dataKey: 'unvalidatedHcpCount' }
    ];

    const rows: any[] = [];
    const row: any = { validatedHcpCount: overviewValidatedHcps.validatedHcpCount,
                       unvalidatedHcpCount: overviewValidatedHcps.unvalidatedHcpCount };
    rows.push(row);

    this.pushToPDF(chart, 'Validated HCPs', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawAudienceGenderExperiencePieChart(audienceGenderExperience: any) {
    let totalMale = 0;
    let totalFemale = 0;
    let totalOther = 0;

    for (const o of audienceGenderExperience) {
      if (o.gender === 'M') {
        for (const l of o.locations) {
          totalMale = totalMale + l.count;
        }
      }
      if (o.gender === 'F') {
        for (const l of o.locations) {
          totalFemale = totalFemale + l.count;
        }
      }
      if (o.gender === 'O') {
        for (const l of o.locations) {
          totalOther = totalOther + l.count;
        }
      }
    }

    const maleLabel: number = totalMale * 100 / (totalMale + totalFemale + totalOther);
    const femaleLabel: number = totalFemale * 100 / (totalMale + totalFemale + totalOther);
    const otherLabel: number = totalOther * 100 / (totalMale + totalFemale + totalOther);

    let high = 0;
    if (maleLabel > high) {
      high = maleLabel;
    }
    if (femaleLabel > high) {
      high = femaleLabel;
    }
    if (otherLabel > high) {
      high = otherLabel;
    }
    const width = 85;
    const height = 80;
    for (const o of audienceGenderExperience) {
      if (o.gender === 'M') {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'name');
        data.addColumn('number', 'count');
        for (const l of o.locations) {
          data.addRow([l.location, l.count]);
        }
        const w: number = width * maleLabel / high;
        const h: number = height * maleLabel / high;

        const options: any = {
          colors: ['#5fbdea', '#ecb057','#a569d6'],
          chartArea: { 'width': w + '%', 'height': h + '%' },
          legend: {
            position: 'none'
          },
          height: 300,
          width: 300
        };

        const chart = new google.visualization.PieChart(document.getElementById('audienceMaleExperiencePieChart'));
        /*this.addToPdfChartQueue(chart, 'Male experience', 200, 200);*/
        chart.draw(data, options);
      }
      if (o.gender === 'F') {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'name');
        data.addColumn('number', 'count');
        for (const l of o.locations) {
          data.addRow([l.location, l.count]);
        }
        const w: number = width * femaleLabel / high;
        const h: number = height * femaleLabel / high;

        const options: any = {
          colors: ['#5fbdea', '#ecb057','#a569d6'],
          chartArea: { 'width': w + '%', 'height': h + '%' },
          legend: {
            alignment: 'center',
            position: 'bottom'
          },
          width: 250,
          height: 200
        };

        const chart = new google.visualization.PieChart(document.getElementById('audienceFemaleExperiencePieChart'));
       /* this.addToPdfChartQueue(chart, 'Female experience', 200, 200);*/
        chart.draw(data, options);
      }

      if (o.gender === 'O') {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'name');
        data.addColumn('number', 'count');
        for (const l of o.locations) {
          data.addRow([l.location, l.count]);
        }
        const w: number = width * otherLabel / high;
        const h: number = height * otherLabel / high;

        const options: any = {
          colors: ['#5fbdea', '#ecb057','#a569d6'],
          chartArea: { 'width': w + '%', 'height': h + '%' },
          legend: {
            alignment: 'center',
            position: 'bottom'
          }
        };
        const chart = new google.visualization.PieChart(document.getElementById('audienceOtherExperiencePieChart'));
        /*this.addToPdfChartQueue(chart, 'Other experience', 200, 200);*/
        chart.draw(data, options);
      }
    }
  }

  drawAudienceSpecializationStackBarChart(audienceSpecialization: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Specialization');
    data.addColumn('number', 'Male');
    data.addColumn('number', 'Female');
    data.addColumn('number', 'Other');
    for (let i = 0; i < audienceSpecialization.length; i++) {
      data.addRow([audienceSpecialization[i].specialization, audienceSpecialization[i].male,
      audienceSpecialization[i].female, audienceSpecialization[i].other]);
    }

    const options: any = {
      legend: { position: 'bottom', alignment: 'end'},
      bar: { groupWidth: '40%' },
      isStacked: true,
      colors: ['#5fbdea', '#ecb057','#a569d6'],
      chartArea: { 'width': '90%', 'height': '70%' },
      vAxis: { viewWindow: {min: 0} },
      height: 300,
      width: 1000,
      hAxis: {
        slantedText: false
      }
    };

    const chart = new google.visualization.ColumnChart(document.getElementById('audienceSpecializationStackedBarChart'));
    const columns = [
      { title: 'Specialization', dataKey: 'specialization' },
      { title: 'Male', dataKey: 'male' },
      { title: 'Female', dataKey: 'female' },
      { title: 'Other', dataKey: 'other' },
    ];

    const rows: any[] = [];
    for (let i = 0; i < audienceSpecialization.length; i++) {
      const row: any = {specialization: audienceSpecialization[i].specialization, male: audienceSpecialization[i].male,
      female: audienceSpecialization[i].female, other: audienceSpecialization[i].other};
      rows.push(row);
    }
    this.pushToPDF(chart, 'Specializations', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawAudienceLocationMapChart(audienceLocation: any) {
    const lData = new google.visualization.DataTable();
    lData.addColumn('string', 'State');
    lData.addColumn('number', 'Popularity');

    for (const loc of audienceLocation) {
      const temp: any[] = [];
      temp.push(loc.state, loc.showValue);
      lData.addRows([temp]);
    }
    this.locationData = audienceLocation;
    const options: any = {
      colorAxis: { colors: ['#2cc2ec'] },
      displayMode: 'regions',
      resolution: 'provinces',
      legend: {
        position: 'none'
      }
    };
    options.region = this.zone === '1'? 'US' : this.zone ==='2' ? 'IN' : 'US'; 
    const chart = new google.visualization.GeoChart(document.getElementById('audienceLocationChart'));
    const columns = [
      { title: 'State', dataKey: 'state' },
      { title: 'Popularity', dataKey: 'popularity' }
    ];

    const rows: any[] = [];
    for (const loc of audienceLocation) {
      const row: any = {state: loc.state, popularity: loc.showValue};
      rows.push(row);
    }
    this.pushToPDF(chart, 'Location', {columns: columns, rows: rows});
    chart.draw(lData, options);
  }

  drawAudienceArcheTypeBarChart(audienceArcheType: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Archetype');
    data.addColumn('number', 'Count');

    data.addRows([
      ['Connectors', audienceArcheType.connectors],
      ['Curators', audienceArcheType.curators],
      ['Followers', audienceArcheType.followers],
      ['Pharmaiety', audienceArcheType.pharmaiety],
      ['Tool Boxers', audienceArcheType.toolBoxers]
    ]);
    const options: any = {
      vAxis: { viewWindow: {min: 0}},
      colors: ['#5fbdea'],
      chartArea: { 'width': '90%', 'height': '70%' },
      legend: {
        position: 'bottom', alignment: 'end'
      },
      bar: { groupWidth: '40%' },
      'width': 540,
      'height': 300
    };

    const chart = new google.visualization.ColumnChart(
    document.getElementById('audienceHCPArcheTypeBarChart'));
    const columns = [
      { title: 'Archetype', dataKey: 'archeType' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    rows.push({archeType: 'Connectors', count: audienceArcheType.connectors});
    rows.push({archeType: 'Curators', count: audienceArcheType.curators});
    rows.push({archeType: 'Followers', count: audienceArcheType.followers});
    rows.push({archeType: 'Pharmaiety', count: audienceArcheType.pharmaiety});
    rows.push({archeType: 'Tool Boxers', count: audienceArcheType.toolBoxers});
    this.pushToPDF(chart, 'HCP Archetype', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawAudienceAssetType(audienceAssetType: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'AssetType');
    data.addColumn('number', 'count');
    for (const o of audienceAssetType) {
      data.addRow([o.imageType, o.count]);
    }

    const options: any = {
      pieHole: 0.5,
      colors: ['#5fbdea', '#ecb057','#a569d6'],
      chartArea: { 'width': '85%', 'height': '80%'},
      legend: {position: 'bottom', alignment: 'center'},
      width: 230,
      height: 300
    };

    const chart = new google.visualization.PieChart(document.getElementById('audienceAssetTypeDonutChart'));
    const columns = [
      { title: 'Slot Type', dataKey: 'assetType' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    let JPEGCount = 0;
    let PNGCount = 0;
    let GIFCount = 0;
    for (const o of audienceAssetType) {
      if (o.imageType === 'JPEG') {
        JPEGCount = o.count;
      } else if (o.imageType === 'PNG') {
        PNGCount = o.count;
      } else if (o.imageType === 'GIF') {
        GIFCount = o.count;
      }
    const row: any = {assetType: o.imageType, count: o.count};
    rows.push(row);
    }
    /*let row: any = {assetType: 'JPEG', count: JPEGCount};
    rows.push(row);
    row = {assetType: 'PNG', count: PNGCount};
    rows.push(row);
    row = {assetType: 'GIF', count: GIFCount};
    rows.push(row); */
    this.pushToPDF(chart, 'Slot Type', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawBehaviorTimeOfDayHeatChart(behaviorTimeOfDay: any) {
    const yAxisValues: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    const xAxisValues: string[] = ['00-03', '03-06', '06-09', '09-12', '12-15', '15-18', '18-21', '21-24'];
    const margin = { top: 10, right: 20, bottom: 20, left: 40 },
          width = 540 - margin.left - margin.right,
          height = 300 - margin.top - margin.bottom;

    const el: HTMLElement = document.getElementById('behaviorheatmap');
    if (el !== undefined && el !== null) {
      el.innerHTML = '';
    }
    const svg = d3.selectAll('#behaviorheatmap')
                  .append('svg')
                  .attr('width', width + margin.left + margin.right)
                  .attr('height', height + margin.top + margin.bottom)
                  .append('g')
                  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    // Build X scales and axis:
    const x = d3.scaleBand()
      .range([0, width])
      .domain(xAxisValues)
      .padding(0.01);

    svg.append('g')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x));

    // Build Y scales and axis:
    const y = d3.scaleBand()
      .rangeRound([height, 0])
      .domain(yAxisValues)
      .padding(0.02);

    svg.append('g')
      .style('text-anchor', 'right')
      .call(d3.axisLeft(y));

    const myColor = d3.scaleLinear<string>()
      .range(['#5fbdea', '#ecb057'])
      .domain([0, 1000]);

    const tooltip = d3.select('#behaviorheatmap')
      .append('div')
      .style('opacity', 0)
      .attr('class', 'tooltip')
      .style('background-color', 'white')
      .style('border', 'solid')
      .style('border-width', '2px')
      .style('border-radius', '5px')
      .style('padding', '10px');

    svg.selectAll()
      .data(behaviorTimeOfDay, function (d: any) { return d; })
      .enter()
      .append('rect')
      .attr('x', function (d: any) { return x(d.hourRange); })
      .attr('y', function (d: any) { return y(d.weekDay); })
      .attr('width', x.bandwidth())
      .attr('height', y.bandwidth())
      .style('fill', function (d: any) { return myColor(d.count); })
      .on('mouseover', function (d) {
        tooltip.style('opacity', 1);
      })
      .on('mousemove', function (d) {
        tooltip
          .html('views: ' + d.count)
          .style('left', (d3.mouse(this)[0] + 70) + 'px')
          .style('top', (d3.mouse(this)[1]) + 'px');
      })
      .on('mouseleave', function (d) {
        tooltip.style('opacity', 0);
      });

      /*let canvas = document.createElement('img');*/
      const canvas = document.createElement('canvas');
      canvg(canvas, svg.node().outerHTML);
      const imgData = canvas.toDataURL('image/png');
      const content = document.createElement('img');
      content.src = imgData;
      content.setAttribute('name', 'Type of day');
      content.setAttribute('width', '400');
      content.setAttribute('height', '400');

      /*document.getElementById('graph-images').appendChild(content);*/

  }

  drawBehaviorDeviceDonutChart(behaviorDevices: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Devices');
    data.addColumn('number', 'count');
    data.addRow(['Desktop', behaviorDevices.desktop]);
    data.addRow(['Mobile', behaviorDevices.mobile]);
    data.addRow(['Tablet', behaviorDevices.tablet]);

    const options: any = {
      pieHole: 0.5,
      colors: ['#5fbdea', '#ecb057','#a569d6'],
      chartArea: { 'width': '90%', 'height': '75%' },
      legend: {
        position: 'bottom', alignment: 'center'
      },
      width: 370,
      height: 245
    };

    const chart = new google.visualization.PieChart(document.getElementById('behaviorDevicesDonutChart'));
    const columns = [
      { title: 'Device', dataKey: 'device' },
      { title: 'Count', dataKey: 'count' }
    ];

    const rows: any[] = [];
    rows.push({device: 'Desktop', count: behaviorDevices.desktop});
    rows.push({device: 'Mobile', count: behaviorDevices.mobile});
    rows.push({device: 'Tablet', count: behaviorDevices.tablet});
    this.pushToPDF(chart, 'Behavior', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawConversionEngagementAreaLineChart(conversionEnagement: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Impressions');
    data.addColumn('number', 'Reach');
    data.addColumn('number', 'Clicks');

    this.engagementImpressions = conversionEnagement.summary.impressions;
    this.engagementReach =  conversionEnagement.summary.reaches;
    this.engagementClicks =  conversionEnagement.summary.clicks;

    const chartData: any[] = conversionEnagement.data;
    for (const c of chartData) {
      data.addRow([c.durationInstance, c.impressions, c.reaches,  c.clicks]);
    }
    const options: any = {
      hAxis: { titleTextStyle: { color: '#333'} },
      vAxis: { viewWindow: { min: 0 } },
      chartArea: { 'width': '85%', 'height': '80%' },
      width: 973,
      height: 350,
      legend: {
          position: 'none',
      },
      series: {
        0: { color: '#ec736b', visibleInLegend: false },
        1: { color: '#92d8f0', visibleInLegend: false },
        2: { color: '#b4a0cd', visibleInLegend: false }
      }
    };

    const chart = new google.visualization.AreaChart(document.getElementById('conversionEngagementAreaChart'));
    const columns = [
      { title: 'Month', dataKey: 'month' },
      { title: 'Impression', dataKey: 'impression' },
      { title: 'Reach', dataKey: 'reach' },
      { title: 'Click', dataKey: 'click' }
    ];

    const rows: any[] = [];
    for (const c of chartData) {
      const row: any = {month: c.durationInstance, impression: c.impressions, reach: c.reaches,  click: c.clicks};
      rows.push(row);
    }
    this.pushToPDF(chart, 'Engagement', {columns: columns, rows: rows});
    chart.draw(data, options);
  }

  drawConversionResultLineChart(conversionResults: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    const toggleValues = Array.from( this.toggleStateMap.values() );
    let dataRows = [];

    toggleValues.forEach(toggleElement => {
      if (this.toggleStateMap.get(toggleElement) !== undefined) {
        data.addColumn('number', this.toggleStateMap.get(toggleElement));
      }
    });

    const pdfTableRows: any[] = [];

    this.resultEarnings = conversionResults.summary.earnings;
    this.resultCPM = conversionResults.summary.cpm;
    this.resultCTR = conversionResults.summary.ctr;
    this.resultCPC = conversionResults.summary.cpc;
    
    /*cpc, cpm, earnings values range can differ a lot, we need to normalize that*/
    const chartData: any[] = conversionResults.data;

    for (const c of chartData) {
      dataRows = [];
      dataRows.push(c.durationInstance);
      toggleValues.forEach(toggleElement => {
        if ( this.toggleStateMap.get(toggleElement) !== undefined ) {
          if (this.toggleStateMap.get(toggleElement) === 'Earnings') {
            dataRows.push(c.totalEarnings);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPM') {
            dataRows.push(c.avgCPM);
          } else if (this.toggleStateMap.get(toggleElement) === 'CTR') {
            dataRows.push(c.ctr);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPC') {
            dataRows.push(c.avgCPC);
          }
        }
      });
      data.addRow(dataRows);
      pdfTableRows.push({
        month: c.durationInstance,
        earnings: c.totalEarnings,
        cpm: c.avgCPM,
        ctr: c.ctr,
        cpc: c.avgCPC
      });
    }

    const r = this.getSeriesAndVAxes();
    const options: any = {
      colors: ['#5fbdea', '#ecb057', '#a569d6', '#3dc3d6'],
      hAxis: { titleTextStyle: { color: '#333' } },
      vAxis: { viewWindowMode: 'explicit', viewWindow: { min: 0} },
      chartArea: { 'width': '85%', 'height': '80%' },
      vAxes: r.vAxes,
      series: r.series,
      width: 973,
      height: 350,
      legend: { position: 'none'}
    };

    const chart = new google.visualization.LineChart(document.getElementById('conversionResultLineChart'));
    const columns = [
      { title: 'Month', dataKey: 'month' },
      { title: 'Earnings', dataKey: 'earnings' },
      { title: 'CPM', dataKey: 'cpm' },
      { title: 'CTR', dataKey: 'ctr' },
      { title: 'CPC', dataKey: 'cpc' }
    ];
    this.pushToPDF(chart, 'Result', {columns: columns, rows: pdfTableRows});
    /*this.addToPdfChartQueue(chart, 'Result', 1000, 400);*/
    chart.draw(data, options);
  }
  public clearFilter(activeTab) {
    this.isApply = true;
    this.enableOverviewTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.saveFilterForm.get('duration').setValue('Custom Duration');
    this.onSelectDuration('Custom Duration');
    this.selectedGender = [];
    this.selectedLocation = [];
    this.selectedSpecialization = [];
    this.getAnalyticsData(activeTab);
  }
  public applyFilter(activeTab) {
    this.isApply = true;
    this.enableOverviewTab = false;
    this.enableAudienceTab = false;
    this.enableBehaviorTab = false;
    this.enableConversionTab = false;
    this.getAnalyticsData(activeTab);
  }
  public changeTab(activeTab) {
    this.hideExtraGraph = true;
    this.isApply = true;
    this.activeTab = activeTab;
    this.analyticsGraphsDataTemp = {
      overviewTopAdAssets: [],
      overviewAssetTypeAndUtilization: [],
      overviewValidatedHcps: null,
      audienceGenderExperience: null,
      audienceSpecialization: [],
      audienceLocation: [],
      audienceArcheType: null,
      audienceAssetType: [],
      behaviorTimeOfDay: [],
      behaviorDevices: null,
      conversionEnagement: null,
      conversionResults: null
    };
    setTimeout(() => {
      this.analyticsGraphsDataTemp = this.analyticsGraphsData;
      if (this.platformId) {
        /*overview charts*/
        if (activeTab === 'overviewTab') {
          // this.spinner.showSpinner.next(true);
          if (this.checkNotNull(this.analyticsGraphsData.overviewTopAdAssets)) {
            this.drawOverviewTopAdAssetsScatterChart(this.analyticsGraphsData.overviewTopAdAssets);
          } else {
            this.resetGraph('overviewTopAdAssetsScatterChart');
          }

          if (this.checkNotNull(this.analyticsGraphsData.overviewAssetTypeAndUtilization)) {
            this.drawOverviewAssetTypeAndUtilizationDonutChart(this.analyticsGraphsData.overviewAssetTypeAndUtilization);
          } else {
            this.resetGraph('overviewBannerAssetTypeAndUtilizationDonutChart');
          }
          if (this.checkNotNull(this.analyticsGraphsData.overviewValidatedHcps)) {
            this.drawOverviewValidatedHcpsDonutChart(this.analyticsGraphsData.overviewValidatedHcps);
          } else {
            this.resetGraph('overviewValidatedHcpsDonutChart');
          }
          // this.updateSubscription = interval(100).subscribe(
          //   (val) => {
          //     if (this.enableOverviewTab) {
          //       this.spinner.showSpinner.next(false);
          //     }
          // });
        }
      }
      /*audience chart*/
      if (activeTab === 'audienceTab') {
        if (this.checkNotNull(this.analyticsGraphsData.audienceSpecialization)) {
          this.drawAudienceSpecializationStackBarChart(this.analyticsGraphsData.audienceSpecialization);
        } else {
          this.resetGraph('audienceSpecializationStackedBarChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.audienceLocation)) {
          this.drawAudienceLocationMapChart(this.analyticsGraphsData.audienceLocation);
        } else {
          this.resetGraph('audienceLocationChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.audienceArcheType)) {
          this.drawAudienceArcheTypeBarChart(this.analyticsGraphsData.audienceArcheType);
        } else {
          this.resetGraph('audienceHCPArcheTypeBarChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.audienceAssetType)) {
          this.drawAudienceAssetType(this.analyticsGraphsData.audienceAssetType);
        } else {
          this.resetGraph('audienceAssetTypeDonutChart');
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableAudienceTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }

      /*behavior chart*/
      if (activeTab === 'behaviorTab') {
        if (this.checkNotNull(this.analyticsGraphsData.behaviorTimeOfDay)) {
          this.drawBehaviorTimeOfDayHeatChart(this.analyticsGraphsData.behaviorTimeOfDay);
        } else {
          this.resetGraph('behaviorheatmap');
        }
        if (this.checkNotNull(this.analyticsGraphsData.behaviorDevices)) {
          this.drawBehaviorDeviceDonutChart(this.analyticsGraphsData.behaviorDevices);
        } else {
          this.resetGraph('behaviorDevicesDonutChart');
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableBehaviorTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }

      /*conversion chart*/
      if (activeTab === 'conversionTab') {
        if (this.checkNotNull(this.analyticsGraphsData.conversionEnagement)) {
          this.drawConversionEngagementAreaLineChart(this.analyticsGraphsData.conversionEnagement);
        } else {
          this.resetGraph('conversionEngagementAreaChart');
        }
        if (this.checkNotNull(this.analyticsGraphsData.conversionResults) && this.analyticsGraphsData.conversionResults.data.length) {
          this.conversionResultData = this.analyticsGraphsData.conversionResults;
          this.drawConversionResultLineChart(this.conversionResultData);
        } else {
          this.resetGraph('conversionResultLineChart');
        }
        // this.updateSubscription = interval(100).subscribe(
        //   (val) => {
        //     if (this.enableConversionTab) {
        //       this.spinner.showSpinner.next(false);
        //     }
        // });
      }

      // This is taking too much of time, so commenting, not sure what will be repurcussions
      if (this.ddTreeLocation) {
        this.ddTreeLocation.onSelect(this.persistSelectedLocation);
        this.ddTreeLocation.updateItems(this.allLocationTreeItems, this.persistSelectedLocation);
      }

      if (this.ddTreeSpecialization) {
        this.ddTreeSpecialization.onSelect(this.persistSelectedSpecialization);
        this.ddTreeSpecialization.updateItems(this.allSpecializationTreeItems, this.persistSelectedSpecialization);
      }

      if (this.ddTreeGender) {
        this.ddTreeGender.onSelect(this.persistSelectedGender);
        this.ddTreeGender.updateItems(this.allGenderTreeItems, this.persistSelectedGender);
      }
      this.hideExtraGraph = false;
    }, 100);
  }
  getAnalyticsData(activeTab) {
    this.hideExtraGraph = true;
    google.charts.load('current', { packages: ['corechart', 'table'], mapsApiKey: config['AGM']['KEY'] });
    this.saveFilterForm.get('platformId').setValue(this.platformId);
    this.saveFilterForm.get('assetId').setValue(this.assetId);
    this.saveFilterForm.get('timezone').setValue(new Date().getTimezoneOffset());
    if (!this.isApply) {
      this.saveFilterForm.get('startDate').setValue(new Date(this.viewingSiteCreateDate).toString());
      this.saveFilterForm.get('endDate').setValue(new Date(this.todayDate).toString());
    } else {
      let startDate = new Date(this.saveFilterForm.get('startDate').value);
      let endDate = new Date(this.saveFilterForm.get('endDate').value);
      if (this.saveFilterForm.get('duration').value === 'Custom Duration') {
        startDate = new Date(new Date(this.saveFilterForm.get('startDate').value).setHours(0o0, 0o0, 0o0));
        endDate = new Date(new Date(this.saveFilterForm.get('endDate').value).setHours(23, 59, 59));
      }
      this.saveFilterForm.get('startDate').setValue(new Date(startDate).toString());
      this.saveFilterForm.get('endDate').setValue(new Date(endDate).toString());
    }
    this.unwindFilter();
    // this.resetPdfNode();
    this.locationData = null;
    if (this.saveFilterForm.get('startDate').value === 'Invalid Date') {
      this.saveFilterForm.get('startDate').setValue(new Date());
    }

    if (this.saveFilterForm.get('endDate').value === 'Invalid Date') {
      this.saveFilterForm.get('endDate').setValue(new Date());
    }
    this.saveFilterForm.get('tabType').setValue('overviewTab');
    this.publisherAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
      response => {
        if (!this.isApply) {
          this.saveFilterForm.get('startDate').setValue(new Date(this.viewingSiteCreateDate));
          this.saveFilterForm.get('endDate').setValue(this.todayDate);
        } else {
          this.saveFilterForm.get('startDate').setValue(new Date(this.saveFilterForm.get('startDate').value));
          this.saveFilterForm.get('endDate').setValue(new Date(this.saveFilterForm.get('endDate').value));
        }
        this.analyticsGraphsData.overviewTopAdAssets = response.overviewTopAdAssets;
        this.analyticsGraphsDataTemp.overviewTopAdAssets = this.analyticsGraphsData.overviewTopAdAssets;

        this.analyticsGraphsData.overviewAssetTypeAndUtilization = response.overviewAssetTypeAndUtilization;
        this.analyticsGraphsDataTemp.overviewAssetTypeAndUtilization = this.analyticsGraphsData.overviewAssetTypeAndUtilization;

        this.analyticsGraphsData.overviewValidatedHcps = response.overviewValidatedHcps;
        this.analyticsGraphsDataTemp.overviewValidatedHcps = this.analyticsGraphsData.overviewValidatedHcps;

        this.startAnalyticsDate = response.startDate;
        this.endAnalyticsDate = response.endDate;
        if (this.loadGraphs) {
          if (this.platformId) {
            /*overview charts*/
            if (this.checkNotNull(response.overviewTopAdAssets)) {
              this.drawOverviewTopAdAssetsScatterChart(response.overviewTopAdAssets);
            } else {
              this.resetGraph('overviewTopAdAssetsScatterChart');
            }

            if (this.checkNotNull(response.overviewAssetTypeAndUtilization)) {
              this.drawOverviewAssetTypeAndUtilizationDonutChart(response.overviewAssetTypeAndUtilization);
            } else {
              this.resetGraph('overviewBannerAssetTypeAndUtilizationDonutChart');
            }
            if (this.checkNotNull(response.overviewValidatedHcps)) {
              this.drawOverviewValidatedHcpsDonutChart(response.overviewValidatedHcps);
            } else {
              this.resetGraph('overviewValidatedHcpsDonutChart');
            }
          }
          this.enableOverviewTab = true;
          if (this.enableOverviewTab && this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab) {
            this.hideExtraGraph = false;
            this.activeTab = activeTab;
          }
        }
      },
      (err: Error) => {

      });
          /*audience chart*/

          /*if (this.checkNotNull(response.audienceGenderExperience)) {
            this.drawAudienceGenderExperiencePieChart(response.audienceGenderExperience);
          }*/
      this.saveFilterForm.get('tabType').setValue('audienceTab');
      this.publisherAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
        response => {
          this.analyticsGraphsData.audienceSpecialization = response.audienceSpecialization;
          this.analyticsGraphsDataTemp.audienceSpecialization = this.analyticsGraphsData.audienceSpecialization;

          this.analyticsGraphsData.audienceLocation = response.audienceLocation;
          this.analyticsGraphsDataTemp.audienceLocation = this.analyticsGraphsData.audienceLocation;

          this.analyticsGraphsData.audienceArcheType = response.audienceArcheType;
          this.analyticsGraphsDataTemp.audienceArcheType = this.analyticsGraphsData.audienceArcheType;

          this.analyticsGraphsData.audienceAssetType = response.audienceAssetType;
          this.analyticsGraphsDataTemp.audienceAssetType = this.analyticsGraphsData.audienceAssetType;

          if (this.loadGraphs) {
            if (this.checkNotNull(response.audienceSpecialization)) {
              this.drawAudienceSpecializationStackBarChart(response.audienceSpecialization);
            } else {
              this.resetGraph('audienceSpecializationStackedBarChart');
            }
            if (this.checkNotNull(response.audienceLocation)) {
              this.drawAudienceLocationMapChart(response.audienceLocation);
            } else {
              this.resetGraph('audienceLocationChart');
            }
            if (this.checkNotNull(response.audienceArcheType)) {
              this.drawAudienceArcheTypeBarChart(response.audienceArcheType);
            } else {
              this.resetGraph('audienceHCPArcheTypeBarChart');
            }
            if (this.checkNotNull(response.audienceAssetType)) {
              this.drawAudienceAssetType(response.audienceAssetType);
            } else {
              this.resetGraph('audienceAssetTypeDonutChart');
            }
            this.enableAudienceTab = true;
            if (this.enableOverviewTab && this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab) {
              this.hideExtraGraph = false;
              this.activeTab = activeTab;
            }
          }
      },
      (err: Error) => {

      });
        /*behavior chart*/
      this.saveFilterForm.get('tabType').setValue('behaviorTab');
      this.publisherAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
        response => {
          this.analyticsGraphsData.behaviorTimeOfDay = response.behaviorTimeOfDay;
          this.analyticsGraphsDataTemp.behaviorTimeOfDay = this.analyticsGraphsData.behaviorTimeOfDay;

          this.analyticsGraphsData.behaviorDevices = response.behaviorDevices;
          this.analyticsGraphsDataTemp.behaviorDevices = this.analyticsGraphsData.behaviorDevices;

          if (this.loadGraphs) {
            if (this.checkNotNull(response.behaviorTimeOfDay)) {
              this.drawBehaviorTimeOfDayHeatChart(response.behaviorTimeOfDay);
            } else {
              this.resetGraph('behaviorheatmap');
            }
            if (this.checkNotNull(response.behaviorDevices)) {
              this.drawBehaviorDeviceDonutChart(response.behaviorDevices);
            } else {
              this.resetGraph('behaviorDevicesDonutChart');
            }
            this.enableBehaviorTab = true;
            if (this.enableOverviewTab && this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab) {
              this.hideExtraGraph = false;
              this.activeTab = activeTab;
            }
          }
      },
      (err: Error) => {

      });
        /*conversion chart*/
      this.saveFilterForm.get('tabType').setValue('conversionTab');
      this.publisherAnalyticsService.getAnalyticsData(this.saveFilterForm.value).subscribe(
        response => {

          this.analyticsGraphsData.conversionEnagement = response.conversionEnagement;
          this.analyticsGraphsDataTemp.conversionEnagement = this.analyticsGraphsData.conversionEnagement;

          this.analyticsGraphsData.conversionResults = response.conversionResults;
          this.analyticsGraphsDataTemp.conversionResults = this.analyticsGraphsData.conversionResults;

          if (this.loadGraphs) {
            if (this.checkNotNull(response.conversionEnagement)) {
              this.drawConversionEngagementAreaLineChart(response.conversionEnagement);
            } else {
              this.resetGraph('conversionEngagementAreaChart');
            }
            if (this.checkNotNull(response.conversionResults) && this.analyticsGraphsData.conversionResults.data.length) {
              this.conversionResultData = response.conversionResults;
              this.drawConversionResultLineChart(this.conversionResultData);
            } else {
              this.resetGraph('conversionResultLineChart');
            }
            this.enableConversionTab = true;
            if (this.enableOverviewTab && this.enableAudienceTab && this.enableBehaviorTab && this.enableConversionTab) {
              this.hideExtraGraph = false;
              this.activeTab = activeTab;
            }
          }

          if (this.ddTreeLocation) {
            this.ddTreeLocation.onSelect(this.persistSelectedLocation);
            this.ddTreeLocation.updateItems(this.allLocationTreeItems, this.persistSelectedLocation);
          }

          if (this.ddTreeSpecialization) {
            this.ddTreeSpecialization.onSelect(this.persistSelectedSpecialization);
            this.ddTreeSpecialization.updateItems(this.allSpecializationTreeItems, this.persistSelectedSpecialization);
          }

          if (this.ddTreeGender) {
            this.ddTreeGender.onSelect(this.persistSelectedGender);
            this.ddTreeGender.updateItems(this.allGenderTreeItems, this.persistSelectedGender);
          }
      },
      (err: Error) => {

      });
    // to get cards data
    this.publisherAnalyticsService.getAnalyticsCardsData(this.saveFilterForm.value).subscribe(
      response => {
        this.platformResults = response;
      },
      error => {}
      );
  }

  resetGraph(elementName: string) {
    const node = document.getElementById(elementName);
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
      const a = document.createElement('span');
      // a.setAttribute('class', 'noChartData');
      if (elementName === 'overviewTopAdAssetsScatterChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-overview-top-slot.jpg"
        alt="loading" style="width:973px; height:300px; margin-top="-1000px"> <p style="position: relative;top:-172px;width:361px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'overviewBannerAssetTypeAndUtilizationDonutChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-overview-utilization.jpg"
        alt="loading" style="width:auto; height:300px;"> <p style="position: relative; top:-162px;width:360px; margin: auto !important;text-align: center; left:-9%">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'overviewValidatedHcpsDonutChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-overview-hcp-validation.jpg"
        alt="loading" style="width:310px; height:300px"> <p style="position: relative;  top:-162px;width:360px; margin: auto !important;text-align: center; left:-13%">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'audienceSpecializationStackedBarChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-audience-specialization.jpg"
        alt="loading" style="width:1000px; height:300px"> <p style="position: relative;top:-162px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'audienceLocationChart') {
        if(this.zone === '1') {
          a.innerHTML =
        `<img src="../assets/images/graph/pub-audience-location.jpg"
        alt="loading" style="width:1000px; height:300px;"> <p style="position: relative; top:-162px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
        }
        else if(this.zone === '2'){a.innerHTML =
          `<img src="../assets/images/graph/pub-audience-location_ind.jpg"
          alt="loading" style="width:1000px; height:300px;"> <p style="position: relative; top:-162px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;}
              }      
      else if (elementName === 'audienceHCPArcheTypeBarChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-audience-archetype.jpg"
        alt="loading" style="width:540px; height:300px"> <p style="position: relative;  top:-182px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'audienceAssetTypeDonutChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-audience-ad-slots.jpg"
        alt="loading" style="width:400px; height:300px;position: relative;right: 46%"> <p style="position: relative; top:-162px;width:320px; margin: auto !important;text-align: center; left:-22%">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'behaviorDevicesDonutChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-behavior-devices.jpg"
        alt="loading" style="width:370px; height:245px"> <p style="position: relative; top:-152px;width: 280px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'conversionEngagementAreaChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-conversion-engagement.jpg"
        alt="loading" style="width:969px; height:368px;"> <p style="position: relative;top:-202px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'conversionResultLineChart') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-conversion-result.jpg"
        alt="loading" style="width:1100px; height:352px;"> <p style="position: relative; top:-212px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else if (elementName === 'behaviorheatmap') {
        a.innerHTML =
        `<img src="../assets/images/graph/pub-behavior-heat.jpg"
        alt="loading" style="width:540px; height:300px"> <p style="position: relative;  top:-172px;width: 400px; margin: auto !important;text-align: center;">You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      } else {
        a.innerHTML = '<p class="noChartData tac">Not enough data to plot graph</p>';
      }
      document.getElementById(elementName).appendChild(a);
    }
  }

  resetGraphs() {
    this.resetGraph('audienceSpecializationStackedBarChart');
    this.resetGraph('audienceLocationChart');
    this.resetGraph('audienceHCPArcheTypeBarChart');
    this.resetGraph('audienceAssetTypeDonutChart');
    this.resetGraph('behaviorDevicesDonutChart');
    this.resetHeatMap();
    this.resetGraph('conversionEngagementAreaChart');
    this.resetGraph('conversionResultLineChart');
    this.resetGraph('overviewTopAdAssetsScatterChart');
    this.resetGraph('overviewBannerAssetTypeAndUtilizationDonutChart');
    this.resetGraph('overviewValidatedHcpsDonutChart');
    this.resetPdfNode();
  }

  resetHeatMap() {
    const el: HTMLElement = document.getElementById('heatmap');
    if (el !== undefined && el !== null) {
      el.innerHTML =
      '<img src="../assets/images/publisher_heat_map.png" alt="loading" style="width:540px; height:300px">';
    }
  }

  resetPdfNode() {
    const node = document.getElementById('chart-n-data');
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
    }
  }

  checkNotNull(data: any) {
    if (data instanceof Array) {
      if (data.length > 0) {
        return true;
      }
      return false;
    } else if (data instanceof Object) {
      if (data && (Object.keys(data).length !== 0)) {
        return true;
      }
      return false;
    } else if (typeof data === 'string' || data instanceof String) {
      if (data.length !== 0) {
        return true;
      }
      return false;
    }

  }

  scale(arr: number[], scaledMin: number, scaledMax: number) {
    const max = Math.max.apply(Math, this);
    const min = Math.min.apply(Math, this);
    return arr.map(num => (scaledMax - scaledMin) * (num - min) / (max - min) + scaledMin);
  }

  private pushToPDF(chart: any, chartName: string, tableData: any) {
    google.visualization.events.addListener(chart, 'ready', function() {
      const chartContent = document.createElement('img');
      chartContent.src = chart.getImageURI();
      chartContent.setAttribute('name', chartName);
      chartContent.setAttribute('id', chartName);

      const tableContent = document.createElement('input');
      tableContent.setAttribute('type', 'text');
      tableContent.setAttribute('name', chartName);
      tableContent.setAttribute('id', chartName);
      tableContent.setAttribute('value', JSON.stringify(tableData));

      document.getElementById('chart-n-data').appendChild(tableContent);
      document.getElementById('chart-n-data').appendChild(chartContent);
    });
  }

  public pdfBuilder() {

    const jsPDF = require('jspdf');
    require('jspdf-autotable');
    const pdfAssetName = this.showSiteAnalytics ? '-' : this.assetName ;
    const pdfPlatformName = this.viewingSiteName ;
    let locationPdfNames: string;
    let specializationPdfNames: string;
    let genderPdfNames: string;
    locationPdfNames = '';
    if (this.selectedLocation.length) {
      let length = 0;
      this.selectedLocation.forEach((element, index) => {
        if (length < 2) {
          const findedLocation = this.locationListForPdf.find(obj => obj.zipcode === element);
          if (findedLocation) {
            length++;
            locationPdfNames = `${locationPdfNames} ${findedLocation.city}, `;
          }
        }
      });
      locationPdfNames = locationPdfNames.trim().slice(0, -1);
      if (this.selectedLocation.length > 2) {
        locationPdfNames = `${locationPdfNames}, ${this.selectedLocation.length - 2} More...` ;
      }
    } else {
      locationPdfNames = '-';
    }
    specializationPdfNames = '';
    if (this.selectedSpecialization.length) {
      let previousName = '';
      let length = 0;
      this.selectedSpecialization.forEach((element, index) => {
        if (length < 2) {
          const findedSpecialization = this.specializationListForPdf.find(obj => obj.Taxonomy === element);
          if (findedSpecialization && previousName !== findedSpecialization.Classification) {
            length++;
            previousName = findedSpecialization.Classification;
            specializationPdfNames = `${specializationPdfNames} ${findedSpecialization.Classification}, `;
          }
        }
      });
      specializationPdfNames = specializationPdfNames.trim().slice(0, -1);
      if (this.selectedSpecialization.length > 2) {
        specializationPdfNames = `${specializationPdfNames}, ${this.selectedSpecialization.length - 2} More...` ;
      }
    } else {
      specializationPdfNames = '-';
    }
    genderPdfNames = '';
    if (this.selectedGender.length) {
      this.selectedGender.forEach((element) => {
        if (element === 'M') {
          genderPdfNames = `${genderPdfNames} Male, `;
        } else if (element === 'F') {
          genderPdfNames = `${genderPdfNames} Female, `;
        }
      });
      genderPdfNames = genderPdfNames.trim().slice(0, -1);
    } else {
      genderPdfNames = '-';
    }
    const doc = new jsPDF('p', 'pt', 'a4', 0);
    doc.setFillColor(242, 242, 242);
    doc.rect(0, 0, 600, 1000, 'F');
    let headerBarImage;
    let docereeLogo;
    headerBarImage = document.getElementById('publisher_header');
    docereeLogo = document.getElementById('doceree_logo');
    doc.addImage(docereeLogo, 'png', 30, 30, 85, 22);
    doc.setFontSize(8);
    doc.addImage(headerBarImage, 'png', 20, 65, 555, 20);
    doc.setTextColor(255, 255, 255);
    doc.text(`${
      new Date(this.startAnalyticsDate).toDateString()} - ${new Date(this.endAnalyticsDate).toDateString()}`, 430, 78);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.text(`${this.userDetails.first_name} ${this.userDetails.last_name}`, 30, 78);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(0, 0, 0);

    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, 85, 555, 60, 'F');

    doc.text('Platform Name', 35, 100);
    doc.text(pdfPlatformName, 100, 100);

    doc.text('Slot Name', 35, 117);
    doc.text(pdfAssetName, 100, 117);

    doc.setDrawColor(0, 0, 0);
    doc.line(240, 140, 240, 90);

    doc.text('Location', 260, 100);
    doc.text(locationPdfNames, 320, 100);

    doc.text('Specialization', 260, 117);
    doc.text(specializationPdfNames, 320, 117);

    doc.text('Gender', 260, 133);
    doc.text(genderPdfNames, 320, 133);

    doc.setFontSize(9);

    const images = document.getElementById('chart-n-data');
    const tagsTemp = images.getElementsByTagName('img');
    const tableDataArrayTemp = images.getElementsByTagName('input');
    let tags: any;
    tags = [];
    let tableDataArray: any;
    tableDataArray = [];

    let topAdAssetsHeight: number;
    let assetTypeUtilizationHeight: number;
    let validatedHCPHeight: number;
    let audienceSpecializationHeight: number;
    let audienceLocationHeight: number;
    let audienceArchetypeHeight: number;
    let audienceAssetTypeHeight: number;
    let deviceBehaviorHeight: number;
    let engagementConversionHeight: number;
    let resultConversionHeight: number;

    for (let i = 0; i < tagsTemp.length; i++) {
      if (tagsTemp[i]['id'] === 'Top Slots') {
        tags[0] = tagsTemp[i];
        tableDataArray[0] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Slot Type & Utilization') {
        tags[1] = tagsTemp[i];
        tableDataArray[1] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Validated HCPs') {
        tags[2] = tagsTemp[i];
        tableDataArray[2] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Specializations') {
        tags[3] = tagsTemp[i];
        tableDataArray[3] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Location') {
        tags[9] = tagsTemp[i];
        tableDataArray[9] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'HCP Archetype') {
        tags[4] = tagsTemp[i];
        tableDataArray[4] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Slot Type') {
        tags[5] = tagsTemp[i];
        tableDataArray[5] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Behavior') {
        tags[6] = tagsTemp[i];
        tableDataArray[6] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Engagement') {
        tags[7] = tagsTemp[i];
        tableDataArray[7] = tableDataArrayTemp[i];
      } else if (tagsTemp[i]['id'] === 'Result') {
        tags[8] = tagsTemp[i];
        tableDataArray[8] = tableDataArrayTemp[i];
      }
    }
    if (this.showSiteAnalytics) {
      topAdAssetsHeight = 181;
      assetTypeUtilizationHeight = 324;
      validatedHCPHeight = 324;
      audienceSpecializationHeight = 510;
      audienceLocationHeight = 663;
      audienceArchetypeHeight = 85;
      audienceAssetTypeHeight = 300;
      deviceBehaviorHeight = 300;
      engagementConversionHeight = 490;
      resultConversionHeight = 654;
    } else {
      audienceSpecializationHeight = 210;
      audienceLocationHeight = 363;
      audienceArchetypeHeight = 517;
      audienceAssetTypeHeight = 115;
      deviceBehaviorHeight = 115;
      engagementConversionHeight = 320;
      resultConversionHeight = 484;
    }
  // **************************************************************
    let tableData;
    let title_name;
    let tableRows: any;
    let count: number;
    if (this.showSiteAnalytics) {
      doc.addImage(headerBarImage, 'png', 20, topAdAssetsHeight - 21, 55, 20);
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.setTextColor(255, 255, 255);
      doc.text('Overview', 25, topAdAssetsHeight - 8);

      doc.setFontSize(9);
      doc.setFontType('normal');
      doc.setTextColor(0, 0, 0);

      // Top Slots start.
      doc.setFillColor(255, 255, 255);
      doc.rect(19.5, topAdAssetsHeight, 381, 140, 'F');
      tableData = JSON.parse(tableDataArray[0].getAttribute('value'));
      tableRows = [];
      count = 0;
      tableData.rows.forEach(element => {
        if (element.earning > 0 && count < 10) {
          count++;
          tableRows.push(element);
        }
      });
      title_name = (tags[0].getAttribute('name'));
      doc.text(title_name, 30, topAdAssetsHeight + 15);
      doc.addImage(tags[0], 'png', 40, topAdAssetsHeight + 20, 360, 120);
      doc.setFillColor(255, 255, 255);
      doc.rect(403, topAdAssetsHeight, 172, 140, 'F');
      doc.autoTable(tableData.columns, tableRows, {
        tableWidth: 159,
        startY: topAdAssetsHeight + 15,
        margin: { horizontal: 409},
        styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
        bodyStyles: { valign: 'top'},
        theme: 'striped',
        headStyles: { fillColor: '#fff', textColor: '#000'}
      });
      // Top Slots ends.

      // Slot Type & Utilization  starts.
      doc.setFillColor(255, 255, 255);
      doc.rect(19.5, assetTypeUtilizationHeight, 151, 150, 'F');
      tableData = JSON.parse(tableDataArray[1].getAttribute('value'));
      tableRows = [];
      count = 0;
      tableData.rows.forEach(element => {
        if ((element.successCount > 0  || element.failureCount > 0) && count < 10) {
          count++;
          tableRows.push(element);
        }
      });
      title_name = (tags[1].getAttribute('name'));
      doc.text(title_name, 30, assetTypeUtilizationHeight + 16);
      doc.addImage(tags[1], 'png', 30, assetTypeUtilizationHeight + 18, 130, 125);
      doc.setFillColor(255, 255, 255);
      doc.rect(173, assetTypeUtilizationHeight, 123, 150, 'F');
      doc.autoTable(tableData.columns, tableRows, {
        tableWidth: 112,
        startY: assetTypeUtilizationHeight + 16,
        margin: { horizontal: 178},
        styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
        bodyStyles: { valign: 'top'},
        theme: 'striped',
        headStyles: { fillColor: '#fff', textColor: '#000'}
      });
      // Slot Type & Utilization ends.

      // Validated HCPs starts
      doc.setFillColor(255, 255, 255);
      doc.rect(299, 324, 151, 150, 'F');
      tableData = JSON.parse(tableDataArray[2].getAttribute('value'));
      title_name = tags[2].getAttribute('name');
      doc.text(title_name, 310, 340);
      doc.addImage(tags[2], 'png', 310, 342, 130, 125);
      doc.setFillColor(255, 255, 255);
      doc.rect(452, 324, 123, 150, 'F');
      doc.autoTable(tableData.columns, tableData.rows, {
        tableWidth: 112,
        startY: 337,
        margin: { horizontal: 457},
        styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
        bodyStyles: { valign: 'top'},
        theme: 'striped',
        headStyles: { fillColor: '#fff', textColor: '#000'}
      });
      // Validated HCPs ends
    }

    doc.addImage(headerBarImage, 'png', 20, audienceSpecializationHeight - 21, 57, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Audience', 25, audienceSpecializationHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);

    // Specializations starts
    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, audienceSpecializationHeight, 381, 150, 'F');
    tableData = JSON.parse(tableDataArray[3].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if ((element.male > 0 || element.female > 0 || element.other > 0) && count < 10) {
        count++;
        tableRows.push(element);
      }
    });
    title_name = tags[3].getAttribute('name');
    doc.text(title_name, 30, audienceSpecializationHeight + 15);
    doc.addImage(tags[3], 'png', 40, audienceSpecializationHeight + 22, 360, 120);
    doc.setFillColor(255, 255, 255);
    doc.rect(403, audienceSpecializationHeight, 172, 150, 'F');
    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 159,
      startY: audienceSpecializationHeight + 15,
      margin: { horizontal: 409},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // Specializations ends.

    // Location starts.
    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, audienceLocationHeight, 381, 150, 'F');
    tableData = JSON.parse(tableDataArray[9].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if (element.popularity > 0  && count < 10) {
        count++;
        element.popularity = element.popularity.toFixed(2);
        tableRows.push(element);
      }
    });
    title_name = tags[9].getAttribute('name');
    doc.text(title_name, 30, audienceLocationHeight + 15);
    doc.addImage(tags[9], 'png', 40, audienceLocationHeight + 20, 300, 125);
    doc.setFillColor(255, 255, 255);
    doc.rect(403, audienceLocationHeight, 172, 150, 'F');
    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 159,
      startY: audienceLocationHeight + 15,
      margin: { horizontal: 409},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // Location ends.
    if (this.showSiteAnalytics) {
      doc.text('1/2', 300, 830);
      doc.addPage();
      doc.setFillColor(242, 242, 242);
      doc.rect(0, 0, 600, 1000, 'F');
      doc.addImage(docereeLogo, 'png', 30, 25, 85, 22);
      doc.setFontSize(8);
      doc.text(`${
        new Date(this.startAnalyticsDate).toDateString()} - ${new Date(this.endAnalyticsDate).toDateString()}`, 440, 40);
      doc.addImage(headerBarImage, 'png', 20, audienceArchetypeHeight  - 21, 55, 20);
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.setTextColor(255, 255, 255);
      doc.text('Audience', 25, audienceArchetypeHeight - 8);

      doc.setFontSize(9);
      doc.setFontType('normal');
      doc.setTextColor(0, 0, 0);
    }

    // HCP Archetype starts.
    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, audienceArchetypeHeight, 381, 175, 'F');
    tableData = JSON.parse(tableDataArray[4].getAttribute('value'));
    title_name = tags[4].getAttribute('name');
    doc.text(title_name, 30, audienceArchetypeHeight + 15);
    doc.addImage(tags[4], 'png', 40, audienceArchetypeHeight + 20, 330, 150);
    doc.setFillColor(255, 255, 255);
    doc.rect(403, audienceArchetypeHeight, 172, 175, 'F');
    doc.autoTable(tableData.columns, tableData.rows, {
      tableWidth: 159,
      startY: audienceArchetypeHeight + 15,
      margin: { horizontal: 409},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // HCP Archetype ends.
    if (!this.showSiteAnalytics) {
      doc.text('1/2', 300, 830);
      doc.addPage();
      doc.setFillColor(242, 242, 242);
      doc.rect(0, 0, 600, 1000, 'F');
      doc.addImage(docereeLogo, 'png', 30, 25, 85, 22);
      doc.setFontSize(8);
      doc.text(`${
        new Date(this.startAnalyticsDate).toDateString()} - ${new Date(this.endAnalyticsDate).toDateString()}`, 440, 40);
    }
    doc.addImage(headerBarImage, 'png', 20, audienceAssetTypeHeight - 21, 55, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Audience', 25, audienceAssetTypeHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);
    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, audienceAssetTypeHeight, 150, 150, 'F');
    // Slot Type starts
    tableData = JSON.parse(tableDataArray[5].getAttribute('value'));
    title_name = (tags[5].getAttribute('name'));
    doc.text(title_name, 30, audienceAssetTypeHeight + 15);
    doc.addImage(tags[5], 'png', 40, audienceAssetTypeHeight + 17, 105, 130);
    doc.setFillColor(255, 255, 255);
    doc.rect(171, audienceAssetTypeHeight, 120, 150, 'F');
    doc.autoTable(tableData.columns, tableData.rows, {
      tableWidth: 105,
      startY: audienceAssetTypeHeight + 15,
      margin: { horizontal: 178},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // Slot Type ends
    doc.addImage(headerBarImage, 'png', 304, deviceBehaviorHeight - 21, 55, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Behavior', 310, deviceBehaviorHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);
    // Behavior starts
    doc.setFillColor(255, 255, 255);
    doc.rect(303.5, deviceBehaviorHeight, 150, 150, 'F');
    tableData = JSON.parse(tableDataArray[6].getAttribute('value'));
    title_name = tags[6].getAttribute('name');
    doc.text(title_name, 315, deviceBehaviorHeight + 15);
    doc.addImage(tags[6], 'png', 305, deviceBehaviorHeight + 30, 145, 95);
    doc.setFillColor(255, 255, 255);
    doc.rect(455, deviceBehaviorHeight, 120, 150, 'F');
     doc.autoTable(tableData.columns, tableData.rows, {
      tableWidth: 105,
      startY: deviceBehaviorHeight + 15,
      margin: { horizontal: 462},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // Behavior ends
    doc.addImage(headerBarImage, 'png', 20, engagementConversionHeight - 21, 65, 20);
    doc.setFontSize(10);
    doc.setFontType('bold');
    doc.setTextColor(255, 255, 255);
    doc.text('Conversion', 25, engagementConversionHeight - 8);

    doc.setFontSize(9);
    doc.setFontType('normal');
    doc.setTextColor(0, 0, 0);
    // Engagement starts
    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, engagementConversionHeight, 381, 160, 'F');
    tableData = JSON.parse(tableDataArray[7].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if ((element.impression > 0 || element.reach > 0 || element.click > 0) && count < 11) {
        count++;
        tableRows.push(element);
      }
    });
    title_name = tags[7].getAttribute('name');
    doc.text(title_name, 30, engagementConversionHeight + 15);
    doc.addImage(tags[7], 'png', 40, engagementConversionHeight + 20, 360, 130);

    doc.setFontSize(5);
    doc.setFillColor('#ec736b');
    doc.circle(277, engagementConversionHeight + 155, 2, 'FD');
    doc.text('Impression', 280, engagementConversionHeight + 157);

    doc.setFillColor('#92d8f0');
    doc.circle(317, engagementConversionHeight + 155, 2, 'FD');
    doc.text('Reach', 320, engagementConversionHeight + 157);

    doc.setFillColor('#b4a0cd');
    doc.circle(347, engagementConversionHeight + 155, 2, 'FD');
    doc.text('Click', 350, engagementConversionHeight + 157);

    doc.setFontSize(9);

    doc.setFillColor(255, 255, 255);
    doc.rect(403, engagementConversionHeight, 172, 160, 'F');
    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 159,
      startY: engagementConversionHeight + 15,
      margin: { horizontal: 409},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // Engagement ends
    // Result starts
    doc.setFillColor(255, 255, 255);
    doc.rect(19.5, resultConversionHeight, 381, 155, 'F');
    tableData = JSON.parse(tableDataArray[8].getAttribute('value'));
    tableRows = [];
    count = 0;
    tableData.rows.forEach(element => {
      if ((element.cpc > 0 || element.cpm > 0 || element.ctr > 0 || element.earnings > 0) && count < 6) {
        count++;
        element.cpc = element.cpc.toFixed(2);
        element.cpm = element.cpm.toFixed(2);
        element.ctr = element.ctr.toFixed(2);
        element.earnings = element.earnings.toFixed(2);
        tableRows.push(element);
      }
    });
    doc.setFillColor(255, 255, 255);
    doc.rect(403, resultConversionHeight, 172, 155, 'F');
    title_name = tags[8].getAttribute('name');
    doc.text(title_name, 30, resultConversionHeight + 15);
    doc.addImage(tags[8], 'png', 40, resultConversionHeight + 20, 360, 120);

    doc.setFontSize(5);

    doc.setFillColor('#5287c6');
    doc.circle(257, resultConversionHeight + 149, 2, 'FD');
    doc.text('Earnings', 260, resultConversionHeight + 151);

    doc.setFillColor('#78c068');
    doc.circle(287, resultConversionHeight + 149, 2, 'FD');
    doc.text('CPM', 290, resultConversionHeight + 151);

    doc.setFillColor('#9a67ab');
    doc.circle(317, resultConversionHeight + 149, 2, 'FD');
    doc.text('CTR', 320, resultConversionHeight + 151);

    doc.setFillColor('#f37a6e');
    doc.circle(347, resultConversionHeight + 149, 2, 'FD');
    doc.text('CPC', 350, resultConversionHeight + 151);

    doc.setFontSize(9);
    doc.setFillColor('#000');

    doc.autoTable(tableData.columns, tableRows, {
      tableWidth: 159,
      startY: resultConversionHeight + 15,
      margin: { horizontal: 409},
      styles: { overflow: 'linebreak', fontSize: 6, cellPadding: 2 },
      bodyStyles: { valign: 'top'},
      theme: 'striped',
      headStyles: { fillColor: '#fff', textColor: '#000'}
    });
    // Result ends
    doc.text('2/2', 300, 830);
    doc.save('test' + '.pdf');
  }

  calculateAspectRatioFit(srcWidth: number, srcHeight: number, maxWidth: number, maxHeight: number) {
    const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return { width: srcWidth * ratio, height: srcHeight * ratio };
  }

  public twoToggle(toggleValue: any) {
    if (this.toggleStateMap.get(toggleValue) === undefined) {
      this.toggleStateMap.set(toggleValue, toggleValue);
      this.activeToggleArray.push(toggleValue);
      if (this.activeToggleArray.length > 2) {
        this['toggle' + this.activeToggleArray[0]] = undefined;
        this.toggleStateMap.set(this.activeToggleArray[0], undefined);
        this.activeToggleArray.shift();
      }
    } else if (this.toggleStateMap.get(toggleValue) === toggleValue) {
      this.toggleStateMap.set(toggleValue, undefined);
      this['toggle' + toggleValue] = undefined;
      this.activeToggleArray = this.activeToggleArray.filter(item => item !== toggleValue);
    }
    this.drawConversionResultLineChart(this.conversionResultData);
  }

  private getSeriesAndVAxes(): any {

    const earningsColor = '#5287c6';
    const cpmColor = '#78c068';
    const cpcColor = '#f37a6e';
    const ctrColor = '#9a67ab';

    let series: any = {};
    let vAxes: any = {};

    let toggleValues = Array.from(this.toggleStateMap.values());
    toggleValues = toggleValues.filter(( element ) => {
      return element !== undefined;
    });

    if (toggleValues && toggleValues.length === 1) {
      if (toggleValues.includes('Earnings')) {
        series = {
          0: { color: earningsColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CPM')) {
        series = {
          0: { color: cpmColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CPC')) {
        series = {
          0: { color: cpcColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes(this.currencySymbol+'#', null);
      }
      if (toggleValues.includes('CTR')) {
        series = {
          0: { color: ctrColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('#\'%\'', null);
      }
    }

    if (toggleValues && toggleValues.length === 2) {
      if (toggleValues.includes('Earnings') && toggleValues.includes('CPM')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CPM')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('Earnings') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', this.currencySymbol+'#');
        }
      }

      if (toggleValues.includes('Earnings') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes(this.currencySymbol+'#', this.currencySymbol+'#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', this.currencySymbol+'#');
        }
      }

      if (toggleValues.includes('CPC') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPC') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes(this.currencySymbol+'#', '#\'%\'');
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('#\'%\'', this.currencySymbol+'#');
        }
      }
    }
    return {series, vAxes};
  }

  populateVAxes(yAxis1Format: string, yAxis2Format: string): any {
    let a: any = {};

    if (yAxis2Format) {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: this.currencySymbol+'#'
        },
        1: {
          minValue: 0,
          max: 100,
          format: '#\'%\''
        }
      };
      a['0']['format'] = yAxis1Format;
      a['1']['format'] = yAxis2Format;
    } else {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: this.currencySymbol+'#'
        }
      };
      a['0']['format'] = yAxis1Format;
    }
    return a;
  }

}
