import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { BankdetailsService } from 'src/app/_services/publisher/bankdetails.service';
import { DashboardService } from 'src/app/_services/publisher/dashboard.service';
import { UserService } from 'src/app/_services/user.service';
import { PublisherDashboard } from '../../_models/publisher_change_dashboard_model';
import { Router } from '@angular/router';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { Observable, interval, Subscription, Subject } from 'rxjs';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { UtilService } from 'src/app/_services/utils/util.service';

@Component({
  selector: 'app-dashboard-details',
  templateUrl: './dashboard-details.component.html',
  styleUrls: ['./dashboard-details.component.scss']
})
export class DashboardDetailsComponent implements OnInit, OnDestroy {

  platformResults: PublisherDashboard;
  userDetailsTemplate;
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private userEmail = JSON.parse(localStorage.getItem('currentUser')).email;
  invoiceList: any[] = [];
  public filteredDuration = [];
  public saveFilterForm: FormGroup;
  startDate: Date;
  endDateMin: Date;
  performanceTrackingResults: any;
  endDate: Date;
  publisherId: string;
  platformId: string;
  public startDateMin = new Date(2019, 1, 1);
  public todayDate = new Date(new Date().setHours(23, 59, 59));
  private updateSubscription: Subscription;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['publisher_dashboard_earnings', 'publisher_dashboard_impressions',
  'publisher_dashboard_reach', 'publisher_dashboard_performance_tracking']);

  public toggleEarnings: any = 'Earnings';
  public toggleCPM: any = undefined;
  public toggleCTR: any = 'CTR';
  public toggleCPC: any = undefined;
  public activeToggleArray: string[] = ['Earnings', 'CTR'];
  public toggleStateMap: Map<string, string> = new Map<string, string>();
  private loadGraph;
  watchingPlatform: any;
  activeId = 1;
  skipId = 10;
  topBarElements = [1, 3, 7, 8];
  sideBarElements = [2, 4, 5, 6];
  rolePermissions: any;
  private currencySymbol : string;

  constructor(
    private dashboardService: DashboardService,
    private bankDetailsService: BankdetailsService,
    private formBuilder: FormBuilder,
    private toolTipService: ToolTipService,
    private eventEmitterService: EventEmitterService,
    private userService: UserService,
    public router: Router,
    public publisherPlatformService: PublisherPlatformService,
    private utilService: UtilService,
  ) {
    localStorage.setItem('activeIcon', JSON.stringify(''));
    if (this.router.getCurrentNavigation().extras.state) {
      this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.platformId);
    }
    this.eventEmitterService.onClickNavigation();
    this.userService.getUserByEmail(this.userEmail).subscribe(
      res => {
        this.userDetails.first_name = res.firstName;
        this.userDetails.last_name = res.lastName;
        this.userDetails.isVisitedDashboard = res.isVisitedDashboard;
      }
    );
    this.userDetailsTemplate = this.userDetails;
    this.bankDetailsService.getBills().subscribe(
      result => {
        this.invoiceList = <[]>result;
      },
      err => {
        console.log('Error! getAllInvoices : ' + err);
      });
    this.doActivity();
  }

  ngOnInit() {
    this.currencySymbol = this.utilService.getCurrencySymbol();
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
    this.loadGraph = true;
    this.getDashboardData();
    this.saveFilterForm = this.formBuilder.group({
      startDate: [Validators.required],
      endDate: [Validators.required],
      duration: ['Custom Duration', Validators.required]
    });
    this.filteredDuration = [
      { value: 'Custom Duration', name: 'Custom Duration' },
      { value: 'Last 24 hours', name: 'Last 24 hours' },
      { value: 'Last 48 hours', name: 'Last 48 hours' },
      { value: 'Last 1 week', name: 'Last 1 week' },
      { value: 'Last 1 month', name: 'Last 1 month' },
      { value: 'Last 6 months', name: 'Last 6 months' }
    ];

    this.toggleStateMap.set('Earnings', 'Earnings');
    this.toggleStateMap.set('CPM', undefined);
    this.toggleStateMap.set('CTR', 'CTR');
    this.toggleStateMap.set('CPC', undefined);

    this.updateSubscription = interval(10000).subscribe(
      (val) => {
        this.doActivity();
      }
    );
    this.fetchTooltips(this.toolTipKeyList);
  }

  scrollToNext() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    console.log('currentEl: ', currentEl);
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    if (skipEl) {
      console.log('skipEl: ', skipEl);
      skipEl.style.display = 'none';
    }
    this.activeId++;
    this.finalizeNextStep();
  }

  scrollToSkip() {
    const currentEl = document.getElementById('overlay_' + this.activeId);
    if (!currentEl) {
      this.skip();
    }
    currentEl.style.display = 'none';

    const skipEl = document.getElementById('overlay_' + this.skipId);
    skipEl.style.display = 'block';
    skipEl.style.position = 'fixed';
    skipEl.style.zIndex = '999';

    const skipButton = document.getElementById('skipButton');
    const skipButtonLocation = skipButton.getBoundingClientRect();
    console.log(skipButtonLocation);
    skipEl.style.left = (skipButtonLocation.left) + 'px';
    skipEl.style.top = (skipButtonLocation.top) + 'px';
  }

  skip() {
    const overLay = document.getElementById('overlay_bg');
    overLay.style.display = 'none';
    if (this.userDetails.isVisitedDashboard === false) {
      this.userService.setDashboardVisitedStatusByEmail(this.userDetails.email, true).subscribe(
        result => {
          console.log('setDashboardVisitedStatusByEmail : SUCCESS');
        },
        error => {
          console.log('setDashboardVisitedStatusByEmail : ERROR');
        }
      );
    }
  }

  finalizeNextStep() {
    let m = document.getElementById('siteclick_' + this.activeId);
    if (this.activeId === 7) {
      m = document.getElementById('siteclick_' + 1);
    }

    if (!m) {
      this.skip();
    }
    const mainElement = m.getBoundingClientRect();
    let x = mainElement.right;
    let y = mainElement.bottom;

    const nextEl = document.getElementById('overlay_' + this.activeId);
    nextEl.style.display = 'block';
    nextEl.style.position = 'fixed';

    nextEl.style.zIndex = '999';

    if (this.topBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the topbar
      x = x - (mainElement.width);
      y = mainElement.bottom + 20;
      // y remains same 
    } else if (this.sideBarElements.indexOf(this.activeId) !== -1) {
      // means this element is in the sidebar
      x = mainElement.right;
      y = y - (mainElement.height / 2);
      // x remains same
    }
    nextEl.style.left = x + 'px';
    nextEl.style.top = y + 'px';
  }
  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
    this.loadGraph = null;
  }

  private doActivity() {
    this.getDashboardData();
  }

  getDashboardData () {
    this.dashboardService.getViewingPlatformDetailForDashboard(this.platformId).subscribe(
      res => {
        let selectedPlatform;
        if (res && res['platforms'] && res['platforms'].length > 0) {
          selectedPlatform = res['platforms'].filter(item => {
            return item.platformId === this.platformId;
          });
        }
        this.platformResults = <PublisherDashboard>res;
        this.platformResults.platforms = (selectedPlatform ? selectedPlatform : this.platformResults.platforms);
        if (this.platformResults.platforms && this.platformResults.platforms.length > 0) {
          this.platformResults.platforms[0].openState = true;
        }
        this.publisherId = this.platformResults.publisherId;
        this.publisherPlatformService.getViewingPlatform(this.publisherId).subscribe(
          result => {
            this.watchingPlatform = result;
            if (!this.checkIfValidDate(this.saveFilterForm.get('startDate').value)) {
              this.saveFilterForm.get('startDate').setValue(result.created.at);
            }
            if (!this.checkIfValidDate(this.saveFilterForm.get('endDate').value)) {
              this.saveFilterForm.get('endDate').setValue(this.todayDate);
            }
            if (this.loadGraph) {
              this.loadPerformanceTracking();
            }
          });
        },
      err => {
        console.log('Error DashboardComponent : ' + err);
      }
    );
  }

  private checkIfValidDate(d) {
    return d instanceof Date;
  }

  openSelectedPlatform(selectedPlatform: any) {
    this.platformResults.platforms.forEach(item => {
      item.openState = false;
      if (item.platformId === selectedPlatform.platformId) {
        item.openState = true;
      }
    });
  }

  getMetricsValue(metricsObj: any[], type: string) {
    if (metricsObj && metricsObj !== undefined) {
      console.log('Metrics Object : ' + metricsObj.find(x => x.event === type));
      return (metricsObj.find(x => x.event === type));
    }
    return '-';
  }

  loadPerformanceTracking() {
    let startDate = new Date(this.saveFilterForm.get('startDate').value);
    let endDate = new Date(this.saveFilterForm.get('endDate').value);

    if (this.saveFilterForm.get('duration').value === 'Custom Duration') {
      startDate = new Date(new Date(this.saveFilterForm.get('startDate').value).setHours(0o0, 0o0, 0o0));
      endDate = new Date(new Date(this.saveFilterForm.get('endDate').value).setHours(23, 59, 59));
    }
    const sDate = startDate.toString();
    const eDate = endDate.toString();
    const performanancePostData = {
      startDate: sDate,
      endDate: eDate,
      publisherId: this.publisherId,
      platformId: this.platformId,
      timezone: new Date().getTimezoneOffset()
    };
    this.dashboardService.getPerformananceAnalyticsData(performanancePostData).subscribe(
      response => {
        this.performanceTrackingResults = response;
        if (this.performanceTrackingResults && this.performanceTrackingResults.data.length) {
          this.drawConversionResultLineChart(this.performanceTrackingResults);
        } else {
          this.resetGraph();
        }
      },
      (err: Error) => {
        console.log(err);
    });
  }

  drawConversionResultLineChart(performanceTrackingResults: any) {
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    const toggleValues = Array.from( this.toggleStateMap.values() );
    let dataRows = [];

    toggleValues.forEach(toggleElement => {
      if (this.toggleStateMap.get(toggleElement) !== undefined) {
        data.addColumn('number', this.toggleStateMap.get(toggleElement));
      }
    });

    const chartData: any[] = performanceTrackingResults.data;
    for (const c of chartData) {
      dataRows = [];
      dataRows.push(c.durationInstance);
      toggleValues.forEach(toggleElement => {
        if ( this.toggleStateMap.get(toggleElement) !== undefined ) {
          if (this.toggleStateMap.get(toggleElement) === 'Earnings') {
            dataRows.push(c.totalEarnings);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPM') {
            dataRows.push(c.avgCPM);
          } else if (this.toggleStateMap.get(toggleElement) === 'CTR') {
            dataRows.push(c.ctr);
          } else if (this.toggleStateMap.get(toggleElement) === 'CPC') {
            dataRows.push(c.avgCPC);
          }
        }
      });
      data.addRow(dataRows);
    }

    const r = this.getSeriesAndVAxes();
    const options: any = {
      colors: ['#ea7971', '#73e2fd', '#1acfdf', '#7e98ff'],
      hAxis: { titleTextStyle: { color: '#333' } },
      chartArea: { 'width': '85%', 'height': '80%' },
      vAxes: r.vAxes,
      series: r.series,
      width: 973,
      height:350,
      legend: { position: 'none'}
    };

    const chart = new google.visualization.LineChart(document.getElementById('performanceTrackingLineChart'));
    chart.draw(data, options);
  }

  onSelectDuration(duration) {
    let date = new Date();
    this.saveFilterForm.get('endDate').setValue(date);
    if (duration === 'Custom Duration') {
      this.saveFilterForm.get('startDate').setValue(date);
      this.saveFilterForm.get('startDate').enable();
      this.saveFilterForm.get('endDate').enable();
    } else if (duration === 'Last 24 hours') {
      date = new Date(new Date().setHours(new Date().getHours() - 24));
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 48 hours') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setHours(new Date().getHours() - 48));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 week') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setDate(new Date().getDate() - 7));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 1 month') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 1));
      this.saveFilterForm.get('startDate').setValue(date);
    } else if (duration === 'Last 6 months') {
      this.saveFilterForm.get('startDate').disable();
      this.saveFilterForm.get('endDate').disable();
      date = new Date(new Date().setMonth(new Date().getMonth() - 6));
      this.saveFilterForm.get('startDate').setValue(date);
    }
    this.startDate = this.saveFilterForm.get('startDate').value;
    this.endDate = this.saveFilterForm.get('endDate').value;
    this.loadPerformanceTracking();
  }
  addStartDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.startDate = new Date(this.saveFilterForm.get('startDate').value);
    this.endDateMin = this.startDate;

    this.loadPerformanceTracking();
  }
  addEndDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDate = new Date(this.saveFilterForm.get('endDate').value);
    this.endDateMin = this.endDate;

    this.loadPerformanceTracking();
  }

  public twoToggle(toggleValue: any) {
    if (this.toggleStateMap.get(toggleValue) === undefined) {
      this.toggleStateMap.set(toggleValue, toggleValue);
      this.activeToggleArray.push(toggleValue);
      if (this.activeToggleArray.length > 2) {
        this['toggle' + this.activeToggleArray[0]] = undefined;
        this.toggleStateMap.set(this.activeToggleArray[0], undefined);
        this.activeToggleArray.shift();
      }
    } else if (this.toggleStateMap.get(toggleValue) === toggleValue) {
      this.toggleStateMap.set(toggleValue, undefined);
      this['toggle' + toggleValue] = undefined;
      this.activeToggleArray = this.activeToggleArray.filter(item => item !== toggleValue);
    }
    if (this.performanceTrackingResults && this.performanceTrackingResults.data.length) {
      this.drawConversionResultLineChart(this.performanceTrackingResults);
    }
  }

  private getSeriesAndVAxes(): any {

    const earningsColor = '#5287c6';
    const cpmColor = '#78c068';
    const cpcColor = '#f37a6e';
    const ctrColor = '#9a67ab';

    let series: any = {};
    let vAxes: any = {};

    let toggleValues = Array.from(this.toggleStateMap.values());
    toggleValues = toggleValues.filter(( element ) => {
      return element !== undefined;
    });

    if (toggleValues && toggleValues.length === 1) {
      if (toggleValues.includes('Earnings')) {
        series = {
          0: { color: earningsColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('$#', null);
      }
      if (toggleValues.includes('CPM')) {
        series = {
          0: { color: cpmColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('$#', null);
      }
      if (toggleValues.includes('CPC')) {
        series = {
          0: { color: cpcColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes('$#', null);
      }
      if (toggleValues.includes('CTR')) {
        series = {
          0: { color: ctrColor, targetAxisIndex: 0 }
        };
        vAxes = this.populateVAxes("#'%'", null);
      }
    }

    if (toggleValues && toggleValues.length === 2) {
      if (toggleValues.includes('Earnings') && toggleValues.includes('CPM')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CPM')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes('$#', '$#');
      }

      if (toggleValues.includes('Earnings') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('$#', "#'%'");
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes("#'%'", '$#');
        }
      }

      if (toggleValues.includes('Earnings') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('Earnings') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: earningsColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: earningsColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes('$#', '$#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CPC')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CPC')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
        } else {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
        }
        vAxes = this.populateVAxes('$#', '$#');
      }

      if (toggleValues.includes('CPM') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPM') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpmColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('$#', "#'%'");
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpmColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes("#'%'", '$#');
        }
      }

      if (toggleValues.includes('CPC') && toggleValues.includes('CTR')) {
        if (toggleValues.indexOf('CPC') < toggleValues.indexOf('CTR')) {
          series = {
            0: { color: cpcColor, targetAxisIndex: 0 },
            1: { color: ctrColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes('$#', "#'%'");
        } else {
          series = {
            0: { color: ctrColor, targetAxisIndex: 0 },
            1: { color: cpcColor, targetAxisIndex: 1 }
          };
          vAxes = this.populateVAxes("#'%'", '$#');
        }
      }
    }
    return {series, vAxes};
  }

  populateVAxes(yAxis1Format: string, yAxis2Format: string): any {
    let a: any = {};

    if (yAxis2Format) {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: '$#'
        },
        1: {
          minValue: 0,
          max: 100,
          format: "#'%'"
        }
      };
      a['0']['format'] = yAxis1Format;
      a['1']['format'] = yAxis2Format;
    } else {
      a = {
        viewWindow: { min: 0 },
        0: {
          viewWindowMode: 'explicit',
          minValue: 0,
          format: '$#'
        }
      };
      a['0']['format'] = yAxis1Format;
    }
    return a;
  }
  resetGraph() {
    const node = document.getElementById('performanceTrackingLineChart');
    if (node) {
      while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
      }
      const a = document.createElement('span');
        a.innerHTML =
        `<img src="../assets/images/graph/pub-conversion-result.jpg"
        alt="loading" style="width:1100px; height:352px">
        <p style="position: relative;  top:-211px;width: 400px; margin: auto !important;text-align: center;">
        You will start seeing analytics data once your slot starts receiving views, impressions and clicks.</p>`;
      document.getElementById('performanceTrackingLineChart').appendChild(a);
    }
  }

}
