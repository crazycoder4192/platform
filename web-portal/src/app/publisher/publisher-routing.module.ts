import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublisherComponent } from './publisher.component';
import {PublisherProfileComponent} from './publisher-profile/publisher-profile.component';
import {PublisherProfileThanksComponent} from './publisher-profile-thanks/publisher-profile-thanks.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { PaymentsComponent } from './payments/payments.component';
import { SettingsComponent } from './settings/settings.component';
import { AssetCreationComponent } from './asset-creation/asset-creation.component';
import { AssetManageComponent } from './asset-manage/asset-manage.component';
import { PlatformCreationComponent } from './platform-creation/platform-creation.component';
import { PublisherPlatformThanksComponent } from './publisher-platform-thanks/publisher-platform-thanks.component';
import { AuthGuard } from '../_guards/auth-guard.guard';
import { PlatformManageComponent } from './platform-manage/platform-manage.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PublisherSupportComponent } from './publisher-support/publisher-support.component';
import { PublisherTicketDetailsComponent } from './publisher-support/publisher-ticket-details/publisher-ticket-details.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { AssetThanksComponent } from './asset-creation/asset-thanks/asset-thanks.component';
import { PlatformThanksFileUploadComponent } from './platform-creation/platform-thanks-file-upload/platform-thanks-file-upload.component';
import { InvitationCodeComponent } from './invitation-code/invitation-code.component';

const routes: Routes = [
  {
    path: '', component: PublisherComponent, canActivate: [AuthGuard],
    children: [
      {
        path: '', canActivateChild: [AuthGuard],
        children: [
          { path: 'publisherProfile', component: PublisherProfileComponent},
          { path: 'publisherProfileThanks', component: PublisherProfileThanksComponent},
          { path: 'assetCreation', component: AssetCreationComponent},
          { path: 'assetThanks', component: AssetThanksComponent},
          { path: 'assetManage', component: AssetManageComponent},
          { path: 'analytics', component: AnalyticsComponent},
          { path: 'payments', component: PaymentsComponent},
          { path: 'settings', component: SettingsComponent},
          { path: 'platformCreation', component: PlatformCreationComponent},
          { path: 'platformEditFileUploadThanks', component: PlatformThanksFileUploadComponent},
          { path: 'platformCreationThanks', component: PublisherPlatformThanksComponent},
          { path: 'platformManage', component: PlatformManageComponent},
          { path: 'dashboard', component: DashboardComponent},
          { path: 'platformDashboard', component: DashboardDetailsComponent},
          { path: 'publisherSupport', component: PublisherSupportComponent},
          { path: 'ticketDetails/:id', component: PublisherTicketDetailsComponent},
          { path: 'invitationCode', component: InvitationCodeComponent},
          { path: '**', redirectTo: '/publisher/dashboard'}
        ]
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublisherRoutingModule { }
