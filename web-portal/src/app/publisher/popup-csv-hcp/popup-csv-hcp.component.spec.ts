import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCsvHcpComponent } from './popup-csv-hcp.component';

describe('PopupCsvHcpComponent', () => {
  let component: PopupCsvHcpComponent;
  let fixture: ComponentFixture<PopupCsvHcpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCsvHcpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCsvHcpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
