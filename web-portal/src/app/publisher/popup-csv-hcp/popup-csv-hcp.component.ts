import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HCPRecord } from 'src/app/_models/hcp_records';
import { PublisherHCPDetails } from 'src/app/_models/publisher-hcp';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { Router } from '@angular/router';
import { PublisherHCPFields } from 'src/app/_models/publisher_hcp_fields';


@Component({
  selector: 'app-popup-csv-hcp',
  templateUrl: './popup-csv-hcp.component.html',
  styleUrls: ['./popup-csv-hcp.component.scss']
})
export class PopupCsvHcpComponent implements OnInit {
  headerError: string;
  public todo = [];
  public done = [];
  public systemHeaders = [];
  public mappedList = [];
  public hcpRecordsForUpload = [];
  public listView: boolean;
  public displayedHcpCsvColumns: string[];
  public hcpRecordsForList = [];
  public publisherHCPDetails: PublisherHCPDetails[];
  public postHCPData: { hcpRecords: HCPRecord[] };
  public publisherHCPFields: PublisherHCPFields[];
  constructor(public matDialogRef: MatDialogRef<PopupCsvHcpComponent>,
    @Inject(MAT_DIALOG_DATA) public hcpData: any,
    public publisherPlatformService: PublisherPlatformService, public router: Router) {
    matDialogRef.disableClose = true;
  }

  ngOnInit() {
    this.listView = false;
    this.displayedHcpCsvColumns = ['regNumber', 'firstName', 'lastName', 'email',
    'dob', 'city', 'state', 'country', 'pincode', 'enableAds', 'status'];
    this.done = this.hcpData.hcpHeaders;
    this.initPublisherHCPFields();
  }
  initPublisherHCPFields() {
    this.publisherPlatformService.getPublisherHCPFields().subscribe(
      res => {
        this.publisherHCPFields = res;
        this.publisherHCPFields.forEach(element => {
          this.systemHeaders.push(element.fieldName);
        });
      },
      err => {
        console.log('Error!');
    });
  }
  onNoClick(): void {
    this.matDialogRef.close();
  }
  drop(event: CdkDragDrop<string[]>) {
    this.headerError = '';
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  saveHeaders() {
    if (this.todo.length === this.systemHeaders.length) {
      this.listView = true;
      this.todo.forEach((element, index) => {
        const mappedObj = {
          'systemField': this.systemHeaders[index],
          'csvField': element
        };
        this.mappedList.push(mappedObj);
      });
    //   this.hcpData.hcpRecords.forEach(thisHcp => {
    //     const tempObj = new HCPRecord();
    //     let err = '';
    //     this.mappedList.forEach(thisKeys => {
    //       if (thisKeys.systemField === 'Registration Number') {
    //         tempObj.registrationNumber = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'First Name') {
    //         tempObj.firstName = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'Last Name') {
    //         tempObj.lastName = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'Email') {
    //         tempObj.email = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'Date of Birth') {
    //         tempObj.dob = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'City') {
    //         tempObj.city = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'State') {
    //         tempObj.state = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'Country') {
    //         tempObj.country = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'Pincode') {
    //         tempObj.pincode = thisHcp[thisKeys.csvField];
    //       }
    //       if (thisKeys.systemField === 'Enable Ads') {
    //         tempObj.enableAds = thisHcp[thisKeys.csvField];
    //       }
    //       /* if (thisKeys.systemField === 'Address Line 1') {
    //          tempObj.addressLine1 = thisHcp[thisKeys.csvField];
    //        }
    //        if (thisKeys.systemField === 'Address Line 2') {
    //          tempObj.addressLine2 = thisHcp[thisKeys.csvField];
    //        }*/
    //     });

    //     if (tempObj.firstName === '') {
    //       err = 'FirstName ';
    //     }
    //     if (tempObj.lastName === '') {
    //       err = err + 'LastName ';
    //     }
    //     if (tempObj.email === '') {
    //       err = err + 'email ';
    //     }
    //     if (tempObj.dob === '') {
    //       err = err + 'Date of Birth ';
    //     }
    //     if (tempObj.city === '') {
    //       err = err + 'City ';
    //     }
    //     if (tempObj.state === '') {
    //       err = err + 'State ';
    //     }
    //     if (tempObj.country === '') {
    //       err = err + 'Country ';
    //     }
    //     if (tempObj.pincode === '') {
    //       err = err + 'Pincode ';
    //     }
    //   if (err !== '') {
    //     tempObj.isValid = false;
    //     tempObj.errMessege = 'Required ' + err + ' field(s) are empty or invalid.';
    //   }  else {
    //     tempObj.isValid = true;
    //     this.hcpRecordsForUpload.push(tempObj);
    //   }
    //   this.hcpRecordsForList.push(tempObj);
    // });

    // this.matDialogRef.close();
  } else {
  this.headerError = 'All fields are mandatory. Please map all system fields';
}
  }
uploadHCPDetails() {
  // this.postHCPData = {
  //   hcpRecords: this.hcpRecordsForUpload
  // };
  // this.publisherPlatformService.createPublisherHCP(this.postHCPData).subscribe(
  //   res => {
      this.matDialogRef.close();
    //   this.router.navigate(['/publisher/platformCreationThanks']);
    // },
    // err => {
    //   console.log('Unknown error occured');
    // });
}

}

