import { Component, OnInit, ViewEncapsulation, Inject, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { DOCUMENT } from '@angular/common';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { PublisherProfileService } from 'src/app/_services/publisher/publisher-profile.service';
import { PublisherPlatform } from 'src/app/_models/publisher_platform_model';
import { UtilService } from 'src/app/_services/utils/util.service';
import { Banner } from 'src/app/_models/utils';
import { element } from '@angular/core/src/render3';
import { Number } from 'src/app/common/validators/validator';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatSnackBar, MatDialog, MatStepper } from '@angular/material';
import { Router } from '@angular/router';
import { PublisherAsset } from 'src/app/_models/publisher_asset_model';
import { format } from 'ssf/types';
import { AssetCreationInstructionsComponent } from 'src/app/common/asset-creation-instructions/asset-creation-instructions.component';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { ToolTipService } from '../../_services/utils/tooltip.service';
import { Subject } from 'rxjs';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';
import { DeveloperShareComponent } from '../developer-share/developer-share.component';

@Component({
  selector: 'app-asset-creation',
  templateUrl: './asset-creation.component.html',
  styleUrls: ['./asset-creation.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class AssetCreationComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  private dom: Document;
  isLinear = false;
  codeInjectionScript = '';
  mobileASISnippet1 = '';
  mobileASISnippet2 = '';
  mobileASISnippet3 = '';
  basicSectionFormGroup: FormGroup;
  constraintSectionFormGroup: FormGroup;
  bidRangeSectionFormGroup: FormGroup;
  // platForms: PublisherPlatform[] = [];
  viewingPlatform: PublisherPlatform;
  banners: Banner[];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  basicTabs: Array<object> = [{
    name: 'Banner',
    size: 'KB',
    formats: [{
      displayName : 'JPEG',
      checked: false,
      selected: false
    },
    {
      displayName : 'PNG',
      checked: false,
      selected: false
    },
    {
      displayName : 'GIF',
      checked: false,
      selected: false
    }]
  }];
  /*,
  {
    name: 'Video',
    size: 'mb',
    formats: [{
      displayName : 'MOV',
      checked: false,
      selected: false
    },
    {
      displayName : 'MP4',
      checked: false,
      selected: false
    }]
  } */
  assetDetails = {
    basicSection: {
      name: '',
      url: '',
      platformName: ''
    },
    selectedBasicsTabName: {
      name: '',
      size: '',
      formats: []
    },
    selectedPlatform: PublisherPlatform,
    selectedBannerType: '',
    maxSize: null,
    minCpcBidRange: null,
    maxCpcBidRange: null,
    minCpmBidRange: null,
    maxCpmBidRange: null
  };

  public assetId: string;
  public assetType = 'Banner';
  public assetInfo: PublisherAsset;
  selecetedFormatType: object;
  isBannerAndValue: boolean;
  formatsArray: any[] = [];
  activeAddAsset: boolean;
  public processing = false;
  platformUrl: any;
  private edittingAssetName = '';
  private currentTab = '';
  public assetNameNotAvailable = false;
  private publisherId = '';
  public assetVerified: boolean;
  public assetStatus: string;
  public focusURLValue = false;
  private ignoreURLCheck = false;
  private ignoreDetailsCheck = false;
  public codeInjectionScriptUpdated = false;
  public assetVerificationPending = false;
  bidRanges: Object;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['publisher_asset_creation_basics_page_url', 'publisher_asset_creation_basics_banner_name',
  'publisher_asset_creation_details_banner_type', 'publisher_asset_creation_constraints_maximum_size',
  'publisher_asset_creation_constraints_permissible_formats', 'publisher_asset_creation_bid_range_cpm_bid_range',
  'publisher_asset_creation_bid_range_cpc_bid_range', 'publisher_asset_creation_banner_api_instructions',
  'publisher_asset_creation_banner_api_code_injection', 'publisher_asset_creation_banner_api_status',
  'publisher_asset_creation_cpm_bid_range', 'publisher_asset_creation_cpc_bid_range']);
  codeInjectionConfirmation: number;
  codeInjectionTypes: any[] = [
    {id: 0, value: 'I have read the instructions and will add the code later.'},
    {id: 1, value: 'I have added the code on my platform.'},
    {id: 2, value: 'I have added the code as well as reviewed the test ad.'}];

    seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
    private currencySymbol : string;
  assetPostUpdateInfo: PublisherAsset;
  marketSelection: any;

  constructor(private _formBuilder: FormBuilder, private assetHTTPService: AssetService,
    private snackBar: MatSnackBar, private utilHTTPService: UtilService, @Inject(DOCUMENT) dom: Document,
    private spinner: SpinnerService, private router: Router, public dialog: MatDialog,
    private platformService: PublisherPlatformService,
    private publisherProfileService: PublisherProfileService,
    private toolTipService: ToolTipService,
    private eventEmitterService: EventEmitterService,
    private amplitudeService: AmplitudeService,
    private publisherUserService: PublisherUserService
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
      localStorage.setItem('activeIcon', JSON.stringify(''));
      this.eventEmitterService.onClickNavigation();
      if (this.router.getCurrentNavigation().extras.state) {
        if (this.router.getCurrentNavigation().extras.state.assetType) {
          this.assetType = JSON.parse(this.router.getCurrentNavigation().extras.state.assetType);
        }
        if (this.router.getCurrentNavigation().extras.state.assetId) {
          this.assetId = JSON.parse(this.router.getCurrentNavigation().extras.state.assetId);
        }
      }

      this.spinner.showSpinner.next(true);
      this.dom = dom;
      this.assetDetails.selectedBasicsTabName = {name: 'Banner', size: 'KB', formats: [
      {displayName : 'JPEG' , checked: false},
      {displayName : 'PNG' , checked: false},
      {displayName : 'GIF' , checked: false}]};
  }

  ngOnInit() {
    this.currencySymbol = this.utilHTTPService.getCurrencySymbol();
    this.publisherUserService.getPublisherDetails(JSON.parse(localStorage.getItem('currentUser')).email).subscribe(
    // this.publisherProfileService.getProfileByEmail(JSON.parse(localStorage.getItem('currentUser')).email).subscribe(
      res => {
        if (res) {
          this.publisherId = res._id;
          if (this.publisherId !== undefined) {
            this.getViewingPlatform();
          }
        } else {
          this.router.navigate(['/publisher/publisherProfile']);
        }
      },
      err => {
        this.router.navigate(['/publisher/publisherProfile']);
      }
    );
    this.basicSectionFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      url: ['', Validators.required],
      platformName: [{ value: '', disabled: true }, Validators.required]
    });
    this.constraintSectionFormGroup = this._formBuilder.group({
      maxSize: ['', Validators.compose([Validators.required, Number, Validators.min(1), Validators.max(200)])],
      format: ['', Validators.required]
    });
    this.bidRangeSectionFormGroup = this._formBuilder.group({
      minCpcBidRange: ['', Validators.compose([
        Validators.required,
        Number, Validators.min(0),
        Validators.max(499),
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ])],
      maxCpcBidRange: ['', Validators.compose([
        Validators.required, Number,
        Validators.min(0), Validators.max(500),
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ])],
      minCpmBidRange: ['', Validators.compose([
        Validators.required,
        Number,
        Validators.min(0),
        Validators.max(1000),
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ])],
      maxCpmBidRange: ['', Validators.compose([
        Validators.required,
        Number,
        Validators.min(0),
        Validators.max(1000),
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ])],
    });
    // call if edit asset.

    this.isBannerAndValue = true;
    this.activeAddAsset  = false;
    this.processing = true;
    this.fetchTooltips(this.toolTipKeyList);
    this.amplitudeService.logEvent('slot_addition - click new slot', null);

  }
  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    })
  }
  getBidRangeMinMax() {
    this.assetHTTPService.getSuggestedBidRange(this.viewingPlatform._id).subscribe(
      res => {
        this.bidRanges = res;
        this.bidRanges['displaySuggestedMaxCPC'] =  Math.round(this.bidRanges['suggestedMaxCPC']);
        this.bidRanges['displaySuggestedMaxCPM'] = Math.round(this.bidRanges['suggestedMaxCPM']);
        this.bidRanges['displaySuggestedMinCPC'] = Math.round(this.bidRanges['suggestedMinCPC']);
        this.bidRanges['displaySuggestedMinCPM'] = Math.round(this.bidRanges['suggestedMinCPM']);

        // this.bidRanges['suggestedMaxCPC'] =  Math.round(this.bidRanges['suggestedMaxCPC']);
        // this.bidRanges['suggestedMaxCPM'] = Math.round(this.bidRanges['suggestedMaxCPM']);
        // this.bidRanges['suggestedMinCPC'] = Math.round(this.bidRanges['suggestedMinCPC']);
        // this.bidRanges['suggestedMinCPM'] = Math.round(this.bidRanges['suggestedMinCPM']);
        this.bidRanges['suggestedMaxCPC'] =  1000;
        this.bidRanges['suggestedMaxCPM'] = 1000;
        this.bidRanges['suggestedMinCPC'] = 0;
        this.bidRanges['suggestedMinCPM'] = 0;
        this.bidRangeSectionFormGroup.reset();
        this.bidRangeSectionFormGroup = this._formBuilder.group({
          minCpcBidRange: ['', Validators.compose([
            Validators.required, Number,
            Validators.min(this.bidRanges['suggestedMinCPC']),
            Validators.max(this.bidRanges['suggestedMaxCPC']),
            Validators.pattern(/^-?(0|[1-9]\d*)?$/)
          ])],
          maxCpcBidRange: ['', Validators.compose([
            Validators.required, Number,
            Validators.min(this.bidRanges['suggestedMinCPC']),
            Validators.max(this.bidRanges['suggestedMaxCPC']),
            Validators.pattern(/^-?(0|[1-9]\d*)?$/)
          ])],
          minCpmBidRange: ['', Validators.compose([
            Validators.required, Number,
            Validators.min(this.bidRanges['suggestedMinCPM']),
            Validators.max(this.bidRanges['suggestedMaxCPM']),
            Validators.pattern(/^-?(0|[1-9]\d*)?$/)
          ])],
          maxCpmBidRange: ['', Validators.compose([
            Validators.required, Number,
            Validators.min(this.bidRanges['suggestedMinCPM']),
            Validators.max(this.bidRanges['suggestedMaxCPM']),
            Validators.pattern(/^-?(0|[1-9]\d*)?$/)
          ])],
        });
        if (this.assetId) {
          this.bidRangeSectionFormGroup.get('minCpcBidRange').setValue(this.assetInfo.bidRange.cpc.min);
          this.bidRangeSectionFormGroup.get('maxCpcBidRange').setValue(this.assetInfo.bidRange.cpc.max);
          this.bidRangeSectionFormGroup.get('minCpmBidRange').setValue(this.assetInfo.bidRange.cpm.min);
          this.bidRangeSectionFormGroup.get('maxCpmBidRange').setValue(this.assetInfo.bidRange.cpm.max);
        }
      },
      err => {
        console.log(err.err);
      }
    );
  }
  getViewingPlatform() {
    this.platformService.getViewingPlatform(this.publisherId).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.viewingPlatform = <PublisherPlatform>res;

        if (this.viewingPlatform && this.viewingPlatform.platformType === 'MobileApp') {
          this.basicSectionFormGroup = this._formBuilder.group({
            name: ['', Validators.required],
            platformName: [{ value: '', disabled: true }, Validators.required]
          });
        } else {
          this.basicSectionFormGroup = this._formBuilder.group({
            name: ['', Validators.required],
            url: ['', Validators.required],
            platformName: [{ value: '', disabled: true }, Validators.required]
          });
        }
        this.basicSectionFormGroup.controls['platformName'].setValue(this.viewingPlatform.name);

        this.getBidRangeMinMax();
        this.setPageURL(this.viewingPlatform);
        this.loadBanners();

        if (this.assetId) {
          this.initEditAsset();
          const basicTab =   this.basicTabs.find((item) => item['name'] === this.assetType);
          this.basicTabChange(basicTab);
        }
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getPlatforms : ' + err);
      }
    );
  }
  loadBanners() {
    this.utilHTTPService.getBanners().subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        const banner = <Banner[]>res;
        this.banners = banner.filter((obj: any) => {
          return obj.platform.toLowerCase() === this.viewingPlatform.platformType.toLowerCase();
        });
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Error! getBanners : ' + err);
      }
    );
  }
  basicTabChange(selTabName) {
    this.assetDetails.selectedBasicsTabName = selTabName;
    this.assetType = selTabName.name;
    if (selTabName.name === 'Video') {
      this.assetDetails.selectedBannerType = '';
    }
    if (!this.assetId && this.currentTab !== selTabName) {
      this.basicSectionFormGroup.get('name').setValue('');
      this.basicSectionFormGroup.get('url').setValue('');
      this.constraintSectionFormGroup.reset();
      this.bidRangeSectionFormGroup.reset();
    }
    this.currentTab = selTabName;
  }

  selectBanner(selBanner) {
    this.assetDetails.selectedBannerType = selBanner;
  }
  copyElementText(id: any) {
    let docElement = null; // Should be <textarea> or <input>
    try {
        docElement = this.dom.getElementById(id);
        docElement.select();
        this.dom.execCommand('copy');
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: 'Text copied to clipboard'
          }
        });
    }
    finally {
      this.dom.getSelection().removeAllRanges();
    }
  }
  validateAssetCreationFirstStep() {
    let isValid = true;
    const formGroup = this.basicSectionFormGroup;
    const nameCtrl = formGroup.controls['name'];
    const urlCtrl = formGroup.controls['url'];
    const platformNameCtrl = formGroup.controls['platformName'];

    if (!nameCtrl.value.trim()) {
      nameCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (this.viewingPlatform.platformType === 'Website') {
      if (!urlCtrl.value.trim()) {
        urlCtrl.setErrors({ require: true });
        isValid = false;
      } else if (urlCtrl.value && this.assetId) {
        const assetURL = (!urlCtrl.value.trim().startsWith('/') ? '/' : '') + urlCtrl.value.trim();
          if ( assetURL.toLowerCase() !== this.assetInfo.url.toLowerCase() && !this.ignoreURLCheck ) {
            this.assetDetailChanged('URL');
            isValid = false;
          }
      }
    }
    
    this.focusOutURL();
    return isValid;
  }

  validateName() {
    if (this.validateAssetCreationFirstStep()) {
      const formGroup = this.basicSectionFormGroup;
      const nameCtrl = formGroup.controls['name'];
      const value: string = nameCtrl.value.trim();
      const assetName = this.edittingAssetName.toLowerCase();
      if (value && value.length > 2 && value.toLowerCase() !== assetName) {
        this.spinner.showSpinner.next(true);
        this.assetHTTPService.checkAssetNameAvailablity({ name: value, email: this.userDetails.email}).subscribe(
          res => {
            this.spinner.showSpinner.next(false);
            nameCtrl.clearValidators();
            this.stepper.next();
            if (this.assetId && this.assetInfo && this.assetInfo.assetStatus === 'Active') {
              this.saveAssetDetailsAndGenrScript();
            }
          },
          err => {
            this.spinner.showSpinner.next(false);
            nameCtrl.setErrors({ nameNotAvailable: true });
          }
        );
      } else if (value && value.length <= 2) {
        nameCtrl.setErrors({ lessLength: true });
      } else if (value && value.length > 2 && value.toLowerCase() === assetName && this.assetInfo &&
        this.assetInfo.assetStatus === 'Active') {
        this.saveAssetDetailsAndGenrScript();
      } else {
        this.stepper.next();
      }
    }

    this.amplitudeService.logEvent('slot_addition - add basic details', null);
    return false;
  }
  validateBanner() {
    const selectedBannerSize = this.assetDetails.selectedBannerType['size'];
    if (this.assetType && !selectedBannerSize && !this.assetId) {
      this.stepper.selectedIndex = 1;
      this.isBannerAndValue = false;
    } else if (this.assetType &&  this.assetId && selectedBannerSize !== this.assetInfo.assetDimensions && !this.ignoreDetailsCheck) {
      this.stepper.selectedIndex = 1;
      this.assetDetailChanged('Details');
    } else {
      this.isBannerAndValue = true;
    }
    let eventProperties = {
      slotDimension: selectedBannerSize
    }
    this.amplitudeService.logEvent('slot_addition - select dimension', eventProperties);
  }
  validateAssetCreationConstraint(): boolean {
    this.amplitudeService.logEvent('slot_addition - select constraints', null);
    let isValid = true;
    const formGroup = this.constraintSectionFormGroup;
    const maxSizeCtrl = formGroup.controls['maxSize'];
    const formatCtrl = formGroup.controls['format'];

    if (!maxSizeCtrl.value) {
      maxSizeCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (this.formatsArray.length === 0) {
      formatCtrl.setErrors({ require: true });
      isValid = false;
      if (this.assetType === 'Video') {
        this.stepper.selectedIndex = 1;
      } else {
        this.stepper.selectedIndex = 2;
      }
    }
    if (this.constraintSectionFormGroup.invalid) {
      isValid = false;
    }

    if (isValid && this.isBannerAndValue) {
      this.activeAddAsset = true;
      if (!this.assetVerified) {
        this.stepper.next();
      }
    }

    return isValid;
  }

  validateAssetCreationBidRange(): boolean {
    let isValid = true;
    const formGroup = this.bidRangeSectionFormGroup;
    const minCpcBidRangeCtrl = formGroup.controls['minCpcBidRange'];
    const maxCpcBidRangeCtrl = formGroup.controls['maxCpcBidRange'];
    const minCpmBidRangeCtrl = formGroup.controls['minCpmBidRange'];
    const maxCpmBidRangeCtrl = formGroup.controls['maxCpmBidRange'];

    const maxCpcBidRangeVal = maxCpcBidRangeCtrl.value;
    const minCpcBidRangeVal = minCpcBidRangeCtrl.value;
    const minCpmBidRangeVal = minCpmBidRangeCtrl.value;
    const maxCpmBidRangeVal = maxCpmBidRangeCtrl.value;

    if (!minCpcBidRangeVal && minCpcBidRangeVal !== 0 ) {
      minCpcBidRangeCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (!maxCpcBidRangeVal && maxCpcBidRangeVal !== 0 ) {
      maxCpcBidRangeCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (minCpcBidRangeVal && minCpcBidRangeVal !== maxCpcBidRangeVal ) {
      if (maxCpcBidRangeVal < minCpcBidRangeVal) {
        maxCpcBidRangeCtrl.setErrors({ invalid: true });
        isValid = false;
      }
    }

    if (!minCpmBidRangeVal && minCpmBidRangeVal !== 0 ) {
      minCpmBidRangeCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (!maxCpmBidRangeVal  && maxCpmBidRangeVal !== 0 ) {
      maxCpmBidRangeCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (minCpmBidRangeVal && minCpmBidRangeVal !== 0 && minCpmBidRangeVal !== maxCpmBidRangeVal ) {
      if (maxCpmBidRangeVal < minCpmBidRangeVal) {
        maxCpmBidRangeCtrl.setErrors({ invalid: true });
        isValid = false;
      }
    }

    if (isValid && this.isBannerAndValue) {
      this.activeAddAsset = true;
    }
    if (!this.bidRangeSectionFormGroup.valid) {
      isValid = false;
    }

    if (isValid) {
      this.saveAssetDetailsAndGenrScript();
      let eventProperties = {
        minCpmBidRangeVal: minCpmBidRangeVal,
        maxCpmBidRangeVal: maxCpmBidRangeVal,
        minCpcBidRangeVal: minCpcBidRangeVal,
        maxCpcBidRangeVal: maxCpcBidRangeVal
      }
      this.amplitudeService.logEvent('slot_addition - create bid range', eventProperties);
    }
    return isValid;
  }

  disableSave() {
    this.activeAddAsset  = false;
  }
  saveAssetDetailsAndGenrScript() {
    this.ignoreURLCheck = true;
    let assetSize = '';
    let assetURL = '';
    if (this.validateAssetCreationFirstStep() && this.validateAssetCreationConstraint()) {
      this.spinner.showSpinner.next(true);
      const assetPostData = this.prepareAssetPostData();
      //this is the last step before we generate the code
      //we need the status as verification pending here so
      //that the user as soon as pastes the ad, we are able
      //to show him the ad

      //assetPostData.assetStatus = 'Verification Pending';
      //commented  above line to  fix publishing ads to deactived then activated assets.
      assetPostData.assetStatus = 'New';
      if (!this.assetId) {
       this.assetHTTPService.storeAsset(assetPostData).subscribe(
          res => {
            this.assetId = res['_id'];
            this.spinner.showSpinner.next(false);
            if (res) {
              this.assetId = res['_id'];
              this.assetStatus = res['assetStatus'];
              this.codeInjectionScript = res['codeSnippet'];

              if (this.viewingPlatform.platformType.toLowerCase() === 'mobileapp') {
                this.mobileASISnippet1 = res['mobileASISnippet1'];
                this.mobileASISnippet2 = res['mobileASISnippet2'];
                this.mobileASISnippet3 = res['mobileASISnippet3'];
              }

              this.initEditAsset();
              this.stepper.next();
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: 'Slot created successfully'
                }
              });
            }
            // this.router.navigate(['/publisher/assetManage']);
          },
          err => {
            this.spinner.showSpinner.next(false);
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
            console.log('Error! saveAssetDetailsAndGenrScript : ' + err);
          });
          this.ignoreURLCheck = false;
      } else {
        assetPostData._id = this.assetId;
        assetPostData.assetStatus = this.assetInfo.assetStatus;
        assetSize = this.assetInfo.assetDimensions;
        assetURL = this.assetInfo.url;
        this.assetHTTPService.updateAsset(assetPostData).subscribe(
          res => {
            this.spinner.showSpinner.next(false);
              this.assetId = res['_id'];
              this.assetStatus = res['assetStatus'];
              this.codeInjectionScript = res['codeSnippet'];
              if (this.assetStatus !== 'Active' && ( assetSize !== res['assetDimensions'] || assetURL !== res['url'] )) {
                this.initEditAsset();
                this.stepper.selectedIndex = 4;
                this.codeInjectionScriptUpdated = true;
              } else {
                this.stepper.selectedIndex = 4;
                //this.router.navigate(['/publisher/assetManage']);
              }
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'success',
                  message: 'Slot updated successfully'
                }
              });
              this.amplitudeService.logEvent('slot_addition - select constraints', null);
          },
          err => {
            this.spinner.showSpinner.next(false);
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
            console.log('Error! saveAssetDetailsAndGenrScript : ' + err);
          }
        );
        this.ignoreURLCheck = false;
      }

    }
  }
  
  callOnChangeFormat(item: any, event) {
    this.formatsArray = [];
    if (!event.checked) {
      item['checked'] = false;
    }
    this.assetDetails.selectedBasicsTabName.formats.forEach((thisItem) => {
      if (thisItem['selected'] || thisItem['checked']) {
        this.formatsArray.push(thisItem);
      }
    });
  }

  prepareAssetPostData() {
    const selectedFormats: Array<string> = [];
    this.assetDetails.selectedBasicsTabName.formats.forEach(item => {
      if (item['selected'] || item['checked']) {
        selectedFormats.push(item['displayName']);
      }
    });
  //  const selPlatformDetails = this.platForms.find((item) => item._id === this.assetDetails.selectedPlatform['_id']);
    const basicSection = this.basicSectionFormGroup;
    const assetPostData = {
      '_id': '',
      'name': basicSection.get('name').value,
      'url': (!this.assetDetails.basicSection.url.startsWith('/') ? '/' : '') +  this.assetDetails.basicSection.url,
      'platformId': this.viewingPlatform._id,
      'fullUrl' : this.platformUrl + this.assetDetails.basicSection.url,
      'assetType' : this.assetDetails.selectedBasicsTabName.name,
      'assetDimensions' : this.assetDetails.selectedBannerType['size'],
      'fileFormats' : selectedFormats,
      'assetSize' : this.assetDetails.maxSize,
      'assetUnit': this.assetDetails.selectedBasicsTabName.size,
      'assetStatus' : 'New',
      'isVerified' : false,
      'code_snippet' : '',
      'deleted': false,
      'bidRange': {
        'cpc': {
            'min': this.bidRangeSectionFormGroup.get('minCpcBidRange').value,
            'max': this.bidRangeSectionFormGroup.get('maxCpcBidRange').value
        },
        'cpm': {
            'min': this.bidRangeSectionFormGroup.get('minCpmBidRange').value,
            'max': this.bidRangeSectionFormGroup.get('maxCpmBidRange').value,
        }
      }
    };
    return assetPostData;
  }
  getStatusForAsset() {
    this.spinner.showSpinner.next(true);
    this.assetHTTPService.getSelectedAssetDetails(this.assetId).subscribe(
       res => {
        this.spinner.showSpinner.next(false);
          this.assetInfo = <PublisherAsset>res;
          this.assetStatus =  res['assetStatus'];
          if (this.assetStatus === 'Active') {
            this.assetVerified = true;
          }
       },
       err => {
        this.spinner.showSpinner.next(false);
         console.log('Error! getStatusForAsset : ' + err);
       }
     );
  }
  initEditAsset() {
    this.formatsArray = [];
    this.assetHTTPService.getSelectedAssetDetails(this.assetId).subscribe(
       res => {
        this.assetInfo = <PublisherAsset>res;
        this.mobileASISnippet1 = this.assetInfo['mobileASISnippet1'];
        this.mobileASISnippet2 = this.assetInfo['mobileASISnippet2'];
        this.mobileASISnippet3 = this.assetInfo['mobileASISnippet3'];
        this.assetStatus = this.assetInfo.assetStatus;
        if (this.assetStatus === 'Active') {
          this.assetVerified = true;
        }
        this.selecetedFormatType = this.basicTabs.find(obj => obj['name'] === this.assetInfo.assetType);
        this.basicSectionFormGroup.get('name').setValue(this.assetInfo.name);
        if (this.viewingPlatform && this.viewingPlatform.platformType === 'Website') {
          this.basicSectionFormGroup.get('url').setValue(this.assetInfo.url.slice(1));
        }
        if (this.viewingPlatform) {
          const platform = this.viewingPlatform;
          const trimmedPlatformUrl = this.platformUrl.trim();
          const lastCharOfAssetUrl = trimmedPlatformUrl.charAt(trimmedPlatformUrl.length - 1);

          if (lastCharOfAssetUrl === '/') {
            this.platformUrl = trimmedPlatformUrl;
          } else {
            this.platformUrl = trimmedPlatformUrl + '/';
          }

          this.basicSectionFormGroup.get('platformName').setValue(platform.name);
          this.assetDetails.selectedPlatform['_id'] = platform._id;
        }
        if (this.assetInfo.assetType === 'Banner') {
          const findedBanner = this.banners.find((item) => item.size === this.assetInfo.assetDimensions);
          this.selectBanner(findedBanner);
        }
        this.constraintSectionFormGroup.get('maxSize').setValue(this.assetInfo.assetSize);
        if (this.assetInfo.bidRange) {
          this.bidRangeSectionFormGroup.get('minCpcBidRange').setValue(this.assetInfo.bidRange.cpc.min);
          this.bidRangeSectionFormGroup.get('maxCpcBidRange').setValue(this.assetInfo.bidRange.cpc.max);
          this.bidRangeSectionFormGroup.get('minCpmBidRange').setValue(this.assetInfo.bidRange.cpm.min);
          this.bidRangeSectionFormGroup.get('maxCpmBidRange').setValue(this.assetInfo.bidRange.cpm.max);
        } else {
          this.bidRangeSectionFormGroup.get('minCpcBidRange').setValue(0);
          this.bidRangeSectionFormGroup.get('maxCpcBidRange').setValue(0);
          this.bidRangeSectionFormGroup.get('minCpmBidRange').setValue(0);
          this.bidRangeSectionFormGroup.get('maxCpmBidRange').setValue(0);
        }
        this.selecetedFormatType['formats'].forEach(item => {
          const findedFormat = this.assetInfo.fileFormats.find(obj => obj === item.displayName);
          if (findedFormat) {
            item.checked = true;
            this.formatsArray.push(item);
          }
        });
        this.codeInjectionScript = this.assetInfo.codeSnippet;
        this.edittingAssetName = this.assetInfo.name;
        this.codeInjectionConfirmation = parseInt(this.assetInfo.addedSlotScriptOption, 10);
       },
       err => {
         console.log('Error! getStatusForAsset : ' + err);
       }
     );
  }
  deActivateAsset() {
    const assetPostData = this.prepareAssetPostData();
    assetPostData._id = this.assetId;
    assetPostData.assetStatus = 'Inactive';
    assetPostData.isVerified = false;
    this.assetHTTPService.updateAsset(assetPostData).subscribe(
       res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: 'Slot deactivated Successfully'
          }
        });
        this.router.navigate(['/publisher/assetManage']);
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }
  activateAsset() {
    this.assetHTTPService.getSelectedAssetDetails(this.assetId).subscribe( res => {
      const assetPostData = <PublisherAsset>res;
      assetPostData.isVerified = false;
      if (res['assetStatus'] === 'Inactive') {
        assetPostData.assetStatus = 'New';
      }

      this.assetHTTPService.updateAsset(assetPostData).subscribe(
        result => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'success',
              message: 'Slot activated Successfully'
            }
          });
         this.router.navigate(['/publisher/assetManage']);
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
      });
    });
  }
  deleteAsset() {
    this.assetHTTPService.deleteAsset(this.assetId).subscribe(
      data => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: 'Slot Deleted Successfully'
          }
        });
        this.router.navigate(['/publisher/assetManage']);
       },
       err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
       }
     );
  }
  openDeleteCofirmDialog(type: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Are you sure you want to ' + type + ' this slot ?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && type === 'delete') {
        this.deleteAsset();
      } else if (result && type === 'deactivate') {
        this.deActivateAsset();
      } else if (result && type === 'activate') {
        this.activateAsset();
      }
    });
  }
  setPageURL(thisPlatform) {
    const trimmedPlatformUrl = thisPlatform.domainUrl.trim();
    const lastCharOfAssetUrl = trimmedPlatformUrl.charAt(trimmedPlatformUrl.length - 1);

    if (lastCharOfAssetUrl === '/') {
      this.platformUrl = trimmedPlatformUrl;
    } else {
      this.platformUrl = trimmedPlatformUrl + '/';
    }
  }
  editBasicTab() {
    this.stepper.selectedIndex = 0;
  }
  editConstraintsTab() {
    this.stepper.selectedIndex = 2;
  }
  editBidRangeTab() {
    this.stepper.selectedIndex = 3;
  }
  showBannerAPITab() {
    this.stepper.selectedIndex = 4;
  }
  focusURL() {
    if (this.assetId && !this.assetVerified) {
      this.focusURLValue = true;
    }
  }
  focusOutURL() {
    if (this.assetId) {
      this.focusURLValue = false;
    }
  }
  viewInstructions() {
    const dialogRef = this.dialog.open(AssetCreationInstructionsComponent, {
      width: '50%',
      data: {
        'title' : 'Do you want to AssetCreationInstructionsComponent this asset, This action cannot be undone !'
      }
    });
  }
  assetDetailChanged(value: string) {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title' : 'Any Change in the existing ' + value + ', will update the Banner API and you need to update the Banner' +
        'API on your platform for this slot. Do you want to proceed.'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        if (value === 'URL') {
          const formGroup = this.basicSectionFormGroup;
          const urlCtrl = formGroup.controls['url'];
          urlCtrl.setValue(this.assetInfo.url);
        }
        if (value === 'Details') {
          const findedBanner = this.banners.find((item) => item.size === this.assetInfo.assetDimensions);
          this.selectBanner(findedBanner);
        }
      } else {
        if (value === 'URL') {
          this.ignoreURLCheck = true;
          this.validateName();
        } else if (value === 'Details') {
          this.ignoreDetailsCheck = true;
          this.stepper.selectedIndex = 2;
        }
      }
      this.ignoreURLCheck = false;
      this.ignoreDetailsCheck = false;
    });
  }

  thanksPage() {
    if (this.codeInjectionConfirmation >= 0) {
      const selectedOption: number = this.codeInjectionConfirmation; // parseInt(this.codeInjectionConfirmation[0], 10);
      console.log(JSON.stringify(this.codeInjectionConfirmation));
      this.spinner.showSpinner.next(true);
      this.assetHTTPService.getSelectedAssetDetails(this.assetId).subscribe(
        res => {
          const asset = <PublisherAsset>res;
          this.router.navigate(['/publisher/assetThanks'],
            { state: {
              'assetName': JSON.stringify(this.edittingAssetName),
              'assetVerified': JSON.stringify(asset.isVerified),
              'assetDimensions': JSON.stringify(asset.assetDimensions),
              'fullUrl': JSON.stringify(asset.fullUrl),
              'codeInjectionSelectedOption': JSON.stringify(selectedOption),
              'platformId': JSON.stringify(this.viewingPlatform._id)
            }});
            this.spinner.showSpinner.next(false);
            let eventProperties = {
              bannerApiStatus: JSON.stringify(selectedOption)
            }
            this.amplitudeService.logEvent('slot_addition - banner api ', eventProperties);
        },
        err => {
          this.snackBar.openFromComponent(CustomSnackbarComponent, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['snackbar-publisher'],
            data: {
              icon: 'error',
              message: err.error['message']
            }
          });
          this.spinner.showSpinner.next(false);
        }
      );
    }
  }

  updateAssetStatus() {
    this.spinner.showSpinner.next(true);
    this.assetHTTPService.getSelectedAssetDetails(this.assetId).subscribe(
      res => {
        const asset = <PublisherAsset>res;
        if ( asset.assetStatus === 'New') {
          asset.assetStatus = 'Verification Pending';
          this.assetHTTPService.updateAsset(asset).subscribe( result => {
            this.assetInfo = <PublisherAsset>result;
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'success',
                message: 'Updated the asset integration status'
              }
            });
            this.spinner.showSpinner.next(false);
          },
          error => {
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: error.error['message']
              }
            });
            this.spinner.showSpinner.next(false);
          });
        } else {
          this.spinner.showSpinner.next(false);
        }
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
        this.spinner.showSpinner.next(false);
      });
  }

  openDeveloperShareModal(action) {
    const dialogRef = this.dialog.open(DeveloperShareComponent, {
      width: '70%',
      data: {
        'action' : action,
        'platformId': this.viewingPlatform._id,
        'assetId': this.assetId,
        'platformName': this.viewingPlatform.name,
        'marketSelection': this.marketSelection
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('closed dev share');
      }
    });
  }
  saveSlotScriptOption() {
    this.assetHTTPService.getSelectedAssetDetails(this.assetId).subscribe(
       res => {
          this.assetPostUpdateInfo = <PublisherAsset>res;
          this.assetPostUpdateInfo.addedSlotScriptOption = JSON.stringify(this.codeInjectionConfirmation);
          this.assetHTTPService.updateAsset(this.assetPostUpdateInfo).subscribe(
            result => {

            },
            err => {
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
          });
       },
       err => {
         console.log('Error! getSelectedAssetDetails : ' + err);
       }
     );
  }
}
