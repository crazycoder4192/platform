import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetThanksComponent } from './asset-thanks.component';

describe('AssetThanksComponent', () => {
  let component: AssetThanksComponent;
  let fixture: ComponentFixture<AssetThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
