import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { BankdetailsService } from 'src/app/_services/publisher/bankdetails.service';
import { Bankdetails } from 'src/app/_models/bankdetails.model';

@Component({
  selector: 'app-asset-thanks',
  templateUrl: './asset-thanks.component.html',
  styleUrls: ['./asset-thanks.component.scss']
})
export class AssetThanksComponent implements OnInit {

  public assetVerified: boolean;
  public assetName: string;
  public assetDimensions: string;
  public fullUrl: string;
  public mainMsg: string;
  public p1Msg: string;
  public p2Msg: string;
  platformId: any;
  bankDetails: any;
  rolePermissions: string[];

  constructor(public router: Router,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private bankDetailsService: BankdetailsService,
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.assetName = JSON.parse(this.router.getCurrentNavigation().extras.state.assetName);
      this.assetVerified = JSON.parse(this.router.getCurrentNavigation().extras.state.assetVerified);
      this.assetDimensions = JSON.parse(this.router.getCurrentNavigation().extras.state.assetDimensions);
      this.fullUrl = JSON.parse(this.router.getCurrentNavigation().extras.state.fullUrl);
      const selectedOption = JSON.parse(this.router.getCurrentNavigation().extras.state.codeInjectionSelectedOption);
      this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.platformId);

      if (this.assetVerified) {
        this.mainMsg = `Your Slot '${this.assetName}' has now been successfully added.`;
        if (selectedOption === 0) {
          this.p1Msg = 'We are able to verify the test ad against your slot.' +
          'You can now start tracking the earnings in the analytics section.';
        }
        if (selectedOption === 1) {
          this.p1Msg = 'You can now start tracking the earnings in the analytics section.';
        }
        if (selectedOption === 2) {
          this.p1Msg = 'You can now start tracking the earnings in the analytics section.';
        }
        
      } else {
        this.mainMsg = `Your Slot '${this.assetName}' has not been added yet.`;
        if (selectedOption === 0) {
          this.p1Msg = 'Do complete the pending steps to complete the successful addition of the slot and grow your earnings.';
        }
        if (selectedOption === 1) {
          this.p1Msg = 'We are unable to verify the test ad on your slot at this time. Please check again later.'
          this.p2Msg = 'If you have any questions related to addition of code on your platform, contact us at-support@doceree.com';
        }
        if (selectedOption === 2) {
          this.p1Msg = 'We are unable to verify the test ad against your slot. Please wait for sometime and try again. '
          this.p2Msg = 'You can also contact us at tech-support@doceree.com, if you have any technical questions for ' +
          'addition of code to your plaform.';
        }
      }
    }
  }

  ngOnInit() {
    this.getBankDetailsByPlatformId();
    this.rolePermissions = JSON.parse(localStorage.getItem('roleAndPermissions'));
  }
  getBankDetailsByPlatformId() {
    this.bankDetailsService.getAccountDetailsByPlatformId(this.platformId).subscribe(
      result => {
        this.bankDetails = result;
      },
      err => {
          console.log('Error! getAllInvoices : ' + err);
      });
  }
  navigateToPayments() {
    this.router.navigate(['/publisher/payments']);
  }
  navigateToAssets() {
    this.router.navigate(['/publisher/assetManage']);
  }

}
