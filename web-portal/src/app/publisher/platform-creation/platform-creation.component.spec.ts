import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformCreationComponent } from './platform-creation.component';

describe('PlatformCreationComponent', () => {
  let component: PlatformCreationComponent;
  let fixture: ComponentFixture<PlatformCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
