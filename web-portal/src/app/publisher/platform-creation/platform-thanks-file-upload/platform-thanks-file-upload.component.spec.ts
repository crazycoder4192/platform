import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformThanksFileUploadComponent } from './platform-thanks-file-upload.component';

describe('PlatformThanksFileUploadComponent', () => {
  let component: PlatformThanksFileUploadComponent;
  let fixture: ComponentFixture<PlatformThanksFileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformThanksFileUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformThanksFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
