import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';

@Component({
  selector: 'app-platform-thanks-file-upload',
  templateUrl: './platform-thanks-file-upload.component.html',
  styleUrls: ['./platform-thanks-file-upload.component.scss']
})
export class PlatformThanksFileUploadComponent implements OnInit {
  public platformName: string;
  public platformLink: string;
  public platformType: string;
  public isHCPUploaded: boolean;
  public platformId: string;

  constructor(public router: Router,
    private spinner: SpinnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private publisherPlatformService: PublisherPlatformService) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.platformName = JSON.parse(this.router.getCurrentNavigation().extras.state.platformName);
      this.platformLink = JSON.parse(this.router.getCurrentNavigation().extras.state.platformLink);
      this.platformType = JSON.parse(this.router.getCurrentNavigation().extras.state.platformType);
      this.isHCPUploaded = JSON.parse(this.router.getCurrentNavigation().extras.state.isHCPUploaded);
      this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.platformId);
      }

   }

  ngOnInit() {
  }

  navigateToAssetManage() {
    this.router.navigate(['/publisher/platformManage']);
  }

}
