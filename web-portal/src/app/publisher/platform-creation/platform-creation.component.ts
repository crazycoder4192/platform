import { Component, OnInit, ViewEncapsulation, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable , Subject } from 'rxjs';
import { PublisherPlatformService } from 'src/app/_services/publisher/publisher-platform.service';
import { map, startWith } from 'rxjs/operators';
import { AdvertiserUtilService } from 'src/app/_services/advertiser/advertiser-util.service';
import { PublisherHCPFields } from 'src/app/_models/publisher_hcp_fields';
import { PublisherProfileService } from 'src/app/_services/publisher/publisher-profile.service';
import { Router } from '@angular/router';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatDialog } from '@angular/material/dialog';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { HCPRecord } from 'src/app/_models/hcp_records';
import { MatStepper, MatRadioChange, MatSnackBar } from '@angular/material';
import { PublisherPlatform } from 'src/app/_models/publisher_platform_model';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DeleteConfirmPopupComponent } from 'src/app/common/delete-confirm-popup/delete-confirm-popup.component';
import { SpinnerService } from 'src/app/_services/spinner.service';
import { UtilService } from 'src/app/_services/utils/util.service';
import { AssetService } from 'src/app/_services/publisher/asset.service';
import { ValidateDomain } from 'src/app/common/validators/validator';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { EventEmitterService } from 'src/app/_services/event-emitter.service';
import { ToolTipService } from '../../_services/utils/tooltip.service' ;
import { blob } from 'd3';
import { CustomSnackbarComponent } from 'src/app/common/custom-snackbar/custom-snackbar.component';
import { DOCUMENT } from '@angular/common';
import { AmplitudeService } from 'src/app/_services/amplitude.service';
import { PublisherUserService } from 'src/app/_services/publisher/publisher-user.service';
import { DeveloperShareComponent } from '../developer-share/developer-share.component';
import { ConfigurationService } from 'src/app/_services/admin/configuration.service';
import { USEROLESElement } from 'src/app/admin/configuration/dataModel/userRolesPermissionsModel';
import { DashboardService } from 'src/app/_services/publisher/dashboard.service';

@Component({
  selector: 'app-platform-creation',
  templateUrl: './platform-creation.component.html',
  styleUrls: ['./platform-creation.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class PlatformCreationComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;
  public pageView = 'AddView';
  public platformFormGroup: FormGroup;
  public uniqueSiteTypes: string[];
  private uniqueApps: string[];
  public typeOfAccount: any = 'Website';
  public publisherPlatform: PublisherPlatform;
  private publisherId: string;
  public typesOfAdvertisers: string[] = [];
  public isValid = false;
  public loadingText = '';
  codeInjectionScript = '';
  private dom: Document;

  public progresValue = 0;
  arrayBuffer: any;
  file: File;
  public platformId: string;
  platform: any;
  public postHCPData: { publisherId: String, platformId: String, hcpRecords: HCPRecord[] };
  public fileTypes = ['CSV'];

  public selectedFileType = 'CSV';
  updatePlatformID: string;
  public publisherHCPField: PublisherHCPFields[];
  public headerError: string;
  public done: string[];
  public todo = [];
  public isFileSelected = false;
  public mappedList: any[];
  private userDetails = JSON.parse(localStorage.getItem('currentUser'));
  private edittingPlatformName = '';
  private edittingDomainURL = '';
  public activeAddPlatform: boolean;
  public editMode = false;
  public addMode = false;
  public hcpVerificationStatus;
  private hcpUploaded = false;
  private invalidFile;
  public urlReadOnly = false;
  tooltipList: [];
  returnedToolTipSubject = new Subject<any[]>();
  toolTipKeyList = new Set(['publisher_platform_creation_basics_type_of_account', 'publisher_platform_creation_basics_name_your_site',
  'publisher_platform_creation_basics_site_domain', 'publisher_platform_creation_basics_trusted_domains',
  'publisher_platform_creation_basics_site_type', 'publisher_platform_creation_basics_market',
  'publisher_platform_creation_basics_site_description', 'publisher_platform_creation_constraints_types_of_advertisers_to_exclude']);

  public headScriptAddedStatus: string;
  public headScriptAddedStatusOptions: string[] = ['I have read the instruction and will do the code injection later.',
                        'I have added the code on my platform.'];
  public invocationScriptAddedStatus: string;
  public invocationScriptAddedStatusOptions: string[] = ['I have read the instruction and will do the code injection later.',
                        'I have added the code on my platform.'];

  listOfExistedUserRolesPermissions: any;
  marketSelection = '1';
  allZonePlatforms: any;
  dashboardData: any;
  totalEarnings: any;
  totalImpressions: any;
  totalReach: any;
  appType: any;
  appTestKey: string;
  appLiveKey: string;

  constructor(
    private formBuilder: FormBuilder,
    private publisherPlatformService: PublisherPlatformService,
    private advertiserUtilService: AdvertiserUtilService,
    private publisherProfileService: PublisherProfileService,
    private router: Router,
    public dialog: MatDialog,
    private ngxXml2jsonService: NgxXml2jsonService,
    private snackBar: MatSnackBar,
    private spinner: SpinnerService,
    private utilService: UtilService,
    private http: HttpClient,
    private assetService: AssetService,
    private toolTipService: ToolTipService,
    private eventEmitterService: EventEmitterService,
    private amplitudeService: AmplitudeService,
    private publisherUserService: PublisherUserService,
    private configurationService: ConfigurationService,
    private dashboardService: DashboardService,
    @Inject(DOCUMENT) dom: Document
  ) {
    this.marketSelection = ((localStorage.getItem('zone') !== 'undefined') ? localStorage.getItem('zone') : '1');
    this.setZone();
    this.appType = ['Android'/*, 'iOS'*/];
    localStorage.setItem('activeIcon', JSON.stringify(''));
    this.eventEmitterService.onClickNavigation();
    if (this.router.getCurrentNavigation().extras) {
      if (this.router.getCurrentNavigation().extras.state) {
        if (this.router.getCurrentNavigation().extras.state.platformType) {
          this.typeOfAccount = JSON.parse(this.router.getCurrentNavigation().extras.state.platformType);
        }
        if (this.router.getCurrentNavigation().extras.state.platformId) {
          this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.platformId);
           if (this.assetService.getActiveAssetsByPlatformId(this.platformId)) {
            this.urlReadOnly = true;
           }
          this.editMode = true;
        }
        if (this.router.getCurrentNavigation().extras.state.addPlatformId) {
          this.platformId = JSON.parse(this.router.getCurrentNavigation().extras.state.addPlatformId);
          this.addMode = true;
          this.typeOfAccount = JSON.parse(this.router.getCurrentNavigation().extras.state.addPlatformType);
        }
      }
    }
    this.publisherUserService.getPublisherDetails(JSON.parse(localStorage.getItem('currentUser')).email).subscribe(
    // this.publisherProfileService.getProfileByEmail(JSON.parse(localStorage.getItem('currentUser')).email).subscribe(
      res => {
        if (res) {
          this.publisherId = res._id;
        } else {
          this.router.navigate(['/publisher/publisherProfile']);
        }
      },
      err => {
        this.router.navigate(['/publisher/publisherProfile']);
      });

      this.dom = dom;
  }

  ngOnInit() {
    this.createForm();
    if (this.typeOfAccount === 'Website') {
      this.initUniquePublisherSiteTypes();
    } else {
      this.initUniquePublisherAppTypes();
    }
    this.advertiserUtilService.getAdvertiserAppTypes().subscribe(
      res => {
        this.typesOfAdvertisers = res;
      },
      err => {
        console.log(err);
      }
    );

    this.initiateScripts();
    if (this.platformId) {
      this.initPlatformById();
      this.pageView = 'EditView';
    }
    this.loadingText = 'Loading your ' + this.typeOfAccount + ' ...';
    this.amplitudeService.logEvent('platform_addition - user clicks on add platform', null);
    this.fetchTooltips(this.toolTipKeyList);
    this.adminRolesAndPermissions();
  }

  initiateScripts() {
    if (this.typeOfAccount.toLowerCase() === 'website') {
      this.publisherPlatformService.getPlatformHeaderScript().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('headCodeSnippet').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting header HCP script!', err);
        });

      this.publisherPlatformService.getDocereeLoginSnippetScript().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('docereeLoginSnippet').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting doceree login snippet script!', err);
        });

      this.publisherPlatformService.getDocereeLogoutSnippetScript().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('docereeLogoutSnippet').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting doceree logout snippet script!', err);
      });
    }

    if (this.typeOfAccount.toLowerCase() === 'mobileapp') {
      this.publisherPlatformService.getMobilePISnippet0().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('mobilePISnippet0').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting mobile platform integration initial snippet', err);
        });


      this.publisherPlatformService.getMobilePISnippet1().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('mobilePISnippet1').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting mobile platform integration first snippet', err);
        });

      this.publisherPlatformService.getMobilePISnippet2().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('mobilePISnippet2').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting mobile platform integration second snippet', err);
        });

      this.publisherPlatformService.getMobilePISnippet3().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('mobilePISnippet3').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting mobile platform integration third snippet', err);
      });

      this.publisherPlatformService.getMobileAISnippet1().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('mobileAISnippet1').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting mobile platform integration third snippet', err);
      });

      this.publisherPlatformService.getMobileAISnippet2().subscribe(
        res => {
          this.codeInjectionScript = res.content;
          this.platformFormGroup.get('mobileAISnippet2').setValue(this.codeInjectionScript);
        },
        err => {
          console.log('Error getting mobile platform integration third snippet', err);
      });
    }


  }

  fetchTooltips(toolTipKeyList) {
    this.toolTipService.fetchToolTipUniversal(toolTipKeyList).subscribe(toolTipList => {
      toolTipList.forEach(toolTip => {
        this[toolTip.key] = toolTip.value;
      });
    });
  }

  initUniquePublisherSiteTypes() {
    this.publisherPlatformService.getUniquePublisherSiteTypes().subscribe(
      res => {
        this.uniqueSiteTypes = res;
      },
      err => {
        console.log('Error!');
      });
  }

  initUniquePublisherAppTypes() {
    this.publisherPlatformService.getUniquePublisherAppTypes().subscribe(
      res => {
        this.uniqueApps = res;
        this.uniqueSiteTypes = this.uniqueApps;
      },
      err => {
        console.log('Error!');
      });
  }

  initPlatformById() {
    this.publisherPlatformService.getPlatformById(this.platformId).subscribe(
      res => {
        this.platform = res;
        this.platformFormGroup.get('_id').setValue(this.platform._id);
        this.platformFormGroup.get('name').setValue(this.platform.name);
        this.platformFormGroup.get('domainUrl').setValue(this.platform.domainUrl);
        this.platformFormGroup.get('trustedSites').setValue(this.platform.trustedSites);
        this.platformFormGroup.get('appType').setValue(this.platform.appType);
        this.platformFormGroup.get('typeOfSite').setValue(this.platform.typeOfSite);
        this.platformFormGroup.get('marketSel').setValue(this.platform.zone);
        this.platformFormGroup.get('description').setValue(this.platform.description);
        // this.platformFormGroup.get('keywords').setValue(this.platform.keywords);
        this.platformFormGroup.get('excludeAdvertisers').setValue(this.platform.excludeAdvertiserTypes);
        this.platformFormGroup.get('gaAPIKey').setValue(this.platform.gaApiKey);
        this.platformFormGroup.get('isHCPUploaded').setValue(this.platform.isHCPUploaded);
        this.platformFormGroup.get('isDeactivated').setValue(this.platform.isDeactivated);
        this.platformFormGroup.get('isDeleted').setValue(this.platform.isDeleted);
        this.platformFormGroup.get('publisherId').setValue(this.platform.publisherId);
        if (this.platform.isHeadScriptAdded) {
          this.headScriptAddedStatus = this.headScriptAddedStatusOptions[1];
        } else {
          this.headScriptAddedStatus = this.headScriptAddedStatusOptions[0];
        }
        if (this.platform.isInvocationScriptAdded) {
          this.invocationScriptAddedStatus = this.invocationScriptAddedStatusOptions[1];
        } else {
          this.invocationScriptAddedStatus = this.invocationScriptAddedStatusOptions[0];
        }
        this.edittingPlatformName = this.platform.name;
        this.edittingDomainURL = this.platform.domainUrl;
        this.hcpUploaded = this.platform.isHCPUploaded;
        this.platformFormGroup.get('appTestKey').setValue(this.platform.appTestKey);
              this.platformFormGroup.get('appLiveKey').setValue(this.platform.appLiveKey);
        this.appTestKey = this.platform.appTestKey;
        this.appLiveKey = this.platform.appLiveKey;
        /*if (this.hcpUploaded === true) { */
          this.publisherPlatformService.checkHCPUploadDataStatusByPlatformId(this.platformId).subscribe(
            result => {
              this.hcpVerificationStatus = result;
            },
            err => {
            });
       /* }*/
       this.getDashboardData();
      },
      err => {
        console.log('Error!');
      }
    );
  }

  createForm() {
    this.platformFormGroup = this.formBuilder.group({
      _id: [this.platformId],
      platformType: [this.typeOfAccount],
      name: ['', Validators.required],
      domainUrl: ['', [Validators.required]],
      trustedSites: [''],
      appType: [''],
      typeOfSite: ['', Validators.required],
      marketSel: ['', Validators.required],
      description: ['', Validators.required],
      // keywords: ['', Validators.required],
      excludeAdvertisers: [],
      gaAPIKey: [''],
      /*fileType: ['CSV'],*/
      isHCPUploaded: [this.hcpUploaded],
      isDeleted: [false],
      isDeactivated: [false],
      publisherId: [this.publisherId],
      file: [''],
      headCodeSnippet: [''],
      mobilePISnippet0: [''],
      mobilePISnippet1: [''],
      mobilePISnippet2: [''],
      mobilePISnippet3: [''],
      mobileAISnippet1: [''],
      mobileAISnippet2: [''],
      appTestKey: [''],
      appLiveKey: [''],
      isHeadScriptAdded: [false],
      docereeLoginSnippet: [''],
      docereeLogoutSnippet: [''],
      isInvocationScriptAdded: [false]
    },
    {
      validator: [ValidateDomain('domainUrl')]
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    this.headerError = '';
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
  /*
  onSelectAll(items: any) {
    if (this.typeOfAccount === 'MobileApp') {
      this.platformFormGroup.get('excludeAdvertisers').setValue(items);
    } else {
      this.platformFormGroup.get('excludeAdvertisers').setValue(items);
    }

  }
  onDeSelectAll(items: any) {
    if (this.typeOfAccount === 'MobileApp') {
      this.platformFormGroup.get('excludeAdvertisers').setValue([]);
    } else {
      this.platformFormGroup.get('excludeAdvertisers').setValue([]);
    }
  }*/

  get f() { return this.platformFormGroup.controls; }

  selectApp(type: String) {
    this.typeOfAccount = type;
    this.loadingText = 'Loading your ' + this.typeOfAccount + ' ...';
    if (!this.platformId) {
      this.platformFormGroup.reset();
      this.initiateScripts();
    }
    if (this.typeOfAccount === 'Website') {
      this.initUniquePublisherSiteTypes();
    } else {
      this.initUniquePublisherAppTypes();
    }
    // this.loadingText = '';
  }

  savePlatform() {
    if (!this.invalidFile && this.checkExcludeAdvertisersLength() && this.validatePlatformFormGroup() && this.platformFormGroup.valid) {
      this.amplitudeService.logEvent('platform_addition - user clicks on done', null);
      this.spinner.showSpinner.next(true);
    // if (this.validatePlatformName()) {
      // let keywords: any = [];
      if (this.platformId) {
        this.updatePlatformID = this.platformId;
      } else {
        this.updatePlatformID = '';
      }
      // const words = this.platformFormGroup.get('keywords').value + '';
      // if (words) {
      //   keywords = words.split(',');
      // }
      const formData: FormData = this.createFormData();

      if (this.file) {
        formData.set('file', this.file, this.file.name);
        this.hcpUploaded = true;
        formData.set('isHCPUploaded', 'true');
      }
      if (!this.platformId) {
        this.publisherPlatformService.addPlatformDetails(formData, this.platformFormGroup.get('marketSel').value).subscribe(
          res => {console.log(res);
            const adminPermission = this.listOfExistedUserRolesPermissions.find((thisItem) =>
            thisItem.roleName.toLowerCase().trim() === 'Admin'.toLowerCase().trim());
            const publisherUser = {
              email: res.created.by,
              userRoleId: adminPermission._id,
              platformId: res._id,
              publisherId: res.publisherId
            };
            if (this.typeOfAccount.toLowerCase() === 'MobileApp'.toLowerCase()) {
              this.platformFormGroup.get('appTestKey').setValue(res.appTestKey);
              this.platformFormGroup.get('appLiveKey').setValue(res.appLiveKey);
              // this.appTestKey = res.appTestKey;
              // this.appLiveKey = res.appLiveKey;
            }
            this.spinner.showSpinner.next(false);
            this.router.navigate(['/publisher/platformCreationThanks'],
              { state: {'platformId': JSON.stringify(res._id), 'platformName':  JSON.stringify(res.name),
                'platformLink': JSON.stringify(res.domainUrl), 'platformType': JSON.stringify(res.typeOfSite),
                'hcpScript': JSON.stringify(res.hcpScript),
                'isHCPUploaded': JSON.stringify(res.isHCPUploaded),
                'platformZOne': JSON.stringify(this.platformFormGroup.get('marketSel').value)}});
          },
          err => {
            this.spinner.showSpinner.next(false);
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
            console.log('err.error.message');
          });
      } else {
        formData.append('_id', this.updatePlatformID);
        this.publisherPlatformService.updatePlatform(formData, this.platformFormGroup.get('marketSel').value).subscribe(
          res => {
            this.spinner.showSpinner.next(false);
            if (this.file) {
              this.router.navigate(['/publisher/platformEditFileUploadThanks']);
            } else {
              this.router.navigate(['/publisher/platformCreationThanks'],
                {
                  state: {
                    'platformId': JSON.stringify(res._id), 'platformName': JSON.stringify(res.name),
                    'platformLink': JSON.stringify(res.domainUrl), 'platformType': JSON.stringify(res.typeOfSite),
                    'hcpScript': JSON.stringify(res.hcpScript),
                    'isHCPUploaded': JSON.stringify(res.isHCPUploaded),
                    'platformZOne': JSON.stringify(this.platformFormGroup.get('marketSel').value)
                  }
                });
            }
          },
          err => {
            this.spinner.showSpinner.next(false);
            if (err.error.message === 'Platform name not available') {
              const formGroup = this.platformFormGroup;
              const nameCtrl = formGroup.controls['name'];
              nameCtrl.setErrors({ nameNotAvailable: true });
            }
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: err.error['message']
              }
            });
            console.log('Unknown error occured');
          });
      }
    } else {
      this.snackBar.openFromComponent(CustomSnackbarComponent, {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
        panelClass: ['snackbar-publisher'],
        data: {
          icon: 'success',
          message: 'Your form is invalid'
        }
      });
    }
  }

  updateExcludeAdvertiserInPlatform() {
    if (this.platformId) {
      this.updatePlatformID = this.platformId;
    } else {
      console.error('Incorrect function call - Insert is not supported via this function');
      return;
    }
    if (!this.checkExcludeAdvertisersLength()) {
      return;
    }
    const formData: FormData = this.createFormData();
    formData.append('_id', this.updatePlatformID);
    this.publisherPlatformService.updatePlatform(formData, this.platformFormGroup.get('marketSel').value).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.router.navigate(['/publisher/platformManage']);
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
        console.log('Unknown error occured');
      });
  }

  updateHeadScriptAddedOption() {
    if (this.platformId) {
      this.updatePlatformID = this.platformId;
    } else {
      console.error('Incorrect function call - Insert is not supported via this function');
      return;
    }
    const formData: FormData = this.createFormData();
    formData.append('_id', this.updatePlatformID);
    this.publisherPlatformService.updatePlatform(formData, this.platformFormGroup.get('marketSel').value).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.router.navigate(['/publisher/platformManage']);
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
        console.log('Unknown error occured');
      });
  }

  updateInvocationScriptAddedOption() {
    if (this.platformId) {
      this.updatePlatformID = this.platformId;
    } else {
      console.error('Incorrect function call - Insert is not supported via this function');
      return;
    }
    const formData: FormData = this.createFormData();
    formData.append('_id', this.updatePlatformID);
    this.publisherPlatformService.updatePlatform(formData, this.platformFormGroup.get('marketSel').value).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.router.navigate(['/publisher/platformManage']);
      },
      err => {
        this.spinner.showSpinner.next(false);
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
        console.log('Unknown error occured');
      });
  }

  copyElementText(id: any, type: string) {
    if (type === 'verification') {
      this.amplitudeService.logEvent('platform_addition - user copies the platform integration code', null);
    } else if (type === 'audienceIdentification') {
      this.amplitudeService.logEvent('platform_addition - user copies the audience identification code', null);
    }
    let docElement = null; // Should be <textarea> or <input>
    try {
        docElement = this.dom.getElementById(id);
        docElement.select();
        this.dom.execCommand('copy');
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: 'Text copied to clipboard'
          }
        });
    }
    finally {
      this.dom.getSelection().removeAllRanges();
    }
  }

  checkExcludeAdvertisersLength() {
    const advLength = this.platformFormGroup.get('excludeAdvertisers').value;
    if (advLength && advLength.length > 2) {
      this.platformFormGroup.get('excludeAdvertisers').setErrors({ advlength: true });
      return false;
    } else {
      // this.stepper.next();
      return true;
    }
  }

  checkExcludeAdvertisersLengthAndMoveToNextStepper() {
    this.amplitudeService.logEvent('platform_addition - user adds exclusions ', null);
    const advLength = this.platformFormGroup.get('excludeAdvertisers').value;
    if (advLength && advLength.length > 2) {
      this.platformFormGroup.get('excludeAdvertisers').setErrors({ advlength: true });
      return false;
    } else {
      if (!this.invalidFile && this.checkExcludeAdvertisersLength() && this.validatePlatformFormGroup() && this.platformFormGroup.valid) {
        this.spinner.showSpinner.next(true);
        if (this.platformId) {
          this.updatePlatformID = this.platformId;
        } else {
          this.updatePlatformID = '';
        }
        const formData: FormData = this.createFormData();
        if (!this.platformId) {
          this.publisherPlatformService.addPlatformDetails(formData, this.platformFormGroup.get('marketSel').value).subscribe(
            res => {
              this.platformId = res._id;
              this.platform = res;
              if (this.typeOfAccount.toLowerCase() === 'MobileApp'.toLowerCase()) {
                this.platformFormGroup.get('appTestKey').setValue(res.appTestKey);
                this.platformFormGroup.get('appLiveKey').setValue(res.appLiveKey);
                // this.appTestKey = res.appTestKey;
                // this.appLiveKey = res.appLiveKey;
              }
              const adminPermission = this.listOfExistedUserRolesPermissions.find((thisItem) =>
              thisItem.roleName.toLowerCase().trim() === 'Admin'.toLowerCase().trim());
              const publisherUser = {
                email: res.created.by,
                userRoleId: adminPermission._id,
                platformId: res._id,
                publisherId: res.publisherId
              };
              this.spinner.showSpinner.next(false);
              this.stepper.next();
            },
            err => {
              this.spinner.showSpinner.next(false);
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
              console.log('err.error.message');
            });
        } else {
          formData.append('_id', this.updatePlatformID);
          this.publisherPlatformService.updatePlatform(formData, this.platformFormGroup.get('marketSel').value).subscribe(
            res => {
              this.spinner.showSpinner.next(false);
              this.stepper.next();
              return true;
            },
            err => {
              this.spinner.showSpinner.next(false);
              if (err.error.message === 'Platform name not available') {
                const formGroup = this.platformFormGroup;
                const nameCtrl = formGroup.controls['name'];
                nameCtrl.setErrors({ nameNotAvailable: true });
              }
              this.snackBar.openFromComponent(CustomSnackbarComponent, {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
                panelClass: ['snackbar-publisher'],
                data: {
                  icon: 'error',
                  message: err.error['message']
                }
              });
              console.log('Unknown error occured');
            });
        }
      } else {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: 'Your form is invalid'
          }
        });
      }
    }
  }

  updateBasicPlatformDetails() {
    const formData: FormData = this.createFormData();
    if (this.platformId) {
      formData.append('_id', this.platformId);
    }
    this.publisherPlatformService.updatePlatform(formData, this.platformFormGroup.get('marketSel').value).subscribe(
      res => {
        this.spinner.showSpinner.next(false);
        this.router.navigate(['/publisher/platformManage']);
      },
      err => {
        this.spinner.showSpinner.next(false);
        console.log('Unknown error occured');
      });
  }

  createFormData() {
    const formData: FormData = new FormData();
    formData.append('name', this.platformFormGroup.get('name').value);
    formData.append('platformType', this.typeOfAccount);
    formData.append('domainUrl', this.platformFormGroup.get('domainUrl').value);
    formData.append('trustedSites', this.platformFormGroup.get('trustedSites').value);
    formData.append('appType', this.platformFormGroup.get('appType').value);
    formData.append('typeOfSite', this.platformFormGroup.get('typeOfSite').value);
    formData.append('description', this.platformFormGroup.get('description').value);
    formData.append('excludeAdvertiserTypes', this.platformFormGroup.get('excludeAdvertisers').value);
    formData.append('gaApiKey', this.platformFormGroup.get('gaAPIKey').value);
    formData.append('publisherId', this.publisherId);
    if (!this.platformId) {
      formData.append('isDeactivated', 'false');
      formData.append('isDeleted', 'false');
    } else {
      formData.append('isDeactivated', this.platform.isDeactivated);
      formData.append('isDeleted', this.platform.isDeleted);
      formData.append('isHCPUploaded', this.platform.isHCPUploaded);
    }
    if (this.headScriptAddedStatus === this.headScriptAddedStatusOptions[0]) {
      formData.set('isHeadScriptAdded', 'false');
    } else {
      formData.set('isHeadScriptAdded', 'true');
    }
    if (this.invocationScriptAddedStatus === this.invocationScriptAddedStatusOptions[0]) {
      formData.set('isInvocationScriptAdded', 'false');
    } else {
      formData.set('isInvocationScriptAdded', 'true');
    }
    return formData;
  }

  deletePlatform() {
    this.spinner.showSpinner.next(true);
    this.publisherPlatformService.getPlatformById(this.platformId).subscribe(
      res => {
        this.publisherPlatform = <PublisherPlatform> res;
        this.publisherPlatform.isDeactivated = true;
        this.publisherPlatform.isDeleted = true;
        this.publisherPlatformService.updatePlatform(this.publisherPlatform, this.platformFormGroup.get('marketSel').value).subscribe(
          result => {
            this.loadPlatformsByPublisherId();
          },
          err => {
            this.spinner.showSpinner.next(false);
            this.snackBar.openFromComponent(CustomSnackbarComponent, {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
              panelClass: ['snackbar-publisher'],
              data: {
                icon: 'error',
                message: err.error.message
              }
            });
          });

      },
      errr => {

      }
    );
  }

  deactivatePlatform() {
    this.spinner.showSpinner.next(true);
    this.publisherPlatformService.getPlatformById(this.platformId).subscribe(
      res => {
        this.publisherPlatform = <PublisherPlatform> res;
        this.publisherPlatform.isDeactivated = true;
        this.publisherPlatformService.updatePlatform(this.publisherPlatform, this.platformFormGroup.get('marketSel').value).subscribe(
          result => {
            // this.publisherPlatform = res;
            this.spinner.showSpinner.next(false);
            this.router.navigate(['/publisher/platformManage']);
          },
          err => {
            this.spinner.showSpinner.next(false);
            console.log('Unknown error occured');
          });
        },
      err => {
        this.spinner.showSpinner.next(false);
      });
  }

  activatePlatform() {
    this.spinner.showSpinner.next(true);
    this.publisherPlatformService.getPlatformById(this.platformId).subscribe(
      res => {
        this.publisherPlatform = <PublisherPlatform> res;
        this.publisherPlatform.isDeactivated = false;
        this.publisherPlatformService.updatePlatform(this.publisherPlatform, this.platformFormGroup.get('marketSel').value).subscribe(
          result => {
            // this.publisherPlatform = res;
            this.spinner.showSpinner.next(false);
            this.router.navigate(['/publisher/platformManage']);
          },
          err => {
            this.spinner.showSpinner.next(false);
            console.log('Unknown error occured');
          });
        },
      err => {
        this.spinner.showSpinner.next(false);
      });
  }

  validatePlatformFormGroup(): boolean {
    let isValid = true;
    const formGroup = this.platformFormGroup;
    const nameCtrl = formGroup.controls['name'];
    const domainCtrl = formGroup.controls['domainUrl'];
    const typeCtrl = formGroup.controls['typeOfSite'];
    const descriptionCtrl = formGroup.controls['description'];
    const marketSelCtrl = formGroup.controls['marketSel'];
    if (!nameCtrl.value) {
      nameCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!marketSelCtrl.value) {
      marketSelCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (!domainCtrl.value) {
      domainCtrl.setErrors({ require: true });
      isValid = false;
    }

    if (!typeCtrl.value) {
      typeCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!descriptionCtrl.value) {
      descriptionCtrl.setErrors({ require: true });
      isValid = false;
    }
    if (!isValid) {
      this.activeAddPlatform = true;
    }

    return isValid;
  }

 async validatePlatformName() {
    if (this.validatePlatformFormGroup()) {
      this.amplitudeService.logEvent('platform_addition - user adds basic details', null);
      const formGroup = this.platformFormGroup;
      const nameCtrl = formGroup.controls['name'];
      const value: string = nameCtrl.value;
      if (value && value.length > 2 && value.toLowerCase() !== this.edittingPlatformName.toLowerCase()) {
        this.spinner.showSpinner.next(true);
        await this.publisherPlatformService.checkPlatformNameAvailablity({ name: value, publisherId: this.publisherId}).subscribe(
          res => {
            this.spinner.showSpinner.next(false);
            nameCtrl.clearValidators();
            return this.checkPlatformURLAvailablity();
          },
          err => {
            this.spinner.showSpinner.next(false);
            nameCtrl.setErrors({ nameNotAvailable: true });
            this.activeAddPlatform = false;
            return false;
          }
        );
      } else if (value && value.length <= 2) {
        nameCtrl.setErrors({ lessLength: true });
        this.activeAddPlatform = false;
        return false;
      } else if (value && value.length > 2 && value.toLowerCase() === this.edittingPlatformName.toLowerCase()) {
          return this.checkPlatformURLAvailablity();
      }
    }
  }

  checkPlatformURLAvailablity() {
    const formGroup = this.platformFormGroup;
    const domainCtrl = formGroup.controls['domainUrl'];
    const value: string = domainCtrl.value;
    this.spinner.showSpinner.next(true);
    if (this.edittingDomainURL.toLowerCase() !== value.toLowerCase()) {
      this.utilService.validateExternalURL('http://' + value).subscribe(
        res => {
          if (res && res.toLowerCase() === 'true') {
          // if (res && res['statusCode'] === 200) {
            this.publisherPlatformService.checkPlatformURLAvailablity({ url: value}).subscribe(
              result => {
                this.spinner.showSpinner.next(false);
                this.stepper.next();
                this.activeAddPlatform = true;
                if (this.editMode && this.checkExcludeAdvertisersLength()) {
                  this.updateBasicPlatformDetails();
                }
                return true;
              },
              error => {
                this.spinner.showSpinner.next(false);
                domainCtrl.setErrors({ nameNotAvailable: true });
                this.activeAddPlatform = false;
              });
          } else {
            this.spinner.showSpinner.next(false);
            domainCtrl.setErrors({ invalidURL: true });
            this.activeAddPlatform = false;
            return false;
          }
        },
        err => {
          this.spinner.showSpinner.next(false);
          domainCtrl.setErrors({ invalidURL: true });
          this.activeAddPlatform = false;
          return false;
        });
    } else {
      if (this.editMode && this.checkExcludeAdvertisersLength()) {
        this.updateBasicPlatformDetails();
      } else {
        this.stepper.next();
        this.activeAddPlatform = true;
      }
      this.spinner.showSpinner.next(false);
      return true;
    }
  }

  disableAddPlatform() {
    this.activeAddPlatform = false;
    this.platformFormGroup.get('excludeAdvertisers').setErrors(null);
  }
  adminRolesAndPermissions() {
    this.configurationService.getalluserolesByModuleType('Publisher').subscribe(
      res => {
        if (res) {
          this.listOfExistedUserRolesPermissions = <USEROLESElement[]>res;
        }
      },
      err => {
        console.log(err);
      });
  }
  nextStepFromDoctorVerificationAPI() {

  }
  /*
  getDataRecordsArrayFromCSVFile(hcpRecordsArray: any, headerLength: any, headers: any) {
    this.dataArr = [];
    for (let i = 1; i < hcpRecordsArray.length; i++) {
      const data = hcpRecordsArray[i].split(',');
      // FOR EACH ROW IN CSV FILE IF THE NUMBER OF COLUMNS
      // ARE SAME AS NUMBER OF HEADER COLUMNS THEN PARSE THE DATA
      if (data.length === headerLength) {
        const hcpRecord = {};
        for (let j = 0; j < headerLength; j++) {
          hcpRecord[headers[j]] = data[j].trim();
        }
        this.dataArr.push(hcpRecord);
      }
    }
    return this.dataArr;
  }
  */
  downloadSampleCsv() {
    const headers: string[] = ['FirstName', 'LastName', 'Specialization', 'Zip', 'Uid'];
    const data: any[] = [
      ['MARIA', 'COOK', 'Internal Medicine', '65114', 'maria.cook@pubone.com'],
      ['DAVID', 'ROGER', 'Dermatology', '21201', 'advid.roger@pubone.com'],
      ['MICHAEL', 'MORGAN', 'Family Medicine', '92134', 'michael.morgan@pubone.com'],
      ['JAMES', 'KIM', 'Psychiatry', '32159', 'james.kim@pubone.com']
    ];

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: headers,
      nullToEmptyString: true
    };
    const csv = new Angular5Csv(data, 'HCPSamplefile', options);
  }
  changeFileType(event: MatRadioChange) {
    this.isFileSelected = false;
    this.selectedFileType = event.value;
    this.progresValue = 0;
  }
  fileChangeListener($event: any): void {
    this.progresValue = 100;
    const input = $event.target;
    const inputName = input.files[0].name;
    this.invalidFile = null;
    this.activeAddPlatform = true;
    this.file = input.files[0];
   if (input && !inputName.endsWith('csv')) {
    this.invalidFile = true;
    this.activeAddPlatform = false;
   }
  }

  validateFile() {

    if (!this.invalidFile) {
      this.stepper.previous();
    }

  }

  openDeleteCofirmDialog(type: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmPopupComponent, {
      width: '50%',
      data: {
        'title': 'Are you sure you want to ' + type + ' this platform?'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && type === 'delete') {
        this.deletePlatform();
      } else if (result && type === 'deactivate') {
        this.deactivatePlatform();
      } else if (result && type === 'activate') {
        this.activatePlatform();
      }
    });
  }
  switchPlatform(platformId) {
    this.publisherPlatformService.switchToPlatform(platformId).subscribe(
      res => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'success',
            message: res.message
          }
        });
        this.spinner.showSpinner.next(false);
        this.router.navigate(['/publisher/platformManage']);
      },
      err => {
        this.snackBar.openFromComponent(CustomSnackbarComponent, {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
          panelClass: ['snackbar-publisher'],
          data: {
            icon: 'error',
            message: err.error['message']
          }
        });
    });
  }
  setZone() {
    localStorage.setItem('zone', this.marketSelection);
  }

  logAmplitudeIntegratePlatform() {
    this.amplitudeService.logEvent('platform_addition - user integrates platform ', null);
  }

  openDeveloperShareModal(action) {
    const dialogRef = this.dialog.open(DeveloperShareComponent, {
      width: '70%',
      data: {
        'action' : action,
        'platformId': this.platformId,
        'platformName': this.platform.name,
        'platformType': this.typeOfAccount,
        'marketSelection': this.platformFormGroup.controls['marketSel'].value
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('closed dev share');
      }
    });
  }
  loadPlatformsByPublisherId() {
    this.publisherPlatformService.getAllZonesByPublisherId().subscribe(
      result => {
        this.allZonePlatforms = result;
        if (this.allZonePlatforms.length) {
          this.marketSelection = this.allZonePlatforms[0].zone;
          this.setZone();
          this.switchPlatform(this.allZonePlatforms[0]._id);
        } else {
          this.spinner.showSpinner.next(false);
          this.router.navigate(['/publisher/platformManage']);
        }
      },
      error => { });
  }
  getDashboardData () {
    this.dashboardService.getViewingPlatformDetailForDashboard(this.platformId).subscribe(
      res => {
        this.dashboardData = res;
        this.totalEarnings = this.dashboardData.earnings.total;
        this.totalImpressions = this.dashboardData.impressions.total;
        this.totalReach = this.dashboardData.reach.total;
      },
      err => {
        console.log('Error DashboardComponent : ' + err);
      }
    );
  }
  onChangeMarketPlace(marketSelection) {
    this.marketSelection = marketSelection;
    // this.setZone();
    this.publisherPlatformService.getDocereeLoginSnippetScript().subscribe(
      res => {
        this.codeInjectionScript = res.content;
        this.platformFormGroup.get('docereeLoginSnippet').setValue(this.codeInjectionScript);
      },
      err => {
        console.log('Error getting doceree login snippet script!', err);
      });
  }

}
