import config from 'config';
import express from 'express';
import jwt from 'jsonwebtoken';
import moment from 'moment';
import UserTokenDAO from '../daos/user-token.dao';
import UserTokenDTO from '../dtos/user-token.dto';
import catchError from '../error/catch-error';
import HandledApplicationError from '../error/handled-application-error';
import { logger } from '../util/winston';
import IAuthenticatedRequest from './authenticated.request';

async function authenticationGuard(
  req: IAuthenticatedRequest,
  res: express.Response,
  next: express.NextFunction,
) {
  try {
    const userTokenDAO: UserTokenDAO = new UserTokenDAO();
    // check header for the token
    const token = req.headers[config.get<string>('jwt.tokenName')] as string;
    const secretKey = config.get<string>('jwt.secretKey');

    if (token === null || token === undefined) {
      throw new HandledApplicationError(500, 'No token provided');
    }
    const tokenData: UserTokenDTO = await userTokenDAO.findByTokenAndType(
      token,
      'login',
    );
    if (tokenData) {
      const expiryTime = moment(tokenData.expiryTime).valueOf();
      const currDateTime = Date.now();
      if (expiryTime <= currDateTime) {
        throw new HandledApplicationError(401, 'Token expired');
      } else {
        req.email = tokenData.email;
        const expDate = moment(currDateTime).add('minutes', config.get<string>('jwt.expiresIn')).toDate();
        tokenData.expiryTime = expDate;
        await userTokenDAO.update(tokenData._id, tokenData);
        next();
      }
    } else {
      throw new HandledApplicationError(401, 'Token expired');
    }
  } catch (err) {
    logger.error(`Referer: ${req.header('referer')}, Origin: ${req.header('origin')}, Request body: ${JSON.stringify(req.body)}`);
    catchError(err, next);
  }
}

export default authenticationGuard;
