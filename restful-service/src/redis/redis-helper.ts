import { ObjectId } from 'mongodb';
import mongoose from 'mongoose';

import AdvertiserBrandDAO from '../daos/advertiser/advertiser-brand.dao';
import AdvertiserDAO from '../daos/advertiser/advertiser.dao';
import MonthlyInvoiceDAO from '../daos/advertiser/monthly-invoice.dao';
import AdvertiserPaymentMethodDAO from '../daos/advertiser/payment-method.dao';
import SubCampaignDAO from '../daos/advertiser/sub-campaign.dao';
import AdvertiserBrandDTO from '../dtos/advertiser/advertiser-brand.dto';
import AdvertiserDTO from '../dtos/advertiser/advertiser.dto';
import InvoiceDTO from '../dtos/advertiser/invoice.dto';
import MonthlyInvoiceDTO from '../dtos/advertiser/monthly-invoice.dto';
import AdvertiserPaymentMethodDTO from '../dtos/advertiser/payment-method.dto';
import SubCampaignDTO from '../dtos/advertiser/sub-campaign.dto';
import { advertiserAudienceModel } from '../models/advertiser/advertiser-audience-schema';
import { indiaInvoiceModel } from '../models/advertiser/invoice-india-schema';
import { usInvoiceModel } from '../models/advertiser/invoice-us-schema';

import { subCampaignModel } from '../models/advertiser/sub-campaign-schema';
import { indiaAdServerContentModel } from '../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../models/utils/adservercontent-us-schema';
import { QueueHandler } from '../queue/queue-handler';
import { logger } from '../util/winston';

const redis = require('./redis.config');

export class RedisHelper {

    private readonly expireTime = 86400;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly paymentMethod: AdvertiserPaymentMethodDAO;
    private readonly monthlyInvoiceDAO: MonthlyInvoiceDAO;
    private readonly subcampaignDAO: SubCampaignDAO;

    constructor() {
        this.brandDAO = new AdvertiserBrandDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.paymentMethod = new AdvertiserPaymentMethodDAO();
        this.monthlyInvoiceDAO = new MonthlyInvoiceDAO();
        this.subcampaignDAO = new SubCampaignDAO();
    }

    // lets call this as a whole, this function has the probability of becoming the
    // bottle neck as the load grows
    // TODO: This is the most heavy function, need a way to optimize it
    public async loadSubcampaignsForNpi() {
        const queueHandler: QueueHandler = QueueHandler.getInstance();
        const data: any = { };
        data.messageType = 'refreshAudienceCache';
        queueHandler.publishMessageFromRest(data);
    }

    public async updateAsset(dto: any) {
        const key = `codesnippet:${dto.codeSnippetId}`;
        const value: any = { };
        if (!dto.deleted) {
            value.platformId = dto.platformId.toString();
            value.assetDimensions = dto.assetDimensions;
            value.assetStatus = dto.assetStatus;
            value.assetSize = dto.assetSize;
            value.fullUrl = dto.fullUrl;
            value.fileFormats = dto.fileFormats.join();
            value.url = dto.url;
            value.assetType = dto.assetType;
            value.isVerified = dto.isVerified;
            value.minCpc = parseInt(dto.bidRange.cpc.min, 10);
            value.maxCpc = parseInt(dto.bidRange.cpc.max, 10);
            value.minCpm = parseInt(dto.bidRange.cpm.min, 10);
            value.maxCpm = parseInt(dto.bidRange.cpm.max, 10);
            value.assetId = dto._id.toString();
            value.zone = dto.zone;

            await redis.delAsync(key);
            await this.hmsetAsync(key, value);
        } else {
            await redis.delAsync(key);
        }
    }

    public async updatePlatform(dto: any) {
        const key = `platform:${dto._id.toString()}`;
        const result: any[] = await redis.hmgetAsync(key, 'platformId');
        if (result[0]) {
            if (dto.isDeactivated || dto.isDeleted || !dto.hcpScript) {
                await redis.delAsync(key);
            } else {
                const value: any = { };
                value.hcpScript = dto.hcpScript;
                value.platformType = dto.platformType;
                value.domainUrl = dto.domainUrl;
                value.typeOfSite = dto.typeOfSite;
                value.platformId = dto._id.toString();
                value.publisherId = dto.publisherId.toString();
                value.zone = dto.zone;
                value.trustedSites = dto.trustedSites;
                value.appTestKey = dto.appTestKey;
                value.appLiveKey = dto.appLiveKey;

                await redis.delAsync(key);
                await this.hmsetAsync(key, value);
            }

        }
    }

    public async updateSubcampaign(subcampaignId: string, reloadTargetAudience: boolean, zone: string) {
        // using id here, because the dto which is being passed did not contain
        // the actual updated dto but only parts of updated dto
        const dto: any = await this.subcampaignDAO.findById(subcampaignId);
        const key = `subcampaign:${dto._id.toString()}`;
        const keyExists: boolean = await redis.existsAsync(key);
        const now = new Date();
        const utcToday = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        if (keyExists) {
            // indicates, key exists
            if (!dto.isPublished || !dto.isApproved || dto.status !== 'Active' ||
            (dto.operationalDetails.endDate < utcToday)) {
                // delete it
                await redis.delAsync(key);
            }

            // delete the other keys
            this.deleteSubcampaignKey(key);
        }

        if (dto.isPublished && dto.isApproved && dto.status === 'Active' &&
            dto.operationalDetails && dto.operationalDetails.endDate >= utcToday) {

            let brandPresent: boolean = false;
            let advertiserPresent: boolean = false;
            const value: any = { };
            value.id = dto._id.toString();
            value.brandId = dto.brandId.toString();

            const bkey = `brand:${value.brandId}:spends`;
            const bvalue = await redis.getAsync(bkey);
            if (bvalue) {
                brandPresent = true;
            }
            const brand: any = await this.brandDAO.findById(dto.brandId.toString(), zone);
            const payment: any = await this.paymentMethod.findByBrandId(dto.brandId.toString(), zone);
            if (payment) {
                value.paymentPresent = !payment.isDeleted;
            } else {
                const monthlyInvoiceDTO: any = await this.monthlyInvoiceDAO.findActiveMonthlyInvoice(
                    dto.advertiserId.toString(), utcToday, zone);
                if (monthlyInvoiceDTO) {
                    value.paymentPresent = true;
                } else {
                    value.paymentPresent = false;
                }
            }

            value.advertiserId = dto.advertiserId.toString();
            const akey = `advertiser:${value.advertiserId}:spends`;
            const avalue = await redis.getAsync(akey);
            if (avalue) {
                advertiserPresent = true;
            }
            const advertiser: any = await this.advertiserDAO.findById(dto.advertiserId.toString());
            const advertiserCreditLimit: any = await this.monthlyInvoiceDAO.findActiveMonthlyInvoice(dto.advertiserId.toString(), utcToday, zone);
            value.advertiserLimit = advertiserCreditLimit ? advertiserCreditLimit.approvedCreditLimit : advertiser.spendLimit;
            value.brandLimit = advertiserCreditLimit ? advertiserCreditLimit.approvedCreditLimit : brand.spendLimit;

            value.bidType = dto.operationalDetails.bidSpecifications[0];
            value.bidAmount = parseInt(dto.operationalDetails.currency, 10);
            value.periodicSpendLimit = parseInt(dto.operationalDetails.bidLimits.budget, 10);
            value.subcampaignLimit = parseInt(dto.operationalDetails.totalBudget, 10);
            value.ctaLink = dto.creativeSpecifications.ctaLink;
            value.ctaScore = dto.creativeSpecifications.ctaScore;
            value.startDate = dto.operationalDetails.startDate;
            value.endDate = dto.operationalDetails.endDate;
            value.last3SubcampaignScore = dto.last3SubcampaignScore;

            await redis.delAsync(key, value);
            await this.hmsetAsync(key, value);
            // put all the subcampaigns in the creative type (i.e. banner, video)- sadd
            let indexKey: string = '';
            if (dto.creativeType) {
                indexKey = `subcampaign:${dto.creativeType.toLowerCase()}`;
                await this.saddAsync(indexKey, key);
            }

            // put all the subcampaigns in the device type (i.e. desktop, mobile, tablet) - sadd
            if (dto.displayTargetDetails && dto.displayTargetDetails.deviceTypes) {
                for (const t of dto.displayTargetDetails.deviceTypes) {
                    indexKey = `subcampaign:${t.toLowerCase()}`;
                    await this.saddAsync(indexKey, key);
                }
            }
            // put all the subcampaign in the targeted publisher site (i.e. Electronic Health Record, Online Learing Portal etc.) - sadd
            if (dto.displayTargetDetails && dto.displayTargetDetails.websiteTypes) {
                for (const t of dto.displayTargetDetails.websiteTypes) {
                    if (t) {
                        indexKey = `subcampaign:${t.toLowerCase()}`;
                        await this.saddAsync(indexKey, key);
                    }
                }
            }

            if (dto.creativeSpecifications && dto.creativeSpecifications.creativeDetails) {
                for (const t of dto.creativeSpecifications.creativeDetails) {
                    // put all the subcampaign by creative size - sadd
                    indexKey = `subcampaign:${t.size.toLowerCase()}`;
                    if (t.size) {
                        await this.saddAsync(indexKey, key);
                    }

                    // put all the subcampaign by creative type (i.e. PNG, JPG) - sadd
                    indexKey = `subcampaign:${t.formatType.toLowerCase()}`;
                    if (t.formatType && t.formatType !== 'NotValidFormat') {
                        await this.saddAsync(indexKey, key);
                    }

                    // put all the subcampaign by creative type (i.e. web or mobile) - sadd
                    indexKey = `subcampaign:${t.platformType.toLowerCase()}`;
                    if (t.platformType) {
                        await this.saddAsync(indexKey, key);
                    }

                    // put all the subcampaign + creative by size vs url
                    if (t.size) {
                        const creativeKey = `creative:${key}:${t.size.toLowerCase()}`;
                        const creativeValue: any = { };
                        creativeValue.url = t.url;
                        creativeValue.id = t._id.toString();
                        await this.hmsetAsync(creativeKey, creativeValue);
                    }
                }
            }

            const perDay: any[] = [];
            const perWeek: any[] = [];
            const perMonth: any[] = [];

            const subcampaignIds: any[] = [];
            if (dto.operationalDetails && dto.operationalDetails.bidLimits) {
                subcampaignIds.push(dto._id.toString());
                const bidLimitType = dto.operationalDetails.bidLimits.type;
                switch (bidLimitType) {
                    case 'Per day':
                        perDay.push(dto._id.toString());
                        break;
                    case 'Per week':
                        perWeek.push(dto._id.toString());
                        break;
                    case 'Per month':
                        perMonth.push(dto._id.toString());
                        break;
                }
            }

            const endDate = new Date(utcToday.getFullYear(), utcToday.getMonth(), utcToday.getDate() + 1);
            let startDate = utcToday;
            const perDayAmountSpent: any[] = await this.getAmountSpentBySubcampaign(perDay, startDate, endDate, zone);
            for (const amount of perDayAmountSpent) {
                const id = amount._id;
                const k = `subcampaign:${id}:periodicspends`;
                const v = amount.total;
                await this.setAsync(k, v);
            }

            startDate = new Date(utcToday.getFullYear(), utcToday.getMonth(), utcToday.getDate() - 6);
            const perWeekAmountSpent: any[] = await this.getAmountSpentBySubcampaign(perWeek, startDate, endDate, zone);
            for (const amount of perWeekAmountSpent) {
                const id = amount._id;
                const k = `subcampaign:${id}:periodicspends`;
                const v = amount.total;
                await this.setAsync(k, v);
            }

            startDate = new Date(utcToday.getFullYear(), utcToday.getMonth(), utcToday.getDate() - 29);
            const perMonthAmountSpent: any[] = await this.getAmountSpentBySubcampaign(perMonth, startDate, endDate, zone);
            for (const amount of perMonthAmountSpent) {
                const id = amount._id;
                const k = `subcampaign:${id}:periodicspends`;
                const v = amount.total;
                await this.setAsync(k, v);
            }

            startDate = new Date(0);
            const totalAmountSpent: any[] = await this.getAmountSpentBySubcampaign(subcampaignIds, startDate, endDate, zone);
            for (const id of subcampaignIds) {
                const k = `subcampaign:${id}:spends`;
                await this.setAsync(k, 0);
            }
            for (const amount of totalAmountSpent) {
                const id = amount._id;
                const k = `subcampaign:${id}:spends`;
                const v = amount.total;
                await this.setAsync(k, v);
            }

            if (!advertiserPresent) {
                const k = `brand:${value.advertiserId}:spends`;
                await this.setAsync(k, 0);
            }

            if (!brandPresent) {
                const k = `brand:${value.brandId}:spends`;
                await this.setAsync(k, 0);
            }
        }

        if (reloadTargetAudience) {
            this.loadSubcampaignsForNpi();
        }
    }

    public async updateMonthlyInvoice(advertiserDTO: AdvertiserDTO, monthlyInvoiceDTO: MonthlyInvoiceDTO, zone: string) {
        const now = new Date();
        const utcToday = new Date(now.getFullYear(), now.getMonth(), now.getDate());

        // get all the brands in an advertiser
        const brands: AdvertiserBrandDTO[] = await this.brandDAO.findByAdvertiserId(advertiserDTO._id.toString(), zone);

        let monthlyInvoiceExist: boolean = true;
        if (monthlyInvoiceDTO.startDate > utcToday || /*monthlyInvoiceDTO.endDate < utcToday ||*/
            monthlyInvoiceDTO.status !== 'Approved' || monthlyInvoiceDTO.deleted) {
            monthlyInvoiceExist = false;
        }

        for (const b of brands) {
            const subcampaigns: SubCampaignDTO[] = await this.subcampaignDAO.listOfActiveSubCampaignsByBrandId(b._id.toString());
            for (const s of subcampaigns) {
                const key = `subcampaign:${s._id.toString()}`;
                const exists: boolean = await redis.existsAsync(key);
                if (exists) {
                    let advertiserLimit = advertiserDTO.spendLimit;
                    let brandLimit = b.spendLimit;
                    if (monthlyInvoiceExist) {
                        advertiserLimit = monthlyInvoiceDTO.approvedCreditLimit;
                        brandLimit = monthlyInvoiceDTO.approvedCreditLimit;
                    }
                    let value: any = { };
                    value.advertiserLimit = advertiserLimit;
                    await this.hmsetAsync(key, value);

                    value = { };
                    value.brandLimit = brandLimit;
                    await this.hmsetAsync(key, value);

                    value = { };
                    value.paymentPresent = monthlyInvoiceExist;
                    await this.hmsetAsync(key, value);
                } else {
                    // nothing to do
                }
            }
        }
    }

    public async updateInvoice(dto: InvoiceDTO, zone: string) {
        const brandId = dto.brandId.toString();
        const advertiserId = dto.advertiserId.toString();

        const totalInvoiceForBrands: any[] = await this.totalInvoicePaidByBrands([brandId], zone);
        const totalInvoiceForAdvertisers: any[] = await this.totalInvoicePaidByAdvertisers([advertiserId], zone);
        const totalBrandAmountSpent: any[] = await this.totalAmountSpentByBrands([brandId], zone);
        const totalAdvertiserAmountSpent: any[] = await this.totalAmountSpentByAdvertisers([advertiserId], zone);

        const bkey = `brand:${brandId}:spends`;
        await this.setAsync(bkey, 0);

        for (const amount of totalBrandAmountSpent) {
            const id = amount._id;
            const key = `brand:${id}:spends`;
            let value = amount.total;
            const invoice = totalInvoiceForBrands.find((x) => (x._id === amount._id.toString()));
            if (invoice) {
                value = value - invoice.total;
            }
            await this.setAsync(key, value);
        }

        const akey = `advertiser:${advertiserId}:spends`;
        await this.setAsync(akey, 0);
        for (const amount of totalAdvertiserAmountSpent) {
            const id = amount._id;
            const key = `advertiser:${id}:spends`;
            let value = amount.total;
            const invoice = totalInvoiceForAdvertisers.find((x) => (x._id === amount._id.toString()));
            if (invoice) {
                value = value - invoice.total;
            }
            await this.setAsync(key, value);
        }
    }

    public async updatePayment(advertiserId: string, zone: string) {
        const brands: AdvertiserBrandDTO[] = await this.brandDAO.findByAdvertiserId(advertiserId, zone);
        for (const b of brands) {
            const p: AdvertiserPaymentMethodDTO = await this.paymentMethod.findByBrandId(b._id.toString(), zone);
            let paymentPresent: boolean = false;
            if (p && !p.deleted) {
                paymentPresent = true;
            }
            const subcampaigns: SubCampaignDTO[] = await this.subcampaignDAO.listOfActiveSubCampaignsByBrandId(b._id.toString());
            for (const s of subcampaigns) {
                const key = `subcampaign:${s._id.toString()}`;
                const exists: boolean = await redis.existsAsync(key);
                if (exists) {
                    const value: any = { };
                    value.paymentPresent = paymentPresent;
                    await this.hmsetAsync(key, value);
                } else {
                    // nothing to do
                }
            }
        }
    }

    public async getInfo(): Promise<any> {
        return await redis.infoAsync();
    }

    private async deleteSubcampaignKey(subcampaignKey: string) {
        // TODO: These all things should come from DB
        const creativeTypes: string[] = ['Banner'];
        const deviceTypes: string[] = ['Desktop', 'Tablet', 'Mobile'];
        const websiteTypes: string[] = ['Physician Networking Platform', 'Online Medical Journal',
                                        'Electronic Health Record', 'Online Learning Portal', 'Tele-Medicine Platform'];
        const sizes: string[] = ['250 x 250', '200 x 200', '468 x 60', '728 x 90', '300 x 250',
                                 '336 x 280', '120 x 600', '160 x 600', '300 x 600', '970 x 90',
                                 '320 x 50', '320 x 100'];
        const formatTypes: string[] = ['PNG', 'JPEG', 'GIF'];
        const platformTypes: string[] = ['Web', 'App'];
        let indexKey: string = '';
        for (const c of creativeTypes) {
            indexKey = `subcampaign:${c.toLowerCase()}`;
            await redis.sremAsync(indexKey, subcampaignKey);
        }
        for (const c of deviceTypes) {
            indexKey = `subcampaign:${c.toLowerCase()}`;
            await redis.sremAsync(indexKey, subcampaignKey);
        }
        for (const c of websiteTypes) {
            indexKey = `subcampaign:${c.toLowerCase()}`;
            await redis.sremAsync(indexKey, subcampaignKey);
        }
        for (const c of sizes) {
            indexKey = `subcampaign:${c.toLowerCase()}`;
            await redis.sremAsync(indexKey, subcampaignKey);
        }
        for (const c of formatTypes) {
            indexKey = `subcampaign:${c.toLowerCase()}`;
            await redis.sremAsync(indexKey, subcampaignKey);
        }
        for (const c of platformTypes) {
            indexKey = `subcampaign:${c.toLowerCase()}`;
            await redis.sremAsync(indexKey, subcampaignKey);
        }

        for (const c of sizes) {
            const creativeKey = `creative:${subcampaignKey}:${c.toLowerCase()}`;
            await redis.delAsync(creativeKey);
        }
    }

    private async getAmountSpentBySubcampaign(subcampaignIds: string[], startDate: Date, endDate: Date, zone: string): Promise<any> {

        const ids = subcampaignIds.map((x) => new ObjectId(x));
        const results = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    advertiserCampID: { $in: ids },
                    dateAndTime: { $gte: startDate, $lte: endDate },
                    bidAmount: { $nin: ['', null] }
                }
            },
            {
                $group: {
                    _id: '$advertiserCampID',
                    total: {
                        $sum: '$bidAmount'
                    }
                }
            }
        ])
        .allowDiskUse(true)
        .exec();
        return results;
    }

    private async totalAmountSpentByBrands(brandIds: string[], zone: string): Promise<any[]> {

        const ids = brandIds.map((x) => new ObjectId(x));
        const results = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: ids },
                    bidAmount: { $nin: ['', null] }
                }
            },
            {
                $group: {
                    _id: '$brandID',
                    total: {
                        $sum: '$bidAmount'
                    }
                }
            }
        ])
        .allowDiskUse(true)
        .exec();
        return results;
    }

    private async totalAmountSpentByAdvertisers(advertiserIds: string[], zone: string): Promise<any[]> {

        const ids = advertiserIds.map((x) => new ObjectId(x));
        const results = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    advertiserID: { $in: ids },
                    bidAmount: { $nin: ['', null] }
                }
            },
            {
                $group: {
                    _id: '$advertiserID',
                    total: {
                        $sum: '$bidAmount'
                    }
                }
            }
        ])
        .allowDiskUse(true)
        .exec();
        return results;
    }

    private async totalInvoicePaidByBrands(brandIds: string[], zone: string): Promise<any[]> {
        const invoices: any[] = await this.getInvoiceModel(zone).aggregate(
            [
                {
                    $match: {
                        brandId: { $in: brandIds },
                        invoiceStatus: 'Paid'
                    }
                },
                {
                    $group: {
                        _id: '$brandId',
                        total: {
                            $sum: '$amount'
                        }
                    }
                }
            ]).exec();
        return invoices;
    }

    private async totalInvoicePaidByAdvertisers(advertiserIds: string[], zone: string): Promise<any[]> {
        const invoices: any[] = await this.getInvoiceModel(zone).aggregate(
            [
                {
                    $match: {
                        advertiserId: { $in: advertiserIds },
                        invoiceStatus: 'Paid'
                    }
                },
                {
                    $group: {
                        _id: '$advertiserId',
                        total: {
                            $sum: '$amount'
                        }
                    }
                }
            ]).exec();
        return invoices;
    }

    private getInvoiceModel(zone: string) {
        if (zone === '1') {
            return usInvoiceModel;
        } else if (zone === '2') {
            return indiaInvoiceModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    private getAdServerContentModel(zone: string) {
        if (zone === '1') {
            return usAdServerContentModel;
        } else if (zone === '2') {
            return indiaAdServerContentModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    private async setAsync(key: string, value: any) {
        if (!value) {
            logger.error(`null value found for key: ${key}`);
        }
        await redis.setAsync(key, value);
        await redis.expire(key, this.expireTime);
    }

    private async hmsetAsync(key: string, value: any) {
        if (!value) {
            logger.error(`null value found for key: ${key}`);
        }
        await redis.hmsetAsync(key, value);
        await redis.expire(key, this.expireTime);
    }

    private async saddAsync(key: string, value: any) {
        if (!value) {
            logger.error(`null value found for key: ${key}`);
        }
        await redis.saddAsync(key, value);
        await redis.expire(key, this.expireTime);
    }

}
