import config from 'config';
import { logger } from '../util/winston';

const Redis = require('redis');
const Bluebird = require('bluebird');
Bluebird.promisifyAll(Redis.RedisClient.prototype);
Bluebird.promisifyAll(Redis.Multi.prototype);

const redisHost = config.get<string>('redis.host');
const redisPort = config.get<string>('redis.port');
const client = Redis.createClient(redisPort, redisHost);

client
  .on('connect', () => {
    logger.info('Connected to Redis');
  });

module.exports = client;
