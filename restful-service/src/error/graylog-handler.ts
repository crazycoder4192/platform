import config from 'config';
const Winston = require ('winston');
const ExpressWinston = require ('express-winston');
const WinstonGraylog2 = require ('winston-graylog2');

const GRAYLOG_HOST = config.get<string>('graylog.host');
const GRAYLOG_PORT = config.get<string>('graylog.port');
const NODE_ENV = process.env.NODE_ENV || 'development';

const appName = config.get<string>('graylog.app-env-facility');

const GrayLogger = new WinstonGraylog2 ({
  name: appName,
  level: 'debug',
  silent: false,
  handleExceptions: false,

  prelog(msg: any) {
    return msg.trim ();
  },

  graylog: {
    servers: [{ host: GRAYLOG_HOST, port: GRAYLOG_PORT }],
    facility: appName,
    bufferSize: 1400
  },

  staticMeta: {
    env: NODE_ENV
  }
});

const ExpressLogger = ExpressWinston.logger ({
  transports: [GrayLogger],
  meta: true,
  msg: 'HTTP {{req.method}} {{req.url}}',
  expressFormat: true,
  colorize: false
});

ExpressWinston.requestWhitelist.push ('body');
ExpressWinston.responseWhitelist.push ('body');

module.exports.ExpressLogger = ExpressLogger;
