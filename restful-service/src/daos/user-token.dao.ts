import config from 'config';
import moment from 'moment';
import { HashPwd } from '../../../restful-service/src/util/hash-pwd';
import UserTokenDTO from '../dtos/user-token.dto';
import { userTokenModel } from '../models/user-token-schema';

export default class UserTokenDAO {
  public userTokenModel: any;
  private readonly hashPwd: HashPwd;

  constructor() {
    this.hashPwd = new HashPwd();
  }

  public async create(user: UserTokenDTO): Promise<UserTokenDTO> {
    const saveUserToken = new userTokenModel(user);
    return await saveUserToken.save();
  }

  public async update(id: string, user: UserTokenDTO): Promise<UserTokenDTO> {
    const updateUserToken = await userTokenModel.findById(id).exec();
    Object.assign(updateUserToken, user);
    return await updateUserToken.save();
  }

  public async findByTokenAndType(
    argToken: string,
    argTokenType: string,
  ): Promise<UserTokenDTO> {
    return await userTokenModel
      .findOne({ token: argToken, tokenType: argTokenType })
      .exec();
  }

  public async findByToken(argToken: string): Promise<UserTokenDTO> {
    return await userTokenModel.findOne({ token: argToken }).exec();
  }

  public async findByEmailAndTokenType(
    argEmail: string,
    argTokenType: string,
  ): Promise<any> {
    return await userTokenModel
      .findOne({ email: argEmail, tokenType: argTokenType })
      .exec();
  }

  public async deleteByEmailAndTokenType(
    email: string,
    tokenType: string,
  ) {
    await userTokenModel
      .deleteOne({ email, tokenType })
      .exec();
  }

  public async findByType(
    argTokenType: string,
  ): Promise<UserTokenDTO[]> {
    return await userTokenModel
      .find({ tokenType: argTokenType })
      .exec();
  }

  public async getUserUnsubscribeToken(email: string) {
    await this.deleteByEmailAndTokenType(email, 'unsubscribe');
    const jwtExpiresInVal = config.get<string>('jwt.unsubscribeExpiresIn');
    const unsubscribeExpiresIn = moment().add(jwtExpiresInVal, 'days').valueOf();
    const jwtToken = await this.hashPwd.jwtToken(email, unsubscribeExpiresIn);
    const token: UserTokenDTO = new UserTokenDTO();
    token.email = email;
    token.expiryTime = new Date(unsubscribeExpiresIn);
    token.token = jwtToken;
    token.tokenType = 'unsubscribe';
    token.created = { at: new Date(), by: email };
    await this.create(token);
    return jwtToken;
  }

}
