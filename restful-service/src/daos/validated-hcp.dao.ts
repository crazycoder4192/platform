import { validatedHcpModel } from '../models/validated-hcp.schema';

export default class ValidatedHcpDAO {

    public async findZipCodes(): Promise<string[]> {
        return await validatedHcpModel.distinct('zip').exec();
    }

    public async findZipCodesByHcpIds(hcpIds: number[]): Promise<string[]> {
        const zips: any[] = await validatedHcpModel.aggregate([
            {
                $match: {
                    npi: { $in: hcpIds }
                }
            },
            {
                $group: {
                    _id: '$zip'
                }
            }
        ]).exec();
        return zips.map((x) => x._id);
    }

    public async findTaxonomies(): Promise<string[]> {
        const taxonomies: any[] = await validatedHcpModel.aggregate([
            {
                $unwind: '$taxonomy'
            },
            {
                $group: {
                    _id: '$taxonomy'
                }
            }
        ]).exec();
        return taxonomies.map((x) => x._id);
    }

    public async findTaxonomiesByHcpIds(hcpIds: number[]): Promise<string[]> {
        const taxonomies: any[] = await validatedHcpModel.aggregate([
            {
                $match: {
                    npi: { $in: hcpIds }
                }
            },
            {
                $unwind: '$taxonomy'
            },
            {
                $group: {
                    _id: '$taxonomy'
                }
            }
        ]).exec();
        return taxonomies.map((x) => x._id);
    }

    public async findGendersByHcpIds(hcpIds: number[]): Promise<string[]> {
        const taxonomies: any[] = await validatedHcpModel.aggregate([
            {
                $match: {
                    npi: { $in: hcpIds }
                }
            },
            {
                $group: {
                    _id: '$gender'
                }
            }
        ]).exec();
        return taxonomies.map((x) => x._id);
    }
}
