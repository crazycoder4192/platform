import ArchetypeDTO from '../../dtos/utils/archetype.dto';
import { archetypeModel } from '../../models/utils/archetype-schema';

export default class ArchetypeDAO {
    public async findAll(): Promise<ArchetypeDTO[]> {
        return await archetypeModel.find().exec();
    }

    public async createMany(details: ArchetypeDTO[]): Promise<ArchetypeDTO[]> {
      return await archetypeModel.insertMany(details);
    }
}
