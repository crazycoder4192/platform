import OtpDTO from '../../dtos/utils/otp.dto';
import { otpModel } from '../../models/utils/otp-schema';

export default class OtpDAO {
  public async findAll(): Promise<OtpDTO[]> {
    return await otpModel.find().exec();
  }

  public async findById(id: any): Promise<OtpDTO> {
    return await otpModel.findById(id).exec();
  }

  public async createOTP(details: OtpDTO): Promise<OtpDTO> {
    return await otpModel.insertMany(details);
  }

  public async updateOTP(id: any, details: OtpDTO): Promise<OtpDTO> {
    const otpDetails = await otpModel.findById(id).exec();
    Object.assign(otpDetails, details);
    return await otpDetails.save();
  }

  // if a user requested OTP and not used that OTP by the current time. Hence, These are invalid OTPs should be marked as Invalid
  public async markPreviousOTPsForPhoneNumberVerificationAsExpired(emailId: string) {
    const currentDate = new Date();
    await otpModel.updateMany({ email: emailId, otpStatus: 'Pending', purpose: 'phoneNumberVerification',
                                expiryTime: { $lte: currentDate } }, { $set: { otpStatus: 'Expired' } }).exec();
  }

  public async updateOTPStatus(ids: string[], status: string) {
    await otpModel.updateMany({ _id: { $in: ids } }, { $set: { otpStatus: status } }).exec();
  }

  public async getOTPsOnCurrentDateForSpecificUserByStatus(emailId: string, status: string): Promise<number> {
    const currentDateAndTime = new Date();
    const startTime = new Date(
      new Date(currentDateAndTime).getFullYear(),
      new Date(currentDateAndTime).getMonth(),
      new Date(currentDateAndTime).getDate(),
      0,
      0,
      0);
    const endTime = new Date(
        new Date(currentDateAndTime).getFullYear(),
        new Date(currentDateAndTime).getMonth(),
        new Date(currentDateAndTime).getDate(),
        23,
        59,
        59);
    return await otpModel.find({ 'email': emailId, 'created.at': { $gte: startTime, $lte: endTime }, 'otpStatus': status }).count().exec();
  }

  // if a user requested OTP in the last 15 mins meaning, OTP is valid
  public async pendingOTPsForPhoneNumberVerification(email: string): Promise<OtpDTO[]> {
    const currentDate = new Date();
    return await otpModel.find({ 'created.by': email, 'otpStatus': 'Pending', 'purpose': 'phoneNumberVerification',
                                 'expiryTime': { $gt: currentDate } }).exec();
  }
}
