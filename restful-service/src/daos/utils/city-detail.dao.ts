import config from 'config';
import MongoClient from 'mongodb';
import CityDetailDTO from '../../dtos/utils/city-detail.dto';
import { taxonomyModel } from '../../models/taxonomy.schema';
import { indiaCityDetailModel } from '../../models/utils/cities-details-india-schema';
import { indiaCityDetailStarterModel } from '../../models/utils/cities-details-india-starter-schema';
import { usCityDetailModel } from '../../models/utils/cities-details-schema';
import { indiaStateDetailModel } from '../../models/utils/state-details-india-schema';
import { usStateDetailModel } from '../../models/utils/state-details-schema';
import { validatedHcpModel } from '../../models/validated-hcp.schema';

export default class CityDetailDAO {

  public async findByCity(argCity: string, zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).find({ city: argCity }).exec();
  }

  public async findByState(zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).distinct('stateFullName').exec();
  }

  public async findUniqueCity(zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).find({ }).limit(10).exec();
  }

  public async findAll(zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).find().exec();
  }

  public async createMany(details: CityDetailDTO[], zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).insertMany(details);
  }

  public async findByCityName(cityName: string, zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).find({ city: cityName }).exec();
  }

  public async findUniqueCountry(zone: string): Promise<CityDetailDTO[]> {
    return await this.getCityDetailModel(zone).distinct('country').exec();
  }
  public async findByZipCode(zipCode: string, zone: string): Promise<CityDetailDTO> {
    return await this.getCityDetailModel(zone).findOne({ zipcode: zipCode }).exec();
  }
  public async findByZipCodes(zipCodes: string[], zone: string): Promise<CityDetailDTO[]> {
    const results = await this.getCityDetailModel(zone).aggregate([
      {
        $match: {
          zipcode: { $in: zipCodes }
        }
      },
      {
        $group: {
          _id: {
            _id: '$_id',
            zipcode: '$zipcode',
            city: '$city',
            country: '$country',
            stateFullName: '$stateFullName',
            latitude: '$latitude',
            longitude: '$longitude'
          }
        }
      }
    ]).exec();

    return results.map((x: any) => x._id);
    // return await this.getCityDetailModel(zone).find({ zipcode: { $in: zipCodes } }).exec();
  }

  public async searchCity(city: string, zone: string): Promise<CityDetailDTO[]> {
    const regexCity = `^${city}`;
    return await this.getCityDetailModel(zone)
      .find({ city: { $regex: regexCity, $options: 'i' } })
      .distinct('city')
      .exec();
  }

  public async searchZipeCode(zipCode: string, zone: string): Promise<CityDetailDTO[]> {
    const regexZipCode = `^${zipCode}`;
    return await this.getCityDetailModel(zone)
      .find({ zipcode: { $regex: regexZipCode, $options: 'i' } })
      .distinct('zipcode')
      .exec();
  }

  public async getValidHCPs(): Promise<any[]> {
    try {
      const response: any = await validatedHcpModel.find({ }).exec();
      return response;
    } catch (err) {
      throw err;
    }
  }

  public async getValidSpecializations(): Promise<any[]> {
    try {
      const validHCPDetails: any[] = await this.getValidHCPs();
      const taxonomys: any[] = [];
      for (const item of validHCPDetails) {
        const ts: string[] = item.taxonomy;
        for (const t of ts) {
          taxonomys.push(t);
        }
      }

      // all these taxonomies should belong to the 'Allopathic and Osteopathic Physicians'
      const response: any = await taxonomyModel.find(
        { Taxonomy: { $in: taxonomys } }).exec();
      return response;
    } catch (err) {
      throw err;
    }
  }

  public async findAllLocations(zone: string): Promise<CityDetailDTO[]> {
    const validHCPDetails: any[] = await this.getValidHCPs();
    // tslint:disable-next-line: prefer-const
    let hcpZipcodes: any[] = [];
    for (const item of validHCPDetails) {
      hcpZipcodes.push(item.zip);
    }
    return await this.getCityDetailModel(zone)
      .find({ zipcode: { $in: hcpZipcodes } })
      .exec();
  }

  public async findLocationByZip(hcpZips: string[], zone: string): Promise<any> {
    let country = '';
    if (zone === '1') {
      country = 'US';
    } else if (zone === '2') {
      country = 'India';
    }
    const zipCodeByStates: any[] = await this.getStateDetailModel(zone).find({ }).exec();
    const zipCodeByCities: any[] = await this.getCityDetailModel(zone).aggregate([
      {
        $match: {
          country,
          zipcode: { $in: hcpZips }
        },
      },
      {
        $sort: { stateFullName: 1, city: 1 }
      },
      {
        $group: {
          _id: {
            stateFullName: '$stateFullName',
            city: '$city'
          },
          zips: {
            $push: {
              zipCode: '$zipcode'
            }
          }
        }
      },
      {
        $group: {
          _id: {
            stateFullName: '$_id.stateFullName'
          },
          citiesAndZips: {
            $push: {
              city: '$_id.city',
              zipCodes: '$zips'
            }
          }
        }
      }

    ]).exec();

    const countryNode: any = { };
    const countryNodeChildren: any[] = [];
    for (const z of zipCodeByCities) {
      const stateNode: any = { };
      const stateNodeChildren: any[] = [];

      const commaSeparatedStateZipCodes: string = zipCodeByStates.find((x) => x.stateFullName === z._id.stateFullName).zipCodes.join();
      stateNode.text = z._id.stateFullName;
      stateNode.value = commaSeparatedStateZipCodes;

      for (const cs of z.citiesAndZips) {
        const cityNode: any = { };
        cityNode.text = cs.city;
        cityNode.value = cs.zipCodes.map((x: any) => x.zipCode).join();
        stateNodeChildren.push(cityNode);
      }
      stateNode.children = stateNodeChildren;
      countryNodeChildren.push(stateNode);
    }
    if (zone === '1') {
      countryNode.text = 'United States';
    } else if (zone === '2') {
      countryNode.text = 'India';
    }
    countryNode.value = '00000';
    countryNode.children = countryNodeChildren;
    return countryNode;
  }
  public async findLocationListByZip(hcpZips: string[], zone: string): Promise<any> {
    return await this.getCityDetailModel(zone).aggregate([
      {
        $match: {
          country: 'US',
          zipcode: { $in: hcpZips }
        },
      }
    ]).exec();
  }

  public async createStarterMany(details: CityDetailDTO[], zone: string): Promise<CityDetailDTO[]> {
    if (zone === '2') {
      return await indiaCityDetailStarterModel.insertMany(details);
    }
  }

  public async findUniqueZipCodesInStarter(zone: string): Promise<string[]> {
    if (zone === '2') {
      return await indiaCityDetailStarterModel.distinct('zipcode').exec();
    }
  }

  private getCityDetailModel(zone: string) {
    if (zone === '1') {
      return usCityDetailModel;
    } else if (zone === '2') {
      return indiaCityDetailModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }

  private getStateDetailModel(zone: string) {
    if (zone === '1') {
      return usStateDetailModel;
    } else if (zone === '2') {
      return indiaStateDetailModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }

}
