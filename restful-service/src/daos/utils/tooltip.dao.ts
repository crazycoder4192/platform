import TooltipDTO from '../../dtos/utils/tooltip.dto';
import { tooltipModel } from '../../models/utils/tooltip-schema';

export default class TooltipDAO {
    public async findAll(): Promise<TooltipDTO[]> {
        return await tooltipModel.find().exec();
    }

    public async createMany(details: TooltipDTO[]): Promise<TooltipDTO[]> {
        return await tooltipModel.insertMany(details);
    }
}
