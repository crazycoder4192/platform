import TaxonomyDTO from '../../dtos/taxonomy.dto';
import { taxonomyIndiaStarterModel } from '../../models/taxonomy-india-starter.schema';
import { taxonomyModel } from '../../models/taxonomy.schema';

export default class TaxonomyDAO {

    public async findSpecializationByTaxonomies(taxonomyCodes: string[]): Promise<any> {
        const aopTaxonomies = await taxonomyModel.aggregate([
            {
                $match: {
                    Taxonomy: { $in: taxonomyCodes },
                    Grouping: 'Allopathic & Osteopathic Physicians'
                }
            },
            {
                $sort: { Classification: 1, Specialization: 1 }
            },
            {
                $group: {
                    _id: {
                        classification: '$Classification'
                    },
                    specializationsAndTaxonomies: {
                        $push: {
                            specialization: '$Specialization',
                            taxonomy: '$Taxonomy'
                        }
                    }
                }
            }
        ]).exec();

        const groupingNode: any = { };
        groupingNode.text = 'Allopathic & Osteopathic Physicians';
        const groupingTaxonomies: string[] = [];
        const groupingNodeChildren: any[] = [];
        for (const a of aopTaxonomies) {
            const classificationNode: any = { };
            const classificationNodeChildren: any[] = [];

            classificationNode.text = a._id.classification;

            const classificationTaxonomies: string[] = [];
            for (const st of a.specializationsAndTaxonomies) {
                const specializationNode: any = { };
                if (st.specialization) {
                    specializationNode.text = st.specialization;
                } else {
                    specializationNode.text = classificationNode.text;
                }
                specializationNode.value = st.taxonomy;
                classificationNodeChildren.push(specializationNode);
                classificationTaxonomies.push(st.taxonomy);
                groupingTaxonomies.push(st.taxonomy);
            }
            classificationNode.value = classificationTaxonomies.join();
            if (classificationNodeChildren.length > 1) {
                classificationNode.children = classificationNodeChildren;
            }
            groupingNodeChildren.push(classificationNode);
        }
        groupingNode.value = groupingTaxonomies.join();
        groupingNode.children = groupingNodeChildren;
        return groupingNode;
    }

    public async findSpecializationListByTaxonomies(taxonomyCodes: string[]): Promise<any> {
        return await taxonomyModel.aggregate([
            {
                $match: {
                    Taxonomy: { $in: taxonomyCodes },
                    Grouping: 'Allopathic & Osteopathic Physicians'
                }
            }
        ]).exec();
    }

    public async findTaxonomiesInStarter(zone: string): Promise<string[]> {
        if (zone === '2') {
            return await taxonomyIndiaStarterModel.distinct('Taxonomy').exec();
        }
    }

    public async createStarterMany(details: TaxonomyDTO[], zone: string): Promise<TaxonomyDTO[]> {
        if (zone === '2') {
            return await taxonomyIndiaStarterModel.insertMany(details);
        }
    }
}
