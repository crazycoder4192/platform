import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import { AnalyticsFilterModel } from '../../models/utils/filter-analytics-schema';

export default class AnalyticsFilterDAO {
  public async findAll(): Promise<FilterAnalyticsDTO[]> {
    return await AnalyticsFilterModel.find().exec();
  }

  public async createFilter(details: FilterAnalyticsDTO): Promise<FilterAnalyticsDTO> {
    return await AnalyticsFilterModel.findOneAndUpdate({ user: details.user, filterName: details.filterName },
      details, { upsert: true }).exec();
  }

  public async getFiltersByUserEmail(email: string): Promise<FilterAnalyticsDTO[]> {
    return await AnalyticsFilterModel.find({ user: email }).exec();
  }

  public async getFilterByNameAndEmail(name: string, useremail: string): Promise<FilterAnalyticsDTO[]> {
    return await AnalyticsFilterModel.find({ filterName: name, user: useremail }).exec();
  }
}
