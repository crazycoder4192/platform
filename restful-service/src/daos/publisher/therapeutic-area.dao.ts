import TherapeuticAreaDTO from '../../dtos/publisher/therapeutic-area.dto';
import { therapeuticAreaModel } from '../../models/publisher/therapeutic-area-schema';

export default class TherapeuticAreaDAO {
  public async createMany(
    details: TherapeuticAreaDTO[],
  ): Promise<TherapeuticAreaDTO[]> {
    return await therapeuticAreaModel.insertMany(details);
  }

  public async findUnique(): Promise<TherapeuticAreaDTO[]> {
    return await therapeuticAreaModel.distinct('name').exec();
  }

  public async findAll(): Promise<TherapeuticAreaDTO[]> {
    return await therapeuticAreaModel.find().exec();
  }
}
