import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import { publisherPlatformModel } from '../../models/publisher/platform-schema';

export default class PublisherPlatformDAO {

  private readonly cap = '^' ;
  private readonly dollar = '$' ;

  public async create(
    dto: PublisherPlatformDTO,
  ): Promise<PublisherPlatformDTO> {
    const model = new publisherPlatformModel(dto);
    return await model.save();
  }

  public async findActivePlatformsByPublisherId(
    argPublisherId: string,
    zone: string
  ): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel
      .find({ publisherId: argPublisherId, zone, isDeleted: false, isDeactivated: false })
      .exec();
  }

  public async findAllPlatformsByPublisherId(
    argPublisherId: string,
    zone: string
  ): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel
      .find({ publisherId: argPublisherId, zone })
      .exec();
  }

  public async findAllForBills(zone: string, platformId?: any): Promise<PublisherPlatformDTO[]> {
    if (platformId) {
      return await publisherPlatformModel.find({ _id: platformId, zone }).exec();
    } else {
      return await publisherPlatformModel.find().exec();
    }
  }

  public async findAll(pubId: any, zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ publisherId: pubId, zone, isDeleted: false, isDeactivated: false }).exec();
  }

  public async findById(id: string, zone: string): Promise<PublisherPlatformDTO> {
    const result: PublisherPlatformDTO[] = await publisherPlatformModel.find({ _id: id, zone }).exec();
    if (result && result.length > 0) {
      return result[0];
    } else {
      return null;
    }
  }

  public async findByIds(ids: any[], zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ _id: { $in: ids }, zone }).exec();
  }

  public async findNonDeletedByIdsAndZone(ids: any[], zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ _id: { $in: ids }, zone, isDeleted: false }).exec();
  }

  public async findNonDeletedByIds(ids: any[]): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ _id: { $in: ids }, isDeleted: false }).exec();
  }

  public async findPlatformsByPlatformTypeAndPublisherId(type: string, id: string, zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.aggregate([
      { $match: {
          platformType : type,
          publisherId : id,
          isDeleted: false
        }
      },
      {
          $group: {
              _id: {
                  name: '$name',
              },
              platforms: {
                  $push: {
                  _id: '$_id',
                  platformActive: '$platformActive',
                  platformType: '$platformType',
                  typeOfSite: '$typeOfSite',
                  domainUrl: '$domainUrl',
                  url: '$url',
                  description: '$description',
                  keywords: '$keywords',
                  gaApiKey: '$gaApiKey',
                  excludeAdvertiserTypes: '$excludeAdvertiserTypes',
                  created: '$created',
                  isWatching: '$isWatching',
                  isDeleted: '$isDeleted',
                  isDeactivated: '$isDeactivated',
                  zone: '$zone',
                  appType: '$appType'
                }
              },
          }
      },
      {
          $project: {
            _id: 0,
            platforms: '$platforms',
            name: '$_id.name'
          }
      },
  ]).exec();
  }

  public async update(id: any, dto: PublisherPlatformDTO, zone: string): Promise<PublisherPlatformDTO> {
    const updatePlatform = await publisherPlatformModel.findById(id, zone).exec();
    Object.assign(updatePlatform, dto);
    return await updatePlatform.save();
  }
  public async findIsWatchingPlatformAndUpdate(id: any, zone: string): Promise<PublisherPlatformDTO> {
    return await publisherPlatformModel.findOneAndUpdate({ publisherId: id, isWatching: true, zone }, { $set: { isWatching: false } }).exec();
  }

  public async findWatchingPlatform(pubId: string): Promise<PublisherPlatformDTO> {
    throw new Error('This function is deprecated');
    // return await publisherPlatformModel.findOne({ publisherId: pubId, isWatching: true }).exec();
  }

  public async findPlatformByPublisherId(pubId: string, zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ publisherId: pubId, zone, isDeleted: false }).exec();
  }

  public async findPlatformsByMultiplePublisherIds(pubIds: any[], zone: string): Promise<PublisherPlatformDTO[]> {
    const platformsDetails: any[] = await publisherPlatformModel.aggregate([
      {
        $match: {
          publisherId: { $in: pubIds },
          isDeleted: false,
          zone
        }
      },
      {
        $group: {
          _id: {
            publisherId: '$publisherId',
          },
          data: {
            $push: {
              name: '$name',
              platformType: '$platformType',
              count: { $sum: 1 }
            }
          }
        }
      }
    ]).exec();
    return platformsDetails;
  }

  public async checkPlatformNameAvailablity(name: string, pubId: string, zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ name: { $regex:  this.cap + name + this.dollar, $options: 'i' },
                                               isDeleted: false, publisherId: pubId, zone }).exec();
  }

  public async checkPlatformURLAvailablity(url: string, zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ domainUrl: { $regex:  this.cap + url + this.dollar, $options: 'i' },
                                               isDeleted: false, zone }).exec();
  }

  public async findSiteTypeById(publisherId: string, zone: string): Promise<string> {
    const result: any = await publisherPlatformModel.findById(publisherId, zone).select('typeOfSite').exec();
    return result.typeOfSite;
  }

  public async findAllPublisher(zone: string): Promise<any[]> {
    return await publisherPlatformModel.distinct('publisherId', zone).exec();
  }

  public async findAllActivePlatforms(zone: string): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ isDeleted: false, isDeactivated: false, zone }).exec();
  }

  public async updateMigrateZone(id: any, dto: PublisherPlatformDTO): Promise<PublisherPlatformDTO> {
    const updatePlatform = await publisherPlatformModel.findById(id).exec();
    Object.assign(updatePlatform, dto);
    return await updatePlatform.save();
  }

  public async findAllMigrateZone(pubId: any): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find({ publisherId: pubId }).exec();
  }

  public async getPublisherPlatformByCond(cond: any, projection: any): Promise<PublisherPlatformDTO[]> {
    return await publisherPlatformModel.find(cond, projection).exec();
  }
}
