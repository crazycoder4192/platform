import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import { roleModel } from '../../models/admin/roles-schema';
import { publisherPlatformModel } from '../../models/publisher/platform-schema';
import { publisherUserModel } from '../../models/publisher/publisher-user-schema';
import { logger } from '../../util/winston';

export default class PublisherUserDAO {

    public async create(newPublisherDTO: PublisherUserDTO): Promise<PublisherUserDTO> {
        const savePublisherUser = new publisherUserModel(newPublisherDTO);
        return await savePublisherUser.save();
    }

    public async update(email: string, existingPublisherUser: PublisherUserDTO): Promise<PublisherUserDTO> {
        const updatePublisherUser = await publisherUserModel.findOne({ email }).exec();
        Object.assign(updatePublisherUser, existingPublisherUser);
        return await updatePublisherUser.save();
    }

    public async findByPublisherId(publisherId: string): Promise<any> {
        return await publisherUserModel.find({ publisherId }).exec();
    }

    public async findByEmail(email: any): Promise<PublisherUserDTO> {
        return await publisherUserModel.findOne({ email }).exec();
    }

    public async findByEmailWithPublisherDetails(email: string): Promise<PublisherUserDTO[]> {
        return await publisherUserModel
        .aggregate()
        .match({ email })
        .lookup({
            from: 'publishers',
            localField: 'publisherId',
            foreignField: '_id',
            as: 'publisherDetails',
        })
        .allowDiskUse(true)
        .exec();
    }

    public async findByPublisherIdAndEmail(publisherId: any, email: any): Promise<PublisherUserDTO> {
        return await publisherUserModel.findOne({ publisherId, email }).exec();
    }

    public async findAllByEmail(email: string): Promise<PublisherUserDTO[]> {
        // note: We are not expecting more than one value here.
        return await publisherUserModel.find({ email }).exec();
    }

    public async getPublisherAdminUsers(publisherId: any): Promise<any> {
        const allUsers = await publisherUserModel.find({ publisherId, deleted: false }).exec();
        const roleDTO = await roleModel.findOne({ moduleType: 'Publisher', roleName: 'Admin' }).exec();
        const adminUsers: string[] = [];
        for (const a of allUsers) {
            if (a.platforms.filter((x) => x.userRoleId === roleDTO._id.toString())) {
                adminUsers.push(a.email);
            }
        }
        return adminUsers;
    }

    public async checkPlatformPermission(email: string, platformId: string, permissions: string[]): Promise<PublisherUserDTO[]> {
        const r: any[] = await roleModel.find({ moduleType: 'Publisher',
                                                permissions: { $in: permissions } }).select('_id').exec();
        return await publisherUserModel.find({
            'email': email, 'platforms.platformId': platformId,
            'platforms.userRoleId': { $in: r.map((x: any) => x._id) }
        }).exec();
    }

    public async checkPublisherPermission(email: string, publisherId: string, permissions: string[]): Promise<PublisherUserDTO[]> {
        const r: any[] = await roleModel.find({ moduleType: 'Publisher',
                                                permissions: { $in: permissions } }).select('_id').exec();
        return await publisherUserModel.find({
            'email': email, 'publisherId': publisherId,
            'platforms.userRoleId': { $in: r.map((x: any) => x._id) }
        }).exec();
    }

    public async findWatchingPlatform(email: string, zone: string): Promise<PublisherPlatformDTO> {
        const publisherUser = await publisherUserModel.findOne({ email }).exec();
        let watchingPlatformId = null;
        for (const p of publisherUser.platforms) {
            if (p.isWatching && p.zone === zone) {
                watchingPlatformId = p.platformId;
                break;
            }
        }
        if (!watchingPlatformId && publisherUser.platforms.length > 0) {
            watchingPlatformId = publisherUser.platforms[0].platformId;
        }
        return await publisherPlatformModel.findOne({ _id: watchingPlatformId }).exec();
    }

}
