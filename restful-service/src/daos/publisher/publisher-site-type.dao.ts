import PublisherSiteTypeDTO from '../../dtos/publisher/publisher-site-type.dto';
import { publisherSiteTypeModel } from '../../models/publisher/site-type-schema';

export default class PublisherSiteTypeDAO {
  public async findUniqueSiteType(): Promise<PublisherSiteTypeDTO[]> {
    return await publisherSiteTypeModel.distinct('siteType').exec();
  }

  public async findAll(): Promise<PublisherSiteTypeDTO[]> {
    return await publisherSiteTypeModel.find().exec();
  }

  public async createMany(
    details: PublisherSiteTypeDTO[],
  ): Promise<PublisherSiteTypeDTO[]> {
    return await publisherSiteTypeModel.insertMany(details);
  }
}
