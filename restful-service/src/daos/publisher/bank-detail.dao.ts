import mongoose from 'mongoose';
import PublisherBankDetailDTO from '../../dtos/publisher/bank-detail.dto';
import { indiaPublisherBankDetailModel } from '../../models/publisher/bank-details-india-schema';
import { usPublisherBankDetailModel } from '../../models/publisher/bank-details-us-schema';

export default class PublisherBankDetailDAO {
  public async findAll(pId: string, zone: string): Promise<PublisherBankDetailDTO[]> {
    return await this.getBankDetailModel(zone).find({ publisherId: pId , deleted : false }).exec();
  }
  public async findById(id: string, zone: string): Promise<PublisherBankDetailDTO> {
    return await this.getBankDetailModel(zone).findById(id).exec();
  }
  public async findByPlatformId(id: string, zone: string): Promise<PublisherBankDetailDTO[]> {
    return await this.getBankDetailModel(zone).find({ platforms: { $in: [mongoose.Types.ObjectId(id)] }, deleted: false }).exec();
  }
  public async create(dto: PublisherBankDetailDTO, zone: string): Promise<PublisherBankDetailDTO> {
    if (zone === '1') {
      const bankDetaildto = new usPublisherBankDetailModel(dto);
      return await bankDetaildto.save();
    } else if (zone === '2') {
      const bankDetaildto = new indiaPublisherBankDetailModel(dto);
      return await bankDetaildto.save();
    } else {
        throw new Error(`Invalid zone ${zone}`);
    }
  }
  public async update(id: string, dto: PublisherBankDetailDTO, zone: string) {
    const updateBankDetailModel = await this.getBankDetailModel(zone).findById(id).exec();
    Object.assign(updateBankDetailModel, dto);
    return await updateBankDetailModel.save();
  }
  public async delete(id: string, dto: PublisherBankDetailDTO, zone: string) {
    return await this.getBankDetailModel(zone).findByIdAndUpdate(id, { deleted: true }, { new: true }).exec();
  }

  public async checkAccountNumberAvailability(accNumber: string, publisherId: string, zone: string) {
    return await this.getBankDetailModel(zone).find({ accountNumber: accNumber, publisherId: mongoose.Types.ObjectId(publisherId),
                                                      deleted: false }).exec();
  }

  public async findByCond(zone: any, cond: any, projection: any) {
    return await this.getBankDetailModel(zone).find(cond, projection).exec();
  }

  private getBankDetailModel(zone: string) {
    if (zone === '1') {
      return usPublisherBankDetailModel;
    } else if (zone === '2') {
      return indiaPublisherBankDetailModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }
}
