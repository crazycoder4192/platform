import AuditVerifiedAssetDTO from '../../dtos/publisher/audit-verified-asset.dto';
import { auditVerifiedAssetModel } from '../../models/publisher/audit-verified-asset-schema';

export default class AuditVerifiedAssetDAO {

  public async create(dto: AuditVerifiedAssetDTO): Promise<AuditVerifiedAssetDTO> {
    const createModel = new auditVerifiedAssetModel(dto);
    return await createModel.save();
  }

  public async update(id: any, dto: AuditVerifiedAssetDTO): Promise<AuditVerifiedAssetDTO> {
    const updateModel = await auditVerifiedAssetModel.findById(id).exec();
    Object.assign(updateModel, dto);
    return await updateModel.save();
  }

  public async findByAssetId(assetId: any): Promise<AuditVerifiedAssetDTO[]> {
    return await auditVerifiedAssetModel.find({ assetId }).exec();
  }
}
