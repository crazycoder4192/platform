import config from 'config';
import e from 'express';
import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import DashboardDTO from '../../dtos/publisher/dashboard.dto';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { logger } from '../../util/winston';

const ObjectId = mongoose.Types.ObjectId;

export default class DashboardDAO {

  private readonly helper: Helper;
  constructor() {
    this.helper = new Helper();
  }

  public async findById(id: string, zone: string): Promise<DashboardDTO> {
    return await this.getAdServerContentModel(zone).findById(id).exec();
  }

  public async findByAssetId(codeSnippetId: string, zone: string, logOutTime?: any): Promise<any> {

    let query: any = {
      $match: { publisherACSID: codeSnippetId }
    };

    if (logOutTime) {
      query = {
        $match: { publisherACSID: codeSnippetId, dateAndTime: { $lte: logOutTime } }
      };
    }

    return await this.getAdServerContentModel(zone)
      .aggregate([
        query,
        {
          $group: {
            _id: '$publisherACSID',
            earnings: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', '$bidType'] },
                      '$bidAmount',
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            clicks: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPC'] },
                      1,
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            impressions: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPM'] },
                      1,
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            views: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPV'] },
                      1,
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            uniqueReach: {
              $addToSet: '$hcpId'
            },
            CPC: {
              $avg: {
                $convert: {
                  input: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPC'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  },
                  to: 'double'
                }
              }
            },
            CPM: {
              $avg: {
                $convert: {
                  input: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPM'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  },
                  to: 'double'
                }
              }
            },
            CPV: {
              $avg: {
                $convert: {
                  input: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPV'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  },
                  to: 'double'
                }
              }
            }
          }
        },
        {
          $project: {
            _id: 1,
            earnings: 1,
            clicks: 1,
            impressions: 1,
            views: 1,
            uniqueReach: 1,
            reachCount: {
              $size: '$uniqueReach'
            },
            CPC: 1,
            CPM: 1,
            CPV: 1,
            CTR: {
              $multiply: [
                { $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] },
                100
              ]
            }
          }
        }
      ])
      .allowDiskUse(true)
      .exec();
  }

  public async getPerformanceAnalyticsForDashboard(filter: any, timezone: any, zone: string): Promise<any> {
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, filter.publisherCreationDate);
    const startDate = u.startDate;
    const endDate = u.endDate;
    const unit = u.unit;
    const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);

    const results: any[] = [];
    const summary: any = {
      totalEarnings: 0,
      ctr: 0,
      cpc: 0,
      cpm: 0,
      cpv: 0
    };

    for (const x of xAxis) {
      const result: any = { };
      result.durationInstance = x;
      result.avgCPM = 0;
      result.avgCPC = 0;
      result.impressions = 0;
      result.clicks = 0;
      result.totalEarnings = 0;
      result.ctr = 0;
      results.push(result);
    }

    const matchCriteria: any = { };
    matchCriteria.publisherID = ObjectId(filter.publisherId);
    if (filter.platformId) {
      matchCriteria.platformID = ObjectId(filter.platformId);
    } else {
      matchCriteria.platformID = { $in: filter.platformIds };
    }
    matchCriteria.dateAndTime = { $gte: startDate, $lt: endDate };

    const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
    const addOffset: any = this.helper.addOffset(timezone);

    addOffset.publisherID = '$publisherID';
    addOffset.bidAmount = '$bidAmount';
    addOffset.typeOfEvent = '$typeOfEvent';
    addOffset.bidType = '$bidType';

    const publisherDurationWiseAnalyticsResults = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: matchCriteria
      },
      {
        $project: addOffset
      },
      {
        $project: {
            durationInstance,
            publisherID: '$publisherID',
            bidAmount: '$bidAmount',
            typeOfEvent: '$typeOfEvent',
            bidType: '$bidType'
        }
      },
      {
        $group: {
          _id: '$durationInstance',
          totalEarnings: { $sum: '$bidAmount' },
          clicks: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPC'] },
                1,
                0
              ]
            }
          },
          impressions: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPM'] },
                1,
                0
              ]
            }
          },
          avgCPC: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPC'] }
                  ]
                },
                '$bidAmount',
                null
              ]
           }
          },
          avgCPM: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPM'] }
                  ]
                },
                { $divide: ['$bidAmount', 1] },
                null
              ]
           }
          }
        }
      }
    ]).exec();
    delete addOffset.publisherID;
    delete addOffset.bidAmount;
    delete addOffset.typeOfEvent;
    delete addOffset.bidType;

    const publisherPercentage: number = config.get<number>('docereeProfitCalculationValue');

    const avgCpcCpm = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: matchCriteria
      },
      {
        $group: {
          _id: null,
          totalAmount: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
              ]
            }
          },
          CPC: {
            $avg: {
              $cond: [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPC'] }
                  ]
                },
                '$bidAmount',
                null
              ]
            }
          },
          CPM: {
            $avg: {
              $cond: [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPM'] }
                  ]
                },
                '$bidAmount',
                null
              ]
            }
          }
        }
      },
      {
        $project: {
          _id: 1,
          CPC: 1,
          CPM: 1
        }
      }]
    ).exec();

    let totalImpressions = 0;
    let totalClicks = 0;
    let totalEarnings = 0;
    for (const result of results) {
      for (const p of publisherDurationWiseAnalyticsResults) {
        if (result.durationInstance === p._id) {
          result.avgCPM = p.avgCPM ?
            (p.avgCPM * 1000 * publisherPercentage) / 100 : 0;
          result.avgCPC = p.avgCPC ?
            (p.avgCPC * publisherPercentage) / 100 : 0;
          result.impressions = p.impressions ? p.impressions : 0;
          result.clicks = p.clicks ? p.clicks : 0;
          result.totalEarnings = p.totalEarnings ?
            (p.totalEarnings * publisherPercentage) / 100 : 0;
          if (result.impressions > 0) {
            result.ctr = (result.clicks / result.impressions) * 100;
          } else {
            result.ctr = 0;
          }

          totalEarnings += result.totalEarnings;
          totalImpressions += result.impressions;
          totalClicks += result.clicks;
          break;
        }
      }
    }

    summary.totalEarnings = totalEarnings;
    summary.ctr = totalClicks * 100 / totalImpressions;
    if (avgCpcCpm && avgCpcCpm.length > 0) {
      summary.cpm = avgCpcCpm[0].CPM * 1000 * publisherPercentage / 100;
      summary.cpc = avgCpcCpm[0].CPC * publisherPercentage / 100;
    } else {
        summary.cpm = 0;
        summary.cpc = 0;
    }
    if (publisherDurationWiseAnalyticsResults.length || avgCpcCpm.length) {
      return { data: results, summary };
    } else {
      return { data: [], summary: null };
    }
  }

  public async getPerformanceResultLineChart(filter: any, timezone: any, zone: string): Promise<any> {
    const startDate = new Date(new Date(filter.startDate).getFullYear(), new Date(filter.startDate).getMonth(), new Date(filter.startDate).getDate());
    const endDate = new Date(new Date(filter.endDate).getFullYear(), new Date(filter.endDate).getMonth(), new Date(filter.endDate).getDate());

    const results: any[] = [];
    const months: string[] = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
                              'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    const startMonth: number = startDate.getMonth();
    const endMonth: number = endDate.getMonth();
    for (let i = startMonth; i <= endMonth; i++) {
      const result: any = { };
      result.month = months[i];
      result.avgCPM = 0;
      result.avgCPC = 0;
      result.avgCPV = 0;
      result.impressions = 0;
      result.clicks = 0;
      result.totalEarnings = 0;
      result.ctr = 0;
      results.push(result);
    }

    const avgCpmAmounts = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherId: ObjectId(filter.publisherId),
          dateAndTime: { $gte: startDate, $lte: endDate },
          bidType: 'CPM'
        }
      },
      {
        $project: {
          projectMonth: {
            $month: {
              date: '$dateAndTime',
              timezone
            }
          },
          projectBidAmount: { $toDouble: '$bidAmount' }
        }
      },
      {
        $group: {
          _id: '$projectMonth', avg: { $avg: '$projectBidAmount' }
        }
      }
    ]).exec();
    for (const avgCpmAmount of avgCpmAmounts) {
      for (const result of results) {
        if (result.month === months[avgCpmAmount._id - 1]) {
          result.avgCPM = avgCpmAmount.avg * 1000;
          break;
        }
      }
    }

    const avgCpcAmounts = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherId: filter.publisherId,
          dateAndTime: { $gte: startDate, $lte: endDate },
          bidType: 'CPC'
        }
      },
      {
        $project: {
          projectMonth: {
            $month: {
              date: '$dateAndTime',
              timezone
            }
          },
          projectBidAmount: { $toDouble: '$bidAmount' }
        }
      },
      {
        $group: {
          _id: '$projectMonth', avg: { $avg: '$projectBidAmount' }
        }
      }
    ]).exec();
    for (const avgCpcAmount of avgCpcAmounts) {
      for (const result of results) {
        if (result.month === months[avgCpcAmount._id - 1]) {
          result.avgCPC = avgCpcAmount.avg;
          break;
        }
      }
    }

    const totalEarnings = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherId: filter.publisherId,
          dateAndTime: { $gte: startDate, $lte: endDate }
        }
      },
      {
        $project: {
          projectMonth: {
            $month: {
              date: '$dateAndTime',
              timezone
            }
          },
          projectBidAmount: { $toDouble: '$bidAmount' }
        }
      },
      {
        $group: {
          _id: '$projectMonth', count: { $sum: '$projectBidAmount' }
        }
      }
    ]).exec();
    for (const totalSpend of totalEarnings) {
      for (const result of results) {
        if (result.month === months[totalSpend._id - 1]) {
          result.totalEarnings = totalSpend.count;
          break;
        }
      }
    }

    const impressions = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherId: filter.publisherId,
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: 'CPM'
        }
      },
      {
        $project: {
          projectMonth: {
            $month: {
              date: '$dateAndTime',
              timezone
            }
          }
        }
      },
      {
        $group: {
          _id: '$projectMonth', count: { $sum: 1 }
        }
      }
    ]).exec();

    const clicks = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherId: filter.publisherId,
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: { $in: ['CPC', 'CPV'] }
        }
      },
      {
        $project: {
          projectMonth: {
            $month: {
              date: '$dateAndTime',
              timezone
            }
          }
        }
      },
      {
        $group: {
          _id: '$projectMonth', count: { $sum: 1 }
        }
      }
    ]).exec();

    let totalImpressions: number = 0;
    let totalClicks: number = 0;

    for (const impression of impressions) {
      for (const result of results) {
        if (result.month === months[impression._id - 1]) {
          totalImpressions = totalImpressions + impression.count;
          result.impressions = impression.count;
          break;
        }
      }
    }

    for (const click of clicks) {
      for (const result of results) {
        if (result.month === months[click._id - 1]) {
          totalClicks = totalClicks + click.count;
          result.clicks = click.count;
          break;
        }
      }
    }

    for (const result of results) {
      if (result.impressions) {
        result.ctr = result.clicks / result.impressions;
      }
    }

    const summary: any = { };
    let spends: number = 0;
    let cpm: number = 0;
    let cpc: number = 0;
    let cpv: number = 0;
    let ctr: number = 0;
    for (const result of results) {
      ctr = ctr + result.ctr;
      cpm = cpm + result.avgCPM;
      cpc = cpc + result.avgCPC;
      cpv = cpv + result.avgCPV;
      spends = spends + result.totalEarnings;
    }
    summary.ctr = totalClicks * 100 / totalImpressions;
    summary.cpm = cpm / results.length;
    summary.cpc = cpc / results.length;
    summary.cpv = cpv / results.length;
    summary.totalEarnings = spends;

    return { data: results, summary };
  }
  public async findAnalyticsForAsset(codeSnippetId: string, platformId: string, startDate: string,
                                     endDate: string, logOutTime: any, zone: string): Promise<any> {

    let query: any; let groupingId;
    if (platformId) {
      query = {
        $match: { platformID: ObjectId(platformId) }
      };
      groupingId = 'platformID';
    } else {
      query = {
        $match: { publisherACSID: codeSnippetId }
      };
      groupingId = 'publisherACSID';
    }

    if (startDate && endDate) {

      const sDate = new Date(
        new Date(startDate).getFullYear(),
        new Date(startDate).getMonth(),
        new Date(startDate).getDate(),
        new Date(startDate).getHours(),
        new Date(startDate).getMinutes(),
        new Date(startDate).getSeconds()
    );
      const eDate = new Date(
        new Date(endDate).getFullYear(),
        new Date(endDate).getMonth(),
        new Date(endDate).getDate(),
        new Date(endDate).getHours(),
        new Date(endDate).getMinutes(),
        new Date(endDate).getSeconds()
    );
      if (platformId) {
        query = {
          $match: { platformID: ObjectId(platformId), dateAndTime: { $gte: sDate, $lte: eDate } }
        };
        groupingId = 'platformID';
      } else {
        query = {
          $match: { publisherACSID: codeSnippetId, dateAndTime: { $gte: sDate, $lte: eDate } }
        };
        groupingId = 'publisherACSID';
      }
    } else if (logOutTime) {
      if (platformId) {
        query = {
          $match: { platformID: ObjectId(platformId), dateAndTime: { $lte: logOutTime } }
        };
        groupingId = 'platformID';
      } else {
        query = {
          $match: { publisherACSID: codeSnippetId, dateAndTime: { $lte: logOutTime } }
        };
        groupingId = 'publisherACSID';
      }
      // if (typeof startDate !== "undefined" && typeof endDate !== "undefined") {
      //   query = {
      //     $match: { platformID: ObjectId(platformId), publisherACSID: codeSnippetId, dateAndTime: { $lte: logOutTime } }
      //   };
      // }
    }

    return await this.getAdServerContentModel(zone)
      .aggregate([
        query,
        {
          $group: {
            _id: `$${groupingId}`,
            earnings: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', '$bidType'] },
                      '$bidAmount',
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            clicks: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPC'] },
                      1,
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            impressions: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPM'] },
                      1,
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            views: {
              $sum: {
                $convert: {
                  input: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPV'] },
                      1,
                      0
                    ]
                  },
                  to: 'double'
                }
              }
            },
            uniqueReach: {
              $addToSet: '$hcpId'
            },
            CPC: {
              $avg: {
                $convert: {
                  input: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPC'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  },
                  to: 'double'
                }
              }
            },
            CPM: {
              $avg: {
                $convert: {
                  input: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPM'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  },
                  to: 'double'
                }
              }
            },
            CPV: {
              $avg: {
                $convert: {
                  input: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPV'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  },
                  to: 'double'
                }
              }
            }
          }
        },
        {
          $project: {
            _id: 1,
            earnings: 1,
            clicks: 1,
            impressions: 1,
            views: 1,
            uniqueReach: 1,
            reachCount: {
              $size: '$uniqueReach'
            },
            CPC: 1,
            CPM: 1,
            CPV: 1,
            CTR: {
              $multiply: [
                { $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] },
                100
              ]
            }
          }
        }
      ])
      .allowDiskUse(true)
      .exec();
  }

  public async getCampaignIdsForAsset(assetCodeSnippetId: string, zone: string): Promise<DashboardDTO[]> {
    return await this.getAdServerContentModel(zone).aggregate([
      {
        $group: {
          _id: {
            publisherACSID: assetCodeSnippetId,
            campaignId: '$advertiserCampID'
          },
          count: { $sum: 1 },
        }
      },
      {
        $project: {
          _id: 0,
          assetID: '$_id.publisherACSID',
          campaignID: '$_id.campaignId',
          totalCampaigns: '$count',
        }
      }
    ]).allowDiskUse(true).exec();
  }

  public async getReach(codeSnippetIds: string[], zone: string, logOutTime?: any) {
    let query: any = {
      $match: { publisherACSID: { $in: codeSnippetIds } }
    };

    if (logOutTime) {
      query = {
        $match: { publisherACSID: { $in: codeSnippetIds }, dateAndTime: { $lte: logOutTime } }
      };
    }

    return await this.getAdServerContentModel(zone)
      .aggregate([
        query,
        {
          $group: {
            _id: null,
            groupedHcpId: {
              $addToSet: '$hcpId'
            }
          }
        },
        {
          $unwind: '$groupedHcpId'
        },
        {
          $group: { _id: null, hcpCount: { $sum: 1 } }
        }
      ])
      .allowDiskUse(true)
      .exec();
  }

  private getAdServerContentModel(zone: string) {
    if (zone === '1') {
        return usAdServerContentModel;
    } else if (zone === '2') {
        return indiaAdServerContentModel;
    } else {
        throw new Error(`Invalid zone ${zone}`);
    }
  }
}
