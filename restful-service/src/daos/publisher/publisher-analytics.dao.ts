import PublisherAnalyticsDTO from '../../dtos/publisher/publisher-analytics.dto';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import { publisherAssetModel } from '../../models/publisher/asset-schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { indiaCityDetailModel } from '../../models/utils/cities-details-india-schema';
import { usCityDetailModel } from '../../models/utils/cities-details-schema';

import config from 'config';
import { ObjectId } from 'mongodb';
import { Helper } from '../../common/helper';
import ArchetypeDAO from '../../daos/utils/archetype.dao';
import PublisherAssetDTO from '../../dtos/publisher/asset.dto';
import ArchetypeDTO from '../../dtos/utils/archetype.dto';
import HandledApplicationError from '../../error/handled-application-error';
import { subCampaignModel } from '../../models/advertiser/sub-campaign-schema';
import { indiaIncomingHcpRequestDetailsModel } from '../../models/incoming-hcp-request-details-india.schema';
import { usIncomingHcpRequestDetailsModel } from '../../models/incoming-hcp-request-details-us.schema';
import { taxonomyModel } from '../../models/taxonomy.schema';
import { AdServerRequestURLModel } from '../../models/utils/adserverrequestURL-schema';
import { validatedHcpModel } from '../../models/validated-hcp.schema';
import { logger } from '../../util/winston';
import IncomingHcpRequestDetailsDAO from '../incoming-hcp-request-details.dao';

export default class PublisherAnalyticsDAO {

  private readonly helper: Helper;
  private readonly archetypeDAO: ArchetypeDAO;
  private readonly incomingHcpRequestDetailsDAO: IncomingHcpRequestDetailsDAO;

  constructor() {
    this.helper = new Helper();
    this.archetypeDAO = new ArchetypeDAO();
    this.incomingHcpRequestDetailsDAO = new IncomingHcpRequestDetailsDAO();
  }
  public async getOverviewTopAdAssetsScatterChart(filter: FilterAnalyticsDTO, zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getOverviewTopAdAssetsScatterChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;
    let assetNames: any;

    // total reaches
    const reaches = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: 'CPM'
        }
      },
      {
        $group: {
          _id: { snippetId: '$publisherACSID', assetType: '$typeOfAdvertisement' }, groupedHcpId: { $addToSet: '$hcpId' }
        }
      },
      {
        $unwind: '$groupedHcpId'
      },
      {
        $group: { _id: '$_id', hcpCount: { $sum: 1 } }
      }
    ]).exec();

    const totalEarnings = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          bidAmount: { $ne: null }
        }
      },
      {
        $group: {
          _id: { snippetId: '$publisherACSID', assetType: '$typeOfAdvertisement' }, count: { $sum: '$bidAmount' }
        }
      }
    ]).exec();

    const snippetIds: string[] = [];
    if (this.checkNotNULL(reaches)) {
      for (const r of reaches) {
        snippetIds.push(r._id.snippetId);
      }
      assetNames = await publisherAssetModel.find({ codeSnippetId: { $in: snippetIds } })
        .select({ name: 1, codeSnippetId: 1 }).exec();
    }

    const result: any[] = [];
    if (this.checkNotNULL(assetNames) && this.checkNotNULL(reaches)) {
      for (const r of reaches) {
        const te = totalEarnings.find((x: any) => x._id.snippetId === r._id.snippetId);
        const o: any = { };
        o.assetName = assetNames.find((x: any) => x.codeSnippetId === r._id.snippetId).name;
        o.assetType = r._id.assetType;
        o.reach = r.hcpCount;
        if (te) {
          o.totalEarnings = te.count;
        } else {
          o.totalEarnings = 0;
        }
        result.push(o);
      }
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getOverviewTopAdAssetsScatterChart');
    return result;
  }

  public async getOverviewAssetTypeAndUtilizationDonutChart(filter: FilterAnalyticsDTO): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getOverviewAssetTypeAndUtilizationDonutChart');
    const docs: PublisherAssetDTO[] = await publisherAssetModel.find({ codeSnippetId: { $in: filter.assetSnippetIds } }).exec();
    const assetIds: string[] = [];
    let assetUtilizations: any;
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    if (this.checkNotNULL(docs)) {
      for (const doc of docs) {
        assetIds.push(doc._id.toString());
      }

      assetUtilizations = await AdServerRequestURLModel.aggregate([
        {
          $match: {
            assetId: { $in: assetIds },
            createdTime: { $gte: startDate, $lte: endDate }
          }
        },
        {
          $group: {
            _id: '$assetId',
            total: { $sum: 1 },
            success: {
              $sum: {
                $cond: [
                  { $eq: ['$responseStatus', '200'] },
                  1,
                  0
                ]
              }
            },
            failed: {
              $sum: {
                $cond: [
                  { $eq: ['$responseStatus', '500'] },
                  1,
                  0
                ]
              }
            }
          }
        }
      ]).allowDiskUse(true).exec();
    }

    let bannerSuccess: number = 0;
    let bannerFailed: number = 0;
    let videoSuccess: number = 0;
    let videoFailed: number = 0;
    const results: any[] = [];

    if (this.checkNotNULL(docs) && this.checkNotNULL(assetUtilizations)) {
      for (const doc of docs) {
        const a = assetUtilizations.find((x: any) => x._id === doc._id.toString());
        if (this.checkNotNULL(a)) {
          if (doc.assetType === 'Banner') {
            bannerSuccess = bannerSuccess + a.success;
            bannerFailed = bannerFailed + a.failed;
          } else if (doc.assetType === 'Video') {
            videoSuccess = videoSuccess + a.success;
            videoFailed = videoFailed + a.failed;
          }
        }
      }

      let o: any = { };
      o.assetType = 'Banner';
      o.success = bannerSuccess;
      o.failed = bannerFailed;
      results.push(o);

      o = { };
      o.assetType = 'Video';
      o.success = videoSuccess;
      o.failed = videoFailed;
      results.push(o);
    }
// tslint:disable-next-line: no-console
    console.timeEnd('getOverviewAssetTypeAndUtilizationDonutChart');
    return results;
  }

  public async getOverviewValidatedHcpsDonutChart(filter: FilterAnalyticsDTO, zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getOverviewValidatedHcpsDonutChart');
    let result: any[] = [];
    const query: any = [
      {
        $match: {
          platformId: filter.platformId,
          firstName: { $ne: null }
        }
      },
      {
        $project: {
          matched: {
            $cond: [{ $eq: ['$isMatched', true] }, 1, 0]
          },
          unmatched: {
            $cond: [{ $eq: ['$isMatched', false] }, 1, 0]
          }
        }
      },
      {
        $group: {
          _id: null,
          unvalidatedHcpCount: { $sum: '$unmatched' },
          validatedHcpCount: { $sum: '$matched' }
        }
      }
    ];
    if (zone === '1') {
      // get the total number
      result = await usIncomingHcpRequestDetailsModel.aggregate(query).exec();
    } else {
      // get the total number
      result = await indiaIncomingHcpRequestDetailsModel.aggregate(query).exec();
    }

    // tslint:disable-next-line: no-console
    console.timeEnd('getOverviewValidatedHcpsDonutChart');

    if (this.checkNotNULL(result)) {
      return result[0];
    } else {
      return { };
    }
  }

  public async getAudienceIds(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceIds');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const hcps = await this.getAdServerContentModel(zone).find({
      publisherACSID: { $in: filter.assetSnippetIds },
      dateAndTime: { $gte: startDate, $lte: endDate },
      hcpId: { $ne: 'N/A' }
    }
    ).distinct('hcpId').exec();
    if (zone === '1') {
      const nhcps: number[] = [];
      for (const hcp of hcps) {
        nhcps.push(+hcp);
      }
      // tslint:disable-next-line: no-console
      console.timeEnd('getAudienceIds');
      return nhcps;
    } else if (zone === '2') {
      const nhps: string[] = await this.incomingHcpRequestDetailsDAO.findUniqueRecords(hcps, zone);
      // tslint:disable-next-line: no-console
      console.timeEnd('getAudienceIds');
      return nhps;
    }

  }

  public async getAudienceGenderExperiencePieChart(hcps: number[]): Promise<any> {
    return { };
    /*const locationByGender: any[] = await validatedHcpModel.aggregate([
      {
        $match: {
          npi: { $in: hcps }
        }
      },
      {
        $group: {
          _id: {
            gender: '$gender',
          },
          locations: {
            $push: {
              location: '$zip',
              count: { $sum: 1 }
            }
          }
        }
      }
    ]).exec();

    const results: any[] = [];
    for (const r of locationByGender) {
      const o: any = { };
      o.gender = r._id.gender;
      o.locations = r.locations;
      results.push(o);
    }
    return results;*/
  }

  public async getAudienceSpecializationStackBarChart(hcps: number[], zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceSpecializationStackBarChart');
    let taxonomyByGender: any[] = [];
    if (zone === '1') {
      taxonomyByGender = await validatedHcpModel.aggregate([
        {
          $match: {
            npi: { $in: hcps }
          }
        },
        {
          $group: {
            _id: {
              taxonomy: { $arrayElemAt: ['$taxonomy', 0] },
              gender: '$gender'
            },
            genderCount: { $sum: 1 }
          }
        },
        {
          $group: {
            _id: '$_id.taxonomy',
            genders: {
              $push: {
                gender: '$_id.gender',
                count: '$genderCount'
              }
            }
          }
        }
      ]).exec();
    } else if (zone === '2') {
      taxonomyByGender = await indiaIncomingHcpRequestDetailsModel.aggregate([
        {
          $match: {
            platformUid: { $in: hcps }
          }
        },
        {
          $group: {
            _id: {
              taxonomy: { $arrayElemAt: ['$taxonomies', 0] },
              gender: '$gender'
            },
            genderCount: { $sum: 1 }
          }
        },
        {
          $group: {
            _id: '$_id.taxonomy',
            genders: {
              $push: {
                gender: '$_id.gender',
                count: '$genderCount'
              }
            }
          }
        }
      ]).exec();
    }

    if (taxonomyByGender && taxonomyByGender.length > 10) {
      let top: any = [];
      for (const t of taxonomyByGender) {
        let count = 0;
        for (const g of t.genders) {
          count = count + g.count;
        }
        top.push({ taxonomy: t, count });
      }

      top.sort((a: any, b: any) => b.count - a.count);
      top = top.slice(0, 10);
      taxonomyByGender = top.map((x: any) => x.taxonomy);
    }
    // output will be of the format
    // { _id: '224P00000X', genders: [ { gender: 'M', count: 1 } ] }
    const txnmy: string[] = [];
    for (const t of taxonomyByGender) {
      txnmy.push(t._id);
    }

    const taxanomies: any[] = await taxonomyModel.find({ Taxonomy: { $in: txnmy } }).exec();

    const results: any[] = [];
    for (const t of taxonomyByGender) {
      const code: string = t._id;
      let who: string = '';
      for (const x of taxanomies) {
        if (x.Taxonomy === code) {
          who = x.Classification;
          break;
        }
      }
      const existing = results.find((x) => x.specialization === who);
      let o: any = { specialization: who, female: 0, male: 0, other: 0 };
      if (existing) {
        o = existing;
      }
      for (const g of t.genders) {
        if (g.gender === 'F') {
          o.female = o.female + g.count;
        } else if (g.gender === 'M') {
          o.male = o.male + g.count;
        } else {
          o.other = o.other + g.count;
        }
      }
      if (!existing) {
        results.push(o);
      }
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceSpecializationStackBarChart');
    // output will be of the format:
    // { taxonomy: 'Prosthetist', genders: [ { gender: 'M', count: 1 } ] }
    return results;
  }

  public async getAudienceLocationMapChart(hcpIds: number[], zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceLocationMapChart');
    // tslint:disable-next-line: no-console
    console.time('getAudienceLocationMapChart-validate-hcp');
    let totalHits: number = 0;
    let locations: any[] = [];
    if (zone === '1') {
      locations = await validatedHcpModel.aggregate([
        {
          $match: {
            npi: { $in: hcpIds }
          }
        },
        {
          $group: {
            _id: '$zip',
            count: { $sum: 1 }
          }
        }
      ]).exec();
    } else if (zone === '2') {
      locations = await indiaIncomingHcpRequestDetailsModel.aggregate([
        {
          $match: {
            platformUid: { $in: hcpIds }
          }
        },
        {
          $group: {
            _id: '$zip',
            count: { $sum: 1 }
          }
        }
      ]).exec();
    }

    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceLocationMapChart-validate-hcp');

    const zipCodes: string[] = [];
    for (const l of locations) {
      zipCodes.push(l._id);
    }

    const cityDetails: any[] = await this.getCityDetailModel(zone).aggregate([
      {
        $match: {
          zipcode: { $in: zipCodes }
        }
      },
      {
        $group: {
          _id: '$stateFullName',
          zip: { $addToSet: '$zipcode' }
        }
      }
    ]).exec();

    const result: any[] = [];
    const resultWithPercentage: any[] = [];

    for (const state of cityDetails) {
      const sum = locations.filter((x) => state.zip.includes(x._id)).map((x) => x.count).reduce((a, b) => a + b, 0);
      const o: any = { };
      o.state = state._id;
      o.count = sum;
      result.push(o);
      totalHits = totalHits + o.count;
    }
    for (const res of result) {
      const val: number = (((res.count / totalHits) * 100) * 100) / 100;
      // const value: number = this.getDrawValueForLocationMap(val);
      const o = {
        state: res.state,
        count: res.count,
      //  drawValue: value,
        showValue: val
       };
      resultWithPercentage.push(o);
    }

    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceLocationMapChart');

    // output will be of the format: { state: 'New York', count: 2 }
    const sortedData = resultWithPercentage.sort((t1, t2) => {
      if (t1.count < t2.count) {
        return 1;
      }
      if (t1.count > t2.count) {
        return -1;
      }
      return 0;
    });
    return sortedData;
  }

  public async getAudienceArcheTypeBarChart(hcpIds: number[], zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceArcheTypeBarChart');
    const scores: any[] = await validatedHcpModel.aggregate([
      {
        $match: {
          npi: { $in: hcpIds }
        }
      },
      {
        $project: {
          score: '$hcpScore.cumulativeScore'
        }
      }
    ]).exec();

    let pharmaiety: number = 0;
    let connectors: number = 0;
    let toolBoxers: number = 0;
    let curators: number = 0;
    let followers: number = 0;
    const archetypes: ArchetypeDTO[] = await this.archetypeDAO.findAll();
    for (const s of scores) {
      const archType: string = this.getArcheType(+s.score, archetypes);
      switch (archType) {
        case 'Pharmaiety': {
          pharmaiety++;
          break;
        }
        case 'Connectors': {
          connectors++;
          break;
        }
        case 'Toolboxers': {
          toolBoxers++;
          break;
        }
        case 'Curators': {
          curators++;
          break;
        }
        case 'Followers': {
          followers++;
          break;
        }
      }
    }
    const archeType: any = { };
    if (connectors > 0) {
      archeType.connectors = connectors;
    }
    if (curators > 0) {
      archeType.curators = curators;
    }
    if (pharmaiety > 0) {
      archeType.pharmaiety = pharmaiety;
    }
    if (followers > 0) {
      archeType.followers = followers;
    }
    if (toolBoxers > 0) {
      archeType.toolBoxers = toolBoxers;
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceArcheTypeBarChart');
    return archeType;
  }

  public async findById(id: string): Promise<PublisherAnalyticsDTO> {
    throw new Error('Method not implemented');
    // return await this.getAdServerContentModel(zone).findById(id).exec();
  }

  public async getAudienceAssetType(filter: FilterAnalyticsDTO, zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceAssetType');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const docs: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: 'CPM',
          creativeId: { $ne: 'N/A' }
        }
      },
      {
        $project: {
          creativeId: '$creativeId'
        }
      }
    ]).exec();

    const creativeIds: ObjectId[] = [];

    for (const d of docs) {
      try {
        creativeIds.push(new ObjectId(d.creativeId));
      } catch (err) {
        // we have a case where creative id is coming as Generic Content
      }
    }

    const data = await subCampaignModel.aggregate([
      {
        $match: {
          'creativeSpecifications.creativeDetails._id': { $in: creativeIds }
        }
      },
      {
        $unwind: '$creativeSpecifications.creativeDetails'
      },
      {
        $match: {
          'creativeSpecifications.creativeDetails._id': { $in: creativeIds }
        }
      },
      {
        $group: {
          _id: '$creativeSpecifications.creativeDetails.formatType',
          count: { $sum: 1 }
        }
      }
    ]).exec();
    // output is of the format
    // { _id: 'image/png', count: 1 }
    const results: any[] = [];
    for (const r of data) {
      const o: any = { };
      let n: string = r._id;
      n = n.toUpperCase();
      if (n.indexOf('IMAGE/') > -1) {
        n = n.substr('IMAGE/'.length, n.length);
      }
      o.imageType = n;
      o.count = r.count;
      results.push(o);
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceAssetType');
    return results;
  }

  public async getBehaviorTimeOfDayHeatChart(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getBehaviorTimeOfDayHeatChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const addOffset: any = this.helper.addOffset(timezone);
    const docs: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: 'CPM'
        }
      },
      {
        $project: addOffset
      },
      {
        $project: {
          projectDayOfWeek: {
            $dayOfWeek: {
              date: '$dateAndTime'
            }
          },
          projectHour: {
            $hour: {
              date: '$dateAndTime'
            }
          }
        }
      },
      {
        $group: {
          _id: {
            projectDayOfWeek: '$projectDayOfWeek',
            projectHour: '$projectHour'
          },
          count: { $sum: 1 }
        }
      }
    ]).exec();

    const days: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    const hours: string[] = ['00-03', '03-06', '06-09', '09-12', '12-15', '15-18', '18-21', '21-24'];

    const results: any[] = [];

    const dbResults: any = [];
    if (!this.checkNotNULL(docs)) {
      return [];
    }

    for (const doc of docs) {
      const result: any = { };
      result.weekDay = days[doc._id.projectDayOfWeek - 1];
      result.hourRange = this.getHourRange(doc._id.projectHour);
      result.count = doc.count;
      dbResults.push(result);
    }
    for (const day of days) {
      for (const hour of hours) {
        const result: any = { };
        result.weekDay = day;
        result.hourRange = hour;
        result.count = 0;
        results.push(result);
      }
    }

    for (const res of dbResults) {
      const temp = results.find((x) => (x.weekDay === res.weekDay && x.hourRange === res.hourRange));
      results[results.indexOf(temp)].count = results[results.indexOf(temp)].count + res.count;
    }
    const resultsWithNonZero: any[] = [];
    for (const res of results) {
      if (res.count > 0) {
        resultsWithNonZero.push(res);
      }
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getBehaviorTimeOfDayHeatChart');
    return resultsWithNonZero;
    /*
    const days: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    const hours: any[] = [
      { range: '00-03', min: 0, max: 3 },
      { range: '03-06', min: 3, max: 6 },
      { range: '06-09', min: 6, max: 9 },
      { range: '09-12', min: 9, max: 12 },
      { range: '12-15', min: 12, max: 15 },
      { range: '15-18', min: 15, max: 18 },
      { range: '18-21', min: 18, max: 21 },
      { range: '21-24', min: 21, max: 24 }];
    const results: any[] = [];

    for (const doc of docs) {
      let result: any = { };
      let range: string = '';

      for (const h of hours) {
        if (doc._id.projectHour >= h.min && doc._id.projectHour < h.max) {
          range = h.range;
        }
      }

      for (const r of results) {
        if (r.dayOfWeek === (doc._id.projectDayOfWeek) && r.range === range) {
          result = r;
          break;
        }
      }

      result.dayOfWeek = days[doc._id.projectDayOfWeek - 1];
      for (const h of hours) {
        if (doc._id.projectHour >= h.min && doc._id.projectHour < h.max) {
          result.hourRange = h.range;
          if (result.count) {
            result.count = result.count + doc.count;
          } else {
            result.count = doc.count;
          }
        }
      }
      results.push(result);
    }
    return results;*/
  }

  public async getBehaviorDeviceDonutChart(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getBehaviorDeviceDonutChart');
    // this site lists the probable devices list we can get: https://developers.whatismybrowser.com/useragents/explore/operating_platform/
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    // the regex is picked from: https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
    // tslint:disable-next-line
    const mobilesAndTablets: string[] = ['/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))','smartphone','tablet'];
    // tslint:disable-next-line
    const mobiles: string[] = ['/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))','smartphone'];
    const mobileAndTabletResultSet = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          'publisherACSID': { $in: filter.assetSnippetIds },
          'dateAndTime': { $gte: startDate, $lte: endDate },
          'typeOfEvent': 'CPM',
          'deviceDetails.device.type': { $in: mobilesAndTablets }
        }
      },
      {
        $group: {
          _id: '$deviceDetails.device.type',
          myCount: { $sum: 1 }
        }
      },
      {
        $project: { _id: 0 }
      }
    ]).exec();

    const mobileResultSet = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          'publisherACSID': { $in: filter.assetSnippetIds },
          'dateAndTime': { $gte: startDate, $lte: endDate },
          'typeOfEvent': 'CPM',
          'deviceDetails.device.type': { $in: mobiles }
        }
      },
      {
        $group: {
          _id: null,
          myCount: { $sum: 1 }
        }
      },
      {
        $project: { _id: 0 }
      }
    ]).exec();

    const allResultSet = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: 'CPM'
        }
      },
      {
        $group: {
          _id: '$deviceDetails.device.type',
          myCount: { $sum: 1 }
        }
      },
      {
        $project: { _id: 0 }
      }
    ]).exec();

    const desktopCount: number = allResultSet.length - mobileAndTabletResultSet.length;
    const tabletCount: number = mobileAndTabletResultSet.length - mobileResultSet.length;
    const mobileCount: number = mobileResultSet.length;

    const o: any = { };
    if (desktopCount >= 0) {
      o.desktop = desktopCount;
    }
    if (tabletCount >= 0) {
      o.tablet = tabletCount;
    }
    if (mobileCount >= 0) {
      o.mobile = mobileCount;
    }

    // tslint:disable-next-line: no-console
    console.timeEnd('getBehaviorDeviceDonutChart');
    if (desktopCount > 0 || tabletCount > 0 || mobileCount > 0) {
      return o;
    } else {
      return { };
    }
  }

  public async getConversionEngagementAreaLineChart(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getConversionEngagementAreaLineChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;
    const unit = u.unit;
    const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);

    const results: any[] = [];
    let returnEmpty = true;

    for (const x of xAxis) {
      const result: any = { };
      result.durationInstance = x;
      result.impressions = 0;
      result.reaches = 0;
      result.views = 0;
      result.clicks = 0;
      results.push(result);
    }

    const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
    const addOffset: any = this.helper.addOffset(timezone);

    addOffset.publisherID = '$publisherID';
    addOffset.bidAmount = '$bidAmount';
    addOffset.typeOfEvent = '$typeOfEvent';
    addOffset.bidType = '$bidType';

    const rs = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate }
        }
      },
      {
        $project: {
            durationInstance,
            publisherID: '$publisherID',
            bidAmount: '$bidAmount',
            typeOfEvent: '$typeOfEvent',
            bidType: '$bidType',
            hcpId: '$hcpId'
        }
      },
      {
        $group: {
          _id: '$durationInstance',
          totalEarnings: { $sum: '$bidAmount' },
          clicks: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPC'] },
                1,
                0
              ]
            }
          },
          impressions: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPM'] },
                1,
                0
              ]
            }
          },
          uniqueReach: {
            $addToSet: '$hcpId'
          }
        }
      },
      {
        $project: {
          _id: 1,
          clicks: 1,
          impressions: 1,
          reaches: {
            $size: '$uniqueReach'
          }
        }
      }
    ]).exec();
    delete addOffset.publisherID;
    delete addOffset.bidAmount;
    delete addOffset.typeOfEvent;
    delete addOffset.bidType;

    // segregate views in the final result   // views not required for this chart
    for (const r of rs) {
      returnEmpty = false;
      for (const result of results) {
        if (result.durationInstance === r._id) {
          result.impressions = r.impressions;
          result.clicks = r.clicks;
          result.reaches = r.reaches;
          break;
        }
      }
    }
    const totalReachInDuration = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          typeOfEvent: 'CPM'
        }
      },
      {
        $group: {
          _id: null, groupedHcpId: { $addToSet: '$hcpId' }
        }
      },
      {
        $unwind: '$groupedHcpId'
      },
      {
        $group: { _id: '$_id', hcpCount: { $sum: 1 } }
      }
    ]).exec();

    const summary: any = { };
    let sImpression: number = 0;
    let sClick: number = 0;
    for (const result of results) {
      sImpression = sImpression + result.impressions;
      sClick = sClick + result.clicks;
    }
    summary.impressions = sImpression;
    summary.clicks = sClick;
    if (totalReachInDuration && totalReachInDuration.length > 0 && totalReachInDuration[0].hcpCount) {
      summary.reaches = totalReachInDuration[0].hcpCount;
    } else {
      summary.reaches = 0;
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getConversionEngagementAreaLineChart');

    if (!returnEmpty) {
      return { data: results, summary };
    }
    return { };
  }

  public async getConversionResultLineChart(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getConversionResultLineChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;
    const unit = u.unit;
    const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);

    const results: any[] = [];

    for (const x of xAxis) {
      const result: any = { };
      result.durationInstance = x;
      result.avgCPM = 0;
      result.avgCPC = 0;
      result.avgCPV = 0;
      result.impressions = 0;
      result.clicks = 0;
      result.totalEarnings = 0;
      result.ctr = 0;
      results.push(result);
    }

    const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
    const addOffset: any = this.helper.addOffset(timezone);

    addOffset.publisherID = '$publisherID';
    addOffset.bidAmount = '$bidAmount';
    addOffset.typeOfEvent = '$typeOfEvent';
    addOffset.bidType = '$bidType';

    const publisherDurationWiseAnalyticsResults = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate }
        }
      },
      {
        $project: addOffset
      },
      {
        $project: {
            durationInstance,
            publisherID: '$publisherID',
            bidAmount: '$bidAmount',
            typeOfEvent: '$typeOfEvent',
            bidType: '$bidType'
        }
      },
      {
        $group: {
          _id: '$durationInstance',
          totalEarnings: { $sum: '$bidAmount' },
          clicks: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPC'] },
                1,
                0
              ]
            }
          },
          impressions: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPM'] },
                1,
                0
              ]
            }
          },
          avgCPC: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPC'] }
                  ]
                },
                '$bidAmount',
                null
              ]
           }
          },
          avgCPM: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPM'] }
                  ]
                },
                { $divide: ['$bidAmount', 1] },
                null
              ]
           }
          }
        }
      }
    ]).exec();
    delete addOffset.publisherID;
    delete addOffset.bidAmount;
    delete addOffset.typeOfEvent;
    delete addOffset.bidType;

    const publisherPercentage: number = config.get<number>('docereeProfitCalculationValue');

    const avgCpcCpm = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: filter.assetSnippetIds },
          dateAndTime: { $gte: startDate, $lte: endDate }
        }
      },
      {
        $group: {
          _id: null,
          totalAmount: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
              ]
            }
          },
          CPC: {
            $avg: {
              $cond: [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPC'] }
                  ]
                },
                '$bidAmount',
                null
              ]
            }
          },
          CPM: {
            $avg: {
              $cond: [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPM'] }
                  ]
                },
                '$bidAmount',
                null
              ]
            }
          }
        }
      },
      {
        $project: {
          _id: 1,
          CPC: 1,
          CPM: 1
        }
      }]
    ).exec();

    let totalImpressions = 0;
    let totalClicks = 0;
    let totalEarnings = 0;
    for (const result of results) {
      for (const p of publisherDurationWiseAnalyticsResults) {
        if (result.durationInstance === p._id) {
          result.avgCPM = p.avgCPM ?
            (p.avgCPM * 1000 * publisherPercentage) / 100 : 0;
          result.avgCPC = p.avgCPC ?
            (p.avgCPC * publisherPercentage) / 100 : 0;
          result.impressions = p.impressions ? p.impressions : 0;
          result.clicks = p.clicks ? p.clicks : 0;
          result.totalEarnings = p.totalEarnings ?
            (p.totalEarnings * publisherPercentage) / 100 : 0;
          if (result.impressions > 0) {
            result.ctr = (result.clicks / result.impressions) * 100;
          } else {
            result.ctr = 0;
          }

          totalEarnings += result.totalEarnings;
          totalImpressions += result.impressions;
          totalClicks += result.clicks;
          break;
        }
      }
    }

    const summary: any = {
      totalEarnings: 0,
      ctr: 0,
      cpc: 0,
      cpm: 0,
      cpv: 0
    };
    summary.earnings = totalEarnings;
    summary.ctr = totalClicks * 100 / totalImpressions;
    if (avgCpcCpm && avgCpcCpm.length > 0) {
      summary.cpm = avgCpcCpm[0].CPM * 1000 * publisherPercentage / 100;
      summary.cpc = avgCpcCpm[0].CPC * publisherPercentage / 100;
    } else {
        summary.cpm = 0;
        summary.cpc = 0;
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getConversionResultLineChart');
    if (avgCpcCpm.length || publisherDurationWiseAnalyticsResults.length) {
      return { data: results, summary };
    } else {
      return { data: [], summary };
    }
  }

  public async getAllPublisherAssetsbyId(ids: string[]): Promise<any[]> {

    return await publisherAssetModel.find({ _id: { $in: ids }, assetType: 'Banner', assetStatus: 'pending' }).exec();
  }

  public async getFilterDetailsForAnalyticsByPlatformId(platformId: string, zone: string): Promise<any> {
    const filteredZipCodes = new Set();
    const filteredTaxonomies = new Set();
    const filteredGender = new Set();
    const locations: any[] = [];
    const taxonomies: any[] = [];
    const genders: any[] = [];
    let filterData = { };

    // TODO: Handle for India HCPs
    const HCPs: any[] = await usIncomingHcpRequestDetailsModel
      .find({ platformId, isMatched: true })
      .exec();
    const validHCPs = new Set();
    for (const dt of HCPs) {
      validHCPs.add(+dt.npi);
    }
    const result: any[] = await validatedHcpModel.aggregate([
      {
        $match: {
          npi: { $in: Array.from(validHCPs.values()) }
        }
      },
      {
        $group: {
          _id: '$npi',
          details: {
            $push: {
              gender: '$gender',
              taxonomy: '$taxonomy',
              zip: '$zip'
            }
          }
        }
      }
    ]).exec();

    for (const obj of result) {
      for (const detail of obj.details) {
        for (const taxonomy of detail.taxonomy) {
          filteredTaxonomies.add(taxonomy);
        }
        filteredZipCodes.add(detail.zip);
        filteredGender.add(detail.gender);
      }
    }
    const validTaxonomy: any[] = await taxonomyModel
      .find({ Taxonomy: { $in: Array.from(filteredTaxonomies.values()) } })
      .exec();
    for (const obj of validTaxonomy) {
      const taxonomy = {
        value: obj.Taxonomy,
        name: obj.Classification
      };
      taxonomies.push(taxonomy);
    }
    const validLocations: any[] = await this.getCityDetailModel(zone).find({ zipcode: { $in: Array.from(filteredZipCodes.values()) } }).exec();
    for (const obj of validLocations) {
      const location = {
        value: obj.city,
        name: obj.city
      };
      locations.push(location);
    }

    for (const obj of filteredGender) {
      let gender = {
        value: 'O',
        name: 'Other'
      };

      if (obj === 'M') {
        gender = {
          value: 'M',
          name: 'Male'
        };
      } else if (obj === 'F') {
        gender = {
          value: 'F',
          name: 'Female'
        };
      }

      genders.push(gender);
    }

    filterData = {
      taxnomy: taxonomies,
      location: locations,
      gender: genders
    };
    return filterData;
  }

  private getArcheType(score: number, archetypes: ArchetypeDTO[]): string {
    for (const archetype of archetypes) {
      if (archetype.hcpMaxScoreRange === 'N/A') {
        archetype.hcpMaxScoreRange = archetype.hcpMinScoreRange + 1;
      }
      if (score >= parseInt(archetype.hcpMinScoreRange, 10) && score <= parseInt(archetype.hcpMaxScoreRange, 10)) {
       return archetype.name;
      }
    }
  }

  private getDrawValueForLocationMap(score: number) {
    if (score >= 21 && score < 41) {
      return 40;
    } else if (score >= 41 && score < 61) {
      return 60;
    } else if (score >= 61 && score < 81) {
      return 80;
    } else if (score >= 81 && score < 101) {
      return 100;
    } else {
      return 20;
    }

  }

  private checkNotNULL(data: any) {
    if (data instanceof Array) {
        if (data.length > 0) {
           return true;
          }
        return false;
    } else if (data instanceof Object) {
        if (data && (Object.keys(data).length !== 0)) {
            return true;
        }
        return false;
    }
  }

  private getHourRange(hour: number): string {
    switch (hour) {
      case 0:
        return '00-03';
      case 1:
        return '00-03';
      case 2:
        return '00-03';
      case 3:
        return '03-06';
      case 4:
        return '03-06';
      case 5:
        return '03-06';
      case 6:
        return '06-09';
      case 7:
        return '06-09';
      case 8:
        return '06-09';
      case 9:
        return '09-12';
      case 10:
        return '09-12';
      case 11:
        return '09-12';
      case 12:
        return '12-15';
      case 13:
        return '12-15';
      case 14:
        return '12-15';
      case 15:
        return '15-18';
      case 16:
        return '15-18';
      case 17:
        return '15-18';
      case 18:
        return '18-21';
      case 19:
        return '18-21';
      case 20:
        return '18-21';
      case 21:
        return '21-24';
      case 22:
        return '21-24';
      case 23:
        return '21-24';
    }
  }

  private getAdServerContentModel(zone: string) {
    if (zone === '1') {
        return usAdServerContentModel;
    } else if (zone === '2') {
        return indiaAdServerContentModel;
    } else {
        throw new Error(`Invalid zone ${zone}`);
    }
  }

  private getCityDetailModel(zone: string) {
    if (zone === '1') {
      return usCityDetailModel;
    } else if (zone === '2') {
      return indiaCityDetailModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }
}
