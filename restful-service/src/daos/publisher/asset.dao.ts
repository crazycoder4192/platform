import config from 'config';
import mongoose from 'mongoose';
import AssetDTO from '../../dtos/publisher/asset.dto';
import { publisherAssetModel } from '../../models/publisher/asset-schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { AdServerRequestURLModel } from '../../models/utils/adserverrequestURL-schema';
const ObjectId = mongoose.Types.ObjectId;

export default class PublisherAssetDAO {

  private readonly cap = '^' ;
  private readonly dollar = '$' ;

  public async findById(id: string): Promise<AssetDTO> {
    return await publisherAssetModel.findById(id).exec();
  }

  public async findAll(pubId: string, watchingPlatformId: any, zone: string): Promise<any> {
    const listOfAssets = await publisherAssetModel.aggregate([
      {
        $match: { publisherId: pubId,
                  platformId: watchingPlatformId,
                  deleted: false }
      }
    ]).allowDiskUse(true).exec();

    const codeSnippetIds: string[] = listOfAssets.map((x: any) => x.codeSnippetId);
    const assetIds: string[] = listOfAssets.map((x: any) => x._id.toString());
    const earningsData: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: { publisherACSID: { $in: codeSnippetIds } }
      },
      {
        $group: {
          _id: '$publisherACSID',
          earnings: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', '$bidType'] },
                '$bidAmount',
                0
              ]
            }
          },
          clicks: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPC'] },
                1,
                0
              ]
            }
          },
          impressions: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPM'] },
                1,
                0
              ]
            }
          },
          views: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', 'CPV'] },
                1,
                0
              ]
            }
          },
          CPC: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPC'] }
                  ]
                },
                '$bidAmount',
                null
              ]
           }
          },
          CPM: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPM'] }
                  ]
                },
                '$bidAmount',
                null
              ]
           }
          },
          CPV: {
            $avg : {
              $cond : [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPV'] }
                  ]
                },
                '$bidAmount',
                null
              ]
           }
          }
        }
      }
    ]).allowDiskUse(true).exec();

    const utilizationData: any[] = await AdServerRequestURLModel.aggregate([
      {
        $match: { assetId: { $in: assetIds } }
      },
      {
        $group: {
          _id: '$assetId',
          total: { $sum: 1 },
          success: {
            $sum: {
              $cond: [
                { $eq: ['$responseStatus', '200'] },
                1,
                0
              ]
            }
          },
          failed: {
            $sum: {
              $cond: [
                { $eq: ['$responseStatus', '500'] },
                1,
                0
              ]
            }
          }
        }
      },
      {
        $project: {
          total: 1,
          success: 1,
          failed: 1,
          utilized: {
            $multiply: [
              { $cond: [ { $eq: [ '$total', 0 ] }, 0, { $divide: ['$success', '$total'] }] },
              100
            ]
          }
        }
      }
    ]).allowDiskUse(true).exec();

    for (const asset of listOfAssets) {
      const e = earningsData.find((x) => x._id === asset.codeSnippetId);
      const transactionalData: any[] = [];
      if (e) {
        transactionalData.push(e);
      }

      if (transactionalData && transactionalData.length === 1) {
        transactionalData[0].CPC = (transactionalData[0].CPC !== null) ?
          (transactionalData[0].CPC * config.get<number>('docereeProfitCalculationValue') / 100) : 0;
        transactionalData[0].CPV = (transactionalData[0].CPV !== null) ?
          (transactionalData[0].CPV * config.get<number>('docereeProfitCalculationValue') / 100) : 0;
        transactionalData[0].CPM = (transactionalData[0].CPM !== null) ?
          (transactionalData[0].CPM * 1000 * config.get<number>('docereeProfitCalculationValue') / 100) : 0;
        transactionalData[0].earnings = (transactionalData[0].earnings !== null) ?
          (transactionalData[0].earnings * config.get<number>('docereeProfitCalculationValue') / 100) : 0;
      } else {
        transactionalData.push({ CPC : '-', CPV : '-', CPM : '-', earnings: '-', clicks: '-', impressions: '-' });
      }

      const u = utilizationData.find((x) => x._id === asset._id.toString());
      asset.utilization = [];
      if (u) {
        asset.utilization.push(u);
      }
      if (asset.utilization && asset.utilization.length !== 1) {
        asset.utilization.push({ success: '-', failed: '-', utilized: '-' });
      }
      if (asset.utilization && asset.utilization[0] && (asset.utilization[0].utilized === null || asset.utilization[0].utilized === undefined)) {
        asset.utilization[0].utilized = 0;
      }
      asset.transactionalData = transactionalData;
    }
    return listOfAssets;
  }

  public async getAllPlatformsForPublisher(pubId: string): Promise<AssetDTO[]> {
    return await publisherAssetModel.aggregate([
      {
        $match: { publisherId: ObjectId(pubId) }
      },
      {
        $group: {
          _id: {
            pubId: '$publisherId',
            platformId: '$platformId'
          },
          assetDetails: {
            $push: {
              assetId: '$_id',
              assetName: '$name',
              snippetId: '$codeSnippetId',
              assetType: '$assetType',
            }
          },
          count: { $sum: 1 }
        }
      },
      {
        $lookup: {
          from: 'publisher_platforms',
          localField: '_id.platformId',
          foreignField: '_id',
          as: 'platformDetails'
        }
      }
    ]).allowDiskUse(true).exec();
  }

  public async create(dto: AssetDTO): Promise<AssetDTO> {
    const createModel = new publisherAssetModel(dto);
    return await createModel.save();
  }

  public async update(id: any, dto: AssetDTO): Promise<AssetDTO> {
    const updateModel = await publisherAssetModel.findById(id).exec();
    Object.assign(updateModel, dto);
    return await updateModel.save();
  }

  public async delete(id: any, dto: AssetDTO): Promise<AssetDTO> {
    const deleteModel = await publisherAssetModel.findById(id).exec();
    Object.assign(deleteModel, dto);
    return await deleteModel.save();
  }

  public async getBannerSizes(): Promise<AssetDTO[]> {
    return await publisherAssetModel
      .aggregate()
      .match({ assetType: 'Banner' })
      .exec();
  }
  public async deleteMany(id: any): Promise<AssetDTO[]> {
    return await publisherAssetModel.updateMany({ platformId: id }, { $set: { deleted: true } }).exec();
  }
  public async deActivateMany(id: any): Promise<AssetDTO[]> {
    return await publisherAssetModel.updateMany({ platformId: id }, { $set: { assetStatus: 'Inactive' } }).exec();
  }

  // get active assets by platform id
  public async findActiveAssetsByPlatformId(id: any): Promise<AssetDTO[]> {
    return await publisherAssetModel.find({ platformId: id, deleted: false, assetStatus: 'Active' },
                        { _id: 1, name: 1, assetType: 1, codeSnippetId: 1, assetDimensions: 1 }).exec();
  }

  public async findAssetsByPlatformId(id: any): Promise<AssetDTO[]> {
    return await publisherAssetModel.find({ platformId: id, deleted: false },
                        { _id: 1, name: 1, assetType: 1, codeSnippetId: 1, assetDimensions: 1 }).exec();
  }

  public async findAllAssetsByPlatformId(id: any): Promise<AssetDTO[]> {
    return await publisherAssetModel.find({ platformId: id },
                        { _id: 1, name: 1, assetType: 1, codeSnippetId: 1, assetDimensions: 1, created: 1 }).exec();
  }

  public async listOfAssetSnippetIdsByPlatformId(platformId: any): Promise<string[]> {
    const docs: any[] = await publisherAssetModel.find({ platformId: new ObjectId(platformId) }).select('codeSnippetId').exec();
    const snippetIds: string[] = [];
    for (const doc of docs) {
      snippetIds.push(doc.codeSnippetId);
    }
    return snippetIds;
  }

  public async getSnippetIdByAssetId(assetId: string): Promise<string> {
    const doc: any = await publisherAssetModel.findOne({ _id: new ObjectId(assetId) }).exec();
    return doc.codeSnippetId;
  }

  public async getAssetsByPlatformId(platformId: string): Promise<AssetDTO[]> {
    const doc: any = await publisherAssetModel.find({ platformId: new ObjectId(platformId), deleted: false }).exec();
    return doc;
  }

  public async getActiveAssetsByPlatformId(platformId: string): Promise<AssetDTO[]> {
    const doc: any = await publisherAssetModel.find({ platformId: new ObjectId(platformId), assetStatus: 'Active' }).exec();
    return doc;
  }

  public async checkUniqueAssetNameWithUser(assetName: string, pId: any): Promise<AssetDTO[]> {
    return await publisherAssetModel.find({ name: { $regex: this.cap + assetName + this.dollar, $options: 'i' },
                                            deleted: false, platformId: pId }).exec();
  }

  public async getHcp(type: string, id: string, zone: string): Promise<number[]> {
    let snippetIds: string[] = [];
    if (type.toLowerCase() === 'platform') {
      snippetIds = await this.listOfAssetSnippetIdsByPlatformId(id);
    } else if (type.toLowerCase() === 'asset') {
      snippetIds = [await this.getSnippetIdByAssetId(id)];
    } else {
      throw new Error('Unknown type; expected type to be platform or asset');
    }

    const reaches: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          publisherACSID: { $in: snippetIds },
          typeOfEvent: 'CPM',
          hcpId: { $ne: 'N/A' }
        }
      },
      {
        $group: {
          _id: '$hcpId'
        }
      }
    ]).exec();

    return reaches.map((x) => parseInt(x._id, 10));
  }

  public async findAllPublisher(): Promise<any[]> {
    return await publisherAssetModel.distinct('publisherId').exec();
  }

  public async findAllPlatforms(): Promise<any[]> {
    return await publisherAssetModel.distinct('platformId').exec();
  }

  public async getActiveAssetCountForAdminByPlatformId(pId: any): Promise<any> {
    return await publisherAssetModel.count({ platformId: pId, deleted: false, assetStatus: 'Active' }).exec();
  }
  public async getAssetsForAdminByACSIDs(ACSIds: any[]): Promise<any> {
    return await publisherAssetModel.find({ codeSnippetId: { $in : ACSIds } }).exec();
  }
  public async getAllAssetCountForAdminByPlatformId(pId: any): Promise<any> {
    return await publisherAssetModel.count({ platformId: pId, deleted: false }).exec();
  }
  public async findCountOfActiveAssetsByPublisherIds(pubIds: any[]): Promise<any> {
    return await publisherAssetModel.aggregate([
      {
        $match: {
          deleted: false,
          assetStatus: 'Active',
          publisherId: { $in: pubIds }
        }
      },
      {
        $group: {
          _id: { pubId: '$publisherId', assetId: '$_id' },
        }
      },
      {
        $group: {
          _id: { publisherId: '$_id.pubId' },
          activeAssets: { $sum: 1 }
        }
      }
    ]).exec();
  }

  private getAdServerContentModel(zone: string) {
    if (zone === '1') {
      return usAdServerContentModel;
    } else if (zone === '2') {
      return indiaAdServerContentModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }
}
