import mongoose from 'mongoose';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import { publisherPlatformModel } from '../../models/publisher/platform-schema';
import { publisherModel } from '../../models/publisher/publisher-schema';

export default class PublisherDAO {

  private readonly publisherAssetDAO: PublisherAssetDAO;
  constructor() {
    this.publisherAssetDAO = new PublisherAssetDAO();
  }

  public async create(dto: PublisherDTO): Promise<PublisherDTO> {
    const savePublisher = new publisherModel(dto);
    return await savePublisher.save();
  }

  public async update(id: any, dto: PublisherDTO): Promise<PublisherDTO> {
    const updatePublisher = await publisherModel.findById(id).exec();
    Object.assign(updatePublisher, dto);
    return await updatePublisher.save();
  }

  public async findByEmail(argEmail: any): Promise<PublisherDTO> {
    return await publisherModel.findOne({ 'user.email': argEmail }).exec();
  }

  public async findById(id: string): Promise<PublisherDTO> {
    return await publisherModel.findById(id).exec();
  }
  public async findByIds(ids: any[]): Promise<PublisherDTO[]> {
    return await publisherModel.find({ _id : { $in: ids } }).exec();
  }

  public async findUsers(): Promise<any[]> {
    return await publisherModel.find({ }).select('user.email').exec();
  }

  public async findAll(): Promise<PublisherDTO[]> {
    return await publisherModel.find({ }).exec();
  }

  public async findPublishersForAdmin(): Promise<any[]> {
    const publishers = await publisherModel.find().exec();
    const publisherIds = [];
    const publisherData = [];
    for (const id of publishers) {
      publisherIds.push(mongoose.Types.ObjectId(id._id));
    }
    const platforms = await publisherPlatformModel.aggregate([
      {
        $match: {
        publisherId: { $in:  publisherIds },
        isDeleted: false,
        isDeactivated: false
      }
        },
      {
          $group: {
            _id: {
              publisherId: '$publisherId',
            },
            data: {
              $push: {
                platformId: '$_id',
                name: '$name',
                platformType: '$platformType',
                isDeactivated: '$isDeactivated',
                typeOfSite: '$typeOfSite',
                count: { $sum: 1 },
                zone: '$zone'
              }
            }
          }
        }
      ]).allowDiskUse(true).exec();
    for (const pub of publishers) {
      const o: any = { };
      o.publisherId = pub._id;
      o.organizationName = pub.organizationName;
      o.country = pub.address.country;
      let publisherActiveAssets = 0;
      let publisherAllAssets = 0;
      for (const platform of platforms) {
        if (pub._id.equals(platform._id.publisherId)) {
          const platformDetails = [];
          o.publisherPlatformCount = platform.data.length;
          for (const data of platform.data) {
            const pfData: any = { };
            pfData.platformId = data.platformId;
            pfData.platformName = data.name;
            pfData.platformType = data.platformType;
            pfData.zone = data.zone ? data.zone : 1;
            const pfId = mongoose.Types.ObjectId(data.platformId);
            pfData.platformAllAssets = await this.publisherAssetDAO.getAllAssetCountForAdminByPlatformId(pfId);
            publisherAllAssets = publisherAllAssets + pfData.platformAllAssets;
            pfData.platformActiveAssets = await this.publisherAssetDAO.getActiveAssetCountForAdminByPlatformId(pfId);
            publisherActiveAssets = publisherActiveAssets + pfData.platformActiveAssets;
            platformDetails.push(pfData);
          }
          o.platforms = platformDetails;
          o.publisherActiveAssets = publisherActiveAssets;
          o.publisherAllAssets = publisherAllAssets;
          o.showPlatform = false;
          break;
        }
      }
      publisherData.push(o);
    }
    return publisherData;
  }

  public async getPublisherNamesByPublisherIds(ids: any[]): Promise<any[]> {
    return await publisherModel.find({ }).select('organizationName').exec();
  }

  public async getPublisherByCond(cond: any, projection: any): Promise<any[]> {
    return await publisherModel.find(cond, projection).exec();
  }

}
