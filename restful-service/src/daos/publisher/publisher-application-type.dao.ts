import PublisherApplicationTypeDTO from '../../dtos/publisher/publisher-application-type.dto';
import { publisherApplicationTypeModel } from '../../models/publisher/application-type-schema';

export default class PublisherApplicationTypeDAO {
  public async createMany(
    details: PublisherApplicationTypeDTO[],
  ): Promise<PublisherApplicationTypeDTO[]> {
    return await publisherApplicationTypeModel.insertMany(details);
  }

  public async findAll(): Promise<PublisherApplicationTypeDTO[]> {
    return await publisherApplicationTypeModel.find().exec();
  }

  public async findUniqueAppType(): Promise<PublisherApplicationTypeDTO[]> {
    return await publisherApplicationTypeModel.distinct('appType').exec();
  }
}
