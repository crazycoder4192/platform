import BillDTO from '../../dtos/publisher/publisher-bill.dto';
import { countryTaxModel } from '../../models/admin/country-tax-schema';
import { indiaBillModel } from '../../models/publisher/publisher-bill-india-schema';
import { usBillModel } from '../../models/publisher/publisher-bill-us-schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';

export default class PublisherBillDAO {

    public async create(dto: BillDTO, zone: string): Promise<BillDTO> {
        if (zone === '1') {
            const model = new usBillModel(dto);
            return await model.save();
        } else if (zone === '2') {
            const model = new indiaBillModel(dto);
            return await model.save();
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    public async update(id: string, billData: BillDTO, zone: string): Promise<BillDTO> {
        const bill = await this.getBillModel(zone).findOne({ _id: id, deleted: false }).exec();
        Object.assign(bill, billData);
        return await bill.save();
    }

    public async findPendingBills(zone: string): Promise<BillDTO[]> {
        return await this.getBillModel(zone).find({ billStatus: 'Pending' }).exec();
    }

    public async findAllByPublisherId(id: string, zone: string): Promise<BillDTO[]> {
        return await this.getBillModel(zone).find({ publisherId: { $in: id } }).exec();
    }

    public async findById(id: string, zone: string): Promise<BillDTO> {
        return await this.getBillModel(zone).findById(id).exec();
    }

    public async findByBillNumber(billNumber: any, zone: string): Promise<BillDTO> {
        return await this.getBillModel(zone).findOne({ billNumber, deleted: false }).exec();
    }

    // get the total amount based on subcampaign Id
    public async getAmount(assetID: any, startDate: Date, endDate: Date, zone: string): Promise<number> {
        const matchCriteria: any = {
            publisherACSID: assetID,
            dateAndTime: { $gte: startDate, $lt: endDate }
        };

        const amountData = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: matchCriteria
            },
            {
                $group: {
                    _id: { publisherACSID: '$assetID' },
                    totalAmount: {
                        $sum: {
                            $cond: [
                                { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
                            ]
                        }
                    }
                }
            }
        ])
        .allowDiskUse(true)
        .exec();

        let totalAmount = 0;
        if (amountData.length > 0) {
            totalAmount = amountData[0].totalAmount;
        }
        return totalAmount;
    }

    // tax calculation
    // TODO: calcuation based on countrywise
    public async getTaxAmount(totalAmount: number, zone: string): Promise<number> {
        const countryTaxData: any = await countryTaxModel.find({ country: 'United States' }).exec();
        let taxAmout = 0;
        if (countryTaxData && countryTaxData.length) {
            if (countryTaxData[0].typeOfTax === 'percentage') {
                taxAmout = totalAmount * countryTaxData[0].percentageOfTax / 100;
            } else if (countryTaxData[0].typeOfTax === 'fixed') {
                taxAmout = countryTaxData[0].percentageOfTax;
            }
        }
        return taxAmout;
    }

    // get the total amount based on platform id
    public async getTotalAmountAndBonusAndCutByPlatformId(platformId: string, zone: string): Promise<any> {
        const amountData = await this.getBillModel(zone).aggregate([
            {
                $match: { platformId }
            },
            {
                $group: {
                    _id: null,
                    totalAmount: {
                        $sum: '$amount'
                    },
                    totalBonus: {
                        $sum: '$bonus'
                    },
                    totalCut: {
                        $sum: '$cut'
                    }

                }
            }
        ])
        .allowDiskUse(true)
        .exec();

        if (amountData && amountData.length > 0) {
            return { amount: amountData[0].totalAmount, bonus: amountData[0].totalBonus,
                     cut: amountData[0].totalCut };
        } else {
            return { amount: 0, bonus: 0, cut: 0 };
        }
    }

    // get the total amount based on platform id
    public async getLastBillByPlatformId(platformId: string, zone: string): Promise<BillDTO> {
        const lastBill: any[] = await this.getBillModel(zone).aggregate([
            {
                $match: { platformId }
            },
            {
                $sort: { billEndDate: -1 }
            },
            {
                $limit : 1
            }
        ])
        .allowDiskUse(true)
        .exec();

        if (lastBill && lastBill.length === 1) {
            return lastBill[0];
        } else {
            return null;
        }
    }
    public async billsGroupbyPublisherIdForAdmin(condition: string, zone: string): Promise<any[]> {
        let query: any = {
            $match: {
                deleted: false,
                billStatus: 'Pending'
             }
          };
        if (condition === 'Paid') {
            query = {
                $match: {
                    deleted: false,
                    billStatus: 'Paid'
                 }
              };
        }
        if (condition === 'All') {
            query = {
                $match: {
                    deleted: false
                 }
              };
        }

        const publishers: any = await this.getBillModel(zone).aggregate([
            query,
            {
                $group: {
                    _id: {
                        publisherId: '$publisherId',
                    },
                    data: {
                    $push: {
                        billId: '$_id',
                        platformId: '$platformId',
                        amount: '$amount',
                        billNumber: '$billNumber',
                        billDate: '$billDate',
                        billStartDate: '$billStartDate',
                        billEndDate: '$billEndDate',
                        billStatus: '$billStatus',
                        transactionNumber: '$transactionNumber',
                        paidDate: '$paidDate',
                        count: { $sum: 1 }
                    }
                    }
                }

            }
        ]).exec();
        return publishers;
    }

    private getBillModel(zone: string) {
        if (zone === '1') {
            return usBillModel;
        } else if (zone === '2') {
            return indiaBillModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    private getAdServerContentModel(zone: string) {
        if (zone === '1') {
            return usAdServerContentModel;
        } else if (zone === '2') {
            return indiaAdServerContentModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
}
