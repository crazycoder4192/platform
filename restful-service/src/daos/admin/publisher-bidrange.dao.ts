import BidRangeDTO from '../../dtos/admin/publisher-bidrange.dto';
import { publisherRangeModel } from '../../models/admin/publisher-bid-range.schema';

export default class PublisherBidRangeDAO {
  public async create(bidRangeDTO: BidRangeDTO): Promise<BidRangeDTO> {
    const bidRange = new publisherRangeModel(bidRangeDTO);
    return await bidRange.save();
  }

  public async getAll(): Promise<BidRangeDTO[]> {
    return await publisherRangeModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<BidRangeDTO> {
    return await publisherRangeModel.findById(id).exec();
  }

  public async update(id: string, bidRange: BidRangeDTO): Promise<BidRangeDTO> {
    const updateBidRange = await publisherRangeModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updateBidRange, bidRange);
    return await updateBidRange.save();
  }

  public async delete(id: string, bidRange: BidRangeDTO): Promise<BidRangeDTO> {
    return await publisherRangeModel.findByIdAndUpdate(id, bidRange, { new: true }).exec();
  }

}
