import ConfigEmailDTO from '../../dtos/admin/config-email.dto';
import { configEmailModel } from '../../models/admin/config-email-schema';

export default class ConfigEmailDAO {
  public async create(dto: ConfigEmailDTO): Promise<ConfigEmailDTO> {
    const model = new configEmailModel(dto);
    return await model.save();
  }

  public async update(
    id: string,
    dto: ConfigEmailDTO,
  ): Promise<ConfigEmailDTO> {
    const model = await configEmailModel.findById(id).exec();
    Object.assign(model, dto);
    return await model.save();
  }

  public async find(): Promise<ConfigEmailDTO[]> {
    return await configEmailModel.find().exec();
  }

  public async findById(id: string): Promise<ConfigEmailDTO> {
    return await configEmailModel.findById(id).exec();
  }

  public async findByActiveState(): Promise<ConfigEmailDTO> {
    return await configEmailModel.findById({ activestate: 'true' }).exec();
  }
}
