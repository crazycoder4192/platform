import ConfigSchedulePaymentDTO from '../../dtos/admin/config-schedulepayments.dtos';
import { configSchedulePaymentsModel } from '../../models/admin/config-schedulepayments-schema';

export default class ConfigSchedulePaymentDAO {
  public async create(dto: ConfigSchedulePaymentDTO): Promise<ConfigSchedulePaymentDTO> {
    const model = new configSchedulePaymentsModel(dto);
    return await model.save();
  }

  public async update(
    id: string,
    dto: ConfigSchedulePaymentDTO,
  ): Promise<ConfigSchedulePaymentDTO> {
    const model = await configSchedulePaymentsModel.findById(id).exec();
    Object.assign(model, dto);
    return await model.save();
  }

  public async getAll(): Promise<ConfigSchedulePaymentDTO[]> {
    return await configSchedulePaymentsModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<ConfigSchedulePaymentDTO> {
    return await configSchedulePaymentsModel.findById(id).exec();
  }

  public async delete(id: string, schedulePayment: ConfigSchedulePaymentDTO): Promise<ConfigSchedulePaymentDTO> {
    return await configSchedulePaymentsModel.findByIdAndUpdate(id, schedulePayment, { new: true }).exec();
  }

}
