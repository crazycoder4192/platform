import CreditVoucherDTO from '../../dtos/admin/credit-voucher.dto';
import { creditVoucherModel } from '../../models/admin/credit-voucher.schema';

export default class CreditVoucherDAO {

  public async create(creditVoucherDTO: CreditVoucherDTO): Promise<CreditVoucherDTO> {
    const creditVoucher = new creditVoucherModel(creditVoucherDTO);
    return await creditVoucher.save();
  }

  public async getAll(): Promise<CreditVoucherDTO[]> {
    return await creditVoucherModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<CreditVoucherDTO> {
    return await creditVoucherModel.findById(id).exec();
  }

  public async update(id: string, creditVoucher: CreditVoucherDTO): Promise<CreditVoucherDTO> {
    const updateCreditVoucher = await creditVoucherModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updateCreditVoucher, creditVoucher);
    return await updateCreditVoucher.save();
  }

  public async delete(id: string, creditVoucher: CreditVoucherDTO): Promise<CreditVoucherDTO> {
    return await creditVoucherModel.findByIdAndUpdate(id, creditVoucher, { new: true }).exec();
  }

}
