import UserDAO from '../../daos/user.dao';
import CountryTaxDTO from '../../dtos/admin/country-tax.dto';
import { countryTaxModel } from '../../models/admin/country-tax-schema';

export default class CountryTaxDAO {
  private readonly userDAO: UserDAO;

  public async create(countryTaxDTO: CountryTaxDTO): Promise<CountryTaxDTO> {
    const countryTax = new countryTaxModel(countryTaxDTO);
    return await countryTax.save();
  }

  public async getAll(): Promise<CountryTaxDTO[]> {
    return await countryTaxModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<CountryTaxDTO> {
    return await countryTaxModel.findById(id).exec();
  }

  public async update(id: string, countryTax: CountryTaxDTO): Promise<CountryTaxDTO> {
    const updateCountryTax = await countryTaxModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updateCountryTax, countryTax);
    return await updateCountryTax.save();
  }

  public async delete(id: string, countryTax: CountryTaxDTO): Promise<CountryTaxDTO> {
    return await countryTaxModel.findByIdAndUpdate(id, countryTax, { new: true }).exec();
  }

}
