import PermissionDTO from '../../dtos/admin/permission.dto';
import { permissionModel } from '../../models/admin/permission-schema';

export default class PermissionDAO {
  public async createMany(details: PermissionDTO[]): Promise<PermissionDTO[]> {
    return await permissionModel.insertMany(details);
  }

  public async findAll(): Promise<PermissionDTO[]> {
    return await permissionModel.find().exec();
  }

  public async findByModule(moduleName: string): Promise<any[]> {
    return await permissionModel
      .aggregate([
        {
          $match: { moduleType: moduleName }
        },
        {
          $group:
          {
            _id: '$tabName',
            items: {
              $addToSet: {
                actionName: '$actionName',
                identifier: '$identifier',
                id: '$_id'
              }
            }
          }
        }
      ])
      .exec();
  }

  public async findById(id: string): Promise<PermissionDTO> {
    return await permissionModel.findById(id).exec();
  }

  public async findByIds(permissionIds: string[]): Promise<PermissionDTO[]> {
    return await permissionModel.find({ _id: { $in: permissionIds } }).exec();
  }
}
