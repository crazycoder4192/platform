import config from 'config';
import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { AdServerRequestURLModel } from '../../models/utils/adserverrequestURL-schema';

export default class DashboardDAO {

    public async getLifetimeSpendsOfTop5AdvertisersForAdmin(zone: string) {
        return await this.getAdServerContentModel(zone).aggregate([
            {
                $match: { }
            },
            {
                $group: {
                    _id: '$advertiserID',
                    totalAmount: {
                      $sum: {
                        $convert: {
                          input: {
                            $cond: [
                              { $eq: ['$typeOfEvent', '$bidType'] },
                              '$bidAmount',
                              0
                            ]
                          },
                          to: 'double'
                        }
                      }
                    }
              }
            },
            { $sort: { totalAmount: -1 } },
        ]).limit(5).exec();
    }

    public async getLifetimeSpendsOfTopAdvertisersForAdmin(zone: string) {
        return await this.getAdServerContentModel(zone).aggregate([
            {
                $match: { }
            },
            {
              $group: {
                  _id: '$advertiserID',
                  totalAmount: {
                    $sum: {
                      $convert: {
                        input: {
                          $cond: [
                            { $eq: ['$typeOfEvent', '$bidType'] },
                            '$bidAmount',
                            0
                          ]
                        },
                        to: 'double'
                      }
                    }
                  }
              }
            },
            { $sort: { totalAmount: -1 } },
        ]).exec();
    }

    public async getLifetimeEarningsOfTop5PublishersForAdmin(zone: string) {
        const publisherPercentage: number = config.get<number>('docereeProfitCalculationValue');
        return await this.getAdServerContentModel(zone).aggregate([
          {
              $match: { }
          },
          {
            $group: {
                _id: '$publisherID',
                amount: {
                  $sum: {
                    $convert: {
                      input: {
                        $cond: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          '$bidAmount',
                          0
                        ]
                      },
                      to: 'double'
                    }
                  }
                }
            }
          },
          { $project: { totalAmount: { $divide: [ { $multiply: ['$amount', publisherPercentage] }, 100 ] } } },
          { $sort: { totalAmount: -1 } },
      ]).limit(5).exec();
    }

    public async getLifetimeEarningsOfTopPublishersForAdmin(zone: string) {
        const publisherPercentage: number = config.get<number>('docereeProfitCalculationValue');
        return await this.getAdServerContentModel(zone).aggregate([
            {
                $match: { }
            },
            {
                $group: {
                    _id: '$publisherID',
                    amount: {
                      $sum: {
                        $convert: {
                          input: {
                            $cond: [
                              { $eq: ['$typeOfEvent', '$bidType'] },
                              '$bidAmount',
                              0
                            ]
                          },
                          to: 'double'
                        }
                      }
                    }
              }
            },
            { $project: { totalAmount: { $divide: [ { $multiply: ['$amount', publisherPercentage] }, 100 ] } } },
            { $sort: { totalAmount: -1 } },
        ]).allowDiskUse(true).exec();
    }

    public async getLifetimeEarningsOfTop5AssetsForAdmin(zone: string) {
      const publisherPercentage: number = config.get<number>('docereeProfitCalculationValue');
      return await this.getAdServerContentModel(zone).aggregate([
          {
              $match: { }
          },
          {
              $group: {
                  _id: '$publisherACSID',
                  amount: {
                    $sum: {
                      $convert: {
                        input: {
                          $cond: [
                            { $eq: ['$typeOfEvent', '$bidType'] },
                            '$bidAmount',
                            0
                          ]
                        },
                        to: 'double'
                      }
                    }
                  }
            }
          },
          { $project: { totalAmount: { $divide: [ { $multiply: ['$amount', publisherPercentage] }, 100 ] } } },
          { $sort: { totalAmount: -1 } },
      ]).limit(5).exec();
  }

    public async getLifetimeSpendsOfTop5CampaignsForAdmin(zone: string) {
      return await this.getAdServerContentModel(zone).aggregate([
          {
              $match: { }
          },
          {
              $group: {
                  _id: '$advertiserCampID',
                  totalAmount: {
                    $sum: {
                      $convert: {
                        input: {
                          $cond: [
                            { $eq: ['$typeOfEvent', '$bidType'] },
                            '$bidAmount',
                            0
                          ]
                        },
                        to: 'double'
                      }
                    }
                  }
            }
          },
          { $sort: { totalAmount: -1 } },
      ]).allowDiskUse(true).limit(5).exec();
    }

    public async getLifeTimeReachforCampaigns(campaignIds: any[], zone: string) {
      const reaches = await this.getAdServerContentModel(zone).aggregate([
        {
          $match: {
            advertiserCampID: { $in: campaignIds },
            typeOfEvent: 'CPM'
          }
        },
        {
          $project: {
            hcpId: 1,
            advertiserCampID: 1
          }
        },
        {
          $group: {
            _id: '$advertiserCampID', HcpId: { $addToSet: '$hcpId' }
          }
        },
        {
          $unwind: '$HcpId'
        },
        {
          $group: { _id: '$_id', reach: { $sum: 1 } }
        }
      ]).allowDiskUse(true).exec();
      return reaches;
    }
    public async getLifetimeEaringnsOfPublishersByPublisherIdsForAdmin(ids: string[], zone: string) {
      const publisherPercentage: number = config.get<number>('docereeProfitCalculationValue');
      return await this.getAdServerContentModel(zone).aggregate([
          {
              $match: {
                platformID: { $in: ids }
               }
          },
          {
              $group: {
                  _id: '$platformID',
                  amount: {
                    $sum: {
                      $convert: {
                        input: {
                          $cond: [
                            { $eq: ['$typeOfEvent', '$bidType'] },
                            '$bidAmount',
                            0
                          ]
                        },
                        to: 'double'
                      }
                    }
                  }
            }
          },
          { $project: { totalAmount: { $divide: [ { $multiply: ['$amount', publisherPercentage] }, 100 ] } } },
          { $sort: { totalAmount: -1 } },
      ]).allowDiskUse(true).exec();
    }

    public async getLifetimeAssetUtilizationsOfTop5AssetsForAdmin(assetIds: any[]) {
      return await AdServerRequestURLModel.aggregate([
        {
          $match: {
            assetId: { $in: assetIds }
          }
        },
        {
          $group: {
            _id: '$assetId',
            total: { $sum: 1 },
            success: {
              $sum: {
                $cond: [
                  { $eq: ['$responseStatus', '200'] },
                  1,
                  0
                ]
              }
            },
            failed: {
              $sum: {
                $cond: [
                  { $eq: ['$responseStatus', '500'] },
                  1,
                  0
                ]
              }
            }
          }
        },
        { $project: { utilizepercent: { $multiply: [ { $divide: [ '$success', '$total'] }, 100 ] } } },
      ]).allowDiskUse(true).exec();
  }

    private getAdServerContentModel(zone: string) {
    if (zone === '1') {
      return usAdServerContentModel;
    } else if (zone === '2') {
      return indiaAdServerContentModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }
}
