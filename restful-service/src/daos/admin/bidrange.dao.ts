import UserDAO from '../../daos/user.dao';
import BidRangeDTO from '../../dtos/admin/bidrange.dto';
import { bidRangeModel } from '../../models/admin/bidrange.schema';

export default class BidRangeDAO {
  private readonly userDAO: UserDAO;

  public async create(bidRangeDTO: BidRangeDTO): Promise<BidRangeDTO> {
    const bidRange = new bidRangeModel(bidRangeDTO);
    return await bidRange.save();
  }

  public async getAll(): Promise<BidRangeDTO[]> {
    return await bidRangeModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<BidRangeDTO> {
    return await bidRangeModel.findById(id).exec();
  }

  public async update(id: string, bidRange: BidRangeDTO): Promise<BidRangeDTO> {
    const updateBidRange = await bidRangeModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updateBidRange, bidRange);
    return await updateBidRange.save();
  }

  public async delete(id: string, bidRange: BidRangeDTO): Promise<BidRangeDTO> {
    return await bidRangeModel.findByIdAndUpdate(id, bidRange, { new: true }).exec();
  }

}
