import PublisherSiteWeightageDTO from '../../dtos/admin/publisher-site-weightage.dto';
import { publisherSiteWeightageModel } from '../../models/admin/publisher-site-weightage.schema';

export default class PublisherSiteWeightageDAO {
  public async create(publisherSiteWeightageDTO: PublisherSiteWeightageDTO): Promise<PublisherSiteWeightageDTO> {
    const publisherSiteWeightage = new publisherSiteWeightageModel(publisherSiteWeightageDTO);
    return await publisherSiteWeightage.save();
  }

  public async getAll(): Promise<PublisherSiteWeightageDTO[]> {
    return await publisherSiteWeightageModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<PublisherSiteWeightageDTO> {
    return await publisherSiteWeightageModel.findById(id).exec();
  }

  public async update(id: string, publisherSiteWeightage: PublisherSiteWeightageDTO): Promise<PublisherSiteWeightageDTO> {
    const updatePublisherSiteWeightage = await publisherSiteWeightageModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updatePublisherSiteWeightage, publisherSiteWeightage);
    return await updatePublisherSiteWeightage.save();
  }

  public async delete(id: string, publisherSiteWeightage: PublisherSiteWeightageDTO): Promise<PublisherSiteWeightageDTO> {
    return await publisherSiteWeightageModel.findByIdAndUpdate(id, publisherSiteWeightage, { new: true }).exec();
  }

}
