import CountryCurrencyDTO from '../../dtos/admin/country-currency.dto';
import { countryCurrencyModel } from '../../models/admin/country-currency.schema';

export default class CountryCurrencyDAO {

  public async create(countryCurrencyDTO: CountryCurrencyDTO): Promise<CountryCurrencyDTO> {
    const countryCurrency = new countryCurrencyModel(countryCurrencyDTO);
    return await countryCurrency.save();
  }

  public async getAll(): Promise<CountryCurrencyDTO[]> {
    return await countryCurrencyModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<CountryCurrencyDTO> {
    return await countryCurrencyModel.findById(id).exec();
  }

  public async update(id: string, countryCurrency: CountryCurrencyDTO): Promise<CountryCurrencyDTO> {
    const updateCountryCurrency = await countryCurrencyModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updateCountryCurrency, countryCurrency);
    return await updateCountryCurrency.save();
  }

  public async delete(id: string, countryCurrency: CountryCurrencyDTO): Promise<CountryCurrencyDTO> {
    return await countryCurrencyModel.findByIdAndUpdate(id, countryCurrency, { new: true }).exec();
  }

}
