import RoleDTO from '../../dtos/admin/role.dto';
import { roleModel } from '../../models/admin/roles-schema';

export default class RoleDAO {

  public async create(dto: RoleDTO): Promise<RoleDTO> {
    const createDTO = new roleModel(dto);
    return await createDTO.save();
  }

  public async getAllRoles(): Promise<RoleDTO[]> {
    return await roleModel
      .aggregate()
      .lookup({
        from: 'permissions',
        localField: 'permissions',
        foreignField: '_id',
        as: 'permissionDetails',
      })
      .allowDiskUse(true)
      .exec();
  }

  public async getRoleById(id: string): Promise<RoleDTO> {
    return await roleModel.findById(id).exec();
  }

  public async getRolesByIds(ids: string[]): Promise<RoleDTO[]> {
    return await roleModel.find({ _id: { $in: ids } }).exec();
  }

  public async getRolesByModule(moduleName: string): Promise<RoleDTO[]> {
    return await roleModel
      .aggregate()
      .match({ moduleType: moduleName })
      .exec();
  }

  public async getRoleByModuleAndRoleName(moduleName: string, userRole: string): Promise<RoleDTO> {
    return await roleModel.findOne({ moduleType: moduleName, roleName:  userRole }).exec();
  }

  public async getRoleByModuleAndPermissionName(moduleName: string, userRole: string): Promise<RoleDTO> {
    return await roleModel.findOne({ moduleType: moduleName, roleName:  userRole }).exec();
  }

  public async update(id: string, dto: RoleDTO): Promise<RoleDTO> {
    const updateDTO = await roleModel.findById(id).exec();
    Object.assign(updateDTO, dto);
    return await updateDTO.save();
  }
}
