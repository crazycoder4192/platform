import config from 'config';
import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import ArchetypeDAO from '../../daos/utils/archetype.dao';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';

export default class AnalyticsDAO {

    private readonly helper: Helper;
    private readonly archetypeDAO: ArchetypeDAO;
    private readonly subCampaignDAO: SubCampaignDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;

    constructor() {
      this.helper = new Helper();
      this.archetypeDAO = new ArchetypeDAO();
      this.subCampaignDAO = new SubCampaignDAO();
      this.advertiserBrandDAO = new AdvertiserBrandDAO();
    }

    public async getAdvertiserBrandsAndTherapeuticAreas(filter: FilterAnalyticsDTO, zone: string) {

        const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
        const startDate = u.startDate;
        const endDate = u.endDate;

        const data = await this.getAdServerContentModel(zone).find({ dateAndTime: { $gte: startDate, $lte: endDate } }).distinct('brandID').exec();

        return await this.advertiserBrandDAO.getBrandTypesByBrandIdsForAdmin(data, zone);
    }

    public async getAdvertiserTopCampaign(filter: FilterAnalyticsDTO, zone: string) {
        const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
        const startDate = u.startDate;
        const endDate = u.endDate;
        const data = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    dateAndTime: { $gte: startDate, $lte: endDate }
                 }
            },
            {
                $group: {
                    _id: { subCampId : '$advertiserCampID', typeOfAdvertisement: '$typeOfAdvertisement' },
                    totalAmount: {
                      $sum: {
                        $convert: {
                          input: {
                            $cond: [
                              { $eq: ['$typeOfEvent', '$bidType'] },
                              '$bidAmount',
                              0
                            ]
                          },
                          to: 'double'
                        }
                      }
                    },
                    typeOfAdvertisement: { $first: '$typeOfAdvertisement' },
                    uniqueReach: {
                        $addToSet: '$hcpId'
                    }
                }
            },
            {
          $unwind: '$uniqueReach'
        },
            {
          $group: { _id: '$_id.subCampId' ,
                    uniqueReach: { $sum: 1 },
                    typeOfAd: { $first: '$_id.typeOfAdvertisement' },
                    amount: { $first: '$totalAmount' }
                }
            },
            {
                $group: {
                    _id: '$typeOfAd' ,
                    data :
                    {
                        $push: {
                            subCampId: '$_id',
                            uniqueReach: '$uniqueReach',
                            spends: '$amount'
                        }
                    }
                }
            }
        ]).allowDiskUse(true).exec();
        return data;
    }

    public async getAdvertiserTopBrands(filter: FilterAnalyticsDTO, zone: string) {
        const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
        const startDate = u.startDate;
        const endDate = u.endDate;
        return await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    dateAndTime: { $gte: startDate, $lte: endDate }
                 }
            },
            {
                $group: {
                    _id: '$brandID',
                    totalAmount: {
                      $sum: {
                        $convert: {
                          input: {
                            $cond: [
                              { $eq: ['$typeOfEvent', '$bidType'] },
                              '$bidAmount',
                              0
                            ]
                          },
                          to: 'double'
                        }
                      }
                    },
                    uniqueSubCampaign: {
                        $addToSet: '$advertiserCampID'
                    },
                    uniqueReach: {
                        $addToSet: '$hcpId'
                    }
                }
            },
            {
                $project : {
                    _id: '$_id',
                    subcampaignCount: { $size: '$uniqueSubCampaign' },
                    uniqueReach: { $size: '$uniqueReach' },
                    spends: '$totalAmount'
                }
            }]).allowDiskUse(true).exec();
    }

    public async getAdvertiserTypeOfAds(filter: FilterAnalyticsDTO, zone: string) {
        const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
        const startDate = u.startDate;
        const endDate = u.endDate;
        const data = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    dateAndTime: { $gte: startDate, $lte: endDate }
                 }
            },
            {
                $group: {
                    _id: { subCampId : '$advertiserCampID', typeOfAdvertisement: '$typeOfAdvertisement' },
                    totalAmount: {
                      $sum: {
                        $convert: {
                          input: {
                            $cond: [
                              { $eq: ['$typeOfEvent', '$bidType'] },
                              '$bidAmount',
                              0
                            ]
                          },
                          to: 'double'
                        }
                      }
                    },
                    typeOfAdvertisement: { $first: '$typeOfAdvertisement' },
                    uniqueReach: {
                        $addToSet: '$hcpId'
                    }
                }
            },
            {
          $unwind: '$uniqueReach'
        },
            {
          $group: { _id: '$_id.subCampId' ,
                    uniqueReach: { $sum: 1 },
                    typeOfAd: { $first: '$_id.typeOfAdvertisement' },
                    amount: { $first: '$totalAmount' }
                }
            },
            {
                $group: {
                    _id: '$typeOfAd' ,
                    events :
                    {
                        $push: {
                            campId: '$_id',
                            uniqueReach: '$uniqueReach',
                            spends: '$amount'
                        }
                    }
                }
            }
        ]).allowDiskUse(true).exec();
        return data;
    }

    public async getAdvertiserAdTypes(filter: FilterAnalyticsDTO, zone: string) {
        const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
        const startDate = u.startDate;
        const endDate = u.endDate;
        const data = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    dateAndTime: { $gte: startDate, $lte: endDate }
                 }
            },
            {
                $group: {
                    _id: '$typeOfAdvertisement',
                    totalcount: {
                      $sum: {
                        $convert: {
                          input: {
                            $cond: [
                              { $eq: ['$typeOfEvent', '$bidType'] },
                              1,
                              0
                            ]
                          },
                          to: 'double'
                        }
                      }
                    }
                }
            }
        ]).allowDiskUse(true).exec();
        return data;
    }
    public async getAdvertiserTrends(filter: FilterAnalyticsDTO, timezone: string, zone: string) {

      const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
      const startDate = u.startDate;
      const endDate = u.endDate;
      const unit = u.unit;
      const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);
      const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
      const addOffset: any = this.helper.addOffset(timezone);

      const results: any[] = [];

      for (const x of xAxis) {
        const result: any = { };
        result.durationInstance = x;
        result.subcampaigns = 0;
        result.reaches = 0;
        result.views = 0;
        result.clicks = 0;
        results.push(result);
      }

      const q: any = { };
      q.dateAndTime = { $gte: startDate, $lte: endDate };

      const impressions = await this.getAdServerContentModel(zone).aggregate([
        {
          $match: q
        },
        {
          $project: addOffset
        },
        {
          $project: {
            durationInstance
          }
        },
        {
          $group: {
            _id: '$durationInstance', count: { $sum: 1 }
          }
        }
      ]).exec();

    //  console.log(impressions);

      const data = await this.getAdServerContentModel(zone).aggregate([
          {
            $match: {
                dateAndTime: { $gte: startDate, $lte: endDate },
              }
          },
          {
            $group: {
              _id: null,
              totalAmount: {
                $sum: {
                  $convert: {
                    input: {
                      $cond: [
                        { $eq: ['$typeOfEvent', '$bidType'] },
                        '$bidAmount',
                        0
                      ]
                    },
                    to: 'double'
                  }
                }
              },
              uniqueSubCampaign: {
                  $addToSet: '$advertiserCampID'
              },
              uniqueReach: {
                  $addToSet: '$hcpId'
              },
              clicks: {
                $sum: {
                  $convert: {
                    input: {
                      $cond: [
                        { $eq: ['$typeOfEvent', 'CPC'] },
                        1,
                        0
                      ]
                    },
                    to: 'double'
                  }
                }
              },
            }
          },
          {
            $project: {
              spends: '$totalAmount',
              subcampaigns: { $size: '$uniqueSubCampaign' },
              reach: { $size: '$uniqueReach' },
              clicks: '$clicks'
            }
          }
        ]).allowDiskUse(true).exec();
     // console.log(data);
      return data;
    }

    private getAdServerContentModel(zone: string) {
      if (zone === '1') {
          return usAdServerContentModel;
      } else if (zone === '2') {
          return indiaAdServerContentModel;
      } else {
          throw new Error(`Invalid zone ${zone}`);
      }
  }
}
