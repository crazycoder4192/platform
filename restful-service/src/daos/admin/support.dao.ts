import TicketDTO from '../../dtos/admin/ticket.dto';
import { ticketModel } from '../../models/admin/ticket-schema';

export default class BannerDAO {
    public async createTicket(ticketDTO: TicketDTO): Promise<TicketDTO> {
        const ticket = new ticketModel(ticketDTO);
        return await ticket.save();
    }
    public async getAllTickets(): Promise<TicketDTO[]> {
      return await ticketModel
      .aggregate()
      .lookup({
        from: 'users',
        localField: 'email',
        foreignField: 'email',
        as: 'userDetails',
      })
      .allowDiskUse(true)
      .exec();
    }
    public async getOneTicket(id: string): Promise<TicketDTO> {
      return await ticketModel.findById(id).exec();
    }
    public async update(id: any, dto: TicketDTO): Promise<TicketDTO> {
      return await ticketModel.findByIdAndUpdate(id, dto, { new: true }).exec();
    }
    public async getAllTicketsByEmail(userEmail: string): Promise<TicketDTO[]> {
      return await ticketModel.find({ email: userEmail }).exec();
    }
}
