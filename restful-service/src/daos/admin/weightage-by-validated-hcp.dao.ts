import WeightageByValidatedHcpDTO from '../../dtos/admin/weightage-by-validated-hcp.dto';
import { weightageByValidatedHcpModel } from '../../models/admin/weightage-by-validated-hcp-schema';

export default class WeightageByValidatedHcpDAO {
  public async create(weightageByValidatedHcpDTO: WeightageByValidatedHcpDTO): Promise<WeightageByValidatedHcpDTO> {
    const weightageByvalidatedHcp = new weightageByValidatedHcpModel(weightageByValidatedHcpDTO);
    return await weightageByvalidatedHcp.save();
  }

  public async getAll(): Promise<WeightageByValidatedHcpDTO[]> {
    return await weightageByValidatedHcpModel.find({ deleted: false }).exec();
  }

  public async getById(id: string): Promise<WeightageByValidatedHcpDTO> {
    return await weightageByValidatedHcpModel.findById(id).exec();
  }

  public async update(id: string, weightageByValidatedHcpDAO: WeightageByValidatedHcpDTO): Promise<WeightageByValidatedHcpDTO> {
    const updateWeightageByValidatedHcp = await weightageByValidatedHcpModel.findOne({ _id: id, deleted: false }).exec();
    Object.assign(updateWeightageByValidatedHcp, weightageByValidatedHcpDAO);
    return await updateWeightageByValidatedHcp.save();
  }

  public async delete(id: string, weightageByvalidatedHcp: WeightageByValidatedHcpDTO): Promise<WeightageByValidatedHcpDTO> {
    return await weightageByValidatedHcpModel.findByIdAndUpdate(id, weightageByvalidatedHcp, { new: true }).exec();
  }

}
