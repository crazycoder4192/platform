import BannerDTO from '../../dtos/admin/banner.dto';
import { bannerModel } from '../../models/admin/banner-schema';

export default class BannerDAO {
  public async createMany(details: BannerDTO[]): Promise<BannerDTO[]> {
    return await bannerModel.insertMany(details);
  }

  public async findAll(): Promise<BannerDTO[]> {
    return await bannerModel.find().exec();
  }

  public async findByModule(moduleName: string): Promise<BannerDTO[]> {
    return await bannerModel
      .aggregate([
        {
          $group: {
            _id: '$moduleType',
            children: {
              $push: {
                _id: '$_id',
                tabName: '$tabName',
                actionName: '$actionName',
              },
            },
            count: { $sum: 1 },
          },
        },
      ])
      .exec();
  }

  public async findById(id: string): Promise<BannerDTO> {
    return await bannerModel.findById(id).exec();
  }

  public async findByIds(bannerIds: string[]): Promise<BannerDTO[]> {
    return await bannerModel.find({ _id: { $in: bannerIds } }).exec();
  }
}
