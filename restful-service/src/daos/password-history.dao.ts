import PasswordHistoryDTO from '../dtos/password-history.dto';
import { passwordHistoryModel } from '../models/password-history-schema';
export default class PasswordHistoryDAO {
  public async create(
    passwordHistory: PasswordHistoryDTO
  ): Promise<PasswordHistoryDTO> {
    const saveUser = new passwordHistoryModel(passwordHistory);
    return await saveUser.save();
  }

  public async getLastThreePasswords(
    argEmail: string
  ): Promise<PasswordHistoryDTO[]> {
    return await passwordHistoryModel
      .find({ email: argEmail })
      .sort({ 'created.at': 'desc' })
      .limit(3)
      .exec();
  }

  public async deleteOldPassword(id: string) {
    return await passwordHistoryModel.findByIdAndRemove(id).exec();
  }

}
