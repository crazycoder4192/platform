import { Helper } from '../../common/helper';
import { subCampaignModel } from '../../models/advertiser/sub-campaign-schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { logger } from '../../util/winston';
import SubCampaignDAO from './sub-campaign.dao';

export default class AdvertiserDashBoardDAO {

    private readonly helper: Helper;
    private readonly subcampaignDAO: SubCampaignDAO;

    constructor() {
        this.helper = new Helper();
        this.subcampaignDAO = new SubCampaignDAO();
    }

    public async listOfActiveSubCampaignsLifeTimeDataByBrandIds(id: any, zone: string): Promise<any> {
        const subCampaignList: any = await subCampaignModel.find({ brandId: { $in: id }, deleted: false, status: 'Active' })
            .sort('-created.at').exec();
        const activeCampaignsData: any = [];
        if (subCampaignList) {
            let transactionalData: any;
            for (const subCampaign of subCampaignList) {
                transactionalData = await this.getAdServerContentModel(zone).aggregate(
                    [
                        {
                            $match: { advertiserCampID: subCampaign._id }
                        },
                        {
                            $group: {
                                _id: '$advertiserCampID',
                                totalAmount: {
                                    $sum: {
                                        $convert: {
                                            input: {
                                                $cond: [ { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0 ]
                                            },
                                            to: 'double'
                                        }
                                    }
                                },
                                clicks: {
                                    $sum: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', 'CPC'] }, 1, 0
                                        ]
                                    }
                                },
                                impressions: {
                                    $sum: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', 'CPM'] }, 1, 0
                                        ]
                                    }
                                },
                                views: {
                                    $sum: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', 'CPV'] }, 1, 0
                                        ]
                                    }
                                },
                                uniqueReach: {
                                    $addToSet: '$hcpId'
                                },
                                CPC: {
                                    $avg: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPC'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    }
                                },
                                CPM: {
                                    $avg: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPM'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    }
                                },
                                CPV: {
                                    $avg: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPV'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    }
                                }
                            }
                        },
                        {
                            $project: {
                                impressions: 1, clicks: 1, views: 1, totalAmount: 1,
                                uniqueReach: 1, reachCount: { $size: '$uniqueReach' },
                                CPC: 1, CPM: 1, CPV: 1, CTR: { $multiply: [ { $cond: [ { $eq: [ '$impressions', 0 ] }, 0,
                                                                                       { $divide: ['$clicks', '$impressions'] }]},
                                                                            100 ] }
                            }
                        }]
                ).exec();
                let impressions = 0; let clicks = 0; let views = 0;
                let CPC = 0; let CPM = 0; let CPV = 0;
                let totalAmount = 0; let brandID; let CTR = 0; let reach = 0;
                for (const transData of transactionalData) {
                    brandID = subCampaign.brandID;
                    totalAmount = transData.totalAmount;
                    clicks = transData.clicks;
                    impressions = transData.impressions;
                    views = transData.views;
                    CPC = transData.CPC;
                    CPM = transData.CPM * 1000;
                    CPV = transData.CPV;
                    CTR = transData.CTR;
                    reach = transData.reachCount;
                }

                activeCampaignsData.push({
                    brandId: subCampaign.brandId, name: subCampaign.name, spends: totalAmount,
                    date: subCampaign.created.at, impressions, clicks, CPC, CPM,
                    CPV, CTR, reach, creativeType: subCampaign.creativeType,
                    creativeSpecifications: subCampaign.creativeSpecifications,
                    campaignId: subCampaign.campaignId, subCampaignId: subCampaign._id
                });
            }
        }
        return await activeCampaignsData;
    }

    public async getAllTransactionalDataOfSubCampaign(id: any, zone: string, logOutTime?: any, scenario?: any): Promise<any[]> {

        let query: any = {
           advertiserCampID: id
        };
        if (logOutTime && scenario) {
            if (scenario === 'Before') {
                query = {
                    advertiserCampID: id,
                    dateAndTime: { $lte: logOutTime }
                };
            } else if (scenario === 'After') {
                query = {
                    advertiserCampID: id,
                    dateAndTime: { $gt: logOutTime }
                };
            }
        }
        return await this.getAdServerContentModel(zone).aggregate([
            { $match : query },
            {
                $group: {
                    _id: '$advertiserCampID',
                    totalAmount: {
                        $sum: {
                            $cond: [
                                { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
                            ]
                        }
                    },
                    clicks: {
                        $sum: {
                            $cond: [
                                { $eq: ['$typeOfEvent', 'CPC'] }, 1, 0
                            ]
                        }
                    },
                    impressions: {
                        $sum: {
                            $cond: [
                                { $eq: ['$typeOfEvent', 'CPM'] }, 1, 0
                            ]
                        }
                    },
                    views: {
                        $sum: {
                            $cond: [
                                { $eq: ['$typeOfEvent', 'CPV'] }, 1, 0
                            ]
                        }
                    },
                    uniqueReach: {
                        $addToSet: '$hcpId'
                    },
                    CPC: {
                        $avg: {
                            $cond: [
                                {
                                    $and: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        { $eq: ['$bidType', 'CPC'] }
                                    ]
                                },
                                '$bidAmount',
                                null
                            ]
                        }
                    },
                    CPM: {
                        $avg: {
                            $cond: [
                                {
                                    $and: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        { $eq: ['$bidType', 'CPM'] }
                                    ]
                                },
                                '$bidAmount',
                                null
                            ]
                        }
                    },
                    CPV: {
                        $avg: {
                            $cond: [
                                {
                                    $and: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        { $eq: ['$bidType', 'CPV'] }
                                    ]
                                },
                                '$bidAmount',
                                null
                            ]
                        }
                    }
                }
            },
            {
                $project: {
                    _id: 1,
                    totalAmount: 1,
                    clicks: 1,
                    impressions: 1,
                    views: 1,
                    uniqueReach: 1,
                    reachCount: {
                        $size: '$uniqueReach'
                    },
                    CPC: 1,
                    CPM: {
                        $multiply: [ '$CPM', 1000 ]
                    },
                    CPV: 1,
                    CTR: {
                        $multiply: [
                            { $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] },
                            100
                        ]
                    }
                }
            }]
        ).exec();
    }

    public async getSubCampaignTransactionalData(id: any, zone: string, logOutTime?: any): Promise<any> {
        const activeCampaignsData: any = [];
        let query: any = {
            $match: { advertiserCampID: id }
        };
        if (logOutTime) {
            query = {
                $match: { advertiserCampID: id, dateAndTime: { $gte: logOutTime } }
            };
        }
        const transactionalData = await this.getAdServerContentModel(zone).aggregate(
            [
                query,
                {
                    $group: {
                        _id: '$advertiserCampID',
                        totalAmount: {
                            $sum: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        clicks: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPC'] }, 1, 0
                                ]
                            }
                        },
                        impressions: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPM'] }, 1, 0
                                ]
                            }
                        },
                        views: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPV'] }, 1, 0
                                ]
                            }
                        },
                        uniqueReach: {
                            $addToSet: '$hcpId'
                        },
                        CPC: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPC'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPM: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPM'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPV: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPV'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        totalAmount: 1, clicks: 1, impressions: 1, views: 1, uniqueReach: 1,
                        reachCount: {
                            $size: '$uniqueReach'
                        },
                        CPC: 1, CPM: 1, CPV: 1,
                        CTR: {
                            $multiply: [
                                { $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] },
                                100
                            ]
                        }
                    }
                }]
        ).exec();
        let impressions = 0; let clicks = 0; let views = 0;
        let CPC = 0; let CPM = 0; let CPV = 0; let CTR = 0; let totalAmount = 0; let reach = 0;
        for (const transData of transactionalData) {
            totalAmount = transData.totalAmount;
            clicks = transData.clicks;
            impressions = transData.impressions;
            views = transData.views;
            CPC = transData.CPC;
            CPM = transData.CPM * 1000;
            CPV = transData.CPV;
            CTR = transData.CTR;
            reach = transData.reachCount;
        }
        activeCampaignsData.push({
            spends: totalAmount, impressions, clicks, views, CTR, CPC, CPM, CPV, reach
        });
        return await activeCampaignsData;
    }

    public async getSubCampaignDataForCards(subcampaignIds: any, logOutTime: any, zone: string): Promise<any> {
        let totalCPC = 0; let totalCPM = 0; let totalCPV = 0; let totalCTR = 0;
        let totalClicks = 0; let totalImpressions = 0; let totalViews = 0;
        let totalSpends = 0; let totalReach = 0;
        const transactionalData = await this.getAdServerContentModel(zone)
            .aggregate([
                {
                    $match: { advertiserCampID: { $in: subcampaignIds } }
                },
                {
                    $group: {
                        _id: null,
                        totalAmount: {
                            $sum: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        clicks: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPC'] }, 1, 0
                                ]
                            }
                        },
                        impressions: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPM'] }, 1, 0
                                ]
                            }
                        },
                        views: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPV'] }, 1, 0
                                ]
                            }
                        },
                        uniqueReach: {
                            $addToSet: '$hcpId'
                        },
                        CPC: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPC'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPM: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPM'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPV: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPV'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        clicks: 1, impressions: 1, views: 1, totalAmount: 1,
                        uniqueReach: 1, reachCount: { $size: '$uniqueReach' },
                        CPC: 1, CPM: 1, CPV: 1,
                        CTR: { $multiply: [{ $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] }, 100] }
                    }
                }
            ]).exec();

        for (const transData of transactionalData) {
            totalSpends += transData.totalAmount;
            totalClicks += transData.clicks;
            totalImpressions += transData.impressions;
            totalViews += transData.views;
            totalCPC += transData.CPC;
            totalCPM += transData.CPM * 1000;
            totalCPV += transData.CPV;
            totalCTR += transData.CTR;
            totalReach += transData.reachCount;
        }

        let totalCPCTillLastLogin = 0; let totalCPMTillLastLogin = 0; let totalCPVTillLastLogin = 0; let totalCTRTillLastLogin = 0;
        let totalClicksTillLastLogin = 0; let totalImpressionsTillLastLogin = 0; let totalViewsTillLastLogin = 0;
        let totalSpendsTillLastLogin = 0; let totalReachTillLastLogin = 0;
        const transactionalDataAfterLogout = await this.getAdServerContentModel(zone).aggregate(
            [
                {
                    $match: { advertiserCampID: { $in: subcampaignIds }, dateAndTime: { $lte: logOutTime } }
                },
                {
                    $group: {
                        _id: null,
                        totalAmount: {
                            $sum: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', '$bidType'] },
                                            '$bidAmount',
                                            0
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        clicks: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPC'] },
                                    1,
                                    0
                                ]
                            }
                        },
                        impressions: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPM'] },
                                    1,
                                    0
                                ]
                            }
                        },
                        views: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPV'] },
                                    1,
                                    0
                                ]
                            }
                        },
                        uniqueReach: {
                            $addToSet: '$hcpId'
                        },
                        CPC: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPC'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPM: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPM'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPV: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPV'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        clicks: 1, impressions: 1, views: 1, totalAmount: 1,
                        uniqueReach: 1, reachCount: { $size: '$uniqueReach' },
                        CPC: 1, CPM: 1, CPV: 1,
                        CTR: { $multiply: [{ $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] }, 100] }
                    }
                }
            ]).exec();
        if (transactionalDataAfterLogout) {
            for (const trans of transactionalDataAfterLogout) {
                totalSpendsTillLastLogin += trans.totalAmount;
                totalClicksTillLastLogin += trans.clicks;
                totalImpressionsTillLastLogin += trans.impressions;
                totalViewsTillLastLogin += trans.views;
                totalCPCTillLastLogin += trans.CPC;
                totalCPMTillLastLogin += trans.CPM * 1000;
                totalCPVTillLastLogin += trans.CPV;
                totalCTRTillLastLogin += trans.CTR;
                totalReachTillLastLogin += trans.reachCount;
            }
        }

        const subcampaignWithActivatedOn: any[] = await this.subcampaignDAO.findActiveSubcampaigns(subcampaignIds);

        const totalActiveSubCampaigns: number = subcampaignWithActivatedOn.length;
        let subcampaignActivatedAfterLogout: number = 0;
        for (const s of subcampaignWithActivatedOn) {
            if (s.activated && s.activated.at) {
                if (s.activated.at.getTime() > logOutTime.getTime()) {
                    subcampaignActivatedAfterLogout = subcampaignActivatedAfterLogout + 1;
                }
            }
        }

        let increasedImpressions = 0;
        if (totalImpressions > 0 && totalImpressionsTillLastLogin > 0) {
            increasedImpressions = (totalImpressions - totalImpressionsTillLastLogin) / totalImpressions * 100;
        }
        let increasedClicks = 0;
        if (totalClicks > 0 && totalClicksTillLastLogin > 0) {
            increasedClicks = (totalClicks - totalClicksTillLastLogin) / totalClicks * 100;
        }
        let increasedViews = 0;
        if (totalViews > 0 && totalViewsTillLastLogin > 0) {
            increasedViews = (totalViews - totalViewsTillLastLogin) / totalViews * 100;
        }
        let increasedSpends = 0;
        if (totalSpends > 0 && totalSpendsTillLastLogin > 0) {
            increasedSpends = ((totalSpends - totalSpendsTillLastLogin) / totalSpends) * 100;
        }
        const increasedActiveCampaigns = (subcampaignActivatedAfterLogout) / totalActiveSubCampaigns * 100;

        let increasedCPC = 0;
        if (totalCPC > 0 && totalCPCTillLastLogin > 0) {
            increasedCPC = (totalCPCTillLastLogin - totalCPC) / totalCPC * 100;
        }
        let increasedCPM = 0;
        if (totalCPM > 0 && totalCPMTillLastLogin > 0) {
            increasedCPM = (totalCPMTillLastLogin - totalCPM) / totalCPM * 100;
        }
        let increasedCPV = 0;
        if (totalCPV > 0 && totalCPVTillLastLogin > 0) {
            increasedCPV = (totalCPVTillLastLogin - totalCPV) / totalCPV * 100;
        }
        let increasedCTR = 0;
        if (totalCTR > 0 && totalCTRTillLastLogin > 0) {
            increasedCTR = (totalCTRTillLastLogin - totalCTR) / totalCTR * 100;
        }
        let increasedReach = 0;
        if (totalReach > 0 && totalReachTillLastLogin > 0) {
            increasedReach = (totalReach - totalReachTillLastLogin) / totalReach * 100;
        }

        let transactionalDataforCards: any = { };
        transactionalDataforCards = {
            increasedImpressions, increasedClicks, increasedViews, increasedSpends,
            increasedActiveCampaigns, increasedCPC, increasedCPM, increasedCPV,
            increasedCTR, increasedReach, totalActiveCampaigns: totalActiveSubCampaigns, totalSpends, totalImpressions, totalClicks,
            totalViews, totalCPC, totalCPM, totalCPV, totalCTR, totalReach
        };
        return await transactionalDataforCards;
    }
    public async getSubCampaignDataForAnalyticsCards(subcampaignIds: any, startDate: any, endDate: any, zone: string): Promise<any> {
        let totalCPC = 0; let totalCPM = 0; let totalCPV = 0; let totalCTR = 0;
        let totalClicks = 0; let totalImpressions = 0; let totalViews = 0;
        let totalSpends = 0; let totalReach = 0;
        const sDate = new Date(
            new Date(startDate).getFullYear(),
            new Date(startDate).getMonth(),
            new Date(startDate).getDate(),
            new Date(startDate).getHours(),
            new Date(startDate).getMinutes(),
            new Date(startDate).getSeconds()
        );
        const eDate = new Date(
            new Date(endDate).getFullYear(),
            new Date(endDate).getMonth(),
            new Date(endDate).getDate(),
            new Date(endDate).getHours(),
            new Date(endDate).getMinutes(),
            new Date(endDate).getSeconds()
        );
        const transactionalData = await this.getAdServerContentModel(zone)
            .aggregate([
                {
                    $match: { advertiserCampID: { $in: subcampaignIds }, dateAndTime: { $gte: sDate, $lte: eDate } }
                },
                {
                    $group: {
                        _id: null,
                        totalAmount: {
                            $sum: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        clicks: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPC'] }, 1, 0
                                ]
                            }
                        },
                        impressions: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPM'] }, 1, 0
                                ]
                            }
                        },
                        views: {
                            $sum: {
                                $cond: [
                                    { $eq: ['$typeOfEvent', 'CPV'] }, 1, 0
                                ]
                            }
                        },
                        uniqueReach: {
                            $addToSet: '$hcpId'
                        },
                        CPC: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPC'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPM: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPM'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        },
                        CPV: {
                            $avg: {
                                $convert: {
                                    input: {
                                        $cond: [
                                            {
                                                $and: [
                                                    { $eq: ['$typeOfEvent', '$bidType'] },
                                                    { $eq: ['$bidType', 'CPV'] }
                                                ]
                                            },
                                            '$bidAmount',
                                            null
                                        ]
                                    },
                                    to: 'double'
                                }
                            }
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        clicks: 1, impressions: 1, views: 1, totalAmount: 1,
                        uniqueReach: 1, reachCount: { $size: '$uniqueReach' },
                        CPC: 1, CPM: 1, CPV: 1,
                        CTR: { $multiply: [{ $cond: [ { $eq: [ '$impressions', 0 ] }, 0, { $divide: ['$clicks', '$impressions'] }] }, 100] }
                    }
                }
            ]).exec();

        for (const transData of transactionalData) {
            totalSpends += transData.totalAmount;
            totalClicks += transData.clicks;
            totalImpressions += transData.impressions;
            totalViews += transData.views;
            totalCPC += transData.CPC;
            totalCPM += transData.CPM * 1000;
            totalCPV += transData.CPV;
            totalCTR += transData.CTR;
            totalReach += transData.reachCount;
        }

        let transactionalDataforCards: any = { };
        transactionalDataforCards = {
            totalSpends, totalImpressions, totalClicks,
            totalViews, totalCPC, totalCPM, totalCPV, totalCTR, totalReach
        };
        return await transactionalDataforCards;
    }

    public async getPerformanceResultLineChart(filter: any, timezone: any, zone: string): Promise<any> {

        const u = this.helper.getStartDateEndDateUnitForDashboard(filter, filter.brandStartDate);
        const startDate = u.startDate;
        const endDate = u.endDate;
        const unit = u.unit;
        const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);

        const results: any[] = [];

        for (const x of xAxis) {
            const result: any = { };
            result.durationInstance = x;
            result.avgCPM = 0;
            result.avgCPC = 0;
            result.impressions = 0;
            result.clicks = 0;
            result.spends = 0;
            result.ctr = 0;
            results.push(result);
        }

        const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
        const addOffset: any = this.helper.addOffset(timezone);

        const brandIdList = filter.brandIdList;
        addOffset.bidAmount = '$bidAmount';
        const avgCpmAmounts = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: brandIdList },
                    bidType: 'CPM',
                    dateAndTime: { $gte: startDate, $lte: endDate }
                }
            },
            {
                $project: addOffset
            },
            {
                $project: {
                    durationInstance,
                    projectBidAmount: { $toDouble: '$bidAmount' }
                }
            },
            {
                $group: {
                    _id: '$durationInstance', avg: { $avg: '$projectBidAmount' }
                }
            }
        ]).exec();
        delete addOffset.bidAmount;
        for (const avgCpmAmount of avgCpmAmounts) {
            for (const result of results) {
                if (result.durationInstance === avgCpmAmount._id) {
                    result.avgCPM = avgCpmAmount.avg * 1000;
                    break;
                }
            }
        }

        addOffset.bidAmount = '$bidAmount';
        const avgCpcAmounts = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: brandIdList },
                    bidType: 'CPC',
                    dateAndTime: { $gte: startDate, $lte: endDate }
                }
            },
            {
                $project: addOffset
            },
            {
                $project: {
                    durationInstance,
                    projectBidAmount: { $toDouble: '$bidAmount' }
                }
            },
            {
                $group: {
                    _id: '$durationInstance', avg: { $avg: '$projectBidAmount' }
                }
            }
        ]).exec();
        delete addOffset.bidAmount;
        for (const avgCpcAmount of avgCpcAmounts) {
            for (const result of results) {
                if (result.durationInstance === avgCpcAmount._id) {
                    if (avgCpcAmount.avg) {
                        result.avgCPC = avgCpcAmount.avg;
                    } else {
                        result.avgCPC = 0;
                    }
                    break;
                }
            }
        }

        addOffset.bidAmount = '$bidAmount';
        const totalSpends = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: brandIdList },
                    dateAndTime: { $gte: startDate, $lte: endDate }
                }
            },
            {
                $project: addOffset
            },
            {
                $project: {
                    durationInstance,
                    projectBidAmount: { $toDouble: '$bidAmount' }
                }
            },
            {
                $group: {
                    _id: '$durationInstance', count: { $sum: '$projectBidAmount' }
                }
            }
        ]).exec();
        delete addOffset.bidAmount;
        for (const totalSpend of totalSpends) {
            for (const result of results) {
                if (result.durationInstance === totalSpend._id) {
                    result.spends = totalSpend.count;
                    break;
                }
            }
        }

        const impressions = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: brandIdList },
                    dateAndTime: { $gte: startDate, $lte: endDate },
                    typeOfEvent: 'CPM'
                }
            },
            {
                $project: addOffset
            },
            {
                $project: {
                    durationInstance,
                    projectBidAmount: { $toDouble: '$bidAmount' }
                }
            },
            {
                $group: {
                    _id: '$durationInstance', count: { $sum: 1 }
                }
            }
        ]).exec();

        const clicks = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: brandIdList },
                    dateAndTime: { $gte: startDate, $lte: endDate },
                    typeOfEvent: { $in: ['CPC'] }
                }
            },
            {
                $project: addOffset
            },
            {
                $project: {
                    durationInstance,
                    projectBidAmount: { $toDouble: '$bidAmount' }
                }
            },
            {
                $group: {
                    _id: '$durationInstance', count: { $sum: 1 }
                }
            }
        ]).exec();

        let totalImpressions: number = 0;
        let totalClicks: number = 0;

        for (const impression of impressions) {
            for (const result of results) {
                if (result.durationInstance === impression._id) {
                    totalImpressions = totalImpressions + impression.count;
                    result.impressions = impression.count;
                    break;
                }
            }
        }

        for (const click of clicks) {
            for (const result of results) {
                if (result.durationInstance === click._id) {
                    totalClicks = totalClicks + click.count;
                    result.clicks = click.count;
                    break;
                }
            }
        }

        for (const result of results) {
            if (result.impressions) {
                result.ctr = result.clicks * 100 / result.impressions;
            }
        }

        const avgCpcCpm = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: {
                    brandID: { $in: brandIdList },
                    dateAndTime: { $gte: startDate, $lte: endDate }
                }
            },
            {
                $group: {
                    _id: null,
                    totalAmount: {
                        $sum: {
                            $cond: [
                                { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
                            ]
                        }
                    },
                    CPC: {
                        $avg: {
                            $cond: [
                                {
                                    $and: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        { $eq: ['$bidType', 'CPC'] }
                                    ]
                                },
                                '$bidAmount',
                                null
                            ]
                        }
                    },
                    CPM: {
                        $avg: {
                            $cond: [
                                {
                                    $and: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        { $eq: ['$bidType', 'CPM'] }
                                    ]
                                },
                                '$bidAmount',
                                null
                            ]
                        }
                    }
                }
            },
            {
                $project: {
                    _id: 1,
                    CPC: 1,
                    CPM: 1
                }
            }]
        ).exec();

        const summary: any = { };
        let spends: number = 0;
        for (const result of results) {
            spends = spends + result.spends;
        }
        summary.ctr = totalClicks * 100 / totalImpressions;
        if (avgCpcCpm && avgCpcCpm.length > 0) {
            summary.cpm = avgCpcCpm[0].CPM ? avgCpcCpm[0].CPM * 1000 : 0;
            summary.cpc = avgCpcCpm[0].CPC ? avgCpcCpm[0].CPC : 0;
        } else {
            summary.cpm = 0;
            summary.cpc = 0;
        }

        summary.spends = spends;
        if (avgCpmAmounts.length || avgCpcAmounts.length || totalSpends.length || impressions.length || clicks.length || avgCpcCpm.length) {
            return { data: results, summary };
        } else {
            return { data: [], summary: null };
        }

    }

    private getAdServerContentModel(zone: string) {
        if (zone === '1') {
            return usAdServerContentModel;
        } else if (zone === '2') {
            return indiaAdServerContentModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
}
