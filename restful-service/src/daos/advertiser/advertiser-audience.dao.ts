import mongoose from 'mongoose';
import AdvertiserAudienceDTO from '../../dtos/advertiser/advertiser-audience.dto';
import { advertiserAudienceModel } from '../../models/advertiser/advertiser-audience-schema';

export class AdvertiserAudienceDAO {

    public async create(dto: AdvertiserAudienceDTO): Promise<AdvertiserAudienceDTO> {
        const saveBrand = new advertiserAudienceModel(dto);
        return await saveBrand.save();
    }

    public async update(id: any, dto: AdvertiserAudienceDTO): Promise<AdvertiserAudienceDTO> {
        const updateModel = await advertiserAudienceModel.findById(id).exec();
        Object.assign(updateModel, dto);
        return await updateModel.save();
    }

    public async findByTargetId(targetId: string): Promise<AdvertiserAudienceDTO[]> {
        return await advertiserAudienceModel.find({ advertiserAudienceTargetId:
            mongoose.Types.ObjectId(targetId) }).exec();
    }

    public async findAll(): Promise<AdvertiserAudienceDTO[]> {
        return await advertiserAudienceModel.find({ }).exec();
    }

}
