import PermissionDTO from '../../dtos/admin/permission.dto';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import { permissionModel } from '../../models/admin/permission-schema';
import { roleModel } from '../../models/admin/roles-schema';
import { advertiserModel } from '../../models/advertiser/advertiser-schema';
import { advertiserUserModel } from '../../models/advertiser/advertiser-user-schema';
import { logger } from '../../util/winston';

export default class AdvertiserUserDAO {

    public async create(dto: AdvertiserUserDTO): Promise<AdvertiserUserDTO> {
        const saveAdvertiserUser = new advertiserUserModel(dto);
        return await saveAdvertiserUser.save();
    }
    public async findAll(userEmail: string): Promise<AdvertiserUserDTO[]> {
        // return await creativeHubModel.find({ advertiserId: advId, creativeType: type, deleted: false }).exec();
        return await advertiserUserModel.aggregate([
            { $match: {
                'created.by' : userEmail,
                'deleted': false
              }
            },
            {
                $group: {
                    _id: {
                        email: '$email',
                    },
                    advertiserUsers: {
                        $push: {
                        _id: '$_id',
                        brands: '$brands',
                        deleted: '$deleted',
                        advertiserId: '$advertiserId',
                        status: '$status',
                        created: '$created'
                      }
                    },
                }
            },
            { $lookup: {
                    from: 'users',
                    localField: '_id.email',
                    foreignField: 'email',
                    as: 'userDetails'
                }
            },
            { $lookup: {
                    from: 'advertiser_brands',
                    localField: 'advertiserUsers.brands.brandId',
                    foreignField: '_id',
                    as: 'brandDetails'
                }
            },
            {
                $project: {
                  _id : 0,
                  advertiserUsers: '$advertiserUsers',
                  users: '$userDetails',
                  brands: '$brandDetails',
                  email: '$_id.email'
                }
            },
        ]).exec();
    }
    public async update(argEmail: string, dto: AdvertiserUserDTO): Promise<AdvertiserUserDTO> {
        const updateAdvertiserUser = await advertiserUserModel.findOne({ email: argEmail }).exec();
        Object.assign(updateAdvertiserUser, dto);
        return await updateAdvertiserUser.save();
    }
    public async findByEmail(argEmail: string): Promise<AdvertiserUserDTO> {
        return await advertiserUserModel.findOne({ email: argEmail }).exec();
    }

    public async findBrandsAndRolesByLoggedInUserEmail(argEmail: string): Promise<any[]> {
        return await advertiserUserModel.aggregate([
            { $unwind : '$brands' },
            { $project : { _id: 0, brandId : '$brands.brandId', role : '$brands.userRoleId' } }
            ]).allowDiskUse(true)
            .exec();
    }

    public async findPermissionsByIds(idList: any[]): Promise<PermissionDTO[]> {
        return await permissionModel.find({ _id : { $in : idList } }, { _id: 0, identifier: 1 }).exec();
    }
    public async findAdvertiserUserBrandsDetails(userEmail: string): Promise<AdvertiserUserDTO[]> {
        return await advertiserUserModel
        .aggregate()
        .match({ email: userEmail })
        .lookup({
            from: 'advertiser_brands',
            localField: 'brands.brandId',
            foreignField: '_id',
            as: 'brandDetails',
        })
        .allowDiskUse(true)
        .exec();
    }
    public async findByEmailWithAdvertiserDetails(userEmail: string): Promise<AdvertiserUserDTO[]> {
        return await advertiserUserModel
        .aggregate()
        .match({ email: userEmail })
        .lookup({
            from: 'advertisers',
            localField: 'advertiserId',
            foreignField: '_id',
            as: 'advertiserDetails',
        })
        .allowDiskUse(true)
        .exec();
    }
    public async findByBrandIdWithUserDetails(ids: any[]): Promise<AdvertiserUserDTO[]> {
        return await advertiserUserModel
        .aggregate()
        .match({ 'brands.brandId': { $in: ids } })
        .lookup({
            from: 'users',
            localField: 'email',
            foreignField: 'email',
            as: 'userDetails',
        })
        .allowDiskUse(true)
        .exec();
    }
    public async findAllByBrandId(id: any): Promise<AdvertiserUserDTO[]> {
        return await advertiserUserModel.find({ 'brands.brandId': id }).exec();
    }

    public async findUserBrandsByEmail(email: string): Promise<AdvertiserUserDTO[]> {
        return await advertiserUserModel.find({ email }).exec();
    }

    public async checkBrandPermission(email: string, brandId: string, permissions: string[]): Promise<AdvertiserUserDTO[]> {
        const r: any[] = await roleModel.find({ moduleType: 'Advertiser',
                                                permissions: { $in: permissions } }).select('_id').exec();
        return await advertiserUserModel.find({
            'email': email, 'brands.brandId': brandId,
            'brands.userRoleId': { $in: r.map((x: any) => x._id) }
        }).exec();
    }

    public async checkAdvertiserPermission(email: string, advertiserId: string, permissions: string[]): Promise<AdvertiserUserDTO[]> {
        const r: any[] = await roleModel.find({ moduleType: 'Advertiser',
                                                permissions: { $in: permissions } }).select('_id').exec();
        return await advertiserUserModel.find({
            'email': email, 'advertiserId': advertiserId,
            'brands.userRoleId': { $in: r.map((x: any) => x._id) }
        }).exec();
    }

    public async findByEmailAndPermission(email: string, permissions: string[]): Promise<AdvertiserUserDTO[]> {
        const r: any[] = await roleModel.find({ moduleType: 'Advertiser',
                                                permissions: { $in: permissions } }).select('_id').exec();
        return await advertiserUserModel.find({
            'email': email,
            'brands.userRoleId': { $in: r.map((x: any) => x._id) }
        }).exec();
    }

    public async findByAdvertiserId(advertiserId: string): Promise<AdvertiserUserDTO[]> {
        return await advertiserUserModel.find({ advertiserId }).exec();
    }

    public async getUserAndBrandDetails(email: string, brandId: any): Promise<any> {
        return await advertiserUserModel.aggregate([
            {
                $match: { email, 'brands.brandId': brandId, 'deleted': false }
            },
            {
                $unwind : { path: '$brands', preserveNullAndEmptyArrays: false }
            },
            {
                $match: { 'brands.brandId': brandId }
            },
            {
                $project: {
                    _id: 0,
                    email: 1,
                    brand: '$brands'
                }
            }
        ]).allowDiskUse(true)
        .exec().then((items: any) => items[0]);
    }

    public async getUsersByBrand(id: any): Promise<any> {
        return await advertiserUserModel.aggregate([
            {
                $match: { 'brands.brandId': id, 'deleted': false }
            },
            {
                $unwind : { path: '$brands', preserveNullAndEmptyArrays: false }
            },
            {
                $match: { 'brands.brandId': id }
            },
            {
                $project: {
                    _id: 0,
                    email: 1,
                    advertiserId: 1,
                    brand: '$brands'
                }
            }
        ]).allowDiskUse(true)
        .exec();
    }

    public async getBrandsAndRolesGoupByUserForGivenBrands(ids: any[]): Promise<any> {
        return await advertiserUserModel.aggregate([
            {
                $match: { 'brands.brandId': { $in: ids }, 'deleted': false }
            },
            {
                $unwind : { path: '$brands', preserveNullAndEmptyArrays: false }
            },
            {
                $match: { 'brands.brandId': { $in: ids } }
            },
            {
                $group: {
                    _id: '$email',
                    brands: {
                        $push: {
                            brandId: '$brands.brandId',
                            userRoleId: '$brands.userRoleId',
                            isWatching: '$brands.isWatching'
                        }
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    email: '$_id',
                    brands: 1
                }
            }
        ]).allowDiskUse(true)
        .exec();
    }
    public async findByAdvertiserIdAndEmail(advId: string, argEmail: string): Promise<AdvertiserUserDTO> {
        return await advertiserUserModel.findOne({ advertiserId: advId, email: argEmail }).exec();
    }

    public async findUsers(): Promise<any[]> {
        return await advertiserUserModel.find({
            deleted: 'false'
        }).select('email').exec();
    }

    public async getAdvertiserAdminUsers(advertiserId: string) {
        const allUsers = await advertiserUserModel.find({ advertiserId, deleted: false }).exec();
        const roleDTO = await roleModel.findOne({ moduleType: 'Advertiser', roleName: 'admin' }).exec();
        const adminUsers: string[] = [];
        for (const a of allUsers) {
            if (a.brands.filter((x) => x.userRoleId === roleDTO._id.toString())) {
                adminUsers.push(a.email);
            }
        }
        return adminUsers;
    }
}
