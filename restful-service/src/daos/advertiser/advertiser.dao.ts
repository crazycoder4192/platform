import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import { advertiserModel } from '../../models/advertiser/advertiser-schema';

export default class AdvertiserDAO {
    public async create(dto: AdvertiserDTO): Promise<AdvertiserDTO> {
        const saveAdvertiser = new advertiserModel(dto);
        return await saveAdvertiser.save();
    }

    public async update(id: any, dto: AdvertiserDTO): Promise<AdvertiserDTO> {
        const updateAdvertiser = await advertiserModel.findById(id).exec();
        Object.assign(updateAdvertiser, dto);
        return await updateAdvertiser.save();
    }

    public async findByEmail(argEmail: any): Promise<AdvertiserDTO> {
        return await advertiserModel.findOne({ 'user.email': argEmail }).exec();
    }

    public async findById(id: string): Promise<AdvertiserDTO> {
        return await advertiserModel.findById(id).exec();
    }

    public async findAll(): Promise<AdvertiserDTO[]> {
        return await advertiserModel.find({ }).exec();
    }

    public async getAdvertisersByIds(ids: any[]): Promise<AdvertiserDTO[]> {
        return await advertiserModel.find({ _id: { $in: ids } }).exec();
    }

    public async getAdvertiserNamesByAdvertiserIds(ids: any[]): Promise<any[]> {
        return await advertiserModel.find({ _id: { $in: ids } }).select('businessName').exec();
    }
}
