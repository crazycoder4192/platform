import { indiaClientBrandMetadataModel } from '../../models/advertiser/client-brand-metadata-india-schema';
import { usClientBrandMetadataModel } from '../../models/advertiser/client-brand-metadata-schema';

export default class ClientBrandMetadataDAO {

    public async getClients(zone: string): Promise<any[]> {
        return await this.getClientBrandMetadataModel(zone).find({ }).select('client').exec();
    }

    public async getBrands(clientId: any, zone: string): Promise<string[]> {
        const result: any = await this.getClientBrandMetadataModel(zone).aggregate([
            {
                $match: { _id: clientId }
            }
        ]).exec();
        if (result && result.length > 0) {
            const b: any[] = result[0].brands;
            return b;
        }
        return [];
    }

    private getClientBrandMetadataModel(zone: string) {
        if (zone === '1') {
            return usClientBrandMetadataModel;
        } else if (zone === '2') {
            return indiaClientBrandMetadataModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
}
