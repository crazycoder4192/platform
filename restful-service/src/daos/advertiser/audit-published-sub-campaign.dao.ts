import AuditPublishedSubCampaignDTO from '../../dtos/advertiser/audit-published-sub-campaign.dto';
import { auditPublishedSubCampaignModel } from '../../models/advertiser/audit-published-sub-campaign-schema';

export default class AuditPublishedSubCampaignDAO {
  public async create(dto: AuditPublishedSubCampaignDTO): Promise<AuditPublishedSubCampaignDTO> {
    const createModel = new auditPublishedSubCampaignModel(dto);
    return await createModel.save();
  }

  public async update(id: any, dto: AuditPublishedSubCampaignDTO): Promise<AuditPublishedSubCampaignDTO> {
    const updateModel = await auditPublishedSubCampaignModel.findById(id).exec();
    Object.assign(updateModel, dto);
    return await updateModel.save();
  }

  public async findBySubcampaignId(subcampaignId: any): Promise<AuditPublishedSubCampaignDTO[]> {
    return await auditPublishedSubCampaignModel.find({ subcampaignId }).exec();
  }
}
