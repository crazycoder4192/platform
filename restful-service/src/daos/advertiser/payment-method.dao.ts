import config from 'config';
import AdvertiserPaymentMethodDTO from '../../dtos/advertiser/payment-method.dto';
import { indiaPaymentMethodModel } from '../../models/advertiser/payment-method-india-schema';
import { usPaymentMethodModel } from '../../models/advertiser/payment-method-us-schema';
import { Encryption } from '../../util/crypto';
const ApiContracts = require('authorizenet').APIContracts;
const ApiControllers = require('authorizenet').APIControllers;

export default class AdvertiserPaymentMethodDAO {

    public listOfMethods: any[] = [];
    private readonly encryption = new Encryption();

    public async create(dto: AdvertiserPaymentMethodDTO, zone: string): Promise<AdvertiserPaymentMethodDTO> {
        if (zone === '1') {
            const paymentMethoddto = new usPaymentMethodModel(dto);
            return await paymentMethoddto.save();
        } else if (zone === '2') {
            const paymentMethoddto = new indiaPaymentMethodModel(dto);
            return await paymentMethoddto.save();
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
    public async update(id: string, dto: AdvertiserPaymentMethodDTO, zone: string) {
        return await this.getPaymentMethodModel(zone).findByIdAndUpdate(id, dto, { new: true }).exec();
    }
    public async delete(id: string, dto: AdvertiserPaymentMethodDTO, zone: string) {
        return await this.getPaymentMethodModel(zone).findByIdAndUpdate(id, { deleted: true }, { new: true }).exec();
    }
    public async findAll(zone: string): Promise<AdvertiserPaymentMethodDTO[]> {
        return await this.getPaymentMethodModel(zone).find({ deleted: false }).exec();
    }
    public async findById(id: string, zone: string): Promise<AdvertiserPaymentMethodDTO> {
        return await this.getPaymentMethodModel(zone).findById(id).exec();
    }
    public async findByEmailId(argEmail: any, zone: string): Promise<AdvertiserPaymentMethodDTO> {
        return await this.getPaymentMethodModel(zone).findOne({ 'created.by': argEmail, 'deleted': false }).exec();
    }

    public async findAllByEmail(argEmail: any, zone: string): Promise<AdvertiserPaymentMethodDTO[]> {
        return await this.getPaymentMethodModel(zone).find({ 'created.by': argEmail, 'deleted': false }).sort({ 'created.at': -1 }).exec();
    }

    public async updatePaymentMethodData(id: string, dto: any, zone: string) {
        return await this.getPaymentMethodModel(zone).findByIdAndUpdate(id, dto, { new: true }).exec();
    }

    public async findAllByEmailandBrands(argEmail: any, brandIds: any[], zone: string): Promise<AdvertiserPaymentMethodDTO[]> {
        return await this.getPaymentMethodModel(zone).find({ 'created.by': argEmail, 'deleted': false, 'brands': { $in: brandIds } }).exec();
    }

    public async findByBrandId(brandIds: any, zone: string): Promise<AdvertiserPaymentMethodDTO> {
        return await this.getPaymentMethodModel(zone).findOne({ brands: brandIds, deleted: false }).exec();
    }

    public async getPaymentMethodsByBrandId(brandId: any, zone: string): Promise<any> {
        return await this.getPaymentMethodModel(zone).aggregate([
            {
                $match: { brands: brandId }
            },
            {
                $unwind : { path: '$brands', preserveNullAndEmptyArrays: false }
            },
            {
                $match: { brands: brandId }
            }
        ]).exec().then((items: any) => items[0]);
    }

    public async findByBrandForAccounts(brandIds: any[], zone: string): Promise<any> {
        return await this.getPaymentMethodModel(zone).find({ brands: { $in: brandIds }, deleted: false }).exec();
    }

    private getPaymentMethodModel(zone: string) {
        if (zone === '1') {
            return usPaymentMethodModel;
        } else if (zone === '2') {
            return indiaPaymentMethodModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
}
