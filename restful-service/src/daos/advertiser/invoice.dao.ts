import moment from 'moment';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import InvoiceDTO from '../../dtos/advertiser/invoice.dto';
import AdvertiserPaymentMethodDTO from '../../dtos/advertiser/payment-method.dto';
import { countryTaxModel } from '../../models/admin/country-tax-schema';
import { indiaInvoiceModel } from '../../models/advertiser/invoice-india-schema';
import { usInvoiceModel } from '../../models/advertiser/invoice-us-schema';
import { subCampaignModel } from '../../models/advertiser/sub-campaign-schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { InvoiceStatus } from '../../util/constants';

export default class InvoiceDAO {

    public async create(dto: InvoiceDTO, zone: string): Promise<InvoiceDTO> {
        if (zone === '1') {
            const invoicedto = new usInvoiceModel(dto);
            return await invoicedto.save();
        } else if (zone === '2') {
            const invoicedto = new indiaInvoiceModel(dto);
            return await invoicedto.save();
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
    public async update(id: string, dto: InvoiceDTO, zone: string) {
        return await this.getInvoiceModel(zone).findByIdAndUpdate(id, dto, { new: true }).exec();
    }
    public async findAll(zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find().exec();
    }
    public async findById(id: string, zone: string): Promise<InvoiceDTO> {
        return await this.getInvoiceModel(zone).findById(id).exec();
    }
    public async findAllByEmail(argEmail: string, zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ 'created.by': argEmail }).exec();
    }
    public async findAllByViewingBrands(brandsList: any, zone: string): Promise<any[]> {
        return await this.getInvoiceModel(zone).find({ brandId: { $in: brandsList } }).sort('-created.at').exec();
    }
    public async getAllInvoicesByBrandId(brandId: any, zone: string): Promise<any[]> {
        return await this.getInvoiceModel(zone).find({ brandId }).sort('-invoiceDate').exec();
    }

    public async findByInvoiceNumber(invoceNuber: string, zone: string): Promise<InvoiceDTO> {
        return await this.getInvoiceModel(zone).findOne({ invoiceNumber: invoceNuber }).exec();
    }
    public async getPending(id?: string, zone?: string): Promise<InvoiceDTO[]> {
        if (id && id != null) {
            return await this.getInvoiceModel(zone).find({ _id: id, invoiceStatus: 'pending', deleted: false }).exec();
        } else {
            return await this.getInvoiceModel(zone).find({ invoiceStatus: 'pending', deleted: false }).exec();
        }
    }
    public async generateInvoice(brand: AdvertiserBrandDTO, paymentMethod: AdvertiserPaymentMethodDTO,
                                 startDate: any, endDate: any, zone: string): Promise<InvoiceDTO> {

        const invoice: any = { };
        const randomString = require('random-string');
        let subCampaigns;
        const brandId = brand._id;
        subCampaigns = await subCampaignModel.find({ brandId }).exec();
        invoice.advertiserId = brand.advertiser;
        invoice.brandId = brand._id;
        invoice.brandName = brand.brandName;
        invoice.billingCycle = `${startDate}-${endDate}`;
        const subCampaignsList = Array();
        let totalAmount = 0;
        if (subCampaigns && subCampaigns.length > 0) {
            let i = 1;
            for (const subCampaign of subCampaigns) {
                const amount = await this.getAmount(brandId, subCampaign._id, startDate, endDate, zone);
                if (amount > 0) {
                    // tslint:disable-next-line: max-line-length
                    const subcampaignStartDate = moment(startDate).format('MM-DD-YYYY');
                    const subcampaignEndDate = moment(endDate).format('MM-DD-YYYY');
                    subCampaignsList.push({
                        item: i, campaignName: subCampaign.name,
                        startDate: subcampaignStartDate, endDate: subcampaignEndDate, amount
                    });
                    totalAmount += amount;
                    i++;
                }
            }
        }
        if (totalAmount > 0) {
            invoice.items = subCampaignsList;
            const taxAmount = await this.getTaxAmount(totalAmount, zone);
            invoice.tax = taxAmount;
            invoice.invoiceStatus = InvoiceStatus.pending;
            const randomInvoiceNumber = randomString({ length: 10, numeric: true, letters: false, special: false });
            invoice.amount = totalAmount;
            invoice.invoiceNumber = randomInvoiceNumber;
            invoice.invoiceDate = new Date();
            invoice.invoiceStartDate = startDate || Date();
            invoice.invoiceEndDate = endDate || Date();
            invoice.created = { at: new Date() };
            if (paymentMethod && paymentMethod.customerId && paymentMethod.customerProfileId) {
                invoice.paymentMethodId = paymentMethod._id;
            } else {
                // invoice.invoiceStatus = InvoiceStatus.noCreditCardDetail;
            }
            if (zone === '1') {
                const invoicedto = new usInvoiceModel(invoice);
                return await invoicedto.save();
            } else if (zone === '2') {
                const invoicedto = new indiaInvoiceModel(invoice);
                return await invoicedto.save();
            } else {
                throw new Error(`Invalid zone ${zone}`);
            }
        } else {
            return await invoice;
        }
    }

    public async getLastXDaysBrandFromAdServerTransaction(daysCount: number, zone: string): Promise<any> {
        const now = new Date();
        // not putting hours here as we want the end date to be like 28 July 2019 00.00.00+Timezone
        const endDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        const startDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() - daysCount);
        const result = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: { dateAndTime: { $gte: startDate, $lte: endDate } }
            },
            {
                $group: {
                    _id: null, uniqueIds: { $addToSet: '$brandID' },
                },
            }
        ]).allowDiskUse(true).exec();
        if (result && result.length > 0) {
            return result[0].uniqueIds;
        } else {
            return [];
        }
    }

    // get the total amount based on subcampaign Id
    public async getAmountForBrands(brandId: any, startDate: any, endDate: any, zone: string): Promise<any> {
        let matchCriteria: any;
        matchCriteria = {
            brandID: brandId
        };
        if (startDate && endDate) {
            matchCriteria.dateAndTime = { $gte: startDate, $lte: endDate };
        } else {
            if (startDate) {
                matchCriteria.dateAndTime = { $gte: startDate };
            }
            if (endDate) {
                matchCriteria.dateAndTime = { $lte: endDate };
            }
        }
        const amountData = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: matchCriteria
            },
            {
                $group: {
                    _id: { brandId: '$brandID', advertiserId: '$advertiserID' },
                    totalAmount: {
                        $sum: {
                            $convert: {
                                input: {
                                    $cond: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        '$bidAmount',
                                        0
                                    ]
                                },
                                to: 'double'
                            }
                        }
                    }
                }
            }
        ])
            .allowDiskUse(true)
            .exec();

        if (amountData && amountData.length > 0) {
            return amountData[0];
        } else {
            return { };
        }
    }

    public async findAllPendingInvoices(zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ invoiceStatus: { $ne: InvoiceStatus.paid }, deleted: false }).exec();
    }

    public async findPendingInvoices(ids: string[], zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ _id: { $in: ids }, invoiceStatus: { $ne: InvoiceStatus.paid }, deleted: false }).exec();
    }

    public async findAllMissingQuickbookInvoices(zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ 'quickbooks.invoiceGenerated': false, 'deleted': false }).exec();
    }

    public async findMissingQuickbookInvoices(ids: string[], zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ '_id': { $in: ids }, 'quickbooks.invoiceGenerated': false, 'deleted': false }).exec();
    }

    public async findAllMissingQuickbookPayments(zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ 'quickbooks.invoiceGenerated': true,
                                                       'quickbooks.paymentGenerated': false, 'deleted': false }).exec();
    }

    public async findMissingQuickbookPayments(ids: string[], zone: string): Promise<InvoiceDTO[]> {
        return await this.getInvoiceModel(zone).find({ '_id': { $in: ids }, 'quickbooks.invoiceGenerated': true,
                                                       'quickbooks.paymentGenerated': false, 'deleted': false }).exec();
    }

    public async invoicesGroupbyAdvertiserIdForAdmin(condition: string, zone: string): Promise<any[]> {
        let query: any = {
            $match: {
                deleted: false,
                invoiceStatus: 'pending'
             }
          };
        if (condition === 'QBInvoicePending') {
            query = {
                $match: {
                    'deleted': false,
                    'quickbooks.invoiceGenerated': false
                 }
              };
        } else if (condition === 'QBPaymentPending') {
            query = {
                $match: {
                    'deleted': false,
                    'quickbooks.paymentGenerated': false
                 }
              };
        }

        const advertisers: any = await this.getInvoiceModel(zone).aggregate([
            query,
            {
                $group: {
                    _id: {
                        advertiserId: '$advertiserId',
                    },
                    data: {
                    $push: {
                        invoiceId: '$_id',
                        brandId: '$brandId',
                        campaignDetails: '$items',
                        amount: '$amount',
                        invoiceNumber: '$invoiceNumber',
                        invoiceDate: '$invoiceDate',
                        invoiceStatus: '$invoiceStatus',
                        quickbooksInvoiceGenerated: '$quickbooks.invoiceGenerated',
                        quickbooksPaymentGenerated: '$quickbooks.paymentGenerated',
                        count: { $sum: 1 }
                    }
                    }
                }

            }
        ]).exec();
        return advertisers;
    }

    // get the total amount based on subcampaign Id
    private async getAmount(brandId: any, subCampaignId: any, startDate: any, endDate: any,
                            zone: string): Promise<number> {

        const startDateQ = new Date(new Date(startDate).getFullYear(), new Date(startDate).getMonth(), new Date(startDate).getDate());
        const endDateQ = new Date(new Date(endDate).getFullYear(), new Date(endDate).getMonth(), new Date(endDate).getDate());
        let matchCriteria: any;
        matchCriteria = {
            advertiserCampID: subCampaignId
        };
        if (startDate && endDate) {
            matchCriteria.dateAndTime = { $gte: startDateQ, $lte: endDateQ };
        } else {
            if (startDate) {
                matchCriteria.dateAndTime = { $gte: startDateQ };
            }
            if (endDate) {
                matchCriteria.dateAndTime = { $lte: endDateQ };
            }
        }
        const amountData = await this.getAdServerContentModel(zone).aggregate([
            {
                $match: matchCriteria
            },
            {
                $group: {
                    _id: { advertiserCampID: '$advertiserCampID' },
                    totalAmount: {
                        $sum: {
                            $convert: {
                                input: {
                                    $cond: [
                                        { $eq: ['$typeOfEvent', '$bidType'] },
                                        '$bidAmount',
                                        0
                                    ]
                                },
                                to: 'double'
                            }
                        }
                    }
                }
            }
        ])
            .allowDiskUse(true)
            .exec();
        let totalAmount = 0;
        if (amountData.length > 0) {
            totalAmount = amountData[0].totalAmount;
        }
        return totalAmount;
    }

    // tax calculation
    // TODO: calcuation based on country advertiser country
    private async getTaxAmount(totalAmount: number, zone: string): Promise<number> {
        const countryTaxData: any = await countryTaxModel.find({ country: 'United States' }).exec();
        let taxAmout = 0;
        if (countryTaxData && countryTaxData.length) {
            if (countryTaxData[0].percentageOfTax > 0) {
                taxAmout = totalAmount * countryTaxData[0].percentageOfTax / 100;
            }
        }
        return taxAmout;
    }

    private getInvoiceModel(zone: string) {
        if (zone === '1') {
            return usInvoiceModel;
        } else if (zone === '2') {
            return indiaInvoiceModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    private getAdServerContentModel(zone: string) {
        if (zone === '1') {
            return usAdServerContentModel;
        } else if (zone === '2') {
            return indiaAdServerContentModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

}
