import AdvertiserPaymentTransactionDTO from '../../dtos/advertiser/advertiser-payment-transactions.dto';
import { usPaymentTransactionModel } from '../../models/advertiser/advertiser-payment-transaction-us-schema';

export default class AdvertiserPaymentTransactionDAO {

    public async create(dto: AdvertiserPaymentTransactionDTO): Promise<AdvertiserPaymentTransactionDTO> {
        const paymentTransactionDto = new usPaymentTransactionModel(dto);
        return await paymentTransactionDto.save();
    }
    public async findAll(): Promise<AdvertiserPaymentTransactionDTO[]> {
        return await usPaymentTransactionModel.find().exec();
    }
    public async findById(id: string): Promise<AdvertiserPaymentTransactionDTO> {
        return await usPaymentTransactionModel.findById(id).exec();
    }

}
