import mongoose from 'mongoose';
import AdvertiserAudienceTargetDTO from '../../dtos/advertiser/advertiser-audience-target.dto';
import { advertiserAudienceTargetModel } from '../../models/advertiser/advertiser-audience-target-schema';
import { indiaIncomingHcpRequestDetailsModel } from '../../models/incoming-hcp-request-details-india.schema';
import { archetypeModel } from '../../models/utils/archetype-schema';
import { validatedHcpModel } from '../../models/validated-hcp.schema';

export class AdvertiserAudienceTargetDAO {

    public async create(dto: AdvertiserAudienceTargetDTO): Promise<AdvertiserAudienceTargetDTO> {
        const saveBrand = new advertiserAudienceTargetModel(dto);
        return await saveBrand.save();
    }

    public async update(id: any, dto: AdvertiserAudienceTargetDTO): Promise<AdvertiserAudienceTargetDTO> {
        const updateModel = await advertiserAudienceTargetModel.findById(id).exec();
        Object.assign(updateModel, dto);
        return await updateModel.save();
    }

    public async findByBrand(brandId: any): Promise<AdvertiserAudienceTargetDTO[]> {
        // not selecting targetDetails as it could potentially contain a large amount of data
        return await advertiserAudienceTargetModel.find({ brandId: mongoose.Types.ObjectId(brandId), deleted: false }).
        select('advertiserId brandId name audienceUploaded created modified').exec();
    }

    public async findById(targetId: any): Promise<AdvertiserAudienceTargetDTO> {
        return await advertiserAudienceTargetModel.findById(targetId).exec();
    }

    public async findByBrandIdAndId(brandId: string, id: string): Promise<AdvertiserAudienceTargetDTO[]> {
        return await advertiserAudienceTargetModel.find({ brandId: mongoose.Types.ObjectId(brandId),
                                                          _id: id}).exec();
    }

    public async getAvailableReachForUs(): Promise<number> {
        return await validatedHcpModel.find({ }).count().exec();
    }

    public async getAvailableReachForIndia(): Promise<number> {
        const results: any[] = await indiaIncomingHcpRequestDetailsModel.find({
            hits: { $gt: 0 },
            isDeleted: false,
            existingRecordId: { $eq: null }
          }).select('_id').exec();
        return results.length;
    }

    public async getPotentialReachForCreate(targetDetails: any): Promise<number[]> {
        try {
            const query = await this.getQueryForCreatePotentialReachForUS(targetDetails);
            const potentialResults = await validatedHcpModel.aggregate([
                {
                    $match: query
                },
                {
                    $group: {
                        _id: '$npi'
                    }
                }
            ]).exec();
            const npis: number[] = potentialResults.map((x: any) => x._id);
            return npis;
        } catch (err) {
            throw err;
        }
    }

    public async getPotentialReachCountForCreateForUS(targetDetails: any): Promise<number> {
        const query = await this.getQueryForCreatePotentialReachForUS(targetDetails);
        const n: number = await validatedHcpModel.find(query).count().exec();
        return n;
    }

    public async getPotentialReachCountForCreateForIndia(targetDetails: any): Promise<number> {
        const query = await this.getQueryForCreatePotentialReachForIndia(targetDetails);
        let n: number = 0;
        let results = await indiaIncomingHcpRequestDetailsModel.find(query).select('_id').exec();
        const recordIds = results.map((x) => x._id.toString());
        results = await indiaIncomingHcpRequestDetailsModel.find({
            $or: [
                { existingRecordId: { $nin: recordIds } },
                { existingRecordId: { $eq: null } }
            ],
            platformUid: { $in: recordIds }
            }).select('_id').exec();
        n = results.length;
        return n;
    }

    public async getPotentialReachForUpload(inputNpis: number[]): Promise<number[]> {
        try {
            const validatedNpis = await validatedHcpModel.find({ npi: { $in: inputNpis } }).distinct('npi').exec();
            return validatedNpis;
        } catch (err) {
            throw err;
        }
    }

    public async findByName(brandId: any, name: string): Promise<AdvertiserAudienceTargetDTO[]> {
        // not selecting targetDetails as it could potentially contain a large amount of data
        return await advertiserAudienceTargetModel.find({ name, brandId: mongoose.Types.ObjectId(brandId),
                                                          deleted: false }).
        select('brandId name').exec();
    }

    private async getQueryForCreatePotentialReachForUS(targetDetails: any): Promise<any> {
        let specializationQuery: any;
        let locationQuery: any;

        const campaignHCPScore: any = await archetypeModel.find({ name: { $in: targetDetails.demoGraphics.behaviours } }).exec();
        const campaignTaxonomy: any[] = targetDetails.demoGraphics.specialization.areaOfSpecialization;
        const campaignGender: string[] = targetDetails.demoGraphics.gender;
        const campaignZip: any[] = [];
        const ls: any[] = targetDetails.geoLocation.locations;
        for (const l of ls) {
            const zs: string = l.zipcode;
            for (const z of zs.split(',')) {
                campaignZip.push(z);
            }
        }
        const scoreQuery: any[] = [];
        campaignHCPScore.forEach((campaignBehaviourItem: any) => {
            if (campaignBehaviourItem.hcpMinScoreRange && campaignBehaviourItem.hcpMaxScoreRange
                && campaignBehaviourItem.hcpMaxScoreRange !== 'N/A') {
                scoreQuery.push({
                    'hcpScore.cumulativeScore':
                        { $gte: +campaignBehaviourItem.hcpMinScoreRange, $lte: +campaignBehaviourItem.hcpMaxScoreRange }
                });
            } else if (campaignBehaviourItem.hcpMaxScoreRange === 'N/A') {
                scoreQuery.push({
                    'hcpScore.cumulativeScore':
                        { $gte: +campaignBehaviourItem.hcpMinScoreRange }
                });
            }
        });

        const genderInitials: string[] = [];
        campaignGender.forEach((campaignGenderItem: string) => {
            if (campaignGenderItem.toLowerCase() === 'male') {
                genderInitials.push('M');
            }
            if (campaignGenderItem.toLowerCase() === 'female') {
                genderInitials.push('F');
            }
            if (campaignGenderItem.toLowerCase() === 'other') {
                genderInitials.push('O');
            }
        });

        if (JSON.parse(targetDetails.geoLocation.isInclude)) {
            locationQuery = { $in: campaignZip };
        } else {
            locationQuery = { $nin: campaignZip };
        }
        if (JSON.parse(targetDetails.demoGraphics.specialization.isInclude)) {
            specializationQuery = { $in: campaignTaxonomy };
        } else {
            specializationQuery = { $nin: campaignTaxonomy };
        }

        const query = {
            $and: [
                { taxonomy: specializationQuery },
                { zip: locationQuery },
                { gender: { $in: genderInitials } },
                { $or: scoreQuery }
            ]
        };
        return query;
    }

    private async getQueryForCreatePotentialReachForIndia(targetDetails: any): Promise<any> {
        let specializationQuery: any;
        let locationQuery: any;

        const campaignTaxonomy: any[] = targetDetails.demoGraphics.specialization.areaOfSpecialization;
        const campaignGender: string[] = targetDetails.demoGraphics.gender;
        const campaignZip: any[] = [];
        const ls: any[] = targetDetails.geoLocation.locations;
        for (const l of ls) {
            const zs: string = l.zipcode;
            for (const z of zs.split(',')) {
                campaignZip.push(z);
            }
        }

        const genderInitials: string[] = [];
        campaignGender.forEach((campaignGenderItem: string) => {
            if (campaignGenderItem.toLowerCase() === 'male') {
                genderInitials.push('M');
            }
            if (campaignGenderItem.toLowerCase() === 'female') {
                genderInitials.push('F');
            }
            if (campaignGenderItem.toLowerCase() === 'other') {
                genderInitials.push('O');
            }
        });

        if (JSON.parse(targetDetails.geoLocation.isInclude)) {
            locationQuery = { $in: campaignZip };
        } else {
            locationQuery = { $nin: campaignZip };
        }
        if (JSON.parse(targetDetails.demoGraphics.specialization.isInclude)) {
            specializationQuery = { $in: campaignTaxonomy };
        } else {
            specializationQuery = { $nin: campaignTaxonomy };
        }

        const query = {
            $and: [
                { taxonomy: specializationQuery },
                { zip: locationQuery },
                { gender: { $in: genderInitials } }
            ]
        };
        return query;
    }
}
