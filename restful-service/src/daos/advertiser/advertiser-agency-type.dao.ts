import AdvertiserAgencyTypeDTO from '../../dtos/advertiser/advertiser-agency-type.dto';
import { advertiserAgencyTypeModel } from '../../models/advertiser/advertiser-agency-types-schema';

export default class AdvertiserAgencyTypeDAO {
  public async createMany(
    details: AdvertiserAgencyTypeDTO[],
  ): Promise<AdvertiserAgencyTypeDTO[]> {
    return await advertiserAgencyTypeModel.insertMany(details);
  }

  public async findAll(): Promise<AdvertiserAgencyTypeDTO[]> {
    return await advertiserAgencyTypeModel.find().exec();
  }

  public async findUniqueType(): Promise<AdvertiserAgencyTypeDTO[]> {
    return await advertiserAgencyTypeModel.distinct('type').exec();
  }
}
