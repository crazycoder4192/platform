import mongoose from 'mongoose';
import CampaignDTO from '../../dtos/advertiser/campaign.dto';
import SubCampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import { campaignModel } from '../../models/advertiser/campaign-schema';
import { subCampaignModel } from '../../models/advertiser/sub-campaign-schema';
const ObjectId = mongoose.Types.ObjectId;

export default class CampaignDAO {

  private readonly cap = '^' ;
  private readonly dollar = '$' ;

  public async createMany(details: CampaignDTO[]): Promise<CampaignDTO[]> {
    return await campaignModel.insertMany(details);
  }

  public async findByBrandId(id: string): Promise<CampaignDTO[]> {
    return await campaignModel.find({ brandId: id, deleted: false }).exec();
  }

  public async getCampaignById(id: string): Promise<CampaignDTO> {
    return await campaignModel.findById(id).exec();
  }

  public async create(dto: CampaignDTO): Promise<CampaignDTO> {
    const createModel = new campaignModel(dto);
    return await createModel.save();
  }
  public async update(id: string, dto: CampaignDTO): Promise<CampaignDTO> {
    return await campaignModel.findByIdAndUpdate(id, dto, { new: true }).exec();
  }

  public async findById(id: any): Promise<CampaignDTO> {
    return await campaignModel.findById(id).exec();
  }
  // added
  public async deleteFromDraft(id: string) {
    await campaignModel.updateMany({ _id: id }, { $set: { deleted : true } }).exec();
    return await subCampaignModel.updateMany({ campaignId: id, isPublished: false }, { $set: { deleted : true } }).exec();
  }
  public async deleteFromCampaign(id: string) {
    await campaignModel.updateMany({ _id: id }, { $set: { deleted : true } }).exec();
    return await subCampaignModel.updateMany({ campaignId: id, isPublished: true }, { $set: { deleted : true } }).exec();
  }
  public async findByIds(campaignIds: string[]): Promise<CampaignDTO[]> {
    return await campaignModel.find({ _id: { $in: campaignIds } }).exec();
  }

  public async findByCreatedByAndBrandId(email: string, watchingBrand: string): Promise<CampaignDTO[]> {
    return await campaignModel.find({ 'created.by': email, 'brandId': watchingBrand }).exec();
  }

  public async campaignNameAvailability(cName: string, brandId: string, advId: string): Promise<CampaignDTO[]> {
    return await campaignModel.find({ name: { $regex: this.cap + cName + this.dollar, $options: 'i' }, brandId: mongoose.Types.ObjectId(brandId),
                                      advertiserId: mongoose.Types.ObjectId(advId), deleted: false }).exec();
  }

  public async findBrandIdByCampaignId(id: string): Promise<CampaignDTO> {
    return await campaignModel.findOne({ _id: id }).select('brandId').exec();
  }

  public async findSubcampaignsByStatus(...status: string[]): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ status: { $in: status } }).exec();
  }

  public async findSubcampaignsByStatusUnderCampaign(campaignId: string, status: string[]): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ status: { $in: status }, campaignId: ObjectId(campaignId), isPublished: true }).exec();
  }
}
