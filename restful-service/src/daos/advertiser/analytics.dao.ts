import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import ArchetypeDAO from '../../daos/utils/archetype.dao';
import ArchetypeDTO from '../../dtos/utils/archetype.dto';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import { bannerModel } from '../../models/admin/banner-schema';
import { subCampaignModel } from '../../models/advertiser/sub-campaign-schema';
import { indiaIncomingHcpRequestDetailsModel } from '../../models/incoming-hcp-request-details-india.schema';
import { publisherAssetModel } from '../../models/publisher/asset-schema';
import { publisherPlatformModel } from '../../models/publisher/platform-schema';
import { taxonomyModel } from '../../models/taxonomy.schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { indiaCityDetailModel } from '../../models/utils/cities-details-india-schema';
import { usCityDetailModel } from '../../models/utils/cities-details-schema';
import { validatedHcpModel } from '../../models/validated-hcp.schema';
import IncomingHcpRequestDetailsDAO from '../incoming-hcp-request-details.dao';

export default class AnalyticsDAO {

  private readonly helper: Helper;
  private readonly archetypeDAO: ArchetypeDAO;
  private readonly incomingHcpRequestDetailsDAO: IncomingHcpRequestDetailsDAO;

  constructor() {
    this.helper = new Helper();
    this.archetypeDAO = new ArchetypeDAO();
    this.incomingHcpRequestDetailsDAO = new IncomingHcpRequestDetailsDAO();
  }

  public async getSpecializationByTaxonomy(ids: string[]): Promise<any[]> {
    return await taxonomyModel.find({ Taxonomy: { $in: ids } }).exec();
  }

  public async getAudienceIds(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceIds');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const hcps = await this.getAdServerContentModel(zone).find({
      advertiserCampID: { $in: filter.subCampaignIds },
      dateAndTime: { $gte: startDate, $lte: endDate },
      hcpId: { $ne: 'N/A' }
    }
    ).distinct('hcpId').exec();
    if (zone === '1') {
      const nhcps: number[] = [];
      for (const hcp of hcps) {
        nhcps.push(+hcp);
      }
      // tslint:disable-next-line: no-console
      console.timeEnd('getAudienceIds');
      return nhcps;
    } else if (zone === '2') {
      const nhps: string[] = await this.incomingHcpRequestDetailsDAO.findUniqueRecords(hcps, zone);
      // tslint:disable-next-line: no-console
      console.timeEnd('getAudienceIds');
      return nhps;
    }
  }

  // NOT USED FUNCTION CURRENTLY
  public async getAudienceGenderExperiencePieChart(hcpIds: number[]): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceGenderExperiencePieChart');
    const locationByGender: any[] = await validatedHcpModel.aggregate([
      {
        $match: {
          npi: { $in: hcpIds }
        }
      },
      {
        $group: {
          _id: {
            gender: '$gender',
          },
          locations: {
            $push: {
              location: '$zip',
              count: { $sum: 1 }
            }
          }
        }
      }
    ]).exec();

    const results: any[] = [];
    for (const r of locationByGender) {
      const o: any = { };
      o.gender = r._id.gender;
      o.locations = r.locations;
      results.push(o);
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceGenderExperiencePieChart');
    return results;
  }

  public async getAudienceSpecializationStackBarChart(filter: FilterAnalyticsDTO, hcpIds: number[], zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceSpecializationStackBarChart');
    const q: any = { };
    if (zone === '1') {
      q.npi = { $in: hcpIds };
    } else {
      q.platformUid = { $in: hcpIds };
    }
    this.populateValidateHcpFilter(q, filter);

    let taxonomyByGender: any[] = [];
    if (zone === '1') {
      taxonomyByGender = await validatedHcpModel.aggregate([
        {
          $match: q
        },
        {
          $group: {
            _id: {
              taxonomy: { $arrayElemAt: ['$taxonomy', 0] },
              gender: '$gender'
            },
            genderCount: { $sum: 1 }
          }
        },
        {
          $group: {
            _id: '$_id.taxonomy',
            genders: {
              $push: {
                gender: '$_id.gender',
                count: '$genderCount'
              }
            }
          }
        }
      ]).exec();
    } else if (zone === '2') {
      taxonomyByGender = await indiaIncomingHcpRequestDetailsModel.aggregate([
        {
          $match: q
        },
        {
          $group: {
            _id: {
              taxonomy: { $arrayElemAt: ['$taxonomies', 0] },
              gender: '$gender'
            },
            genderCount: { $sum: 1 }
          }
        },
        {
          $group: {
            _id: '$_id.taxonomy',
            genders: {
              $push: {
                gender: '$_id.gender',
                count: '$genderCount'
              }
            }
          }
        }
      ]).exec();
    }

    if (taxonomyByGender && taxonomyByGender.length > 10) {
      let top: any = [];
      for (const t of taxonomyByGender) {
        let count = 0;
        for (const g of t.genders) {
          count = count + g.count;
        }
        top.push({ taxonomy: t, count });
      }

      top.sort((a: any, b: any) => b.count - a.count);
      top = top.slice(0, 10);
      taxonomyByGender = top.map((x: any) => x.taxonomy);
    }
    // output will be of the format
    // { _id: '224P00000X', genders: [ { gender: 'M', count: 1 } ] }
    const txnmy: string[] = [];
    for (const t of taxonomyByGender) {
      txnmy.push(t._id);
    }

    const taxanomies: any[] = await taxonomyModel.find({ Taxonomy: { $in: txnmy } }).exec();
    const results: any[] = [];
    for (const t of taxonomyByGender) {
      const code: string = t._id;
      let who: string = '';
      for (const x of taxanomies) {
        if (x.Taxonomy === code) {
          /*if (x.Specialization) {
            who = x.Specialization
          } else if (x.Grouping) {
            who = x.Grouping
          } else if (x.Classification) {
            who = x.Classification;
          }*/
          who = x.Classification;
          break;
        }
      }
      const existing = results.find((x) => x.specialization === who);
      let o: any = { specialization: who, female: 0, male: 0, other: 0 };
      if (existing) {
        o = existing;
      }
      for (const g of t.genders) {
        if (g.gender === 'F') {
          o.female = o.female + g.count;
        } else if (g.gender === 'M') {
          o.male = o.male + g.count;
        } else {
          o.other = o.other + g.count;
        }
      }
      if (!existing) {
        results.push(o);
      }
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceSpecializationStackBarChart');
    // output will be of the format:
    // { taxonomy: 'Prosthetist', genders: [ { gender: 'M', count: 1 } ] }
    return results;
  }

  public async getAudienceLocationMapChart(filter: FilterAnalyticsDTO, hcpIds: number[], zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceLocationMapChart');
    let totalHits: number = 0;
    const q: any = { };
    if (zone === '1') {
      q.npi = { $in: hcpIds };
    } else if (zone === '2') {
      q.platformUid = { $in: hcpIds };
    }

    this.populateValidateHcpFilter(q, filter);

    let locations: any[] = [];
    if (zone === '1') {
      locations = await validatedHcpModel.aggregate([
        {
          $match: q
        },
        {
          $group: {
            _id: '$zip',
            count: { $sum: 1 }
          }
        }
      ]).exec();
    } else if (zone === '2') {
      locations = await indiaIncomingHcpRequestDetailsModel.aggregate([
        {
          $match: q
        },
        {
          $group: {
            _id: '$zip',
            count: { $sum: 1 }
          }
        }
      ]).exec();
    }

    const zipCodes: string[] = [];
    for (const l of locations) {
      zipCodes.push(l._id);
    }

    const cityDetails: any[] = await this.getCityDetailModel(zone).aggregate([
      {
        $match: {
          zipcode: { $in: zipCodes }
        }
      },
      {
        $group: {
          _id: '$stateFullName',
          zip: { $addToSet: '$zipcode' }
        }
      }
    ]).exec();

    const result: any[] = [];
    for (const state of cityDetails) {
      const sum = locations.filter((x) => state.zip.includes(x._id)).map((x) => x.count).reduce((a, b) => a + b, 0);
      const o: any = { };
      o.state = state._id;
      o.count = sum;
      result.push(o);
      totalHits = totalHits + o.count;
    }
    const resultWithPercentage: any[] = [];
    for (const res of result) {
      const val: number = (((res.count / totalHits) * 100) * 100) / 100;
      // const value: number = this.getDrawValueForLocationMap(val);
      const o = {
        state: res.state,
        count: res.count,
        //  drawValue: value,
        showValue: val
      };
      resultWithPercentage.push(o);
    }

    const sortedArray: any[] = resultWithPercentage.sort((obj1, obj2) => {
      if (obj1.count < obj2.count) {
        return 1;
      }
      if (obj1.count > obj2.count) {
        return -1;
      }
      return 0;
    });
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceLocationMapChart');
    // output will be of the format: { state: 'New York', count: 2 }
    return sortedArray;
  }

  public async getAudienceArcheTypeBarChart(filter: FilterAnalyticsDTO, hcpIds: number[]): Promise<any> {
    const q: any = { };
    q.npi = { $in: hcpIds };
    this.populateValidateHcpFilter(q, filter);

    const scores: any[] = await validatedHcpModel.aggregate([
      {
        $match: q
      },
      {
        $project: {
          score: '$hcpScore.cumulativeScore'
        }
      }
    ]).exec();

    let pharmaiety: number = 0;
    let connectors: number = 0;
    let toolBoxers: number = 0;
    let curators: number = 0;
    let followers: number = 0;
    const archetypes: ArchetypeDTO[] = await this.archetypeDAO.findAll();
    for (const s of scores) {
      const archType: string = this.getArcheType(s.score, archetypes);
      switch (archType) {
        case 'Pharmaiety': {
          pharmaiety++;
          break;
        }
        case 'Connectors': {
          connectors++;
          break;
        }
        case 'Toolboxers': {
          toolBoxers++;
          break;
        }
        case 'Curators': {
          curators++;
          break;
        }
        case 'Followers': {
          followers++;
          break;
        }
      }
    }
    const archeType: any = { };
    if (connectors > 0) {
      archeType.connectors = connectors;
    }
    if (curators > 0) {
      archeType.curators = curators;
    }
    if (pharmaiety > 0) {
      archeType.pharmaiety = pharmaiety;
    }
    if (followers > 0) {
      archeType.followers = followers;
    }
    if (toolBoxers > 0) {

      archeType.toolBoxers = toolBoxers;
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceArcheTypeBarChart');
    return archeType;
  }

  public async getAudienceFrequency(filter: FilterAnalyticsDTO, timezone: string, hcpIds: number[], zone: string): Promise<any[]> {
    // tslint:disable-next-line: no-console
    console.time('getAudienceFrequency');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    let filteredHcpIds: number[] = hcpIds;
    if (filter.isHcpFilterSelected) {
      filteredHcpIds = await this.getFilteredHcp(filter, hcpIds);
    }
    const result: any[] = [];
    const hcps = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          advertiserCampID: { $in: filter.subCampaignIds },
          dateAndTime: { $gte: startDate, $lte: endDate },
          hcpId: { $in: filteredHcpIds.map(String) }
          // TODO: may be add type of event here?
        }
      },
      {
        $group: {
          _id: '$hcpId',
          count: { $sum: 1 }
        }
      },
      {
        $project : {
          _id : 1,
          count : 1 }
      },
      {
        $group: {
          _id: '$count',
          count: { $sum: 1 }
        }
      },
      {
        $sort: {
          count: -1
        }
      },
      {
        $limit: 5
      }
    ]).exec();
    let totalViews: number = 0;
    for (const hcp of hcps) {
        totalViews += hcp.count;
    }
    for (const hcp of hcps) {
      const object = {
        _id : hcp._id,
        count : hcp.count,
        percent: (((hcp.count / totalViews) * 100) * 100) / 100
      };
      result.push(object);
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getAudienceFrequency');
    return result;

  }

  public async getBehaviorDisplayLocationStackBarChart(filter: FilterAnalyticsDTO, timezone: string, zone: string) {
    // tslint:disable-next-line: no-console
    console.time('getBehaviorDisplayLocationStackBarChart');
    const selectedDimensions = new Set();
    const selectedPlatforms = [];

    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const q: any = { };
    q.advertiserCampID = { $in: filter.subCampaignIds };
    q.dateAndTime = { $gte: startDate, $lte: endDate };
    if (filter.isHcpFilterSelected) {
      const allHcpIds: number[] = await this.getAudienceIds(filter, timezone, zone);
      const filteredHcpIds = await this.getFilteredHcp(filter, allHcpIds);
      q.hcpId = { $in: filteredHcpIds.map(String) };
    }

    // get all the platforms on which the ads were rendered
    // should we get unique rendering? i.e. based no guid
    // should get ouptut in the form { "_id" : "xyz" }
    const platforms = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $group: {
          _id: '$platformID'
        }
      }
    ]).exec();

    // find the platform types from the list of platforms
    // should get output in the form: { "_id" : "Dante", "platformIds" : [ "The Banquet", "Divine Comedy", "Eclogues" ] }
    // The output is
    // [ { _id: 5d0cd12d9e271009bc2a54bc } ]
    // Hence iterating through the list
    for (const platform of platforms) {
      selectedPlatforms.push(mongoose.Types.ObjectId(platform._id));
    }

    const platformByPlaformTypes = await publisherPlatformModel.aggregate([
      {
        $match: {
          _id: { $in: selectedPlatforms }
        }
      },
      {
        $group: {
          _id: '$typeOfSite', platformIds: { $push: '$_id' }
        }
      }
    ]).exec();

    const bannerDetails = await bannerModel.find({ }).exec();

    const results: any[] = [];
    for (const p of platformByPlaformTypes) {
      const countByDimension = await this.getAdServerContentModel(zone).aggregate([
        {
          $match: {
            advertiserCampID: { $in: filter.subCampaignIds },
            dateAndTime: { $gte: startDate, $lte: endDate },
            platformID: { $in: p.platformIds }
          }
        },
        {
          $group: {
            _id: '$dimension',
            count: { $sum: 1 }
          }
        }
      ]).exec();

      const result: any = { };
      result.platformType = p._id;
      const platformsDetails: any = [];
      for (const b of bannerDetails) {
        const platform: any = { };
        const c = countByDimension.find((x: any) => (x._id === b.size));
        platform.dimension = b.name;
        if (c) {
          platform.count = c.count;
        } else {
          platform.count = 0;
        }
        platformsDetails.push(platform);
      }
      /*for (const c of countByDimension) {
        const platform: any = { };
        platform.dimension = bannerDetails.find((x) => x.size === c._id).name;
        platform.count = c.count;
        platformsDetails.push(platform);
      }*/
      result.platform = platformsDetails;
      results.push(result);
    }
// tslint:disable-next-line: no-console
    console.timeEnd('getBehaviorDisplayLocationStackBarChart');
    return results;
  }

  public async getBehaviorTimeOfDayHeatChart(filter: FilterAnalyticsDTO, timezone: string, zone: string) {
    // tslint:disable-next-line: no-console
    console.time('getBehaviorTimeOfDayHeatChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const addOffset: any = this.helper.addOffset(timezone);

    const q: any = { };
    q.advertiserCampID = { $in: filter.subCampaignIds };
    q.dateAndTime = { $gte: startDate, $lte: endDate };
    q.typeOfEvent = 'CPM';

    if (filter.isHcpFilterSelected) {
      const allHcpIds: number[] = await this.getAudienceIds(filter, timezone, zone);
      const filteredHcpIds = await this.getFilteredHcp(filter, allHcpIds);
      q.hcpId = { $in: filteredHcpIds.map(String) };
    }

    const docs: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          projectDayOfWeek: {
            $dayOfWeek: {
              date: '$dateAndTime'
            }
          },
          projectHour: {
            $hour: {
              date: '$dateAndTime'
            }
          }
        }
      },
      {
        $group: {
          _id: {
            projectDayOfWeek: '$projectDayOfWeek',
            projectHour: '$projectHour'
          },
          count: { $sum: 1 }
        }
      }
    ]).exec();

    const days: string[] = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    const hours: string[] = ['00-03', '03-06', '06-09', '09-12', '12-15', '15-18', '18-21', '21-24'];

    const results: any[] = [];

    const dbResults: any = [];
    if (!this.checkNotNULL(docs)) {
      return [];
    }

    for (const doc of docs) {
      const result: any = { };
      result.weekDay = days[doc._id.projectDayOfWeek - 1];
      result.hourRange = this.getHourRange(doc._id.projectHour);
      result.count = doc.count;
      dbResults.push(result);
    }
    for (const day of days) {
      for (const hour of hours) {
        const result: any = { };
        result.weekDay = day;
        result.hourRange = hour;
        result.count = 0;
        results.push(result);
      }
    }

    for (const res of dbResults) {
      const temp = results.find((x) => (x.weekDay === res.weekDay && x.hourRange === res.hourRange));
      results[results.indexOf(temp)].count = results[results.indexOf(temp)].count + res.count;
    }
    const resultsWithNonZero: any[] = [];
    for (const res of results) {
      if (res.count > 0) {
        resultsWithNonZero.push(res);
      }
    }
    // tslint:disable-next-line: no-console
    console.timeEnd('getBehaviorTimeOfDayHeatChart');
    return resultsWithNonZero;
    // output will be of the format:
    // [ { dayOfWeek: 'Sunday', hourRange: '03-06', count: 21 } ]
  }

  public async getBehaviorDeviceDonutChart(filter: FilterAnalyticsDTO, timezone: string, zone: string) {
    // tslint:disable-next-line: no-console
    console.time('getBehaviorDeviceDonutChart');
    // this site lists the probable devices list we can get: https://developers.whatismybrowser.com/useragents/explore/operating_platform/
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    // the regex is picked from: https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
    // tslint:disable-next-line
    const mobilesAndTablets: string[] = ['/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))','smartphone','tablet'];
    // tslint:disable-next-line
    const mobiles: string[] = ['/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))','smartphone'];

    const q: any = { };
    q.advertiserCampID = { $in: filter.subCampaignIds };
    q.dateAndTime = { $gte: startDate, $lte: endDate };
    q.typeOfEvent = 'CPM';

    if (filter.isHcpFilterSelected) {
      const allHcpIds: number[] = await this.getAudienceIds(filter, timezone, zone);
      const filteredHcpIds = await this.getFilteredHcp(filter, allHcpIds);
      q.hcpId = { $in: filteredHcpIds.map(String) };
    }
    const allResultSet: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $group: {
          _id: '$deviceDetails.device.type',
          myCount: { $sum: 1 }
        }
      }
    ]).exec();

    let totalCount = 0;
    for (const a of allResultSet) {
      totalCount = totalCount + a.myCount;
    }

    const tablet = allResultSet.find((x) => x._id === 'tablet');
    let tabletCount = 0;
    if (tablet) { tabletCount = tablet.myCount; }

    const desktop = allResultSet.find((x) => x._id === 'desktop');
    let desktopCount = 0;
    if (desktop) { desktopCount = desktop.myCount; }

    const mobileCount: number = totalCount - tabletCount - desktopCount;

    // tslint:disable-next-line: no-console
    console.timeEnd('getBehaviorDeviceDonutChart');
    if (desktopCount > 0 || tabletCount > 0 || mobileCount > 0) {
      return { desktop: desktopCount, tablet: tabletCount, mobile: mobileCount };
    } else {
      return { };
    }
  }

  public async getConversionEngagementAreaLineChart(filter: FilterAnalyticsDTO, timezone: string, zone: string) {

    // tslint:disable-next-line: no-console
    console.time('getConversionEngagementAreaLineChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;
    const unit = u.unit;
    const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);

    const results: any[] = [];

    for (const x of xAxis) {
      const result: any = { };
      result.durationInstance = x;
      result.impressions = 0;
      result.reaches = 0;
      result.views = 0;
      result.clicks = 0;
      results.push(result);
    }

    const q: any = { };
    q.dateAndTime = { $gte: startDate, $lte: endDate };
    q.advertiserCampID = { $in: filter.subCampaignIds };

    if (filter.isHcpFilterSelected) {
      const allHcpIds: number[] = await this.getAudienceIds(filter, timezone, zone);
      const filteredHcpIds = await this.getFilteredHcp(filter, allHcpIds);
      q.hcpId = { $in: filteredHcpIds.map(String) };
    }

    const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
    const addOffset: any = this.helper.addOffset(timezone);

    // total number of impressions by month
    q.typeOfEvent = 'CPM';
    const impressions = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: 1 }
        }
      }
    ]).exec();
    // segregate impressions in the final result
    for (const impression of impressions) {
      for (const result of results) {
        if (result.durationInstance === impression._id) {
          result.impressions = impression.count;
          break;
        }
      }
    }

    // total number of clicks by month
    q.typeOfEvent = 'CPC';
    const clicks = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: 1 }
        }
      }
    ]).exec();
    // segregate clicks in the final result
    for (const click of clicks) {
      for (const result of results) {
        if (result.durationInstance === click._id) {
          result.clicks = click.count;
          break;
        }
      }
    }

    // total number of views by month
    q.typeOfEvent = 'CPV';
    const views = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: 1 }
        }
      }
    ]).exec();
    // segregate views in the final result
    for (const view of views) {
      for (const result of results) {
        if (result.durationInstance === view._id) {
          result.views = view.count;
          break;
        }
      }
    }

    // segregate unique hcpids by month
    q.typeOfEvent = 'CPM';
    addOffset.hcpId = '$hcpId';
    const reaches = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance,
          hcpId: '$hcpId'
        }
      },
      {
        $group: {
          _id: '$durationInstance', groupedHcpId: { $addToSet: '$hcpId' }
        }
      },
      {
        $unwind: '$groupedHcpId'
      },
      {
        $group: { _id: '$_id', hcpCount: { $sum: 1 } }
      }
    ]).exec();
    delete addOffset.hcpId;

    const totalReachInDuration = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $group: {
          _id: null, groupedHcpId: { $addToSet: '$hcpId' }
        }
      },
      {
        $unwind: '$groupedHcpId'
      },
      {
        $group: { _id: null, hcpCount: { $sum: 1 } }
      }
    ]).exec();
    // segregate views in the final result
    for (const reach of reaches) {
      for (const result of results) {
        if (result.durationInstance === reach._id) {
          result.reaches = reach.hcpCount;
          break;
        }
      }
    }

    const summary: any = { };
    let sImpression: number = 0;
    let sClick: number = 0;
    let sView: number = 0;
    for (const result of results) {
      sImpression = sImpression + result.impressions;
      sClick = sClick + result.clicks;
      sView = sView + result.views;
    }
    summary.impressions = sImpression;
    summary.clicks = sClick;
    summary.views = sView;
    if (totalReachInDuration && totalReachInDuration.length > 0 && totalReachInDuration[0].hcpCount) {
      summary.reaches = totalReachInDuration[0].hcpCount;
    } else {
      summary.reaches = 0;
    }

    // tslint:disable-next-line: no-console
    console.timeEnd('getConversionEngagementAreaLineChart');
    if (summary.clicks > 0 || summary.impressions > 0 || summary.reaches > 0 || summary.views > 0) {
      return { data: results, summary };
    } else {
      return { };
    }
  }

  public async getConversionResultsLineChart(filter: FilterAnalyticsDTO, timezone: string, zone: string) {
    // tslint:disable-next-line: no-console
    console.time('getConversionResultsLineChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;
    const unit = u.unit;
    const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);
    const results: any[] = [];

    for (const x of xAxis) {
      const result: any = { };
      result.durationInstance = x;
      result.avgCPM = 0;
      result.avgCPC = 0;
      result.avgCPV = 0;
      result.impressions = 0;
      result.clicks = 0;
      result.totalSpends = 0;
      result.ctr = 0;
      results.push(result);
    }

    const q: any = { };
    q.advertiserCampID = { $in: filter.subCampaignIds };
    q.dateAndTime = { $gte: startDate, $lte: endDate };

    const durationInstance: any = this.helper.getDurationInstance(unit, timezone);
    const addOffset: any = this.helper.addOffset(timezone);

    if (filter.isHcpFilterSelected) {
      const allHcpIds: number[] = await this.getAudienceIds(filter, timezone, zone);
      const filteredHcpIds = await this.getFilteredHcp(filter, allHcpIds);
      q.hcpId = { $in: filteredHcpIds.map(String) };
    }

    q.bidType = 'CPM';
    addOffset.bidAmount = '$bidAmount';
    const avgCpmAmounts = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance,
          projectBidAmount: { $toDouble: '$bidAmount' }
        }
      },
      {
        $group: {
          _id: '$durationInstance', avg: { $avg: '$projectBidAmount' }
        }
      }
    ]).exec();
    delete addOffset.bidAmount;
    for (const avgCpmAmount of avgCpmAmounts) {
      for (const result of results) {
        if (result.durationInstance === avgCpmAmount._id) {
          if (avgCpmAmount.avg) {
            result.avgCPM = avgCpmAmount.avg * 1000;
          }
          break;
        }
      }
    }

    q.bidType = 'CPC';
    addOffset.bidAmount = '$bidAmount';
    const avgCpcAmounts = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance,
          projectBidAmount: { $toDouble: '$bidAmount' }
        }
      },
      {
        $group: {
          _id: '$durationInstance', avg: { $avg: '$projectBidAmount' }
        }
      }
    ]).exec();
    delete addOffset.bidAmount;
    for (const avgCpcAmount of avgCpcAmounts) {
      for (const result of results) {
        if (result.durationInstance === avgCpcAmount._id) {
          if (avgCpcAmount.avg) {
            result.avgCPC = avgCpcAmount.avg;
          }
          break;
        }
      }
    }

    delete q.bidType;
    addOffset.bidAmount = '$bidAmount';
    const totalSpends = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance,
          projectBidAmount: { $toDouble: '$bidAmount' }
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: '$projectBidAmount' }
        }
      }
    ]).exec();
    delete addOffset.bidAmount;
    for (const totalSpend of totalSpends) {
      for (const result of results) {
        if (result.durationInstance === totalSpend._id) {
          if (totalSpend.count) {
            result.totalSpends = totalSpend.count;
          }
          break;
        }
      }
    }

    q.typeOfEvent = 'CPM';
    const impressions = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: 1 }
        }
      }
    ]).exec();

    q.typeOfEvent = { $in: ['CPC', 'CPV'] };
    const clicks = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: 1 }
        }
      }
    ]).exec();

    let totalImpressions: number = 0;
    let totalClicks: number = 0;

    for (const impression of impressions) {
      for (const result of results) {
        if (result.durationInstance === impression._id) {
          totalImpressions = totalImpressions + impression.count;
          result.impressions = impression.count;
          break;
        }
      }
    }

    for (const click of clicks) {
      for (const result of results) {
        if (result.durationInstance === click._id) {
          totalClicks = totalClicks + click.count;
          result.clicks = click.count;
          break;
        }
      }
    }

    for (const result of results) {
      if (result.impressions) {
        result.ctr = result.clicks * 100 / result.impressions;
      }
    }

    delete q.bidType;
    delete q.typeOfEvent;
    const avgCpcCpm = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $group: {
          _id: null,
          totalAmount: {
            $sum: {
              $cond: [
                { $eq: ['$typeOfEvent', '$bidType'] }, '$bidAmount', 0
              ]
            }
          },
          CPC: {
            $avg: {
              $cond: [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPC'] }
                  ]
                },
                '$bidAmount',
                null
              ]
            }
          },
          CPM: {
            $avg: {
              $cond: [
                {
                  $and: [
                    { $eq: ['$typeOfEvent', '$bidType'] },
                    { $eq: ['$bidType', 'CPM'] }
                  ]
                },
                '$bidAmount',
                null
              ]
            }
          }
        }
      },
      {
        $project: {
          _id: 1,
          CPC: 1,
          CPM: 1
        }
      }]
    ).exec();

    const summary: any = { };
    let spends: number = 0;
    for (const result of results) {
      spends = spends + result.totalSpends;
    }
    summary.ctr = totalClicks * 100 / totalImpressions;
    if (avgCpcCpm && avgCpcCpm.length > 0) {
      summary.cpm = avgCpcCpm[0].CPM ? avgCpcCpm[0].CPM * 1000 : 0;
      summary.cpc = avgCpcCpm[0].CPC ? avgCpcCpm[0].CPC : 0;
    } else {
      summary.cpm = 0;
      summary.cpc = 0;
    }
    summary.spends = spends;
    // tslint:disable-next-line: no-console
    console.timeEnd('getConversionResultsLineChart');
    if (avgCpcCpm.length || clicks.length || impressions.length || totalSpends.length || avgCpcAmounts.length || avgCpmAmounts.length) {
      return { data: results, summary };
    } else {
      return { };
    }
  }

  public async getOverviewTopAdAssetsScatterChart(filter: FilterAnalyticsDTO, timezone: string, zone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getOverviewTopAdAssetsScatterChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;

    const q: any = { };
    q.dateAndTime = { $gte: startDate, $lte: endDate };
    q.advertiserCampID = { $in: filter.subCampaignIds };
    q.typeOfEvent = 'CPM';

    if (filter.isHcpFilterSelected) {
      const allHcpIds: number[] = await this.getAudienceIds(filter, timezone, zone);
      const filteredHcpIds = await this.getFilteredHcp(filter, allHcpIds);
      q.hcpId = { $in: filteredHcpIds.map(String) };
    }
    // total reaches
    const reaches = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $group: {
          _id: { subcampaignId: '$advertiserCampID', assetType: '$typeOfAdvertisement' }, groupedHcpId: { $addToSet: '$hcpId' }
        }
      },
      {
        $unwind: '$groupedHcpId'
      },
      {
        $group: { _id: '$_id', hcpCount: { $sum: 1 } }
      }
    ]).exec();

    delete q.typeOfEvent;
    q.bidAmount = { $ne: null };
    const totalSpends = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: q
      },
      {
        $group: {
          _id: { subcampaignId: '$advertiserCampID', assetType: '$typeOfAdvertisement' }, count: { $sum: '$bidAmount' }
        }
      },
      {
        $sort: {
          count: -1
        }
      }
    ]).exec();

    const subcampaignIds: string[] = [];
    for (const r of reaches) {
      subcampaignIds.push(r._id.subcampaignId);
    }

    const subcampaignNames: any = await subCampaignModel.find({ _id: { $in: subcampaignIds } })
      .select({ name: 1 }).exec();

    const result: any[] = [];
    for (const r of reaches) {
      const te = totalSpends.find((x: any) => x._id.subcampaignId.valueOf().toString() === r._id.subcampaignId.valueOf().toString());
      const o: any = { };
      o.subcampaignName = subcampaignNames.find((x: any) => x._id.valueOf().toString() === r._id.subcampaignId.valueOf().toString()).name;
      o.assetType = r._id.assetType;
      o.reach = r.hcpCount;
      if (te) {
        o.totalSpends = te.count;
      } else {
        o.totalSpends = 0;
      }
      result.push(o);
    }
  // tslint:disable-next-line: no-console
    console.timeEnd('getOverviewTopAdAssetsScatterChart');
    return result;
  }

  public async getActiveCampaignsAdTypeColumnChart(filter: FilterAnalyticsDTO, timezone: string): Promise<any> {
    // tslint:disable-next-line: no-console
    console.time('getActiveCampaignsAdTypeColumnChart');
    const u = this.helper.getStartDateEndDateUnitForDashboard(filter, null);
    const startDate = u.startDate;
    const endDate = u.endDate;
    const unit = u.unit;
    const xAxis: string[] = this.helper.getXAxisPoints(startDate, endDate, unit, timezone);
    const results: any[] = [];

    for (const x of xAxis) {
      const result: any = { };
      result.durationInstance = x;
      result.bannerAds = 0;
      // result.videoAds = 0;
     // result.emailAds = 0;
      results.push(result);
    }

    const durationInstance: any = this.helper.getActivationAtInstance(unit, timezone);

    const q: any = { };
    q['activated.at'] = { $gte: startDate, $lte: endDate };
    q._id = { $in: filter.subCampaignIds };

    const addOffset: any = this.helper.addOffsetForActivatedAt(timezone);
    const activatedCampaigns = await subCampaignModel.aggregate([
      {
        $match: q
      },
      {
        $project: addOffset
      },
      {
        $project: {
          durationInstance
        }
      },
      {
        $group: {
          _id: '$durationInstance', count: { $sum: 1 }
        }
      }
    ]).exec();

    // tslint:disable-next-line: no-console
    console.timeEnd('getActiveCampaignsAdTypeColumnChart');
    if (this.checkNotNULL(activatedCampaigns)) {
      for (const a of activatedCampaigns) {
        for (const result of results) {
          if (result.durationInstance === a._id) {
            result.bannerAds = a.count;
            break;
          }
        }
      }
    } else {
      return [];
    }
    return results;
  }

  private populateValidateHcpFilter(q: any, filter: FilterAnalyticsDTO) {
    if (filter.specialization && filter.specialization.length > 0) {
      q.taxonomy = { $in: filter.specialization };
    }
    if (filter.location && filter.location.length > 0) {
      q.zip = { $in: filter.location };
    }
    if (filter.gender && filter.gender.length > 0) {
      q.gender = { $in: filter.gender };
    }
  }

  private async getFilteredHcp(filter: FilterAnalyticsDTO, hcpIds: number[]): Promise<number[]> {
    const q: any = { };
    q.npi = { $in: hcpIds };
    this.populateValidateHcpFilter(q, filter);

    const filteredHcp: any[] = await validatedHcpModel.aggregate([
      {
        $match: q
      },
      {
        $project: {
          _id: '$npi'
        }
      }
    ]).exec();
    return filteredHcp.map((x) => x._id);
  }

  private getArcheType(score: number, archetypes: ArchetypeDTO[]): string {
    for (const archetype of archetypes) {
      if (archetype.hcpMaxScoreRange === 'N/A') {
        archetype.hcpMaxScoreRange = archetype.hcpMinScoreRange + 1;
      }
      if (score >= parseInt(archetype.hcpMinScoreRange, 10) && score <= parseInt(archetype.hcpMaxScoreRange, 10)) {
       return archetype.name;
      }
    }
  }
  private getHourRange(hour: number): string {

    switch (hour) {
      case 0:
        return '00-03';
      case 1:
        return '00-03';
      case 2:
        return '00-03';
      case 3:
        return '03-06';
      case 4:
        return '03-06';
      case 5:
        return '03-06';
      case 6:
        return '06-09';
      case 7:
        return '06-09';
      case 8:
        return '06-09';
      case 9:
        return '09-12';
      case 10:
        return '09-12';
      case 11:
        return '09-12';
      case 12:
        return '12-15';
      case 13:
        return '12-15';
      case 14:
        return '12-15';
      case 15:
        return '15-18';
      case 16:
        return '15-18';
      case 17:
        return '15-18';
      case 18:
        return '18-21';
      case 19:
        return '18-21';
      case 20:
        return '18-21';
      case 21:
        return '21-24';
      case 22:
        return '21-24';
      case 23:
        return '21-24';
    }
  }
  private getDrawValueForLocationMap(score: number) {
    if (score >= 21 && score < 41) {
      return 35;
    } else if (score >= 41 && score < 61) {
      return 50;
    } else if (score >= 61 && score < 81) {
      return 75;
    } else if (score >= 81 && score < 101) {
      return 100;
    } else {
      return 10;
    }

  }
  private checkNotNULL(data: any) {
    if (data instanceof Array) {
        if (data.length > 0) {
           return true;
          }
        return false;
    } else if (data instanceof Object) {
        if (data && (Object.keys(data).length !== 0)) {
            return true;
        }
        return false;
    }
  }

  private getAdServerContentModel(zone: string) {
    if (zone === '1') {
        return usAdServerContentModel;
    } else if (zone === '2') {
        return indiaAdServerContentModel;
    } else {
        throw new Error(`Invalid zone ${zone}`);
    }
  }

  private getCityDetailModel(zone: string) {
    if (zone === '1') {
      return usCityDetailModel;
    } else if (zone === '2') {
      return indiaCityDetailModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }

}
