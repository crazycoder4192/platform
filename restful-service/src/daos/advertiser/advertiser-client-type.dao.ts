import AdvertiserClientTypeDTO from '../../dtos/advertiser/advertiser-client-type.dto';
import { advertiserClientTypeModel } from '../../models/advertiser/advertiser-client-types-schema';

export default class AdvertiserClientTypeDAO {
  public async createMany(
    details: AdvertiserClientTypeDTO[],
  ): Promise<AdvertiserClientTypeDTO[]> {
    return await advertiserClientTypeModel.insertMany(details);
  }

  public async findAll(): Promise<AdvertiserClientTypeDTO[]> {
    return await advertiserClientTypeModel.find().exec();
  }

  public async findUniqueType(): Promise<AdvertiserClientTypeDTO[]> {
    return await advertiserClientTypeModel.distinct('type').exec();
  }
}
