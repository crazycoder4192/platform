import mongoose from 'mongoose';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import { advertiserBrandModel } from '../../models/advertiser/advertiser-brand-schema';
import { logger } from '../../util/winston';

export default class AdvertiserBrandDAO {
    private readonly cap = '^' ;
    private readonly dollar = '$' ;

    public async create(dto: AdvertiserBrandDTO): Promise<AdvertiserBrandDTO> {
        const saveBrand = new advertiserBrandModel(dto);
        return await saveBrand.save();
    }

    public async findById(id: string, zone: string): Promise<AdvertiserBrandDTO> {
        return await advertiserBrandModel.findOne({ _id: id, zone }).exec();
    }

    public async findByIdWithoutZone(id: string): Promise<AdvertiserBrandDTO> {
        return await advertiserBrandModel.findOne({ _id: id }).exec();
    }

    public async getBrands(allBrandIds: any[], zone: string): Promise<AdvertiserBrandDTO[]> {
        return await advertiserBrandModel.aggregate([
            { $match: { _id: { $in: allBrandIds }, zone } },
        ]).exec();
    }

    public async getBrandsNotInPayments(allBrandIds?: any, zone?: string): Promise<AdvertiserBrandDTO[]> {
        return await advertiserBrandModel.find({ _id: { $in: allBrandIds }, zone }).exec();
    }

    public async getClientBrands(allBrandIds: string[], zone: string): Promise<AdvertiserBrandDTO[]> {
        return await advertiserBrandModel.aggregate([
            { $match: { _id: { $in: allBrandIds }, zone } },
            {
                $group: {
                    _id: {
                        clientName: '$clientName',
                    },
                    brands: {
                        $push: {
                        _id: '$_id',
                        brandName: '$brandName',
                        brandType: '$brandType',
                        clientType: '$clientType'
                      }
                    },
                }
            },
            {
                $project: {
                  _id: 0,
                  brands: '$brands',
                  clientName: '$_id.clientName'
                }
            },
        ]).exec();
    }

    public async update(id: any, dto: AdvertiserBrandDTO): Promise<AdvertiserBrandDTO> {
        const updateAdvertiserBrand = await advertiserBrandModel.findById(id).exec();
        Object.assign(updateAdvertiserBrand, dto);
        return await updateAdvertiserBrand.save();
    }
    public async delete(id: string, advertiserBrand: AdvertiserBrandDTO): Promise<AdvertiserBrandDTO> {
        return await advertiserBrandModel.findByIdAndUpdate(id, advertiserBrand, { new: true }).exec();
    }
    public async brandNameAvailability(bName: string, advId: string, zone: string): Promise<AdvertiserBrandDTO[]> {
        return await advertiserBrandModel.find({ brandName: { $regex: this.cap + bName + this.dollar, $options: 'i' },
                                                 advertiser : mongoose.Types.ObjectId(advId),
                                                 zone}).exec();
    }
    public async updateManyClientInfo(idList: string[], newClientName: string, newClientType: string): Promise<AdvertiserBrandDTO> {
        return await advertiserBrandModel.updateMany({ _id: { $in: idList } },
            { $set: { clientName : newClientName, clientType : newClientType }
        }).exec();
    }
    public async findByAdvertiserId(id: string, zone: string): Promise<AdvertiserBrandDTO[]> {
        return await advertiserBrandModel.find({ advertiser: id, zone }).exec();
    }

    public async getClientWiseBrands(allBrandIds: string[], zone: string): Promise<any[]> {
        return await advertiserBrandModel.aggregate([
            { $match: { _id: { $in: allBrandIds } } },
            {
                $group: {
                    _id: {
                        clientName: '$clientName',
                        clientType: '$clientType'
                    },
                    brands: {
                        $push: {
                        brandId: '$_id',
                        brandName: '$brandName',
                        brandType: '$brandType',
                        zone: '$zone'
                      }
                    },
                }
            },
            {
                $project: {
                  _id: 0,
                  clientName: '$_id.clientName',
                  clientType: '$_id.clientType',
                  brands: '$brands'
                }
            },
        ]).exec();
    }

    public async getOldestCreatedAt(allBrandIds: string[], zone: string): Promise<Date> {
        const result: any[] = await advertiserBrandModel.aggregate([
            { $match: { _id: { $in: allBrandIds }, zone } },
            {
                $sort: { 'created.at': 1 }
            },
            {
                $limit: 1
            }
        ]).exec();
        if (result && result.length > 0) {
            return result[0].created.at;
        }
        return null;
    }

    public async getBrandTypesByBrandIdsForAdmin(allBrands: any[], zone: string): Promise<any[]> {
        return await advertiserBrandModel.aggregate([
            {
                $match : {
                    _id: { $in: allBrands },
                    zone
                },
            },
            {
                $group: {
                    _id : { brandId: '$_id', brandType : '$brandType' }
                }
            },
            {
                $group: {
                    _id: '$_id.brandType',
                    brandIds: {
                        $push: {
                            brandId: '$_id.brandId'
                        }
                    }
                }
            }
        ]).exec();
    }

    public async getBrandNamesByBrandIdsForAdmin(allBrandIds: string[], zone: string): Promise<any[]> {
        return await advertiserBrandModel.find({ _id: { $in: allBrandIds }, zone }).select('brandName').exec();
    }

    public async updateMigrateZone(id: any, dto: AdvertiserBrandDTO): Promise<AdvertiserBrandDTO> {
        const updateBrand = await advertiserBrandModel.findById(id).exec();
        Object.assign(updateBrand, dto);
        return await updateBrand.save();
    }

    public async findAllMigrateZone(id: string): Promise<AdvertiserBrandDTO[]> {
        return await advertiserBrandModel.find({ advertiser: id }).exec();
    }

}
