import mongoose from 'mongoose';
import CreativeHubDTO from '../../dtos/advertiser/creative-hub.dto';
import { creativeHubModel } from '../../models/advertiser/creative-hub-schema';
export default class CreativeHubDAO {

    public async create(creativeHubDTO: CreativeHubDTO): Promise<CreativeHubDTO> {
        const creativeHub = new creativeHubModel(creativeHubDTO);
        return await creativeHub.save();
    }

    public async findByBrandId(brandId: string, type: string): Promise<CreativeHubDTO[]> {
        // return await creativeHubModel.find({ advertiserId: advId, creativeType: type, deleted: false }).exec();
        return await creativeHubModel.aggregate([
            { $match: {
                brandId,
                creativeType : type,
                deleted: false
              }
            },
            {
                $group: {
                    _id: {
                        sectionName: '$sectionName',
                    },
                    creativeHubs: {
                        $push: {
                        _id: '$_id',
                        name: '$name',
                        creativeDetails: '$creativeDetails',
                        deletedUsers: '$deletedUsers',
                        deleted: '$deleted',
                        advertiserId: '$advertiserId',
                        status: '$status',
                        creativeType: '$creativeType',
                        created: '$created'
                      }
                    },
                }
            },
            {
                $project: {
                  _id : 0,
                  creativeHubsSectionWise: '$creativeHubs',
                  sectionName: '$_id.sectionName'
                }
            },
        ]).exec();
    }

    public async findById(id: string): Promise<CreativeHubDTO> {
        return await creativeHubModel.findById(id).exec();
    }

    public async findByName(subCampaignName: string): Promise<CreativeHubDTO[]> {
        return await creativeHubModel.find({ name: subCampaignName, deleted: false }).exec();
    }

    public async update(id: string, creativeHubDTO: CreativeHubDTO): Promise<CreativeHubDTO> {
        const updateCreativeHub = await creativeHubModel.findOne({ _id: id, deleted: false }).exec();
        Object.assign(updateCreativeHub, creativeHubDTO);
        return await updateCreativeHub.save();
    }

    public async delete(id: string, creativeHubDTO: CreativeHubDTO): Promise<CreativeHubDTO> {
        return await creativeHubModel.findByIdAndUpdate(id, creativeHubDTO, { new: true }).exec();
    }
    public async deleteMany(creativeHubs: any): Promise<CreativeHubDTO> {
        return await creativeHubModel.updateMany(creativeHubs, { new: true }).exec();
    }
    public async updateSection(id: string, creativeHubDTO: CreativeHubDTO): Promise<CreativeHubDTO> {
        return await creativeHubModel.findByIdAndUpdate(id, creativeHubDTO, { new: true }).exec();
    }
    public async findAllByBannerSize(brandId: string, bannerSize: string): Promise<CreativeHubDTO[]> {
        return await creativeHubModel.find({
            'brandId': brandId,
            'creativeDetails.size': bannerSize,
            'deleted': false,
            'creativeType' : 'image'
        }).exec();
    }
    public async findAllVideoCreatives(brandId: string): Promise<CreativeHubDTO[]> {
        return await creativeHubModel.find({
            brandId,
            deleted: false,
            creativeType : 'video'
        }).exec();
    }

    public async findByBrandIdAndName(brandId: string, name: string): Promise<CreativeHubDTO[]> {
        return await creativeHubModel.find({
            brandId: mongoose.Types.ObjectId(brandId),
            name,
            deleted: false
        }).exec();
    }
}
