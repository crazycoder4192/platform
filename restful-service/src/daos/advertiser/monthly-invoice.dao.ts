import MonthlyInvoiceDTO from '../../dtos/advertiser/monthly-invoice.dto';
import { indiaMonthlyInvoiceModel } from '../../models/advertiser/monthly-invoice-india-schema';
import { usMonthlyInvoiceModel } from '../../models/advertiser/monthly-invoice-us-schema';
import { CreditLineStatus } from '../../util/constants';
import { logger } from '../../util/winston';

export default class MonthlyInvoiceDAO {

    public async create(dto: MonthlyInvoiceDTO, zone: string): Promise<MonthlyInvoiceDTO> {
        if (zone === '1') {
            const saveNotification = new usMonthlyInvoiceModel(dto);
            return await saveNotification.save();
        } else if (zone === '2') {
            const saveNotification = new indiaMonthlyInvoiceModel(dto);
            return await saveNotification.save();
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
    public async findById(id: any, zone: string): Promise<MonthlyInvoiceDTO> {
        return await this.getMonthlyInvoiceModel(zone).findOne({ _id: id }).exec();
    }

    public async update(id: any, dto: MonthlyInvoiceDTO, zone: string): Promise<MonthlyInvoiceDTO> {
        const updateCreditLine = await this.getMonthlyInvoiceModel(zone).findById(id).exec();
        Object.assign(updateCreditLine, dto);
        return await updateCreditLine.save();
    }

    public async findByAdvertiserId(advertiserId: any, zone: string): Promise<MonthlyInvoiceDTO[]> {
        return await this.getMonthlyInvoiceModel(zone).find({ advertiserId }).sort({ startDate: 1 }).exec();
    }

    public async findByStatus(creditlineStatus: any, zone: string): Promise<MonthlyInvoiceDTO[]> {
        return await this.getMonthlyInvoiceModel(zone).find({ status: creditlineStatus }).exec();
    }

    public async findActiveMonthlyInvoice(advertiserId: any, currentDate: Date, zone: string): Promise<MonthlyInvoiceDTO> {
        const dtos: MonthlyInvoiceDTO[] = await this.getMonthlyInvoiceModel(zone).find({
            advertiserId,
            status: CreditLineStatus.approved,
            startDate: { $lt: currentDate }
        }).sort({ startDate: 'desc' }).exec();
        if (dtos && dtos.length > 0) {
            if (dtos.length === 1) {
                return dtos[0];
            } else {
                logger.error('More than 1 approved credit line found. Picking the last one');
                return dtos[dtos.length - 1];
            }
        }
        return null;
    }

    private getMonthlyInvoiceModel(zone: string) {
        if (zone === '1') {
            return usMonthlyInvoiceModel;
        } else if (zone === '2') {
            return indiaMonthlyInvoiceModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
}
