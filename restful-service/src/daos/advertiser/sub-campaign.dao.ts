import config from 'config';
import MongoClient from 'mongodb';
import mongoose from 'mongoose';
import { advertiserBrandModel } from '../..//models/advertiser/advertiser-brand-schema';
import { campaignModel } from '../..//models/advertiser/campaign-schema';
import SubCampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import { advertiserModel } from '../../models/advertiser/advertiser-schema';
import { subCampaignModel } from '../../models/advertiser/sub-campaign-schema';
import { userModel } from '../../models/user-schema';
import { archetypeModel } from '../../models/utils/archetype-schema';

import { usIncomingHcpRequestDetailsModel } from '../../models/incoming-hcp-request-details-us.schema';
import { publisherAssetModel } from '../../models/publisher/asset-schema';
import { publisherPlatformModel } from '../../models/publisher/platform-schema';
import { indiaAdServerContentModel } from '../../models/utils/adservercontent-india-schema';
import { usAdServerContentModel } from '../../models/utils/adservercontent-us-schema';
import { validatedHcpModel } from '../../models/validated-hcp.schema';
import { SubcampaignStatus } from '../../util/constants';
import { logger } from '../../util/winston';

const ObjectId = mongoose.Types.ObjectId;
const statusObj: any = ['Error', 'Under review', 'On hold', 'Paused', 'Active', 'Completed', 'Recently Completed'];

export default class SubCampaignDAO {

  private readonly cap = '^' ;
  private readonly dollar = '$' ;

  public async createMany(details: SubCampaignDTO[]): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.insertMany(details);
  }

  public async create(dto: SubCampaignDTO): Promise<SubCampaignDTO> {
    const createModel = new subCampaignModel(dto);
    return await createModel.save();
  }

  public async update(id: any, dto: SubCampaignDTO): Promise<SubCampaignDTO> {
    const updateModel = await subCampaignModel.findById(id).exec();
    Object.assign(updateModel, dto);
    return await updateModel.save();
  }

  public async delete(id: string, dto: SubCampaignDTO) {
    return await subCampaignModel.findByIdAndUpdate(id, dto, { new: true }).exec();
  }

  public async findById(id: string): Promise<SubCampaignDTO> {
    return await subCampaignModel.findById(id).exec();
  }

  public async findByIds(subcampaignIds: string[]): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ _id: { $in: subcampaignIds } }).exec();
  }
  public async findByWatchingBrandId(watchingBrandId: string): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ brandId: watchingBrandId }).exec();
  }

  public async findDrafts(emailId: string, id: string): Promise<any> {
    const campaigns = await campaignModel.find({ brandId: id, deleted: false }).exec();
    const Ary = Array();
    for (const campaign of campaigns) {
      const user: any = await userModel.findOne({ email: campaign.created.by }).exec();
      let createdUser = user.created.by;
      if (user.firstName && user.lastName) {
      createdUser = `${user.firstName}  ${user.lastName}`;
      }
      const subCampaigns = await subCampaignModel.find({
        campaignId: campaign._id, brandId: campaign.brandId, deleted: false, isPublished: false, isApproved: false
      }).exec();

      if (subCampaigns.length === 0) { continue; }

      const startDate = subCampaigns.sort((a: any, b: any) => {
        const dateA: any = new Date(a.created.at);
        const dateB: any = new Date(b.created.at);
        return (dateA - dateB);
      }).map((p: any) => p.created.at);
      const endDate = subCampaigns.sort((a: any, b: any) => {
        const dateA: any = new Date(a.modified.at);
        const dateB: any = new Date(b.modified.at);
        return (dateB - dateA);
      }).map((p: any) => p.modified.at);
      const sortedStartDate = startDate[0] ? startDate[0] : campaign.created.at;
      const sortedModifiedDate = endDate[0] ? endDate[0] : campaign.modified.at;
      for (const subcampaign of subCampaigns) {
        const subCampaignUser: any = await userModel.findOne({ email: subcampaign.created.by }).exec();
        let subCampaignCreatedUser = subCampaignUser.created.by;
        if (subCampaignUser.firstName && subCampaignUser.lastName) {
          subCampaignCreatedUser = `${subCampaignUser.firstName}  ${subCampaignUser.lastName}`;
        }
        subcampaign.created.by = subCampaignCreatedUser;
      }
      Ary.push({
        campaign: campaign.name, dateCreated: sortedStartDate,
        lastSaved: sortedModifiedDate,
        createdBy: createdUser,
        id: campaign._id, brandId: campaign.brandId, subDomainDetails: subCampaigns
      });
    }
    return Ary;
  }

  public async findPublishedCampaigns(): Promise<any> {
    return await subCampaignModel.aggregate([
      { $match: { isPublished: true, deleted: false } },
      {
        $lookup: {
          localField: 'campaignId',
          from: 'advertiser_campaigns',
          foreignField: '_id',
          as: 'campaigninfo'
        }
      },
      {
        $lookup: {
          localField: 'brandId',
          from: 'advertiser_brands',
          foreignField: '_id',
          as: 'brandinfo'
        }
      },
      {
        $project: {
          'name': 1,
          'creativeType': 1,
          'creativeSpecifications.creativeDetails': 1,
          'operationalDetails': 1,
          'created.at': 1,
          'modified.at': 1,
          'isApproved': 1,
          'status': 1,
          'brandId': 1,
          'campaignName': { $arrayElemAt: ['$campaigninfo.name', 0] },
          'brandName': { $arrayElemAt: ['$brandinfo.brandName', 0] },
          'campaigninfo': 1,
          'zone': 1
        }
      }
    ])
      .exec();
  }

  public async findCampaignWithSubCampaigns(id: string, zone: string): Promise<any> {
    const brandDetails = await advertiserBrandModel.aggregate([{ $match: { _id: id } }]).exec();
    const campaignDetails = await campaignModel.aggregate([{ $match: { brandId: id } }]).exec();
    for (const campaign of campaignDetails) {
      campaign.brandName = brandDetails[0].brandName;
      campaign.subCampaignInfo = await subCampaignModel.aggregate([{
        $match: { campaignId: campaign._id, deleted: false, isPublished: true }
      }]).exec();
      campaign.campaingSpend = 0;
      campaign.campaingReach = 0;
      let transactionalData: any;
      const subcampaignIds: any[] = [];
      for (const subCampaign of campaign.subCampaignInfo) {
        subcampaignIds.push(subCampaign._id);
        transactionalData = await this.getAdServerContentModel(zone).aggregate(
          [
            {
              $match: { advertiserCampID: subCampaign._id }
            },
            {
              $group: {
                _id: '$advertiserCampID',
                TotalAmount: {
                  $sum: {
                    $convert: {
                      input: {
                        $cond: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          '$bidAmount',
                          0
                        ]
                      },
                      to: 'double'
                    }
                  }
                },
                Clicks: {
                  $sum: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPC'] },
                      1,
                      0
                    ]
                  }
                },
                Impressions: {
                  $sum: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPM'] },
                      1,
                      0
                    ]
                  }
                },
                Views: {
                  $sum: {
                    $cond: [
                      { $eq: ['$typeOfEvent', 'CPV'] },
                      1,
                      0
                    ]
                  }
                },
                uniqueReach: {
                  $addToSet: '$hcpId'
                },
                CPC: {
                  $avg: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPC'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  }
                },
                CPM: {
                  $avg: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPM'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  }
                },
                CPV: {
                  $avg: {
                    $cond: [
                      {
                        $and: [
                          { $eq: ['$typeOfEvent', '$bidType'] },
                          { $eq: ['$bidType', 'CPV'] }
                        ]
                      },
                      '$bidAmount',
                      null
                    ]
                  }
                }
              }
            },
            {
              $project:
              {
                TotalAmount: 1,
                Clicks: 1,
                Impressions: 1,
                Views: 1,
                uniqueReach: 1,
                reachCount: {
                  $size: '$uniqueReach'
                },
                CPC: 1,
                CPM: { $multiply: ['$CPM', 1000] },
                CPV: 1,
              }
            }]
        ).exec();
        if (transactionalData.length > 0 && transactionalData[0].TotalAmount !== undefined) {
          campaign.campaingSpend = (campaign.campaingSpend + +transactionalData[0].TotalAmount);
        }
        /*if (transactionalData.length > 0 && transactionalData[0].reachCount !== undefined) {
          campaign.campaingReach = (campaign.campaingReach + +transactionalData[0].reachCount);
        }*/
        const totalReachInDuration = await this.getAdServerContentModel(zone).aggregate(
        [
          {
            $match: { advertiserCampID: { $in: subcampaignIds } }
          },
          {
            $group: {
              _id: null, groupedHcpId: { $addToSet: '$hcpId' }
            }
          },
          {
            $unwind: '$groupedHcpId'
          },
          {
            $group: { _id: null, hcpCount: { $sum: 1 } }
          }
        ]).exec();
        if (totalReachInDuration && totalReachInDuration.length > 0 && totalReachInDuration[0].hcpCount) {
          campaign.campaingReach = totalReachInDuration[0].hcpCount;
        } else {
          campaign.campaingReach = 0;
        }
        subCampaign.transactionalData = transactionalData;
      }
    }

    const finalCampaignAry = Array();
    for (const campaign of campaignDetails) {
      const campaignTemp = Object.assign({ }, campaign);
      const startDate = await campaignTemp.subCampaignInfo.sort((a: any, b: any) => {
        const dateA: any = new Date(a.operationalDetails.startDate);
        const dateB: any = new Date(b.operationalDetails.startDate);
        return (dateA - dateB);
      }).map((p: any) => p.operationalDetails.startDate);
      const endDate = await campaignTemp.subCampaignInfo.sort((a: any, b: any) => {
        const dateA: any = new Date(a.operationalDetails.endDate);
        const dateB: any = new Date(b.operationalDetails.endDate);
        return (dateB - dateA);
      }).map((p: any) => p.operationalDetails.endDate);
      const status: any = [...new Set(await campaignTemp.subCampaignInfo.map((p: any) => p.status
      ))];
      campaignTemp.status = status[0];
      const statusTemp = statusObj.forEach((item: any) => {
        if (!campaignTemp.status) {
          campaignTemp.subCampaignInfo.find((obj: any) => {
            if (obj.status === item) {
              campaignTemp.status = item;
            }
          });
        } else {
          return campaignTemp.status;
        }
      });

      campaignTemp.startDate = startDate[0];
      campaignTemp.endDate = endDate[0];
      // campaignTemp.status = statusTemp;
      const user = await userModel.findOne({ email: campaign.created.by }).exec();
      campaignTemp.createdBy = `${user.firstName} ${user.lastName}`;

      if (campaignTemp.startDate && campaignTemp.endDate) {
        finalCampaignAry.push(campaignTemp);
      }
    }
    return finalCampaignAry;
  }

  public async providerList(emailId: string): Promise<any> {
    return await advertiserModel.aggregate([
      {
        $lookup: {
          localField: '_id',
          from: 'advertiser_sub_campaigns',
          foreignField: 'advertiserId',
          as: 'subCampaignInfo'
        }
      },
      {
        $project: {
          '_id': 1,
          'businessName': 1,
          'advertiserType': 1,
          'created.at': 1,
          'subCampaignInfo': {
            $filter: {
              input: '$subCampaignInfo',
              as: 'subCampaign',
              cond: {
                $eq: ['$$subCampaign.status', 'Active']
              }
            }
          }
        }
      }
    ])
      .exec();
  }

  public async getPotentialReachAgainstToCampaingDetails(campaignDetails: any): Promise<any> {
    try {
      let validHCPResults: any;
      let potentialResults: any[];
      let bannerResponse: any;
      let specializationQuery: any;
      let locationQuery: any;
      const campaignHCPScore: any = await archetypeModel.find({ name: { $in: campaignDetails.demoGraphics.behaviours } }).exec();
      const campaignTaxonomy: any[] = campaignDetails.demoGraphics.specialization.areaOfSpecialization;
      const campaignGender: string[] = campaignDetails.demoGraphics.gender;
      const campaignZip: any[] = [];
      const ls: any[] = campaignDetails.geoLocation.locations;
      for (const l of ls) {
        const zs: string = l.zipcode;
        for (const z of zs.split(',')) {
          campaignZip.push(z);
        }
      }
      validHCPResults = await validatedHcpModel.find({ }).exec();
      const scoreQuery: any[] = [];
      campaignHCPScore.forEach((campaignBehaviourItem: any) => {
        if (campaignBehaviourItem.hcpMinScoreRange && campaignBehaviourItem.hcpMaxScoreRange
          && campaignBehaviourItem.hcpMaxScoreRange !== 'N/A') {
          scoreQuery.push({
            'hcpScore.cumulativeScore':
              { $gte: +campaignBehaviourItem.hcpMinScoreRange, $lte: +campaignBehaviourItem.hcpMaxScoreRange }
          });
        } else if (campaignBehaviourItem.hcpMaxScoreRange === 'N/A') {
          scoreQuery.push({
            'hcpScore.cumulativeScore':
              { $gte: +campaignBehaviourItem.hcpMinScoreRange }
          });
        }
      });

      const genderInitials: string[] = [];
      campaignGender.forEach((campaignGenderItem: string) => {
        if (campaignGenderItem.toLowerCase() === 'male') {
          genderInitials.push('M');
        }
        if (campaignGenderItem.toLowerCase() === 'female') {
          genderInitials.push('F');
        }
        if (campaignGenderItem.toLowerCase() === 'other') {
          genderInitials.push('O');
        }
      });

      if (JSON.parse(campaignDetails.geoLocation.isInclude)) {
        locationQuery = { $in: campaignZip };
      } else {
        locationQuery = { $nin: campaignZip };
      }
      if (JSON.parse(campaignDetails.demoGraphics.specialization.isInclude)) {
        specializationQuery = { $in: campaignTaxonomy };
      } else {
        specializationQuery = { $nin: campaignTaxonomy };
      }

      const query = {
        $and: [
          { taxonomy: specializationQuery },
          { zip: locationQuery },
          { gender: { $in: genderInitials } },
          { $or: scoreQuery }
        ]
      };
      potentialResults = await validatedHcpModel.find(query).exec();
      const npis: number[] = potentialResults.map((x) => x.npi);
      bannerResponse = await this.getBannerSizesAndBidRange(npis);
      return {
        totalHcps: validHCPResults.length,
        potentialReach: potentialResults.length,
        potentialResponse: potentialResults,
        bannerDetails: bannerResponse
      };
    } catch (err) {
      throw err;
    }
  }

  public async getBannerSizesAndBidRange(npis: number[]): Promise<any> {
    let bidRange: any = { };
    let bannerSizes: any[] = [];
    let platformIds: any[] = [];

    if (npis && npis.length > 0) {
      const uniquePlatforms: any[] = await usIncomingHcpRequestDetailsModel.aggregate([
        {
          $match: { matchedNpi: { $in: npis } }
        },
        {
          $group: {
            _id: null,
            uids: { $addToSet: '$platformId' }
          }
        }
      ]).exec();

      if (uniquePlatforms && uniquePlatforms.length > 0) {
        platformIds = uniquePlatforms[0].uids.map((x: any) => ObjectId(x));
      }
    }

    // TODO: till the time we have a decent base
    // if (platformIds.length === 0 || npis.length < 100) {
    const allPlatforms = await publisherPlatformModel.find({ isDeleted: false, isDeactivated: false }).exec();
    for (const r of allPlatforms) {
      platformIds.push(r._id);
    }
    // }

    bidRange = await publisherAssetModel.aggregate([
      {
        $match: {
          platformId: { $in: platformIds }
        }
      },
      {
        $group: {
          _id: null,
          minCpcBidRange: { $min: '$bidRange.cpc.min' },
          maxCpcBidRange: { $max: '$bidRange.cpc.max' },
          minCpmBidRange: { $min: '$bidRange.cpm.min' },
          maxCpmBidRange: { $max: '$bidRange.cpm.max' }
        }
      }
    ]).exec();

    const assetDimensions: any[] = await publisherAssetModel.aggregate([
      {
        $match: {
          platformId: { $in: platformIds }
        }
      },
      {
        $group: {
          _id: { size: '$assetDimensions', platformType: '$platformType' }
        }
      }
    ]).exec();

    if (assetDimensions && assetDimensions.length) {
      bannerSizes = assetDimensions.map((x) => x._id);
    }

    let minCpcBidRange = 0;
    // setting it to 100 for the cases where in the initial days, we wont be having any
    // assets
    let maxCpcBidRange = 100;
    let minCpmBidRange = 0;
    let maxCpmBidRange = 100;
    if (bidRange && bidRange.length > 0) {
      minCpcBidRange = bidRange[0].minCpcBidRange;
      maxCpcBidRange = bidRange[0].maxCpcBidRange;
      minCpmBidRange = bidRange[0].minCpmBidRange;
      maxCpmBidRange = bidRange[0].maxCpmBidRange;
    }
    const result: any = {
      cpc: {
        minCpcBidRange,
        maxCpcBidRange
      },
      cpm: {
        minCpmBidRange,
        maxCpmBidRange
      },
      bannerSizes
    };
    return result;
  }

  public async listOfActiveSubCampaignsByBrandId(id: string): Promise<any> {
    return await subCampaignModel.find({ brandId: id, deleted: false, status: 'Active' }).exec();
  }

  public async listOfActiveSubCampaignsByBrand(id: string): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.aggregate([
      {
        $match: {
          brandId: id,
          deleted: false,
          status: 'Active'
        }
      }
    ]).exec();
  }

  public async listAllSubcampaignsByBrandId(id: string): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ brandId: id }).exec();
  }

  public async listOfSubCampaignsByBrandId(id: string): Promise<any> {
    return await subCampaignModel.find({ brandId: id, deleted: false }).exec();
  }

  public async listOfSubCampaignsByCampaignId(id: string): Promise<any> {
    return await subCampaignModel.find({ campaignId: id, isPublished: true, deleted: false }).exec();
  }

  public async findLast3SubcampaignScores(advertiserId: string, zone: string): Promise<number> {
    const subcampaignScore: number[] = [];

    const last3SubcampaignDTOs: SubCampaignDTO[] = await subCampaignModel.find({
      'advertiserId': advertiserId,
      'status': 'completed',
      'operationalDetails.endDate': { $gte: new Date() }
    }).sort({ 'operationalDetails.endDate': -1 })
    .limit(3).
    exec();

    for (const past3SubcampaignId of last3SubcampaignDTOs) {
      const clicksJson: any = await this.getAdServerContentModel(zone).aggregate([
          {
            $match: {
              subCampaignId: past3SubcampaignId,
              clickedOn: { $exists: true, $ne: null }
            }
          },
          {
            $count: 'no_of_clicks'
          }
        ]).exec();

      let clicks: number = 0;
      if (clicksJson) {
          clicks = clicksJson.no_of_clicks;
      }

      const impressionsJson: any = await this.getAdServerContentModel(zone).aggregate([
          {
            $match: {
              subCampaignId: past3SubcampaignId,
              viewedOn: { $exists: true, $ne: null }
            }
          },
          {
            $count: 'no_of_impressions'
          }
        ]).exec();

      let impressions: number = 0;
      if (impressionsJson) {
          impressions = impressionsJson.no_of_impressions;
      }

      if (clicks === 0 || impressions === 0) {
          subcampaignScore.push(0);
      } else {
          subcampaignScore.push(clicks / impressions);
      }
    }
    let sum: number = 0;
    for (const s of subcampaignScore) {
        sum = sum + s;
    }
    // save the score in the subcampaign table
    return sum / 3;
  }

  public async getAverageCtr(advertiserId: string, zone: string): Promise<number> {
    let avg: number = +config.get<string>('average-default-ctr');
    let allSubcampaigns: any[] = await subCampaignModel.find({
      'advertiserId': advertiserId,
      'status': 'completed',
      'operationalDetails.endDate': { $gte: new Date() }
    }).select('name').exec();

    if (allSubcampaigns.length < 3) {
        allSubcampaigns = await subCampaignModel.find({
          'status': 'completed',
          'operationalDetails.endDate': { $gte: new Date() }
        }).select('name').exec();
    }

    if (allSubcampaigns.length >= 3) {
      const clicks: any = await this.getAdServerContentModel(zone).aggregate([
        {
          $match: {
            advertiserCampID: { $in: allSubcampaigns.map((x) => x._id) },
            typeOfEvent: 'CPC'
          }
        },
        {
          $group: { _id: null, count: { $sum: 1 } }
        }
      ]).exec();

      const impressions: any = await this.getAdServerContentModel(zone).aggregate([
        {
          $match: {
            advertiserCampID: { $in: allSubcampaigns.map((x) => x._id) },
            typeOfEvent: 'CPM'
          }
        },
        {
          $group: { _id: null, count: { $sum: 1 } }
        }
      ]).exec();
      if (impressions.count > 0 && clicks.count > 0) {
        avg = clicks.count / impressions.count;
      }
    }
    return avg;
  }

  public async subCampaignNameAvailability(sCampName: string, campId: string): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ name: { $regex: this.cap + sCampName + this.dollar, $options: 'i' },
                                         campaignId: mongoose.Types.ObjectId(campId), deleted: false }).exec();
  }

  public async getHcp(type: string, id: string, zone: string): Promise<number[]> {
    let subCampaignIds: string[] = [];
    if (type.toLowerCase() === 'brand') {
      const s: any[] = await this.listOfSubCampaignsByBrandId(id);
      subCampaignIds = s.map((x) => x._id);
    } else if (type.toLowerCase() === 'campaign') {
      const s: any[] = await this.listOfSubCampaignsByCampaignId(id);
      subCampaignIds = s.map((x) => x._id);
    } else if (type.toLowerCase() === 'subcampaign') {
      subCampaignIds = [id];
    } else {
      throw new Error('Unknown type; expected type to be brand, campaign, and subcampaign');
    }

    const reaches: any[] = await this.getAdServerContentModel(zone).aggregate([
      {
        $match: {
          advertiserCampID: { $in: subCampaignIds },
          typeOfEvent: 'CPM',
          hcpId: { $ne: 'N/A' }
        }
      },
      {
        $group: {
          _id: '$hcpId'
        }
      }
    ]).exec();

    return reaches.map((x) => parseInt(x._id, 10));
  }

  public async findBrandIdBySubcampaignId(id: string): Promise<SubCampaignDTO> {
    return await subCampaignModel.findOne({ _id: id }).select('brandId').exec();
  }

  public async findAllAdvertiser(): Promise<any[]> {
    return await subCampaignModel.distinct('advertiserId').exec();
  }

  public async findActiveSubcampaigns(subcampaignIds: string[]): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ _id: { $in: subcampaignIds }, status: 'Active' }).select('activated').exec();
  }

  public async findCountOfActiveCampaignsByAdvertiserId(advIds: any): Promise<any[]> {
    return await subCampaignModel.aggregate([
      {
        $match: {
          deleted: false,
          status: 'Active',
          advertiserId: { $in: advIds }
        }
      },
      {
        $group: {
          _id: { advId: '$advertiserId', campaignId: '$campaignId' },
        }
      },
      {
        $group: {
          _id: { advertiserId: '$_id.advId' },
          activeCampaigns: { $sum: 1 }
        }
      }
    ]).exec();
  }

  public async findCountOfTotalCampaignsByAdvertiserId(advId: any): Promise<number> {
    const result = await subCampaignModel.aggregate([
      {
        $match: {
          advertiserId: advId._id,
          isPublished: true
        }
      },
      {
        $group: {
          _id: { advId: '$advertiserId' },
          count: { $sum: 1 }
        }
      }
    ]).exec();
    if (result && result.length > 0) {
      return result[0].count;
    }
    return 0;
  }

  public async findSubcampaignCandidateForRecentlyCompleted(): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.aggregate([
      {
        $match: {
          'operationalDetails.endDate' : { $lt : new Date() },
          'status': { $ne: SubcampaignStatus.recentlyCompleted }
        }
      },
      {
        $project: {
          status: 1
        }
      }
    ]).exec();
  }

  public async findSubcampaignCandidateForCompleted(): Promise<SubCampaignDTO[]> {
    const date7DaysAgo = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
    return await subCampaignModel.aggregate([
      {
        $match: {
          'status': SubcampaignStatus.recentlyCompleted,
          'modified.at': { $lt : date7DaysAgo }
        }
      },
      {
        $project: {
          status: 1
        }
      }
    ]).exec();
  }

  public async findByAudienceTargetIds(audienceTargetId: string): Promise<SubCampaignDTO[]> {
    return await subCampaignModel.find({ audienceTargetIds: { $in: [audienceTargetId] } }).exec();
  }

  public async getSubCampaignDetailsByIdsForAdmin(ids: any[]): Promise<any[]> {
    return await subCampaignModel.find({ _id: { $in: ids } }).exec();
  }

  public async getActiveAudienceTargetIds(): Promise<any[]> {
    const subcampaignAudienceTargetIds: any[] = await subCampaignModel.find({ isPublished: true, isApproved: true, status: 'Active' }).select('audienceTargetIds').exec();
    let tempIds: any[] = [].concat(...subcampaignAudienceTargetIds.map((o) => o.audienceTargetIds));
    tempIds = tempIds.map((o) => o.toString());
    const allTargetIds: any[] = [...new Set(tempIds)];
    const allTargetObjectIds: any[] = [];
    for (const a of allTargetIds) {
        allTargetObjectIds.push(new ObjectId(a));
    }
    return allTargetObjectIds;
  }

  public async getSubCampaignStatusByIdsForAdmin(ids: any[]): Promise<any[]> {
    const data = await subCampaignModel.aggregate([
      {
        $match: {
          brandId: { $in: ids }
        }
      },
      {
        $group: {
          _id: '$brandId',
          active : {
            $sum: { $cond: [{ $eq: ['$status', 'Active'] }, 1, 0] }
          },
          inactive : {
            $sum: { $cond: [{ $eq: ['$status', 'Inactive'] }, 1, 0] }
          }
        }
      }
    ]).exec();
    return data;
 }

  public async getSubCampaignNamesByIdsForAdmin(ids: any[]): Promise<any[]> {
    return await subCampaignModel.find({ _id: { $in: ids } }).select('name').exec();
  }

  private getAdServerContentModel(zone: string) {
    if (zone === '1') {
      return usAdServerContentModel;
    } else if (zone === '2') {
      return indiaAdServerContentModel;
    } else {
      throw new Error(`Invalid zone ${zone}`);
    }
  }

}
