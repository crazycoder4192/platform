import ObjectivesDTO from '../../dtos/advertiser/objectives.dto';
import { ObjectivesModel } from '../../models/advertiser/objectives-schema';

export default class ObjectivesDAO {
  public async createMany(
    details: ObjectivesDTO[],
  ): Promise<ObjectivesDTO[]> {
    return await ObjectivesModel.insertMany(details);
  }

  public async findUnique(): Promise<ObjectivesDTO[]> {
    return await ObjectivesModel.distinct('name').exec();
  }

  public async findAll(): Promise<ObjectivesDTO[]> {
    return await ObjectivesModel.find().exec();
  }
}
