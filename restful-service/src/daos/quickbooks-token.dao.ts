import QuickbooksTokenDTO from '../dtos/quickbooks-token.dto';
import { indiaQuickbookTokenModel } from '../models/quickbooks-token-india-schema';
import { usQuickbookTokenModel } from '../models/quickbooks-token-schema';

export default class QuickbooksTokenDAO {

    public async create(dto: QuickbooksTokenDTO, zone: string): Promise<QuickbooksTokenDTO> {
        if (zone === '1') {
            const model = new usQuickbookTokenModel(dto);
            return await model.save();
        } else if (zone === '2') {
            const model = new indiaQuickbookTokenModel(dto);
            return await model.save();
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    public async update(id: any, dto: QuickbooksTokenDTO, zone: string): Promise<QuickbooksTokenDTO> {
        const model = await this.getQuickbookTokenModel(zone).findById(id).exec();
        Object.assign(model, dto);
        return await model.save();
    }

    public async find(zone: string): Promise<QuickbooksTokenDTO[]> {
        return await this.getQuickbookTokenModel(zone).find({ }).exec();
    }

    private getQuickbookTokenModel(zone: string) {
        if (zone === '1') {
            return usQuickbookTokenModel;
        } else if (zone === '2') {
            return indiaQuickbookTokenModel;
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }
}
