import InvitationCodeDTO from '../dtos/invitation-code.dto';
import { invitationCodeModel } from '../models/invitation-code-schema';

export default class InvitationCodeDAO {
    public async create(dto: InvitationCodeDTO): Promise<InvitationCodeDTO> {
        const model = new invitationCodeModel(dto);
        return await model.save();
    }

    public async update(id: any, dto: InvitationCodeDTO): Promise<InvitationCodeDTO> {
        const model = await invitationCodeModel.findById(id).exec();
        Object.assign(model, dto);
        return await model.save();
    }

    public async findByCode(code: string): Promise<InvitationCodeDTO> {
        return await invitationCodeModel.findOne({ code }).exec();
    }

    public async findByCreatedBy(email: string): Promise<InvitationCodeDTO[]> {
        return await invitationCodeModel.find({ 'created.by': email }).exec();
    }
}
