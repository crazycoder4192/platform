import NotificationDTO from '../dtos/notification.dto';
import { notificationModel } from '../models/notifications-schema';

export default class NotificationDAO {
    public async create(dto: NotificationDTO): Promise<NotificationDTO> {
        const saveNotification = new notificationModel(dto);
        return await saveNotification.save();
    }
    public async findById(id: any): Promise<NotificationDTO> {
        return await notificationModel.findOne({ _id: id }).exec();
    }
    public async update(id: any, dto: NotificationDTO): Promise<NotificationDTO> {
        const updateNotification = await notificationModel.findById(id).exec();
        Object.assign(updateNotification, dto);
        return await updateNotification.save();
    }

    public async findByEmailAndBrandAndReadStatus(email: string, brandId: string, isRead: boolean): Promise<NotificationDTO[]> {
        return await notificationModel.find({ 'email' : email, 'isRead': isRead,
                                              'typeDetails.type': 'brand', 'typeDetails.typeId': brandId, 'delete': false }).exec();
    }

    public async findByEmailAndPlatformAndReadStatus(email: string, platformId: string, isRead: boolean): Promise<NotificationDTO[]> {
        return await notificationModel.find({ 'email' : email, 'isRead': isRead,
                                              'typeDetails.type': 'platform', 'typeDetails.typeId': platformId, 'delete': false }).exec();
    }

    public async findByEmailAndGlobalAndReadStatus(email: string, isRead: boolean): Promise<NotificationDTO[]> {
        return await notificationModel.find({ 'email' : email, 'isRead': isRead,
                                              'typeDetails.type': 'general', 'delete': false }).exec();
    }
}
