import InterestedUserDTO from '../dtos/interested-user.dto';
import { interestedUserModel } from '../models/interested-user-schema';

export default class InterestedUserDAO {

    public async create(dto: InterestedUserDTO): Promise<InterestedUserDTO> {
        const model = new interestedUserModel(dto);
        return await model.save();
    }

    public async update(id: string, dto: InterestedUserDTO): Promise<InterestedUserDTO> {
        const updateModel = await interestedUserModel.findById(id).exec();
        Object.assign(updateModel, dto);
        return await updateModel.save();
    }

    public async findByEmail(email: string): Promise<InterestedUserDTO> {
        return await interestedUserModel.findOne({ email }).exec();
    }
    public async findById(id: string): Promise<InterestedUserDTO> {
        return await interestedUserModel.findById(id).exec();
    }
    public async findAll(): Promise<InterestedUserDTO[]> {
        return await interestedUserModel.find().exec();
    }
}
