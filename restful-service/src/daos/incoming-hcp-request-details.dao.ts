import IncomingHcpRequestDetailsDTO from '../dtos/incoming-hcp-request-details.dto';
import { indiaIncomingHcpRequestDetailsModel } from '../models/incoming-hcp-request-details-india.schema';
import { usIncomingHcpRequestDetailsModel } from '../models/incoming-hcp-request-details-us.schema';

export default class IncomingHcpRequestDetailsDAO {
  public async findByPlatformId(platformId: string, zone: string): Promise<IncomingHcpRequestDetailsDTO[]> {
    return await this.getIncomingHcpRequestDetailModel(zone).find({ platformId }).exec();
  }

  public async findMatchedByPlatformId(platformId: string, zone: string): Promise<IncomingHcpRequestDetailsDTO[]> {
    return await this.getIncomingHcpRequestDetailModel(zone).find({ platformId,
                                                                    isMatched: true }).exec();
  }

  public async countByPlatformId(platformId: string, zone: string): Promise<number> {
    const result = await this.getIncomingHcpRequestDetailModel(zone).aggregate([
      {
        $match: {
          platformId,
          isMatched: true
        }
      },
      {
        $group: {
          _id: '$platformId',
          npis: { $addToSet: '$matchedNpi' }
        }
      },
      {
        $unwind: '$npis'
      },
      {
        $group: { _id: '$_id', npiCount: { $sum: 1 } }
      }
    ]).exec();
    if (result && result.length > 0) {
      return result[0].npiCount;
    } else {
      return 0;
    }
  }

  public async findUniqueRecords(recordIds: string[], zone: string): Promise<string[]> {
    const results: any[] = await indiaIncomingHcpRequestDetailsModel.find({
      $or: [
        { existingRecordId: { $nin: recordIds } },
        { existingRecordId: { $eq: null } }
      ],
      platformUid: { $in: recordIds }
    }).select('platformUid').exec();
    if (results && results.length) {
      return results.map((x) => x.platformUid.toString());
    } else {
      return [];
    }
  }

  private getIncomingHcpRequestDetailModel(zone: string) {
    if (zone === '1') {
        return usIncomingHcpRequestDetailsModel;
    } else if (zone === '2') {
        return indiaIncomingHcpRequestDetailsModel;
    } else {
        throw new Error(`Invalid zone ${zone}`);
    }
  }
}
