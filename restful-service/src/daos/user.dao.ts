import mongoose from 'mongoose';
import UserDTO from '../dtos/user.dto';
import { advertiserBrandModel } from '../models/advertiser/advertiser-brand-schema';
import { advertiserModel } from '../models/advertiser/advertiser-schema';
import { advertiserUserModel } from '../models/advertiser/advertiser-user-schema';
import { auditPublishedSubCampaignModel } from '../models/advertiser/audit-published-sub-campaign-schema';
import { campaignModel } from '../models/advertiser/campaign-schema';
import { creativeHubModel } from '../models/advertiser/creative-hub-schema';
import { indiaPaymentMethodModel } from '../models/advertiser/payment-method-india-schema';
import { usPaymentMethodModel } from '../models/advertiser/payment-method-us-schema';
import { subCampaignModel } from '../models/advertiser/sub-campaign-schema';
import { notificationModel } from '../models/notifications-schema';
import { passwordHistoryModel } from '../models/password-history-schema';
import { publisherAssetModel } from '../models/publisher/asset-schema';
import { auditVerifiedAssetModel } from '../models/publisher/audit-verified-asset-schema';
import { indiaPublisherBankDetailModel } from '../models/publisher/bank-details-india-schema';
import { usPublisherBankDetailModel } from '../models/publisher/bank-details-us-schema';
import { publisherPlatformModel } from '../models/publisher/platform-schema';
import { userModel } from '../models/user-schema';
import { userTokenModel } from '../models/user-token-schema';

export default class UserDAO {
  public async create(user: UserDTO): Promise<UserDTO> {
    const saveUser = new userModel(user);
    return await saveUser.save();
  }

  public async update(id: string, user: UserDTO): Promise<UserDTO> {
    const updateUser = await userModel.findById(id).exec();
    Object.assign(updateUser, user);
    return await updateUser.save();
  }
  public async updatefirstlastname(argEmail: string, user: any): Promise<UserDTO> {
    return await userModel.findOneAndUpdate({ email: argEmail }, user).exec();
  }
  public async findByEmail(argEmail: string): Promise<UserDTO> {
    return await userModel.findOne({ email: argEmail }).exec();
  }
  public async updateIsVisitedDashboard(argEmail: string, user: any): Promise<UserDTO> {
    return await userModel.findOneAndUpdate({ email: argEmail }, user).exec();
  }

  public async findByEmailAndStatus(
    argEmail: string,
    argStatus: string,
  ): Promise<UserDTO> {
    return await userModel
      .findOne({ email: argEmail, userStatus: argStatus })
      .exec();
  }

  public async updateFailureAttempt(id: string) {
    return await userModel
      .findOneAndUpdate(
        { _id: id },
        {
          $inc: { loginFailureAttempts: 1 },
          $set: { lastFailureTime: Date.now() },
        },
      )
      .exec();
  }

  public async lock(id: string) {
    return await userModel
      .findOneAndUpdate(
        { _id: id },
        { $set: { isLocked: 1, lastFailureTime: Date.now() } },
      )
      .exec();
  }

  public async getAllAdminUsers(argEmail: string): Promise<UserDTO[]> {
    return userModel.aggregate([
      { $match: {
          userType: 'admin',
          email: { $ne: argEmail }
        }
      }
    ]);
  }

  public async getUserByEmail(argEmail: string): Promise<UserDTO> {
    return userModel.aggregate([{ $match: { email: argEmail } }]).exec();
  }

  public async deleteUserByEmail(argEmail: string): Promise<any> {
    const getUser: any = await this.getUserByEmail(argEmail);
    if (getUser[0].userType === 'advertiser') {
      await auditPublishedSubCampaignModel.remove({ 'created.by': argEmail }).exec();
      await advertiserBrandModel.remove({ 'created.by': argEmail }).exec();
      await campaignModel.remove({ 'created.by': argEmail }).exec();
      await usPaymentMethodModel.remove({ 'created.by': argEmail }).exec();
      await indiaPaymentMethodModel.remove({ 'created.by': argEmail }).exec();
      await subCampaignModel.remove({ 'created.by': argEmail }).exec();
      await advertiserModel.remove({ 'created.by': argEmail }).exec();
      await advertiserUserModel.remove({ 'created.by': argEmail }).exec();
      await creativeHubModel.remove({ 'created.by': argEmail }).exec();
      await notificationModel.remove({ email: argEmail }).exec();
    } else if (getUser[0].userType === 'publisher') {
      await publisherPlatformModel.remove({ 'created.by': argEmail }).exec();
      await usPublisherBankDetailModel.remove({ 'created.by': argEmail }).exec();
      await indiaPublisherBankDetailModel.remove({ 'created.by': argEmail }).exec();
      await auditVerifiedAssetModel.remove({ 'created.by': argEmail }).exec();
      await publisherAssetModel.remove({ 'created.by': argEmail }).exec();
    }
    await passwordHistoryModel.remove({ email: argEmail }).exec();
    await userTokenModel.remove({ email: argEmail }).exec();
    return await userModel.remove({ email: argEmail }).exec();
  }

  public async getById(id: string): Promise<UserDTO> {
    return await userModel.findById(id).exec();
  }

  public async findVerificationPendingUsersForEmail(): Promise<UserDTO[]> {
    const setOne: UserDTO[] = await userModel.find({
      'userStatus': { $ne: 'active' },
      'reminder.verificationPending.noOfMailsSent': { $exists: true, $lt: 4 }
    }).exec();

    const setTwo: UserDTO[] = await userModel.find({
      'userStatus': { $ne: 'active' },
      'remider.verificationPending.noOfMailsSent': { $exists: false }
    }).exec();

    const finalSet: UserDTO[] = [];
    for (const u of setOne) {
      if (!finalSet.find((z) => z.email === u.email)) {
        finalSet.push(u);
      }
    }
    for (const u of setTwo) {
      if (!finalSet.find((y) => y.email === u.email)) {
        finalSet.push(u);
      }
    }
    return finalSet;
  }

  public async findUsersNearingPasswordExpiry(): Promise<UserDTO[]> {
    const expiryDate: Date = new Date();
    expiryDate.setDate(expiryDate.getDate() - 3);
    return await userModel.find({
      userStatus: 'active',
      passwordExpiryDate: { $lte: expiryDate }
    }).exec();
  }

  public async findActiveUserTypes(userType: string): Promise<any[]> {

    const setOne: UserDTO[] = await userModel.find({
      'userStatus': { $eq: 'active' },
      'hasPersonalProfile': false,
      'userType': userType,
      'reminder.profileCompletionPending.noOfMailsSent': { $exists: true, $lt: 4 }
    }).exec();

    const setTwo: UserDTO[] = await userModel.find({
      'userStatus': { $eq: 'active' },
      'hasPersonalProfile': false,
      'userType': userType,
      'remider.profileCompletionPending.noOfMailsSent': { $exists: false }
    }).exec();

    const finalSet: UserDTO[] = [];
    for (const u of setOne) {
      if (!finalSet.find((z) => z.email === u.email)) {
        finalSet.push(u);
      }
    }
    for (const u of setTwo) {
      if (!finalSet.find((y) => y.email === u.email)) {
        finalSet.push(u);
      }
    }
    return finalSet;
  }

  public async isUserSubscribedForEmail(argEmail: string): Promise<boolean> {
    const user = await userModel.findOne({ email: argEmail }).exec();
    return user.isSubscribedForEmail;
  }

  public async findActiveUserByEmail(email: string): Promise<UserDTO> {
    return await userModel.findOne({ email, userStatus: 'active' }).exec();
  }
}
