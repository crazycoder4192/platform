import { stream } from 'winston';
import { MailHelper } from './mail-helper';

export class PublisherMail {

    private readonly mailHelper: MailHelper;

    constructor() {
        this.mailHelper = new MailHelper();
    }

    public async sendSetupCompleteButNotAddedPlatformMail(to: string, username: string, userCreateDate: string,
                                                          unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'docereeAccountActiveDateLink', value: userCreateDate },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "What's stopping you from adding your platform to Doceree?",
            'setup-complete-not-added-platform-mail-template.html',
            variablesAndValues
        );
    }

    public async sendFirstPlatformAddedMail(to: string, username: string, platformName: string,
                                            dashboardLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'platformNameLink', value: platformName },
            { key: 'redirectLink', value: dashboardLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Congratulations on adding your first platform to Doceree!',
            'first-platform-added-mail-template.html',
            variablesAndValues
        );
    }

    public async sendPlatformAddedMail(to: string, username: string, platformName: string,
                                       dashboardLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'platformNameLink', value: platformName },
            { key: 'platformNameSecondLink', value: platformName },
            { key: 'redirectLink', value: dashboardLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "You've successfully added a platform to Doceree",
            'platform-added-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED - Platform is added while Profile creation. So we will be sending the email reminder
    // for profile not complete
    public async sendNoPlatformReminderMail(to: string, buttonToAddPlatformLink: string, addedPublisherOnDate: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'buttonToAddAssetLink', value: buttonToAddPlatformLink },
            { key: 'addedPlatformOnDate', value: addedPublisherOnDate }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Profile Added But Not Added Platfrom',
            'profile-added-but-not-added-platform-mail-template.html',
            variablesAndValues
        );
    }

    public async sendNoAssetReminderMail(to: string, username: string, platformNameLink: string, addedPlatformOnDate: string,
                                         addAssetLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'platformNameLink', value: platformNameLink },
            { key: 'addedPlatformOnDate', value: addedPlatformOnDate },
            { key: 'buttonToAddAssetLink', value: addAssetLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "Your platform can't make money on Doceree until you add an inventory",
            'platform-added-but-not-added-assets-mail-template.html',
            variablesAndValues
        );
    }

    public async sendAddedJustOneAssetsAddMoreAssetsMail(to: string, username: string, buttonToAddAssetLink: string,
                                                         unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'buttonToAddAssetLink', value: buttonToAddAssetLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'On Doceree, more slots = more revenues',
            'added-just-one-asset-add-more-assets-mail-template.html',
            variablesAndValues
        );
    }

    public async sendPlatformRemovedMail(to: string, username: string, platformName: string,
                                         removePlatformLink: string, platformRestoreLink: string,
                                         unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'platformNameLink', value: platformName },
            { key: 'removePlatformLink', value: removePlatformLink },
            { key: 'platformRestoreLink', value: platformRestoreLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Are you sure you want to remove your platform from Doceree?',
            'platform-removed-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendPlatformDeactivateMail(to: string, username: string, activatePlatformLink: string,
                                            unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'restorePlatform', value: activatePlatformLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'We see you’ve hit pause… But why?',
            'platform-deactivated-mail-template.html',
            variablesAndValues
        );
    }

    public async sendAssetsRemovedMail(to: string, username: string, assetRemovedNameLink: string, buttonToRemoveAssetLink: string,
                                       assetsRestoreLink: string, FeedbackFormLink: string,
                                       unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'assetRemovedNameLink', value: assetRemovedNameLink },
            { key: 'buttonToRemoveAssetLink', value: buttonToRemoveAssetLink },
            { key: 'assetsRestoreLink', value: assetsRestoreLink },
            { key: 'FeedbackFormLink', value: FeedbackFormLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Are you sure you want to remove asset from Doceree?',
            'asset-removed-mail-template.html',
            variablesAndValues
        );
    }

    public async sendAssetsDeactivateMail(to: string, username: string, assetName: string, FeedbackFormLink: string,
                                          assetsRestoreLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'assetNameLink', value: assetName },
            { key: 'FeedbackFormLink', value: FeedbackFormLink },
            { key: 'assetsRestoreLink', value: assetsRestoreLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'We see you’ve hit pause… But why?',
            'asset-deactivate-mail-template.html',
            variablesAndValues
        );
    }

    public async sendMonthlyPayment(to: string, username: string, billMonth: string,
                                    attachment: any, paymentPageLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'paymentOfMonthLink', value: billMonth },
            { key: 'paymentPageLink', value: paymentPageLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Here is your money',
            'monthly-payment-mail-template.html',
            variablesAndValues,
            attachment
        );
    }

    public async sendMonthlyAch(to: string, billMonth: string, attachment: any): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: 'Doceree Admin' },
            { key: 'paymentOfMonthLink', value: billMonth }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Monthly ACH',
            'monthly-payment-mail-template.html',
            variablesAndValues,
            attachment
        );
    }

    public async sendProblemWithPaymentMail(to: string, username: string, paymentMethodName: string,
                                            accountNumber: string, platformName: string, buttonToPayment: string,
                                            unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'paymentMethodName', value: paymentMethodName },
            { key: 'accountNumber', value: accountNumber },
            { key: 'platformName', value: platformName },
            { key: 'buttonToPayment', value: buttonToPayment },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "Important: We're unable to credit funds to your account",
            'problem-with-payment-publisher-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendMonthlyReportOfPlatformsMail(to: string, username: string, platformSummariesLink: string,
                                                  unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'platformSummariesLink', value: platformSummariesLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "Here's how your platform(s) performed on Doceree this month",
            'monthly-report-platform-mail-template.html',
            variablesAndValues
        );
    }

    public async sendGrantedUserAccessExistingUserMail(to: string, def: string, publisherPlatformName: string,
                                                       publisherUserRoleName: string, loginPageLink: string): Promise<any> {
            const variablesAndValues: any[] = [
                { key: 'def', value: def },
                { key: 'publisherPlatformName', value: publisherPlatformName },
                { key: 'publisherUserRoleName', value: publisherUserRoleName },
                { key: 'loginPageLink', value: loginPageLink }
            ];

            return await this.mailHelper.readTemplate(
                to,
                `Congratulations! You've been added to ${publisherPlatformName} team on Doceree`,
                'granted-user-access-existing-user-publisher-mail-template.html',
                variablesAndValues
            );
    }
    public async sendGrantedNewUserAnalyticsAccessMail(to: string, def: string, publisherPlatformName: string,
                                                       registerPageLink: string): Promise<any> {
            const variablesAndValues: any[] = [
                { key: 'def', value: def },
                { key: 'publisherPlatformName', value: publisherPlatformName },
                { key: 'emailIdWhichInvitationWasSent', value: to },
                { key: 'registerPageLink', value: registerPageLink }
            ];

            return await this.mailHelper.readTemplate(
                to,
                `${def} has invited you to join ${publisherPlatformName} team on Doceree`,
                'granted-new-user-analyst-publisher-mail-template.html',
                variablesAndValues
            );
    }
    public async sendGrantedNewUserAdminAccessMail(to: string, def: string, publisherPlatformName: string,
                                                   registerPageLink: string): Promise<any> {
            const variablesAndValues: any[] = [
                { key: 'def', value: def },
                { key: 'publisherPlatformName', value: publisherPlatformName },
                { key: 'emailIdWhichInvitationWasSent', value: to },
                { key: 'registerPageLink', value: registerPageLink }
            ];

            return await this.mailHelper.readTemplate(
                to,
                `${def} has invited you to join ${publisherPlatformName} team on Doceree`,
                'granted-new-user-publisher-mail-template.html',
                variablesAndValues
            );
    }
    public async sendGrantedNewUserDeveloperAccessMail(to: string, def: string, publisherPlatformName: string,
                                                       registerPageLink: string) {
            const variablesAndValues: any[] = [
                { key: 'def', value: def },
                { key: 'publisherPlatformName', value: publisherPlatformName },
                { key: 'emailIdWhichInvitationWasSent', value: to },
                { key: 'registerPageLink', value: registerPageLink },
            ];

            return await this.mailHelper.readTemplate(
                to,
                `${def} has invited you to join ${publisherPlatformName} team on Doceree`,
                'granted-new-user-developer-publisher-mail-template.html',
                variablesAndValues
            );
    }
    public async sendInvitationAndHeaderCode(to: string, senderName: string, platformName: string,
                                             registerPageLink: string, code: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'registerPageLink', value: registerPageLink },
            { key: 'code', value: code },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has invited you to join ${platformName} team on Doceree`,
            'platform-integration-new-developer-mail-template.html',
            variablesAndValues
        );
    }
    public async sendHeaderCode(to: string, senderName: string, platformName: string,
                                loginPageLink: string, code: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'loginPageLink', value: loginPageLink },
            { key: 'code', value: code },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has shared the developer code for ${platformName} integration`,
            'platform-integration-existing-developer-mail-template.html',
            variablesAndValues
        );
    }
    public async sendInvitationAndInvocationCode(to: string, senderName: string, platformName: string,
                                                 registerPageLink: string, codeDocereeLogin: string, codeDocereeLogout: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'registerPageLink', value: registerPageLink },
            { key: 'codeDocereeLogin', value: codeDocereeLogin },
            { key: 'codeDocereeLogout', value: codeDocereeLogout },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has invited you to join ${platformName} team on Doceree`,
            'audience-indentification-new-developer-mail-template.html',
            variablesAndValues
        );
    }
    public async sendInvocationCode(to: string, senderName: string, platformName: string,
                                    loginPageLink: string, codeDocereeLogin: string, codeDocereeLogout: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'loginPageLink', value: loginPageLink },
            { key: 'codeDocereeLogin', value: codeDocereeLogin },
            { key: 'codeDocereeLogout', value: codeDocereeLogout },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has shared the developer code for ${platformName} integration`,
            'audience-indentification-existing-developer-mail-template.html',
            variablesAndValues
        );
    }
    public async sendInvitationAndAdIntegrationCode(to: string, senderName: string, platformName: string,
                                                    registerPageLink: string, code: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'registerPageLink', value: registerPageLink },
            { key: 'code', value: code },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has invited you to join ${platformName} team on Doceree`,
            'ad-slot-integration-new-developer-mail-template.html',
            variablesAndValues
        );
    }
    public async sendAdIntegrationCode(to: string, senderName: string, platformName: string,
                                       loginPageLink: string, code: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'loginPageLink', value: loginPageLink },
            { key: 'code', value: code },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has shared the developer code for ${platformName} integration`,
            'ad-slot-integration-existing-developer-mail-template.html',
            variablesAndValues
        );
    }

    public async sendInvitationAndMobilePISnippet(to: string, firstName: string, platformName: string,
                                                  registerPageLink: string, snippet0: string, snippet1: string,
                                                  snippet2: string, snippet3: string): Promise<boolean> {
            const variablesAndValues: any[] = [
                { key: 'senderName', value: firstName },
                { key: 'platformName', value: platformName },
                { key: 'registerPageLink', value: registerPageLink },
                { key: 'code0', value: snippet0 },
                { key: 'code1', value: snippet1 },
                { key: 'code2', value: snippet2 },
                { key: 'code3', value: snippet3 }
            ];
            return await this.mailHelper.readTemplate(
                to,
                `${firstName} has shared the developer code for ${platformName} integration`,
                'platform-integration-android-new-developer-mail-template.html',
                variablesAndValues
            );
    }

    public async sendMobilePISnippet(to: any, firstName: string, platformName: string, redirectUrl: string,
                                     snippet0: string, snippet1: string, snippet2: string, snippet3: string): Promise<boolean> {

            const variablesAndValues: any[] = [
                { key: 'senderName', value: firstName },
                { key: 'platformName', value: platformName },
                { key: 'loginPageLink', value: redirectUrl },
                { key: 'code0', value: snippet0 },
                { key: 'code1', value: snippet1 },
                { key: 'code2', value: snippet2 },
                { key: 'code3', value: snippet3 }
            ];

            return await this.mailHelper.readTemplate(
                to,
                `${firstName} has shared the developer code for ${platformName} integration`,
                'platform-integration-android-existing-developer-mail-template.html',
                variablesAndValues
            );
    }

    public async sendInvitationAndInvocationCodeForMobile(to: string, senderName: string, platformName: string,
                                                          registerPageLink: string, codeDocereeLogin: string,
                                                          codeDocereeLogout: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'registerPageLink', value: registerPageLink },
            { key: 'codeDocereeLogin', value: codeDocereeLogin },
            { key: 'codeDocereeLogout', value: codeDocereeLogout },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has invited you to join ${platformName} team on Doceree`,
            'audience-indentification-android-new-developer-mail-template.html',
            variablesAndValues
        );
    }

    public async sendInvocationCodeForMobile(to: string, senderName: string, platformName: string,
                                             loginPageLink: string, codeDocereeLogin: string, codeDocereeLogout: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'senderName', value: senderName },
            { key: 'platformName', value: platformName },
            { key: 'loginPageLink', value: loginPageLink },
            { key: 'codeDocereeLogin', value: codeDocereeLogin },
            { key: 'codeDocereeLogout', value: codeDocereeLogout },
            { key: 'platformNameSecond', value: platformName }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${senderName} has shared the developer code for ${platformName} integration`,
            'audience-indentification-android-existing-developer-mail-template.html',
            variablesAndValues
        );
    }

    public async sendInvitationAndMobileAdIntegrationCode(to: string, senderName: string,
                                                          platformName: string, registerPageLink: string,
                                                          snippet1: string, snippet2: string, snippet3: string): Promise<boolean> {
            const variablesAndValues: any[] = [
                { key: 'senderName', value: senderName },
                { key: 'platformName', value: platformName },
                { key: 'registerPageLink', value: registerPageLink },
                { key: 'code1', value: snippet1 },
                { key: 'code2', value: snippet2 },
                { key: 'code3', value: snippet3 }
            ];

            return await this.mailHelper.readTemplate(
                to,
                `${senderName} has shared the developer code for ${platformName} integration`,
                'ad-slot-integration-android-new-developer-mail-template.html',
                variablesAndValues
            );
        }

    public async sendMobileAdIntegrationCode(to: string, senderName: string,
                                             platformName: string, redirectUrl: string,
                                             snippet1: string, snippet2: string, snippet3: string): Promise<boolean> {
            const variablesAndValues: any[] = [
                { key: 'senderName', value: senderName },
                { key: 'platformName', value: platformName },
                { key: 'loginPageLink', value: redirectUrl },
                { key: 'code1', value: snippet1 },
                { key: 'code2', value: snippet2 },
                { key: 'code3', value: snippet3 }
            ];

            return await this.mailHelper.readTemplate(
                to,
                `${senderName} has shared the developer code for ${platformName} integration`,
                'ad-slot-integration-android-existing-developer-mail-template.html',
                variablesAndValues
            );
        }

}
