import config from 'config';
import { MailHelper } from './mail-helper';

export class AdminMail {

    private readonly mailHelper: MailHelper;

    constructor() {
        this.mailHelper = new MailHelper();
    }

    public async sendNewCampaignReceviedForApprovalMail(to: string, newCampaignApprovalTime: string,
                                                        campaignNameLink: string, countryNameLink: string, newCampaignReceivedBrandNameLink: string,
                                                        reviewCampaignPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'campaignNameLink', value: campaignNameLink },
            { key: 'newCampaignReceivedBrandNameLink', value: newCampaignReceivedBrandNameLink },
            { key: 'reviewCampaignPageLink', value: reviewCampaignPageLink },
            { key: 'countryNameLink', value: countryNameLink },
            { key: 'newCampaignApprovalTime', value: newCampaignApprovalTime }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'New Campaign Received For Approval',
            'new-campaign-received-for-approval-mail-template.html',
            variablesAndValues
        );
    }

    public async sendNewCreditLineApprovalRequestReceivedMail(to: string, accountNameLink: string, countryNameLink: string,
                                                              creditLineApprovalPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'accountNameLink', value: accountNameLink },
            { key: 'creditLineApprovalPageLink', value: creditLineApprovalPageLink },
            { key: 'countryNameLink', value: countryNameLink },
        ];

        return await this.mailHelper.readTemplate(
            to,
            'New Credit Line Approval Request Received',
            'new-credit-line-approval-request-received-mail-template.html',
            variablesAndValues
        );
    }

    public async sendGrantedUserAccessForUserWhoHakksAnExistingAccountOrNewUserMail(to: string, adminLoginPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'adminLoginPageLink', value: adminLoginPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Granted User Access',
            'user-who-has-an-existing-account -or-new-user-mail-template.html',
            variablesAndValues
        );
    }

    public async sendNewTicketGeneratedMail(to: string, ticketGeneratedTimeLink: string, accountNameOfAdvertiserOrPublisherLink: string,
                                            ticketNumberOfAdvertiserOrPublisherLink: string, identifyAdvertiserOrPublisherLink: string,
                                            ticketPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'identifyAdvertiserOrPublisherLink', value: identifyAdvertiserOrPublisherLink },
            { key: 'accountNameOfAdvertiserOrPublisherLink', value: accountNameOfAdvertiserOrPublisherLink },
            { key: 'ticketNumberOfAdvertiserOrPublisherLink', value: ticketNumberOfAdvertiserOrPublisherLink },
            { key: 'ticketGeneratedTimeLink', value: ticketGeneratedTimeLink },
            { key: 'ticketPageLink', value: ticketPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'New Ticket Generated',
            'new-ticket-generated-mail-template.html',
            variablesAndValues
        );
    }
}
