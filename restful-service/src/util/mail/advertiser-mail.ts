import { validate } from 'node-cron';
import { MailHelper } from './mail-helper';

export class AdvertiserMail {

    private readonly mailHelper: MailHelper;

    constructor() {
        this.mailHelper = new MailHelper();
    }

    public async sendNoCampaignReminderMail(to: string, username: string, date: string,
                                            unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'date', value: date },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "What's stopping you from running your first campaign?",
            'setup-complete-not-run-campaign-mail-template.html',
            variablesAndValues
        );
    }

    public async sendFirstRunCampaignMail(to: string, username: string, campaignName: string,
                                          unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'campaignNameLink', value: campaignName },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Congratulations on your first campaign!',
            'first-campaign-run-mail-template.html',
            variablesAndValues
        );
    }

    public async sendCampaignAddedMail(to: string, username: string, campaignName: string,
                                       unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'campaignNameLink', value: campaignName },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Campaign creation successful',
            'campaign-added-mail-template.html',
            variablesAndValues
        );
    }

    public async sendCampaignApprovedMail(to: string, username: string, campaignName: string, dashboardLink: string,
                                          unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'campaignNameLink', value: campaignName },
            { key: 'buttonToDashboardLink', value: dashboardLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "Your campaign's been approved!",
            'campaign-approved-mail-template.html',
            variablesAndValues
        );
    }

    public async sendCampaignNotApprovedMail(to: string, username: string, campaignName: string, draftCampaignLink: string,
                                             unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'campaignNameLink', value: campaignName },
            { key: 'buttonToEditCampaignLink', value: draftCampaignLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Your campaign needs some corrections',
            'campaign-not-approved-mail-template.html',
            variablesAndValues
        );
    }

    public async sendCampaignStoppedMail(to: string, username: string, dashboardLink: string,
                                         unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'redirectLink', value: dashboardLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Your campaign has been stopped',
            'campaign-stopped-mail-template.html',
            variablesAndValues
        );
    }

    public async sendCampaignCompleteMail(to: string, username: string, campaignName: string,
                                          date: Date, analyticsLink: string,
                                          newCampaignLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'campaignNameLink', value: campaignName },
            { key: 'date', value: date },
            { key: 'redirectLink', value: analyticsLink },
            { key: 'redirectingLink', value: newCampaignLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Your campaign was completed successfully',
            'campaign-complete-mail-template.html',
            variablesAndValues
        );
    }

    public async sendInvoiceMail(to: string, username: string, invoiceNumber: string,
                                 receiptMonth: string, receiptYear: string, receiptDateAndMonth: string,
                                 invoiceDate: string, paymentPageLink: string, attachment: any): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'invoiceNumberLink', value: invoiceNumber },
            { key: 'receiptMonthLink', value: receiptDateAndMonth },
            { key: 'date', value: invoiceDate },
            { key: 'paymentPageLink', value: paymentPageLink }
        ];

        // TODO: need to add attachment
        return await this.mailHelper.readTemplate(
            to,
            `Your invoice for ${receiptMonth}, ${receiptYear}`,
            'monthly-invoice-mail-template.html',
            variablesAndValues,
            attachment
        );
    }

    public async sendNewPaymentMethodAddedMail(to: string, username: string, goToPaymentsLink: string,
                                               unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'goToPaymentsLink', value: goToPaymentsLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'New payment method added to your Doceree account',
            'new-payment-method-added-mail-template.html',
            variablesAndValues
        );
    }

    public async sendApplicationForCreditLineMail(to: string, username: string,
                                                  redirectingLink: string, redirectLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'redirectingLink', value: redirectingLink },
            { key: 'redirectLink', value: redirectLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "We've received your application for a credit line",
            'application-for-creditline-mail-template.html',
            variablesAndValues
        );
    }

    public async sendConfirmationOfCreditLineStatusApprovedMail(to: string, username: string, date: string,
                                                                unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'date', value: date },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Congratulations! Your credit line application is approved',
            'confirmation-of-creditline-status-mail-template.html',
            variablesAndValues
        );
    }

    public async sendConfirmationOfCreditLineStatusRejectedMail(to: string, username: string, date: string,
                                                                unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'date', value: date },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Sorry, your credit line application is denied',
            'confirmation-of-creditline-status-rejected-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendConfirmationOfCreditLineStatusReviewMail(to: string, username: string, date: string,
                                                              redirectLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'date', value: date },
            { key: 'redirectLink', value: redirectLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Your credit line application needs additional documents',
            'confirmation-of-creditline-status-under-review-mail-template.html',
            variablesAndValues
        );
    }

    public async sendProblemWithPaymentMail(to: string, username: string, cardNumber: string,
                                            redirectLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'paymentMethodName', value: cardNumber },
            { key: 'redirectLink', value: redirectLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Important: Your payment method needs attention',
            'problem-with-payment-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendMonthlyReportCampaignMail(to: string, username: string, campaignSummariesLink: string,
                                               unsubscribeLink: string, campaignMonthLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'campaignSummariesLink', value: campaignSummariesLink },
            { key: 'unsubscribeLink', value: unsubscribeLink },
            { key: 'campaignMonthLink', value: campaignMonthLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `Here's what you achieved in ${campaignMonthLink}`,
            'monthly-report-campaign-mail-template.html',
            variablesAndValues
        );
    }

    public async sendGrantedUserAccessExistingUserMail(to: string, def: string, advertiserBrandName: string,
                                                       advertiserUserRoleName: string, loginPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'def', value: def },
            { key: 'advertiserBrandName', value: advertiserBrandName },
            { key: 'advertiserUserRoleName', value: advertiserUserRoleName },
            { key: 'loginPageLink', value: loginPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `Congratulations! You've been added to ${advertiserBrandName} team on Doceree`,
            'granted-user-access-existing-user-mail-template.html',
            variablesAndValues
        );
    }

    public async sendGrantedNewUserAdminAccessMail(to: string, def: string, newUserBrandName: string,
                                                   emailIdWhichInvitationWasSent: string, registerPageLink: string
                                              ): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'def', value: def },
            { key: 'newUserBrandNameLink', value: newUserBrandName },
            { key: 'emailIdWhichInvitationWasSent', value: emailIdWhichInvitationWasSent },
            { key: 'newUserBrandNameSecondLink', value: newUserBrandName },
            { key: 'registerPageLink', value: registerPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${def} has invited you to join ${newUserBrandName} team on Doceree`,
            'granted-new-user-mail-template.html',
            variablesAndValues
        );
   }

    public async sendGrantedNewUserAnalyticsAccessMail(to: string, def: string, newUserBrandName: string,
                                                       emailIdWhichInvitationWasSent: string,
                                                       registerPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'def', value: def },
            { key: 'newUserBrandNameLink', value: newUserBrandName },
            { key: 'emailIdWhichInvitationWasSent', value: emailIdWhichInvitationWasSent },
            { key: 'newUserBrandNameSecondLink', value: newUserBrandName },
            { key: 'registerPageLink', value: registerPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${def} has invited you to join ${newUserBrandName} team on Doceree`,
            'granted-new-user-analytics-mail-template.html',
            variablesAndValues
        );
    }

    public async sendGrantedNewUserEditorAccessMail(to: string, def: string, newUserBrandName: string,
                                                    emailIdWhichInvitationWasSent: string,
                                                    registerPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'def', value: def },
            { key: 'newUserBrandNameLink', value: newUserBrandName },
            { key: 'emailIdWhichInvitationWasSent', value: emailIdWhichInvitationWasSent },
            { key: 'newUserBrandNameSecondLink', value: newUserBrandName },
            { key: 'registerPageLink', value: registerPageLink },
        ];

        return await this.mailHelper.readTemplate(
            to,
            `${def} has invited you to join ${newUserBrandName} team on Doceree`,
            'granted-new-user-editor-mail-template.html',
            variablesAndValues
        );
    }

}
