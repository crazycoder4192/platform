const readline = require('readline');
const fs = require('fs');
import config from 'config';
import path from 'path';
import { Mail } from '../mail';
import { logger } from '../winston';

export class MailHelper {
    private readonly mail: Mail;

    constructor() {
        this.mail = new Mail();
    }

    public async readTemplate(
        to: string,
        subject: string,
        templateName: string,
        variableAndValues: any[],
        attachment?: any): Promise<boolean> {

        const s3Location: string = config.get<string>('mail.image-s3-location');
        // populate s3 variables here
        variableAndValues.push({ key: 'doceree-01.jpg', value: `${s3Location}/doceree-01.jpg` });
        variableAndValues.push({ key: 'doceree-04.jpg', value: `${s3Location}/doceree-04.jpg` });
        variableAndValues.push({ key: 'doceree-09.jpg', value: `${s3Location}/doceree-09.jpg` });
        variableAndValues.push({ key: 'doceree-10.jpg', value: `${s3Location}/doceree-10.jpg` });
        variableAndValues.push({ key: 'doceree-11.jpg', value: `${s3Location}/doceree-11.jpg` });
        variableAndValues.push({ key: 'doceree-12.jpg', value: `${s3Location}/doceree-12.jpg` });
        variableAndValues.push({ key: 'doceree-13.jpg', value: `${s3Location}/doceree-13.jpg` });
        variableAndValues.push({ key: 'doceree-14.jpg', value: `${s3Location}/doceree-14.jpg` });
        variableAndValues.push({ key: 'doceree-17.jpg', value: `${s3Location}/doceree-17.jpg` });
        variableAndValues.push({ key: 'doceree-19.jpg', value: `${s3Location}/doceree-19.jpg` });
        variableAndValues.push({ key: 'doceree-20.jpg', value: `${s3Location}/doceree-20.jpg` });
        variableAndValues.push({ key: 'doceree-21.jpg', value: `${s3Location}/doceree-21.jpg` });
        variableAndValues.push({ key: 'doceree-22.jpg', value: `${s3Location}/doceree-22.jpg` });
        variableAndValues.push({ key: 'doceree-23.jpg', value: `${s3Location}/doceree-23.jpg` });
        variableAndValues.push({ key: 'doceree-24.jpg', value: `${s3Location}/doceree-24.jpg` });
        variableAndValues.push({ key: 'doceree-25.jpg', value: `${s3Location}/doceree-25.jpg` });
        variableAndValues.push({ key: 'doceree-27.jpg', value: `${s3Location}/doceree-27.jpg` });
        variableAndValues.push({ key: 'doceree-28.jpg', value: `${s3Location}/doceree-28.jpg` });
        variableAndValues.push({ key: 'doceree-29.jpg', value: `${s3Location}/doceree-29.jpg` });
        variableAndValues.push({ key: 'doceree-30.jpg', value: `${s3Location}/doceree-30.jpg` });
        variableAndValues.push({ key: 'doceree-31.jpg', value: `${s3Location}/doceree-31.jpg` });
        variableAndValues.push({ key: 'doceree-32.jpg', value: `${s3Location}/doceree-32.jpg` });
        variableAndValues.push({ key: 'doceree-33.jpg', value: `${s3Location}/doceree-33.jpg` });
        variableAndValues.push({ key: 'doceree-34.jpg', value: `${s3Location}/doceree-34.jpg` });
        variableAndValues.push({ key: 'doceree-35.jpg', value: `${s3Location}/doceree-35.jpg` });
        variableAndValues.push({ key: 'doceree-36.jpg', value: `${s3Location}/doceree-36.jpg` });
        variableAndValues.push({ key: 'button-ad-01.jpg', value: `${s3Location}/button-ad-01.jpg` });
        variableAndValues.push({ key: 'button-ad-02.jpg', value: `${s3Location}/button-ad-02.jpg` });
        variableAndValues.push({ key: 'button-ad-03.png', value: `${s3Location}/button-ad-03.png` });
        variableAndValues.push({ key: 'button-ad-04.png', value: `${s3Location}/button-ad-04.png` });
        variableAndValues.push({ key: 'button-ad-05.png', value: `${s3Location}/button-ad-05.png` });
        variableAndValues.push({ key: 'button-ad-06.png', value: `${s3Location}/button-ad-06.png` });
        variableAndValues.push({ key: 'button-ad-07.png', value: `${s3Location}/button-ad-07.png` });
        variableAndValues.push({ key: 'button-ad-08.png', value: `${s3Location}/button-ad-08.png` });
        variableAndValues.push({ key: 'button-ad-09.png', value: `${s3Location}/button-ad-09.png` });
        variableAndValues.push({ key: 'button-ad-10.png', value: `${s3Location}/button-ad-10.png` });
        variableAndValues.push({ key: 'button-ad-11.png', value: `${s3Location}/button-ad-11.png` });
        variableAndValues.push({ key: 'button-ad-12.png', value: `${s3Location}/button-ad-12.png` });
        variableAndValues.push({ key: 'button-ad-13.png', value: `${s3Location}/button-ad-13.png` });
        variableAndValues.push({ key: 'button-ad-14.png', value: `${s3Location}/button-ad-14.png` });
        variableAndValues.push({ key: 'button-ad-15.png', value: `${s3Location}/button-ad-15.png` });
        variableAndValues.push({ key: 'account-ad.png', value: `${s3Location}/account-ad.png` });
        variableAndValues.push({ key: 'summaries-ad.png', value: `${s3Location}/summaries-ad.png` });
        variableAndValues.push({ key: 'create-account-ad.png', value: `${s3Location}/create-account-ad.png` });
        variableAndValues.push({ key: 'credit-line-ad.png', value: `${s3Location}/credit-line-ad.png` });
        variableAndValues.push({ key: 'submit-ad.png', value: `${s3Location}/submit-ad.png` });
        variableAndValues.push({ key: 'payments-ad.png', value: `${s3Location}/payments-ad.png` });
        variableAndValues.push({ key: 'setup-ad.png', value: `${s3Location}/setup-ad.png` });
        variableAndValues.push({ key: 'review-ad.png', value: `${s3Location}/review-ad.png` });
        variableAndValues.push({ key: 'add-campaign-ad.png', value: `${s3Location}/add-campaign-ad.png` });
        variableAndValues.push({ key: 'delete-ad.png', value: `${s3Location}/delete-ad.png` });
        variableAndValues.push({ key: 'schedule-ad.png', value: `${s3Location}/schedule-ad.png` });
        variableAndValues.push({ key: 'application-credit-line-ad.jpg', value: `${s3Location}/application-credit-line-ad.jpg` });
        variableAndValues.push({ key: 'asset-deactivate-pub.jpg', value: `${s3Location}/asset-deactivate-pub.jpg` });
        variableAndValues.push({ key: 'asset-removed-pub.jpg', value: `${s3Location}/asset-removed-pub.jpg` });
        variableAndValues.push({ key: 'campaign-approved-ad.jpg', value: `${s3Location}/campaign-approved-ad.jpg` });
        variableAndValues.push({ key: 'campaign-complete-ad.jpg', value: `${s3Location}/campaign-complete-ad.jpg` });
        variableAndValues.push({ key: 'campaign-not -approved-ad.jpg', value: `${s3Location}/campaign-not -approved-ad.jpg` });
        variableAndValues.push({ key: 'campaign-stopped-ad.jpg', value: `${s3Location}/campaign-stopped-ad.jpg` });
        variableAndValues.push({ key: 'existing-user-ad.jpg', value: `${s3Location}/existing-user-ad.jpg` });
        variableAndValues.push({ key: 'first-campaign-run-ad.jpg', value: `${s3Location}/first-campaign-run-ad.jpg` });
        variableAndValues.push({ key: 'first-platform-added-pub.jpg', value: `${s3Location}/first-platform-added-pub.jpg` });
        variableAndValues.push({ key: 'monthly-invoice-ad.jpg', value: `${s3Location}/monthly-invoice-ad.jpg` });
        variableAndValues.push({ key: 'monthly-payment-pub.jpg', value: `${s3Location}/monthly-payment-pub.jpg` });
        variableAndValues.push({ key: 'monthly-report-campaign-ad.jpg', value: `${s3Location}/monthly-report-campaign-ad.jpg` });
        variableAndValues.push({ key: 'monthly-report-platform-pub.jpg', value: `${s3Location}/monthly-report-platform-pub.jpg` });
        variableAndValues.push({ key: 'more-assets-pub.jpg', value: `${s3Location}/more-assets-pub.jpg` });
        variableAndValues.push({ key: 'new-payment-method-ad.jpg', value: `${s3Location}/new-payment-method-ad.jpg` });
        variableAndValues.push({ key: 'new-user-admin-ad.jpg', value: `${s3Location}/new-user-admin-ad.jpg` });
        variableAndValues.push({ key: 'platform-added-not-added-assets.jpg', value: `${s3Location}/platform-added-not-added-assets.jpg` });
        variableAndValues.push({ key: 'platform-added-pub.jpg', value: `${s3Location}/platform-added-pub.jpg` });
        variableAndValues.push({ key: 'platform-deactivate-pub.jpg', value: `${s3Location}/platform-deactivate-pub.jpg` });
        variableAndValues.push({ key: 'platform-removal-pub.jpg', value: `${s3Location}/platform-removal-pub.jpg` });
        variableAndValues.push({ key: 'problem-with-payment-ad.jpg', value: `${s3Location}/problem-with-payment-ad.jpg` });
        variableAndValues.push({ key: 'problem-with-payment-pub.jpg', value: `${s3Location}/problem-with-payment-pub.jpg` });
        variableAndValues.push({ key: 'setup-complete-no-campaign-ad.jpg', value: `${s3Location}/setup-complete-no-campaign-ad.jpg` });
        variableAndValues.push({ key: 'button-pub-01.jpg', value: `${s3Location}/button-pub-01.jpg` });
        variableAndValues.push({ key: 'button-pub-02.jpg', value: `${s3Location}/button-pub-02.jpg` });
        variableAndValues.push({ key: 'button-pub-03.jpg', value: `${s3Location}/button-pub-03.jpg` });
        variableAndValues.push({ key: 'button-pub-04.jpg', value: `${s3Location}/button-pub-04.jpg` });
        variableAndValues.push({ key: 'button-pub-05.jpg', value: `${s3Location}/button-pub-05.jpg` });
        variableAndValues.push({ key: 'button-pub-06.jpg', value: `${s3Location}/button-pub-06.jpg` });
        variableAndValues.push({ key: 'button-pub-07.jpg', value: `${s3Location}/button-pub-07.jpg` });
        variableAndValues.push({ key: 'button-pub-08.jpg', value: `${s3Location}/button-pub-08.jpg` });
        variableAndValues.push({ key: 'button-pub-09.jpg', value: `${s3Location}/button-pub-09.jpg` });
        variableAndValues.push({ key: 'button-pub-10.jpg', value: `${s3Location}/button-pub-10.jpg` });
        variableAndValues.push({ key: 'button-pub-11.jpg', value: `${s3Location}/button-pub-11.jpg` });
        variableAndValues.push({ key: 'button-pub-12.jpg', value: `${s3Location}/button-pub-12.jpg` });
        variableAndValues.push({ key: 'button-pub-13.png', value: `${s3Location}/button-pub-13.png` });
        variableAndValues.push({ key: 'button-pub-14.png', value: `${s3Location}/button-pub-14.png` });
        variableAndValues.push({ key: 'button-pub-16.jpg', value: `${s3Location}/button-pub-16.jpg` });
        variableAndValues.push({ key: 'inventory-pub.png', value: `${s3Location}/inventory-pub.png` });
        variableAndValues.push({ key: 'payments-pub.png', value: `${s3Location}/payments-pub.png` });
        variableAndValues.push({ key: 'receipt-pub.png', value: `${s3Location}/receipt-pub.png` });
        variableAndValues.push({ key: 'schedule-pub.png', value: `${s3Location}/schedule-pub.png` });
        variableAndValues.push({ key: 'summaries-pub.png', value: `${s3Location}/summaries-pub.png` });
        variableAndValues.push({ key: 'setup-pub.png', value: `${s3Location}/setup-pub.png` });
        variableAndValues.push({ key: 'restore-asset.png', value: `${s3Location}/restore-asset.png` });
        variableAndValues.push({ key: 'remove-asset.png', value: `${s3Location}/remove-asset.png` });
        variableAndValues.push({ key: 'delete-pub.png', value: `${s3Location}/delete-pub.png` });
        variableAndValues.push({ key: 'review-ad.png', value: `${s3Location}/review-ad.png` });
        variableAndValues.push({ key: 'asset-removed-pub.jpg', value: `${s3Location}/asset-removed-pub.jpg` });
        variableAndValues.push({ key: 'add-campaign-ad.jpg', value: `${s3Location}/add-campaign-ad.jpg` });
        variableAndValues.push({ key: 'campaign-analytics-ad.jpg', value: `${s3Location}/campaign-analytics-ad.jpg` });
        variableAndValues.push({ key: 'assets-pub.jpg', value: `${s3Location}/assets-pub.jpg` });
        variableAndValues.push({ key: 'my-payments-pub.jpg', value: `${s3Location}/my-payments-pub.jpg` });
        variableAndValues.push({ key: 'dashboard-pub.jpg', value: `${s3Location}/dashboard-pub.jpg` });
        variableAndValues.push({ key: 'asset-removed-image-pub.jpg', value: `${s3Location}/asset-removed-image-pub.jpg` });
        variableAndValues.push({ key: 'feeback-73.jpg', value: `${s3Location}/feeback-73.jpg` });
        variableAndValues.push({ key: 'my-payments-pub.jpg', value: `${s3Location}/my-payments-pub.jpg` });
        variableAndValues.push({ key: 'remove-platform.png', value: `${s3Location}/remove-platform.png` });
        variableAndValues.push({ key: 'restore-platform.png', value: `${s3Location}/restore-platform.png` });
        variableAndValues.push({ key: 'doceree-facebook.png', value: `${s3Location}/doceree-facebook.png` });
        variableAndValues.push({ key: 'doceree-twitter.png', value: `${s3Location}/doceree-twitter.png` });
        variableAndValues.push({ key: 'doceree-linkedin.png', value: `${s3Location}/doceree-linkedin.png` });
        variableAndValues.push({ key: 'doceree-twt-pub.png', value: `${s3Location}/doceree-twt-pub.png` });
        variableAndValues.push({ key: 'doceree-in-pub.png', value: `${s3Location}/doceree-in-pub.png` });
        variableAndValues.push({ key: 'doceree-fb-pub.png', value: `${s3Location}/doceree-fb-pub.png` });
        variableAndValues.push({ key: 'accunt-setup-ad.jpg', value: `${s3Location}/accunt-setup-ad.jpg` });
        variableAndValues.push({ key: 'acunt-setup-pub.jpg', value: `${s3Location}/acunt-setup-pub.jpg` });
        variableAndValues.push({ key: 'Del-accunt-pub.jpg', value: `${s3Location}/Del-accunt-pub.jpg` });
        variableAndValues.push({ key: 'del-ad.jpg', value: `${s3Location}/del-ad.jpg` });
        variableAndValues.push({ key: 'password-changed-pub.jpg', value: `${s3Location}/password-changed-pub.jpg` });
        variableAndValues.push({ key: 'pswrd-chngd-ad.jpg', value: `${s3Location}/pswrd-chngd-ad.jpg` });
        variableAndValues.push({ key: 'reg-ad.jpg', value: `${s3Location}/reg-ad.jpg` });
        variableAndValues.push({ key: 'reg-complete-veri-profile-incomplete-pub.jpg', value: `${s3Location}/reg-complete-veri-profile-incomplete-pub.jpg` });
        variableAndValues.push({ key: 'reg-complete-veri-pro-incomplete.jpg', value: `${s3Location}/reg-complete-veri-pro-incomplete.jpg` });
        variableAndValues.push({ key: 'reg-pub.jpg', value: `${s3Location}/reg-pub.jpg` });
        variableAndValues.push({ key: 'reg-ver-complete-pro-in-pub.jpg', value: `${s3Location}/reg-ver-complete-pro-in-pub.jpg` });
        variableAndValues.push({ key: 'reg-veri-complete-profile-incomplete.jpg', value: `${s3Location}/reg-veri-complete-profile-incomplete.jpg` });
        variableAndValues.push({ key: 'reset-password-pub.jpg', value: `${s3Location}/reset-password-pub.jpg` });
        variableAndValues.push({ key: 'reset-psswrd-ad.jpg', value: `${s3Location}/reset-psswrd-ad.jpg` });
        variableAndValues.push({ key: 'support-ad.jpg', value: `${s3Location}/support-ad.jpg` });
        variableAndValues.push({ key: 'support-response-pub.jpg', value: `${s3Location}/support-response-pub.jpg` });
        variableAndValues.push({ key: 'time-to-change-passwrd-pub.jpg', value: `${s3Location}/time-to-change-passwrd-pub.jpg` });
        variableAndValues.push({ key: 'time-to-chng-pswrd-ad.jpg', value: `${s3Location}/time-to-chng-pswrd-ad.jpg` });
        variableAndValues.push({ key: 'create-account-advertiser-67.png', value: `${s3Location}/create-account-advertiser-67.png` });
        variableAndValues.push({ key: 'change-password-advertiser-63.png', value: `${s3Location}/change-password-advertiser-63.png` });
        variableAndValues.push({ key: 'change-password-pub-63.png', value: `${s3Location}/change-password-pub-63.png` });
        variableAndValues.push({ key: 'regstration-ad-67.png', value: `${s3Location}/regstration-ad-67.png` });
        variableAndValues.push({ key: 'regstration-pub-67.png', value: `${s3Location}/regstration-pub-67.png` });
        variableAndValues.push({ key: 'reset-password-advertiser-65.png', value: `${s3Location}/reset-password-advertiser-65.png` });
        variableAndValues.push({ key: 'reset-password-publisher-65.png', value: `${s3Location}/reset-password-publisher-65.png` });
        variableAndValues.push({ key: 'advertiser-dashboard.jpg', value: `${s3Location}/advertiser-dashboard.jpg` });
        variableAndValues.push({ key: 'advertiser-delete-account.jpg', value: `${s3Location}/advertiser-delete-account.jpg` });
        variableAndValues.push({ key: 'publisher-dashboard.jpg', value: `${s3Location}/publisher-dashboard.jpg` });
        variableAndValues.push({ key: 'publisher-delete-account.jpg', value: `${s3Location}/publisher-delete-account.jpg` });
        variableAndValues.push({ key: 'review-ticket-pub.jpg', value: `${s3Location}/review-ticket-pub.jpg` });
        variableAndValues.push({ key: 'review-ticket-ad.jpg', value: `${s3Location}/review-ticket-ad.jpg` });
        variableAndValues.push({ key: 'existing-user-publisher.jpg', value: `${s3Location}/existing-user-publisher.jpg` });
        variableAndValues.push({ key: 'create-account-granted-user-button-publisher.png', value: `${s3Location}/create-account-granted-user-button-publisher.png` });
        variableAndValues.push({ key: 'new-user-admin-publisher.jpg', value: `${s3Location}/new-user-admin-publisher.jpg` });
        variableAndValues.push({ key: 'new-user-developer-publisher.jpg', value: `${s3Location}/new-user-developer-publisher.jpg` });
        variableAndValues.push({ key: 'new-user-analyst-publisher.jpg', value: `${s3Location}/new-user-analyst-publisher.jpg` });
        variableAndValues.push({ key: 'added-one-add-more-slots-publisher-button.png', value: `${s3Location}/added-one-add-more-slots-publisher-button.png` });
        variableAndValues.push({ key: 'add-more-slot-image-publisher.jpg', value: `${s3Location}/add-more-slot-image-publisher.jpg` });
        variableAndValues.push({ key: 'create-account-granted-user-access-button-publisher.png', value: `${s3Location}/create-account-granted-user-access-button-publisher.png` });
        variableAndValues.push({ key: 'ad-slot-integration-new-developer.jpg', value: `${s3Location}/ad-slot-integration-new-developer.jpg` });
        variableAndValues.push({ key: 'ad-slot-integration-existing-developer.jpg',
                                 value: `${s3Location}/ad-slot-integration-existing-developer.jpg` });
        variableAndValues.push({ key: 'audience-integration-existing-developer.jpg', value: `${s3Location}/audience-integration-existing-developer.jpg` });
        variableAndValues.push({ key: 'audience-integration-new-developer.jpg', value: `${s3Location}/audience-integration-new-developer.jpg` });
        variableAndValues.push({ key: 'platform-integration-existing-developer.jpg', value: `${s3Location}/platform-integration-existing-developer.jpg` });
        variableAndValues.push({ key: 'platform-integration-new-developer.jpg', value: `${s3Location}/platform-integration-new-developer.jpg` });

        // figure out template location
        const templateFileDirectory = config.get<string>('mail.html-template-dir');
        const fileName = templateName;
        let templateFileLocation = path.resolve(templateFileDirectory);
        templateFileLocation = path.resolve(templateFileLocation + path.sep + fileName);
        const readInterface = readline.createInterface({
            input: fs.createReadStream(templateFileLocation),
            output: process.stdout,
            console: false
        });

        const lines: string[] = [];
        readInterface.on('line', (line: string) => {
            lines.push(line);
        });

        return readInterface.on('close', async () => {
            let finalContent: string = '';
            for (let line of lines) {
                if (line.indexOf('#$[') > 0) {
                    for (const v of variableAndValues) {
                        if (line.indexOf(`#$[${v.key}]$#`) > 0) {
                            line = line.replace(`#$[${v.key}]$#`, v.value);
                        }
                    }
                }
                finalContent = `${finalContent}${line}\n`;
            }
            const from: string = config.get<string>('mail.user');
            const text = 'Not used';
            if (to) {
                if (attachment) {
                    return await this.mail.send(from, to, subject, text, finalContent, attachment);
                } else {
                    return await this.mail.send(from, to, subject, text, finalContent);
                }
            } else {
                throw new Error('No receipient address provided');
            }
        });
    }
}
