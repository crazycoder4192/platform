import config from 'config';
import { MailHelper } from './mail-helper';

export class GeneralMail {
    private readonly mailHelper: MailHelper;

    constructor() {
        this.mailHelper = new MailHelper();
    }

    public async sendRegistrationCompletedAdvertiserMail(to: string, activationLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'activationLink', value: activationLink }
        ];
        return await this.mailHelper.readTemplate(
            to,
            'Verify your email to activate your Doceree account',
            'registration-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendRegistrationCompletedPublisherMail(to: string, activationLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'activationLink', value: activationLink }
        ];
        return await this.mailHelper.readTemplate(
            to,
            'Verify your email to activate your Doceree account',
            'registration-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendReminderToCompleteVerificationAdvertiserMail(to: string,
                                                                  activationLink: string,
                                                                  noOfDays: number, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'activationLink', value: activationLink },
            { key: 'date', value: noOfDays },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Verify your email... It will only take 30 seconds!',
            'reminder-registration-complete-verification-profile-incomplete-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendReminderToCompleteVerificationPublisherMail(to: string,
                                                                 activationLink: string,
                                                                 noOfDays: number, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'activationLink', value: activationLink },
            { key: 'date', value: noOfDays },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            ' Verify your email... It will only take 30 seconds',
            'reminder-registration-complete-verification-profile-incomplete-publisher-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendVerificationCompleteMail(to: string, clickHereForUpdateProfileLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'clickHereForUpdateProfileLink', value: clickHereForUpdateProfileLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Verification Complete',
            'verification-complete-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendReminderToCompleteProfileLeftHalfWayAdvertiser(to: string, profilePageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'profilePageLink', value: profilePageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Reminder Profile Incomplete',
            'reminder-profile-left-half-way-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendReminderToCompleteProfileLeftHalfWayPublisher(to: string, profilePageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'profilePageLink', value: profilePageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Reminder Profile Incomplete',
            'reminder-profile-left-half-way-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendReminderToCompleteProfileAdvertiser(to: string, noOfDays: number, profilePageLink: string,
                                                         unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'noOfDays', value: noOfDays },
            { key: 'profilePageLink', value: profilePageLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "You've been verified, now complete your profile",
            'reminder-registration-verification-complete-profile-incomplete-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendReminderToCompleteProfilePublisher(to: string, noOfDays: number, profilePageLink: string,
                                                        unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'date', value: noOfDays },
            { key: 'profilePageLink', value: profilePageLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "You've been verified, now complete your profile",
            'reminder-registration-verification-complete-profile-incomplete-publisher-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendAccountSetupCompletedAdvertiserMail(to: string, adCampaignPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'adCampaignPageLink', value: adCampaignPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Account Setup Complete',
            'account-setup-complete-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    // NOT USED
    public async sendAccountSetupCompletedPublisherMail(to: string, platformPageLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'platformPageLink', value: platformPageLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Account Setup Complete',
            'account-setup-complete-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendResetPasswordAdvertiserMail(to: string, username: string, activationLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'email', value: to },
            { key: 'activationLink', value: activationLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Reset your Doceree password',
            'reset-password-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendResetPasswordPublisherMail(to: string, username: string, activationLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'email', value: to },
            { key: 'activationLink', value: activationLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Reset your Doceree password',
            'reset-password-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendPasswordChangedAdvertiserMail(to: string, username: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Password change confirmation',
            'change-password-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendPasswordChangedPublisherMail(to: string, username: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Password change confirmation',
            'change-password-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendReminderToChangePasswordAdvertiserMail(to: string, username: string, url: string,
                                                            unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'redirectLink', value: url },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "It's time to change your password",
            'time-to-change-the-password-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendReminderToChangePasswordPublisherMail(to: string, username: string, url: string,
                                                           unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'redirectLink', value: url },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "It's time to change your password",
            'time-to-change-the-password-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendSupportAutoResponseAdvertiserMail(to: string, username: string, ticketNumber: string,
                                                       redirectLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'referenceNumberLink', value: ticketNumber },
            { key: 'redirectLink', value: redirectLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Need help? We’ve got your back',
            'support-auto-response-ad-mail-template.html',
            variablesAndValues
        );
    }

    public async sendSupportAutoResponsePublisherMail(to: string, username: string, ticketNumber: string,
                                                      redirectLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'username', value: username },
            { key: 'referenceNumberLink', value: ticketNumber },
            { key: 'redirectLink', value: redirectLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Need help? We’ve got your back',
            'support-auto-response-pub-mail-template.html',
            variablesAndValues
        );
    }

    public async sendChangeinPolicyTermsMail(to: string, policyNameLink: string, date: string,
                                             unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'policyNameLink', value: policyNameLink },
            { key: 'date', value: date },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            'Change In Policy Terms',
            'change-policy-terms-mail-template.html',
            variablesAndValues
        );
    }

    public async sendDeleteAccountAdvertiserMail(to: string, accountDeleteLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'accountDeleteLink', value: accountDeleteLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "What? No! Don't say goodbye!",
            'delete-account-advertiser-mail-template.html',
            variablesAndValues
        );
    }

    public async sendDeleteAccountPublisherMail(to: string, accountDeleteLink: string, unsubscribeLink: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'accountDeleteLink', value: accountDeleteLink },
            { key: 'unsubscribeLink', value: unsubscribeLink }
        ];

        return await this.mailHelper.readTemplate(
            to,
            "What? No! Don't say goodbye!",
            'delete-account-publisher-mail-template.html',
            variablesAndValues
        );
    }

    public async sendInvitationMail(to: string, invitationCode: string,
                                    firstName: string): Promise<boolean> {
        const variablesAndValues: any[] = [
            { key: 'invitationCode', value: invitationCode },
            { key: 'firstName', value: firstName }
        ];
        return await this.mailHelper.readTemplate(
            to,
            'Your exclusive invite to the world of Doceree',
            'invitation-code-mail-template.html',
            variablesAndValues
        );
    }

    public getUnsubscribeLink(jwtToken: string) {
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        return `${url}/unsubscribeUser?token=${jwtToken}`;
    }
}
