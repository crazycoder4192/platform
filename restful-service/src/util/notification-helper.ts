import NotificationDAO from '../daos/notification.dao';
import NotificationDTO from '../dtos/notification.dto';
import { NotificationIcon, NotificationRoute, NotificationType } from './constants';

export class NotificationHelper {

    private readonly notificationDAO: NotificationDAO;

    constructor() {
        this.notificationDAO = new NotificationDAO();
    }

    public async addNotification(users: string[], message: string, icon: NotificationIcon,
                                 type: NotificationType, typeId: string, routeTo: NotificationRoute) {
        for (const user of users) {
            const notification: NotificationDTO = new NotificationDTO();
            notification.typeDetails = { type, typeId, routeTo };
            notification.email = user;
            notification.isRead = false;
            notification.message = message;
            notification.icon = icon;
            notification.created = { at: new Date() };
            await this.notificationDAO.create(notification);
        }
    }

    public async markNotificationAsRead(email: string, notificationId: string) {
        const dto: NotificationDTO = await this.notificationDAO.findById(notificationId);
        dto.isRead = true;
        this.notificationDAO.update(dto._id, dto);
    }

    public async notificationForAdvertiser(email: string, brandId: string) {
        const unreadGeneralNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndGlobalAndReadStatus(email,
            false);
        const readGeneralNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndGlobalAndReadStatus(email,
            true);

        const unreadBrandNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndBrandAndReadStatus(email,
            brandId, false);
        const readBrandNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndBrandAndReadStatus(email,
            brandId, true);

        const unreadNotificationCount: number = unreadBrandNotifications.length + unreadGeneralNotifications.length;
        const notifications: any = [];
        notifications.push(...unreadBrandNotifications, ...unreadGeneralNotifications, ...readBrandNotifications, ...readGeneralNotifications);
        return { notifications, unreadNotificationCount };
    }

    public async notificationForPublisher(email: string, platformId: string) {
        const unreadGeneralNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndGlobalAndReadStatus(email,
            false);
        const readGeneralNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndGlobalAndReadStatus(email,
            true);

        const unreadPlatformNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndPlatformAndReadStatus(email,
            platformId, false);
        const readPlatformNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndPlatformAndReadStatus(email,
            platformId, true);

        const unreadNotificationCount: number = unreadPlatformNotifications.length + unreadGeneralNotifications.length;
        const notifications: any = [];
        notifications.push(...unreadPlatformNotifications, ...unreadGeneralNotifications, ...readPlatformNotifications, ...readGeneralNotifications);
        return { notifications, unreadNotificationCount };
    }

    public async markDelete(notificationId: string) {
        const notification: NotificationDTO = await this.notificationDAO.findById(notificationId);
        notification.delete = true;
        await this.notificationDAO.update(notificationId, notification);
    }

    public async unreadNotificationsForAdvertiser(email: string, brandId: string) {
        const unreadGeneralNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndGlobalAndReadStatus(email, false);
        const unreadBrandNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndBrandAndReadStatus(email, brandId, false);
        const unreadNotificationCount: number = unreadBrandNotifications.length + unreadGeneralNotifications.length;
        return unreadNotificationCount;
    }

    public async unreadNotificationsForPublisher(email: string, platformId: string) {
        const unreadGeneralNotifications: NotificationDTO[] = await this.notificationDAO.findByEmailAndGlobalAndReadStatus(email, false);
        const unreadPlatformNotifications: NotificationDTO[] =
        await this.notificationDAO.findByEmailAndPlatformAndReadStatus(email, platformId, false);
        const unreadNotificationCount: number = unreadPlatformNotifications.length + unreadGeneralNotifications.length;
        return unreadNotificationCount;
    }
}
