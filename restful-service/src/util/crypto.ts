// Nodejs encryption with CTR
import config from 'config';
import crypto from 'crypto';

export class Encryption {
  /*public encrypt(text: string): string {
    const cipher = crypto.createCipher(
      'aes-256-cbc',
      config.get<string>('crypto.secret_key'),
    );
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  }

  public decrypt(text: string): string {
    if (text === null || typeof text === 'undefined') {
      return text;
    }
    const decipher = crypto.createDecipher(
      'aes-256-cbc',
      config.get<string>('crypto.secret_key'),
    );
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  }*/

  public encrypt(text: string) {
    const cipher = crypto.createCipheriv('aes-256-cbc',
      new Buffer(config.get<string>('crypto.secret_key')), new Buffer('docereesecretkey'));
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  }
  public decrypt(text: string) {
    if (text === null || typeof text === 'undefined') {
      return text;
    }
    const decipher = crypto.createDecipheriv('aes-256-cbc',
      new Buffer(config.get<string>('crypto.secret_key')), new Buffer('docereesecretkey'));
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    // return `****-****-****- ${dec.slice(dec.length - 4, dec.length)}`;
    return dec;
  }
  public decryptCardNumber(text: string) {
    const decipher = crypto.createDecipheriv('aes-256-cbc',
      new Buffer(config.get<string>('crypto.secret_key')), new Buffer('docereesecretkey'));
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return `XXXX-XXXX-XXXX-${dec.slice(dec.length - 4, dec.length)}`;
    // return dec;
  }
  public decryptCvv(text: string) {
    const decipher = crypto.createDecipheriv('aes-256-cbc',
      new Buffer(config.get<string>('crypto.secret_key')), new Buffer('docereesecretkey'));
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return '****';
    // return dec;
  }
}
