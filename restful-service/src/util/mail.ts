import config from 'config';
const kue = require('kue');

export class Mail {

  private readonly queue: any;
  constructor() {
    this.queue = kue.createQueue({
      prefix: 'q',
      redis: {
        port: config.get<string>('redis.port'),
        host: config.get<string>('redis.host')
      }
    });
  }

  public async send(
    mFrom: string,
    mTo: string,
    mSubject: string,
    mText: string,
    mBody: string,
    mAttachments?: any[]
  ): Promise<boolean> {

    const mailOptions = {
      from: mFrom, // sender address
      html: mBody,
      subject: mSubject, // Subject line
      text: mText, // plaintext body
      to: mTo, // list of receivers
      attachments: mAttachments
    };

    this.queue.create('send-mail', mailOptions).save();

    return true;
  }
}
