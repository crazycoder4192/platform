import bcrypt from 'bcryptjs';
import config from 'config';
import jwt from 'jsonwebtoken';
import moment from 'moment';
import PasswordHistoryDAO from '../daos/password-history.dao';
import UserTokenDAO from '../daos/user-token.dao';
import PasswordHistoryDTO from '../dtos/password-history.dto';

export class HashPwd {
  private readonly userTokenDAO: UserTokenDAO;
  private readonly passwordHistoryDAO: PasswordHistoryDAO;

  public async jwtToken(email: string, expiryInSeconds: number): Promise<any> {
    const hostKey = config.get<string>('jwt.secretKey');
    const jwttoken = jwt.sign({ email, exp: expiryInSeconds }, hostKey);
    return jwttoken;
  }

  public async verifyToken(token: string): Promise<string> {
    try {
      const tokenData = await this.userTokenDAO.findByToken(token);
      const decoded: any = jwt.decode(token);
      const expiryTime = moment(tokenData.expiryTime).valueOf();
      if (tokenData.email === decoded.email && expiryTime <= Date.now()) {
        return 'INVALID_TOKEN';
      } else {
        return 'VALID_TOKEN';
      }
    } catch (err) {
      return 'INVALID_TOKEN';
    }
  }

  public async generateHash(
    prefixSecretKey: string,
    password: string,
  ): Promise<string> {
    const passwordModified = await this.hashComparision(
      prefixSecretKey,
      password,
    );
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hashedPassword = bcrypt.hashSync(passwordModified, salt);
    return hashedPassword;
  }

  public async hashComparision(
    prefixSecretKey: string,
    password: string
  ): Promise<string> {
    const prefixKeyArray = prefixSecretKey.match(/.{1,4}/g);
    const passwordModified =
      prefixKeyArray[2] +
      password +
      prefixKeyArray[1] +
      password +
      prefixKeyArray[0] +
      password +
      prefixKeyArray[3] +
      password;
    return passwordModified;
  }

  public async nameExistsInPassword(
    firstName: string,
    lastName: string,
    password: string
  ): Promise<boolean> {
    let firstNameExists = false;
    let lastNameExists = false;
    let firstName4CharsExists = false;
    let lastName4CharsExists = false;
    let exists = false;
    if (firstName) {
      firstNameExists = password
        .toLowerCase()
        .includes(firstName.toLowerCase());
      firstName4CharsExists = password.toLowerCase().includes(
        firstName
          .toLowerCase()
          .substring(0, 4)
          .toLowerCase()
      );
    }
    if (lastName) {
      lastNameExists = password.toLowerCase().includes(lastName.toLowerCase());
      lastName4CharsExists = password
        .toLowerCase()
        .includes(lastName.toLowerCase().substring(0, 4));
    }
    if (firstNameExists || lastNameExists) {
      exists = true;
    }
    if (firstName4CharsExists || lastName4CharsExists) {
      exists = true;
    }
    return exists;
  }

  public async generateSecurityAnswerHash(
    securityAnswer: string
  ): Promise<string> {
    const securityKey = config.get<string>('securityAnswersKey.secretKey');
    const securityAnswerModified = await this.hashComparision(securityKey, securityAnswer);

    const saltRounds = 8;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hashedPassword = bcrypt.hashSync(securityAnswerModified, salt);
    return hashedPassword;
  }

  public async validateSecurityAnswer(securityAnswer: string, dbsecurityAnswer: string): Promise<boolean> {
    const passSecret = await this.hashComparision(
      config.get<string>('securityAnswersKey.secretKey'),
      securityAnswer
    );
    return bcrypt.compareSync(passSecret, dbsecurityAnswer);
  }

  private async checkingPasswordHistory(
    emailId: string,
    passwordSecret: string,
    password: string,
  ): Promise<boolean> {
    let exists = false;
    const passwordData: PasswordHistoryDTO[] = await this.passwordHistoryDAO.getLastThreePasswords(
      emailId,
    );
    for (const ele of passwordData) {
      const newPasswordSecret = await this.hashComparision(
        passwordSecret,
        password,
      );
      const result = bcrypt.compareSync(newPasswordSecret, ele.password);
      if (!result) {
        exists = true;
      }
    }
    return exists;
  }
}
