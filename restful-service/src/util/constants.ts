export enum UserTypes {
    advertiser = 'advertiser',
    publisher = 'publisher',
    admin = 'admin'
}

export enum UserStatuses {
    active = 'active',
    inactive = 'inactive'
}

export enum AdvertiserPermissions {
    editOrgDetails = 'edit_org_details',
    editPassword = 'edit_password',
    editEmailSettings = 'edit_email_settings',

    addCampaign = 'add_campaign',
    editCampaign = 'edit_campaign',
    viewCampaign = 'view_campaign',
    deleteCampaign = 'delete_campaign',

    addCreativeHub = 'add_creative_hub',
    editCreativeHub = 'edit_creative_hub',
    viewCreativeHub = 'view_creative_hub',
    deleteCreativeHub = 'delete_creative_hub',

    viewDashboard = 'view_dashboard',

    viewAnalytics = 'view_analytics',

    addSupportTicket = 'add_support_ticket',
    editSupportTicket = 'edit_support_ticket',
    viewSupportTicket = 'view_support_ticket',

    addPaymentMethod = 'add_payment_method',
    editPaymentMethod = 'edit_payment_method',
    viewPaymentMethod = 'view_payment_method',
    deletePaymentMethod = 'delete_payment_method',

    addPaymentInvoice = 'add_payment_invoice',
    viewPaymentInvoice = 'view_payment_invoice',

    addCreditLine = 'add_credit_line',
    editCreditLine = 'edit_credit_line',
    viewCreditLine = 'view_credit_line',

    addBrand = 'add_brand',
    editBrand = 'edit_brand',
    viewBrand = 'view_brand',

    addUser = 'add_user',
    editUser = 'edit_user',
    viewUser = 'view_user'
}

export enum PublisherPermissions {
    editOrgDetails = 'edit_org_details',
    editPassword = 'edit_password',
    editEmailSettings = 'edit_email_settings',

    addAsset = 'add_asset',
    editAsset = 'edit_assset',
    viewAsset = 'view_asset',
    deleteAsset = 'delete_asset',

    viewDashboard = 'view_dashboard',

    viewAnalytics = 'view_analytics',

    addSupportTicket = 'add_support_ticket',
    editSupportTicket = 'edit_support_ticket',
    viewSupportTicket = 'view_support_ticket',

    addPaymentMethod = 'add_payment_method',
    editPaymentMethod = 'edit_payment_method',
    viewPaymentMethod = 'view_payment_method',
    deletePaymentMethod = 'delete_payment_method',

    addPaymentBill = 'add_payment_bill',
    viewPaymentBill = 'view_payment_bill',

    addPlatform = 'add_platform',
    editPlatform = 'edit_platform',
    viewPlatform = 'view_platform',

    addUser = 'add_user',
    editUser = 'edit_user',
    viewUser = 'view_user'
}

export enum SubcampaignStatus {
    draft = 'Draft',
    publish = 'Publish',
    underReview = 'Under review',
    error = 'Error',
    approved = 'Active',
    paused = 'Paused',
    completed = 'Completed',
    recentlyCompleted = 'Recently Completed'
}

export enum TimeFilters {
    last24Hours = 'last24Hours',
    last48Hours = 'last48Hours',
    last1Week = 'last1Week',
    last1Month = 'last1Month',
    last6Months = 'last6Months'
}

export enum NotificationIcon {
    success = 'success',
    info = 'info',
    error = 'error'
}

export enum NotificationType {
    general = 'general',
    brand = 'brand',
    platform = 'platform'
}

export enum NotificationRoute {
    advertiser_payment = 'payment',
    advertiser_change_password = 'change-password',
    advertiser_campaign = 'campaign',
    advertiser_manage_account = 'manage-account',
    publisher_payment = 'payment',
    publisher_change_password = 'change-password',
    publisher_manage_platform = 'manage-platform',
    email_settings = 'email-settings',
    publisher_manage_account = 'manage-account'
}

export enum CreditLineStatus {
    under_review = 'Under-Review',
    approved = 'Approved',
    rejected = 'Rejected'
}

export enum InvoiceStatus {
    pending = 'Pending',
    paid = 'Paid',
    noCreditCardDetail = 'No Credit Card Details Available'
}

export enum BillStatus {
    pending = 'Pending',
    paid = 'Paid',
    noBankDetail = 'No Bank Details Available'
}

export enum PlatformHCPValidationStatus {
    verificationFailed = 'Verification Failed',
    verified = 'Verified',
    uploadPending = 'Upload Pending'
}

export enum PaymentStatus {
    success = 'Success',
    fail = 'Fail'
}
