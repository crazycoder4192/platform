import config from 'config';
import * as nodemailer from 'nodemailer';
import { logger } from './winston';
const kue = require('kue');

export class MailWorker {
    private readonly queue: any;
    private readonly transporter: nodemailer.Transporter;

    constructor() {
        this.queue = kue.createQueue({
            prefix: 'q',
            redis: {
              port: config.get<string>('redis.port'),
              host: config.get<string>('redis.host')
            }
        });
        const userKey = config.get<string>('mail.user');
        const passKey = config.get<string>('mail.password');

        this.transporter = nodemailer.createTransport({
            auth: {
                pass: passKey, // generated ethereal password
                user: userKey, // generated ethereal user
            },
            service: 'Outlook365',
        });
    }

    public processQueue() {
        this.queue.process('send-mail', async (job: any, done: any) => {
            try {
                await this.transporter.sendMail(job.data);
                await this.sleep(100);
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
                return false;
            }
            done();
        });
    }

    private sleep(milliseconds: number): Promise<any> {
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
    }
}
