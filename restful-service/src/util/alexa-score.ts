import config from 'config';
import Request from 'request-promise';
import convert from 'xml-js';
import { logger } from '../util/winston';

export class AlexaScore {
    private readonly alexaUrl: string;
    constructor() {
        this.alexaUrl = config.get<string>('alexa.url');
    }

    public async evaluate(url: string): Promise<number> {
        const response = await Request.get({
            url: this.alexaUrl,
            qs: {
                cli: '10',
                url
            }
        });
        const jsonResponse: any = JSON.parse(convert.xml2json(response, { compact: true, spaces: 4 }));
        if (jsonResponse && jsonResponse.ALEXA && jsonResponse.ALEXA.SD && jsonResponse.ALEXA.SD.REACH
            && jsonResponse.ALEXA.SD.POPULARITY._attributes.TEXT) {
            return +(jsonResponse.ALEXA.SD.POPULARITY._attributes.TEXT);
        } else {
            logger.error(`Unable to determine publisher rank from Alexa ${url}`);
            return Number.MAX_SAFE_INTEGER;
        }
    }
}
