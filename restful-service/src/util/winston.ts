import appRoot from 'app-root-path';
import { Options } from 'morgan';
import winston, { createLogger, format, transports } from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
const Sentry = require('winston-sentry-raven-transport');
const winstonElasticSearch = require('winston-elasticsearch');
const os = require('os');
import config from 'config';
const transport = new DailyRotateFile({
  datePattern: 'YYYY-MM-DD',
  filename: `${appRoot}/logs/app_%DATE%.log`,
  maxSize: '20m',
  zippedArchive: false,
});

const enumerateErrorFormat = winston.format((info) => {
  if (info instanceof Error) {
    return Object.assign({
      message: info.message,
      stack: info.stack
    }, info);
  }
  return info;
});

const env = config.get<string>('log-environment');
const esUrl = config.get<string>('elastic-search-url');
const options = {
  file: {
    datePattern: 'YYYY-MM-DD',
    filename: `${appRoot}/logs/app_%DATE%.log`,
    maxSize: '20m',
    zippedArchive: true,
    level: 'info',
    handleExceptions: true,
    json: true
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
  esTransportOpts: {
    level: 'info',
    clientOpts: {
      host: esUrl,
      log: 'info'
    },
    transformer: (data: any) => {
      const transformed: any = { };
      transformed['@timestamp'] = new Date().toISOString();
      transformed.source_host = os.hostname();
      transformed.message = data.message;
      if (typeof transformed.message === 'object') {
        transformed.message = JSON.stringify(transformed.message);
      }
      transformed.severity = data.level;
      transformed.fields = { environment: env };
      return transformed;
    }
  }
};

const sentryDsn = config.get<string>('sentry.dsn');
const streamingLogs = config.get<string>('sentry.streaming-logs');
const sentryOptions = {
  dsn: sentryDsn,
  level: 'warn',
  levelsMap: {
    info: 'info',
    debug: 'debug',
    warn: 'warning',
    error: 'error'
  },
  install: true
};

export const logger = winston.createLogger({
  transports: [
    new DailyRotateFile(options.file),
    ...(streamingLogs ? [new Sentry(sentryOptions), ] : [new winston.transports.Console(options.console)]),
    // new winstonElasticSearch(options.esTransportOpts)
  ],
  exitOnError: false, // do not exit on handled exceptions
});

export const morganOption: Options = {
  stream: {
    write(message: string) {
      logger.info(message.trim());
    },
  },
};
