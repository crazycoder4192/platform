import mongoose from 'mongoose';
import QuickbooksTokenDTO from '../dtos/quickbooks-token.dto';

const quickbooksTokenSchema = new mongoose.Schema({
    createdAt: {
        default: Date.now,
        required: true,
        type: Date,
    },
    accessToken: String,
    refreshToken: String
});

export const indiaQuickbookTokenModel = mongoose.model<QuickbooksTokenDTO & mongoose.Document>(
    'quickbooks_token_india',
    quickbooksTokenSchema,
    'quickbooks_token_india',
);
