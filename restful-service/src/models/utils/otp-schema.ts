import mongoose from 'mongoose';
import OtpDTO from '../../dtos/utils/otp.dto';

const otpSchema = new mongoose.Schema({
    otp: {
        type: String,
        required: true
    },
    expiryTime: {
        type: Date,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    otpStatus: {
        type: String,
        required: true
    },
    purpose: {
        type: String,
        required: true
    },
    gateWay: {
        type: String,
        required: true
    },
    otpChannel: {
        type: String,
        required: true
    },
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    }
});

export const otpModel = mongoose.model<OtpDTO & mongoose.Document>('one_time_passwords', otpSchema);
