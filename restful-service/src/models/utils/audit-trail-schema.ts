import mongoose from 'mongoose';

const apiAuditDetails = {
  '/': {
    type: 'APP',
    feature: '/',
    action: 'Invoked',
    desc: 'Base API accessed',
  },
  '/admin': {
    type: 'ADMIN',
    feature: '/admin',
    action: 'Invoked',
    desc: 'Admin Module accessed',
  },
  '/admin/smtpdetails': {
    type: 'ADMIN',
    feature: '/admin/smtpdetails',
    action: 'Invoked',
    desc: 'Requested SMTP configuration details',
  },
};

export interface IAuditTrail extends mongoose.Document {
  action: string;
  actionDescription: string;
  applicationType: string;
  date: Date;
  feature: string;
  sourceIp: string;
  userId: string;
}

const auditTrailSchema = new mongoose.Schema({
  action: {
    required: false,
    type: String,
  },
  actionDescription: {
    required: false,
    type: String,
  },
  applicationType: {
    required: false,
    type: String,
  },
  date: {
    default: Date.now,
    type: Date,
  },
  feature: {
    required: false,
    type: String,
  },
  sourceIp: {
    required: false,
    type: String,
  },
  userId: {
    required: false,
    type: String,
  },
});

export const auditTrailModel = mongoose.model<IAuditTrail>(
  'audittrails',
  auditTrailSchema,
);
