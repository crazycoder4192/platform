import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const AdServerRequestURLSchema = new Schema({ }, { strict: false });

export const AdServerRequestURLModel = mongoose.model('adserver_requesturl_details', AdServerRequestURLSchema);
