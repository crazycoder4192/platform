import mongoose from 'mongoose';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';

const filterAnalyticsSchema = new mongoose.Schema({
    filterName: {
        type: String,
        required: true
    },
    specialization: [{
        type: String
    }],
    gender: [{
        type: String
    }],
    location: [{
        type: String
    }],
    user: {
        type: String,
        required: false
    },
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    }
});

export const AnalyticsFilterModel = mongoose.model<FilterAnalyticsDTO & mongoose.Document>('analytics_filters', filterAnalyticsSchema);
