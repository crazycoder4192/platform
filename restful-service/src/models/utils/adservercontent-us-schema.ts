import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const AdServerContentSchema = new Schema({ }, { strict: false });

export const usAdServerContentModel = mongoose.model('adserver_transactional_details', AdServerContentSchema);
