import mongoose from 'mongoose';
import StateDetailDTO from '../../dtos/utils/state-detail.dto';

const stateDetailSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  stateCode: {
    required: false,
    type: String,
  },
  stateFullName: {
    required: true,
    type: String,
  },
  zipCodes: [{
    type: String
  }]
});

export const indiaStateDetailModel = mongoose.model<
  StateDetailDTO & mongoose.Document
>('state_details_india', stateDetailSchema, 'state_details_india');
