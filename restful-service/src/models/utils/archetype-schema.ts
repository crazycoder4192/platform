import mongoose from 'mongoose';
import ArchetypeDTO from '../../dtos/utils/archetype.dto';

const archetypeSchema = new mongoose.Schema({
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date
        },
        by: {
            required: true,
            type: String
        },
    },
    description: {
        required: false,
        type: String
    },
    hcpMinScoreRange: {
        required: false,
        type: String
    },
    hcpMaxScoreRange: {
        required: false,
        type: String
    },
    modified: {
        at: {
            type: Date
        },
        by: {
            type: String
        },
    },
    name: {
        required: false,
        type: String
    }
});

export const archetypeModel = mongoose.model<
    ArchetypeDTO & mongoose.Document
>('archetypes', archetypeSchema);
