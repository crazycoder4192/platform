import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const AdServerContentSchema = new Schema({ }, { strict: false });

export const indiaAdServerContentModel = mongoose.model('adserver_transactional_details_india',
AdServerContentSchema, 'adserver_transactional_details_india');
