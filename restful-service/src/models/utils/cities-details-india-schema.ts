import mongoose from 'mongoose';
import CityDetailDTO from '../../dtos/utils/city-detail.dto';

const cityDetailSchema = new mongoose.Schema({
  city: {
    required: false,
    type: String,
  },
  country: {
    required: false,
    type: String,
  },
  county: {
    required: false,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  decommissioned: {
    required: false,
    type: String,
  },
  latitude: {
    required: false,
    type: String,
  },
  longitude: {
    required: false,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  stateCode: {
    required: false,
    type: String,
  },
  stateFullName: {
    required: true,
    type: String,
  },
  timezone: {
    required: false,
    type: String,
  },
  zipcode: {
    required: false,
    type: String,
  },
  zipcodeType: {
    required: false,
    type: String,
  },
});

export const indiaCityDetailModel = mongoose.model<
  CityDetailDTO & mongoose.Document
>('city_state_details_india', cityDetailSchema, 'city_state_details_india');
