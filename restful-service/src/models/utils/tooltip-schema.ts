import mongoose from 'mongoose';
import TooltipDTO from '../../dtos/utils/tooltip.dto';

const tooltipschema = new mongoose.Schema({
    created: {
        by: {
            required: true,
            type: String
        },
    },
    key: {
        required: true,
        type: String
    },
    value: {
        required: true,
        type: String
    },
});

export const tooltipModel = mongoose.model<TooltipDTO & mongoose.Document>('tooltip', tooltipschema);
