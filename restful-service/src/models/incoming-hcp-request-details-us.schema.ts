import mongoose from 'mongoose';
import IncomingHcpRequestDetailsDTO from '../dtos/incoming-hcp-request-details.dto';

const incomingHcpRequestDetailsSchema = new mongoose.Schema({

    platformUid: String,
    platformId: String,
    firstName: {
        type: String,
        required: false
    },
    lastName: {
        type: String,
        required: false
    },
    specialization: {
        type: String,
        required: false
    },
    taxonomy: {
        type: String,
        required: false
    },
    zip: {
        type: String,
        required: false
    },
    npi: {
        type: String,
        required: false
    },
    location: [{ }],
    deviceDetails: [{ }],
    createdAt: {
        default: Date.now,
        required: true,
        type: Date,
    },
    updatedAt: {
        default: Date.now,
        required: true,
        type: Date,
    },
    hits: {
        type: Number,
        required: false
    },
    isMatched: Boolean,
    matchInfo: { },
    matchScore: { },
    matchedNpi: Number,
    isDeleted: Boolean
});

export const usIncomingHcpRequestDetailsModel = mongoose.model<IncomingHcpRequestDetailsDTO & mongoose.Document>(
    'incoming-hcp-request-details',
    incomingHcpRequestDetailsSchema,
    'incoming-hcp-request-details',
);
