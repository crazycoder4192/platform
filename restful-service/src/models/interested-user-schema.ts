import mongoose from 'mongoose';
import InterestedUserDTO from '../dtos/interested-user.dto';
import { invitationCodeModel } from '../models/invitation-code-schema';

const interestedUserSchema = new mongoose.Schema({
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            required: true,
            type: String,
        },
    },
    modified: {
        at: {
            default: Date.now,
            required: false,
            type: Date,
        },
        by: {
            required: false,
            type: String,
        },
    },
    email: String,
    status: String
});

export const interestedUserModel = mongoose.model<InterestedUserDTO & mongoose.Document>(
    'interested_user',
    interestedUserSchema,
    'interested_user',
);
