import mongoose from 'mongoose';
import NotificationDTO from '../dtos/notification.dto';

const notificationSchema = new mongoose.Schema({
    typeDetails: {
        type: {
            type: String,
            required: true
        },
        typeId: {
            type: String,
            required: false
        },
        routeTo: {
            type: String,
            required: false
        }
    },
    email: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        default: false
    },
    status: {
        type: String,
        required: false
    },
    delete: {
        type: Boolean,
        default: false
    },
    message: {
        type: String,
        required: true
    },
    isRead: {
        type: Boolean,
        default: false
    },
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        }
    },
    notified: {
        at: {
            type: Date,
        }
    },
    modified: {
        at: {
            type: Date,
        }
    }
});
export const notificationModel = mongoose.model<NotificationDTO & mongoose.Document>('notification', notificationSchema);
