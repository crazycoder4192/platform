import mongoose from 'mongoose';
import UserTokenDTO from '../dtos/user-token.dto';

const userTokenSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  email: String,
  expiryTime: Date,
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  status: String,
  token: { type: String, unique: true },
  tokenType: { type: String },
});

export const userTokenModel = mongoose.model<UserTokenDTO & mongoose.Document>(
  'user_tokens',
  userTokenSchema,
);
