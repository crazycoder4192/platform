import mongoose from 'mongoose';
import InvitationCodeDTO from '../dtos/invitation-code.dto';

const invitationCodeSchema = new mongoose.Schema({
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            required: true,
            type: String,
        },
    },
    used: {
        at: {
            default: Date.now,
            required: false,
            type: Date,
        },
        by: {
            required: false,
            type: String,
        },
    },
    invitation: {
        at: {
            default: Date.now,
            required: false,
            type: Date,
        },
        to: {
            required: false,
            type: String,
        },
    },
    code: String
});

export const invitationCodeModel = mongoose.model<InvitationCodeDTO & mongoose.Document>(
    'invitation_code',
    invitationCodeSchema,
    'invitation_code',
);
