import mongoose from 'mongoose';
import PasswordHistoryDTO from '../dtos/password-history.dto';

const passwordHistorySchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  email: { type: String },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  password: { required: true, type: String },
});

export const passwordHistoryModel = mongoose.model<
  PasswordHistoryDTO & mongoose.Document
>('password_history', passwordHistorySchema);
