import mongoose from 'mongoose';
import ValidatedHcpDTO from '../dtos/validated-hcp.dto';

const validatedHcpSchema = new mongoose.Schema({
    createdAt: {
        default: Date.now,
        required: true,
        type: Date,
    },
    npi: Number,
    email: String,
    firstName: String,
    gender: String,
    lastName: String,
    phone: String,
    taxonomy: [{
        type: String
    }],
    updatedAt: {
        default: Date.now,
        required: true,
        type: Date,
    },
    zip: String,
    isOnlinePresenceUpdated: Boolean,
    linkedInRecord: {
        id: String,
        username: String,
        followers: String
    },
    twitterRecord: {
        id: String,
        username: String,
        followers: String
    },
    hcpScore: {
        cumulativeScore: Number,
        attributeScore: [{
            attribute: String,
            score: Number
        }]
    }
});

export const validatedHcpModel = mongoose.model<ValidatedHcpDTO & mongoose.Document>(
    'validated-hcp',
    validatedHcpSchema,
    'validated-hcp',
);
