import mongoose from 'mongoose';
import UserDTO from '../dtos/user.dto';
import { roleModel } from '../models/admin/roles-schema';

const userSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  email: {
    match: /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
    required: true,
    trim: true,
    type: String,
    unique: true,
  },
  firstName: {
    trim: true,
    type: String,
  },
  isLocked: {
    default: 0,
    type: Number,
  },
  hasPassword: {
    type: Boolean,
    default: true
  },
  hasPersonalProfile: {
    type: Boolean,
    default: false
  },
  feedback: {
    trim: true,
    type: String,
  },
  isSubscribedForEmail: {
    type: Boolean,
    default: true
  },
  overlayShown: {
    default: false,
    type: Boolean,
  },
  hasSecurityQuestions: {
    type: Boolean,
    default: false
  },
  lastFailureTime: {
    type: Date,
  },
  lastLoginTime: {
    type: Date,
  },
  lastLogoutTime: {
    type: Date,
  },
  lastName: {
    trim: true,
    type: String,
  },
  loginFailureAttempts: {
    default: 0,
    type: Number,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  password: {
    trim: true,
    type: String,
  },
  passwordExpiryDate: {
    type: Date,
  },
  passwordSecret: {
    trim: true,
    type: String,
  },
  resetPassword: {
    default: false,
    type: Boolean,
  },
  securityAnswer1: {
    type: String,
  },
  securityAnswer2: {
    type: String,
  },
  securityQuestion1: {
    type: String,
  },
  securityQuestion2: {
    type: String,
  },
  // TODO: can we think of some other structure where role is not present in user table?
  userRole: {
    default: null,
    ref: roleModel,
    type: mongoose.Schema.Types.ObjectId,
  },
  userStatus: {
    required: true,
    trim: true,
    type: String,
  },
  userType: {
    required: true,
    trim: true,
    type: String,
  },
  reminder: {
    passwordAboutToExpire: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    },
    verificationPending: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    },
    profileCompletionPending: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    },
    noCampaign: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    },
    noPlatform: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    },
    noAsset: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    },
    oneAsset: {
      mailSentAt: {
        type: Date,
        required: false
      },
      noOfMailsSent: {
        type: Number,
        required: false
      }
    }
  }
});

export const userModel = mongoose.model<UserDTO & mongoose.Document>(
  'users',
  userSchema,
);
