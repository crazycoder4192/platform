import mongoose from 'mongoose';
import TaxonomyDTO from '../dtos/taxonomy.dto';

const taxonomySchema = new mongoose.Schema({
    createdAt: {
        default: Date.now,
        required: true,
        type: Date,
    },
    Taxonomy: String,
    Classification: String,
    Grouping: String,
    Specialization: String,
    updatedAt: {
        default: Date.now,
        required: true,
        type: Date,
    }
});

export const taxonomyIndiaStarterModel = mongoose.model<TaxonomyDTO & mongoose.Document>(
    'taxonomy_india_starter',
    taxonomySchema,
    'taxonomy_india_starter',
);
