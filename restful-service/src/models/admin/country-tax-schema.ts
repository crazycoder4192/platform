import mongoose from 'mongoose';
import CountryTaxDTO from '../../dtos/admin/country-tax.dto';

const countryTaxSchema = new mongoose.Schema({
  address: {
    addressLine1: {
      required: false,
      trim: true,
      type: String,
    },
    addressLine2: {
      required: false,
      trim: true,
      type: String,
    },
    city: {
      required: true,
      trim: true,
      type: String,
    },
    state: {
      required: true,
      trim: true,
      type: String,
    },
    zipcode: {
      required: true,
      trim: true,
      type: String,
    },
  },
  country: {
    required: true,
    trim: true,
    type: String,
    unique: true,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  percentageOfTax: {
    required: true,
    trim: true,
    type: String,
  },
  taxRegistrationNumber: {
    required: true,
    trim: true,
    type: String,
  },
  typeOfTax: {
    required: true,
    trim: true,
    type: String,
  },
});

export const countryTaxModel = mongoose.model<
  CountryTaxDTO & mongoose.Document
>('country_taxes', countryTaxSchema);
