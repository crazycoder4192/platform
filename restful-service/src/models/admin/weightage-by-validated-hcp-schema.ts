import mongoose from 'mongoose';
import WeightageByValidatedHcpDTO from '../../dtos/admin/weightage-by-validated-hcp.dto';

const weightageByValidatedHcpSchema = new mongoose.Schema({
  min: {
    type: Number,
    default: 0
  },
  max: {
    type: Number,
    default: 0
  },
  weightage: String,
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date
    },
    by: {
      required: true,
      type: String
    }
  },
  modified: {
    at: {
      type: Date
    },
    by: {
      type: String
    }
  }
});

export const weightageByValidatedHcpModel = mongoose.model<WeightageByValidatedHcpDTO & mongoose.Document>(
  'weightage_by_validated_hcps', weightageByValidatedHcpSchema
);
