// Require mongoose package
import mongoose from 'mongoose';
import RoleDTO from '../../dtos/admin/role.dto';
import { permissionModel } from './permission-schema';

const roleSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: false,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  moduleType: {
    required: false,
    type: String,
  },
  permissions: [
    {
      required: true,
      type: String,
    },
  ],
  roleName: {
    required: false,
    type: String,
  },
});

export const roleModel = mongoose.model<RoleDTO & mongoose.Document>(
  'user_role',
  roleSchema,
);
