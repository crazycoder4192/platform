import mongoose from 'mongoose';
import ConfigSchedulePaymentDTO from '../../dtos/admin/config-schedulepayments.dtos';

const configSchedulePaymentsSchema = new mongoose.Schema({
    scheduleDayofMonth: {
        required: true,
        type: String,
    },
    enableSchedule: {
        required: true,
        type: String,
    },
    created: {
        at: {
          default: Date.now,
          required: true,
          type: Date
        },
        by: {
          required: true,
          type: String
        }
    },
    modified: {
        at: {
          type: Date,
        },
        by: {
          type: String,
        },
    },
    deleted: {
        type: Boolean,
        default: false,
        required: true
    }
});

export const configSchedulePaymentsModel = mongoose.model<ConfigSchedulePaymentDTO & mongoose.Document>
('ConfigSchedulePayments', configSchedulePaymentsSchema);
