import mongoose from 'mongoose';
import PermissionsDTO from '../../dtos/admin/permission.dto';

const permissionSchema = new mongoose.Schema({
  actionName: {
    required: false,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  moduleType: {
    required: false,
    type: String,
  },
  tabName: {
    required: false,
    type: String,
  },
  identifier: {
    required: true,
    type: String,
  },
});

export const permissionModel = mongoose.model<
  PermissionsDTO & mongoose.Document
>('permissions', permissionSchema);
