import mongoose from 'mongoose';
import PublisherSiteWeightageDTO from '../../dtos/admin/publisher-site-weightage.dto';

const publisherSiteWeightageSchema = new mongoose.Schema({
  weightage: String,
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date
    },
    by: {
      required: true,
      type: String
    }
  },
  modified: {
    at: {
      type: Date
    },
    by: {
      type: String
    }
  }
});

export const publisherSiteWeightageModel = mongoose.model<PublisherSiteWeightageDTO & mongoose.Document>(
  'publisher_site_weightage', publisherSiteWeightageSchema
);
