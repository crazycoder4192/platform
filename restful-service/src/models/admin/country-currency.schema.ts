import mongoose from 'mongoose';
import CountryCurrencyDTO from '../../dtos/admin/country-currency.dto';

const countryCurrencySchema = new mongoose.Schema({
  countryName: {
    type: String,
    trim: true,
    required: true
  },
  countryISO3Code: {
    type: String,
    trim: true,
    required: true
  },
  countryISO2Code: {
    type: String,
    trim: true,
    required: true
  },
  currencyCode: {
    type: String,
    trim: true,
    required: true
  },
  currencyName: {
    type: String,
    trim: true,
    required: true
  },
  symbol: {
    type: String,
    trim: true,
    required: true
  },
  active: {
    type: Boolean,
    default: true,
    required: true
  },
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date
    },
    by: {
      required: true,
      type: String
    }
  },
  modified: {
    at: {
      type: Date
    },
    by: {
      type: String
    }
  }
});

export const countryCurrencyModel = mongoose.model<CountryCurrencyDTO & mongoose.Document>(
  'country_currencies', countryCurrencySchema
);
