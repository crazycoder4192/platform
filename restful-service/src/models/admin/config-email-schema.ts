import mongoose from 'mongoose';
import ConfigEmailDTO from '../../dtos/admin/config-email.dto';

const configEmailSchema = new mongoose.Schema({
  activestate: {
    default: false,
    required: true,
    type: Boolean,
  },
  category: {
    required: true,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  host: {
    required: true,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  pass: {
    required: true,
    type: String,
  },
  port: {
    required: true,
    type: String,
  },
  secure: String,
  user: {
    required: true,
    type: String,
  },
});

export const configEmailModel = mongoose.model<
  ConfigEmailDTO & mongoose.Document
>('ConfigEmail', configEmailSchema);
