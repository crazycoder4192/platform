import mongoose from 'mongoose';
import BannerDTO from '../../dtos/admin/banner.dto';

const bannerSchema = new mongoose.Schema({
  name: {
    required: false,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  description: {
    trim: true,
    required: true,
    type: String
  },
  size: {
    trim: true,
    required: true,
    type: String
  },
  platform: {
    trim: true,
    required: true,
    type: String
  },
  deleted: {
      type: Boolean,
      default: false,
      required : true
  }
});

export const bannerModel = mongoose.model<
BannerDTO & mongoose.Document
>('banners', bannerSchema);
