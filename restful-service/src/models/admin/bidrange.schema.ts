import mongoose from 'mongoose';
import BidRangeDTO from '../../dtos/admin/bidrange.dto';

const bidRangeSchema = new mongoose.Schema({
  country: {
    type: String,
    trim: true,
    required: true
  },
  cpc: {
    min: {
      type: Number,
      default: 0
    },
    max: {
      type: Number,
      default: 0
    }
  },
  cpm: {
    min: {
      type: Number,
      default: 0
    },
    max: {
      type: Number,
      default: 0
    }
  },
  cpv: {
    min: {
      type: Number,
      default: 0
    },
    max: {
      type: Number,
      default: 0
    }
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date
    },
    by: {
      required: true,
      type: String
    }
  },
  currencyCode: {
    type: String,
    trim: true,
    required: false
  },
  currencySymbol: {
    type: String,
    trim: true,
    required: true
  },
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  modified: {
    at: {
      type: Date
    },
    by: {
      type: String
    }
  }
});

export const bidRangeModel = mongoose.model<BidRangeDTO & mongoose.Document>(
  'bid_ranges', bidRangeSchema
);
