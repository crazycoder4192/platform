import mongoose from 'mongoose';
import CreditVoucherDTO from '../../dtos/admin/credit-voucher.dto';

const creditVoucherSchema = new mongoose.Schema({
  country: {
    type: String,
    trim: true,
    required: true
  },
  voucherName: {
    type: String,
    trim: true,
    required: true
  },
  voucherCode: {
    type: String,
    trim: true,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  expiryDate: {
    type: Date,
    required: true
  },
  active: {
    type: Boolean,
    default: true,
    required: true
  },
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date
    },
    by: {
      required: true,
      type: String
    }
  },
  modified: {
    at: {
      type: Date
    },
    by: {
      type: String
    }
  }
});

export const creditVoucherModel = mongoose.model<CreditVoucherDTO & mongoose.Document>(
  'credit_voucher', creditVoucherSchema
);
