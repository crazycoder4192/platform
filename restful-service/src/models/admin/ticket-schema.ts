import mongoose from 'mongoose';
import TicketDTO from '../../dtos/admin/ticket.dto';

const ticketSchema = new mongoose.Schema({
    ticketId: {
        required: true,
        type: Number,
    },
    email: {
        required: true,
        type: String,
    },
    orgName: {
        required: true,
        type: String
    },
    userType: {
        required: true,
        type: String,
    },
    ticketMessage: {
        required: true,
        type: String
    },
    ticketType: {
        required: true,
        type: String
    },
    status: {
        required: true,
        type: String,
        enum: ['New', 'Open', 'In Progress', 'Resolved', 'Reopen']
    },
    severity: {
        required: false,
        type: String
    },
    priority: {
        required: false,
        type: String
    },
    comments: [{
        commentTime: {
            default: Date.now,
            type: Date
        },
        commentBy: {
            required: false,
            type: String
        },
        comment: {
            required: false,
            type: String
        }
    }],
    latestAssignedUser: {
        email: {
            required: false,
            type: String
        },
        at: {
            default: Date.now,
            type: Date
        }
    },
    assignedUserHistory: [{
        email: {
            required: false,
            type: String
        },
        at: {
            default: Date.now,
            type: Date
        }
    }],
    created: {
        at: {
            default: Date.now,
            type: Date,
        },
        by: {
            required: false,
            type: String,
        },
  },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
  },
});

export const ticketModel = mongoose.model<TicketDTO & mongoose.Document>('support_ticket', ticketSchema);
