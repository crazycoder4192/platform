import mongoose from 'mongoose';
import BidRangeDTO from '../../dtos/admin/publisher-bidrange.dto';

const publisherBidRangeSchema = new mongoose.Schema({
  min: {
    type: Number,
    default: 0
  },
  max: {
    type: Number,
    default: 0
  },
  deleted: {
    type: Boolean,
    default: false,
    required: true
  },
  type: String,
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date
    },
    by: {
      required: true,
      type: String
    }
  },
  modified: {
    at: {
      type: Date
    },
    by: {
      type: String
    }
  }
});

export const publisherRangeModel = mongoose.model<BidRangeDTO & mongoose.Document>(
  'publisher_bid_ranges', publisherBidRangeSchema
);
