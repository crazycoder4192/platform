import mongoose from 'mongoose';
import PublisherDTO from '../../dtos/publisher/publisher.dto';

const publisherSchema = new mongoose.Schema({
  acceptTerms: {
    required: false,
    type: Boolean,
  },
  phoneNumber: {
    required: false,
    type: String,
  },
  isPhNumberVerified: {
    required: false,
    type: Boolean,
  },
  address: {
    addressLine1: {
      required: false,
      type: String,
    },
    addressLine2: {
      required: false,
      type: String,
    },
    city: {
      required: false,
      type: String,
    },
    state: {
      required: false,
      type: String,
    },
    type: Object,
    zipcode: {
      required: false,
      type: String,
    },
    country: {
      required: false,
      type: String,
    },
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  organizationName: {
    required: false,
    type: String,
  },
  user: {
    email: {
      required: false,
      type: String,
    },
    type: Object,
  },
  notificationSettings: {
    billingAlerts: {
        type: Boolean,
        default: true
    },
    campaignMaintainance: {
        type: Boolean,
        default: true
    },
    newsLetters: {
        type: Boolean,
        default: true
    },
    customizedHelpPerformance: {
        type: Boolean,
        default: true
    },
    disapprovedAdsPolicy: {
        type: Boolean,
        default: true
    },
    reports: {
        type: Boolean,
        default: true
    },
    specialOffers: {
        type: Boolean,
        default: true
    }
  },
});

export const publisherModel = mongoose.model<PublisherDTO & mongoose.Document>(
  'publisher',
  publisherSchema,
);
