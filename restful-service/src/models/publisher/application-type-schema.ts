import mongoose from 'mongoose';
import PublisherApplicationTypeDTO from '../../dtos/publisher/publisher-application-type.dto';

const publisherApplicationTypeSchema = new mongoose.Schema({
  appType: {
    required: true,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: true,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
});

export const publisherApplicationTypeModel = mongoose.model<
  PublisherApplicationTypeDTO & mongoose.Document
>('publisher_app_types', publisherApplicationTypeSchema);
