import mongoose from 'mongoose';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import { publisherPlatformModel } from './platform-schema';
import { publisherModel } from './publisher-schema';

const PublisherUserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: false
    },
    publisherId: {
        default: null,
        ref: publisherModel,
        type: mongoose.Schema.Types.ObjectId,
    },
    deleted: {
        type: Boolean,
        required: false,
        default: false,
    },
    platforms: [
        {
            platformId: {
                default: null,
                ref: publisherPlatformModel,
                type: mongoose.Schema.Types.ObjectId,
            },
            userRoleId: {
                type: String,
                required: true
            },
            isWatching: {
                type: Boolean,
                required: true
            },
            zone: {
              required: false,
              type: String
            }
        }
    ],
    notificationSettings: {
        billingAlerts: {
            type: Boolean,
            default: true
        },
        campaignMaintainance: {
            type: Boolean,
            default: true
        },
        newsLetters: {
            type: Boolean,
            default: true
        },
        customizedHelpPerformance: {
            type: Boolean,
            default: true
        },
        disapprovedAdsPolicy: {
            type: Boolean,
            default: true
        },
        reports: {
            type: Boolean,
            default: true
        },
        specialOffers: {
            type: Boolean,
            default: true
        }
    },
    created: {
        at: {
            default: Date.now,
            type: Date,
        },
        by: {
            required: true,
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    }
},
{
    usePushEach : true
});
export const publisherUserModel = mongoose.model<PublisherUserDTO & mongoose.Document>('publisher_user', PublisherUserSchema);
