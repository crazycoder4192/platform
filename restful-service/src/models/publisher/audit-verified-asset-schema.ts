import mongoose from 'mongoose';
import AuditVerifiedAssetDTO from '../../dtos/publisher/audit-verified-asset.dto';

const AuditVerifiedAssetSchema = new mongoose.Schema({
  assetId: {
    required: true,
    type: String,
  },
  assetDimensions: {
    required: false,
    type: String,
  },
  assetSize: {
    required: false,
    type: Number,
  },
  assetUnit: {
    required: false,
    type: String,
  },
  assetStatus: {
    required: false,
    type: String,
  },
  assetType: {
    required: false,
    type: String,
  },
  codeSnippetId: {
    required: false,
    type: String,
  },
  codeSnippet: {
    required: false,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  deleted: {
    default: false,
    required: true,
    type: Boolean,
  },
  isVerified: {
    default: false,
    required: true,
    type: Boolean,
  },
  fileFormats: {
    required: false,
    type: Array,
  },
  fullUrl: {
    required: false,
    type: String,
  },
  modified: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: false,
      type: String,
    }
  },
  name: {
    required: false,
    type: String,
  },
  platformType: {
    required: false,
    type: String,
  },
  url: {
    required: false,
    type: String,
  },
  bidRange: {
    cpc: {
      min: {
        required: true,
        type: Number,
      },
      max: {
        required: true,
        type: Number,
      }
    },
    cpm: {
      min: {
        required: true,
        type: Number,
      },
      max: {
        required: true,
        type: Number,
      }
    }
  },
  assetCreated: {
    at: { default: Date.now, required: false, type: Date },
    by: { required: false, type: String, },
  },
  assetModified: {
    at: { type: Date, required: false },
    by: { type: String, required: false }
  },
  zone: {
    required: false,
    type: String
  }
});

export const auditVerifiedAssetModel = mongoose.model<
AuditVerifiedAssetDTO & mongoose.Document
>('publisher_audit_verified_asset', AuditVerifiedAssetSchema);
