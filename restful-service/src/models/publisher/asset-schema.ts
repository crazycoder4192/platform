import mongoose from 'mongoose';
import PublisherAssetDTO from '../../dtos/publisher/asset.dto';
import { publisherPlatformModel } from './platform-schema';
import { publisherModel } from './publisher-schema';

const PublisherAssetSchema = new mongoose.Schema({
  assetDimensions: {
    required: false,
    type: String,
  },
  assetSize: {
    required: false,
    type: Number,
  },
  assetUnit: {
    required: false,
    type: String,
  },
  assetStatus: {
    required: false,
    type: String,
  },
  assetType: {
    required: false,
    type: String,
  },
  codeSnippetId: {
    required: false,
    type: String,
  },
  codeSnippet: {
    required: false,
    type: String,
  },
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  deleted: {
    default: false,
    required: true,
    type: Boolean,
  },
  isVerified: {
    default: false,
    required: true,
    type: Boolean,
  },
  fileFormats: {
    required: false,
    type: Array,
  },
  fullUrl: {
    required: false,
    type: String,
  },
  modified: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: false,
      type: String,
    }
  },
  name: {
    required: false,
    type: String,
  },
  platformType: {
    required: false,
    type: String,
  },
  platformId: {
    default: null,
    ref: publisherPlatformModel,
    type: mongoose.Schema.Types.ObjectId,
  },
  publisherId: {
    default: null,
    ref: publisherModel,
    type: mongoose.Schema.Types.ObjectId,
  },
  url: {
    required: false,
    type: String,
  },
  bidRange: {
    cpc: {
      min: {
        required: true,
        type: Number,
      },
      max: {
        required: true,
        type: Number,
      }
    },
    cpm: {
      min: {
        required: true,
        type: Number,
      },
      max: {
        required: true,
        type: Number,
      }
    }
  },
  zone: {
    required: false,
    type: String
  },
  addedSlotScriptOption: {
    required: false,
    type: String,
  },
  mobileASISnippet1: {
    required: false,
    type: String,
  },
  mobileASISnippet2: {
    required: false,
    type: String,
  },
  mobileASISnippet3: {
    required: false,
    type: String,
  },
});

export const publisherAssetModel = mongoose.model<
  PublisherAssetDTO & mongoose.Document
>('publisher_asset', PublisherAssetSchema);
