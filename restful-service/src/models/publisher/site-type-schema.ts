import mongoose from 'mongoose';
import PublisherSiteTypeDTO from '../../dtos/publisher/publisher-site-type.dto';

const publisherSiteTypeSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: true,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  siteType: {
    required: true,
    type: String,
  },
  weightage: {
    required: true,
    type: Number,
  },
});

export const publisherSiteTypeModel = mongoose.model<
  PublisherSiteTypeDTO & mongoose.Document
>('publisher_site_types', publisherSiteTypeSchema);
