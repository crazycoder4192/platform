import mongoose from 'mongoose';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import { publisherModel } from './publisher-schema';

const publisherPlatformSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: false,
    type: String,
  },
  excludeAdvertiserTypes: {
    required: false,
    type: Array,
  },
  gaAnalyticsStatus: {
    required: false,
    type: String,
  },
  gaApiKey: {
    required: false,
    type: String,
  },
  hcpOverallStatus: {
    required: false,
    type: String,
  },
  hcpScript: {
    default: '',
    required: false,
    type: String,
  },
  keywords: {
    required: false,
    type: Array,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  name: {
    required: true,
    type: String,
  },
  platformType: {
    required: false,
    type: String,
  },
  publisherId: {
    ref: publisherModel,
    required: true,
    type: mongoose.Schema.Types.ObjectId,
  },
  typeOfSite: {
    required: true,
    type: String,
  },
  domainUrl: {
    required: true,
    type: String,
  },
  webrank: {
    required: false,
    type: Number,
  },
  isDeleted: {
    required: false,
    type: Boolean,
    default: false
  },
  isDeactivated: {
    required: false,
    type: Boolean,
    default: false
  },
  isHCPUploaded: {
    type: Boolean,
    default: false
  },
  averageHcpScore: {
    required: false,
    type: Number,
  },
  isWatching: {
    type: Boolean,
    default: false
  },
  paymentSlots:
    [{
      thresholdAmount: {
        type: Number,
        default: false
      },
      percentageGiven: {
        type: Number,
        default: false
      }
    }],
  isHeadScriptAdded: {
    type: Boolean,
    default: false
  },
  isInvocationScriptAdded: {
    type: Boolean,
    default: false
  },
  zone: {
    required: false,
    type: String
  },
  trustedSites: {
    required: false,
    type: String
  },
  appType: {
    required: false,
    type: String
  },
  appTestKey: {
    required: false,
    type: String
  },
  appLiveKey: {
    required: false,
    type: String
  }
});

export const publisherPlatformModel = mongoose.model<
  PublisherPlatformDTO & mongoose.Document
>('publisher_platform', publisherPlatformSchema);
