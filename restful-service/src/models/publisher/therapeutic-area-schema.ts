import mongoose from 'mongoose';
import TherapeuticAreaDTO from '../../dtos/publisher/therapeutic-area.dto';

const therapeuticAreaSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: false,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  name: {
    required: false,
    type: String,
  },
});

export const therapeuticAreaModel = mongoose.model<
  TherapeuticAreaDTO & mongoose.Document
>('therapeutic_area', therapeuticAreaSchema);
