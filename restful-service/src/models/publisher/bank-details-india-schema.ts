import mongoose from 'mongoose';
import PublisherBankDetailDTO from '../../dtos/publisher/bank-detail.dto';
import { Encryption } from '../../util/crypto';
import { publisherPlatformModel } from './platform-schema';

const encryption = new Encryption();
const PublisherBankDetailsSchema = new mongoose.Schema(
  {
    accountHolderName: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    accountHolderAddress: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    accountHolderCIN: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    accountHolderGSTIN: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    accountNumber: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    accountType: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    bankName: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    bankCountry: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    bankAddress: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    ifsc: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },
    created: {
      at: {
        default: Date.now,
        required: true,
        type: Date,
      },
      by: {
        required: true,
        type: String,
      },
    },
    customerPaymentProfileId: {
      required: false,
      type: String,
    },
    customerProfileId: {
      required: false,
      type: String,
    },
    deleted: {
      default: false,
      required: true,
      type: Boolean,
    },
    modified: {
      at: {
        type: Date,
      },
      by: {
        type: String,
      },
    },
    platforms: [
      {
        ref: publisherPlatformModel,
        type: mongoose.Schema.Types.ObjectId,
      },
    ],
    publisherId: {
      required: false,
      type: mongoose.Schema.Types.ObjectId,
    },

    swiftCode: {
      get: encryption.decrypt,
      set: encryption.encrypt,
      type: String,
    },

    taxNumber: {
      required: false,
      type: String,
    },
  },
  {
    toJSON: { getters: true },
  },
);

export const indiaPublisherBankDetailModel = mongoose.model<
  PublisherBankDetailDTO & mongoose.Document
>('publisher_bank_detail_india', PublisherBankDetailsSchema, 'publisher_bank_details_india');
