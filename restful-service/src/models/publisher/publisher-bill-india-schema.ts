import mongoose from 'mongoose';
import BillDTO from '../../dtos/publisher/publisher-bill.dto';
import { indiaPublisherBankDetailModel } from './bank-details-india-schema';
import { publisherModel } from './publisher-schema';

const BillSchema = new mongoose.Schema({
    publisherId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: publisherModel
    },
    platformId: {
        type: String,
        required: false
    },
    items: [{
        no: { required: false, type: String },
        name: { required: false, type: String },
        dimensions: { required: false, type: String },
        duration: { required: false, type: String },
        amount: { required: false, type: String }
    }],
    billingCycle: {
        type: String,
        required: false
    },
    paymentMethodId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: indiaPublisherBankDetailModel
    },
    billNumber: {
        type: String,
        required: false
    },
    billStatus: {
        type: String,
        required: false
    },
    transactionNumber: {
        type: String,
        required: false
    },
    billDate: {
        required: true,
        type: Date,
    },
    paidDate: {
        required: false,
        type: Date,
    },
    billStartDate: {
        required: true,
        type: Date,
    },
    billEndDate: {
        required: true,
        type: Date,
    },
    amount: {
        type: Number,
        required: false
    },
    tax: {
        type: Number,
        required: false
    },
    bonus: {
        type: Number,
        required: false
    },
    cut: {
        type: Number,
        required: false
    },
    deleted: {
        type: Boolean,
        required: false,
        default: false,
    },
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    },
    quickbooks: {
        billGenerated: {
            type: Boolean,
            required: false
        }
    }
});
export const indiaBillModel = mongoose.model<BillDTO & mongoose.Document>('publisher_bills_india',
BillSchema, 'publisher_bills_india');
