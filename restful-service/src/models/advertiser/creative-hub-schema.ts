import mongoose from 'mongoose';
import CreativeHubDTO from '../../dtos/advertiser/creative-hub.dto';
import { advertiserBrandModel } from './advertiser-brand-schema';
import { advertiserModel } from './advertiser-schema';

const creativeHubSchema = new mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    creativeType: {
        required: true,
        type: String
    },
    creativeDetails: [
        {
            formatType: { required: false, type: String },
            url: { required: false, type: String },
            size: { required: false, type: String },
            resolution: { required: false, type: String },
            dimensions: { required: false, type: String },
            styleName: { required: false, type: String }
        }
    ],
    sectionName: {
        required: false,
        type: String
    },
    brandId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: advertiserBrandModel,
        required: true
    },
    advertiserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: advertiserModel,
        required: true
    },
    status: {
        required: false,
        type: String
    },
    created: {
        at: {
            type: Date
        },
        by: {
            type: String
        },
    },
    modified: {
        at: {
            type: Date
        },
        by: {
            type: String
        },
    },
    deleted: {
        default: false,
        type: Boolean
    },
});

export const creativeHubModel = mongoose.model<
    CreativeHubDTO & mongoose.Document
>('creative_hub', creativeHubSchema);
