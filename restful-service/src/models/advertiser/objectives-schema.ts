import mongoose from 'mongoose';
import ObjectivesDTO from '../../dtos/advertiser/objectives.dto';

const objectiveSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: false,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  name: {
    required: false,
    type: String,
  },
});

export const ObjectivesModel = mongoose.model<
ObjectivesDTO & mongoose.Document
>('objectives', objectiveSchema);
