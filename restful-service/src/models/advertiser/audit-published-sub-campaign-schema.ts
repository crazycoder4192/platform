import mongoose from 'mongoose';
import AuditPublishedSubCampaignDTO from '../../dtos/advertiser/audit-published-sub-campaign.dto';
import { advertiserAudienceTargetModel } from './advertiser-audience-target-schema';

const auditPublishedSubCampaignSchema = new mongoose.Schema({
    subcampaignId: { required: true, type: String },
    name: { required: true, type: String },
    objective: { required: true, type: String },
    creativeType: { required: true, type: String },
    isPublished: { required : true, type: Boolean, default: false },
    isApproved: { required : true, type: Boolean, default: false },
    status: { required: false, type: String, default: 'Under review' },
    rejectionReason: { required: false, type: String, default: '' },
    deleted: { type: Boolean, required: false, default: false },
    audienceTargetIds: [{
        default: null,
        ref: advertiserAudienceTargetModel,
        type: mongoose.Schema.Types.ObjectId,
    }],
    operationalDetails: {
        startDate: { required: false, type: Date },
        endDate: { required: false, type: Date },
        currency: { required: false, type: String },
        bidSpecifications: { required: false, type: Array },
        totalBudget: { required: false, type: String },
        bidLimits: {
            budget: { required: false, type: String },
            type: { required: false, type: String }
        }
    },
    creativeSpecifications: {
        ctaLink: { required: false, type: String },
        ctaScore: { required: false, type: Number },
        creativeDetails: [
            { formatType: { required: false, type: String },
              url: { required: false, type: String },
              weight: { required: false, type: String },
              resolution: { required: false, type: String },
              size: { required: false, type: String },
              platformType: { required: false, type: String },
              style: { required: false, type: String }
            }
        ]
    },
    last3SubcampaignScore: { required: false, type: Number },
    created: {
        at: { default: Date.now, required: true, type: Date },
        by: { required: true, type: String, },
    },
    subcampaignCreated: {
        at: { default: Date.now, required: true, type: Date },
        by: { required: true, type: String, },
    },
    subcampaignModified: {
        at: { type: Date, required: false },
        by: { type: String, required: false }
    },
    subcampaingActivated: {
        at: { type: Date, required: false },
        by: { type: String, required: false }
    },
    avgCtr: { required: false, type: Number },
    zone: {
      required: false,
      type: String
    }
});

export const auditPublishedSubCampaignModel = mongoose.model<
AuditPublishedSubCampaignDTO & mongoose.Document
>('advertiser_audit_published_sub_campaigns', auditPublishedSubCampaignSchema);
