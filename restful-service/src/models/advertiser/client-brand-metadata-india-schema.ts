import mongoose from 'mongoose';
import ClientBrandMetadataDTO from '../../dtos/advertiser/client-brand-metadata.dto';

const clientBrandMetadataSchema = new mongoose.Schema({
    client: {
        type: String,
        required: true
    },
    brands: [{
        brand: { required: false, type: String },
        therapyName: { required: false, type: String },
    }],
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: false
        }
    }
});

export const indiaClientBrandMetadataModel = mongoose.model<ClientBrandMetadataDTO
& mongoose.Document>(
    'client_brand_metadata_india', clientBrandMetadataSchema, 'client_brand_metadata_india');
