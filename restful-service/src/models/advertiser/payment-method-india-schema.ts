import mongoose from 'mongoose';
import AdvertiserPaymentMethodDTO from '../../dtos/advertiser/payment-method.dto';
import { Encryption } from '../../util/crypto';
import { userModel } from '../user-schema';
import { advertiserBrandModel } from './advertiser-brand-schema';
const encryption = new Encryption();

const PaymentMethodSchema = new mongoose.Schema({
    billingCountry: {
        type: String,
        required: false
    },
    billingCurrency: {
        type: String,
        required: false
    },
    // cardNumber: {
    //     get: encryption.decryptCardNumber,
    //     set: encryption.encrypt,
    //     type: String,
    //     required: false
    // },
    nameOnCard: {
        type: String,
        required: false
    },
    expiryDate: {
        // get: encryption.decrypt,
        // set: encryption.encrypt,
        type: String,
        required: false
    },
    brands: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: advertiserBrandModel
        }
    ],
    // amount: {
    //     type: Number,
    //     required: false
    // },
    deleted: {
        type: Boolean,
        required: false,
        default: false,
    },
    customerId: {
        type: Number,
        required: false,
    },
    customerProfileId: {
        type: Number,
        required: false,
    },
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            required: true,
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    },
    phoneNumber: {
        type: String,
        required: false
    },
    isPhNumberVerified: {
        type: Boolean,
        required: false
    },
    hasBillingAddress: {
        type: Boolean,
        required: false
    },
    address: {
        type: Object,
        addressLine1: {
            type: String,
            required: true
        },
        addressLine2: {
            type: String,
            required: false
        },
        city: {
            type: String,
            required: true
        },
        state: {
            type: String,
            required: true
        },
        zipcode:
        {
            type: String,
            required: true
        },
        country: {
            type: String,
            required: true
        },
    }
});

export const indiaPaymentMethodModel = mongoose.model<AdvertiserPaymentMethodDTO & mongoose.Document>
    ('advertiser_payment_method_india', PaymentMethodSchema, 'advertiser_payment_methods_india');
