import mongoose from 'mongoose';
import AdvertiserPaymentTransactionDTO from '../../dtos/advertiser/advertiser-payment-transactions.dto';
import { advertiserBrandModel } from './advertiser-brand-schema';
import { usPaymentMethodModel } from './payment-method-us-schema';

const PaymentTransactionSchema = new mongoose.Schema({
    invoiceNumber: {
        type: String,
        required: false
    },
    transactionId: {
        type: String,
        required: false
    },
    responseCode: {
        type: String,
        required: false
    },
    accountNumber: {
        type: String,
        required: false
    },
    accountType: {
        type: String,
        required: false
    },
    requestPayload: {
        type: String,
        required: false
    },
    responsePayload: {
        type: String,
        required: false
    },
    brands: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: advertiserBrandModel
        }
    ],
    amount: {
        type: Number,
        required: false
    },
    paymentMethodId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: usPaymentMethodModel
    },
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    }
});
export const usPaymentTransactionModel = mongoose.model
    <AdvertiserPaymentTransactionDTO & mongoose.Document>('advertiser_payment_transaction', PaymentTransactionSchema);
