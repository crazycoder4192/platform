import mongoose from 'mongoose';
import AdvertiserAgencyTypeDTO from '../../dtos/advertiser/advertiser-agency-type.dto';

const advertiserAgencyTypeSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: true,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  type: {
    required: true,
    type: String,
  },
});

export const advertiserAgencyTypeModel = mongoose.model<
AdvertiserAgencyTypeDTO & mongoose.Document
>('advertiser_agency_types', advertiserAgencyTypeSchema);
