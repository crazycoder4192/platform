import mongoose from 'mongoose';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import { userModel } from '../user-schema';

const advertiserSchema = new mongoose.Schema({
    businessName: {
        type: String,
        required: true
    },
    advertiserType: {
        type: String,
        required: true
    },
    organizationType: {
        type: String,
        required: false
    },
    acceptTerms:
    {
        type: Boolean,
        required: false
    },
    phoneNumber:
    {
        type: String,
        required: false
    },
    isPhNumberVerified:
    {
        type: Boolean,
        required: false
    },
    address: {
        type: Object,
        addressLine1: {
            type: String,
            required: true
        },
        addressLine2: {
            type: String,
            required: false
        },
        city: {
            type: String,
            required: true
        },
        state: {
            type: String,
            required: true
        },
        zipcode:
        {
            type: String,
            required: true
        },
        country: {
            type: String,
            required: true
        },
    },
    user: {
        email:
        {
            type: String,
            required: true
        }
    },
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }

    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    spendLimit:
    {
        type: Number,
        default: 1000,
    }
});

// advertiserSchema.pre('save', function (next) {
//     const currentTime = Date.now();

//     this.modified.timeAt = currentTime;
//     // Set a value for createdAt only if it is null
//     if (!this.created.timeAt) {
//         this.created.timeAt = currentTime;
//     }
//     // Call the next function in the pre-save chain
//     next();
// });

export const advertiserModel = mongoose.model<AdvertiserDTO & mongoose.Document>('advertiser', advertiserSchema);
