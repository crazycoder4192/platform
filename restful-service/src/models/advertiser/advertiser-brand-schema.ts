import mongoose from 'mongoose';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import { advertiserModel } from '../advertiser/advertiser-schema';
import { userModel } from '../user-schema';

const advertiserBrandSchema = new mongoose.Schema({
    brandName: {
        type: String,
        required: false
    },
    brandType: {
        type: String,
        required: false
    },
    clientName: {
        type: String,
        required: false
    },
    clientType:
    {
        type: String,
        required: false
    },
    advertiser:
    {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: advertiserModel
    },
    brandStatus: {
        type: Boolean,
        required: false
    },
    user: {
        type: Object,
        email:
        {
            type: String,
            required: false
        }
    },
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }

    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    spendLimit:
    {
        type: Number,
        default: 500,
    },
    zone: {
      required: false,
      type: String
    }
});

export const advertiserBrandModel = mongoose.model<AdvertiserBrandDTO & mongoose.Document>('advertiser_brands', advertiserBrandSchema);
