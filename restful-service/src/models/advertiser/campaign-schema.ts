import mongoose from 'mongoose';
import CampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import { advertiserBrandModel } from './advertiser-brand-schema';
import { advertiserModel } from './advertiser-schema';

const CampaignSchema = new mongoose.Schema({
  advertiserId: {
    default: null,
    ref: advertiserModel,
    type: mongoose.Schema.Types.ObjectId,
  },
  brandId: {
    default: null,
    ref: advertiserBrandModel,
    type: mongoose.Schema.Types.ObjectId,
  },
  name: { required: true, type: String },
  deleted: { type: Boolean, required: false, default: false },
  created: {
    at: { default: Date.now, required: true, type: Date },
    by: { required: true, type: String, },
  },
  modified: {
    at: { type: Date, required: false },
    by: { type: String, required: false }
  }
});

export const campaignModel = mongoose.model<
  CampaignDTO & mongoose.Document
>('advertiser_campaigns', CampaignSchema);
