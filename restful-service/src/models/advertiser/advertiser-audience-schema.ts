import mongoose from 'mongoose';
import AdvertiserAudienceDTO from '../../dtos/advertiser/advertiser-audience.dto';
import { advertiserAudienceTargetModel } from './advertiser-audience-target-schema';

const advertiserAudienceSchema = new mongoose.Schema({
    advertiserAudienceTargetId: {
        default: null,
        ref: advertiserAudienceTargetModel,
        type: mongoose.Schema.Types.ObjectId,
    },
    npi: [{
        type: Number,
        required: false
    }],
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }

    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    }
});

export const advertiserAudienceModel = mongoose.model<AdvertiserAudienceDTO & mongoose.Document>('advertiser_audience', advertiserAudienceSchema);
