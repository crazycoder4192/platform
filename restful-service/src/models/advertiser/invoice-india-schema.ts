import mongoose from 'mongoose';
import InvoiceDTO from '../../dtos/advertiser/invoice.dto';
import { advertiserBrandModel } from './advertiser-brand-schema';
import { indiaPaymentMethodModel } from './payment-method-india-schema';

const InvoiceSchema = new mongoose.Schema({
    paymentMethodId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: indiaPaymentMethodModel
    },
    invoiceNumber: {
        type: String,
        required: false
    },
    accountNumber: {
        type: String,
        required: false
    },
    accountType: {
        type: String,
        required: false
    },
    advertiserId: {
        type: String,
        required: false
    },
    invoiceStatus: {
        type: String,
        required: false
    },
    invoiceDate: {
        required: true,
        type: Date,
    },
    invoiceStartDate: {
        required: true,
        type: Date,
    },
    invoiceEndDate: {
        required: true,
        type: Date,
    },
    amount: {
        type: Number,
        required: false
    },
    tax: {
        type: Number,
        required: false
    },
    brandId: {
        type: String,
        required: false
    },
    brandName: {
        type: String,
        required: false
    },
    items: [{
            item: { required: false, type: String },
            campaignName: { required: false, type: String },
            startDate: { required: false, type: Date },
            endDate: { required: false, type: Date },
            amount: { required: false, type: String }
    }],
    deleted: {
        type: Boolean,
        required: false,
        default: false,
    },
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    },
    quickbooks: {
        invoiceNumber: {
            type: String,
            required: false
        },
        invoiceGenerated: {
            type: Boolean,
            required: false
        },
        paymentGenerated: {
            type: Boolean,
            required: false
        }
    }
});
export const indiaInvoiceModel = mongoose.model<InvoiceDTO & mongoose.Document>('invoice_india', InvoiceSchema, 'invoices_india');
