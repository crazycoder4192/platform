import mongoose from 'mongoose';
import AdvertiserClientTypeDTO from '../../dtos/advertiser/advertiser-client-type.dto';

const advertiserClientTypeSchema = new mongoose.Schema({
  created: {
    at: {
      default: Date.now,
      required: true,
      type: Date,
    },
    by: {
      required: true,
      type: String,
    },
  },
  description: {
    required: true,
    type: String,
  },
  modified: {
    at: {
      type: Date,
    },
    by: {
      type: String,
    },
  },
  type: {
    required: true,
    type: String,
  },
});

export const advertiserClientTypeModel = mongoose.model<
  AdvertiserClientTypeDTO & mongoose.Document
>('advertiser_client_types', advertiserClientTypeSchema);
