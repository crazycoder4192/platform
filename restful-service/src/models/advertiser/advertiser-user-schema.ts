import mongoose from 'mongoose';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import { advertiserBrandModel } from './advertiser-brand-schema';
import { advertiserModel } from './advertiser-schema';
import { creativeHubModel } from './creative-hub-schema';

const AdvertiserUserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: false
    },
    advertiserId: {
        default: null,
        ref: advertiserModel,
        type: mongoose.Schema.Types.ObjectId,
    },
    status: {
        type: String,
        required: false
    },
    deleted: {
        type: Boolean,
        required: false,
        default: false,
    },
    brands: [
        {
            brandId: {
                default: null,
                ref: advertiserBrandModel,
                type: mongoose.Schema.Types.ObjectId,
            },
            userRoleId: {
                type: String,
                required: true
            },
            isWatching: {
                type: Boolean,
                required: true
            },
            zone: {
              required: false,
              type: String
            }
        }
    ],
    creativeSections: [
        {
            name: {
                type: String,
                required: false
            },
            creatives: [
                {
                    default: null,
                    ref: creativeHubModel,
                    type: mongoose.Schema.Types.ObjectId,
                }
            ],
        }
    ],
    notificationSettings: {
        billingAlerts: {
            type: Boolean,
            default: true
        },
        campaignMaintainance: {
            type: Boolean,
            default: true
        },
        newsLetters: {
            type: Boolean,
            default: true
        },
        customizedHelpPerformance: {
            type: Boolean,
            default: true
        },
        disapprovedAdsPolicy: {
            type: Boolean,
            default: true
        },
        reports: {
            type: Boolean,
            default: true
        },
        specialOffers: {
            type: Boolean,
            default: true
        }
    },
    created: {
        at: {
            default: Date.now,
            type: Date,
        },
        by: {
            required: true,
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    }
},
{
    usePushEach : true
});
export const advertiserUserModel = mongoose.model<AdvertiserUserDTO & mongoose.Document>('advertiser_user', AdvertiserUserSchema);
