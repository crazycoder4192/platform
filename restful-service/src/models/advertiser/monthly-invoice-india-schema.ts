import mongoose from 'mongoose';
import MonthlyInvoiceDTO from '../../dtos/advertiser/monthly-invoice.dto';
import { indiaPaymentMethodModel } from './payment-method-india-schema';

const MonthlyInvoiceSchema = new mongoose.Schema({
    advertiserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: indiaPaymentMethodModel
    },
    documents: [{
        location: {
            type: String,
            required: true
        },
        type: {
            type: String,
            required: true
        },
    }],
    status: {
        type: String,
        required: true
    },
    rejectionReason: {
        type: String,
        required: false
    },
    approvedCreditLimit: {
        type: Number,
        required: false
    },
    deleted: {
        type: Boolean,
        required: false,
        default: false,
    },
    created: {
        at: {
            default: Date.now,
            required: true,
            type: Date,
        },
        by: {
            type: String,
        },
    },
    modified: {
        at: {
            type: Date,
        },
        by: {
            type: String,
        },
    },
    hasOrganizationRegistered: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    companyAddress: {
        type: String,
        required: true
    },
    streetAddress: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    zipCode: {
        type: String,
        required: true
    },
    companyContactName: {
        type: String,
        required: true
    },
    taxId: {
        type: String,
        required: true
    },
    billingContactName: {
        type: String,
        required: false
    },
    billingEmail: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: false
    },
    dunsNumber: {
        type: String,
        required: false
    },
    expectedIncome: {
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        required: true
    },
    comment: {
        type: String,
        required: true
    },
});
export const indiaMonthlyInvoiceModel = mongoose.model<MonthlyInvoiceDTO & mongoose.Document>('monthly_invoice_india', MonthlyInvoiceSchema, 'monthly_invoices_india');
