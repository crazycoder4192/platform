import mongoose from 'mongoose';
import AdvertiserAudienceTargetDTO from '../../dtos/advertiser/advertiser-audience-target.dto';
import { advertiserBrandModel } from './advertiser-brand-schema';
import { advertiserModel } from './advertiser-schema';

const advertiserAudienceTargetSchema = new mongoose.Schema({
    advertiserId: {
        default: null,
        ref: advertiserModel,
        type: mongoose.Schema.Types.ObjectId,
    },
    brandId: {
        default: null,
        ref: advertiserBrandModel,
        type: mongoose.Schema.Types.ObjectId,
    },
    name: {
        type: String,
        required: true
    },
    audienceUploaded: {
        type: Boolean,
        required: true
    },
    createAudienceTargetDetails: {
        geoLocation: {
            isInclude: { required: false, type: Boolean, default: false },
            locations: { required: false, type: Array }
        },
        demoGraphics: {
            specialization: {
                isInclude: { required: false, type: Boolean, default: false },
                areaOfSpecialization: { required: false, type: Array },
                groupOfSpecialization: { required: false, type: Array }
            },
            gender: { required: false, type: Array },
            behaviours: { required: false, type: Array }
        }
    },
    uploadAudienceTargetDetails: {
        actualFile: {
            type: String,
            required: false
        },
        curatedFile: {
            type: String,
            required: false
        }
    },
    created: {
        by: {
            type: String,
            required: true,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }

    },
    modified: {
        by: {
            type: String,
            required: false,
        },
        at: {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }
});

export const advertiserAudienceTargetModel =
mongoose.model<AdvertiserAudienceTargetDTO & mongoose.Document>('advertiser_audience_target',
advertiserAudienceTargetSchema);
