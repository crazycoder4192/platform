const amqp = require('amqplib/callback_api');
import buffer from 'buffer';
import config from 'config';
import { logger } from '../util/winston';

export class QueueHandler {

    public static getInstance(): QueueHandler {
        if (!this.instance) {
            this.instance = new QueueHandler();
            this.instance.initialize();
        }
        return this.instance;
    }

    private static instance: QueueHandler;

    public amqpConn: any;
    public channel: any;

    private fromRestExchangeName: string;
    private fromRestRequestQueueName: string;
    private readonly offlinePubQueue: any[] = [];

    private constructor() {
        // blank
    }

    public initialize() {
        const rabbitMqConnectionString: string = `${config.get<string>('rabbit-mq.url')}?heartbeat=60`;
        QueueHandler.getInstance().fromRestExchangeName = config.get<string>('rabbit-mq.from-rest.exhange');
        QueueHandler.getInstance().fromRestRequestQueueName = config.get<string>('rabbit-mq.from-rest.request-queue');

        amqp.connect(rabbitMqConnectionString, (err: any, conn: any) => {
            if (err) {
                logger.error('[AMQP]', err.message);
                return setTimeout(QueueHandler.getInstance().initialize, 1000);
            }
            conn.on('error', (err1: any) => {
                if (err1.message !== 'Connection closing') {
                    logger.error('[AMQP] conn error', err1.message);
                }
            });
            conn.on('close', () => {
                logger.error('[AMQP] reconnecting');
                return setTimeout(QueueHandler.getInstance().initialize, 1000);
            });
            logger.info('[AMQP] connected');
            QueueHandler.getInstance().amqpConn = conn;
            QueueHandler.getInstance().whenConnected();
        });
    }

    public publishMessageFromRest(data: any) {
        try {
            QueueHandler.getInstance().channel.publish(QueueHandler.getInstance().fromRestExchangeName,
                QueueHandler.getInstance().fromRestRequestQueueName,
                buffer.Buffer.from(JSON.stringify(data)), { persistent: true },
                (err: any, ok: any) => {
                    if (err) {
                        logger.error('[AMQP] publish message from rest', err);
                        QueueHandler.getInstance().channel.connection.close();
                    }
                });
        } catch (e) {
            logger.error('[AMQP] publish message from reset', e);
            QueueHandler.getInstance().offlinePubQueue.push([data]);
        }
    }

    private whenConnected() {
        QueueHandler.getInstance().startPublisher();
    }

    private startPublisher() {
        QueueHandler.getInstance().amqpConn.createConfirmChannel((err: any, ch: any) => {
            if (QueueHandler.getInstance().closeOnErr(err)) { return; }
            ch.on('error', (err1: any) => {
                logger.error('[AMQP] channel error', err1);
            });
            ch.on('close', () => {
                logger.info('[AMQP] channel closed');
            });

            QueueHandler.getInstance().channel = ch;
            while (QueueHandler.getInstance().offlinePubQueue.length > 0) {
                const d = QueueHandler.getInstance().offlinePubQueue.shift();
                QueueHandler.getInstance().publishMessageFromRest(d);
            }
        });
    }

    private closeOnErr(err: any) {
        if (!err) {
            return false;
        }
        logger.info('[AMQP] error', err);
        QueueHandler.getInstance().amqpConn.close();
        return true;
    }
}
