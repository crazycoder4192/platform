require('newrelic');
import App from './app';
require('source-map-support').install();
import { AdminController } from './controllers/admin/admin.controller';
import { AdvertiserController } from './controllers/advertiser/advertiser.controller';
import { MigrationController } from './controllers/migrate/migration.controller';
import { NotificationController } from './controllers/notification.controller';
import { PublisherController } from './controllers/publisher/publisher.controller';
import { QuickbookController } from './controllers/quick-book.controller';
import { StatusController } from './controllers/status.controller';
import { UserController } from './controllers/users';
import { AnalyticsFilterController } from './controllers/util/analytics-filter.controller';
import { SMSController } from './controllers/util/sms.controller';
import { UtilController } from './controllers/util/util-controller';
import { ZendeskController } from './controllers/zendesk.controller';
import { QueueHandler } from './queue/queue-handler';

const app = new App([
  new AdminController(),
  new UserController(),
  new PublisherController(),
  new AdvertiserController(),
  new UtilController(),
  new AnalyticsFilterController(),
  new NotificationController(),
  new QuickbookController(),
  new ZendeskController(),
  new SMSController(),
  new StatusController(),
  new MigrationController()
]);

async function invoke() {
  const queueHandler: QueueHandler = QueueHandler.getInstance();
}
invoke();

app.listen();
