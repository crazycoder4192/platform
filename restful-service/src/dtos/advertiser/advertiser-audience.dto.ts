import AdvertiserAudienceTargetDTO from './advertiser-audience-target.dto';
import AdvertiserBrandDTO from './advertiser-brand.dto';
import Advertiser from './advertiser.dto';

export default class AdvertiserAudienceDTO {
    public _id: string;
    public advertiserAudienceTargetId: AdvertiserAudienceTargetDTO;
    public npi: number[];
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
}
