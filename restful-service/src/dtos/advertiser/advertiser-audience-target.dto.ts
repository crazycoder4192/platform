import AdvertiserAudienceDTO from './advertiser-audience.dto';
import AdvertiserBrandDTO from './advertiser-brand.dto';
import Advertiser from './advertiser.dto';

export default class AdvertiserAudienceTargetDTO {
    public _id: string;
    public advertiserId: Advertiser;
    public brandId: AdvertiserBrandDTO;
    public name: string;
    public audienceUploaded: boolean;
    public createAudienceTargetDetails: {
        geoLocation: { isInclude: boolean; locations: any[] };
        demoGraphics: {
            specialization: { isInclude: boolean; areaOfSpecialization: string[]; groupOfSpecialization: any[] };
            gender: string[];
            behaviours: string[]
        };
    };
    public uploadAudienceTargetDetails: {
        actualFile: string;
        curatedFile: string;
    };
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
    public deleted: boolean;
}
