import AdvertiserAudienceTargetDTO from './advertiser-audience-target.dto';

export default class AuditPublishedSubCampaignDTO {
    public _id: string;
    public subcampaignId: string;
    public name: string;
    public objective: string;
    public creativeType: string;
    public isPublished: boolean;
    public isApproved: boolean;
    public status: string;
    public rejectionReason: string;
    public deleted: boolean;
    public audienceTargetIds: AdvertiserAudienceTargetDTO[];
    public displayTargetDetails: {
        websiteTypes: string[];
        deviceTypes: string[];
    };
    public operationalDetails: {
        startDate: Date;
        endDate: Date;
        currency: string;
        bidSpecifications: string[];
        totalBudget: string;
        bidLimits: { budget: string; type: string; }
    };
    public creativeSpecifications: {
        ctaLink: string;
        ctaScore: number;
        creativeDetails: [
            { formatType: string;
              url: string;
              weight: string;
              resolution: string;
              size: string;
              platformType: string;
              style: string;
            }
        ]
    };
    public last3SubcampaignScore: number;
    public created: { by: string; at: Date; };
    public subcampaignCreated: { by: string; at: Date; };
    public subcampaignModified: { by: string; at: Date; };
    public subcampaignActivated: { by: string; at: Date; };
    public avgCtr: number;
    public zone: string;
  }
