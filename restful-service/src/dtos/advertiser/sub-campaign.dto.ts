import AdvertiserAudienceTargetDTO from './advertiser-audience-target.dto';
import AdvertiserBrandDTO from './advertiser-brand.dto';
import Advertiser from './advertiser.dto';
import CampaignDTO from './campaign.dto';

export default class SubCampaignDTO {
    public _id: string;
    public advertiserId: Advertiser;
    public brandId: AdvertiserBrandDTO;
    public campaignId: CampaignDTO;
    public name: string;
    public objective: string;
    public creativeType: string;
    public isPublished: boolean;
    public isApproved: boolean;
    public status: string;
    public rejectionReason: string;
    public deleted: boolean;
    public audienceTargetIds: AdvertiserAudienceTargetDTO[];
    public displayTargetDetails: {
        websiteTypes: string[];
        deviceTypes: string[];
    };
    public operationalDetails: {
        startDate: Date;
        endDate: Date;
        currency: string;
        bidSpecifications: string[];
        totalBudget: string;
        bidLimits: { budget: string; type: string; }
    };
    public creativeSpecifications: {
        ctaLink: string;
        ctaScore: number;
        creativeDetails: [
            { formatType: string;
              url: string;
              weight: string;
              resolution: string;
              size: string;
              platformType: string;
              style: string;
            }
        ]
    };
    public last3SubcampaignScore: number;
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
    public activated: { by: string; at: Date; };
    public avgCtr: number;
    public zone: string;
  }
