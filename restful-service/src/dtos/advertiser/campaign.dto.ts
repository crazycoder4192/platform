import AdvertiserBrandDTO from './advertiser-brand.dto';
import Advertiser from './advertiser.dto';

export default class CampaignDTO {
    public _id: string;
    public advertiserId: Advertiser;
    public brandId: AdvertiserBrandDTO;
    public name: string;
    public deleted: boolean;
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
  }
