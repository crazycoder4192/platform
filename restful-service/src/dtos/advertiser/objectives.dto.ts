export default class ObjectivesDTO {
  public name: string;
  public created: {
    by: string;
    at: Date;
  };
  public description: string;
  public modified: {
    by: string;
    at: Date;
  };
}
