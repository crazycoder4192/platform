export default class AdvertiserUserDTO {
    public _id: any;
    public email: string;
    public advertiserId: string;
    public status: string;
    public deleted: boolean;
    public brands: Array<{
            zone: string,
            brandId: string,
            userRoleId: string;
            isWatching: boolean;
    }>;
    public notificationSettings: {
            billingAlerts: boolean;
            campaignMaintainance: boolean;
            newsLetters: boolean;
            customizedHelpPerformance: boolean;
            disapprovedAdsPolicy: boolean;
            reports: boolean;
            specialOffers: boolean;
    };
    public created: {
        at: Date;
        by: string;
    };
    public modified: {
        at: Date;
        by: string;
    };
}
