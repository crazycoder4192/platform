
export default class InvoiceDTO {
    public _id: string;
    public brandId: string;
    public brandName: string;
    public advertiserId: string;
    public paymentMethodId: string;
    public accountNumber: string;
    public accountType: string;
    public invoiceNumber: string;
    public invoiceStatus: string;
    public invoiceDate: Date;
    public invoiceStartDate: Date;
    public invoiceEndDate: Date;
    public amount: string;
    public tax: string;
    public items: any[];
    public deleted: boolean;
    public created: {
        by: string;
        at: Date;
    };
    public modified: {
        by: string;
        at: Date;
    };
    public quickbooks: {
        invoiceNumber: string;
        invoiceGenerated: boolean;
        paymentGenerated: boolean;
    };
}
