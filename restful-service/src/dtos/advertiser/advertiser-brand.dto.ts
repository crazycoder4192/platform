export default class AdvertiserBrandDTO {
    public _id: string;
    public brandName: string;
    public brandType: string;
    public clientName: string;
    public clientType: string;
    public advertiser: string;
    public loggedInEmail: string;
    public spendLimit: number;
    public zone: string;
    public user: { email: string; };
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
}
