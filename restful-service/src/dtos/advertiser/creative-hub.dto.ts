import AdvertiserBrandDTO from './advertiser-brand.dto';
import Advertiser from './advertiser.dto';

export default class CreativeHubDTO {
    public _id: string;
    public name: string;
    public creativeType: string;
    public creativeDetails: [
        {
            formatType: string;
            url: string;
            size: string;
            resolution: string;
            dimensions: string;
            styleName: string;
        }
    ];
    public sectionName: string;
    public status: string;
    public brandId: AdvertiserBrandDTO;
    public advertiserId: Advertiser;
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
    public deleted: boolean;
}
