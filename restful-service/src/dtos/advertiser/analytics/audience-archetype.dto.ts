export default class ArcheType {
    // public lb_pharmaiety: string = 'Pharmaiety';
    public pharmaiety: number = 0;
    // public lb_connectors: string = 'Connectors';
    public connectors: number = 0;
    // public lb_toolBoxers: string = 'Tool Boxers';
    public toolBoxers: number = 0;
    // public lb_curators: string = 'Curators';
    public curators: number = 0;
    public followers: number = 0;
}
