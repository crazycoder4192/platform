export default class LocationData {
    public state: string;
    public numberOfEntries: number;
    public percentage: number;
}
