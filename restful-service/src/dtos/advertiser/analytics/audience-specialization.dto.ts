export default class Specialization {
    public male: number = 0;
    public female: number = 0;
    public other: number = 0;
    public specialization: string;
}
interface KeyValuePair {
    key: string;
    value: string;
}
