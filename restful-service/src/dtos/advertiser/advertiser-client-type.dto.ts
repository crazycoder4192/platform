export default class AdvertiserClientTypeDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public description: string;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
  public type: string;
}
