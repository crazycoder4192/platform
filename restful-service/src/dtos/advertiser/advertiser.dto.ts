export default class Advertiser {
    public _id: any;
    public businessName: string;
    public advertiserType: string;
    public organizationType: string;
    public acceptTerms: string;
    public phoneNumber: string;
    public isPhNumberVerified: boolean;
    public address: {
            addressLine1: string,
            addressLine2: string;
            city: string;
            state: string;
            zipcode: string;
            country: string;
    };
    public spendLimit: number;
    public user: { email: string; };
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
}
