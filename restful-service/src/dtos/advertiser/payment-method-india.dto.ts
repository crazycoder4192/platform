export default class AdvertiserPaymentMethodDTO {
    public _id: string;
    public billingCountry: string;
    public billingCurrency: string;
    // public cardNumber: string;
    public cardType: string;
    public nameOnCard: string;
    public expiryDate: string;
    public cvv: string;
    public brands: string[];
    // public amount: string;
    public deleted: boolean;
    public customerId: number;
    public customerProfileId: number;
    public created: {
        by: string;
        at: Date;
    };
    public modified: {
        by: string;
        at: Date;
    };
}
