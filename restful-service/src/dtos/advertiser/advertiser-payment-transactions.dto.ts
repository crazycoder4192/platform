
export default class AdvertiserPaymentTransactionDTO {
    public _id: string;
    public invoiceNumber: string;
    public requestPayload: string;
    public responsePayload: string;
    public status: string;
    public transactionId: string;
    public responseCode: string;
    public accountNumber: string;
    public accountType: string;
    public brands: string[];
    public amount: string;
    public deleted: boolean;
    public paymentMethodId: string;
    public created: {
        by: string;
        at: Date;
    };
    public modified: {
        by: string;
        at: Date;
    };
}
