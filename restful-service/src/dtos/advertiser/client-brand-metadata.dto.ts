export default class ClientBrandMetadataDTO {
    public _id: string;
    public client: string;
    public brands: any[];
    public created: { by: string; at: Date; };
    public modified: { by: string; at: Date; };
}
