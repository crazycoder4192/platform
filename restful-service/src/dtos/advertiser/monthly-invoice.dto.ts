export default class MonthlyInvoiceDTO {
    public _id: string;
    public advertiserId: string;
    public advertiserDetails: any;
    public documents: [{
        location: string,
        type: string
    }];
    public status: string;
    public comment: string;
    public rejectionReason: string;
    public approvedCreditLimit: number;
    public deleted: boolean;
    public created: {
        by: string;
        at: Date;
    };
    public modified: {
        by: string;
        at: Date;
    };
    public hasOrganizationRegistered: string;
    public name: string;
    public email: string;
    public companyName: string;
    public companyAddress: string;
    public streetAddress: string;
    public country: string;
    public state: string;
    public city: string;
    public zipCode: string;
    public companyContactName: string;
    public taxId: string;
    public billingContactName: string;
    public billingEmail: string;
    public phoneNumber: string;
    public dunsNumber: string;
    public expectedIncome: string;
    public startDate: Date;
}
