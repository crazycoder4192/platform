export default class AnalyticsDTO {
    public audienceGenderExperience: any;
    public audienceSpecialization: any;
    public audienceArcheType: any;
    public audienceLocation: any;
    public audienceFrequency: any;

    public behaviorTimeOfDayEnagement: any;
    public behaviorDisplayLocation: any;
    public behaviorDevices: any;

    public conversionEnagement: any;
    public conversionResult: any;

    public campaignsTopCampaigns: any;
    public campaignsActiveCampaigns: any;

    public startDate: any;
    public endDate: any;
}
