export default class TaxonomyDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public taxonomy: number;
    public classification: Date;
    public grouping: string;
    public specialization: string;
    public createdAt: Date;
    public updatedAt: Date;
}
