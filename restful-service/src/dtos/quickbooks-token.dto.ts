export default class QuickbooksTokenDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public accessToken: string;
    public refreshToken: string;
    public createdAt: Date;
}
