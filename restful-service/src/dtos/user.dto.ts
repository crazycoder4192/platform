import RoleDTO from '../dtos/admin/role.dto';
export default class UserDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public created: {
    by: string;
    at: Date;
  };
  public email: string;
  public firstName: string;
  public isLocked: number;
  public hasPassword: boolean;
  public hasPersonalProfile: boolean;
  public feedback: string;
  public isSubscribedForEmail: boolean;
  public overlayShown: boolean;
  public hasSecurityQuestions: boolean;
  public lastFailureTime: Date;
  public lastLoginTime: Date;
  public lastLogoutTime: Date;
  public lastName: string;
  public loginFailureAttempts: number;
  public modified: {
    by: string;
    at: Date;
  };
  public password: string;
  public passwordExpiryDate: Date;
  public passwordSecret: string;
  public resetPassword: boolean;
  public securityAnswer1: string;
  public securityAnswer2: string;
  public securityQuestion1: string;
  public securityQuestion2: string;
  public userRole: RoleDTO;
  public userStatus: string;
  public userType: string;
  public reminder: {
    passwordAboutToExpire: {
      mailSentAt: Date;
      noOfMailsSent: number;
    },
    verificationPending: {
      mailSentAt: Date;
      noOfMailsSent: number;
    },
    profileCompletionPending: {
      mailSentAt: Date;
      noOfMailsSent: number;
    },
    noCampaign: {
      mailSentAt: Date;
      noOfMailsSent: number;
    },
    noPlatform: {
      mailSentAt: Date;
      noOfMailsSent: number;
    },
    noAsset: {
      mailSentAt: Date;
      noOfMailsSent: number;
    },
    oneAsset: {
      mailSentAt: Date;
      noOfMailsSent: number;
    }
  };
}
