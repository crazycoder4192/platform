export default class InvitationCodeDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public code: string;
    public created: {
        by: string;
        at: Date;
    };
    public invitation: {
        to: string;
        at: Date;
    };
    public used: {
        by: string;
        at: Date;
    };
}
