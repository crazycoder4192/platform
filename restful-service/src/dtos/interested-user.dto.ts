export default class InteresetedUserDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public email: string;
    public created: {
        by: string;
        at: Date;
    };
    public status: string;
    public modified: {
        at: Date;
        by: string;
    };
}
