export default class IncomingHcpRequestDetailsDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public platformUid: string;
    public platformId: string;
    public firstName: string;
    public lastName: string;
    public taxonomy: string;
    public specialization: string;
    public zip: string;
    public npi: string;
    public city: string;
    public location: any[];
    public deviceDetails: any[];

    public createdAt: Date;
    public updatedAt: Date;
    public hits: number;

    public isMatched: boolean;
    public matchInfo: any;
    public matchScore: any;
    public matchedNpi: number;
}
