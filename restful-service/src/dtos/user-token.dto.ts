export default class UserTokenDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public created: {
    by: string;
    at: Date;
  };
  public email: string;
  public expiryTime: Date;
  public modified: {
    by: string;
    at: Date;
  };
  public status: string;
  public token: string;
  public tokenType: string;
}
