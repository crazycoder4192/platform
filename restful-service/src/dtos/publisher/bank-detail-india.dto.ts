export default class PublisherBankDetailDTO {
  public _id: string;
  public bankName: string;
  public bankCountry: string;
  public accountHolderName: string;
  public accountNumber: string;
  public accountType: string;
  public swiftCode: string;
  public taxNumber: string;
  public publisherId: string;
  public platforms: string[];
  public customerProfileId: string;
  public customerPaymentProfileId: string;
  public deleted: boolean;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
}
