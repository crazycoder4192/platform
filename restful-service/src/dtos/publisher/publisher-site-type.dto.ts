export default class PublisherSiteTypeDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public created: {
    by: string;
    at: Date;
  };
  public description: string;
  public modified: {
    by: string;
    at: Date;
  };
  public siteType: string;
  public weightage: number;
}
