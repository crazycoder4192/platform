
export default class PublisherAnalyticsDTO {
    public overviewTopAdAssets: any;
    public overviewAssetTypeAndUtilization: any;
    public overviewValidatedHcps: any;
    public audienceGenderExperience: any;
    public audienceSpecialization: any;
    public audienceLocation: any;
    public audienceArcheType: any;
    public audienceAssetType: any;
    public behaviorTimeOfDay: any;
    public behaviorDevices: any;
    public conversionEnagement: any;
    public conversionResults: any;
    public startDate: any;
    public endDate: any;
}
