
export default class AuditVerifiedAssetDTO {
    public _id: string;
    public assetId: string;
    public assetDimensions: string;
    public assetSize: number;
    public assetUnit: string;
    public assetStatus: string;
    public assetType: string;
    public codeSnippetId: string;
    public codeSnippet: string;
    public created: {
        by: string;
        at: Date;
    };
    public deleted: boolean;
    public isVerified: boolean;
    public fileFormats: string[];
    public fullUrl: string;

    public name: string;
    public platformType: string;
    public url: string;
    public bidRange: {
        cpc: {
            min: number;
            max: number;
        },
        cpm: {
            min: number;
            max: number;
        }
    };
    public assetCreated: { by: string; at: Date; };
    public assetModified: { by: string; at: Date; };
    public zone: string;
    public addedSlotScriptOption: string;
}
