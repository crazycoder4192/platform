export default class BillDTO {
    public _id: string;
    public paymentMethodId: string;
    public publisherId: any;
    public platformId: string;
    public billNumber: string;
    public transactionNumber: string;
    public billStatus: string;
    public billDate: Date;
    public paidDate?: Date;
    public billStartDate: Date;
    public billEndDate: Date;
    public amount: number;
    public bonus: number;
    public cut: number;
    public tax: number;
    public items: string[];
    public billingCycle: string;
    public deleted: boolean;
    public created: {
        by: string;
        at: Date;
    };
    public modified: {
        by: string;
        at: Date;
    };
    public quickbooks: {
        billGenerated: boolean;
    };
}
