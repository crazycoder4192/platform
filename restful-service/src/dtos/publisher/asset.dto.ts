import PublisherPlatformDTO from './publisher-platform.dto';
import PublisherDTO from './publisher.dto';

export default class PublisherAssetDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public assetDimensions: string;
  public assetSize: number;
  public assetUnit: string;
  public assetStatus: string;
  public assetType: string;
  public codeSnippetId: string;
  public codeSnippet: string;
  public created: {
    by: string;
    at: Date;
  };
  public deleted: boolean;
  public isVerified: boolean;
  public fileFormats: string[];
  public fullUrl: string;
  public modified: {
    by: string;
    at: Date;
  };
  public name: string;
  public platformType: string;
  public platformId: PublisherPlatformDTO;
  public publisherId: PublisherDTO;
  public url: string;
  public bidRange: {
    cpc: {
      min: number;
      max: number;
    },
    cpm: {
      min: number;
      max: number;
    }
  };
  public zone: string;
  public addedSlotScriptOption: string;

  public mobileASISnippet1: string;
  public mobileASISnippet2: string;
  public mobileASISnippet3: string;
}
