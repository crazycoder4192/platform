export default class PublisherApplicationTypeDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public appType: string;
  public created: {
    by: string;
    at: Date;
  };
  public description: string;
  public modified: {
    by: string;
    at: Date;
  };
}
