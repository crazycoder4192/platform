import PublisherDTO from '../publisher/publisher.dto';

export default class PublisherPlatformDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public name: string;
  public domainUrl: string;
  public platformType: string;
  public typeOfSite: string;
  public description: string;
  public keywords: string[];
  public excludeAdvertiserTypes: string[];
  public gaApiKey: string;
  public gaAnalyticsStatus: string;
  public hcpOverallStatus: string;
  public publisherId: PublisherDTO;
  public webrank: number;
  public isDeleted: boolean;
  public isDeactivated: boolean;
  public isHCPUploaded: boolean;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
  public isWatching: boolean;
  public paymentSlots: PublisherPaymentSlot[];
  public hcpScript: string;
  public isHeadScriptAdded: boolean;
  public isInvocationScriptAdded: boolean;
  public zone: string;
  public trustedSites: string;
  public appType: string;
  public appTestKey: string;
  public appLiveKey: string;
}

export class PublisherPaymentSlot {
  public thresholdAmount: number;
  public percentageGiven: number;
}
