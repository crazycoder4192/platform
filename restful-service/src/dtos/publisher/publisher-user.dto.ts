export default class PublisherUserDTO {
    public _id: any;
    public email: string;
    public publisherId: string;
    public deleted: boolean;
    public platforms: Array<{
        zone: string,
        platformId: string,
        userRoleId: string;
        isWatching: boolean;
    }>;
    public notificationSettings: {
            billingAlerts: boolean;
            campaignMaintainance: boolean;
            newsLetters: boolean;
            customizedHelpPerformance: boolean;
            disapprovedAdsPolicy: boolean;
            reports: boolean;
            specialOffers: boolean;
    };
    public created: {
        at: Date;
        by: string;
    };
    public modified: {
        at: Date;
        by: string;
    };
}
