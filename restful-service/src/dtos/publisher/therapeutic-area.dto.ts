export default class TherapeuticAreaDTO {
  public name: string;
  public created: {
    by: string;
    at: Date;
  };
  public description: string;
  public modified: {
    by: string;
    at: Date;
  };
}
