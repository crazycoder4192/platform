export default class Publisher {
  /*tslint:disable-next-line*/
  public _id: string;
  public acceptTerms: boolean;
  public address: {
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    zipcode: string;
    country: string;
  };
  public phoneNumber: string;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
  public organizationName: string;
  public user: {
    email: string;
  };
  public isPhNumberVerified: boolean;
  public notificationSettings: {
    billingAlerts: boolean;
    campaignMaintainance: boolean;
    newsLetters: boolean;
    customizedHelpPerformance: boolean;
    disapprovedAdsPolicy: boolean;
    reports: boolean;
    specialOffers: boolean;
  };
}
