export default class ConfigSchedulePaymentDTO {
    public _id: string;
    public scheduleDayofMonth: string;
    public enableSchedule: string;
    public created: {
      by: string;
      at: Date;
    };
    public modified: {
      by: string;
      at: Date;
    };
    public deleted: boolean;
}
