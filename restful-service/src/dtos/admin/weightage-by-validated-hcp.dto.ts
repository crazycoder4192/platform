export default class WeightageByValidatedHcpDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public weightage: string;
    public min: number;
    public max: number;
    public deleted: boolean;
    public created: {
      by: string;
      at: Date;
    };
    public modified: {
      by: string;
      at: Date;
    };
  }
