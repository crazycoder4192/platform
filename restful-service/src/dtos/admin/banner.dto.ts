export default class BannerDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public name: string;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
  public description: string;
  public size: string;
  public platform: string;
  public deleted: boolean;
}
