export default class CountryTaxDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public address: {
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    zipcode: string;
  };
  public country: string;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
  public percentageOfTax: string;
  public taxRegistrationNumber: string;
  public typeOfTax: string;
  public deleted: boolean;
}
