export default class CountryCurrencyDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public countryName: string;
  public countryISO3Code: string;
  public countryISO2Code: string;
  public currencyCode: string;
  public currencyName: string;
  public symbol: string;
  public deleted: boolean;
  public active: boolean;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
}
