export default class PermissionDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public moduleType: string;
  public tabName: string;
  public actionName: string;
  public identifier: string;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
}
