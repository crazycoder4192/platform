export default class AdminAdvertiserAnalyticsDTO {
    public brandAndTherapeuticData: any[];
    public topBrands: any[];
    public topSubCampaigns: any[];
    public typeOfAds: any[];
    public trends: any[];
  }
