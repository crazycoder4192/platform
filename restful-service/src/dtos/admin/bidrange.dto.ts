export default class BidRangeDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public country: string;
    public currencyCode: string;
    public currencySymbol: string;
    public cpm: {
      min: number;
      max: number;
    };
    public cpc: {
        min: number;
        max: number;
    };
    public cpv: {
        min: number;
        max: number;
    };
    public created: {
      by: string;
      at: Date;
    };
    public modified: {
      by: string;
      at: Date;
    };
    public deleted: boolean;
  }
