import PermissionDTO from '../admin/permission.dto';

export default class RoleDTO {
  public _id: string;
  public moduleType: string;
  public roleName: string;
  public description: string;
  public permissions: string[];
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
}
