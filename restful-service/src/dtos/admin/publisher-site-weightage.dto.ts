export default class PublisherSiteWeightageDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public weightage: string;
    public deleted: boolean;
    public created: {
      by: string;
      at: Date;
    };
    public modified: {
      by: string;
      at: Date;
    };
  }
