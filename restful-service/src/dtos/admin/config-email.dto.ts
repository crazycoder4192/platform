export default class ConfigEmailDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public activestate: boolean;
  public category: string;
  public created: {
    by: string;
    at: Date;
  };
  public host: string;
  public modified: {
    by: string;
    at: Date;
  };
  public pass: string;
  public port: string;
  public secure: string;
  public user: string;
}
