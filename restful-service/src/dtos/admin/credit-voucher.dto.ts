export default class CreditVoucherDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public country: string;
  public voucherName: string;
  public voucherCode: string;
  public amount: string;
  public expiryDate: string;
  public deleted: boolean;
  public active: boolean;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
}
