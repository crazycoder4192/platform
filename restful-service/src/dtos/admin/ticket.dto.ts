export default class TicketDTO {
    public _id: string;
    public ticketId: number;
    public email: string;
    public orgName: string;
    public userType: string;
    public ticketMessage: string;
    public ticketType: string;
    public status: string;
    public severity: string;
    public priority: string;
    public comments: [{
      commentTime: string;
      commentBy: string;
      comment: string;
    }];
    public latestAssignedUser: {
      email: string;
      at: Date;
    };
    public assignedUserHistory: [{
      email: string;
      at: Date;
    }];
    public created: {
      by: string;
      at: Date;
    };
    public modified: {
      by: string;
      at: Date;
    };
  }
