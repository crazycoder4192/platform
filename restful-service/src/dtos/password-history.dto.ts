export default class PasswordHistoryDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public created: {
    by: string;
    at: Date;
  };
  public email: string;
  public modified: {
    by: string;
    at: Date;
  };
  public password: string;
}
