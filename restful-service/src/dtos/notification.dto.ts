export default class NotificationDTO {
    public _id: string;
    public typeDetails: { type: string; typeId: string; routeTo: string};
    public icon: string;
    public isRead: boolean;
    public delete: boolean;
    public status: string;
    public message: string;
    public email: string;
    public created: { at: Date; };
    public modified: { at: Date; };
    public notified: { at: Date; };
}
