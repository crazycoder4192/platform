export default class ValidatedHcpDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public npi: number;
    public createdAt: Date;
    public email: string;
    public firstName: string;
    public gender: string;
    public lastName: string;
    public phone: string;
    public taxonomy: string[];
    public updatedAt: Date;
    public zip: string;
    public isOnlinePresenceUpdated: number;
    public linkedInRecord: {
        id: string;
        username: string;
        followers: string;
    };
    public twitterRecord: {
        id: string;
        username: string;
        followers: string;
    };
    public hcpScore: {
        cumulativeScore: number;
        attributeScore: [{
            attribute: string;
            score: number;
        }]
    };
}
