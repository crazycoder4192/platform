export default class ArchetypeDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public name: string;
  public description: string;
  public hcpMinScoreRange: string;
  public hcpMaxScoreRange: string;
  public created: {
    by: string;
    at: Date;
  };
  public modified: {
    by: string;
    at: Date;
  };
}
