export default class FilterAnalyticsDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public startDate: Date;
    public endDate: Date;
    public specialization: string[];
    public gender: string[];
    public location: string[];
    public user: string;
    public filterName: string;
    public subCampaignIds: any[];
    public assetSnippetIds: string[];
    public platformId: string;
    public brandId: object;
    public advertiserId: object;
    public publisherId: object;
    public created: {
      by: string;
      at: Date;
    };
    public modified: {
      by: string;
      at: Date;
    };
    public isHcpFilterSelected: boolean;
}
