export default class CityDetailDTO {
  /*tslint:disable-next-line*/
  public _id: string;
  public city: string;
  public country: string;
  public county: string;
  public created: {
    by: string;
    at: Date;
  };
  public decommissioned: string;
  public latitude: string;
  public longitude: string;
  public modified: {
    by: string;
    at: Date;
  };
  public stateCode: string;
  public stateFullName: string;
  public timezone: string;
  public zipcode: string;
  public zipcodeType: string;
}
