export default class StateDetailDTO {
    /*tslint:disable-next-line*/
    public _id: string;
    public created: {
      by: string;
      at: Date;
    };
    public stateCode: string;
    public stateFullName: string;
    public zipCodes: string[];
}
