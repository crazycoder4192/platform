export default class TooltipDTO {

  public key: string;
  public value: string;
  public created: {
    by: string;
  };
}
