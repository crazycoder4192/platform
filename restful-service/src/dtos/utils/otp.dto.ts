export default class OtpDTO {
    public _id: string;
    public otp: number;
    public email: string;
    public expiryTime: Date;
    public otpStatus: string;
    public purpose: string;
    public gateWay: string;
    public otpChannel: string;
    public created: {
        by: string;
        at: Date;
    };
    public modified: {
        by: string;
        at: Date;
    };
}
