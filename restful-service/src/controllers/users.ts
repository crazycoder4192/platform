import bcrypt from 'bcryptjs';
import config from 'config';
import express from 'express';
import jwt from 'jsonwebtoken';
import moment from 'moment';
import { logger } from '../../src/util/winston';
import IController from '../common/controller-interface';
import { Validation } from '../common/validations';
import AdvertiserUserDAO from '../daos/advertiser/advertiser-user.dao';
import InterestedUserDAO from '../daos/interested-user.dao';
import InvitationCodeDAO from '../daos/invitation-code.dao';
import PasswordHistoryDAO from '../daos/password-history.dao';
import PublisherUserDAO from '../daos/publisher/publisher-user.dao';
import PublisherDAO from '../daos/publisher/publisher.dao';
import UserTokenDAO from '../daos/user-token.dao';
import UserDAO from '../daos/user.dao';
import AdvertiserUserDTO from '../dtos/advertiser/advertiser-user.dto';
import InterestedUserDTO from '../dtos/interested-user.dto';
import InvitationCodeDTO from '../dtos/invitation-code.dto';
import PasswordHistoryDTO from '../dtos/password-history.dto';
import PublisherUserDTO from '../dtos/publisher/publisher-user.dto';
import PublisherDTO from '../dtos/publisher/publisher.dto';
import UserTokenDTO from '../dtos/user-token.dto';
import UserDTO from '../dtos/user.dto';
import catchError from '../error/catch-error';
import HandledApplicationError from '../error/handled-application-error';
import throwIfInputValidationError from '../error/input-validation-error';
import IAuthenticatedRequest from '../guards/authenticated.request';
import authenticationGuard from '../guards/authentication.guard';
import { NotificationIcon, NotificationRoute, NotificationType, UserStatuses, UserTypes } from '../util/constants';
import { Encryption } from '../util/crypto';
import { HashPwd } from '../util/hash-pwd';
import { Mail } from '../util/mail';
import { GeneralMail } from '../util/mail/general-mail';
import { NotificationHelper } from '../util/notification-helper';
import { AuthorizeAdmin } from './admin/authorize-admin';

export class UserController implements IController {
  public path = '/user';
  public router = express.Router();

  private readonly hashPwd: HashPwd;
  private readonly generalMail: GeneralMail;
  private readonly userDAO: UserDAO;
  private readonly userTokenDAO: UserTokenDAO;
  private readonly passwordHistoryDAO: PasswordHistoryDAO;
  private readonly advertiserUserDAO: AdvertiserUserDAO;
  private readonly publisherUserDAO: PublisherUserDAO;
  private readonly publisherDAO: PublisherDAO;
  private readonly invitationCodeDAO: InvitationCodeDAO;
  private readonly interestedUserDAO: InterestedUserDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly encryption: Encryption;
  private readonly notificationHelper: NotificationHelper;

  constructor() {
    this.hashPwd = new HashPwd();
    this.generalMail = new GeneralMail();
    this.userDAO = new UserDAO();
    this.userTokenDAO = new UserTokenDAO();
    this.passwordHistoryDAO = new PasswordHistoryDAO();
    this.advertiserUserDAO = new AdvertiserUserDAO();
    this.publisherDAO = new PublisherDAO();
    this.publisherUserDAO = new PublisherUserDAO();
    this.invitationCodeDAO = new InvitationCodeDAO();
    this.interestedUserDAO = new InterestedUserDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
    this.encryption = new Encryption();
    this.notificationHelper = new NotificationHelper();
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/register`, this.register);
    this.router.post(`${this.path}/regeneratetoken`, this.reGenerateToken);
    this.router.post(`${this.path}/forgotpassword`, this.forgotPassword);
    this.router.get(`${this.path}/reset`, this.resetPassword);
    this.router.get(`${this.path}/activateuser`, this.activateUser);
    this.router.get(`${this.path}/verifytoken`, this.verifyToken);
    this.router.post(`${this.path}/password_expiry_reset`, this.passwordExpiryReset);
    this.router.post(`${this.path}/updatePassword`, this.updatePassword);
    this.router.post(`${this.path}/updateAddUserPassword`, this.updateAddUserPassword);
    this.router.post(`${this.path}/changepassword`, authenticationGuard, this.changePassword);
    this.router.post(`${this.path}/savequestions`, authenticationGuard, this.saveQuestions);
    this.router.post(`${this.path}/login`, this.login);
    this.router.get(`${this.path}/isLoggedIn`, authenticationGuard, this.isLoggedIn);
    this.router.get(`${this.path}/logout`, authenticationGuard, this.logout);
    this.router.post(`${this.path}/email`, authenticationGuard, this.getDetailsFromEmail);
    this.router.post(`${this.path}/extendToken`, authenticationGuard, this.extendToken);
    this.router.put(`${this.path}/updatefirstlastname`, authenticationGuard, this.updatefirstlastname);
    this.router.put(`${this.path}/updateUser`, this.updateUser);
    this.router.post(`${this.path}/checkpassword`, authenticationGuard, this.checkUserPassword);
    this.router.delete(`${this.path}/delete`, authenticationGuard, this.deleteUser);
    this.router.put(`${this.path}/notificationsettings`, authenticationGuard, this.notificationSettings);
    this.router.get(`${this.path}/reminder/verificationpending`, authenticationGuard, this.verificationPending);
    this.router.get(`${this.path}/reminder/nearingpasswordexpiry`, authenticationGuard, this.nearingPasswordExpiry);
    this.router.post(`${this.path}/cron`, this.encryptedLogin);
    this.router.post(`${this.path}/addinvitationcodes`, authenticationGuard, this.addInvitationCodes);
    this.router.post(`${this.path}/validateinvitationcode`, this.validateInvitationCode);
    this.router.post(`${this.path}/addinteresteduser`, this.addInterestedUser);
    this.router.get(`${this.path}/my/invites`, authenticationGuard, this.getMyInvites);
    this.router.post(`${this.path}/send/invitation`, authenticationGuard, this.sendInvite);
    this.router.get(`${this.path}/unsubscribeUser`, this.unsubscribeUser);
    this.router.get(`${this.path}/submittedFeedback`, this.submittedFeedback);
  }

  private readonly addInvitationCodes = async (req: IAuthenticatedRequest,
                                               res: express.Response, next: express.NextFunction) => {
    await this.authorizeAdmin.authorize(req.email);
    try {
      const newInvitationCodes: string[] = [];
      for (let i = 0; i < 5; i++) {
        const newInvitationCodeDTO: InvitationCodeDTO = new InvitationCodeDTO();
        const code = this.generateCode(6);
        newInvitationCodes.push(code);
        newInvitationCodeDTO.code = code;
        newInvitationCodeDTO.created = { at: new Date(), by: req.email };
        await this.invitationCodeDAO.create(newInvitationCodeDTO);
      }
      res.status(200).json({ newInvitationCodes, success: true, status: 201 });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly validateInvitationCode = async (req: IAuthenticatedRequest,
                                                   res: express.Response, next: express.NextFunction) => {
    try {
      const invitationCodeDTO = await this.invitationCodeDAO.findByCode(req.body.invitationCode);
      if (invitationCodeDTO) {
        if (invitationCodeDTO.used && invitationCodeDTO.used.by) {
          const user: UserDTO = await this.userDAO.findByEmail(invitationCodeDTO.used.by);
          if (!user || user.userStatus !== UserStatuses.active) {
            // allow the invitation code to be reused
          } else {
            throw new HandledApplicationError(409, 'Invitation code is already used.');
          }
        }
      } else {
        throw new HandledApplicationError(409, 'Invalid invitation code');
      }
      res.status(200).json({ success: true, status: 200 });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly addInterestedUser = async (req: IAuthenticatedRequest,
                                              res: express.Response, next: express.NextFunction) => {
    try {
      let interestedUserDTO: InterestedUserDTO = await this.interestedUserDAO.findByEmail(req.body.interestedUser);
      if (!interestedUserDTO) {
        interestedUserDTO = new InterestedUserDTO();
        interestedUserDTO.email = req.body.interestedUser;
        interestedUserDTO.created = { at: new Date(), by: req.body.interestedUser };
        interestedUserDTO.status = 'NoActionTaken';
        await this.interestedUserDAO.create(interestedUserDTO);
      }
      res.status(200).json({ success: true });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getMyInvites = async (req: IAuthenticatedRequest,
                                         res: express.Response, next: express.NextFunction) => {
      try {
        const invitationCodeDTOs: InvitationCodeDTO[] = await this.invitationCodeDAO.findByCreatedBy(req.email);
        const results: any[] = [];
        for (const i of invitationCodeDTOs) {
          const r: any = { };

          r.invited = false;
          r.invitedUser = '';

          if (i.invitation && i.invitation.to) {
            r.invitedUser = i.invitation.to;
            r.invited = true;
          }
          results.push(r);
        }
        res.status(200).json({ success: true, results });
      } catch (err) {
        catchError(err, next);
      }
  }

  private readonly sendInvite = async (req: IAuthenticatedRequest,
                                       res: express.Response, next: express.NextFunction) => {
      try {
        const invitationCodeDTOs: InvitationCodeDTO[] = await this.invitationCodeDAO.findByCreatedBy(req.email);
        let availableInvitationCodeDTO: InvitationCodeDTO = null;
        for (const i of invitationCodeDTOs) {
          if (!i.invitation || !i.invitation.to) {
            availableInvitationCodeDTO = i;
            break;
          }
        }

        if (availableInvitationCodeDTO) {
          const sendMail: boolean = await this.sendInvitationMail(req.email, req.body.email, availableInvitationCodeDTO.code);
          if (sendMail) {
            availableInvitationCodeDTO.invitation = { to: req.body.email, at: new Date() };
            this.invitationCodeDAO.update(availableInvitationCodeDTO._id, availableInvitationCodeDTO);
            res.status(200).json({ success: true, message: 'Invitation successfully sent' });
          } else {
            res.status(201).json({
              success: false,
              message: 'Issue encountered while sending email, please check the email & try again after some time.'
            });
          }
        } else {
          res.status(201).json({
            success: false,
            message: 'No more invite is available.'
          });
        }
      } catch (err) {
        catchError(err, next);
      }
  }

  private readonly register = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      // Step:1 -- Validate input request
      const validation = new Validation();
      await validation.validateRegisterUser(req);

      // Step:2 -- Verify email already registered or not
      const existingUser: UserDTO = await this.userDAO.findByEmail(req.body.email);
      if (existingUser && existingUser.userStatus === UserStatuses.active) {
        throw new HandledApplicationError(409, 'A user with that email has already registered. Please use a different email');
      }

      // Step:3 -- Verify email domain valid or not
      /*const info: IVerifiedInfo = await this.verifyEmailDomain(req.body.email);
      if (!info.success) {
        throw new HandledApplicationError(422, 'The email entered do not belong to valid domain');
      }*/

      const isInvitationOnly = (config.get<string>('invite-only').toLowerCase() === 'true');
      let invitationCodeDTO: InvitationCodeDTO;
      if (isInvitationOnly) {
        if (!req.body.invitationCode) {
          throw new HandledApplicationError(409, 'Registration is only enabled via invitation');
        }
        invitationCodeDTO = await this.invitationCodeDAO.findByCode(req.body.invitationCode);
        if (invitationCodeDTO) {
          if (invitationCodeDTO.used && invitationCodeDTO.used.by) {
            if (existingUser && existingUser.userStatus !== UserStatuses.active &&
              invitationCodeDTO.used.by === req.body.email) {
                // allow
              } else {
                throw new HandledApplicationError(409, 'Invitation code is already used');
              }
          }
        } else {
          throw new HandledApplicationError(409, 'Invalid invitation code');
        }
      }

      // Step:4 -- Generate random key and hash password
      const randomString = require('random-string');
      const passwordSecretKey = randomString({
        length: 16,
        numeric: true,
        letters: true,
        special: false
      });
      const hashedPassword: string = await this.hashPwd.generateHash(passwordSecretKey, req.body.password);

      // Step:5 -- Convert to User DTO and Create User in DB
      let userDetail = this.toUserDTO(req);
      userDetail.password = hashedPassword;
      userDetail.passwordSecret = passwordSecretKey;
      if (existingUser) {
        userDetail = await this.userDAO.update(existingUser._id, userDetail);
      } else {
        userDetail = await this.userDAO.create(userDetail);
      }

      // Step:6 -- Add Password to History
      const passwordHistoryData: PasswordHistoryDTO = new PasswordHistoryDTO();
      passwordHistoryData.email = req.body.email;
      passwordHistoryData.password = hashedPassword;
      passwordHistoryData.created = { at: new Date(), by: req.body.email };
      await this.passwordHistoryDAO.create(passwordHistoryData);

      // Step:7 -- Generate Activation Token and Save in DB
      const expiresIn = moment().add(config.get<string>('jwt.expiresIn'), 'minutes').valueOf();
      const jwtToken = await this.hashPwd.jwtToken(req.body.email, expiresIn);
      let token: UserTokenDTO = new UserTokenDTO();
      if (existingUser) {
        token = await this.userTokenDAO.findByEmailAndTokenType(req.body.email, 'registration');
      }
      token.email = req.body.email;
      token.expiryTime = new Date(expiresIn);
      token.token = jwtToken;
      token.tokenType = 'registration';
      token.created = { at: new Date(), by: req.body.email };
      if (existingUser) {
        await this.userTokenDAO.update(token._id, token);
      } else {
        await this.userTokenDAO.create(token);
      }

      const newInvitationCodes: string[] = [];
      if (isInvitationOnly) {
        invitationCodeDTO.used = { at: new Date(), by: req.body.email };
        await this.invitationCodeDAO.update(invitationCodeDTO._id, invitationCodeDTO);

        if (!existingUser) {
          // generate 5 codes and send it to the user
          for (let i = 0; i < 5; i++) {
            const newInvitationCodeDTO: InvitationCodeDTO = new InvitationCodeDTO();
            const code = this.generateCode(6);
            newInvitationCodes.push(code);
            newInvitationCodeDTO.code = code;
            newInvitationCodeDTO.created = { at: new Date(), by: req.body.email };
            await this.invitationCodeDAO.create(newInvitationCodeDTO);
          }
        }
      }

      // Step:7 -- Send Email with activation link
      let sendStatus: boolean = false;
      if (req.body.userType === UserTypes.advertiser) {
          sendStatus = await this.sendRegistrationAdvertiserMail(req.body.email, jwtToken);
        } else if (req.body.userType === UserTypes.publisher) {
          sendStatus = await this.sendRegistrationPublisherMail(req.body.email, jwtToken);
        }
      if (sendStatus) {
        return res.status(201).json({ message: 'Added successfully.', success: true, status: 201 });
      } else {
        throw new HandledApplicationError(417, 'Error while sending email, please contact support team');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly reGenerateToken = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        // Step:7 -- Generate Activation Token and Save in DB
      const user: UserDTO = await this.userDAO.findByEmail(req.body.email);
      if (user.userStatus.toLowerCase() === 'active') {
        return res.status(201).json({ message: 'User is already activated.', success: false, status: 201 });
      } else {
        const expiresIn = moment().add(config.get<string>('jwt.expiresIn'), 'minutes').valueOf();
        const jwtToken = await this.hashPwd.jwtToken(req.body.email, expiresIn);
        const token: UserTokenDTO = new UserTokenDTO();

        token.email = req.body.email;
        token.expiryTime = new Date(expiresIn);
        token.token = jwtToken;
        token.tokenType = 'registration';
        token.created = { at: new Date(), by: req.body.email };
        await this.userTokenDAO.create(token);
        // Step:7 -- Send Email with activation link
        let sendStatus: boolean = false;
        if (user.userType === UserTypes.advertiser) {
          sendStatus = await this.sendRegistrationAdvertiserMail(req.body.email, jwtToken);
        } else if (user.userType === UserTypes.publisher) {
          sendStatus = await this.sendRegistrationPublisherMail(req.body.email, jwtToken);
        }
        if (sendStatus) {
          return res.status(201).json({ message: 'Activation link sent successfully.', success: true, status: 201 });
        } else {
          throw new HandledApplicationError(417, 'Error while sending email, please contact support team');
        }
      }
    } catch (err) {
        catchError(err, next);
    }
}
  private toUserDTO(req: express.Request): UserDTO {
    const userDetail: UserDTO = new UserDTO();
    userDetail.email = req.body.email;
    userDetail.firstName = req.body.firstName;
    userDetail.lastName = req.body.lastName;
    userDetail.passwordExpiryDate = moment().add(config.get<string>('passwordExpireDays'), 'days').toDate();
    userDetail.userStatus = 'pending';
    userDetail.userType = req.body.userType;
    userDetail.created = { at: new Date(), by: req.body.email };
    userDetail.lastFailureTime = new Date();
    return userDetail;
  }

  private async sendRegistrationAdvertiserMail(
    email: string,
    jwtToken: any
  ): Promise<boolean> {
    const url: string = config.get<string>('mail.web-portal-redirection-url');

    return await this.generalMail.sendRegistrationCompletedAdvertiserMail(
      email,
      `${url}/verifytoken?token=${jwtToken}&tokenType=registration`
    );
  }

  private async sendRegistrationPublisherMail(
    email: string,
    jwtToken: any
  ): Promise<boolean> {
    const url: string = config.get<string>('mail.web-portal-redirection-url');

    return await this.generalMail.sendRegistrationCompletedPublisherMail(
      email,
      `${url}/verifytoken?token=${jwtToken}&tokenType=registration`
    );
  }

  private async sendInvitationMail(
    senderEmail: any,
    invitedEmail: string,
    invitationCode: string
  ): Promise<boolean> {
    const sender: UserDTO = await this.userDAO.findByEmail(senderEmail);
    return await this.generalMail.sendInvitationMail(
      invitedEmail,
      invitationCode,
      `${sender.firstName}`
    );
  }

  private readonly forgotPassword = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const user = await this.checkEmailExists(req);
      if (user.hasSecurityQuestions && !req.body.securityQuestion && !req.body.securityQuesAnswer) {
        return res.json({
          success: false,
          message: 'Please provide answer to any one of the security question',
          email: req.body.email,
          hasSecurityQuestions: true,
          questions: [user.securityQuestion1, user.securityQuestion2]
        });
      }
      if (user.hasSecurityQuestions && req.body.securityQuestion && req.body.securityQuesAnswer) {
        let dbSecurityAnswer = '';
        if (user.securityQuestion1 === req.body.securityQuestion) {
          dbSecurityAnswer = user.securityAnswer1;
        } else if (user.securityQuestion2 === req.body.securityQuestion) {
          dbSecurityAnswer = user.securityAnswer2;
        } else {
          throw new HandledApplicationError(
            400,
            'Invalid security question provided',
          );
        }
        const result: boolean = await this.hashPwd.validateSecurityAnswer(req.body.securityQuesAnswer, dbSecurityAnswer);
        if (!result) {
          throw new HandledApplicationError(
            400,
            'Invalid answer provided, please try again',
          );
        }
      }
      const sendStatus: boolean = await this.createForgotPasswordTokenAndSendMail(user, res);
      if (sendStatus) {
        return res.json({ message: 'Email sent successfully', success: true, hasSecurityQuestions: false });
      } else {
        throw new HandledApplicationError(417, 'Error while sending email, please contact support team');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private async createForgotPasswordTokenAndSendMail(user: UserDTO, res: express.Response) {
    await this.userTokenDAO.deleteByEmailAndTokenType(user.email, 'reset');
    const expiresIn = moment().add(config.get<string>('jwt.expiresIn'), 'minutes').valueOf();
    const jwtToken = await this.hashPwd.jwtToken(user.email, expiresIn);
    const token: UserTokenDTO = new UserTokenDTO();
    token.email = user.email;
    token.expiryTime = new Date(expiresIn);
    token.token = jwtToken;
    token.tokenType = 'reset';
    token.created = { at: new Date(), by: user.email };
    await this.userTokenDAO.create(token);

    let firstName = '';
    if (user.firstName) {
      firstName = `${user.firstName}`;
    }
    const sendStatus: boolean = true;
    if (user.userType === UserTypes.advertiser) {
      await this.sendForgetPasswordAdvertiserMail(
        user.email,
        firstName,
        jwtToken,
      );
      return sendStatus;
    }
    if (user.userType === UserTypes.publisher) {
      await this.sendForgetPasswordPublisherMail(
        user.email,
        firstName,
        jwtToken,
      );
      return sendStatus;
    }
  }

  private async checkEmailExists(req: any) {
    req
      .checkBody('email', "Mandatory field 'email' missing in request body")
      .exists();
    await throwIfInputValidationError(req);

    const user: UserDTO = await this.userDAO.findByEmailAndStatus(
      req.body.email,
      'active',
    );
    if (user === null) {
      throw new HandledApplicationError(
        400,
        'Invalid email address provided, please try again'
      );
    }
    return user;
  }

  private async validateAnswer(req: any) {
    req
      .checkBody(
        'answer',
        "Mandatory field 'answer' missing in request body",
      )
      .exists();
    await throwIfInputValidationError(req);
  }

  private async sendForgetPasswordAdvertiserMail(
    email: string,
    firstName: string,
    jwtToken: any,
  ): Promise<boolean> {
    const url: string = config.get<string>('mail.web-portal-redirection-url');
    const resetPasswordLink: string = `${url}/resetpassword/${jwtToken}`;
    return await this.generalMail.sendResetPasswordAdvertiserMail(email,
      firstName,
      resetPasswordLink);
  }

  private async sendForgetPasswordPublisherMail(
    email: string,
    firstName: string,
    jwtToken: any,
  ): Promise<boolean> {
    const url: string = config.get<string>('mail.web-portal-redirection-url');
    const resetPasswordLink: string = `${url}/resetpassword/${jwtToken}`;
    return await this.generalMail.sendResetPasswordPublisherMail(email,
      firstName,
      resetPasswordLink);
  }

  private readonly resetPassword = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      // TODO: for reset password, the name is ID, for activate user the name is token?
      req
        .checkQuery('id', "Mandatory field 'id' missing in request query")
        .exists();
      await throwIfInputValidationError(req);

      const token: UserTokenDTO = await this.userTokenDAO.findByTokenAndType(
        req.query.id,
        'reset',
      );
      if (token === null) {
        throw new HandledApplicationError(400, 'Invalid token, please reset password again');
      }

      const decoded: any = jwt.verify(
        req.query.id,
        config.get<string>('jwt.secretKey'),
      );
      if (decoded === null) {
        throw new HandledApplicationError(400, 'Invalid token, please reset password again');
      }

      if (decoded.exp < Date.now()) {
        throw new HandledApplicationError(
          400,
          'Token expired, please reset password again',
        );
      }
      const userData: UserDTO = await this.userDAO.findByEmail(token.email);

      if (userData) {
        return res.json({
          success: true,
          message: 'Reset password success',
          hasSecurityQuestions: userData.hasSecurityQuestions
        });
      } else {
        throw new HandledApplicationError(400, 'Invalid token, please reset password again');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly activateUser = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      req
        .checkQuery('token', "Mandatory field 'token' missing in request query")
        .exists();
      throwIfInputValidationError(req);

      const userToken: UserTokenDTO = await this.userTokenDAO.findByTokenAndType(
        req.query.token,
        'registration',
      );
      if (userToken === null) {
        throw new HandledApplicationError(
          401,
          'Invalid token, please try registration again',
        );
      }
      const user: UserDTO = await this.userDAO.findByEmailAndStatus(
        userToken.email,
        'pending',
      );
      if (user === null) {
        throw new HandledApplicationError(
          401,
          'Unable to find user against user token, please try registration again',
        );
      }

      const expiryTime = moment(userToken.expiryTime).valueOf();
      if (expiryTime <= Date.now()) {
        throw new HandledApplicationError(
          401,
          'Token expired, please try registration again',
        );
      }

      user.userStatus = 'active';
      await this.userDAO.update(user._id, user);
      await this.userTokenDAO.deleteByEmailAndTokenType(
        userToken.email,
        'register',
      );
      res.json({
        status: true,
        message: 'User activated successfully, please login',
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly verifyToken = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      req
        .checkQuery('token', "Mandatory field 'token' missing in request query")
        .exists();
      req
        .checkQuery('tokenType', "Mandatory field 'tokenType' missing in request query")
        .exists();
      await throwIfInputValidationError(req);
      const userToken: UserTokenDTO = await this.userTokenDAO.findByTokenAndType(
        req.query.token,
        req.query.tokenType
      );

      if (userToken === null) {
        throw new HandledApplicationError(400, `Invalid token of type ${req.query.tokenType}`);
      }
      const jwtTokenInfo: any = jwt.verify(userToken.token, config.get<string>('jwt.secretKey'));
      if (jwtTokenInfo.exp < Date.now()) {
        throw new HandledApplicationError(
          400, 'Your token has expired, please try again'
        );
      } else {
        const user: UserDTO = await this.userDAO.findByEmailAndStatus(userToken.email, 'pending');
        if (userToken.tokenType === 'registration') {
          user.userStatus = 'active';
          await this.userDAO.update(user._id, user);
          await this.userTokenDAO.deleteByEmailAndTokenType(userToken.email, 'registration');
          res.json({
            status: true,
            message: 'Congratulations! Your account has been successfully activated. Please login'
          });
        } else if (userToken.tokenType === 'addUser') {
          res.json({
            status: true,
            message: 'Congratulations! Your account has been successfully activated. Please update your password',
            email: user.email,
            userType: user.userType,
            userStatus: user.userStatus
          });
        }
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private async checkPasswordAndConfirmPassword(
    password: string,
    confirmPassword: string,
  ) {
    if (password !== confirmPassword) {
      throw new HandledApplicationError(
        417,
        'Password and confirm password must match',
      );
    }
  }

  private async checkPassword(password: string, secretKey: string, dbpasswordHash: string) {
    const passwordSecret = await this.hashPwd.hashComparision(secretKey, password);
    const result = bcrypt.compareSync(passwordSecret, dbpasswordHash);
    if (!result) {
      throw new HandledApplicationError(401, 'Incorrect current password');
    }
  }

  private async checkPasswordContainsFirstNameLastName(
    firstName: string,
    lastName: string,
    password: string
  ) {
    const exists = await this.hashPwd.nameExistsInPassword(
      firstName,
      lastName,
      password
    );
    if (exists) {
      throw new HandledApplicationError(
        417,
        'Password should not contain first 4 chars of first name or last name',
      );
    }
  }

  private async compareOldAndNewPassword(oldPassword: string, newPassword: string) {
    if (oldPassword === newPassword) {
      throw new HandledApplicationError(
        417,
        'Your new password should not be same as the current password',
      );
    }
  }

  private async checkPasswordInHistory(
    email: string,
    password: string,
    secret: string
  ) {
    const passwordHistories: PasswordHistoryDTO[] = await this.passwordHistoryDAO.getLastThreePasswords(
      email
    );

    if (passwordHistories && passwordHistories.length > 0) {
      for (const passwordHistory of passwordHistories) {
        const newPasswordSecret = await this.hashPwd.hashComparision(
          secret,
          password
        );
        const compare = bcrypt.compareSync(
          newPasswordSecret,
          passwordHistory.password
        );
        if (compare) {
          throw new HandledApplicationError(
            417,
            'New Password should not match with the last 3 passwords',
          );
        }
      }
    }
  }

  private async savePassword(user: UserDTO, password: string, secret: string) {
    // TODO: confirm with compugain if we should be using a new secret or the old one
    /*const randomString = require("random-string");
    const passwordSecretKey = randomString({ length: 16, numeric: true, letters: true, special: false });*/

    const hashedPassword = await this.hashPwd.generateHash(secret, password);
    user.password = hashedPassword;
    user.passwordSecret = secret;
    user.isLocked = 0;
    user.loginFailureAttempts = 0;
    user.passwordExpiryDate = moment().add(config.get<string>('passwordExpireDays'), 'days').toDate();
    user.modified = { at: new Date(), by: user.email };
    await this.userDAO.update(user._id, user);

    const passwordHistories: PasswordHistoryDTO[] = await this.passwordHistoryDAO.getLastThreePasswords(
      user.email
    );

    if (passwordHistories && passwordHistories.length === 3) {
      await this.passwordHistoryDAO.deleteOldPassword(passwordHistories[passwordHistories.length - 1]._id);
    }

    const passworHistory = new PasswordHistoryDTO();
    passworHistory.email = user.email;
    passworHistory.password = hashedPassword;
    passworHistory.created = { at: new Date(), by: user.email };
    await this.passwordHistoryDAO.create(passworHistory);
  }

  private readonly passwordExpiryReset = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const validation = new Validation();
      await validation.validatePasswordInput(req, 'currentPwd');
      await validation.validatePasswordInput(req, 'password');
      await validation.validatePasswordInput(req, 'confirmPassword');
      await this.checkPasswordAndConfirmPassword(
        req.body.password,
        req.body.confirmPassword
      );
      await this.compareOldAndNewPassword(req.body.currentPwd, req.body.password);
      const userData: UserDTO = await this.userDAO.getById(req.body.id);
      await this.checkPassword(req.body.currentPwd, userData.passwordSecret, userData.password);
      await this.checkPasswordContainsFirstNameLastName(
        userData.firstName,
        userData.lastName,
        req.body.password
      );

      await this.checkPasswordInHistory(userData.email, req.body.password, userData.passwordSecret);
      await this.savePassword(userData, req.body.password, userData.passwordSecret);

      return res.json({
        status: true,
        message: 'Your password has been changed, now you can login',
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly updatePassword = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const validation = new Validation();
      await validation.validatePasswordInput(req, 'password');
      await validation.validatePasswordInput(req, 'confirmPassword');

      await this.checkPasswordAndConfirmPassword(
        req.body.password,
        req.body.confirmPassword
      );
      const userTokenData: UserTokenDTO = await this.userTokenDAO.findByToken(req.body.token);
      const userData: UserDTO = await this.userDAO.findByEmail(userTokenData.email);

      if (!userData.hasSecurityQuestions) {
        await validation.validateSecurityQuestionsReq(req);
      }

      await this.checkPasswordContainsFirstNameLastName(
        userData.firstName,
        userData.lastName,
        req.body.password
      );

      await this.checkPasswordInHistory(userData.email, req.body.password, userData.passwordSecret);

      if (!userData.hasSecurityQuestions) {
        userData.hasSecurityQuestions = true;
        userData.securityQuestion1 = req.body.securityQuestion1;
        userData.securityAnswer1 = await this.hashPwd.generateSecurityAnswerHash(req.body.securityAnswer1);
        userData.securityQuestion2 = req.body.securityQuestion2;
        userData.securityAnswer2 = await this.hashPwd.generateSecurityAnswerHash(req.body.securityAnswer2);
      }

      await this.savePassword(userData, req.body.password, userData.passwordSecret);

      await this.userTokenDAO.deleteByEmailAndTokenType(
        userData.email,
        'reset'
      );
      return res.json({
        status: true,
        message: 'Your password has been changed, now you can login',
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly updateAddUserPassword = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const validation = new Validation();
      await validation.validateRegisterUser(req);

      const userTokenData: UserTokenDTO = await this.userTokenDAO.findByToken(req.body.token);
      const userData: UserDTO = await this.userDAO.findByEmail(userTokenData.email);

      const randomString = require('random-string');
      const passwordSecretKey = randomString({
        length: 16,
        numeric: true,
        letters: true,
        special: false
      });
      userData.userStatus = 'active';
      await this.savePassword(userData, req.body.password, passwordSecretKey);

      await this.userTokenDAO.deleteByEmailAndTokenType(
        userData.email,
        'addUser'
      );
      return res.json({
        status: true,
        message: 'Your password has been updated, now you can login',
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly changePassword = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const validation = new Validation();
      await validation.validateChangePasswordReq(req);

      const userData: UserDTO = await this.userDAO.findByEmail(req.email);

      // await this.checkPassword(req.body.currentPwd, userData.passwordSecret, userData.password);
      const passwordSecret = await this.hashPwd.hashComparision(userData.passwordSecret, req.body.currentPwd);
      const result = bcrypt.compareSync(passwordSecret, userData.password);
      if (!result) {
        return res.json({ success: false, message: 'Incorrect current password' });
      }
      const passwordHistories: PasswordHistoryDTO[] = await this.passwordHistoryDAO.getLastThreePasswords(
        userData.email
      );
      if (passwordHistories && passwordHistories.length > 0) {
        for (const passwordHistory of passwordHistories) {
          const newPasswordSecret = await this.hashPwd.hashComparision(
            userData.passwordSecret,
            userData.password
          );
          const compare = bcrypt.compareSync(
            newPasswordSecret,
            passwordHistory.password
          );
          if (compare) {
            return res.json({ success: false, message: 'New Password should not match with the last 3 passwords' });
          }
        }
      }
      await this.savePassword(userData, req.body.newPassword, userData.passwordSecret);
      if (userData.userType === UserTypes.advertiser) {
        const isSubscribedForEmail = userData.isSubscribedForEmail;
        if (userData.hasPersonalProfile) {
          if (isSubscribedForEmail) {
            await this.generalMail.sendPasswordChangedAdvertiserMail(
              req.email,
              userData.firstName,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }
      if (userData.userType === UserTypes.publisher) {
        const isSubscribedForEmail = userData.isSubscribedForEmail;
        if (userData.hasPersonalProfile) {
          if (isSubscribedForEmail) {
            await this.generalMail.sendPasswordChangedPublisherMail(
              req.email,
              userData.firstName,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }
      res.json({ success: true, message: 'Password Changed successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly saveQuestions = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const validation = new Validation();
      await validation.validateSecurityQuestionsReq(req);

      const userData: UserDTO = await this.userDAO.findByEmail(req.email);
      if (!userData) {
        throw new HandledApplicationError(401, 'Unable to find user with the given email');
      }

      if (req.body.securityQuestion1) {
        userData.securityQuestion1 = req.body.securityQuestion1;
        userData.securityAnswer1 = await this.hashPwd.generateSecurityAnswerHash(req.body.securityAnswer1);
      }
      if (req.body.securityQuestion2) {
        userData.securityQuestion2 = req.body.securityQuestion2;
        userData.securityAnswer2 = await this.hashPwd.generateSecurityAnswerHash(req.body.securityAnswer2);
      }
      userData.hasSecurityQuestions = true;
      userData.modified = { at: new Date(), by: req.email };
      await this.userDAO.update(userData._id, userData);
      return res.json({
        success: true,
        message: 'Security questions added successfully',
        hasSecurityQuestions: true
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly encryptedLogin = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!req.body) {
        throw new HandledApplicationError(400, 'Incorrect login request');
      }
      req.body.email = this.encryption.decrypt(req.body.key);
      req.body.password = this.encryption.decrypt(req.body.value);
      this.login(req, res, next);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly login = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!req.body) {
        throw new HandledApplicationError(400, 'Incorrect login request');
      }
      // check user
      const user: UserDTO = await this.userDAO.findByEmail(
        req.body.email
      );
      if (!user) {
        throw new HandledApplicationError(401, 'Invalid credentials!');
      }

      if (user.userStatus.toLowerCase() === 'pending') {
        throw new HandledApplicationError(401, 'Account is not activated!');
      }
      if (user.userStatus.toLowerCase() === 'deleted') {
        throw new HandledApplicationError(401, 'User is disabled');
      }
      // end
      const passSecret = await this.hashPwd.hashComparision(
        user.passwordSecret,
        req.body.password,
      );
      const result = bcrypt.compareSync(passSecret, user.password);

      if (result) {
        if (user.loginFailureAttempts >= 3 && user.isLocked) {
          const diff = moment(Date.now().valueOf()).diff(
            user.lastFailureTime.getTime(),
            'hours'
          );
          if (diff < 24) {
            throw new HandledApplicationError(
              401,
              'Your account has been locked, Please reset your password using Forgot password link'
            );
          }
        }
        if (moment(user.passwordExpiryDate).valueOf() <= Date.now()) {
          return res.json({ status: false, message: 'password_expiry_reset', id: user._id });
        }

        // User not logged out explicitely and after some time token expired, so update last logout time.
        if (user.lastLogoutTime) {
          if (user.lastLogoutTime < user.lastLoginTime) {
            const tokenData: UserTokenDTO = await this.userTokenDAO.findByEmailAndTokenType(req.body.email, 'login');
            // Checking login token expiry time greater than current time then reduce 2 secs
            // from current time and update as last logout time
            if (tokenData) {
              if (tokenData.expiryTime > new Date()) {
                user.lastLogoutTime = new Date(Date.now() - 2000);
              } else {
                user.lastLogoutTime = tokenData.expiryTime;
              }
            } else {
              // lets continue with the last logout time we have
            }
          }
        } else {
          const tokenData: UserTokenDTO = await this.userTokenDAO.findByEmailAndTokenType(req.body.email, 'login');
          if (tokenData) {
            if (tokenData.expiryTime > new Date()) {
              user.lastLogoutTime = new Date(Date.now() - 2000);
            } else {
              user.lastLogoutTime = tokenData.expiryTime;
            }
          } else {
            user.lastLogoutTime = new Date(Date.now() - 2000);
          }
        }

        const expiresIn: number = moment()
          .add('minutes', config.get<string>('jwt.expiresIn'))
          .valueOf();
        const jwttoken = await this.hashPwd.jwtToken(req.body.email, expiresIn);
        // deleting already existed token for logged in user
        await this.userTokenDAO.deleteByEmailAndTokenType(req.body.email, 'login');

        const token: UserTokenDTO = new UserTokenDTO();
        token.email = req.body.email;
        token.token = jwttoken;
        token.tokenType = 'login';
        token.expiryTime = new Date(expiresIn);
        token.created = { at: new Date(), by: req.body.email };
        await this.userTokenDAO.create(token);

        user.lastLoginTime = new Date();
        user.isLocked = 0;
        user.loginFailureAttempts = 0;
        await this.userDAO.update(user._id, user);

        res.json({
          email: user.email,
          first_name: user.firstName,
          last_name: user.lastName,
          message: 'Login successful',
          security: user.hasSecurityQuestions,
          status: true,
          token: jwttoken,
          usertype: user.userType,
          lastLogoutTime: user.lastLogoutTime,
          hasPersonalProfile: user.hasPersonalProfile
        });
      } else {
        if (user.loginFailureAttempts < 3 && !user.isLocked) {
          await this.userDAO.updateFailureAttempt(user._id);
          throw new HandledApplicationError(
            401, 'Invalid credentials',
          );
        }
        const diff = moment(Date.now().valueOf()).diff(
          user.lastFailureTime.getTime(),
          'hours'
        );
        if (user.loginFailureAttempts >= 3 && diff < 24) {
          await this.userDAO.lock(user._id);
          throw new HandledApplicationError(
            401,
            'Your account has been locked, Please reset your password using Forgot password link'
          );
        }
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly isLoggedIn = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const userToken = await this.userTokenDAO.findByTokenAndType(req.query.token, 'login');
      if (userToken) {
        const expiryTime = moment(userToken.expiryTime).valueOf();
        if (expiryTime <= Date.now()) {
          throw new HandledApplicationError(
            401,
            'Token expired, please try login again',
          );
        }
        res.json({ success: true, message: 'VALID_TOKEN' });
      } else {
        res.json({ success: false, message: 'INVALID_TOKEN' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly logout = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      await this.userTokenDAO.deleteByEmailAndTokenType(req.email, 'login');
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      user.lastLogoutTime = new Date();
      await this.userDAO.update(user._id, user);
      res.json({ success: true, message: 'Logged Out' });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getDetailsFromEmail = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const user: UserDTO = await this.userDAO.findByEmail(req.body.email);
      if (req.body && req.body.isVisitedDashboard && user.overlayShown === false &&
          user.overlayShown !== req.body.isVisitedDashboard) {
        const saveUser = { overlayShown: req.body.isVisitedDashboard };
        this.userDAO.updateIsVisitedDashboard(req.body.email, saveUser);
      }
      res.json({ firstName: user.firstName, lastName: user.lastName, userType: user.userType,
                 hasSecurityQuestions: user.hasSecurityQuestions,
                 isVisitedDashboard: user.overlayShown });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly extendToken = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const userToken: UserTokenDTO = await this.userTokenDAO.findByTokenAndType(
        req.body.token,
        'login',
      );
      if (userToken) {
        userToken.expiryTime = moment(userToken.expiryTime)
          .add(config.get<string>('jwt.expiresIn'), 'minutes')
          .toDate();
        this.userTokenDAO.update(userToken._id, userToken);
        res.json({ success: true, message: 'session extended' });
      } else {
        res.json({ success: false, message: 'Error, session not extended' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private verifyEmailDomain(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const verifier = require('email-exist');
      verifier.verify(email, (err: any, info: any) => {
        if (err !== null) {
          resolve(err);
        } else {
          resolve(info);
        }
      });
    });
  }

  private readonly updatefirstlastname = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const saveUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        hasPersonalProfile: true
      };
      res.json(this.userDAO.updatefirstlastname(req.email, saveUser));
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly updateUser = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      // Step-1 : Add validations on input request

      // Step:2 -- Verify email exists or not
      const user: UserDTO = await this.userDAO.findByEmail(req.body.email);
      if (!user) {
        throw new HandledApplicationError(409, 'Invalid User. Please use valid email');
      }

      // Step:4 -- Generate random key and hash password
      const randomString = require('random-string');
      const passwordSecretKey = randomString({
        length: 16,
        numeric: true,
        letters: true,
        special: false
      });
      const hashedPassword: string = await this.hashPwd.generateHash(passwordSecretKey, req.body.password);

      user.password = hashedPassword;
      user.passwordSecret = passwordSecretKey;
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.securityQuestion1 = req.body.securityQuestion1;
      user.securityAnswer1 = await this.hashPwd.generateSecurityAnswerHash(req.body.securityAnswer1);
      user.securityQuestion2 = req.body.securityQuestion2;
      user.securityAnswer2 = await this.hashPwd.generateSecurityAnswerHash(req.body.securityAnswer2);
      user.hasSecurityQuestions = true;
      user.hasPassword = true;
      user.hasPersonalProfile = true;
      user.passwordExpiryDate = moment().add(config.get<string>('passwordExpireDays'), 'days').toDate();

      const updatedUser = await this.userDAO.update(user._id, user);
      res.json({
        success: true,
        message: 'Updated details sucessfully'
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly checkUserPassword = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const userData: UserDTO = await this.userDAO.findByEmail(req.email);
      const passwordSecret = await this.hashPwd.hashComparision(userData.passwordSecret, req.body.password);
      const result = bcrypt.compareSync(passwordSecret, userData.password);
      if (!result) {
        res.json({
          success: false,
          message: 'Password incorrect'
        });
      } else {
        res.json({
          success: true,
          message: 'Password matched'
        });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly deleteUser = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      // user can only delete himself
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      user.userStatus = 'deleted';
      await this.userDAO.update(user._id, user);
      let url: string = config.get<string>('mail.web-portal-redirection-url');
      if (user.userType === 'advertiser') {
        if (user.hasPersonalProfile) {
          if (user.isSubscribedForEmail) {
            url = `${url}/advertiser/settings`;
            await this.generalMail.sendDeleteAccountAdvertiserMail(
              req.email,
              url,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }
      if (user.userType === 'publisher') {
        if (user.hasPersonalProfile) {
          if (user.isSubscribedForEmail) {
            url = `${url}/publisher/settings`;
            await this.generalMail.sendDeleteAccountPublisherMail(
              req.email,
              url,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }
      res.json({
        success: true,
        message: 'User deleted sucessfully!'
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly notificationSettings = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      if (user.userType === 'advertiser') {
        const advertiserUser: AdvertiserUserDTO = await this.advertiserUserDAO.findByEmail(req.email);
        advertiserUser.notificationSettings = req.body;
        await this.advertiserUserDAO.update(advertiserUser.email, advertiserUser);

        try {
          const notificationIcon: NotificationIcon = NotificationIcon.info;
          const notificationMessage: string = 'We notice that you just changed your email alert settings. Was this initiated by you? In case this was an unauthorised change, get in touch with us immediately';
          await this.notificationHelper.addNotification([user.email], notificationMessage, notificationIcon,
            NotificationType.general, null, null);
        } catch (err) {
          logger.error(`${err.stack}\n${new Error().stack}`);
        }
      } else {
        const publisherUser: PublisherUserDTO = await this.publisherUserDAO.findByEmail(req.email);
        publisherUser.notificationSettings = req.body;
        await this.publisherUserDAO.update(publisherUser.email, publisherUser);

        try {
          const notificationIcon: NotificationIcon = NotificationIcon.info;
          const notificationMessage: string = 'We notice that you just changed your email alert settings. Was this initiated by you? In case this was an unauthorised change, get in touch with us immediately';
          await this.notificationHelper.addNotification([user.email], notificationMessage, notificationIcon,
            NotificationType.general, null, null);
        } catch (err) {
          logger.error(`${err.stack}\n${new Error().stack}`);
        }
      }
      res.json({
        success: true,
        message: 'Notification saved sucessfully!'
      });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly verificationPending = async (req: IAuthenticatedRequest,
                                                res: express.Response, next: express.NextFunction) => {

    await this.authorizeAdmin.authorize(req.email);
    let availableUserEmail: string[] = [];
    const mailSentToUsers: string[] = [];
    try {
      const url: string = config.get<string>('mail.web-portal-redirection-url');

      // no of verification pending reminder email sent
      // last verification pending reminder email sent

      // get all the users who are inactive and count of verification pending reminder
      // mail is less than 4
      const users: UserDTO[] = await this.userDAO.findVerificationPendingUsersForEmail();
      const currentDate: Date = new Date();
      availableUserEmail = users.map((x) => x.email);
      for (const user of users) {
        if (user.reminder.verificationPending) {
          let lastMailSentAt: Date = user.reminder.verificationPending.mailSentAt;
          if (!lastMailSentAt) {
            lastMailSentAt = user.created.at;
          }
          let noOfTimesReminderSent: number = 0;
          if (user.reminder.verificationPending.noOfMailsSent) {
            noOfTimesReminderSent = user.reminder.verificationPending.noOfMailsSent;
          }
          if (noOfTimesReminderSent < 3) {
            if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
              this.sendVerificationPendingReminderMail(user, url, currentDate, noOfTimesReminderSent);
              mailSentToUsers.push(user.email);
            } else {
              // dont do anything
            }
          } else if (noOfTimesReminderSent === 3) {
            if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
              this.sendVerificationPendingReminderMail(user, url, currentDate, noOfTimesReminderSent);
              mailSentToUsers.push(user.email);
            } else {
              // dont do anything
            }
          } else {
            // dont do anything; we dont have to send mail post 4 reminders
          }
        }
      }

      res.status(200).json({ message: `Available: ${availableUserEmail}, Mail Sent To: ${mailSentToUsers}`, success: true, status: 201 });
    } catch (err) {
      catchError(err, next);
    }
  }

  private async sendVerificationPendingReminderMail(user: UserDTO, url: string, currentDate: Date,
                                                    noOfTimesReminderSent: number) {
    const userToken: UserTokenDTO = await this.userTokenDAO.findByEmailAndTokenType(user.email, 'registration');
    if (userToken && user.userType === UserTypes.advertiser) {
      // send mail
      if (user.isSubscribedForEmail) {
        const timeDifference = Math.abs(currentDate.getTime() - user.created.at.getTime());
        const noOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
        await this.generalMail.sendReminderToCompleteVerificationAdvertiserMail(
          user.email,
          `${url}/verifytoken?token=${userToken.token}&tokenType=registration`,
          noOfDays,
          `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
        );
      }
    }
    if (userToken && user.userType === UserTypes.publisher) {
      // send mail
      if (user.isSubscribedForEmail) {
        const timeDifference = Math.abs(currentDate.getTime() - user.created.at.getTime());
        const noOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
        await this.generalMail.sendReminderToCompleteVerificationPublisherMail(
          user.email,
          `${url}/verifytoken?token=${userToken.token}&tokenType=registration`,
          noOfDays,
          `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
        );
      }
    }
    user.reminder.verificationPending.mailSentAt = currentDate;
    user.reminder.verificationPending.noOfMailsSent = noOfTimesReminderSent + 1;
    await this.userDAO.update(user._id, user);
  }

  private readonly nearingPasswordExpiry = async (req: IAuthenticatedRequest,
                                                  res: express.Response, next: express.NextFunction) => {
    let availableUserEmail: string[] = [];
    const mailSentToUsers: string[] = [];
    const currentDate: Date = new Date();
    try {
      await this.authorizeAdmin.authorize(req.email);
      const users: UserDTO[] = await this.userDAO.findUsersNearingPasswordExpiry();
      availableUserEmail = users.map((x) => x.email);
      for (const user of users) {
        if (user.reminder.passwordAboutToExpire) {
          let noOfTimesReminderSent: number = 0;
          if (user.reminder.passwordAboutToExpire.noOfMailsSent) {
            noOfTimesReminderSent = user.reminder.passwordAboutToExpire.noOfMailsSent;
          }
          if (noOfTimesReminderSent === 0) {
            const url: string = config.get<string>('mail.web-portal-redirection-url');
            this.sendPasswordAboutToExpireReminderMail(user, url, currentDate, 0);
            mailSentToUsers.push(user.email);
          }
        }
      }
      res.status(200).json({ message: `Available: ${availableUserEmail}, Mail Sent To: ${mailSentToUsers}`, success: true, status: 201 });
    } catch (err) {
      catchError(err, next);
    }
  }

  private async sendPasswordAboutToExpireReminderMail(user: UserDTO, url: string, currentDate: Date,
                                                      noOfTimesReminderSent: number) {
    if (user.userType === UserTypes.advertiser) {
      if (user.hasPersonalProfile) {
        if (user.isSubscribedForEmail) {
          url = `${url}/advertiser/settings`;
          await this.generalMail.sendReminderToChangePasswordAdvertiserMail(
            user.email,
            user.firstName,
            url,
            `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
          );
        }
      }
      try {
        const notificationIcon: NotificationIcon = NotificationIcon.info;
        const notificationMessage: string = 'A quick reminder to let you know that your current account password will expire in three days. We urge you to change your password within the next 72 hours to avoid password recovery hassles';
        await this.notificationHelper.addNotification([user.email], notificationMessage, notificationIcon,
          NotificationType.general, null, null);
      } catch (err) {
        logger.error(`${err.stack}\n${new Error().stack}`);
      }
    }
    if (user.userType === UserTypes.publisher) {
      if (user.hasPersonalProfile) {
        if (user.isSubscribedForEmail) {
          url = `${url}/publisher/settings`;
          await this.generalMail.sendReminderToChangePasswordPublisherMail(
            user.email,
            user.firstName,
            url,
            `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
          );
        }
      }
      try {
        const notificationIcon: NotificationIcon = NotificationIcon.info;
        const notificationMessage: string = 'A quick reminder to let you know that your current account password will expire in three days. We urge you to change your password within the next 72 hours to avoid password recovery hassles';
        await this.notificationHelper.addNotification([user.email], notificationMessage, notificationIcon,
          NotificationType.general, null, null);
      } catch (err) {
        logger.error(`${err.stack}\n${new Error().stack}`);
      }
    }
    user.reminder.passwordAboutToExpire.mailSentAt = currentDate;
    user.reminder.passwordAboutToExpire.noOfMailsSent = noOfTimesReminderSent + 1;
    await this.userDAO.update(user._id, user);
  }

  private generateCode(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  private readonly unsubscribeUser = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  try {
    req
      .checkQuery('token', "Mandatory field 'token' missing in request query")
      .exists();
    await throwIfInputValidationError(req);

    const userToken: UserTokenDTO = await this.userTokenDAO.findByTokenAndType(
      req.query.token,
      'unsubscribe'
    );
    if (userToken) {
      const user: UserDTO = await this.userDAO.findByEmailAndStatus(userToken.email, 'active');
      user.isSubscribedForEmail = false;
      await this.userDAO.update(user._id, user);
      res.json({
        status: true,
        message: 'You have successfully unsubscribed.'
      });
    } else {
      throw new HandledApplicationError(500, 'Invalid token for unsubscription');
    }
  } catch (err) {
    catchError(err, next);
  }
 }

  private readonly submittedFeedback = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const userToken: UserTokenDTO = await this.userTokenDAO.findByTokenAndType(
        req.query.token,
        req.query.tokenType
      );
      if (!userToken) {
        throw new HandledApplicationError(409, 'Invalid User. Please use valid email');
      }

      if (userToken) {
        const user: UserDTO = await this.userDAO.findByEmailAndStatus(userToken.email, 'active');
        user.feedback = req.query.feedback;
        await this.userDAO.update(user._id, user);
        return res.json({
          success: true,
        });
      }
    } catch (err) {
      catchError(err, next);
    }
  }
}

interface IVerifiedInfo {
  success: boolean;
  info: string;
  addr: string;
  response: string;
}
