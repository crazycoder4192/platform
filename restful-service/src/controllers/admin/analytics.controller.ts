import express from 'express';
import mongoose from 'mongoose';
import AnalyticsDAO from '../../daos/admin/analytics.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import InvoiceDAO from '../../daos/advertiser/invoice.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import PublisherBankDetailDAO from '../../daos/publisher/bank-detail.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import AdminAdvertiserAnalyticsDTO from '../../dtos/admin/advertiser-analytics.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { Encryption } from '../../util/crypto';
import { AuthorizeAdmin } from './authorize-admin';

export class AdminAnalyticsController {

    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly analyticsDAO: AnalyticsDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly subCampaignDAO: SubCampaignDAO;
    private readonly encryption: Encryption;
    private readonly bankDetailDAO: PublisherBankDetailDAO;
    private readonly publisherDAO: PublisherDAO;
    private readonly publisherPlatformDAO: PublisherPlatformDAO;

    constructor() {
        this.bankDetailDAO = new PublisherBankDetailDAO();
        this.authorizeAdmin = new AuthorizeAdmin();
        this.analyticsDAO = new AnalyticsDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.subCampaignDAO = new SubCampaignDAO();
        this.encryption = new Encryption();
        this.publisherDAO = new PublisherDAO();
        this.publisherPlatformDAO = new PublisherPlatformDAO();
    }

    public getAnalyticsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
         const brandsAndSubcamps = await this.getAdvertisersAnalyticsData(req, res, next);
         return res.json(brandsAndSubcamps);

        } catch (err) {
            catchError(err, next);
          }
    }

    public getBankDetails = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        let result: any = [];
        try {
            let zone;
            if (req.body.zone === '0') {
                zone = ['1', '2'];
                for (const index in zone) {
                    if (!result.length) {
                        const promiseResult = await this.getBankDetailsZoneWise(zone[index]);
                        result = promiseResult;
                    } else {
                        const promiseResult = await this.getBankDetailsZoneWise(zone[index]);
                        result = result.concat(promiseResult);
                    }
                }
                res.json(result);
            } else {
                res.json(await this.getBankDetailsZoneWise(req.body.zone));
            }
        } catch (e) {
            catchError(e, next);
        }
    }

    private getBankDetailsZoneWise(zone: string) {
        return new Promise(async (resolve, reject) => {
            try {
                const result: any = [];
                const cond = (zone === '1') ? { deleted: false } : { bankCountry: this.encryption.encrypt('India'), deleted: false };
                const projection = { bankName: 1, bankCountry: 1, accountHolderName: 1, accountNumber: 1, accountType: 1,
                                     swiftCode: 1, taxNumber: 1, publisherId: 1, platforms: 1};
                const bankDetailsPromise = await this.bankDetailDAO.findByCond(zone, cond, projection);
                if (bankDetailsPromise.length) {
                    for (const index in bankDetailsPromise) {
                        if (bankDetailsPromise[index]) {
                            const platforms = bankDetailsPromise[index].platforms;
                            const pubCond = { _id: bankDetailsPromise[index].publisherId };
                            const pubProjection = { organizationName: 1 };
                            const publisherPromise = await this.publisherDAO.getPublisherByCond(pubCond, pubProjection);
                            const pltfnmCond = { _id: { $in: platforms } };
                            const pltfnmProjection = { name: 1 };
                            const platformPromise = await this.publisherPlatformDAO.getPublisherPlatformByCond(pltfnmCond, pltfnmProjection);
                            for (const i in platformPromise) {
                                if (platformPromise[i]) {
                                    result.push({
                                        platformName: platformPromise[i].name,
                                        publisherName: publisherPromise[0].organizationName,
                                        bankName : bankDetailsPromise[index].bankName,
                                        bankCountry : bankDetailsPromise[index].bankCountry || 'US',
                                        accountHolderName : bankDetailsPromise[index].accountHolderName,
                                        accountNumber : bankDetailsPromise[index].accountNumber,
                                        accountType : bankDetailsPromise[index].accountType,
                                        swiftCode : bankDetailsPromise[index].swiftCode,
                                        taxNumber : bankDetailsPromise[index].taxNumber
                                    });
                                }
                            }
                        }
                    }
                    resolve(result);
                }
            } catch (e) {
                reject(e);
            }
        });
    }

    private readonly getAdvertisersAnalyticsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        await this.authorizeAdmin.authorize(req.email);
        const brandAndTherapeuticData: any[] = [];
        let activeSubCamp = 0;
        let inactiveSubCamp = 0;
        const topSubCampaigns: any[] = [];
        const topSubCampaignsIds: any[] = [];
        const topSubCampaignsNamesMap = new Map();
        const topBrands: any[] = [];
        const topBrandsIds: any[] = [];
        const topBrandsNamesMap = new Map();
        const timezone: string = req.body.timezone;
        const adminAdvertiserAnalyticsDTO: AdminAdvertiserAnalyticsDTO = new AdminAdvertiserAnalyticsDTO();
        const brands = await this.analyticsDAO.getAdvertiserBrandsAndTherapeuticAreas(req.body, req.header('zone'));

        for (const ele of brands) {
            const detail: any = { };
            detail.brandType = ele._id;
            const brandIdArray: any[] = [];
            for (const brand of ele.brandIds) {
                brandIdArray.push(mongoose.Types.ObjectId(brand.brandId));
            }
            const details = await this.subCampaignDAO.getSubCampaignStatusByIdsForAdmin(brandIdArray);
            for (const info of details) {
                activeSubCamp = activeSubCamp + info.active;
                inactiveSubCamp = inactiveSubCamp + info.inactive;
            }
            detail.active = activeSubCamp;
            detail.inactive = inactiveSubCamp;
            brandAndTherapeuticData.push(detail);
        }

        const topCampaignsDetails = await this.analyticsDAO.getAdvertiserTopCampaign(req.body, req.header('zone'));

        topCampaignsDetails.forEach((ele: any) => {
            ele.data.forEach((el: any) => {
                topSubCampaignsIds.push(el.subCampId);
            });
        });

        const topSubCampainNames = await this.subCampaignDAO.getSubCampaignNamesByIdsForAdmin(topSubCampaignsIds);

        topSubCampainNames.forEach((ele) => {
            topSubCampaignsNamesMap.set(ele._id.toString(), ele.name);
        });

        topCampaignsDetails.forEach((ele: any) => {
            const data: any = { };
            data.adType = ele._id;
            const subCampsInfo: any[] = [];
            ele.data.forEach((el: any) => {
                const detail: any = { };
                detail.subCampaignName = topSubCampaignsNamesMap.get(el.subCampId.toString());
                detail.spends = el.spends;
                detail.reach = el.uniqueReach;
                subCampsInfo.push(detail);
            });
            data.subcamps = subCampsInfo;
            topSubCampaigns.push(data);
        });

        const topBrandsDetails = await this.analyticsDAO.getAdvertiserTopBrands(req.body, req.header('zone'));

        for (const brandId of topBrandsDetails) {
            topBrandsIds.push(mongoose.Types.ObjectId(brandId._id));
        }
        const topBrandsNames = await this.advertiserBrandDAO.getBrandNamesByBrandIdsForAdmin(topBrandsIds, req.header('zone'));
        for (const ele of topBrandsNames) {
            topBrandsNamesMap.set(ele._id.toString(), ele.brandName);
        }

        for (const ele of topBrandsDetails) {
            const detail: any = { };
            // const brandId: string = ele._id;
           // detail.brandName = topBrandsNamesMap.get(brandId.toString());
            detail.reach = ele.uniqueReach;
            detail.subcampaigncount = ele.subcampaignCount;
            detail.spends = ele.spends;
            topBrands.push(detail);
        }

        const adTypes = await this.analyticsDAO.getAdvertiserAdTypes(req.body, req.header('zone'));

        const trends = await this.analyticsDAO.getAdvertiserTrends(req.body, timezone, req.header('zone'));

        adminAdvertiserAnalyticsDTO.brandAndTherapeuticData = brandAndTherapeuticData;
        adminAdvertiserAnalyticsDTO.topSubCampaigns = topSubCampaigns;
        adminAdvertiserAnalyticsDTO.topBrands = topBrands;
        adminAdvertiserAnalyticsDTO.typeOfAds = adTypes;

        return adminAdvertiserAnalyticsDTO;
    }
}
