import express from 'express';
import CountryCurrencyDAO from '../../daos/admin/country-currency.dao';
import CountryCurrencyDTO from '../../dtos/admin/country-currency.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class CountryCurrencyController {
  private readonly countryCurrencyDAO: CountryCurrencyDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;

  constructor() {
    this.authorizeAdmin = new AuthorizeAdmin();
    this.countryCurrencyDAO = new CountryCurrencyDAO();
  }

  public getAllCountryCurrencies = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const countryCurrencies: CountryCurrencyDTO[] = await this.countryCurrencyDAO.getAll();
      res.json(countryCurrencies);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createCountryCurrency = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateCountryCurrencyInput(req);
      await throwIfInputValidationError(req);

      const dto: CountryCurrencyDTO = this.toCountryCurrencyDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.countryCurrencyDAO.create(dto);
      res.json({ success: true, message: `${dto.countryName} country added successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateCountryCurrency = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateCountryCurrencyInput(req);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);

      const dto: CountryCurrencyDTO = this.toCountryCurrencyDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.countryCurrencyDAO.update(req.params.id, dto);
      res.json({ success: true, message: `${dto.countryName} country updated successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getCountryCurrency = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const countryCurrency: CountryCurrencyDTO = await this.countryCurrencyDAO.getById(req.params.id);
      res.json(countryCurrency);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteCountryCurrency = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const countryCurrency: CountryCurrencyDTO = await this.countryCurrencyDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: `${countryCurrency.countryName} country deleted successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  private validateCountryCurrencyInput(req: IAuthenticatedRequest) {
    // TODO: add validation
  }

  private toCountryCurrencyDTO(req: IAuthenticatedRequest): CountryCurrencyDTO {
    const dto: CountryCurrencyDTO = new CountryCurrencyDTO();
    dto.countryName = req.body.countryName;
    dto.countryISO3Code = req.body.countryISO3Code;
    dto.countryISO2Code = req.body.countryISO2Code;
    dto.currencyCode = req.body.currencyCode;
    dto.currencyName = req.body.currencyName;
    dto.symbol = req.body.symbol;
    return dto;
  }
}
