import express from 'express';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherPlatformDTO, { PublisherPaymentSlot } from '../../dtos/publisher/publisher-platform.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class PublisherPaymentController {
    private readonly platformDAO: PublisherPlatformDAO;
    private readonly authorizeAdmin: AuthorizeAdmin;

    constructor() {
        this.platformDAO = new PublisherPlatformDAO();
        this.authorizeAdmin = new AuthorizeAdmin();
    }

    // this api will be used by the doceree admin to enter
    // the offer that we are going to give to the publisher
    public updatePublisherPayment = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            this.authorizeAdmin.authorize(req.email);
            if (req.body.slots) {
                const argSlots: any[] = req.body.slots;
                if (req.params.id) {
                    const platformId: string = req.params.id;
                    const platformDTO: PublisherPlatformDTO = new PublisherPlatformDTO();

                    const slots: PublisherPaymentSlot[] = [];
                    const slot: PublisherPaymentSlot = new PublisherPaymentSlot();
                    for (const r of argSlots) {
                        slot.thresholdAmount = r.thresholdAmount;
                        slot.percentageGiven = r.percentageGiven;
                        slots.push(slot);
                    }
                    platformDTO.paymentSlots = slots;
                    this.platformDAO.update(platformId, platformDTO, req.header('zone'));
                    return res.json({ });
                } else {
                    throw new HandledApplicationError(500, 'Platform ID is not passed');
                }
            } else {
                throw new HandledApplicationError(500, 'Payment range is not passed');
            }

        } catch (err) {
            catchError(err, next);
        }
    }
}
