import express from 'express';
import mongoose from 'mongoose';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import InvoiceDAO from '../../daos/advertiser/invoice.dao';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import InvoiceDTO from '../../dtos/advertiser/invoice.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class AdvertiserPaymentController {
    private readonly invoiceDAO: InvoiceDAO;
    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;

    constructor() {
        this.invoiceDAO = new InvoiceDAO();
        this.authorizeAdmin = new AuthorizeAdmin();
        this.advertiserDAO = new AdvertiserDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
    }

    public getAdvertiserInvoices = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            this.authorizeAdmin.authorize(req.email);
            const invoices = await this.invoiceDAO.invoicesGroupbyAdvertiserIdForAdmin(req.params.type, req.header('zone'));
            const advertiserIds = [];
            const brandIds = new Set();
            for (const invoice of invoices) {
                advertiserIds.push(invoice._id.advertiserId);
                for (const detail of invoice.data) {
                    const brandId = detail.brandId;
                    brandIds.add(mongoose.Types.ObjectId(brandId));
                }
            }
            const advertiserBrands = await this.advertiserBrandDAO.getBrands(Array.from(brandIds), req.header('zone'));
            const brandNames = new Map();
            for (const brand of advertiserBrands) {
                const brandId: string = brand._id;
                brandNames.set(brandId.toString(), brand.brandName);
            }
            const advertisers: AdvertiserDTO[] = await this.advertiserDAO.getAdvertisersByIds(advertiserIds);
            const invoiceDetails = [];
            for (const advertiser of advertisers) {
                for (const invoice of invoices) {
                    if (advertiser._id.equals(invoice._id.advertiserId)) {
                        const details: any = { };
                        details.advertiserId = advertiser._id;
                        details.advertiserName = advertiser.businessName;
                        let totalAdvertiserAmount = 0;
                        const brandDetails = [];
                        for (const data of invoice.data) {
                            const brandDetail: any = { };
                            brandDetail.invoiceId = data.invoiceId;
                            brandDetail.invoiceDate = data.invoiceDate;
                            totalAdvertiserAmount = totalAdvertiserAmount + data.amount;
                            brandDetail.brandId = data.brandId;
                            const brandId: string = brandDetail.brandId;
                            brandDetail.brandName = brandNames.get(brandId.toString());
                            brandDetail.amount = data.amount;
                            brandDetail.invoiceStatus = data.invoiceStatus;
                            brandDetail.qbInvoice = data.quickbooksInvoiceGenerated;
                            brandDetail.qbPayment = data.quickbooksPaymentGenerated;
                            brandDetails.push(brandDetail);
                        }
                        details.brandDetails = brandDetails;
                        details.amountInvoiced = totalAdvertiserAmount;
                        details.showBrand = false;
                        invoiceDetails.push(details);
                    }
                }
            }
            return res.json(invoiceDetails);
        } catch (err) {
            catchError(err, next);
        }

    }
}
