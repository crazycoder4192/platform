import express from 'express';
import PermissionDAO from '../../daos/admin/permission.dao';
import RoleDAO from '../../daos/admin/role.dao';
import RoleDTO from '../../dtos/admin/role.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class RoleController {
  private readonly roleDAO: RoleDAO;
  private readonly permissionDAO: PermissionDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;

  constructor() {
    this.roleDAO = new RoleDAO();
    this.permissionDAO = new PermissionDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
  }

  public getUserRoles = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    if (req && req.query && req.query.id) {
      try {
        const role: RoleDTO = await this.roleDAO.getRoleById(req.query.id);
        res.json(role);
      } catch (err) {
        catchError(err, next);
      }
    } else if (req && req.query && req.query.modulename) {
      try {
        const userRoles: RoleDTO[] = await this.roleDAO.getRolesByModule(
          req.query.modulename,
        );
        res.json(userRoles);
      } catch (err) {
        catchError(err, next);
      }
    } else {
      try {
        const userRoles: RoleDTO[] = await this.roleDAO.getAllRoles();
        res.json(userRoles);
      } catch (err) {
        catchError(err, next);
      }
    }
  }

  public createUserRoles = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      // TODO: Verify if all the fields are provided
      const dto: RoleDTO = await this.toRoleDTO(req);
      dto.created = { at: new Date(), by: req.email };

      await this.roleDAO.create(dto);
      res.json({ message: 'User role added successfully', success: true });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateUserRoles = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      // TODO: Verify if all the fields are provided
      const dto: RoleDTO = await this.toRoleDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.roleDAO.update(req.body.id, dto);
      res.json({ message: 'User role updated successfully', success: true });
    } catch (err) {
      catchError(err, next);
    }
  }

  private async toRoleDTO(req: IAuthenticatedRequest): Promise<RoleDTO> {
    const dto: RoleDTO = new RoleDTO();
    dto.description = req.body.description;
    dto.moduleType = req.body.moduleType;
    dto.permissions = req.body.permissions;
    dto.roleName = req.body.roleName;
    return dto;
  }
}
