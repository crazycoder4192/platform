import express from 'express';
import IController from '../../common/controller-interface';
import authenticationGuard from '../../guards/authentication.guard';
import { InvoiceGenerationController } from '../advertiser/invoice-generation.controller';
import { PublisherTransformedHCPController } from '../publisher/publisher-transformed-hcp.controller';
import { AdvertiserPaymentController } from './advertiser-payment.controller';
import { BidRangeController } from './bidrange.controller';
import { CountryCurrencyController } from './country-currency.controller';
import { CountryTaxController } from './country-tax.controller';
import { CreditVoucherController } from './credit-voucher.controller';
import { PermissionController } from './permission.controller';
import { PublisherPaymentController } from './publisher-payment.controller';
import { PublisherController } from './publisher.controller';

import { AdminDashboardController } from '../admin/admindashboard.controller';
import { BillController } from '../publisher/bill.controller';
import { AdminAnalyticsController } from './analytics.controller';
import { InterestedUsersController } from './interested-users.controller';
import { PublisherBidRangeController } from './publisher-bidrange.controller';
import { PublisherSiteWeightageController } from './publisher-site-weightage.controller';
import { RoleController } from './role.controller';
import { SchedulePaymentsController } from './schedule-payment.controller';
import { SmtpController } from './smtp.controller';
import { SupportController } from './support.controller';
import { UserController } from './user.controller';
import { WeightageByValidatedHcpController } from './weightage-by-validated-hcp.controller';

export class AdminController implements IController {
  public path = '/admin';
  public router = express.Router();

  private readonly smtpController: SmtpController;
  private readonly roleController: RoleController;
  private readonly permissionController: PermissionController;
  private readonly userController: UserController;
  private readonly countryTaxController: CountryTaxController;
  private readonly bidRangeController: BidRangeController;
  private readonly countryCurrencyController: CountryCurrencyController;
  private readonly creditVoucherController: CreditVoucherController;
  private readonly schedulePaymentsController: SchedulePaymentsController;
  private readonly publisherBidRangeController: PublisherBidRangeController;
  private readonly weightageByValidatedHcpController: WeightageByValidatedHcpController;
  private readonly publisherSiteWeightageController: PublisherSiteWeightageController;
  private readonly supportController: SupportController;
  private readonly publisherPaymentController: PublisherPaymentController;
  private readonly publisherController: PublisherController;
  private readonly advertiserPaymentController: AdvertiserPaymentController;
  private readonly invoiceGenerationController: InvoiceGenerationController;
  private readonly publisherTransformedHCPController: PublisherTransformedHCPController;
  private readonly billController: BillController;
  private readonly interestedUsersController: InterestedUsersController;
  private readonly adminDashboardController: AdminDashboardController;
  private readonly adminAnalyticsController: AdminAnalyticsController;

  constructor() {
    this.smtpController = new SmtpController();
    this.roleController = new RoleController();
    this.permissionController = new PermissionController();
    this.userController = new UserController();
    this.countryTaxController = new CountryTaxController();
    this.bidRangeController = new BidRangeController();
    this.countryCurrencyController = new CountryCurrencyController();
    this.creditVoucherController = new CreditVoucherController();
    this.schedulePaymentsController = new SchedulePaymentsController();
    this.publisherBidRangeController = new PublisherBidRangeController();
    this.publisherSiteWeightageController = new PublisherSiteWeightageController();
    this.weightageByValidatedHcpController = new WeightageByValidatedHcpController();
    this.supportController = new SupportController();
    this.publisherPaymentController = new PublisherPaymentController();
    this.publisherController = new PublisherController();
    this.advertiserPaymentController = new AdvertiserPaymentController();
    this.invoiceGenerationController = new InvoiceGenerationController();
    this.publisherTransformedHCPController = new PublisherTransformedHCPController();
    this.billController = new BillController();
    this.interestedUsersController = new InterestedUsersController();
    this.adminDashboardController = new AdminDashboardController();
    this.adminAnalyticsController = new AdminAnalyticsController();
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getRoot);
    this.router.get(`${this.path}/smtpdetails`, authenticationGuard, this.smtpController.getSMTPDetails);
    this.router.post(`${this.path}/smtpdetails`, authenticationGuard, this.smtpController.createSMTPDetails);
    this.router.put(`${this.path}/smtpdetails`, authenticationGuard, this.smtpController.updateSMTPDetails);

    this.router.get(`${this.path}/countrytaxes`, authenticationGuard, this.countryTaxController.getAllCountryTaxes);
    this.router.post(`${this.path}/countrytaxes`, authenticationGuard, this.countryTaxController.createCountryTax);
    this.router.put(`${this.path}/countrytaxes/:id`, authenticationGuard, this.countryTaxController.updateCountryTax);
    this.router.get(`${this.path}/countrytaxes/:id`, authenticationGuard, this.countryTaxController.getCountryTax);
    this.router.delete(`${this.path}/countrytaxes/:id`, authenticationGuard, this.countryTaxController.deleteCountryTax);

    this.router.get(`${this.path}/userpermissions`, authenticationGuard, this.permissionController.getUserPermissions);

    this.router.get(`${this.path}/userroles`, authenticationGuard, this.roleController.getUserRoles);
    this.router.post(`${this.path}/userroles`, authenticationGuard, this.roleController.createUserRoles);
    this.router.put(`${this.path}/userroles`, authenticationGuard, this.roleController.updateUserRoles);

    this.router.post(`${this.path}/addUser`, authenticationGuard, this.userController.addUser);
    this.router.get(`${this.path}/users`, authenticationGuard, this.userController.getAllAdminUsers);
    this.router.get(`${this.path}/users/:id`, authenticationGuard, this.userController.getAdminUser);
    this.router.put(`${this.path}/users/:id`, authenticationGuard, this.userController.updateAdminUser);
    this.router.delete(`${this.path}/users/:id`, authenticationGuard, this.userController.deleteAdminUser);
    this.router.get(`${this.path}/userDetails/:id`, authenticationGuard, this.userController.getUserDetailsByEmail);
    this.router.delete(`${this.path}/userDetails/:id`, authenticationGuard, this.userController.deleteUserDetailsByEmail);

    this.router.get(`${this.path}/bidranges`, authenticationGuard, this.bidRangeController.getAllBidRanges);
    this.router.post(`${this.path}/bidranges`, authenticationGuard, this.bidRangeController.createBidRange);
    this.router.put(`${this.path}/bidranges/:id`, authenticationGuard, this.bidRangeController.updateBidRange);
    this.router.get(`${this.path}/bidranges/:id`, authenticationGuard, this.bidRangeController.getBidRange);
    this.router.delete(`${this.path}/bidranges/:id`, authenticationGuard, this.bidRangeController.deleteBidRange);

    this.router.get(`${this.path}/countriescurrencies`, authenticationGuard, this.countryCurrencyController.getAllCountryCurrencies);
    this.router.post(`${this.path}/countriescurrencies`, authenticationGuard, this.countryCurrencyController.createCountryCurrency);
    this.router.put(`${this.path}/countriescurrencies/:id`, authenticationGuard, this.countryCurrencyController.updateCountryCurrency);
    this.router.get(`${this.path}/countriescurrencies/:id`, authenticationGuard, this.countryCurrencyController.getCountryCurrency);
    this.router.delete(`${this.path}/countriescurrencies/:id`, authenticationGuard, this.countryCurrencyController.deleteCountryCurrency);

    this.router.get(`${this.path}/creditvouchers`, authenticationGuard, this.creditVoucherController.getAllCreditVouchers);
    this.router.post(`${this.path}/creditvouchers`, authenticationGuard, this.creditVoucherController.createCreditVoucher);
    this.router.put(`${this.path}/creditvouchers/:id`, authenticationGuard, this.creditVoucherController.updateCreditVoucher);
    this.router.get(`${this.path}/creditvouchers/:id`, authenticationGuard, this.creditVoucherController.getCreditVoucher);
    this.router.delete(`${this.path}/creditvouchers/:id`, authenticationGuard, this.creditVoucherController.deleteCreditVoucher);

    this.router.get(`${this.path}/schedulepayments`, authenticationGuard, this.schedulePaymentsController.getAllSchedulePayments);
    this.router.post(`${this.path}/schedulepayments`, authenticationGuard, this.schedulePaymentsController.createSchedulePayment);
    this.router.put(`${this.path}/schedulepayments/:id`, authenticationGuard, this.schedulePaymentsController.updateSchedulePayment);
    this.router.get(`${this.path}/schedulepayments/:id`, authenticationGuard, this.schedulePaymentsController.getSchedulePayment);
    this.router.delete(`${this.path}/schedulepayments/:id`, authenticationGuard, this.schedulePaymentsController.deleteSchedulePayment);

    this.router.get(`${this.path}/publisherbidranges`, authenticationGuard, this.publisherBidRangeController.getAllBidRanges);
    this.router.post(`${this.path}/publisherbidranges`, authenticationGuard, this.publisherBidRangeController.createBidRange);
    this.router.put(`${this.path}/publisherbidranges/:id`, authenticationGuard, this.publisherBidRangeController.updateBidRange);
    this.router.get(`${this.path}/publisherbidranges/:id`, authenticationGuard, this.publisherBidRangeController.getBidRange);
    this.router.delete(`${this.path}/publisherbidranges/:id`, authenticationGuard, this.publisherBidRangeController.deleteBidRange);

    this.router.get(`${this.path}/weightagebyvalidatedhcps`, authenticationGuard, this.weightageByValidatedHcpController.getAllWeightageByValidatedHcps); //tslint:disable-line
    this.router.post(`${this.path}/weightagebyvalidatedhcps`, authenticationGuard, this.weightageByValidatedHcpController.createWeightageByValidatedHcp); //tslint:disable-line
    this.router.put(`${this.path}/weightagebyvalidatedhcps/:id`, authenticationGuard, this.weightageByValidatedHcpController.updateWeightageByValidatedHcp); //tslint:disable-line
    this.router.get(`${this.path}/weightagebyvalidatedhcps/:id`, authenticationGuard, this.weightageByValidatedHcpController.getWeightageByValidatedHcp); //tslint:disable-line
    this.router.delete(`${this.path}/weightagebyvalidatedhcps/:id`, authenticationGuard, this.weightageByValidatedHcpController.deleteWeightageByValidatedHcp); //tslint:disable-line

    this.router.get(`${this.path}/publishersiteweightages`, authenticationGuard, this.publisherSiteWeightageController.getAllPublisherSiteWeightages);
    this.router.post(`${this.path}/publishersiteweightages`, authenticationGuard, this.publisherSiteWeightageController.createPublisherSiteWeightage);
    this.router.put(`${this.path}/publishersiteweightages/:id`, authenticationGuard, this.publisherSiteWeightageController.updatePublisherSiteWeightage); //tslint:disable-line
    this.router.get(`${this.path}/publishersiteweightages/:id`, authenticationGuard, this.publisherSiteWeightageController.getPublisherSiteWeightage);
    this.router.delete(`${this.path}/publishersiteweightages/:id`, authenticationGuard, this.publisherSiteWeightageController.deletePublisherSiteWeightage); //tslint:disable-line

    // support
    this.router.post(`${this.path}/supportticket`, authenticationGuard, this.supportController.createTicket);
    this.router.put(`${this.path}/supportticket`, authenticationGuard, this.supportController.updateTicket);
    this.router.get(`${this.path}/supportticket`, authenticationGuard, this.supportController.getTicketsByEmail);
    this.router.post(`${this.path}/supportticket/comment`, authenticationGuard, this.supportController.addComment);
    this.router.get(`${this.path}/supportticket/:id`, authenticationGuard, this.supportController.getTicketComment);
    this.router.get(`${this.path}/supportticketbyemail`, authenticationGuard, this.supportController.getTicketsByEmail);
    this.router.get(`${this.path}/supportarticles`, authenticationGuard, this.supportController.getTitles);
    // end
    this.router.post(`${this.path}/publisherpayment/:id`, authenticationGuard, this.publisherPaymentController.updatePublisherPayment);
    this.router.get(`${this.path}/publisher`, authenticationGuard, this.publisherController.getPublishers);
    this.router.post(`${this.path}/publisher/platform`, authenticationGuard, this.publisherController.getPlatformsbyPublisherIds);

    this.router.get(`${this.path}/provider/invoices/:type`, authenticationGuard, this.advertiserPaymentController.getAdvertiserInvoices);

    this.router.put(`${this.path}/invoices/updateqbinvoice`, authenticationGuard,
    this.invoiceGenerationController.addMissingQuickbooksInvoices);
    this.router.put(`${this.path}/invoices/updateqbpayment`, authenticationGuard,
    this.invoiceGenerationController.addMissingQuickbooksPayments);
    this.router.post(`${this.path}/invoice/processpending`, authenticationGuard, this.invoiceGenerationController.processAllPendingInvoices);

    this.router.get(`${this.path}/hcp/notifystatus/:platformId`, authenticationGuard,
    this.publisherTransformedHCPController.notifyHcpValidationStatus);

    this.router.get(`${this.path}/publisher/bills/:type`, authenticationGuard,
    this.billController.generatePublisherBillsForAdmin);
    this.router.put(`${this.path}/publisher/bills`, authenticationGuard,
    this.billController.markBillAsPaid);
    this.router.get(`${this.path}/interestedusers`, authenticationGuard,
    this.interestedUsersController.getAllInterestedUsers);
    this.router.get(`${this.path}/interestedusers/emailsent/:id`, authenticationGuard,
    this.interestedUsersController.updateToEmailSent);
    this.router.get(`${this.path}/dashboard/topfiveadvertisers`, authenticationGuard,
    this.adminDashboardController.getTop5Advertisers);
    this.router.get(`${this.path}/dashboard/topfivepublishers`, authenticationGuard,
    this.adminDashboardController.getTop5Publishers);
    this.router.get(`${this.path}/dashboard/topadvertisers`, authenticationGuard,
    this.adminDashboardController.getTopAdvertisers);
    this.router.get(`${this.path}/dashboard/toppublishers`, authenticationGuard,
    this.adminDashboardController.getTopPublishers);
    this.router.get(`${this.path}/dashboard/topassets`, authenticationGuard,
    this.adminDashboardController.getTop5Assets);
    this.router.get(`${this.path}/dashboard/topcampaigns`, authenticationGuard,
    this.adminDashboardController.getTop5Campaigns);
    this.router.post(`${this.path}/analytics`, authenticationGuard,
    this.adminAnalyticsController.getAnalyticsData);
    this.router.post(`${this.path}/getBankDetails`, authenticationGuard,
    this.adminAnalyticsController.getBankDetails);
    }
  private readonly getRoot = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.send('Doceree Admin API page');
  }
}
