import express from 'express';
import PublisherSiteWeightageDAO from '../../daos/admin/publisher-site-weightage.dao';
import PublisherSiteWeightageDTO from '../../dtos/admin/publisher-site-weightage.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class PublisherSiteWeightageController {
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly publisherSiteWeightageDAO: PublisherSiteWeightageDAO;

  constructor() {
    this.authorizeAdmin = new AuthorizeAdmin();
    this.publisherSiteWeightageDAO = new PublisherSiteWeightageDAO();
  }

  public getAllPublisherSiteWeightages = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const publisherSiteWeightages: PublisherSiteWeightageDTO[] = await this.publisherSiteWeightageDAO.getAll();
      res.json(publisherSiteWeightages);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createPublisherSiteWeightage = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      await throwIfInputValidationError(req);
      const dto: PublisherSiteWeightageDTO = this.toPublisherSiteWeightageDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.publisherSiteWeightageDAO.create(dto);
      res.json({ success: true, message: 'site weightage added successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updatePublisherSiteWeightage = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);
      const dto: PublisherSiteWeightageDTO = this.toPublisherSiteWeightageDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.publisherSiteWeightageDAO.update(req.params.id, dto);
      res.json({ success: true, message: 'site weightage updated successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getPublisherSiteWeightage = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const PublisherSiteWeightage: PublisherSiteWeightageDTO = await this.publisherSiteWeightageDAO.getById(req.params.id);
      res.json(PublisherSiteWeightage);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deletePublisherSiteWeightage = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const PublisherSiteWeightage: PublisherSiteWeightageDTO = await this.publisherSiteWeightageDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: 'site weightage deleted successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  private toPublisherSiteWeightageDTO(req: IAuthenticatedRequest): PublisherSiteWeightageDTO {
    const dto: PublisherSiteWeightageDTO = new PublisherSiteWeightageDTO();
    dto.weightage = req.body.weightage;
    return dto;
  }
}
