import express from 'express';
import PermissionDAO from '../../daos/admin/permission.dao';
import PermissionDTO from '../../dtos/admin/permission.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';

export class PermissionController {
  private readonly permissionDAO: PermissionDAO;

  constructor() {
    this.permissionDAO = new PermissionDAO();
  }

  public getUserPermissions = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    if (req && req.query && req.query.id) {
      try {
        const permissions: PermissionDTO[] = await this.permissionDAO.findByIds(
          req.query.id,
        );
        res.json(permissions);
      } catch (err) {
        catchError(err, next);
      }
    } else if (req && req.query && req.query.moduleName) {
      try {
        const permissions: any[] = await this.permissionDAO.findByModule(
          req.query.moduleName,
        );
        res.json(permissions);
      } catch (err) {
        catchError(err, next);
      }
    } else {
      try {
        const permissions: PermissionDTO[] = await this.permissionDAO.findAll();
        res.json(permissions);
      } catch (err) {
        catchError(err, next);
      }
    }
  }
}
