import config from 'config';
import express from 'express';
import moment from 'moment';
import PasswordHistoryDAO from '../../daos/password-history.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import PasswordHistoryDTO from '../../dtos/password-history.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { HashPwd } from '../../util/hash-pwd';
import { Mail } from '../../util/mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { AuthorizeAdmin } from './authorize-admin';

export class UserController {
  private readonly hashPwd: HashPwd;
  private readonly mail: Mail;

  private readonly userDAO: UserDAO;
  private readonly userTokenDAO: UserTokenDAO;
  private readonly passwordHistoryDAO: PasswordHistoryDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly generalMail: GeneralMail;

  constructor() {
    this.hashPwd = new HashPwd();
    this.mail = new Mail();
    this.userDAO = new UserDAO();
    this.userTokenDAO = new UserTokenDAO();
    this.passwordHistoryDAO = new PasswordHistoryDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
    this.generalMail = new GeneralMail();
  }

  public addUser = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email does not appear to be valid').isEmail();

    // check the validation object for errors
    const validationErrors = req.validationErrors();
    if (validationErrors) {
      return res.json({
        message: 'Request body has no valid data',
        success: false,
      });
    }

    const info: IVerifiedInfo = await this.verifyEmailDomain(req.body.email);
    if (!info.success) {
      throw new HandledApplicationError(422, 'The email entered do not belong to valid domain');
    }

    const userDetail: UserDTO = new UserDTO();
    userDetail.created = { at: new Date(), by: req.email };
    userDetail.email = req.body.email;
    userDetail.userRole = req.body.userRole;
    userDetail.userStatus = 'pending';
    userDetail.userType = req.body.userType;
    userDetail.hasPassword = false;
    userDetail.hasPersonalProfile = false;
    userDetail.hasSecurityQuestions = false;

    try {
      const user: UserDTO = await this.userDAO.findByEmail(req.body.email);
      // if a user was found, that means the user's email matches the entered email
      if (user) {
        return res.json({
          message:
            'A user with that email has already registered. Please use a different email',
          success: false,
        });
      }

      let tokenStr: string;
      try {
        const createdUser: UserDTO = await this.userDAO.create(userDetail);

        const jwtExpiresInVal = config.get<string>('jwt.expiresIn');
        const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
        tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);

        const tokenDTO: UserTokenDTO = new UserTokenDTO();
        tokenDTO.email = req.body.email;
        tokenDTO.expiryTime = new Date(expiresIn);
        tokenDTO.token = tokenStr;
        tokenDTO.tokenType = 'addUser';
        tokenDTO.created = { at: new Date(), by: req.email };
        await this.userTokenDAO.create(tokenDTO);
      } catch (err) {
        return res.json({
          message: `Error while saving user details ${err}`,
          success: false,
        });
      }

      const sendStatus: boolean = await this.sendMail(
        req.body.email,
        req.body.firstName,
        tokenStr
      );
      if (sendStatus) {
        return res.json({ message: 'Added successfully.', success: true });
      } else {
        return res.json({ message: 'Email not sent', success: false });
      }
    } catch (err) {
      return res.json({ message: `Error while saving user details ${err}`, success: false });
    }
  }

  public getAllAdminUsers = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const adminUsers: UserDTO[] = await this.userDAO.getAllAdminUsers(req.email);
      res.json(adminUsers);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getAdminUser = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const adminUser: UserDTO = await this.userDAO.getById(req.params.id);
      res.json(adminUser);
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateAdminUser = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const userDetails: UserDTO = new UserDTO();
      userDetails.userRole = req.body.userRole;
      userDetails.modified = { at: new Date(), by: req.email };
      await this.userDAO.update(req.params.id, userDetails);
      res.json({ success: true, message: 'User updated successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteAdminUser = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const userDetails: UserDTO = new UserDTO();
      userDetails.userStatus = 'Deleted';
      userDetails.modified = { at: new Date(), by: req.email };
      await this.userDAO.update(req.params.id, userDetails);
      res.json({ success: true, message: 'User deleted successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getUserDetailsByEmail = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const userDetails: UserDTO = await this.userDAO.getUserByEmail(req.params.id);
      res.json(userDetails);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteUserDetailsByEmail = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const userDetails: any = await this.userDAO.deleteUserByEmail(req.params.id);
      res.json(userDetails);
    } catch (err) {
      catchError(err, next);
    }
  }

  private async verifyEmailDomain(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const verifier = require('email-exist');
      verifier.verify(email, (err: any, info: any) => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(info);
        }
      });
    });
  }

  private async sendMail(
    email: string,
    firstName: string,
    jwtToken: string,
  ): Promise<boolean> {
    const url: string = config.get<string>('mail.web-portal-redirection-url');
    return await this.generalMail.sendRegistrationCompletedAdvertiserMail(
      email,
      `${url}/verifytoken?token=${jwtToken}&tokenType=addUser`
    );
  }

}

interface IVerifiedInfo {
  success: boolean;
  info: string;
  addr: string;
  response: string;
}
