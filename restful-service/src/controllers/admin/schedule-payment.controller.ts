import config from 'config';
import express from 'express';
import cron from 'node-cron';
import ConfigSchedulePaymentDAO from '../../daos/admin/config-schedulepayments.dao';
import AdvertiserPaymentTransactionDAO from '../../daos/advertiser/advertiser-payment-transaction.dao';
import InvoiceDAO from '../../daos/advertiser/invoice.dao';
import AdvertiserPaymentMethodDAO from '../../daos/advertiser/payment-method.dao';
import ConfigSchedulePaymentDTO from '../../dtos/admin/config-schedulepayments.dtos';
import AdvertiserPaymentTransactionDTO from '../../dtos/advertiser/advertiser-payment-transactions.dto';
import InvoiceDTO from '../../dtos/advertiser/invoice.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
const ApiContracts = require('authorizenet').APIContracts;
const ApiControllers = require('authorizenet').APIControllers;
import { AuthorizeNetHelper } from '../util/authorize-net-helper';

export class SchedulePaymentsController {
    private readonly configSchedulePaymentDAO: ConfigSchedulePaymentDAO;
    private readonly invoiceDAO: InvoiceDAO;
    private readonly authorizeNetHelper: AuthorizeNetHelper;
    private readonly advertiserPaymentMethodDAO: AdvertiserPaymentMethodDAO;
    private readonly advertiserPaymentTransactionDAO: AdvertiserPaymentTransactionDAO;
    constructor() {
        this.configSchedulePaymentDAO = new ConfigSchedulePaymentDAO();
        this.invoiceDAO = new InvoiceDAO();
        this.advertiserPaymentMethodDAO = new AdvertiserPaymentMethodDAO();
        this.advertiserPaymentTransactionDAO = new AdvertiserPaymentTransactionDAO();
        this.authorizeNetHelper = new AuthorizeNetHelper();
    }

    public getAllSchedulePayments = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const schedulePayments: ConfigSchedulePaymentDTO[] = await this.configSchedulePaymentDAO.getAll();
            res.json(schedulePayments);
        } catch (err) {
            catchError(err, next);
        }
    }

    public createSchedulePayment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            req
                .checkBody('scheduleDayofMonth', "Mandatory field 'scheduleDayofMonth' missing in request body")
                .exists();
            await throwIfInputValidationError(req);
            let scheduledFlag = false;
            if (req.body.enableSchedule === 'Yes') {
                scheduledFlag = true;
            }
            // schedule all pending invoices using cron job
            cron.schedule(`* 01 ${req.body.scheduleDayofMonth} * *`, async () => {
                await this.processPendingInvoices(req.email, req.header('zone'));
            }, { scheduled: scheduledFlag });

            const dto: ConfigSchedulePaymentDTO = this.toConfigSchedulePaymentDTO(req);
            dto.created = { at: new Date(), by: req.email };
            await this.configSchedulePaymentDAO.create(dto);
            res.json({ success: true, message: 'schedule day added successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateSchedulePayment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            req
                .checkBody('_id', "Mandatory field '_id' missing in request body")
                .exists();
            await throwIfInputValidationError(req);
            req
                .checkBody('scheduleDayofMonth', "Mandatory field 'scheduleDayofMonth' missing in request body")
                .exists();
            await throwIfInputValidationError(req);
            let scheduledFlag = false;
            if (req.body.enableSchedule === 'Yes') {
                scheduledFlag = true;
            }

            cron.schedule(`* 01 ${req.body.scheduleDayofMonth} * *`, async () => {
                await this.processPendingInvoices(req.email, req.header('zone'));
            }, { scheduled: scheduledFlag });

            const dto: ConfigSchedulePaymentDTO = this.toConfigSchedulePaymentDTO(req);
            dto.modified = { at: new Date(), by: req.email };
            await this.configSchedulePaymentDAO.update(req.params.id, dto);
            res.json({ success: true, message: 'schedule payments updated successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getSchedulePayment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const schedulePayment: ConfigSchedulePaymentDTO = await this.configSchedulePaymentDAO.getById(req.params.id);
            res.json(schedulePayment);
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteSchedulePayment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            req
                .checkParams('id', "Mandatory field 'id' missing in request param")
                .exists();
            await throwIfInputValidationError(req);
            const updateObj: any = {
                deleted: true,
                modified: { at: new Date(), by: req.email }
            };
            await this.configSchedulePaymentDAO.delete(req.params.id, updateObj);
            res.json({ success: true, message: 'schedule payment deleted successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }
    private async processPendingInvoices(emailId: string, zone: string) {
        const listOfInvoices = await this.invoiceDAO.getPending();
        if (listOfInvoices != null) {
            listOfInvoices.forEach(async (invoice) => {
                const paymentDetails = await this.advertiserPaymentMethodDAO.findById(invoice.paymentMethodId, zone);
                const ctrl = await this.authorizeNetHelper.chargeCustomerProfile(paymentDetails.customerId,
                    paymentDetails.customerProfileId, invoice.invoiceNumber, +invoice.amount);
                ctrl.execute(async () => {
                    const apiResponse = ctrl.getResponse();
                    const response = new ApiContracts.CreateTransactionResponse(apiResponse);
                    if (response != null) {
                        if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                            if (response.getTransactionResponse().getMessages() != null) {
                                const invoiceUpdateObject: InvoiceDTO = new InvoiceDTO();
                                invoiceUpdateObject.invoiceStatus = 'paid';
                                this.invoiceDAO.update(invoice._id, invoiceUpdateObject, zone);

                                const transaction: AdvertiserPaymentTransactionDTO = new AdvertiserPaymentTransactionDTO();
                                transaction.created = { at: new Date(), by: emailId };
                                transaction.paymentMethodId = paymentDetails._id;
                                transaction.amount = invoice.amount;
                                transaction.invoiceNumber = invoice.invoiceNumber;
                                transaction.accountNumber = response.getTransactionResponse().getAccountNumber();
                                transaction.accountType = response.getTransactionResponse().getAccountType();
                                transaction.responseCode = response.getTransactionResponse().getResponseCode();
                                transaction.transactionId = response.getTransactionResponse().getRefTransID();
                                this.advertiserPaymentTransactionDAO.create(transaction);
                                // throw new HandledApplicationError(200, response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                            } else {
                                if (response.getTransactionResponse().getErrors() != null) {
                                    throw new HandledApplicationError(422,
                                        response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                                }
                            }
                        } else {
                            if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
                                throw new HandledApplicationError(422, response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                            } else {
                                throw new HandledApplicationError(422, response.getMessages().getMessage()[0].getText());
                            }
                        }
                    } else {
                        throw new HandledApplicationError(422, 'Null Response.');
                    }
                });
            });
        }
    }

    private toConfigSchedulePaymentDTO(req: IAuthenticatedRequest): ConfigSchedulePaymentDTO {
        const dto: ConfigSchedulePaymentDTO = new ConfigSchedulePaymentDTO();
        dto.scheduleDayofMonth = req.body.scheduleDayofMonth;
        dto.enableSchedule = req.body.enableSchedule;
        return dto;
    }
}
