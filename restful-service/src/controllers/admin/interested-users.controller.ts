import express from 'express';
import mongoose from 'mongoose';
import InterestedUserDAO from '../../daos/interested-user.dao';
import InteresetedUserDTO from '../../dtos/interested-user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class InterestedUsersController {
    private readonly interestedUserDAO: InterestedUserDAO;
    private readonly authorizeAdmin: AuthorizeAdmin;

    constructor() {
        this.interestedUserDAO = new InterestedUserDAO();
        this.authorizeAdmin = new AuthorizeAdmin();
    }

    public getAllInterestedUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            const interestedUsers: InteresetedUserDTO[] = await this.interestedUserDAO.findAll();
            res.json(interestedUsers);
        } catch (err) {
          catchError(err, next);
        }
    }

    public updateToEmailSent = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
      try {
          await this.authorizeAdmin.authorize(req.email);
          const interestedUser: InteresetedUserDTO = await this.interestedUserDAO.findById(req.params.id);
          interestedUser.modified.at = new Date();
          interestedUser.modified.by = req.email;
          interestedUser.status = 'EmailSent';
          await this.interestedUserDAO.update(req.params.id, interestedUser);
          res.json({ message: 'Record Updated Successfully' });
      } catch (err) {
        catchError(err, next);
      }
  }
}
