import express from 'express';
import mongoose from 'mongoose';
import PermissionDAO from '../../daos/admin/permission.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserDAO from '../../daos/user.dao';
import PermissionDTO from '../../dtos/admin/permission.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { UserStatuses, UserTypes } from '../../util/constants';
import { GeneralMail } from '../../util/mail/general-mail';
import { logger } from '../../util/winston';
import { AuthorizeAdmin } from './authorize-admin';

export class PublisherController {
    public router = express.Router();
    private readonly publisherDAO: PublisherDAO;
    private readonly publisherPlatformDAO: PublisherPlatformDAO;

    constructor() {
      this.publisherDAO = new PublisherDAO();
      this.publisherPlatformDAO = new PublisherPlatformDAO();
    }

    public getPublishers = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
      ) => {
        try {
            req.checkBody('email', 'Email is required').notEmpty();
            req.checkBody('email', 'Email does not appear to be valid').isEmail();
        /*    const info: IVerifiedInfo = await this.verifyEmailDomain(req.email);
            if (!info.success) {
              throw new HandledApplicationError(422, 'The email entered do not belong to valid domain');
            }*/
            const details: PublisherDTO[] = await this.publisherDAO.findPublishersForAdmin();
            if (details) {
                res.status(200).json(details);
            } else {
                res.status(200).json(null);
            }
        } catch (err) {
          catchError(err, next);
        }
      }
    public getPlatformsbyPublisherIds = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
      try {
        req.checkBody('email', 'Email is required').notEmpty();
        req.checkBody('email', 'Email does not appear to be valid').isEmail();
        const info: IVerifiedInfo = await this.verifyEmailDomain(req.email);
        if (!info.success) {
          throw new HandledApplicationError(422, 'The email entered do not belong to valid domain');
        }
        const arrayOfIds: any[] = [];
        for (const id of req.body) {
          arrayOfIds.push(mongoose.Types.ObjectId(id));
        }
        const details: any = await this.publisherPlatformDAO.findPlatformsByMultiplePublisherIds(arrayOfIds, req.header('zone'));
        if (details) {
            res.status(200).json(details);
        } else {
            res.status(200).json(null);
        }
      } catch (err) {
        catchError(err, next);
      }
    }

    private async verifyEmailDomain(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
        const verifier = require('email-exist');
        verifier.verify(email, (err: any, info: any) => {
        if (err !== null) {
            reject(err);
        } else {
            resolve(info);
        }
        });
    });
    }
}

interface IVerifiedInfo {
    success: boolean;
    info: string;
    addr: string;
    response: string;
  }
