import express from 'express';
import ConfigEmailDAO from '../../daos/admin/config-email.dao';
import ConfigEmailDTO from '../../dtos/admin/config-email.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class SmtpController {
  private readonly configEmailDAO: ConfigEmailDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;

  constructor() {
    this.configEmailDAO = new ConfigEmailDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
  }

  public getSMTPDetails = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    await this.authorizeAdmin.authorize(req.email);
    if (req && req.query && req.query.id) {
      try {
        req
          .checkQuery('id', "Mandatory field 'id' missing in request query")
          .exists();
        await throwIfInputValidationError(req);
        const emailConfig: ConfigEmailDTO = await this.configEmailDAO.findById(
          req.query.id,
        );
        res.json(emailConfig);
      } catch (err) {
        catchError(err, next);
      }
    } else {
      try {
        const emailConfig: ConfigEmailDTO[] = await this.configEmailDAO.find();
        res.json(emailConfig);
      } catch (err) {
        catchError(err, next);
      }
    }
  }

  public createSMTPDetails = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateConfigEmailInput(req);
      await throwIfInputValidationError(req);

      const dto: ConfigEmailDTO = this.toConfigEmailDTO(req);
      dto.created = { at: new Date(), by: req.email };

      await this.configEmailDAO.create(dto);
      res.json({ message: 'SMTP details added successfully', success: true });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateSMTPDetails = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateConfigEmailInput(req);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);

      const dto: ConfigEmailDTO = this.toConfigEmailDTO(req);
      dto.modified = { at: new Date(), by: req.email };

      await this.configEmailDAO.update(req.body._id, dto);
      res.json({ message: 'SMTP details updated successfully', success: true });
    } catch (err) {
      catchError(err, next);
    }
  }

  private validateConfigEmailInput(req: IAuthenticatedRequest) {
    req
      .checkBody('host', "Mandatory field 'host' missing in request body")
      .exists();
    req
      .checkBody('port', "Mandatory field 'port' missing in request body")
      .exists();
    req
      .checkBody('secure', "Mandatory field 'secure' missing in request body")
      .exists();
    req
      .checkBody('user', "Mandatory field 'user' missing in request body")
      .exists();
    req
      .checkBody('pass', "Mandatory field 'pass' missing in request body")
      .exists();
    req
      .checkBody('activestate', "Mandatory field 'activestate' missing in request body")
      .exists();
  }

  private toConfigEmailDTO(req: IAuthenticatedRequest): ConfigEmailDTO {
    const dto: ConfigEmailDTO = new ConfigEmailDTO();
    dto.host = req.body.host;
    dto.port = req.body.port;
    dto.secure = req.body.secure;
    dto.user = req.body.user;
    dto.pass = req.body.pass;
    dto.category = req.body.category;
    dto.activestate = req.body.activestate;
    return dto;
  }
}
