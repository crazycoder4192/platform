import express from 'express';
import mongoose from 'mongoose';
import DashboardDAO from '../../daos/admin/dashboard.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class AdminDashboardController {
    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly subCampaignDAO: SubCampaignDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly publisherAssetDAO: PublisherAssetDAO;
    private readonly publisherDAO: PublisherDAO;
    private readonly dashboardDAO: DashboardDAO;

    constructor() {
        this.authorizeAdmin = new AuthorizeAdmin();
        this.subCampaignDAO = new SubCampaignDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.publisherAssetDAO = new PublisherAssetDAO();
        this.publisherDAO = new PublisherDAO();
        this.dashboardDAO = new DashboardDAO();
    }

    public getTop5Advertisers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const topFiveAdvertisers: any[] = [];
            const advertiserIds: any[] = [];
            await this.authorizeAdmin.authorize(req.email);
            const advertiserDetails: any = await this.dashboardDAO.getLifetimeSpendsOfTop5AdvertisersForAdmin(req.header('zone'));
            advertiserDetails.forEach((ele: any) => {
                advertiserIds.push(mongoose.Types.ObjectId(ele._id));
            });
            const activeCampaigns = await this.subCampaignDAO.findCountOfActiveCampaignsByAdvertiserId(advertiserIds);
            const advertisersInfo = await this.advertiserDAO.getAdvertiserNamesByAdvertiserIds(advertiserIds);
            const advertiserNames = new Map();
            const advertiserActiveCampaigns = new Map();
            activeCampaigns.forEach((ele: any) => {
                const advertiserId: string = ele._id.advertiserId;
                advertiserActiveCampaigns.set(advertiserId.toString() , ele.activeCampaigns);
            });
            advertisersInfo.forEach((ele: any) => {
                const advertiserId: string = ele._id;
                advertiserNames.set(advertiserId.toString() , ele.businessName);
            });
            advertiserDetails.forEach((ele: any) => {
                const temp: any = { };
                temp._id = ele._id;
                const advertiserId: string = ele._id;
                temp.advertiser = advertiserNames.get(advertiserId.toString());
                temp.totalAmount = ele.totalAmount;
                temp.activeCampaigns = advertiserActiveCampaigns.get(advertiserId.toString()) !== undefined ?
                advertiserActiveCampaigns.get(advertiserId.toString()) : 0;
                temp.thisWeek = 0.00;
                topFiveAdvertisers.push(temp);
            });

            return res.json(topFiveAdvertisers);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getTop5Publishers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const topFivePublishers: any[] = [];
            const publisherIds: any[] = [];
            await this.authorizeAdmin.authorize(req.email);
            const publisherDetails: any = await this.dashboardDAO.getLifetimeEarningsOfTop5PublishersForAdmin(req.header('zone'));
            publisherDetails.forEach((ele: any) => {
                publisherIds.push(mongoose.Types.ObjectId(ele._id));
            });
            const activeAssets = await this.publisherAssetDAO.findCountOfActiveAssetsByPublisherIds(publisherIds);
            const publishersInfo = await this.publisherDAO.getPublisherNamesByPublisherIds(publisherIds);
            const publisherNames = new Map();
            const publisherActveAssets = new Map();
            activeAssets.forEach((ele: any) => {
                const publisherId: string = ele._id.publisherId;
                publisherActveAssets.set(publisherId.toString() , ele.activeAssets);
            });
            publishersInfo.forEach((ele: any) => {
                const publisherId: string = ele._id;
                publisherNames.set(publisherId.toString() , ele.organizationName);
            });
            publisherDetails.forEach((ele: any) => {
                const temp: any = { };
                temp._id = ele._id;
                const publisherId: string = ele._id;
                temp.publisher = publisherNames.get(publisherId.toString());
                temp.totalAmount = ele.totalAmount;
                temp.activeAssets = publisherActveAssets.get(publisherId.toString()) !== undefined ?
                publisherActveAssets.get(publisherId.toString()) : 0;
                temp.thisWeek = 0.00;
                topFivePublishers.push(temp);
            });

            return res.json(topFivePublishers);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getTopAdvertisers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const topFiveAdvertisers: any[] = [];
            const advertiserIds: any[] = [];
            await this.authorizeAdmin.authorize(req.email);
            const advertiserDetails: any = await this.dashboardDAO.getLifetimeSpendsOfTopAdvertisersForAdmin(req.header('zone'));
            advertiserDetails.forEach((ele: any) => {
                advertiserIds.push(mongoose.Types.ObjectId(ele._id));
            });
            const activeCampaigns = await this.subCampaignDAO.findCountOfActiveCampaignsByAdvertiserId(advertiserIds);
            const advertisersInfo = await this.advertiserDAO.getAdvertiserNamesByAdvertiserIds(advertiserIds);
            const advertiserNames = new Map();
            const advertiserActiveCampaigns = new Map();
            activeCampaigns.forEach((ele: any) => {
                const advertiserId: string = ele._id.advertiserId;
                advertiserActiveCampaigns.set(advertiserId.toString() , ele.activeCampaigns);
            });
            advertisersInfo.forEach((ele: any) => {
                const advertiserId: string = ele._id;
                advertiserNames.set(advertiserId.toString() , ele.businessName);
            });
            advertiserDetails.forEach((ele: any) => {
                const temp: any = { };
                temp._id = ele._id;
                const advertiserId: string = ele._id;
                temp.advertiser = advertiserNames.get(advertiserId.toString());
                temp.totalAmount = ele.totalAmount;
                temp.activeCampaigns = advertiserActiveCampaigns.get(advertiserId.toString()) !== undefined ?
                advertiserActiveCampaigns.get(advertiserId.toString()) : 0;
                temp.thisWeek = 0.00;
                topFiveAdvertisers.push(temp);
            });

            return res.json(topFiveAdvertisers);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getTopPublishers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const topFivePublishers: any[] = [];
            const publisherIds: any[] = [];
            await this.authorizeAdmin.authorize(req.email);
            const publisherDetails: any = await this.dashboardDAO.getLifetimeEarningsOfTopPublishersForAdmin(req.header('zone'));
            publisherDetails.forEach((ele: any) => {
                publisherIds.push(mongoose.Types.ObjectId(ele._id));
            });
            const activeAssets = await this.publisherAssetDAO.findCountOfActiveAssetsByPublisherIds(publisherIds);
            const publishersInfo = await this.publisherDAO.getPublisherNamesByPublisherIds(publisherIds);
            const publisherNames = new Map();
            const publisherActveAssets = new Map();
            activeAssets.forEach((ele: any) => {
                const publisherId: string = ele._id.publisherId;
                publisherActveAssets.set(publisherId.toString() , ele.activeAssets);
            });
            publishersInfo.forEach((ele: any) => {
                const publisherId: string = ele._id;
                publisherNames.set(publisherId.toString() , ele.organizationName);
            });
            publisherDetails.forEach((ele: any) => {
                const temp: any = { };
                temp._id = ele._id;
                const publisherId: string = ele._id;
                temp.publisher = publisherNames.get(publisherId.toString());
                temp.totalAmount = ele.totalAmount;
                temp.activeAssets = publisherActveAssets.get(publisherId.toString()) !== undefined ?
                publisherActveAssets.get(publisherId.toString()) : 0;
                temp.thisWeek = 0.00;
                topFivePublishers.push(temp);
            });

            return res.json(topFivePublishers);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getTop5Assets = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const topFiveAssets: any[] = [];
            const assetpublisherACSIDs: any[] = [];
            const assetIds: any[] = [];
            const platformIds: Set<string> = new Set();
            const platformEarningsMap = new Map();
            const platformIdMap = new Map();
            const assetNamesMap = new Map();
            const assetTypeMap = new Map();
            const assetIdMap = new Map();
            const assetUtilizationMap = new Map();
            await this.authorizeAdmin.authorize(req.email);
            const assetDetails: any = await this.dashboardDAO.getLifetimeEarningsOfTop5AssetsForAdmin(req.header('zone'));
            assetDetails.forEach((ele: any) => {
                assetpublisherACSIDs.push(ele._id);
            });
            const assets = await this.publisherAssetDAO.getAssetsForAdminByACSIDs(assetpublisherACSIDs);
            assets.forEach((ele: any) => {
                platformIds.add(ele.platformId);
                assetIds.push(ele._id.toString());
                assetIdMap.set(ele.codeSnippetId, ele._id);
                assetNamesMap.set(ele.codeSnippetId, ele.name);
                assetTypeMap.set(ele.codeSnippetId, ele.assetType);
                platformIdMap.set(ele.codeSnippetId, ele.platformId.toString());
            });
            const assetUtilization = await this.dashboardDAO.getLifetimeAssetUtilizationsOfTop5AssetsForAdmin(assetIds);
            assetUtilization.forEach((ele: any) => {
                assetUtilizationMap.set(ele._id.toString(), ele.utilizepercent);
            });
            const platformEarnings = await this.dashboardDAO.getLifetimeEaringnsOfPublishersByPublisherIdsForAdmin([...platformIds],
                req.header('zone'));
            platformEarnings.forEach((ele: any) => {
                platformEarningsMap.set(ele._id.toString(), ele.totalAmount);
            });
            assetDetails.forEach((ele: any) => {
                const temp: any = { };
                temp._id = assetIdMap.get(ele._id);
                temp.assetName = assetNamesMap.get(ele._id);
                temp.assetType = assetTypeMap.get(ele._id);
                temp.platformEarnings = platformEarningsMap.get(platformIdMap.get(ele._id));
                temp.assetEarnings = ele.totalAmount;
                temp.utilized = assetUtilizationMap.get(temp._id.toString());
                temp.thisWeek = 0.00;
                topFiveAssets.push(temp);
            });

            return res.json(topFiveAssets);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getTop5Campaigns = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const topFiveCampaigns: any[] = [];
            const subCampaignIds: any[] = [];
            const campaignIds = new Set();
            const subCampaignReachMap = new Map();
            const subCampaignNameMap = new Map();
            const subCampaignSpendsMap = new Map();
            const campaignSubCampaignIdsMap = new Map();
            const campaignNameMap = new Map();
            const subCampaigns = await this.dashboardDAO.getLifetimeSpendsOfTop5CampaignsForAdmin(req.header('zone'));
            subCampaigns.forEach((ele: any) => {
                subCampaignIds.push(mongoose.Types.ObjectId(ele._id));
                subCampaignSpendsMap.set(ele._id.toString(), ele.totalAmount);
            });
            const campaignReach = await this.dashboardDAO.getLifeTimeReachforCampaigns(subCampaignIds, req.header('zone'));
            campaignReach.forEach((ele: any) => {
                subCampaignReachMap.set(ele._id.toString(), ele.reach);
            });
            const subCampaignDetails = await this.subCampaignDAO.getSubCampaignDetailsByIdsForAdmin(subCampaignIds);
            subCampaignDetails.forEach((ele: any) => {
                campaignIds.add(ele.campaignId.toString());
                campaignSubCampaignIdsMap.set(ele._id.toString(), ele.campaignId.toString());
                subCampaignNameMap.set(ele._id.toString(), ele.name);
            });

            subCampaignDetails.forEach((ele: any) => {
                const temp: any = { };
                temp._id = ele._id;
                temp.subcampaignName = ele.name;
                temp.type = ele.creativeType;
                temp.spends = subCampaignSpendsMap.get(ele._id.toString());
                temp.costPerClick = ele.totalAmount;
                temp.reach = subCampaignReachMap.get(ele._id.toString());
                temp.thisWeek = 0.00;
                topFiveCampaigns.push(temp);
            });
            return res.json(topFiveCampaigns);
        } catch (err) {
            catchError(err, next);
        }
    }
}
