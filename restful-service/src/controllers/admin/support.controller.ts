import config from 'config';
import express from 'express';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdminMail } from '../../util/mail/admin-mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { logger } from '../../util/winston';
import { ZendeskHelper } from '../util/zendesk-helper';
import { AuthorizeAdmin } from './authorize-admin';

export class SupportController {
  private readonly userDAO: UserDAO;
  private readonly AdvertiserDAO: AdvertiserDAO;
  private readonly publisherDAO: PublisherDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly adminMail: AdminMail;
  private readonly userTokenDAO: UserTokenDAO;
  private readonly generalMail: GeneralMail;
  private readonly zendeskHelper: ZendeskHelper;
  private readonly publisherUserDAO: PublisherUserDAO;

  private readonly validPriorities: string[] = ['urgent', 'high', 'normal', 'low'];
  private readonly validStatuses: string[] = ['new', 'open', 'pending', 'hold', 'solved', 'closed'];

  constructor() {
    this.userDAO = new UserDAO();
    this.AdvertiserDAO = new AdvertiserDAO();
    this.publisherDAO = new PublisherDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
    this.adminMail = new AdminMail();
    this.userTokenDAO = new UserTokenDAO();
    this.generalMail = new GeneralMail();
    this.zendeskHelper = new ZendeskHelper();
    this.publisherUserDAO = new PublisherUserDAO();
  }

  public createTicket = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.body.email);
      if (userDTO) {
        const userFirstName: string = userDTO.firstName;
        const userLastName: string = userDTO.lastName;
        const userEmail: string = userDTO.email;
        const userExtId: string = userDTO._id.toString();
        const userType: string = userDTO.userType;

        let phoneNumber: string = '';
        let orgName: string = '';
        let orgExtId: string = '';
        if (userDTO.userType === 'advertiser') {
          const advertiserDTO: AdvertiserDTO = await this.AdvertiserDAO.findByEmail(userDTO.email);
          phoneNumber = advertiserDTO.phoneNumber;
          orgName = advertiserDTO.businessName;
          orgExtId = advertiserDTO._id.toString();
        }
        if (userDTO.userType === 'publisher') {
          const publisherUser = await this.publisherUserDAO.findByEmail(userDTO.email);
          const publisherDTO: PublisherDTO = await this.publisherDAO.findById(publisherUser.publisherId);
          phoneNumber = publisherDTO.phoneNumber;
          orgName = publisherDTO.organizationName;
          orgExtId = publisherDTO._id.toString();
        }

        let orgsResponse: any = await this.zendeskHelper.getOrg(orgExtId);
        if (!orgsResponse || orgsResponse.length === 0 || !orgsResponse.organizations[0] || orgsResponse.organizations[0].count === 0) {
          await this.zendeskHelper.createOrganization(orgName, orgExtId);
          orgsResponse = await this.zendeskHelper.getOrg(orgExtId);
        }

        let usersResponse: any = await this.zendeskHelper.getUser(userExtId);
        if (!usersResponse || usersResponse.length === 0 || !usersResponse.users[0] || usersResponse.users[0].count === 0) {
          await this.zendeskHelper.createUser(userFirstName, userLastName, userEmail,
            phoneNumber, userType, userExtId);
          usersResponse = await this.zendeskHelper.getUser(userExtId);
        }
        if (usersResponse && usersResponse.users[0] && orgsResponse && orgsResponse.organizations[0]) {
          if (usersResponse.users[0].organization_id) {
            if (usersResponse.users[0].organization_id === orgsResponse.organizations[0].id) {
              // already associated
            } else {
              // TODO: dont know what should we do?
              logger.error('ALERT: User is associated with a different organization');
            }
          } else {
            await this.zendeskHelper.associateUserWithOrganization(orgExtId, userExtId);
          }
          const ticket = await this.zendeskHelper.createTicket(userExtId, req.body.subject, req.body.comment, 'normal');

          const url: string = config.get<string>('mail.web-portal-redirection-url');
          if (userDTO.userType === 'publisher') {
            if (userDTO.hasPersonalProfile) {
              if (userDTO.isSubscribedForEmail) {
                this.generalMail.sendSupportAutoResponsePublisherMail(
                  req.email,
                  userDTO.firstName,
                  ticket.ticket.id,
                  `${url}/publisher/publisherSupport`,
                  `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                );
              }
            }
          }
          if (userDTO.userType === 'advertiser') {
            if (userDTO.hasPersonalProfile) {
              if (userDTO.isSubscribedForEmail) {
                this.generalMail.sendSupportAutoResponseAdvertiserMail(
                  req.email,
                  userDTO.firstName,
                  ticket.ticket.id,
                  `${url}/advertiser/advertiserSupport`,
                  `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                );
              }
            }
          }
          res.json({ success: true, message: `Ticket added successfully, Your ticket id is ${ticket.ticket.id}` });
        } else {
          throw new HandledApplicationError(500, 'Unable to find user in Zendesk');
        }
      } else {
        res.json({ success: false, message: `Email ${userDTO.email} is not exist !` });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateTicket = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
      const verifyResponse: any = await this.zendeskHelper.verifyTicketId(req.body.ticketId, userDTO._id.toString());
      if (verifyResponse.valid) {
        let priority: string = '';
        if (req.body.priority) {
          priority = req.body.priority.toLowerCase();
          if (!this.validPriorities.includes(priority)) {
            throw new HandledApplicationError(500, `Invalid priority, expected ${this.validPriorities}`);
          }
        }
        let status: string = '';
        if (req.body.status) {
          status = req.body.status.toLowerCase();
          if (!this.validStatuses.includes(status)) {
            throw new HandledApplicationError(500, `Invalid status, expected ${this.validStatuses}`);
          }
        }
        if (!req.body.subject) {
          throw new HandledApplicationError(500, 'Subject cannot be blank');
        }
        const updateTicketResponse: any = await this.zendeskHelper.updateTicket(req.body.ticketId,
          verifyResponse.userId, req.body.subject, priority, status);
        res.json({ success: true, message: 'Ticket updated successfully' });
      } else {
        // TODO: Need to change here if we want organization admin to see all the tickets
        logger.error('Ticket does not belong to the logged in user');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public addComment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
      const verifyResponse: any = await this.zendeskHelper.verifyTicketId(req.body.ticketId, userDTO._id.toString());
      if (verifyResponse.valid) {
        await this.zendeskHelper.addTicketComment(req.body.ticketId, userDTO._id.toString(), req.body.comment);
        res.json({ success: true, message: 'Comment added successfully' });
      } else {
        // TODO: Need to change here if we want organization admin to see all the tickets
        logger.error('Ticket does not belong to the logged in user');
        res.json({ success: false, message: 'Ticket is not valid' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public getTicketsByEmail = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
      const ticketResponse: any = await this.zendeskHelper.getAllTicketsForUser(userDTO._id.toString());
      const dTickets: any[] = [];
      if (ticketResponse) {
        for (const ticket of ticketResponse.tickets) {
          const t = this.enrichTicketPayload(ticket, req.email, userDTO.userType, ticketResponse.userId);
          dTickets.push(t);
        }
      }
      res.json(dTickets);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getTicketComment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
      const verifyResponse: any = await this.zendeskHelper.verifyTicketId(req.params.id, userDTO._id.toString());
      if (verifyResponse.valid) {
        const ticketCommentResponse: any = await this.zendeskHelper.getTicketComments(req.params.id, userDTO._id.toString());
        const t: any = this.enrichTicketPayload(verifyResponse.ticket, req.email, userDTO.userType, verifyResponse.userId);
        const comments: any[] = [];
        for (const comment of ticketCommentResponse.comments) {
          const c: any = { };
          c.comment = comment.body;
          const createdBy = (verifyResponse.userId === comment.author_id) ? userDTO.email : 'support@doceree.com';
          c.created = { at: comment.created_at, by: createdBy };
          comments.push(c);
        }
        t.comments = comments;
        res.json({ userDetails: userDTO, ticketDetails: t });
      } else {
        // TODO: Need to change here if we want organization admin to see all the tickets
        logger.error('Ticket does not belong to the logged in user');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public getTitles = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
      let articlesResponse: any = await this.zendeskHelper.getArticles(1);
      const sectionsResponse: any = await this.zendeskHelper.getSections();

      let publisherSectionId: number = 0;
      let advertiserSectionId: number = 0;
      let generalSectionId: number = 0;

      for (const s of sectionsResponse.sections) {
        if (s.name.toLowerCase() === 'publisher') {
          publisherSectionId = s.id;
        }
        if (s.name.toLowerCase() === 'advertiser') {
          advertiserSectionId = s.id;
        }
        if (s.name.toLowerCase() === 'general') {
          generalSectionId = s.id;
        }
      }

      const generalArticles: any[] = [];
      const advertiserArticles: any[] = [];
      const publisherArticles: any[] = [];
      const totalPages = parseInt(articlesResponse.page_count, 10);

      let currentPage: number = 0;
      while (currentPage < totalPages) {
        currentPage++;

        for (const article of articlesResponse.articles) {
          const payload: any = { };
          payload.title = article.title;
          payload.href = article.html_url;
          if (userDTO.userType === 'publisher' && article.section_id === publisherSectionId) {
            publisherArticles.push(payload);
          }
          if (userDTO.userType === 'advertiser' && article.section_id === advertiserSectionId) {
            advertiserArticles.push(payload);
          }
          if (article.section_id === generalSectionId) {
            generalArticles.push(payload);
          }
        }
        if (currentPage < totalPages) {
          articlesResponse = await this.zendeskHelper.getArticles(currentPage + 1);
        } else {
          break;
        }
      }
      if (userDTO.userType === 'publisher') {
        res.json({ generalArticles, publisherArticles });
      }
      if (userDTO.userType === 'advertiser') {
        res.json({ generalArticles, advertiserArticles });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private enrichTicketPayload(ticket: any, email: string, userType: string, zendeskUserId: string): any {
    const t: any = { };
    t.ticketId = ticket.id;
    t.email = email;
    t.userType = userType;
    t.created = { at: ticket.created_at };
    t.updated = { at: ticket.updated_at };
    t.subject = ticket.subject;
    t.comment = ticket.description;
    t.status = ticket.status ? ticket.status.toLowerCase().charAt(0).toUpperCase()
      + ticket.status.toLowerCase().slice(1) : ticket.status;
    t.priority = ticket.priority ? ticket.priority.toLowerCase().charAt(0).toUpperCase()
      + ticket.priority.toLowerCase().slice(1) : ticket.priority;
    t.requestor = (zendeskUserId === ticket.requestor_id) ? email : 'support@doceree.com';
    t.assignee = (zendeskUserId === ticket.assignee_id) ? email : 'support@doceree.com';
    return t;
  }
}
