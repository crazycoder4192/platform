import express from 'express';
import PublisherBidRangeDAO from '../../daos/admin/publisher-bidrange.dao';
import PublisherBidRangeDTO from '../../dtos/admin/publisher-bidrange.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class PublisherBidRangeController {
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly bidRangeDAO: PublisherBidRangeDAO;

  constructor() {
    this.authorizeAdmin = new AuthorizeAdmin();
    this.bidRangeDAO = new PublisherBidRangeDAO();
  }

  public getAllBidRanges = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const bidRanges: PublisherBidRangeDTO[] = await this.bidRangeDAO.getAll();
      res.json(bidRanges);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      await throwIfInputValidationError(req);
      const dto: PublisherBidRangeDTO = this.toBidRangeDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.bidRangeDAO.create(dto);
      res.json({ success: true, message: 'bid range added successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);
      const dto: PublisherBidRangeDTO = this.toBidRangeDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.bidRangeDAO.update(req.params.id, dto);
      res.json({ success: true, message: 'bid range updated successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const bidRange: PublisherBidRangeDTO = await this.bidRangeDAO.getById(req.params.id);
      res.json(bidRange);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const bidRange: PublisherBidRangeDTO = await this.bidRangeDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: 'bid range deleted successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  private toBidRangeDTO(req: IAuthenticatedRequest): PublisherBidRangeDTO {
    const dto: PublisherBidRangeDTO = new PublisherBidRangeDTO();
    dto.type = req.body.type;
    dto.min = req.body.min;
    dto.max = req.body.max;
    return dto;
  }
}
