import express from 'express';
import CreditVoucherDAO from '../../daos/admin/credit-voucher.dao';
import CreditVoucherDTO from '../../dtos/admin/credit-voucher.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class CreditVoucherController {
  private readonly creditVoucherDAO: CreditVoucherDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;

  constructor() {
    this.creditVoucherDAO = new CreditVoucherDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
  }

  public getAllCreditVouchers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const creditVouchers: CreditVoucherDTO[] = await this.creditVoucherDAO.getAll();
      res.json(creditVouchers);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createCreditVoucher = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateCreditVoucherInput(req);
      await throwIfInputValidationError(req);

      const dto: CreditVoucherDTO = this.toCreditVoucherDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.creditVoucherDAO.create(dto);
      res.json({ success: true, message: `${dto.voucherName} credit voucher added successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateCreditVoucher = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateCreditVoucherInput(req);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);

      const dto: CreditVoucherDTO = this.toCreditVoucherDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.creditVoucherDAO.update(req.params.id, dto);
      res.json({ success: true, message: `${dto.voucherName} credit voucher updated successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getCreditVoucher = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const creditVoucher: CreditVoucherDTO = await this.creditVoucherDAO.getById(req.params.id);
      res.json(creditVoucher);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteCreditVoucher = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const creditVoucher: CreditVoucherDTO = await this.creditVoucherDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: `${creditVoucher.country} ${creditVoucher.voucherName}  credit voucher deleted successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  private validateCreditVoucherInput(req: IAuthenticatedRequest) {
    // TODO: add validation
  }

  private toCreditVoucherDTO(req: IAuthenticatedRequest): CreditVoucherDTO {
    const dto: CreditVoucherDTO = new CreditVoucherDTO();
    dto.country = req.body.country;
    dto.voucherName = req.body.voucherName;
    dto.voucherCode = req.body.voucherCode;
    dto.amount = req.body.amount;
    dto.expiryDate = req.body.expiryDate;
    return dto;
  }
}
