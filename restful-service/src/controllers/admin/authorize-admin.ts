import UserDAO from '../../daos/user.dao';
import UserDTO from '../../dtos/user.dto';
import HandledApplicationError from '../../error/handled-application-error';
import { UserTypes } from '../../util/constants';
import { logger } from '../../util/winston';

export class AuthorizeAdmin {
    private readonly userDAO: UserDAO;

    constructor() {
        this.userDAO = new UserDAO();
    }

    public async authorize(email: string) {
        const userDTO: UserDTO = await this.userDAO.findByEmail(email);
        if (!userDTO || userDTO.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
            logger.error('You do not have enough priviliges to perform this operation');
            throw new HandledApplicationError(500, 'You do not have enough priviliges to perform this operation');
        }
    }
}
