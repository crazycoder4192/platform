import express from 'express';
import CountryTaxDAO from '../../daos/admin/country-tax.dao';
import CountryTaxDTO from '../../dtos/admin/country-tax.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class CountryTaxController {
  private readonly countryTaxDAO: CountryTaxDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;

  constructor() {
    this.countryTaxDAO = new CountryTaxDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
  }

  public getAllCountryTaxes = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const countryTaxes: CountryTaxDTO[] = await this.countryTaxDAO.getAll();
      res.json(countryTaxes);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createCountryTax = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateCountryTaxInput(req);
      await throwIfInputValidationError(req);

      const dto: CountryTaxDTO = this.toCountryTaxDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.countryTaxDAO.create(dto);
      res.json({ success: true, message: `${dto.country} country tax added successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateCountryTax = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateCountryTaxInput(req);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);

      const dto: CountryTaxDTO = this.toCountryTaxDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.countryTaxDAO.update(req.params.id, dto);
      res.json({ success: true, message: `${dto.country} country tax updated successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getCountryTax = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const countryTax: CountryTaxDTO = await this.countryTaxDAO.getById(req.params.id);
      res.json(countryTax);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteCountryTax = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const countryTax: CountryTaxDTO = await this.countryTaxDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: `${countryTax.country} country tax deleted successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  private validateCountryTaxInput(req: IAuthenticatedRequest) {
    // TODO: add validation
  }

  private toCountryTaxDTO(req: IAuthenticatedRequest): CountryTaxDTO {
    const dto: CountryTaxDTO = new CountryTaxDTO();
    dto.address = req.body.address;
    dto.country = req.body.country;
    dto.percentageOfTax = req.body.percentageOfTax;
    dto.taxRegistrationNumber = req.body.taxRegistrationNumber;
    dto.typeOfTax = req.body.typeOfTax;
    return dto;
  }
}
