import express from 'express';
import WeightageByValidatedHcpDAO from '../../daos/admin/weightage-by-validated-hcp.dao';
import WeightageByValidatedHcpDTO from '../../dtos/admin/weightage-by-validated-hcp.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class WeightageByValidatedHcpController {
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly weightageByValidatedHcpDAO: WeightageByValidatedHcpDAO;

  constructor() {
    this.authorizeAdmin = new AuthorizeAdmin();
    this.weightageByValidatedHcpDAO = new WeightageByValidatedHcpDAO();
  }

  public getAllWeightageByValidatedHcps = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const weightageByValidatedHcp: WeightageByValidatedHcpDTO[] = await this.weightageByValidatedHcpDAO.getAll();
      res.json(weightageByValidatedHcp);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createWeightageByValidatedHcp = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      await throwIfInputValidationError(req);
      const dto: WeightageByValidatedHcpDTO = this.toWeightageByValidatedHcpDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.weightageByValidatedHcpDAO.create(dto);
      res.json({ success: true, message: 'weightage by validated hcp added successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateWeightageByValidatedHcp = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);
      const dto: WeightageByValidatedHcpDTO = this.toWeightageByValidatedHcpDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.weightageByValidatedHcpDAO.update(req.params.id, dto);
      res.json({ success: true, message: 'weightage by validated hcp updated successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getWeightageByValidatedHcp = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const weightageByvalidatedHcp: WeightageByValidatedHcpDTO = await this.weightageByValidatedHcpDAO.getById(req.params.id);
      res.json(weightageByvalidatedHcp);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteWeightageByValidatedHcp = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const weightageByvalidatedHcp: WeightageByValidatedHcpDTO = await this.weightageByValidatedHcpDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: 'validated hcp deleted successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  private toWeightageByValidatedHcpDTO(req: IAuthenticatedRequest): WeightageByValidatedHcpDTO {
    const dto: WeightageByValidatedHcpDTO = new WeightageByValidatedHcpDTO();
    dto.weightage = req.body.weightage;
    dto.min = req.body.min;
    dto.max = req.body.max;
    return dto;
  }
}
