import express from 'express';
import BidRangeDAO from '../../daos/admin/bidrange.dao';
import BidRangeDTO from '../../dtos/admin/bidrange.dto';
import catchError from '../../error/catch-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from './authorize-admin';

export class BidRangeController {
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly bidRangeDAO: BidRangeDAO;

  constructor() {
    this.authorizeAdmin = new AuthorizeAdmin();
    this.bidRangeDAO = new BidRangeDAO();
  }

  public getAllBidRanges = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const bidRanges: BidRangeDTO[] = await this.bidRangeDAO.getAll();
      res.json(bidRanges);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateBidRangeInput(req);
      await throwIfInputValidationError(req);

      const dto: BidRangeDTO = this.toBidRangeDTO(req);
      dto.created = { at: new Date(), by: req.email };
      await this.bidRangeDAO.create(dto);
      res.json({ success: true, message: `${dto.country} bid range added successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      this.validateBidRangeInput(req);
      req
        .checkBody('_id', "Mandatory field '_id' missing in request body")
        .exists();
      await throwIfInputValidationError(req);

      const dto: BidRangeDTO = this.toBidRangeDTO(req);
      dto.modified = { at: new Date(), by: req.email };
      await this.bidRangeDAO.update(req.params.id, dto);
      res.json({ success: true, message: `${dto.country} bid range updated successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const bidRange: BidRangeDTO = await this.bidRangeDAO.getById(req.params.id);
      res.json(bidRange);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteBidRange = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      req
        .checkParams('id', "Mandatory field 'id' missing in request param")
        .exists();
      await throwIfInputValidationError(req);
      const updateObj: any = {
        deleted: true,
        modified: { at: new Date(), by: req.email }
      };
      const bidRange: BidRangeDTO = await this.bidRangeDAO.delete(req.params.id, updateObj);
      res.json({ success: true, message: `${bidRange.country} bid range deleted successfully` });
    } catch (err) {
      catchError(err, next);
    }
  }

  private validateBidRangeInput(req: IAuthenticatedRequest) {
    // TODO: add validation
  }

  private toBidRangeDTO(req: IAuthenticatedRequest): BidRangeDTO {
    const dto: BidRangeDTO = new BidRangeDTO();
    dto.country = req.body.country;
    dto.currencyCode = req.body.currencyCode;
    dto.currencySymbol = req.body.currencySymbol;
    dto.cpm = req.body.cpm;
    dto.cpc = req.body.cpc;
    dto.cpv = req.body.cpv;
    return dto;
  }
}
