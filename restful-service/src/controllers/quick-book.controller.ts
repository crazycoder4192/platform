import config from 'config';
import express from 'express';
import QuickbooksTokenDAO from '../daos/quickbooks-token.dao';
import QuickbooksTokenDTO from '../dtos/quickbooks-token.dto';
import catchError from '../error/catch-error';
import HandledApplicationError from '../error/handled-application-error';
import IAuthenticatedRequest from '../guards/authenticated.request';
import { logger } from '../util/winston';
import { QuickbooksHelper } from './util/quickbooks-helper';
const OAuthClient = require('intuit-oauth');
const QuickBooks = require('node-quickbooks-promise');
import IController from '../common/controller-interface';
import authenticationGuard from '../guards/authentication.guard';
import { AuthorizeAdmin } from './admin/authorize-admin';

export class QuickbookController implements IController {

    public path = '/quickbooks';
    public router = express.Router();
    private oauthClient: any = null;
    private readonly quickbooksTokenDAO: QuickbooksTokenDAO;
    private readonly quickbooksHelper: QuickbooksHelper;
    private readonly authorizeAdmin: AuthorizeAdmin;

    constructor() {
        this.initializeRoutes();
        this.quickbooksTokenDAO = new QuickbooksTokenDAO();
        this.quickbooksHelper = new QuickbooksHelper();
        this.authorizeAdmin = new AuthorizeAdmin();
    }

    public login = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            this.getOauthClient(req.header('zone'));
            const authUri = this.oauthClient.authorizeUri({ scope: [OAuthClient.scopes.Accounting, OAuthClient.scopes.OpenId], state: req.header('zone') });
            logger.info(`authorization url: ${authUri}`);
            res.json(authUri);
        } catch (err) {
            catchError(err, next);
        }
    }

    public redirect = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const zone: string = req.query.state;
            this.getOauthClient(zone);
            const s = await this.oauthClient.createToken(req.url);

            const tokens: any[] = await this.quickbooksTokenDAO.find(zone);
            if (tokens && tokens.length > 0) {
                if (tokens.length > 1) {
                    throw new HandledApplicationError(500, 'Did not expect more than one records');
                }
            } else {
                const t: QuickbooksTokenDTO = new QuickbooksTokenDTO();
                t.accessToken = s.token.access_token;
                t.refreshToken = s.token.refresh_token;
                await this.quickbooksTokenDAO.create(t, zone);
            }

            if (tokens && tokens.length === 1) {
                const tokenDTO: QuickbooksTokenDTO = tokens[0];
                tokenDTO.accessToken = s.token.access_token;
                tokenDTO.refreshToken = s.token.refresh_token;
                await this.quickbooksTokenDAO.update(tokenDTO._id, tokenDTO, zone);
            }

            logger.info(`refresh token: ${s.token.refresh_token}`);
            res.json({ });
        } catch (err) {
            catchError(err, next);
        }
    }

    public createBankAccount = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            res.json(await this.quickbooksHelper.createBankAccount(req.body.accountName, req.header('zone')));
        } catch (err) {
            logger.error(JSON.stringify(err));
            catchError(err, next);
        }
    }

    public createAdvertiserItem = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            res.json(await this.quickbooksHelper.createItemForAdvertiser(req.body, req.header('zone')));
        } catch (err) {
            logger.error(JSON.stringify(err));
            catchError(err, next);
        }
    }

    public createPublisherItem = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            res.json(await this.quickbooksHelper.createItemForPublisher(req.body, req.header('zone')));
        } catch (err) {
            catchError(err, next);
        }
    }

    /*public regenerate = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const output = await this.getAccessToken();
            res.json({ output });
        } catch (err) {
            catchError(err, next);
        }
    }

    public mainAdvertiser = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const docereeAccountName = 'TestAccount';
            let accountDetail = await this.findIncomeAccountByName(docereeAccountName);
            if (!accountDetail) {
                accountDetail = await this.createIncomeAccount(docereeAccountName);
            }

            const item: any = { };
            item.name = 'Impressions';
            item.type = 'Service';
            item.description = 'Description for Impression';
            item.accountName = docereeAccountName;
            let itemDetails = await this.findItemByName(item.name);
            if (!itemDetails) {
                await this.createItemForAdvertiser(item);
            }

            item.name = 'Clicks';
            item.type = 'Service';
            item.description = 'Description for Clicks';
            item.accountName = docereeAccountName;
            itemDetails = await this.findItemByName(docereeAccountName);
            if (!itemDetails) {
                await this.createItemForAdvertiser(item);
            }

            const customer: any = { };
            customer.organizationName = 'Test advertiser organization';
            customer.email = 'test@test.com';
            customer.firstName = 'Ad';
            customer.lastName = 'Doe';
            customer.phoneNumber = '123-456-7890';
            customer.addressLine1 = 'ad address line 1';
            customer.city = 'ad city';
            customer.state = 'ad state';
            customer.country = 'ad country';
            const customerDetails = await this.findCustomerByEmail(customer.email);
            if (!customerDetails) {
                await this.createCustomer(customer);
            }

            const invoice: any = { };
            invoice.customerEmail = 'test@test.com';
            invoice.line = [];
            let line: any = { };
            line.description = 'Description for Impressions';
            line.amount = 100;
            line.itemName = 'Impressions';
            line.quantity = 1000;
            invoice.line.push(line);
            line = { };
            line.description = 'Description for Clicks';
            line.amount = 85;
            line.itemName = 'Clicks';
            line.quantity = 3;
            invoice.line.push(line);
            invoice.dueDate = '2019-09-22';
            const invoiceId = await this.createInvoice(invoice);

            const payment: any = { };
            payment.amount = 185;
            payment.customerEmail = 'test@test.com';
            payment.invoiceId = invoiceId;
            await this.createPayment(payment);

        } catch (err) {
            catchError(err, next);
        }
    }

    public mainPublisher = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const docereeAccountName = 'TestAccount';
            let accountDetail = await this.findIncomeAccountByName(docereeAccountName);
            if (!accountDetail) {
                accountDetail = await this.createIncomeAccount(docereeAccountName);
            }

            let item: any = { };
            item.name = 'Assets';
            item.type = 'Service';
            item.description = 'Description for Assets';
            item.accountName = docereeAccountName;
            let itemDetails = await this.findItemByName(item.name);
            if (!itemDetails) {
                await this.createItemForPublisher(item);
            }

            item = { };
            item.name = 'Bonus';
            item.type = 'Service';
            item.description = 'Description for Bouns';
            item.accountName = docereeAccountName;
            itemDetails = await this.findItemByName(item.name);
            if (!itemDetails) {
                await this.createItemForPublisher(item);
            }

            const vendor: any = { };
            vendor.organizationName = 'Test advertiser organization';
            vendor.email = 'test-publisher@test.com';
            vendor.firstName = 'Pub';
            vendor.lastName = 'Doe';
            vendor.phoneNumber = '123-456-7890';
            vendor.addressLine1 = 'pub address line 1';
            vendor.city = 'pub city';
            vendor.state = 'pub state';
            vendor.country = 'pub country';
            const vendorDetails = await this.findVendorByEmail(vendor.email);
            if (!vendorDetails) {
                await this.createVendor(vendor);
            }

            const bill: any = { };
            bill.vendorEmail = 'test-publisher@test.com';
            bill.line = [];
            let line: any = { };
            line.description = 'Description for Assets';
            line.amount = 100;
            line.itemName = 'Assets';
            line.quantity = 1000;
            bill.line.push(line);
            line = { };
            line.description = 'Description for Bonus';
            line.amount = 85;
            line.itemName = 'Bonus';
            line.quantity = 3;
            bill.line.push(line);
            bill.dueDate = '2019-09-22';
            const output = await this.createBill(bill);
            res.json({ output });
        } catch (err) {
            catchError(err, next);
        }
    }*/

    private initializeRoutes() {
        this.router.get(`${this.path}/login`, authenticationGuard, this.login);
        this.router.get(`${this.path}/redirect`, this.redirect);
        this.router.post(`${this.path}/bank-account`, authenticationGuard, this.createBankAccount);
        this.router.post(`${this.path}/advertiser-item`, authenticationGuard, this.createAdvertiserItem);
        this.router.post(`${this.path}/publisher-item`, authenticationGuard, this.createPublisherItem);
    }

    private getOauthClient(zone: string) {
        if (zone === '1') {
            this.oauthClient = new OAuthClient({
                clientId: config.get<string>('quickbooks.clientId'),
                clientSecret: config.get<string>('quickbooks.clientSecret'),
                environment: config.get<string>('quickbooks.environment'),
                redirectUri: config.get<string>('quickbooks.redirectUri')
            });
        } else if (zone === '2') {
            this.oauthClient = new OAuthClient({
                clientId: config.get<string>('quickbooks.india.clientId'),
                clientSecret: config.get<string>('quickbooks.india.clientSecret'),
                environment: config.get<string>('quickbooks.india.environment'),
                redirectUri: config.get<string>('quickbooks.india.redirectUri')
            });
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }

    }

    /*public createAccountReceivableAccount = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const qbo = await this.getQbo();
            const payload: any = { };
            payload.Name = 'MyExpenseAccount';
            payload.AccountType = 'Expense';
            let output = await qbo.createAccount(payload);
            res.json(output);
        } catch (err) {
            catchError(err, next);
        }
    }*/
}
