import config from 'config';
import express from 'express';
import moment = require('moment');
import RoleDAO from '../../daos/admin/role.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { NotificationIcon, NotificationRoute, NotificationType, UserStatuses, UserTypes } from '../../util/constants';
import { HashPwd } from '../../util/hash-pwd';
import { AdvertiserMail } from '../../util/mail/advertiser-mail';
import { NotificationHelper } from '../../util/notification-helper';
import { logger } from '../../util/winston';

export class AdvertiserUserHelper {
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly userDAO: UserDAO;
    private readonly hashPwd: HashPwd;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly advertiserMail: AdvertiserMail;
    private readonly roleDAO: RoleDAO;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly notificationHelper: NotificationHelper;

    constructor() {
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.userDAO = new UserDAO();
        this.hashPwd = new HashPwd();
        this.userTokenDAO = new UserTokenDAO();
        this.advertiserMail = new AdvertiserMail();
        this.roleDAO = new RoleDAO();
        this.brandDAO = new AdvertiserBrandDAO();
        this.notificationHelper = new NotificationHelper();
    }

    public async createUser(
        loggedInUser: UserDTO,
        req: IAuthenticatedRequest,
        res: express.Response) {
        const dto: AdvertiserUserDTO = this.toAdvertiserUserDTO(req);
        dto.created = { at: new Date(), by: req.email };
        // get first user
        const user: UserDTO = await this.userDAO.findByEmail(req.body.email);
        if (user && user.userType !== 'advertiser') {
            return { message: 'User exists but not an advertiser!', success: false };
        } else if (user && user.userType === 'advertiser') {
            // validate email and advertiser user
            const sameAdvertiserUser: AdvertiserUserDTO =
                await this.advertiserUserDAO.findByAdvertiserIdAndEmail(req.body.advertiserId, req.body.email);
            // if the user and this advertiser is not mapped, then
            if (!sameAdvertiserUser) {
                // check if this user is registered with some other advertiser and probably with other brands
                const advertiserUserByEmail: AdvertiserUserDTO = await this.advertiserUserDAO.findByEmail(req.body.email);
                if (advertiserUserByEmail) {
                    // user is associated with a different advertiser
                    return {
                        message: 'User belongs to a different advertiser, cannot be associated to this advertiser',
                        success: false
                    };
                } else {
                    // may be this block is called upon a new user profile creation
                    dto.brands[0].isWatching = true;
                    const result = await this.advertiserUserDAO.create(dto);
                    // add send email here
                    if (req.body.brandId && req.body.roleId) {
                        const roleDTO = await this.roleDAO.getRoleById(req.body.roleId);
                        const brandDTO = await this.brandDAO.findById(req.body.brandId, req.header('zone'));
                        const url: string = config.get<string>('mail.web-portal-redirection-url');
                        const sender: UserDTO = await this.userDAO.findByEmail(req.email);
                        if (req.email !== req.body.email) {
                            await this.advertiserMail.sendGrantedUserAccessExistingUserMail(
                                req.body.email,
                                sender.firstName,
                                brandDTO.brandName,
                                roleDTO.roleName,
                                `${url}/login`
                            );
                        }
                        // send notification - starts
                        try {
                            // get all the admin users in the advertiser for notification
                            const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(req.body.advertiserId);
                            const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                            if (sendNotificationTo.length > 0) {
                                const notificationIcon: NotificationIcon = NotificationIcon.info;
                                const notificationMessage: string = `A quick update on recent account activity: ${req.body.email} has been added to the team for ${brandDTO.brandName} as ${roleDTO.roleName} by ${loggedInUser.firstName}`;
                                await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                                    NotificationType.brand, brandDTO._id.toString(), NotificationRoute.advertiser_manage_account);
                            }
                        } catch (err) {
                            logger.error(`${err.stack}\n${new Error().stack}`);
                        }
                        // send notification - ends
                    }
                    return result;
                }
            } else if (sameAdvertiserUser && (user.userStatus === 'active' || user.userStatus === 'pending')) {
                // this is the flow where within the same advertiser, we are adding a new brand
                if (!sameAdvertiserUser.brands.length) {
                    dto.brands[0].isWatching = true;
                } else {
                    // zone specific modification - set the default platform in case we have two zones
                    const existingAdvertiserUserInZone: any[] = sameAdvertiserUser.brands.filter((x) => x.zone === req.header('zone'));
                    if (existingAdvertiserUserInZone.length === 0) {
                        dto.brands[0].isWatching = true;
                    }
                }
                // TODO: Add a check if the brand already does not exist
                // add send here - may be existing user is invited for additional brand
                let sameRoleBrandExists = false;
                for (const s of sameAdvertiserUser.brands) {
                    if (s.brandId.toString() === dto.brands[0].brandId.toString() &&
                        s.userRoleId.toString() === dto.brands[0].userRoleId.toString()) {
                        sameRoleBrandExists = true;
                        break;
                    }
                }
                if (!sameRoleBrandExists) {
                    sameAdvertiserUser.brands.push(dto.brands[0]);
                }

                sameAdvertiserUser.modified = { at: new Date(), by: req.email };
                // update advertiser user
                await this.advertiserUserDAO.update(sameAdvertiserUser.email, sameAdvertiserUser);
                if (req.body.brandId && req.body.roleId) {
                    const roleDTO = await this.roleDAO.getRoleById(req.body.roleId);
                    const brandDTO = await this.brandDAO.findById(req.body.brandId, req.header('zone'));
                    const url: string = config.get<string>('mail.web-portal-redirection-url');
                    const sender: UserDTO = await this.userDAO.findByEmail(req.email);
                    if (req.email !== req.body.email) {
                        await this.advertiserMail.sendGrantedUserAccessExistingUserMail(
                            req.body.email,
                            sender.firstName,
                            brandDTO.brandName,
                            roleDTO.roleName,
                            `${url}/login`
                        );
                    }

                    // send notification - starts
                    try {
                        // get all the admin users in the advertiser for notification
                        const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(req.body.advertiserId);
                        const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                        if (sendNotificationTo.length > 0) {
                            const notificationIcon: NotificationIcon = NotificationIcon.info;
                            const notificationMessage: string = `A quick update on recent account activity: ${req.body.email} has been added to the team for ${brandDTO.brandName} as ${roleDTO.roleName} by ${loggedInUser.firstName}`;
                            await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                                NotificationType.brand, brandDTO._id.toString(), NotificationRoute.advertiser_manage_account);
                        }
                    } catch (err) {
                        logger.error(`${err.stack}\n${new Error().stack}`);
                    }
                    // send notification - ends
                }
                return { message: 'Brand Added successfully !', success: true };
            } else if (sameAdvertiserUser && user.userStatus === 'deleted') {
                return { message: 'User is deleted !', success: false };
            }
        } else {
            // varify domain
            // const info: IVerifiedInfo = await this.verifyEmailDomain(req.body.email);
            // create user and token
            const userDetail: UserDTO = new UserDTO();
            userDetail.created = { at: new Date(), by: req.email };
            userDetail.email = req.body.email;
            userDetail.userRole = null;
            userDetail.userStatus = 'pending';
            userDetail.userType = 'advertiser';
            userDetail.hasPassword = false;
            userDetail.hasPersonalProfile = false;
            userDetail.hasSecurityQuestions = false;
            // create for registered users.
            // token generation
            let tokenStr: string;
            const createdUser: UserDTO = await this.userDAO.create(userDetail);
            const jwtExpiresInVal = config.get<string>('jwt.addUserExpiresIn');
            const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
            tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);

            const tokenDTO: UserTokenDTO = new UserTokenDTO();
            tokenDTO.email = req.body.email;
            tokenDTO.expiryTime = new Date(expiresIn);
            tokenDTO.token = tokenStr;
            tokenDTO.tokenType = 'addUser';
            tokenDTO.created = { at: new Date(), by: req.email };
            await this.userTokenDAO.create(tokenDTO);
            dto.brands[0].isWatching = true;
            const roleDTO = await this.roleDAO.getRoleById(dto.brands[0].userRoleId);
            const brandDTO = await this.brandDAO.findById(dto.brands[0].brandId, req.header('zone'));
            const result = await this.advertiserUserDAO.create(dto);
            const url: string = config.get<string>('mail.web-portal-redirection-url');
            const sender: UserDTO = await this.userDAO.findByEmail(req.email);
            let sendStatus: boolean = false;
            if (roleDTO.roleName.toLowerCase() === 'admin') {
                sendStatus = await this.advertiserMail.sendGrantedNewUserAdminAccessMail(
                    req.body.email,
                    sender.firstName,
                    brandDTO.brandName,
                    dto.email,
                    `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
                );
            }

            if (roleDTO.roleName.toLowerCase() === 'analyst') {
                sendStatus = await this.advertiserMail.sendGrantedNewUserAnalyticsAccessMail(
                    req.body.email,
                    sender.firstName,
                    brandDTO.brandName,
                    dto.email,
                    `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
                );
            }
            if (roleDTO.roleName.toLowerCase() === 'editor') {
                sendStatus = await this.advertiserMail.sendGrantedNewUserEditorAccessMail(
                    req.body.email,
                    sender.firstName,
                    brandDTO.brandName,
                    dto.email,
                    `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
                );
            }
            // send notification - starts
            try {
                // get all the admin users in the advertiser for notification
                const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(req.body.advertiserId);
                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                if (sendNotificationTo.length > 0) {
                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                    const notificationMessage: string = `A quick update on recent account activity: ${req.body.email} has been added to the team for ${brandDTO.brandName} as ${roleDTO.roleName} by ${loggedInUser.firstName}`;
                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                        NotificationType.brand, brandDTO._id.toString(), NotificationRoute.advertiser_manage_account);
                }
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
            }
            // send notification - ends
            if (sendStatus) {
                return { message: 'Added successfully.', success: true };
            } else {
                return { message: 'Email not sent', success: false };
            }
        }
    }

    private toAdvertiserUserDTO(req: IAuthenticatedRequest): AdvertiserUserDTO {
        const payload: AdvertiserUserDTO = new AdvertiserUserDTO();
        payload.email = req.body.email;
        payload.brands = [{
            zone: req.header('zone'),
            brandId: req.body.brandId,
            userRoleId: req.body.roleId,
            isWatching: false
        }];
        payload.advertiserId = req.body.advertiserId;
        return payload;
    }
}
