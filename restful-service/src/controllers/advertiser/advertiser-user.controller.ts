import config from 'config';
import express from 'express';
import moment = require('moment');
import RoleDAO from '../../daos/admin/role.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import RoleDTO from '../../dtos/admin/role.dto';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { NotificationIcon, NotificationRoute, NotificationType, UserStatuses, UserTypes } from '../../util/constants';
import { HashPwd } from '../../util/hash-pwd';
import { Mail } from '../../util/mail';
import { AdvertiserMail } from '../../util/mail/advertiser-mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { NotificationHelper } from '../../util/notification-helper';
import { logger } from '../../util/winston';
import { AdvertiserUserHelper } from './advertiser-user.helper';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class AdvertiserUserController {
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly userDAO: UserDAO;
    private readonly hashPwd: HashPwd;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly mail: Mail;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly advertiserMail: AdvertiserMail;
    private readonly generalMail: GeneralMail;
    private readonly roleDAO: RoleDAO;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly notificationHelper: NotificationHelper;
    private readonly advertiserUserHelper: AdvertiserUserHelper;

    constructor() {
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.userDAO = new UserDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.hashPwd = new HashPwd();
        this.userTokenDAO = new UserTokenDAO();
        this.mail = new Mail();
        this.advertiserMail = new AdvertiserMail();
        this.generalMail = new GeneralMail();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.roleDAO = new RoleDAO();
        this.brandDAO = new AdvertiserBrandDAO();
        this.notificationHelper = new NotificationHelper();
        this.advertiserUserHelper = new AdvertiserUserHelper();
    }

    public createAdvertiserUser = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction
    ) => {
        try {
            const loggedInUser: UserDTO = await this.userDAO.findByEmail(req.email);
            if (loggedInUser.userType.toLowerCase() !== UserTypes.advertiser.toLowerCase()
                || loggedInUser.userStatus.toLowerCase() !== UserStatuses.active.toLowerCase()) {
                    logger.error(`Logged in user[${req.email}] is not an advertiser`);
                    throw new HandledApplicationError(500, 'Logged in user is not an advertiser');
            }

            req.checkBody('email', 'Email is required').notEmpty();
            req.checkBody('email', 'Email does not appear to be valid').isEmail();

            // check the validation object for errors
            const validationErrors = req.validationErrors();
            if (validationErrors) {
                return res.json({
                    message: 'Request body has no valid data',
                    success: false,
                });
            }
            const createUserResponse = await this.advertiserUserHelper.createUser(loggedInUser, req, res);
            res.json(createUserResponse);
        } catch (err) {
            catchError(err, next);
        }
    }

    // TODO: This function will replace the current function
    /*public getAllAdvertiserUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // only admin will have right to view all the users
            const authorizePayload: any = { advertiserId: req.params.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewUser);

            const advertiserUsers: any[] = await this.advertiserUserDAO.findByAdvertiserId(req.params.advertiserId);
            const details: any[] = [];

            const setUniqueBrandIds: Set<string> = new Set();
            for (const a of advertiserUsers) {
                if (a.brands && a.brands.length > 0) {
                    const tempBrandIds: string[] = a.brands.map((x: any) => x.brandId.toString());
                    for (const t of tempBrandIds) {
                        setUniqueBrandIds.add(t);
                    }
                }
            }
            let uniqueBrandIds: any[] = [...setUniqueBrandIds];
            uniqueBrandIds = uniqueBrandIds.map((x) => mongoose.Types.ObjectId(x));

            const setUniqueRoleIds: Set<string> = new Set();
            for (const a of advertiserUsers) {
                if (a.brands && a.brands.length > 0) {
                    const tempRoleIds: string[] = a.brands.map((x: any) => x.userRoleId);
                    for (const t of tempRoleIds) {
                        setUniqueRoleIds.add(t);
                    }
                }
            }
            let uniqueRoleIds: any[] = [...setUniqueRoleIds];
            uniqueRoleIds = uniqueRoleIds.map((x) => mongoose.Types.ObjectId(x));

            const brands: AdvertiserBrandDTO[] = await this.brandDAO.getBrands(uniqueBrandIds);
            const roles: RoleDTO[] = await this.roleDAO.getRolesByIds(uniqueRoleIds);

            for (const pu of advertiserUsers) {
                const user: UserDTO = await this.userDAO.findByEmail(pu.email);
                for (const p of pu.brands) {
                    const brand = brands.find((x) => (x._id.toString() === p.brandId.toString()));
                    const role = roles.find((x) => (x._id.toString() === p.userRoleId));
                    const d: any = {
                        email: user.email,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        brandName: brand.brandName,
                        roleName: role.roleName
                    };
                    details.push(d);
                }
            }
            res.json(details);
        } catch (err) {
            catchError(err, next);
        }
    }*/

    public getAllAdvertiserUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUsers = await this.advertiserUserDAO.findAll(req.email);
            res.json(advertiserUsers);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAdvertiserUser = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmailWithAdvertiserDetails(req.params.email);
            if (advertiserUser && advertiserUser.length > 0) {
                res.json(advertiserUser[0]);
            } else {
                res.json(null);
            }

        } catch (err) {
            catchError(err, next);
        }
    }
    public getAdvertiserDetails = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.params.email);
            if (advertiserUser) {
                const advertiser = await this.advertiserDAO.findById(advertiserUser.advertiserId);
                res.json(advertiser);
            } else {
                res.json(null);
            }
        } catch (err) {
            catchError(err, next);
        }
    }
    public getAdvertiserUserPermissions = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.params.email);
            let watchingBrand: any;
            if (advertiserUser) {
                if (req.header('zone') === '1' || req.header('zone') === '2') {
                    watchingBrand = advertiserUser.brands.find((obj) => obj.isWatching && obj.zone === req.header('zone'));
                    if (!watchingBrand) {
                        watchingBrand = advertiserUser.brands.find((x) => x.zone === req.header('zone'));
                    }
                } else {
                    watchingBrand = advertiserUser.brands.find((obj) => obj.isWatching);
                }
                const userRole: RoleDTO = await this.roleDAO.getRoleById(
                    watchingBrand.userRoleId);
                return res.json({ userRole: userRole.roleName, userPermissions: userRole.permissions, zone: watchingBrand.zone });
            } else {
                return res.json({ userRole: '', userPermissions: [] });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteAdvertiserUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            req
                .checkParams('email', "Mandatory field 'email' missing in request param")
                .exists();
            await throwIfInputValidationError(req);
            // get user and change.
            const user: UserDTO = await this.userDAO.findByEmail(req.params.email);
            // get advertiser user and change
            const advertiserUser: AdvertiserUserDTO = await this.advertiserUserDAO.findByEmail(req.params.email);
            advertiserUser.deleted = true;
            advertiserUser.modified = { at: new Date(), by: req.email };

            // get all the brands of this advertiser user
            let canDelete: boolean = true;
            const onlyAdminBrands: string[] = [];
            for (const brand of advertiserUser.brands) {
                const role: RoleDTO = await this.roleDAO.getRoleById(brand.userRoleId);
                // check if the advertiser user is admin of this brand
                if (role.roleName.toLowerCase() === 'admin') {
                    // check if he is the only admin user of this brand
                    const users: AdvertiserUserDTO[] = await this.advertiserUserDAO.findAllByBrandId(brand.brandId);
                    let onlyBrandAdmin: boolean = true;
                    for (const u of users) {
                        if (u.email !== req.params.email) {
                            const thisBrand = u.brands.find((x) => (x.brandId.toString()) === (brand.brandId.toString()));
                            const thisRole: RoleDTO = await this.roleDAO.getRoleById(thisBrand.userRoleId);
                            if (thisRole.roleName.toLowerCase() === 'admin') {
                                onlyBrandAdmin = true;
                                onlyAdminBrands.push(brand.brandId);
                                break;
                            }
                        }
                    }
                    if (onlyBrandAdmin) {
                        canDelete = false;
                        // not breaking here so that we can figure out all the brands
                        // where the user is the only admin
                    }
                }
            }

            if (!canDelete) {
                const usBrandNames: any[] = await this.brandDAO.getBrandNamesByBrandIdsForAdmin(onlyAdminBrands, '1');
                const indiaBrandNames: any[] = await this.brandDAO.getBrandNamesByBrandIdsForAdmin(onlyAdminBrands, '2');
                const brandNames: any[] = [...usBrandNames, ...indiaBrandNames];
                const names: string[] = brandNames.map((x) => x.brandName);
                return res.json({ message: `User is the only admin for brand/s ${names}`, success: false });
            }

            // delete token
            await this.userTokenDAO.deleteByEmailAndTokenType(user.email, 'addUser');
            // update advertiser user
            const advertiserUserDTO: AdvertiserUserDTO = await this.advertiserUserDAO.update(advertiserUser.email, advertiserUser);
            // get all the admin users in the advertiser for notification
            try {
                const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(req.body.advertiserId);
                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                if (sendNotificationTo.length > 0) {
                    const loggedInUser = await this.userDAO.findByEmail(req.email);
                    const name: string = user.firstName ? user.firstName : user.email;
                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                    const notificationMessage: string = `A quick update on recent account activity: ${name} has been removed by ${loggedInUser.firstName}`;
                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                        NotificationType.brand, null, NotificationRoute.advertiser_manage_account);
                }
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
            }

            res.json({ success: true, message: `${advertiserUserDTO.email} user deleted from advertiser successfully` });
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteBrandFromAdvertiserUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // get advertiser user and change
            const advertiserUser: AdvertiserUserDTO = await this.advertiserUserDAO.findByEmail(req.body.email);
            advertiserUser.brands.splice(advertiserUser.brands.findIndex((item) => item.brandId === req.body.brandId), 1);
            advertiserUser.modified = { at: new Date(), by: req.email };
            // update advertiser user
            const advertiserUserDTO: AdvertiserUserDTO = await this.advertiserUserDAO.update(advertiserUser.email, advertiserUser);

            try {
                // get all the admin users in the advertiser for notification
                const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(advertiserUserDTO.advertiserId);
                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                if (sendNotificationTo.length > 0) {
                    const removedUser = await this.userDAO.findByEmail(req.body.email);
                    const loggedInUser = await this.userDAO.findByEmail(req.email);
                    const brand = await this.brandDAO.findById(req.body.brandId, req.header('zone'));
                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                    const name: string = removedUser.firstName ? removedUser.firstName : removedUser.email;
                    const notificationMessage: string = `A quick update on recent account activity: ${name} has been removed for ${brand.brandName} by ${loggedInUser.firstName}`;
                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                        NotificationType.brand, req.body.brandId, NotificationRoute.advertiser_manage_account);
                }
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
            }
            res.json({ success: true, message: 'User deleted from the brand successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateBrandRoleOfAdvertiserUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // get advertiser user and change
            const advertiserUser: AdvertiserUserDTO = await this.advertiserUserDAO.findByEmail(req.body.email);
            // get user role
            const userRole: RoleDTO = await this.roleDAO.getRoleByModuleAndRoleName('Advertiser', req.body.userRole);
            advertiserUser.brands.forEach((element) => {
                if (element.brandId && element.brandId.toString() === req.body.brandId) {
                    element.userRoleId = userRole._id;
                }
            });
            advertiserUser.modified = { at: new Date(), by: req.email };
            // update advertiser user
            const advertiserUserDTO: AdvertiserUserDTO = await this.advertiserUserDAO.update(advertiserUser.email, advertiserUser);
            res.json({ success: true, message: 'Brand user role updated successfully' });
            } catch (err) {
            catchError(err, next);
            }
    }

    public reGenerateToken = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        let tokenStr: string;
        try {
            // delete token
            const dlatedToken = await this.userTokenDAO.deleteByEmailAndTokenType(req.body.email, 'addUser');
            // create token and send email
            const jwtExpiresInVal = config.get<string>('jwt.addUserExpiresIn');
            const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
            tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);

            const tokenDTO: UserTokenDTO = new UserTokenDTO();
            tokenDTO.email = req.body.email;
            tokenDTO.expiryTime = new Date(expiresIn);
            tokenDTO.token = tokenStr;
            tokenDTO.tokenType = 'addUser';
            tokenDTO.created = { at: new Date(), by: req.email };
            await this.userTokenDAO.create(tokenDTO);
            const result: UserDTO = await this.userDAO.findByEmail(req.body.email);
            const url: string = config.get<string>('mail.web-portal-redirection-url');
            const sendStatus: boolean = await this.generalMail.sendRegistrationCompletedAdvertiserMail(
            req.body.email,
            `${url}/verifytoken?token=${tokenStr}&tokenType=addUser`
            );
            if (sendStatus) {
                return res.json({ message: 'Activation link sent successfully.', success: true });
            } else {
                return res.json({ message: 'Email not sent', success: false });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public switchToBrandOfLoggedinUser = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // get advertiser user and change
            const advertiserUser: AdvertiserUserDTO = await this.advertiserUserDAO.findByEmail(req.email);
            let isAdmin = false;
            if (advertiserUser.brands.length === 1) {
                advertiserUser.brands[0].isWatching = true;
                const role: RoleDTO = await this.roleDAO.getRoleById(advertiserUser.brands[0].userRoleId);
                if (role.roleName === 'Admin') {
                    isAdmin = true;
                } else {
                    isAdmin = false;
                }
            } else {
                advertiserUser.brands.forEach(async (element) => {
                    if (element.zone === req.header('zone')) {
                        if (element.isWatching === true) {
                            element.isWatching = false;
                        }

                        if (element.brandId.toString() === req.body.brandId) {
                            element.isWatching = true;
                            const role: RoleDTO = await this.roleDAO.getRoleById(element.userRoleId);
                            if (role.roleName === 'Admin') {
                                isAdmin = true;
                            } else {
                                isAdmin = false;
                            }
                        }
                    }
                });
            }
            advertiserUser.modified = { at: new Date(), by: req.email };
            // update advertiser user
            const advertiserUserDTO: AdvertiserUserDTO = await this.advertiserUserDAO.update(advertiserUser.email, advertiserUser);
            res.json({ isAdminBrand: isAdmin, success: true, message: 'Brand switched successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAdvertiserUserBrands = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUserBrandsDetails = await this.advertiserUserDAO.findAdvertiserUserBrandsDetails(req.email);
            return res.json(advertiserUserBrandsDetails[0]);
        } catch (err) {
            catchError(err, next);
        }
    }

    private async verifyEmailDomain(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
          const verifier = require('email-exist');
          verifier.verify(email, (err: any, info: any) => {
            if (err !== null) {
              reject(err);
            } else {
              resolve(info);
            }
          });
        });
    }

    private async sendMail(email: string, jwtToken: string): Promise<boolean> {
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        return await this.generalMail.sendRegistrationCompletedAdvertiserMail(
            email,
            `${url}/verifytoken?token=${jwtToken}&tokenType=addUser`
        );
    }
}

interface IVerifiedInfo {
    success: boolean;
    info: string;
    addr: string;
    response: string;
}
