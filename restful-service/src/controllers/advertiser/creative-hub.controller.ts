import express from 'express';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import CreativeHubDAO from '../../daos/advertiser/creative-hub.dao';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import CreativeHubDTO from '../../dtos/advertiser/creative-hub.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions } from '../../util/constants';
import { logger } from '../../util/winston';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class CreativeHubController {
    private readonly creativeHubDAO: CreativeHubDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;

    constructor() {
        this.creativeHubDAO = new CreativeHubDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.advertiserDAO = new AdvertiserDAO();
    }

    public getAllCreatives = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true
            && thisBrand.zone === req.header('zone'));
            if (watchingBrand) {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);
                const creatives: CreativeHubDTO[] = await this.creativeHubDAO.findByBrandId(watchingBrand.brandId, req.query.creativeType);
                res.json(creatives);
            } else {
                res.json([]);
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    public addCreative = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => (thisBrand.isWatching === true && thisBrand.zone === req.header('zone')));
            if (watchingBrand) {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.addCampaign, AdvertiserPermissions.addCreativeHub);

                let creativeHubDTO: CreativeHubDTO = new CreativeHubDTO();
                creativeHubDTO = await this.toCreativeHubDTO(creativeHubDTO, req);
                const brand: AdvertiserBrandDTO = await this.advertiserBrandDAO.findById(watchingBrand.brandId, req.header('zone'));
                creativeHubDTO.brandId = brand;
                creativeHubDTO.advertiserId = await this.advertiserDAO.findById(brand.advertiser);

                const creativesExisted: CreativeHubDTO[] = await this.creativeHubDAO.findByName(creativeHubDTO.name);
                let existingCreativeHubDTO: CreativeHubDTO;
                for (const c of creativesExisted) {
                    if (c.brandId.toString() === brand._id.toString()) {
                        existingCreativeHubDTO = c;
                    }
                }
                if (existingCreativeHubDTO) {
                    creativeHubDTO.modified = { at: new Date(), by: req.email };

                    const incomingCreativeDetails = creativeHubDTO.creativeDetails;
                    for (const e of existingCreativeHubDTO.creativeDetails) {
                        if (e) {
                            const c: any = incomingCreativeDetails.find((x) => x.url === e.url);
                            if (!c) {
                                incomingCreativeDetails.push(e);
                            }
                        }
                    }
                    await this.creativeHubDAO.update(existingCreativeHubDTO._id, creativeHubDTO);
                } else {
                    creativeHubDTO.created = { at: new Date(), by: req.email };
                    await this.creativeHubDAO.create(creativeHubDTO);
                }
                res.json({ success: true, message: `${creativeHubDTO.name} added successfully` });
            } else {
                res.json({ success: false, message: 'No brand available to add creative against' });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateCreative = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            let creativeHubDTO: CreativeHubDTO = await this.creativeHubDAO.findById(req.params.id);
            if (creativeHubDTO) {
                const authorizePayload: any = { brandId: creativeHubDTO.brandId._id };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.editCampaign, AdvertiserPermissions.editCreativeHub);
                creativeHubDTO = await this.toCreativeHubDTO(creativeHubDTO, req);
                creativeHubDTO.modified = { at: new Date(), by: req.email };
                await this.creativeHubDAO.update(req.params.id, creativeHubDTO);
                res.json({ success: true, message: `${creativeHubDTO.name} updated successfully` });
            } else {
                logger.error(`Invalid creative hub identifier [${req.params.id}]`);
                throw new HandledApplicationError(500, 'Invalid creative hub identifier');
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getCreative = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const creativeHubDTO: CreativeHubDTO = await this.creativeHubDAO.findById(req.params.id);
            if (creativeHubDTO) {
                const authorizePayload: any = { brandId: creativeHubDTO.brandId._id };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);
                res.json(creativeHubDTO);
            } else {
                logger.error(`Invalid creative hub identifier [${req.params.id}]`);
                throw new HandledApplicationError(500, 'Invalid creative hub identifier');
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteCreative = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const creativeHubDTO: CreativeHubDTO = await this.creativeHubDAO.findById(req.params.id);
            if (creativeHubDTO) {
                const authorizePayload: any = { brandId: creativeHubDTO.brandId._id };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.deleteCampaign, AdvertiserPermissions.deleteCreativeHub);
                creativeHubDTO.deleted = true;
                creativeHubDTO.modified = { at: new Date(), by: req.email };
                await this.creativeHubDAO.delete(req.params.id, creativeHubDTO);
                res.json({ success: true, message: 'Deleted successfully' });
            } else {
                logger.error(`Invalid creative hub identifier [${req.params.id}]`);
                throw new HandledApplicationError(500, 'Invalid creative hub identifier');
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteManyCreative = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            req.body.forEach(async (id: any) => {
                const creativeHubDTO = await this.creativeHubDAO.findById(id);
                if (creativeHubDTO) {
                    const authorizePayload: any = { brandId: creativeHubDTO.brandId._id };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                        AdvertiserPermissions.deleteCampaign, AdvertiserPermissions.deleteCreativeHub);
                    creativeHubDTO.deleted = true;
                    creativeHubDTO.modified = { at: new Date(), by: req.email };
                    await this.creativeHubDAO.delete(id, creativeHubDTO);
                } else {
                    logger.error(`Invalid creative hub identifier [${req.params.id}]`);
                    throw new HandledApplicationError(500, 'Invalid creative hub identifier');
                }
            });
            res.json({ success: true, message: 'Deleted successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public addCreativeToSection = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            req.body.creativeHubIds.forEach(async (id: any) => {
                const creativeHubDTO = await this.creativeHubDAO.findById(id);
                if (creativeHubDTO) {
                    const authorizePayload: any = { brandId: creativeHubDTO.brandId._id };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                        AdvertiserPermissions.addCampaign, AdvertiserPermissions.addCreativeHub);

                    creativeHubDTO.sectionName = req.body.sectionName;
                    creativeHubDTO.modified = { at: new Date(), by: req.email };
                    await this.creativeHubDAO.updateSection(id, creativeHubDTO);
                } else {
                    logger.error(`Invalid creative hub identifier [${req.params.id}]`);
                    throw new HandledApplicationError(500, 'Invalid creative hub identifier');
                }
            });
            res.json({ success: true, message: 'Added to selected section successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAllCreativesByBannerSize = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true);
            if (watchingBrand) {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);

                const creatives: CreativeHubDTO[] = await this.creativeHubDAO.findAllByBannerSize(watchingBrand.brandId, req.query.bannerSize);
                let creativesResult: any[];
                creativesResult = [];
                creatives.forEach((element) => {
                    element.creativeDetails.forEach((thisCreative) => {
                        if (thisCreative.size === req.query.bannerSize) {
                            creativesResult.push(thisCreative);
                        }
                    });
                });
                res.json(creativesResult);
            } else {
                res.json([]);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAllVideoCreatives = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true);
            if (watchingBrand) {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);
                const creatives: CreativeHubDTO[] = await this.creativeHubDAO.findAllVideoCreatives(advertiserUser.advertiserId);
                const creativesResult: any[] = [];
                creatives.forEach((element) => {
                    element.creativeDetails.forEach((thisCreative) => {
                        creativesResult.push(thisCreative);
                    });
                });
                res.json(creativesResult);
            } else {
                res.json([]);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public downloadAllFilesInASection = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true);
            if (watchingBrand) {
                const brandId = watchingBrand.brandId;
                const authorizePayload: any = { brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);
                const name = req.params.name;
                const creatives: CreativeHubDTO[] = await this.creativeHubDAO.findByBrandIdAndName(brandId, name);
                const archiver = require('archiver-promise');
                const archive = archiver('zip');
                res.attachment('test.zip');
                archive.pipe(res);
                const archiveMaterials = await this.zipFiles(archive, creatives);
                archiveMaterials.finalize();
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    public getCreativeUrls = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true);
            if (watchingBrand) {
                const brandId = watchingBrand.brandId;
                const authorizePayload: any = { brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);
                const id = req.params.id;
                const creative: CreativeHubDTO = await this.creativeHubDAO.findById(id);
                if (creative && /*creative.brandId.toString() === brandId &&*/ creative.creativeDetails) {
                    res.json(creative.creativeDetails.map((x) => x.url));
                } else {
                    res.json([]);
                }
            } else {
                res.json([]);
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    private async zipFiles(archive: any, materials: CreativeHubDTO[]): Promise<any> {
        const request = require('request');
        for (const m of materials) {
            const creativeDetails: any[] = m.creativeDetails;
            for (const c of creativeDetails) {
                const url = c.url;
                const fileName = url.substr(url.lastIndexOf('/') + 1, url.length).split('?')[0];
                try {
                    const opts = {
                        uri: `${url}`,
                        method: 'GET'
                    };
                    const res = await request(opts);
                    archive.append(res, { name: `${fileName}` });
                } catch (err) {
                    logger.error(`${err.stack}\n${new Error().stack}`);
                }
            }
        }
        return archive;
    }

    private async toCreativeHubDTO(dto: CreativeHubDTO, req: IAuthenticatedRequest): Promise<CreativeHubDTO> {
        dto.name = req.body.name;
        dto.creativeType = req.body.creativeType;
        dto.creativeDetails = req.body.creativeDetails;
        dto.sectionName = req.body.sectionName;
        dto.status = req.body.status;
        return dto;
    }
}
