import config from 'config';
import express from 'express';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import UserDAO from '../../daos/user.dao';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
const paypal = require('paypal-rest-sdk');

export class PaypalGatewayController {
    private readonly advertiserDAO: AdvertiserDAO;
    constructor() {
        this.advertiserDAO = new AdvertiserDAO();
    }

    public payNow = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserDetails = await this.advertiserDAO.findByEmail(req.email);
            const customerDetails = {
                firstName: advertiserDetails.businessName,
                lastName: advertiserDetails.businessName,
                address: advertiserDetails.address.addressLine1,
                city: advertiserDetails.address.city,
                state: advertiserDetails.address.state,
                zipcode: advertiserDetails.address.zipcode,
                country: req.body.billingCountry,
                email: req.email,
                description: 'advertiser profile detials'
            };

            paypal.configure({
                mode: config.get<string>('payPalGatewayDetails.mode'), // sandbox or live
                client_id: config.get<string>('payPalGatewayDetails.client_id'),
                client_secret: config.get<string>('payPalGatewayDetails.client_secret')
            });

            const createPaymentJson = {
                intent: 'sale',
                payer: {
                    payment_method: 'paypal'
                },
                redirect_urls: {
                    return_url: config.get<string>('payPalGatewayDetails.return_url'),
                    cancel_url: config.get<string>('payPalGatewayDetails.cancel_url')
                },
                transactions: [{
                    amount: {
                        currency: 'USD',
                        total: '1.00'
                    },
                    description: 'This is the payment description.'
                }]
            };
            paypal.payment.create(createPaymentJson, (error: any, payment: any) => {
                if (error) {
                    catchError(error, next);
                } else {
                    // tslint:disable-next-line: prefer-for-of
                    for (let i = 0; i < payment.links.length; i++) {
                        if (payment.links[i].rel === 'approval_url') {
                            res.json({ success: true, message: payment.links[i].href });
                        }
                    }
                }
            });
        } catch (err) {
            catchError(err, next);
        }
    }

    public paymentSuccess = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const paymentId = req.params.paymentId;
        const PayerID = req.params.PayerID;
        const executePaymentJson = {
            payer_id: PayerID,
            transactions: [{
                amount: {
                    currency: 'USD',
                    total: '1.00'
                }
            }]
        };

        paypal.payment.execute(paymentId, executePaymentJson, (error: any, payment: any) => {
            if (error) {
                res.json({ success: false, message: error.response });
            } else {
                res.json({ success: true, message: payment });
            }
        });

    }
    public paymentFailure = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        res.json({ success: true, message: req.params.id });
    }
}
