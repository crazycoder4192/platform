import express from 'express';
import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserDashBoardDAO from '../../daos/advertiser/advertiser-dashboard.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import CampaignDAO from '../../daos/advertiser/campaign.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import UserDAO from '../../daos/user.dao';
import SubCampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions } from '../../util/constants';
import { logger } from '../../util/winston';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class DashBoardController {

    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly advertiserDashBoardDAO: AdvertiserDashBoardDAO;
    private readonly campaignDAO: CampaignDAO;
    private readonly subCampaignDAO: SubCampaignDAO;
    private readonly userDAO: UserDAO;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly helper: Helper;

    constructor() {
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.advertiserDashBoardDAO = new AdvertiserDashBoardDAO();
        this.campaignDAO = new CampaignDAO();
        this.subCampaignDAO = new SubCampaignDAO();
        this.userDAO = new UserDAO();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.helper = new Helper();
    }

    public getLifeTimeDataForDashboard = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const dashBoardObj = {
                cardsData : {
                    spends: { total: 0, beforeLogout: 0, change: 0 },
                    impressions: { total: 0, beforeLogout: 0, change: 0 },
                    clicks: { total: 0, beforeLogout: 0, change: 0 },
                    views: { total: 0, beforeLogout: 0, change: 0 },
                    reach: { total: 0, beforeLogout: 0, change: 0 },
                    avgCPC: { total: 0, beforeLogout: 0, change: 0 },
                    avgCPM: { total: 0, beforeLogout: 0, change: 0 },
                    avgCPV: { total: 0, beforeLogout: 0, change: 0 },
                    activeCampaigns: { total: 0, beforeLogout: 0, change: 0 },
                    CTR: { total: 0, beforeLogout: 0, change: 0 },
                    outstandingAmt: { total: 0, dueDate: new Date() }
                },
                clientBrands: new Array()
            };

            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            let authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
            const userData = await this.userDAO.findByEmail(req.email);
            const lastLogoutTime = this.helper.sanitizeLastLogoutTime(userData);
            const allBrandIds: any[] = [];
            const allBrand: any[] = [];
            const zoneBrands: any[] = advertiserUser.brands.filter((thisBrand: any) => thisBrand.zone === req.header('zone'));
            for (const thisBrand of zoneBrands) {
                // TODO: we should be only adding those brands here where the user has the permission to view
                // dashboard data
                try {
                    authorizePayload = { brandId: thisBrand.brandId };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                    allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
                    allBrand.push({ brandId: mongoose.Types.ObjectId(thisBrand.brandId),
                                    watching : thisBrand.isWatching});
                } catch (err) {
                    // don't do anything
                }
            }
            const brandsGroupByClientNames: any = await this.advertiserBrandDAO.getClientBrands(allBrandIds, req.header('zone'));
            for (const brandsGroupByClientName of brandsGroupByClientNames) {
                const clientObj = {
                    clientName: brandsGroupByClientName.clientName,
                    brandData: new Array()
                };
                for (const clientBrand of brandsGroupByClientName.brands) {
                    const brandWatching = allBrand.filter((x: any) => (x.brandId.toString() === clientBrand._id.toString()));
                    const brandObj = {
                            brandId: clientBrand._id, brandName: clientBrand.brandName,
                            isWatching: ((brandWatching && brandWatching.length > 0) ? brandWatching[0].watching : false),
                            spends: { total: 0, beforeLogout: 0, change: 0 },
                            impressions: { total: 0, beforeLogout: 0, change: 0 },
                            clicks: { total: 0, beforeLogout: 0, change: 0 },
                            views: { total: 0, beforeLogout: 0, change: 0 },
                            reach: { total: 0, beforeLogout: 0, change: 0 },
                            avgCPC: { total: 0, beforeLogout: 0, change: 0 },
                            avgCPM: { total: 0, beforeLogout: 0, change: 0 },
                            avgCPV: { total: 0, beforeLogout: 0, change: 0 },
                            activeCampaigns: { total: 0, beforeLogout: 0, change: 0 },
                            CTR: { total: 0, beforeLogout: 0, change: 0 },
                            outstandingAmt: { total: 0, dueDate: new Date() },
                            uniqueHCPs: new Array(),
                            activeSubCampaignsDetails: new Array(),
                            recentSubCampaignsDetails: new Array()
                        };

                    let brandTotalSpends = 0; let brandTotalImpressions = 0; let brandTotalClicks = 0; let brandTotalViews = 0;
                    let brandTotalCPC = 0; let brandTotalCPM = 0; let brandTotalCPV = 0; const brandHCPS = new Array();
                    let brandActiveCPCSubCampaigns = 0; let brandActiveCPMSubCampaigns = 0; let brandActiveCPVSubCampaigns = 0;

                    const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.listAllSubcampaignsByBrandId(clientBrand._id);
                    for (const subCampaign of subCampaignList) {
                        const subCampaignObj = {
                            name: subCampaign.name, campaignId: subCampaign.campaignId, subCampaignId: subCampaign._id,
                            bidType: subCampaign.operationalDetails.bidSpecifications[0], created_at: subCampaign.created.at,
                            spends: 0, impressions: 0, clicks: 0, views: 0, reach: 0, CPC: 0, CPM: 0, CPV: 0, CTR: 0,
                            uniqueHCPIds: new Array()
                        };

                        const subCampaignDetails: any[] = await this.advertiserDashBoardDAO.getAllTransactionalDataOfSubCampaign(
                            subCampaign._id, req.header('zone'));
                        if (subCampaignDetails && subCampaignDetails.length > 0) {

                            const subCampaignData = subCampaignDetails[0];
                            subCampaignObj.spends = subCampaignData.totalAmount ? subCampaignData.totalAmount : 0,
                            subCampaignObj.impressions = subCampaignData.impressions ? subCampaignData.impressions : 0,
                            subCampaignObj.clicks = subCampaignData.clicks ? subCampaignData.clicks : 0,
                            subCampaignObj.views = subCampaignData.views ? subCampaignData.views : 0,
                            subCampaignObj.reach = subCampaignData.reachCount ? subCampaignData.reachCount : 0,
                            subCampaignObj.CPC = subCampaignData.CPC ? subCampaignData.CPC : 0,
                            subCampaignObj.CPM = subCampaignData.CPM ? subCampaignData.CPM : 0,
                            subCampaignObj.CPV = subCampaignData.CPV ? subCampaignData.CPV : 0,
                            subCampaignObj.CTR = subCampaignData.CTR ? subCampaignData.CTR : 0;
                            subCampaignObj.uniqueHCPIds.push(...subCampaignData.uniqueReach);

                            if (subCampaignObj.bidType === 'CPC') {
                                brandActiveCPCSubCampaigns += 1;
                            }
                            if (subCampaignObj.bidType === 'CPM') {
                                brandActiveCPMSubCampaigns += 1;
                            }
                            if (subCampaignObj.bidType === 'CPV') {
                                brandActiveCPVSubCampaigns += 1;
                            }
                        }

                        brandTotalSpends += subCampaignObj.spends;
                        brandTotalClicks += subCampaignObj.clicks;
                        brandTotalImpressions += subCampaignObj.impressions;
                        brandTotalViews += subCampaignObj.views;
                        brandTotalCPC += subCampaignObj.CPC;
                        brandTotalCPM += subCampaignObj.CPM;
                        brandTotalCPV += subCampaignObj.CPV;
                        brandHCPS.push(...subCampaignObj.uniqueHCPIds);
                        if (subCampaign.status.toLowerCase() === 'active') {
                            brandObj.activeSubCampaignsDetails.push(subCampaignObj);
                        }
                    }
                    brandObj.spends.total = brandTotalSpends;
                    brandObj.impressions.total = brandTotalImpressions;
                    brandObj.clicks.total = brandTotalClicks;
                    brandObj.views.total = brandTotalViews;
                    brandObj.activeCampaigns.total = brandObj.activeSubCampaignsDetails.length;
                    /*brandObj.avgCPC.total = brandActiveCPCSubCampaigns > 0 ?
                                brandTotalCPC / brandActiveCPCSubCampaigns : 0;
                    brandObj.avgCPM.total = brandActiveCPMSubCampaigns > 0 ?
                                brandTotalCPM / brandActiveCPMSubCampaigns : 0;
                    brandObj.avgCPV.total = brandActiveCPVSubCampaigns > 0 ?
                                brandTotalCPV / brandActiveCPVSubCampaigns : 0; */
                    brandObj.uniqueHCPs = [...new Set(brandHCPS)];
                    brandObj.reach.total = brandObj.uniqueHCPs.length;
                    brandObj.CTR.total = (brandTotalImpressions > 0) ? (brandTotalClicks / brandTotalImpressions) * 100 : 0;
                    // Recent sub campaigns
                    brandObj.recentSubCampaignsDetails.push.apply(brandObj.recentSubCampaignsDetails, brandObj.activeSubCampaignsDetails);
                    brandObj.recentSubCampaignsDetails.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime());
                    brandObj.activeSubCampaignsDetails.sort((a, b) => b.spends - a.spends);
                    clientObj.brandData.push(brandObj);
                }
                dashBoardObj.clientBrands.push(clientObj);
            }

            // TODO: lets invoke the function to get the card data:
            // get all subcampaigns in brand id
            let allSubCampaignIds: any[] = [];
            for (const brandId of allBrandIds) {
                const subCampaignIds = await this.subCampaignDAO.listAllSubcampaignsByBrandId(brandId);
                if (subCampaignIds) {
                    subCampaignIds.forEach((thisSubcampaign: any) => {
                        allSubCampaignIds.push(mongoose.Types.ObjectId(thisSubcampaign._id));
                    });
                }
            }

            allSubCampaignIds = [...new Set(allSubCampaignIds)];
            const cardsData = await this.advertiserDashBoardDAO.getSubCampaignDataForCards(allSubCampaignIds, lastLogoutTime, req.header('zone'));

            dashBoardObj.cardsData.spends.total = cardsData.totalSpends;
            dashBoardObj.cardsData.spends.change = cardsData.increasedSpends;

            dashBoardObj.cardsData.clicks.total = cardsData.totalClicks;
            dashBoardObj.cardsData.clicks.change = cardsData.increasedClicks;

            dashBoardObj.cardsData.impressions.total = cardsData.totalImpressions;
            dashBoardObj.cardsData.impressions.change = cardsData.increasedImpressions;

            dashBoardObj.cardsData.activeCampaigns.total = cardsData.totalActiveCampaigns;
            dashBoardObj.cardsData.activeCampaigns.change = cardsData.increasedActiveCampaigns;

            dashBoardObj.cardsData.avgCPC.total = cardsData.totalCPC;
            dashBoardObj.cardsData.avgCPC.change = cardsData.increasedCPC;

            dashBoardObj.cardsData.avgCPM.total = cardsData.totalCPM;
            dashBoardObj.cardsData.avgCPM.change = cardsData.increasedCPM;

            dashBoardObj.cardsData.CTR.total = cardsData.totalCTR;
            dashBoardObj.cardsData.CTR.change = cardsData.increasedCTR;

            dashBoardObj.cardsData.reach.total = cardsData.totalReach;
            dashBoardObj.cardsData.reach.change = cardsData.increasedReach;

            res.json(dashBoardObj);
        } catch (err) {
            throw err;
        }
    }

    public getClientWiseBrandsLifeTimeData = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            let authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
            const allBrandIds: any[] = [];
            const zoneBrands: any[] = advertiserUser.brands.filter((thisBrand: any) => thisBrand.zone === req.header('zone'));
            zoneBrands.forEach(async (thisBrand: any) => {
                try {
                    authorizePayload = { brandId: thisBrand.brandId };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                    allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
                } catch (err) {
                    // don't do anything
                }
            });
            const adminClientBrands: any = await this.advertiserBrandDAO.getClientBrands(allBrandIds, req.header('zone'));
            const dashBoardObj: any = [];
            const totalValuesForCards: any = [];
            if (adminClientBrands) {
                let totalClientSpends = 0; let totalClientImpressions = 0; let totalClientClicks = 0;
                let totalClientViews = 0; let totalClientCTR = 0; let totalClientCPC = 0; let totalClientReach = 0;
                let totalClientCPV = 0; let totalBrandSpends = 0; let totalBrandImpressions = 0;
                let totalBrandClicks = 0; const totalBrandViews = 0; let totalBrandCTR = 0; let totalBrandReach = 0;
                let totalBrandCPC = 0; let totalBrandCPV = 0;
                for (const clientBrands of adminClientBrands) {
                    if (clientBrands.brands) {
                        const brandTemp: any = [];
                        for (const brand of clientBrands.brands) {
                            const campaignList = await this.campaignDAO.findByBrandId(brand._id);
                            if (campaignList) {
                                const campaignsTemp: any = []; const subCampaignsTemp: any = [];
                                let totalCampaignSpends = 0; let totalCampaignImpressions = 0;
                                let totalCampaignClicks = 0; let totalCampaignCTR = 0;
                                let totalCampaignCPC = 0; let totalCampaignCPV = 0; let totalCampaignReach = 0;
                                for (const campaign of campaignList) {
                                    const subCampaignList = await this.subCampaignDAO.listOfSubCampaignsByCampaignId(campaign._id);
                                    let totalSpends = 0; let totatlImpressions = 0;
                                    let totalClicks = 0; let totalViews = 0; let totalCTR = 0;
                                    let totalCPC = 0; let totalCPM = 0; let totalCPV = 0; let totalReach = 0;
                                    for (const sub of subCampaignList) {
                                        const transaData = await this.advertiserDashBoardDAO.getSubCampaignTransactionalData(sub._id, req.header('zone'));
                                        if (transaData.length > 0) {
                                            subCampaignsTemp.push({
                                                name: sub.name,
                                                spends: transaData[0].spends ? transaData[0].spends : 0,
                                                impressions: transaData[0].impressions ? transaData[0].impressions : 0,
                                                clicks: transaData[0].clicks ? transaData[0].clicks : 0,
                                                views: transaData[0].views ? transaData[0].views : 0,
                                                CPC: transaData[0].CPC ? transaData[0].CPC : 0,
                                                CPM: transaData[0].CPM ? transaData[0].CPM : 0,
                                                CPV: transaData[0].CPV ? transaData[0].CPV : 0,
                                                CTR: transaData[0].CTR ? transaData[0].CTR : 0,
                                                reach: transaData[0].reach ? transaData[0].reach : 0
                                            });

                                            totalSpends += transaData[0].spends;
                                            totatlImpressions += transaData[0].impressions;
                                            totalClicks += transaData[0].clicks;
                                            totalViews += transaData[0].views;
                                            totalCPC += transaData[0].CPC;
                                            totalCPM += transaData[0].CPM;
                                            totalCPV += transaData[0].CPV;
                                            totalCTR += transaData[0].CTR;
                                            totalReach += transaData[0].reach;
                                        }
                                    }
                                    totalCampaignSpends += totalSpends;
                                    totalCampaignImpressions += totatlImpressions;
                                    totalCampaignClicks += totalClicks;
                                    totalCampaignCTR += totalCTR;
                                    totalCampaignCPC += totalCPC;
                                    totalCampaignCPV += totalCPV;
                                    totalCampaignReach += totalReach;

                                    campaignsTemp.push({
                                        name: campaign.name, spends: totalCampaignSpends, impressions: totalCampaignImpressions,
                                        clicks: totalCampaignClicks, CTR: totalCampaignCTR, CPC: totalCampaignCPC, CPV: totalCampaignCPV,
                                        sub: subCampaignsTemp, totalCampaignReach
                                    });
                                }
                                totalBrandSpends = totalCampaignSpends;
                                totalBrandImpressions = totalCampaignImpressions;
                                totalBrandClicks = totalCampaignClicks;
                                totalBrandCTR = totalCampaignCTR;
                                totalBrandCPC = totalCampaignCPC;
                                totalBrandCPV = totalCampaignCPV;
                                totalBrandReach = totalCampaignReach;
                                if (subCampaignsTemp.length > 0) {
                                    brandTemp.push({ brandId: brand._id,
                                                     brand: brand.brandName, spends: totalBrandSpends, impressions: totalBrandImpressions,
                                                     clicks: totalBrandClicks, CTR: totalBrandCTR, CPC: totalBrandCPC, CPV: totalBrandCPV,
                                                     campaigns: subCampaignsTemp, openState: false,
                                                     reach: totalBrandReach
                                    });
                                }
                            }
                        }
                        if (brandTemp.length) {
                            totalClientSpends += totalBrandSpends;
                            totalClientImpressions += totalBrandImpressions;
                            totalClientClicks += totalBrandClicks;
                            totalClientViews += totalBrandViews;
                            totalClientCTR += totalBrandCTR;
                            totalClientCPC += totalBrandCPC;
                            totalClientCPV += totalBrandCPV;
                            totalClientReach += totalBrandReach;
                            dashBoardObj.push({ clientName: clientBrands.clientName, brandData: brandTemp });
                        }
                    }
                }
            }
            res.json(dashBoardObj);
        } catch (err) {
            throw err;
        }
    }

    public getActiveCampaignLifeTimeData = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const allBrandIds: any[] = [];
            if (req && req.params && req.params.id) {
                if (req.params.id === 'All') {
                    const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
                    if (!advertiserUser) {
                        throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
                    }
                    let authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                    advertiserUser.brands.forEach(async (thisBrand: any) => {
                        try {
                            authorizePayload = { brandId: thisBrand.brandId };
                            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                            allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
                        } catch (err) {
                            // don't do anything
                        }
                    });
                } else {
                    const authorizePayload: any = { brandId: req.params.id };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                    allBrandIds.push(req.params.id);
                }
            }
            const subCampaignList = await this.advertiserDashBoardDAO.listOfActiveSubCampaignsLifeTimeDataByBrandIds(allBrandIds, req.header('zone'));
            res.json(subCampaignList);
        } catch (err) {
            throw err;
        }
    }

    public getBrandDataForCards = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            let allBrandIds: any[] = [];
            if (req && req.params && req.params.id) {
                if (req.params.id === 'All') {
                    allBrandIds = [];
                    let authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                    advertiserUser.brands.forEach(async (thisBrand: any) => {
                        try {
                            authorizePayload = { brandId: thisBrand.brandId };
                            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                            allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
                        } catch (err) {
                            // don't do anything
                        }
                    });
                } else {
                    const authorizePayload: any = { brandId: req.params.id };
                    await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                    allBrandIds.push(req.params.id);
                }
            }
            const userData = await this.userDAO.findByEmail(req.email);
            let logOutTime = new Date();
            if (userData.lastFailureTime) {
                logOutTime = this.helper.sanitizeLastLogoutTime(userData);
            } else if (userData.lastLoginTime) {
                logOutTime = userData.lastLoginTime;
            } else if (userData.created && userData.created.at) {
                logOutTime = userData.created.at;
            }

            let allSubCampaignIds: any[] = [];
            for (const brandId of allBrandIds) {
                const subCampaignIds = await this.subCampaignDAO.listAllSubcampaignsByBrandId(brandId);
                if (subCampaignIds) {
                    subCampaignIds.forEach((thisSubcampaign: any) => {
                        allSubCampaignIds.push(mongoose.Types.ObjectId(thisSubcampaign._id));
                    });
                }
            }

            allSubCampaignIds = [...new Set(allSubCampaignIds)];

            const cardsData = await this.advertiserDashBoardDAO.getSubCampaignDataForCards(allSubCampaignIds, logOutTime, req.header('zone'));
            res.json(cardsData);
        } catch (err) {
            throw err;
        }
    }

    public getPerformanceAnalyticsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { brandId: req.body.brandId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
            const brandIdListArray: string[] = [];
            // Only brand dashboard has Performance Analytics Chart... So one brandId is enough
            brandIdListArray.push(req.body.brandId);
            const brandIdObjectList: any[] = [];
            for (const f of brandIdListArray) {
                brandIdObjectList.push(mongoose.Types.ObjectId(f));
            }
            const brandStartDate = await this.advertiserBrandDAO.getOldestCreatedAt(brandIdObjectList, req.header('zone'));

            const filter = {
                brandStartDate,
                startDate: req.body.startDate,
                endDate: req.body.endDate,
                brandIdList: brandIdObjectList
            };
            const timezone: string = req.body.timezone;
            const performanceResults = await this.advertiserDashBoardDAO.getPerformanceResultLineChart(filter, timezone, req.header('zone'));
            res.json(performanceResults);
        } catch (err) {
            catchError(err, next);
        }
    }
}
