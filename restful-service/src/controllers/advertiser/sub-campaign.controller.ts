import config from 'config';
import express from 'express';
import { AdvertiserAudienceTargetDAO } from '../../daos/advertiser/advertiser-audience-target.dao';
import { AdvertiserAudienceDAO } from '../../daos/advertiser/advertiser-audience.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import AuditPublishedSubCampaignDAO from '../../daos/advertiser/audit-published-sub-campaign.dao';
import CampaignDAO from '../../daos/advertiser/campaign.dao';
import MonthlyInvoiceDAO from '../../daos/advertiser/monthly-invoice.dao';
import AdvertiserPaymentMethodDAO from '../../daos/advertiser/payment-method.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import NotificationDAO from '../../daos/notification.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserAudienceTargetDTO from '../../dtos/advertiser/advertiser-audience-target.dto';
import AdvertiserAudienceDTO from '../../dtos/advertiser/advertiser-audience.dto';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import Advertiser from '../../dtos/advertiser/advertiser.dto';
import AuditPublishedSubCampaignDTO from '../../dtos/advertiser/audit-published-sub-campaign.dto';
import CampaignDTO from '../../dtos/advertiser/campaign.dto';
import MonthlyInvoiceDTO from '../../dtos/advertiser/monthly-invoice.dto';
import SubCampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import NotificationDTO from '../../dtos/notification.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { RedisHelper } from '../../redis/redis-helper';
import { AlexaScore } from '../../util/alexa-score';
import { AdvertiserPermissions, CreditLineStatus, NotificationIcon, NotificationRoute, NotificationType, SubcampaignStatus, UserTypes } from '../../util/constants';
import { AdvertiserMail } from '../../util/mail/advertiser-mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { NotificationHelper } from '../../util/notification-helper';
import { logger } from '../../util/winston';
import { AuthorizeAdvertiser } from './authorize-advertiser';
const dateformat = require('dateformat');

export class SubCampaignController {
    public router = express.Router();
    private readonly subCampaignDAO: SubCampaignDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly campaignDAO: CampaignDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly notificationDAO: NotificationDAO;
    private readonly alexaScore: AlexaScore;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly userDAO: UserDAO;
    private readonly advertiserMail: AdvertiserMail;
    private readonly notificationHelper: NotificationHelper;
    private readonly paymentMethod: AdvertiserPaymentMethodDAO;
    private readonly monthlyInvoiceDAO: MonthlyInvoiceDAO;
    private readonly auditPublishedSubcampaignDAO: AuditPublishedSubCampaignDAO;
    private readonly audienceTargetDAO: AdvertiserAudienceTargetDAO;
    private readonly audienceDAO: AdvertiserAudienceDAO;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly generalMail: GeneralMail;
    private readonly redisHelper: RedisHelper;

    constructor() {
        this.subCampaignDAO = new SubCampaignDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.brandDAO = new AdvertiserBrandDAO();
        this.campaignDAO = new CampaignDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.notificationDAO = new NotificationDAO();
        this.alexaScore = new AlexaScore();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.userDAO = new UserDAO();
        this.advertiserMail = new AdvertiserMail();
        this.notificationHelper = new NotificationHelper();
        this.paymentMethod = new AdvertiserPaymentMethodDAO();
        this.monthlyInvoiceDAO = new MonthlyInvoiceDAO();
        this.auditPublishedSubcampaignDAO = new AuditPublishedSubCampaignDAO();
        this.audienceTargetDAO = new AdvertiserAudienceTargetDAO();
        this.audienceDAO = new AdvertiserAudienceDAO();
        this.userTokenDAO = new UserTokenDAO();
        this.generalMail = new GeneralMail();
        this.redisHelper = new RedisHelper();
    }

    public findPublishedCampaigns = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            // is this accessed only be admin?
            const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
            if (!userDTO || userDTO.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error('You do not have enough priviliges to perform this operation');
                throw new HandledApplicationError(500, 'You do not have enough priviliges to perform this operation');
            }
            const subCampaignPendingList = await this.subCampaignDAO.findPublishedCampaigns();
            res.json(subCampaignPendingList);
        } catch (err) {
            throw err;
        }
    }

    public getAllDrafts = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) =>
            thisBrand.isWatching === true && thisBrand.zone === req.header('zone'));
            if (watchingBrand) {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign);
                const subCampaignList = await this.subCampaignDAO.
                findDrafts(req.email, watchingBrand.brandId);
                res.json(subCampaignList);
            } else {
                res.json([]);
            }
        } catch (err) {
            throw err;
        }
    }

    public providerList = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
            if (!userDTO || userDTO.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error('You do not have enough priviliges to perform this operation');
                throw new HandledApplicationError(500, 'You do not have enough priviliges to perform this operation');
            }
            const subCampaignList = await this.subCampaignDAO.providerList(req.email);
            res.json(subCampaignList);
        } catch (err) {
            throw err;
        }
    }

    public getOneSubCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
        }
        if (req && req.params && req.params.id) {
            try {
                const authorizePayload: any = { subcampaignId: req.params.id };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign);
                const subCampaignList = await this.subCampaignDAO.findById(req.params.id);
                res.json(subCampaignList);
            } catch (err) {
                throw err;
            }
        }
    }

    public findCampaignWithSubCampaigns = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true
            && thisBrand.zone === req.header('zone'));
            if (watchingBrand) {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign);
                const subCampaignList = await this.subCampaignDAO.findCampaignWithSubCampaigns(watchingBrand.brandId, req.header('zone'));
                res.json(subCampaignList);
            } else {
                res.json([]);
            }
        } catch (err) {
            throw err;
        }
    }

    public createSubCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const dto: SubCampaignDTO = await this.toAdvertiserSubCampaign(req, advertiserUser.advertiserId.toString());
            const authorizePayload: any = { campaignId: dto.campaignId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.addCampaign);

            dto.created = { at: new Date(), by: req.email };
            dto.modified = { at: new Date(), by: req.email };
            const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.subCampaignNameAvailability(dto.name,
                dto.campaignId._id);
            if (subCampaignList.length > 0) {
                return res.status(500).json({ type: 'Error', message: 'Sub Campaign Name not available' });
            }
            const temp = await this.subCampaignDAO.create(dto);

            // update campaign if campaign is deleted.
            const campaign = await this.campaignDAO.findById(dto.campaignId);
            if (campaign.deleted) {
                campaign.deleted = false;
                campaign.modified = { at: new Date(), by: req.email };
                await this.campaignDAO.update(campaign._id, campaign);
            }
            // end
            const toUser: UserDTO = await this.userDAO.findByEmail(req.email);

            res.json({ message: 'Campaign added successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateSubCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { subcampaignId: req.body._id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.editCampaign);

            const existingDTO: SubCampaignDTO = await this.subCampaignDAO.findById(req.body._id);
            let operationChangesForApprovedCampaign: boolean = false;
            let creativesChangesForApprovedCampaign: boolean = false;
            if (existingDTO) {
                // tslint:disable-next-line
                if (existingDTO.status === SubcampaignStatus.approved || existingDTO.status === SubcampaignStatus.paused 
                    || existingDTO.status === SubcampaignStatus.recentlyCompleted) {
                    operationChangesForApprovedCampaign = this.checkIfOperationDetailsChanged(existingDTO, req.body);
                    creativesChangesForApprovedCampaign = this.checkIfCreativesChanged(existingDTO, req.body);
                }
            } else {
                throw new HandledApplicationError(500, 'Invalid subcampaign id passed');
            }
            const dto: SubCampaignDTO = await this.toAdvertiserSubCampaign(req, advertiserUser.advertiserId.toString());
            dto._id = req.body._id;
            if (creativesChangesForApprovedCampaign) {
                dto.status = SubcampaignStatus.underReview;
                dto.isApproved = false;
            }

            dto.modified = { at: new Date(), by: req.email };
            const subCampaign: SubCampaignDTO = await this.subCampaignDAO.findById(dto._id);
            const selectedCampaignId: any = subCampaign.campaignId;
            if ((subCampaign.name.toLowerCase() !== dto.name.toLowerCase()) || (selectedCampaignId.toString() !== dto.campaignId._id.toString())) {
                const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.subCampaignNameAvailability(dto.name,
                    dto.campaignId._id);
                if (subCampaignList.length > 0) {
                    return res.status(500).json({ type: 'Error', message: 'Sub Campaign Name not available' });
                }
            }
            const reloadTargetAudience: boolean = this.isAudienceTargetUpdated(existingDTO.audienceTargetIds,
                dto.audienceTargetIds);
            const temp = await this.subCampaignDAO.update(req.body._id, dto);
            await this.redisHelper.updateSubcampaign(req.body._id, reloadTargetAudience, req.header('zone'));

            if (creativesChangesForApprovedCampaign || operationChangesForApprovedCampaign) {
                await this.auditPublishedSubcampaignDAO.create(await this.toAuditDTO(req.email, temp));
            }

            // if the user is creating a campaign
            if ((!existingDTO.isPublished && dto.isPublished)) {
                const toUser: UserDTO = await this.userDAO.findByEmail(advertiserUser.email);
                const advertiserSubCampaignCount = await this.subCampaignDAO.findCountOfTotalCampaignsByAdvertiserId(dto.advertiserId);
                if (toUser.hasPersonalProfile) {
                    if (toUser.isSubscribedForEmail) {
                        if (advertiserSubCampaignCount > 1) {
                            await this.advertiserMail.sendCampaignAddedMail(
                                req.email,
                                toUser.firstName,
                                dto.name,
                                `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                            );
                        } else {
                            await this.advertiserMail.sendFirstRunCampaignMail(
                                req.email,
                                toUser.firstName,
                                dto.name,
                                `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                            );
                        }
                    }
                }
            } else if (creativesChangesForApprovedCampaign) {
                // if the user is editing an already published campaign
                // TODO: This is not the right mail
                /*const toUser: UserDTO = await this.userDAO.findByEmail(advertiserUser.email);
                if (toUser.isSubscribedForEmail) {
                    await this.advertiserMail.sendFirstRunCampaignMail(
                        req.email,
                        toUser.firstName,
                        dto.name,
                        `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                    );
                }*/
                // send notification - starts
                try {
                    // get all the admin users in the advertiser for notification
                    const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(dto.advertiserId._id.toString());
                    const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                    if (sendNotificationTo.length > 0) {
                        const notificationIcon: NotificationIcon = NotificationIcon.info;
                        const notificationMessage: string = `A quick update on recent account activity: ${req.email} just edited the active campaign ${dto.name}
                        for ${dto.brandId.brandName}. The campaign has been paused while the changes are being reviewed. We will update you about its status within the next 24 hours`;
                        await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                            NotificationType.brand, dto.brandId._id.toString(), NotificationRoute.advertiser_manage_account);
                    }
                } catch (err) {
                    logger.error(`${err.stack}\n${new Error().stack}`);
                }
                // send notification - ends
            }
            res.json({ message: 'Campaign updated successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public changeStatusSubCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        // TODO: will this be accessed by both admin & advertiser?
        const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
        let reloadTargetAudience: boolean = false;
        if (userDTO && userDTO.userType.toLowerCase() === UserTypes.admin.toLowerCase()) {
            // all good, let it go ahead
        } else if (userDTO && userDTO.userType.toLowerCase() === UserTypes.advertiser.toLowerCase()) {
            const authorizePayload: any = { subcampaignId: req.body._id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.addCampaign);
        } else {
            logger.error(`You [${req.email}] do not have enough priviliges to perform this operation`);
            throw new HandledApplicationError(500, 'You do not have enough priviliges to perform this operation');
        }

        try {
            const dto: SubCampaignDTO = await this.subCampaignDAO.findById(req.body.id);
            const brandDTO: AdvertiserBrandDTO = await this.brandDAO.findById(req.body.brandId, req.header('zone'));
            const oldStatus: string = dto.status;
            dto.status = req.body.status ? req.body.status : 'Under review';
            let notificationIcon: NotificationIcon = NotificationIcon.info;
            let notificationMessage: string = '';
            // change messsage according to status
            switch (req.body.status) {
                case 'Under review': {
                    await this.checkIfBrandHasPaymentAssociated(req.body.brandId,
                        brandDTO.advertiser.toString(), req.header('zone'));
                    dto.isApproved = false;
                    notificationIcon = NotificationIcon.info;
                    dto.activated = { at: null, by: null };
                    notificationMessage = `${req.body.name} Campaign is under review`;
                    break;
                }
                case 'Error': {
                    if (userDTO && userDTO.userType.toLowerCase() === UserTypes.admin.toLowerCase()) {
                        if (oldStatus && oldStatus !== 'Under review') {
                            throw new HandledApplicationError(500, `Expected subcampaign state to be 'Under review', found ${oldStatus}`);
                        }
                        if (req.body.rejectionReason) {
                            dto.rejectionReason = req.body.rejectionReason;
                            notificationMessage = `${req.body.name} Campaign is rejected. Reason - ${dto.rejectionReason}`;
                        } else {
                            notificationMessage = `We’re sorry to inform you that your campaign ${req.body.name} has been rejected. We request you to refer to our content guidelines, fix the issue, and resubmit your campaign for approval`;
                        }
                        dto.isApproved = false;
                        notificationIcon = NotificationIcon.error;
                        dto.activated = { at: null, by: null };
                    }
                    break;
                }
                case 'On hold': {
                    dto.isApproved = false;
                    notificationIcon = NotificationIcon.info;
                    dto.activated = { at: null, by: null };
                    notificationMessage = `${req.body.name} Campaign is on hold`;
                    break;
                }
                case 'Paused': {
                    dto.isApproved = true;
                    notificationIcon = NotificationIcon.info;
                    notificationMessage = `A quick update on recent account activity: ${userDTO.firstName} just paused the active campaign ${req.body.name} for ${brandDTO.brandName}`;
                    break;
                }
                case 'Recently Completed': {
                    notificationIcon = NotificationIcon.info;
                    notificationMessage = `A quick update on recent account activity: ${userDTO.firstName} stopped the active campaign ${req.body.name} for ${brandDTO.brandName}`;
                    break;
                }
                default: {
                    // this is the case for active
                    if (userDTO) {
                        if (userDTO.userType.toLowerCase() === UserTypes.admin.toLowerCase()) {
                            if (oldStatus && oldStatus.toLowerCase() !== 'Under review'.toLowerCase()) {
                                throw new HandledApplicationError(500, `Expected subcampaign state to be 'Under review', found ${oldStatus}`);
                            }
                            dto.rejectionReason = ''; // reset rejection reason
                            await this.checkIfBrandHasPaymentAssociated(req.body.brandId, brandDTO.advertiser.toString(),
                            req.header('zone'));
                            dto.isApproved = true;
                            notificationIcon = NotificationIcon.success;
                            notificationMessage = `Congratulations! Your campaign ${req.body.name} has just been approved!`;
                            dto.activated = { at: new Date(), by: req.email };
                            this.scoreSubcampaign(dto._id, req.header('zone'));
                            reloadTargetAudience = true;
                        } else {
                            notificationMessage = `${userDTO.firstName} resumed the campaign ${req.body.name} for ${brandDTO.brandName}`;
                        }
                    }
                    break;
                }
            }
            const advertiserUsers: AdvertiserUserDTO[] = await this.advertiserUserDAO.findAllByBrandId(dto.brandId);
            const emails: string[] = advertiserUsers.map((p) => p.email);
            if (advertiserUsers.length > 0) {
                await this.notificationHelper.addNotification(emails, notificationMessage, notificationIcon,
                    NotificationType.brand, req.body.brandId, NotificationRoute.advertiser_campaign);
            }

            // send mails
            advertiserUsers.forEach(async (advertiserUser) => {
                const toUser: UserDTO = await this.userDAO.findByEmail(advertiserUser.email);
                const url: string = config.get<string>('mail.web-portal-redirection-url');
                if (req.body.status === 'Error') {
                    if (toUser.hasPersonalProfile) {
                        if (toUser.isSubscribedForEmail) {
                            await this.advertiserMail.sendCampaignNotApprovedMail(
                                advertiserUser.email,
                                toUser.firstName,
                                dto.name,
                                `${url}/advertiser/drafts`,
                                `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(advertiserUser.email))}`
                            );
                        }
                    }
                }
                if (req.body.status === 'Active') {
                    if (toUser.hasPersonalProfile) {
                        if (toUser.isSubscribedForEmail) {
                            if (oldStatus && oldStatus.toLowerCase() === 'Under review'.toLowerCase()) {
                                await this.advertiserMail.sendCampaignApprovedMail(
                                    advertiserUser.email,
                                    toUser.firstName,
                                    dto.name,
                                    `${url}/advertiser/dashboard`,
                                    `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(advertiserUser.email))}`
                                );
                            }
                        }
                    }
                }
                if (req.body.status === 'Recently Completed') {
                    if (toUser.hasPersonalProfile) {
                        if (toUser.isSubscribedForEmail) {
                            let endDate = dto.operationalDetails.endDate;
                            const currentDate = new Date();
                            if (endDate > currentDate) {
                                endDate = currentDate;
                            }
                            endDate = dateformat(endDate, 'mmmm dd, yyyy');
                            await this.advertiserMail.sendCampaignCompleteMail(
                                advertiserUser.email,
                                toUser.firstName,
                                dto.name,
                                endDate,
                                `${url}/advertiser/analytics`,
                                `${url}/advertiser/campaignCreation`,
                                `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(advertiserUser.email))}`
                            );
                        }
                    }
                }
                if (req.body.status === 'Paused') {
                    if (toUser.hasPersonalProfile) {
                        if (toUser.isSubscribedForEmail) {
                            await this.advertiserMail.sendCampaignStoppedMail(
                                advertiserUser.email,
                                toUser.firstName,
                                `${url}/advertiser/dashboard`,
                                `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(advertiserUser.email))}`
                            );
                        }
                    }
                }
            });

            // update sub campaign
            const temp = await this.subCampaignDAO.update(req.body.id, dto);
            await this.redisHelper.updateSubcampaign(req.body.id, reloadTargetAudience, req.header('zone'));
            res.json({ message: 'Campaign updated successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteSubCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }

            const authorizePayload: any = { subcampaignId: req.params.id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.deleteCampaign);

            const dto: SubCampaignDTO = await this.subCampaignDAO.findById(req.params.id);
            // tslint:disable-next-line
            if (dto.status === SubcampaignStatus.approved || dto.status === SubcampaignStatus.completed ||
                dto.status === SubcampaignStatus.paused || dto.status === SubcampaignStatus.recentlyCompleted) {
                    throw new HandledApplicationError(500, 'Cannot delete once active subcampaign');
                }
            dto.deleted = true;
            dto.modified = { at: new Date(), by: req.email };
            const temp = await this.subCampaignDAO.delete(req.params.id, dto);
            res.json({ message: 'Campaign deleted successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public deactivateSubcampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            // only via admins
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }

            // list all subcampaigns which are past due date
            let result: any[] = await this.subCampaignDAO.findSubcampaignCandidateForRecentlyCompleted();
            // set their status as inactive
            for (const r of result) {
                const dto: SubCampaignDTO = new SubCampaignDTO();
                dto.status = SubcampaignStatus.recentlyCompleted;
                await this.subCampaignDAO.update(r._id, dto);
                await this.redisHelper.updateSubcampaign(r._id.toString(), false, req.header('zone'));
            }

            // list all subcampaigns which have past the 7 days grace period
            result = await this.subCampaignDAO.findSubcampaignCandidateForCompleted();

            // set their status as inactive
            for (const r of result) {
                const dto: SubCampaignDTO = new SubCampaignDTO();
                dto.status = SubcampaignStatus.completed;
                await this.subCampaignDAO.update(r._id, dto);
                await this.redisHelper.updateSubcampaign(r._id.toString(), false, req.header('zone'));
            }

            res.json({ message: 'Subcampaigns marked completed and recently completed', success: true });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAllSubCampaignsByCampaignId = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
        }

        if (req && req.params && req.params.campaignId) {
            try {
                const authorizePayload: any = { campaignId: req.params.campaignId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign);

                const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.
                    listOfSubCampaignsByCampaignId(req.params.campaignId);
                res.json(subCampaignList);
            } catch (err) {
                throw err;
            }
        }
    }

    public getAllActiveSubCampaignsByBrandId = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
        }
        if (req && req.params && req.params.brandId) {
            try {
                const authorizePayload: any = { brandId: req.params.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign);

                const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.listOfActiveSubCampaignsByBrandId
                    (req.params.brandId);
                res.json(subCampaignList);
            } catch (err) {
                throw err;
            }
        }
    }

    public getAllSubCampaignsByBrandId = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
        }

        if (req && req.params && req.params.brandId) {
            try {
                const authorizePayload: any = { brandId: req.params.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewAnalytics);

                const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.listOfSubCampaignsByBrandId
                    (req.params.brandId);
                res.json(subCampaignList);
            } catch (err) {
                throw err;
            }
        }
    }

    public subCampaignNameAvailability = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { campaignId: req.params.campaignId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.viewCampaign);

            const subCampaignList: SubCampaignDTO[] = await this.subCampaignDAO.subCampaignNameAvailability(req.body.name,
                                                                 req.body.campaignId);
            if (subCampaignList.length > 0) {
                return res.status(500).json({ type: 'Error', message: 'Sub Campaign Name not available' });
              } else {
                return res.status(200).json({ type: 'Success', message: 'Sub Campaign Name available' });
              }
        } catch (err) {
            catchError(err, next);
        }
    }

    public noCampaigns = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const advertisers: Advertiser[] = await this.advertiserDAO.findAll();
            const currentDate: Date = new Date();

            let advertiserIdsFromSubcampaignTable: any[] = await this.subCampaignDAO.findAllAdvertiser();
            advertiserIdsFromSubcampaignTable = advertiserIdsFromSubcampaignTable.map((x) => x.toString());
            const mailSentToUsers: string[] = [];
            for (const a of advertisers) {
                const id = a._id.toString();
                if (!advertiserIdsFromSubcampaignTable.includes(id)) {
                    const user = await this.userDAO.findActiveUserByEmail(a.user.email);
                    if (user) {
                        let lastMailSentAt: Date = a.created.at;
                        let noOfTimesReminderSent: number = 0;
                        if (user.reminder && user.reminder.noCampaign) {
                            lastMailSentAt = user.reminder.noCampaign.mailSentAt;
                            if (!lastMailSentAt) {
                                lastMailSentAt = a.created.at;
                            }
                            if (user.reminder.noCampaign.noOfMailsSent) {
                                noOfTimesReminderSent = user.reminder.noCampaign.noOfMailsSent;
                            }
                        }
                        if (noOfTimesReminderSent < 3) {
                            if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
                                this.sendNoCampaignReminderMail(user, currentDate, noOfTimesReminderSent);
                                mailSentToUsers.push(user.email);
                            } else {
                                // dont do anything
                            }
                        } else if (noOfTimesReminderSent === 3) {
                            if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
                                this.sendNoCampaignReminderMail(user, currentDate, noOfTimesReminderSent);
                                mailSentToUsers.push(user.email);
                            } else {
                                // dont do anything
                            }
                        } else {
                            // dont do anything; we dont have to send mail post 4 reminders
                        }
                    }
                }
            }
            return res.status(200).json({ type: 'Success', message: `Mail sent to users: ${mailSentToUsers}` });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getCreativeUrls = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true
            && thisBrand.zone === req.header('zone'));
            if (watchingBrand) {
                const brandId = watchingBrand.brandId;
                const authorizePayload: any = { brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                    AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewCreativeHub);
                const id = req.params.id;
                const subcampaign: SubCampaignDTO = await this.subCampaignDAO.findById(id);
                if (subcampaign && subcampaign.brandId._id.toString() === brandId && subcampaign.creativeSpecifications
                 && subcampaign.creativeSpecifications.creativeDetails) {
                    res.json(subcampaign.creativeSpecifications.creativeDetails.map((x) => x.url));
                } else {
                    res.json([]);
                }
            } else {
                res.json([]);
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    public getPotentialReachBannerSizeBidRange = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { subcampaignId: req.body.id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.addCampaign);

            const targetIds: string[] = req.body.audienceTargetIds;
            let totalAvailableReach = 0;
            if (req.header('zone') === '1') {
                totalAvailableReach = await this.audienceTargetDAO.getAvailableReachForUs();
            } else if (req.header('zone') === '2') {
                totalAvailableReach = await this.audienceTargetDAO.getAvailableReachForIndia();
            }
            const uniqueNpis: Set<number> = new Set();
            for (const t of targetIds) {
                const targetDTO: AdvertiserAudienceTargetDTO = await this.audienceTargetDAO.findById(t);
                const audienceDTOs: AdvertiserAudienceDTO[] = await this.audienceDAO.findByTargetId(targetDTO._id);
                if (audienceDTOs && audienceDTOs.length > 1) {
                    throw new HandledApplicationError(500, 'Did not expect multiple audience corresponding to a single target');
                }
                if (audienceDTOs[0]) {
                    for (const n of audienceDTOs[0].npi) {
                        uniqueNpis.add(n);
                    }
                }
            }

            const totalTargetedReach = uniqueNpis.size;
            let bidAndBannerDetails = { };
            bidAndBannerDetails = await this.subCampaignDAO.getBannerSizesAndBidRange([...uniqueNpis]);
            res.json({ totalHcps: totalAvailableReach, potentialReach: totalTargetedReach, bidAndBannerDetails });
        } catch (err) {
            catchError(err, next);
        }
    }

    private checkIfOperationDetailsChanged(dto: SubCampaignDTO, body: any): boolean {
        if (dto.operationalDetails.bidSpecifications && dto.operationalDetails.bidSpecifications.length > 0) {
            if (body.operationalDetails.bidSpecifications && body.operationalDetails.bidSpecifications.length > 0) {
                if (dto.operationalDetails.bidSpecifications[0] !== body.operationalDetails.bidSpecifications[0]) {
                    throw new HandledApplicationError(500, 'Cannot change the bid type from CPC to CPM & vice versa');
                }
            }
        }
        if (dto.operationalDetails.bidLimits.budget !== body.operationalDetails.bidLimits.budget) {
            return true;
        }
        if (dto.operationalDetails.bidLimits.type !== body.operationalDetails.bidLimits.type) {
            return true;
        }
        if (dto.operationalDetails.currency !== body.operationalDetails.currency) {
            return true;
        }
        if (dto.operationalDetails.totalBudget !== body.operationalDetails.totalBudget) {
            return true;
        }
        if (dto.operationalDetails.startDate !== body.operationalDetails.startDate) {
            return true;
        }
        if (dto.operationalDetails.endDate !== body.operationalDetails.endDate) {
            return true;
        }
        return false;
    }

    private checkIfCreativesChanged(dto: SubCampaignDTO, body: any) {
        if (dto.creativeSpecifications.creativeDetails.length < body.creativeSpecifications.creativeDetails.length) {
            // indicates new files are uploaded
            return true;
        }
        // this check should exclude any deleted file
        for (const c of body.creativeSpecifications.creativeDetails) {
            const creatives: any[] = dto.creativeSpecifications.creativeDetails;
            const foundCreative = creatives.find((x) => x.url === c.url);
            if (!foundCreative) {
                return true;
            }
        }
        return false;
    }

    private async checkIfBrandHasPaymentAssociated(brandId: string, advertiserId: string, zone: string) {
        // payment method basicaly contains credit card details
        let paymentMethodPresent: boolean = false;
        const paymentMethod = await this.paymentMethod.findByBrandId(brandId, zone);
        if (paymentMethod && !paymentMethod.deleted) {
            paymentMethodPresent = true;
        }
        // we need to check for monthly invoice method as well
        const monthlyInvoiceDTOs: MonthlyInvoiceDTO[] = await this.monthlyInvoiceDAO.findByAdvertiserId(advertiserId, zone);
        for (const m of monthlyInvoiceDTOs) {
           if (m.status === CreditLineStatus.approved) {
               paymentMethodPresent = true;
           }
        }
        if (!paymentMethodPresent) {
            logger.error(`No payment method linked with the brand​: ${brandId}`);
            throw new HandledApplicationError(500, 'No payment method linked with the brand​');
        }
    }

    private async scoreSubcampaign(subcampaignId: string, zone: string) {
        const dto: SubCampaignDTO = await this.subCampaignDAO.findById(subcampaignId);
        if (dto && dto.creativeSpecifications) {
            dto.creativeSpecifications.ctaScore = await this.alexaScore.evaluate(dto.creativeSpecifications.ctaLink);
        }
        dto.last3SubcampaignScore = await this.subCampaignDAO.findLast3SubcampaignScores(dto.advertiserId._id, zone);
        dto.avgCtr = await this.subCampaignDAO.getAverageCtr(dto.advertiserId._id, zone);
        await this.subCampaignDAO.update(dto._id, dto);
    }

    private async toAdvertiserSubCampaign(req: IAuthenticatedRequest, advertiserId: string): Promise<SubCampaignDTO> {
        const dto = new SubCampaignDTO();
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const advertiserDetails: Advertiser = await this.advertiserDAO.findById(
            advertiserUser.advertiserId,
        );
        const brandDetails: AdvertiserBrandDTO = await this.brandDAO.findById(
            req.body.brandId, req.header('zone')
        );
        const campDetails: CampaignDTO = await this.campaignDAO.getCampaignById(
            req.body.campaignId,
        );
        dto.name = req.body.name.trim();
        dto.advertiserId = advertiserDetails;
        dto.brandId = brandDetails;
        dto.campaignId = campDetails;
        dto.objective = req.body.objective ? req.body.objective : '';
        dto.creativeType = req.body.creativeType ? req.body.creativeType : '';
        dto.isPublished = req.body.isPublished ? req.body.isPublished : false;
        if (dto.isPublished) {
            await this.checkIfBrandHasPaymentAssociated(dto.brandId._id, advertiserId, req.header('zone'));
        }
        dto.status = req.body.status ? req.body.status : SubcampaignStatus.underReview;
        for (const id of req.body.audienceTargetIds) {
            const target: any[] = await this.audienceTargetDAO.findByBrandIdAndId(dto.brandId._id, id);
            if (!target || target.length !== 1) {
                throw new HandledApplicationError(500, 'Invalid Audience Target Id');
            }
        }
        dto.audienceTargetIds = req.body.audienceTargetIds ? req.body.audienceTargetIds : '';
        dto.displayTargetDetails = req.body.displayTargetDetails ? req.body.displayTargetDetails : '';
        dto.operationalDetails = req.body.operationalDetails ? req.body.operationalDetails : '';
        dto.creativeSpecifications = req.body.creativeSpecifications ? req.body.creativeSpecifications : '';
        dto.zone = req.header('zone');

        return dto;
    }

    private isAudienceTargetUpdated(prevIds: any[], newIds: any[]): boolean {
        if (prevIds.length !== newIds.length) {
            return true;
        }
        prevIds = prevIds.map((x) => x.toString());
        newIds = newIds.map((x) => x.toString());
        for (const p of prevIds) {
            const n = newIds.find((x) => x === p);
            if (!n) {
                return true;
            }
        }
        for (const n of newIds) {
            const p = newIds.find((x) => x === n);
            if (!p) {
                return true;
            }
        }
    }

    private async toAuditDTO(email: string, dto: SubCampaignDTO): Promise<AuditPublishedSubCampaignDTO> {
        const auditDTO = new AuditPublishedSubCampaignDTO();
        auditDTO.subcampaignId = dto._id.toString();
        auditDTO.name = dto.name.trim();
        auditDTO.objective = dto.objective;
        auditDTO.creativeType = dto.creativeType;
        auditDTO.isPublished = dto.isPublished;
        auditDTO.status = dto.status;
        auditDTO.audienceTargetIds = dto.audienceTargetIds;
        auditDTO.displayTargetDetails = dto.displayTargetDetails;
        auditDTO.operationalDetails = dto.operationalDetails;
        auditDTO.creativeSpecifications = dto.creativeSpecifications;
        auditDTO.subcampaignCreated = dto.created;
        auditDTO.subcampaignActivated = dto.activated;
        auditDTO.subcampaignModified = dto.modified;
        auditDTO.created = { at: new Date(), by: email };
        auditDTO.zone = dto.zone;
        return auditDTO;
    }

    private async sendNoCampaignReminderMail(user: UserDTO, currentDate: Date,
                                             noOfTimesReminderSent: number) {
        if (user.hasPersonalProfile) {
            if (user.isSubscribedForEmail) {
                const date = dateformat(user.created.at, 'mmmm dS, yyyy');
                await this.advertiserMail.sendNoCampaignReminderMail(
                    user.email,
                    user.firstName,
                    date,
                    `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
                );
            }
            user.reminder.noCampaign.mailSentAt = currentDate;
            user.reminder.noCampaign.noOfMailsSent = noOfTimesReminderSent + 1;
            this.userDAO.update(user._id, user);
        }
    }
}
