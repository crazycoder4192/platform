import express from 'express';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import CampaignDAO from '../../daos/advertiser/campaign.dao';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import Advertiser from '../../dtos/advertiser/advertiser.dto';
import CampaignDTO from '../../dtos/advertiser/campaign.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions, SubcampaignStatus } from '../../util/constants';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class CampaignController {
    public router = express.Router();
    private readonly campaignDAO: CampaignDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;

    constructor() {
        this.campaignDAO = new CampaignDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.brandDAO = new AdvertiserBrandDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
    }

    public getAllCampaigns = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true
            && thisBrand.zone === req.header('zone'));
            if (!watchingBrand) {
                res.json([]);
            } else {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewCampaign);
                const campaignList: CampaignDTO[] = await this.campaignDAO.findByBrandId(watchingBrand.brandId);
                res.json(campaignList);
            }
          } catch (err) {
            catchError(err, next);
          }
    }

    public createCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const authorizePayload: any = { brandId: req.body._id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);
            const dto: CampaignDTO = await this.toAdvertiserCampaign(req);
            dto.created = { at: new Date(), by: req.email };
            const campaignList: CampaignDTO[] = await this.campaignDAO.campaignNameAvailability(dto.name, dto.brandId._id, dto.advertiserId._id);
            if (campaignList.length > 0) {
                return res.status(500).json({ type: 'Error', message: 'Campaign Name not available' });
            }
            const temp = await this.campaignDAO.create(dto);
            res.json({ message: 'Campaign added successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const authorizePayload: any = { campaignId: req.params.id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.editCampaign);
            const dto: CampaignDTO = new CampaignDTO();
            dto.name = req.body.name ? req.body.name : '';
            dto.modified = { at: new Date(), by: req.email };
            const campaign: CampaignDTO = await this.campaignDAO.findById(req.params.id);
            if (campaign.name.toLowerCase() !== dto.name.toLowerCase()) {
                const campaignList: CampaignDTO[] = await this.campaignDAO.campaignNameAvailability(dto.name, campaign.brandId._id,
                                                             campaign.advertiserId._id);
                if (campaignList.length > 0) {
                    return res.status(500).json({ type: 'Error', message: 'Campaign Name not available' });
                }
            }
            const temp = await this.campaignDAO.update(req.params.id, dto);
            res.json({ message: 'Campaign updated successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteCampaign = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const authorizePayload: any = { campaignId: req.params.id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.deleteCampaign);
            let temp: any;

            // check if this campaign has any already published subcampaign, if yes,
            // dont allow the delete
            const subcampaigns: any[] = await this.campaignDAO.findSubcampaignsByStatusUnderCampaign(req.params.id,
                                [SubcampaignStatus.underReview, SubcampaignStatus.approved, SubcampaignStatus.completed,
                                 SubcampaignStatus.paused, SubcampaignStatus.recentlyCompleted]);
            if (subcampaigns && subcampaigns.length > 0) {
                throw new HandledApplicationError(500, 'Cannot delete the campaign which once contained approved subcampaign');
            }
            if (req.params.type === 'draft') {
                temp = await this.campaignDAO.deleteFromDraft(req.params.id);
            } else if (req.params.type === 'campaign') {
                temp = await this.campaignDAO.deleteFromCampaign(req.params.id);
            }
            res.json({ message: 'Campaign deleted successfully', success: true, id: temp._id });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getCampaignsByCreatedByUserEmail = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true);
            if (!watchingBrand) {
                res.json([]);
            } else {
                const authorizePayload: any = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewCampaign);
                const campaignList: CampaignDTO[] = await this.campaignDAO.findByCreatedByAndBrandId(req.params.email, watchingBrand.brandId);
                res.json(campaignList);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getCampaignsByBrandId = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const authorizePayload: any = { brandId: req.params.brandId };
            await this.authorizeAdvertiser.authorize(
                req.email, authorizePayload, AdvertiserPermissions.viewCampaign, AdvertiserPermissions.viewAnalytics
            );
            const campaignList: CampaignDTO[] = await this.campaignDAO.findByBrandId(req.params.brandId);
            res.json(campaignList);
        } catch (err) {
            catchError(err, next);
        }

    }

    public campaignNameAvailability = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            const authorizePayload: any = { campaignId: req.params._id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);
            const campaignList: CampaignDTO[] = await this.campaignDAO.campaignNameAvailability(req.body.name, req.body.brandId, req.body.advId);
            if (campaignList.length > 0) {
                return res.status(500).json({ type: 'Error', message: 'Campaign name not available' });
              } else {
                return res.status(200).json({ type: 'Success', message: 'Campaign name available' });
              }
        } catch (err) {
            catchError(err, next);
        }
    }

    private async toAdvertiserCampaign(req: IAuthenticatedRequest): Promise<CampaignDTO> {
        const dto = new CampaignDTO();
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
        }
        const advertiserDetails: Advertiser = await this.advertiserDAO.findById(
            advertiserUser.advertiserId,
        );
        const brandDetails: AdvertiserBrandDTO = await this.brandDAO.findById(
          req.body.brandId, req.header('zone')
        );
        dto.name = req.body.name.trim();
        dto.advertiserId = advertiserDetails;
        dto.brandId = brandDetails;
        return dto;
    }
}
