import config from 'config';
import express from 'express';
import moment from 'moment';
import mongoose from 'mongoose';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import InvoiceDAO from '../../daos/advertiser/invoice.dao';
import InvoiceDTO from '../../dtos/advertiser/invoice.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { advertiserBrandModel } from '../../models/advertiser/advertiser-brand-schema';
import { advertiserModel } from '../../models/advertiser/advertiser-schema';
import { advertiserUserModel } from '../../models/advertiser/advertiser-user-schema';
import { Mail } from '../../util/mail';
const fs = require('fs');
const PDFDocument = require('pdfkit');
import path from 'path';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserPaymentTransactionDAO from '../../daos/advertiser/advertiser-payment-transaction.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import MonthlyInvoiceDAO from '../../daos/advertiser/monthly-invoice.dao';
import AdvertiserPaymentMethodDAO from '../../daos/advertiser/payment-method.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import AdvertiserPaymentTransactionDTO from '../../dtos/advertiser/advertiser-payment-transactions.dto';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import MonthlyInvoiceDTO from '../../dtos/advertiser/monthly-invoice.dto';
import AdvertiserPaymentMethodDTO from '../../dtos/advertiser/payment-method.dto';
import UserDTO from '../../dtos/user.dto';
import HandledApplicationError from '../../error/handled-application-error';
import { RedisHelper } from '../../redis/redis-helper';
import { AdvertiserPermissions, InvoiceStatus, NotificationIcon, NotificationRoute, NotificationType, PaymentStatus, UserTypes } from '../../util/constants';
import { AdvertiserMail } from '../../util/mail/advertiser-mail';
import { logger } from '../../util/winston';
import { AuthorizeNetHelper } from '../util/authorize-net-helper';
import { QuickbooksHelper } from '../util/quickbooks-helper';
import { AuthorizeAdvertiser } from './authorize-advertiser';
const ApiContracts = require('authorizenet').APIContracts;
const ApiControllers = require('authorizenet').APIControllers;
const SDKConstants = require('authorizenet').Constants;
import UserTokenDAO from '../../daos/user-token.dao';
import { GeneralMail } from '../../util/mail/general-mail';
import { NotificationHelper } from '../../util/notification-helper';
const dateformat = require('dateformat');

export class InvoiceGenerationController {
    public res: any = null;
    private readonly invoiceDAO: InvoiceDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly userDAO: UserDAO;
    private readonly paymentMethodDAO: AdvertiserPaymentMethodDAO;
    private readonly mail: Mail;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly quickbooksHelper: QuickbooksHelper;
    private readonly authorizeNetHelper: AuthorizeNetHelper;
    private readonly advertiserPaymentTransactionDAO: AdvertiserPaymentTransactionDAO;
    private readonly monthlyInvoiceDAO: MonthlyInvoiceDAO;
    private readonly advertiserMail: AdvertiserMail;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly generalMail: GeneralMail;
    private readonly redisHelper: RedisHelper;
    private readonly notificationHelper: NotificationHelper;

    constructor() {
        this.userDAO = new UserDAO();
        this.invoiceDAO = new InvoiceDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.paymentMethodDAO = new AdvertiserPaymentMethodDAO();
        this.mail = new Mail();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.quickbooksHelper = new QuickbooksHelper();
        this.authorizeNetHelper = new AuthorizeNetHelper();
        this.advertiserPaymentTransactionDAO = new AdvertiserPaymentTransactionDAO();
        this.advertiserMail = new AdvertiserMail();
        this.monthlyInvoiceDAO = new MonthlyInvoiceDAO();
        this.userTokenDAO = new UserTokenDAO();
        this.generalMail = new GeneralMail();
        this.redisHelper = new RedisHelper();
        this.notificationHelper = new NotificationHelper();
    }

    // This function will be invoked by admin and the invoice will be generated for all the brands
    // This function will be invoked via the cron job. It will generate the invoice if the
    // amount has exceeded $500/brand of $1000/account OR we have reached 28 day of the month
    public generateInvoices = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }

            const generateInvoiceForBrands: Map<string, any> = new Map();
            const brandInvoiceDetails: any[] = [];
            const advertiserInvoiceDetails: Map<string, any[]> = new Map();
            const brandById: Map<string, any> = new Map();
            const creditLimitByAdvertiserId: Map<number, any> = new Map();

            const brandIds = await this.invoiceDAO.getLastXDaysBrandFromAdServerTransaction(35, req.header('zone'));
            for (const brandId of brandIds) {
                const currentDate = new Date();
                const invoices = await this.invoiceDAO.getAllInvoicesByBrandId(brandId, req.header('zone'));
                let startDate;
                let endDate;
                const brandDetails: AdvertiserBrandDTO = await advertiserBrandModel.findById(brandId).exec();
                if (invoices && invoices.length > 0) {
                    const latestInvoice = invoices[0];
                    startDate = latestInvoice.invoiceEndDate;
                } else {
                    if (!startDate && brandDetails) {
                        startDate = brandDetails.created.at;
                    }
                }
                // let the end date be of the UTC format 28 July 2019 00:00:00+Timezone
                endDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());

                if (currentDate.getDate() === config.get<number>('invoiceDefaultExecutionDate')) {
                    brandInvoiceDetails.push({ brandId, startDate, endDate });
                    brandById.set(brandId, brandDetails);
                } else {
                    // TODO: amount check for brand is greater than 500 or not
                    // this.invoiceDAO.generateInvoice(brand._id, startDate, endDate);
                    const amountForBrand: any = await this.invoiceDAO.getAmountForBrands(brandId, startDate, endDate, req.header('zone'));
                    if (amountForBrand && amountForBrand._id && amountForBrand._id.advertiserId) {
                        if (advertiserInvoiceDetails.get(amountForBrand._id.advertiserId)) {
                            const x: any[] = advertiserInvoiceDetails.get(amountForBrand._id.advertiserId);
                            x.push({ brandId, startDate, endDate, amount: x });
                            advertiserInvoiceDetails.set(amountForBrand._id.advertiserId, x);
                        } else {
                            const x: any[] = [];
                            x.push({ brandId, startDate, endDate, amount: amountForBrand.totalAmount });
                            advertiserInvoiceDetails.set(amountForBrand._id.advertiserId, x);
                        }

                        const approvedCreditLineAmount: number = await this.getAdvertiserCreditLineLimit(brandDetails.advertiser,
                            new Date(), req.header('zone'));
                        let brandSpendLimit = brandDetails.spendLimit;
                        if (approvedCreditLineAmount > 0) {
                            brandSpendLimit = approvedCreditLineAmount;
                        }
                        if (amountForBrand.totalAmount >= brandSpendLimit) {
                            brandInvoiceDetails.push({ brandId, startDate, endDate });
                            brandById.set(brandId, brandDetails);
                        }
                    }
                }
            }

            for (const key of advertiserInvoiceDetails.keys()) {
                const adto: AdvertiserDTO = await this.advertiserDAO.findById(key);
                const approvedCreditLineAmount: number = await this.getAdvertiserCreditLineLimit(adto._id, new Date(), req.header('zone'));
                let advertiserSpendLimit = adto.spendLimit;
                if (approvedCreditLineAmount > 0) {
                    advertiserSpendLimit = approvedCreditLineAmount;
                }
                const values: any[] = advertiserInvoiceDetails.get(key);
                const amount: number[] = values.map((x) => x.amount);
                let totalAmount = 0;
                for (const a of amount) {
                    totalAmount = totalAmount + a;
                }
                if (totalAmount >= advertiserSpendLimit) {
                    for (const v of values) {
                        generateInvoiceForBrands.set(v.brandId, { brandId: v._id, startDate: v.startDate, endDate: v.endDate });
                    }
                }
            }
            // this may override few of the key values which are set in the above code block
            for (const d of brandInvoiceDetails) {
                generateInvoiceForBrands.set(d.brandId, d);
            }

            for (const key of generateInvoiceForBrands.keys()) {
                const value: any = generateInvoiceForBrands.get(key);
                if (value) {
                    const invoiceBrandDetails: AdvertiserBrandDTO = await advertiserBrandModel.findById(value.brandId).exec();
                    const paymentMethod = await this.paymentMethodDAO.findByBrandId(invoiceBrandDetails._id, req.header('zone'));

                    const invoiceDTO: InvoiceDTO = await this.invoiceDAO.generateInvoice(invoiceBrandDetails,
                        paymentMethod, value.startDate, value.endDate, req.header('zone'));
                    // invoke quickbooks invoice creation and get invoice id
                    const brandDetails = brandById.get(value.brandId);
                    const advertiserDTO: any = await this.advertiserDAO.findById(brandDetails.advertiser.toString());
                    try {
                        await this.createQuickbooksInvoice(invoiceDTO, advertiserDTO, req.header('zone'));
                    } catch (err) {
                        console.trace(err);// tslint:disable-line
                        logger.error(`error while creating invoice ${JSON.stringify(err)}`);
                        invoiceDTO.quickbooks = { invoiceGenerated: false, paymentGenerated: false, invoiceNumber: '' };
                        this.invoiceDAO.update(invoiceDTO._id, invoiceDTO, req.header('zone'));
                    }

                    // we are not dependent on the quickbooks invoice to be successfully
                    // created
                    if (paymentMethod && invoiceDTO && invoiceDTO.invoiceNumber !== '') {
                        await this.chargePayment(invoiceDTO, advertiserDTO, req.header('zone'));
                    }
                    // send notification - starts
                    try {
                        // get all the admin users in the advertiser for notification
                        const sendNotificationTo: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(req.body.advertiserId);
                        if (sendNotificationTo.length > 0) {
                            const notificationIcon: NotificationIcon = NotificationIcon.info;
                            const notificationMessage: string = `A new invoice for ${invoiceBrandDetails.brandName} is now available`;
                            await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                                NotificationType.brand, invoiceBrandDetails._id.toString(), NotificationRoute.advertiser_payment);
                        }
                    } catch (err) {
                        logger.error(`${err.stack}\n${new Error().stack}`);
                    }
                    // send notification - ends
                }
            }
            res.json({ status: true, message: 'invoice generated' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getOutStandingBrandAmount = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const totalAmount: number = await this.calculateOutstandingAmount(req.email, req.params.brandId, req.header('zone'));
            res.json({ status: true, outstandingAmount: totalAmount });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getOutstandingAmount = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const totalAmount: number = await this.calculateOutstandingAmount(req.email, null, req.header('zone'));
            res.json({ status: true, outstandingAmount: totalAmount });
        } catch (err) {
            catchError(err, next);
        }
    }

    // to get advertiserwise list invoices
    public getInvoices = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewPaymentInvoice);

            let allBrandIds: any[];
            allBrandIds = [];
            if (advertiserUser) {
                advertiserUser.brands.forEach((thisBrand: any) => {
                    allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
                });
            }
            const listOfInvoices = await this.invoiceDAO.findAllByViewingBrands(allBrandIds, req.header('zone'));
            res.json(listOfInvoices);
        } catch (err) {
            catchError(err, next);
        }
    }

    // to download the invoice module
    public downloadInvoiceById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewPaymentInvoice);

            const invoiceData = await this.invoiceDAO.findById(req.params.id, req.header('zone'));
            if (advertiserUser.advertiserId.toString() !== invoiceData.advertiserId.toString()) {
                throw new HandledApplicationError(500, 'Invoice ID does not belong to the logged in advertiser');
            }
            if (invoiceData) {
                const tmp = require('tmp-promise');
                const o = await tmp.dir({ unsafeCleanup: true });
                const timestamp = new Date().valueOf();
                const filepath = `INV-${timestamp}.pdf`;
                const fullpath = path.join(o.path, filepath);

                await this.createInvoicePdf(invoiceData, fullpath, req.header('zone'));
                fs.exists(fullpath, (existed: any) => {
                    if (existed) {
                        res.writeHead(200, {
                            'Content-Type': 'application/octet-stream'
                        });
                        fs.createReadStream(fullpath).pipe(res);
                        o.cleanup();
                    } else {
                        res.writeHead(400, { 'Content-Type': 'text/plain' });
                        res.end('ERROR File does not exist');
                    }
                });
            } else {
                logger.error(`Invalid invoice download request for invoice id [${req.params.id}]`);
                res.writeHead(400, { 'Content-Type': 'text/plain' });
                res.end('ERROR Invalid invoice download request');
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    // to send an email invoice pdf
    public emailInvoiceById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewPaymentInvoice);

            const invoice = await this.invoiceDAO.findById(req.params.id, req.header('zone'));
            if (advertiserUser.advertiserId.toString() !== invoice.advertiserId.toString()) {
                throw new HandledApplicationError(500, 'Invoice ID does not belong to the logged in advertiser');
            }
            if (invoice) {
                await this.sendEmail(invoice, req.email, req.header('zone'));
                res.json({ status: 'success', message: 'Email has been sent successfully' });
            } else {
                logger.error(`Invalid invoice email request for invoice id [${req.params.id}]`);
                res.json({ status: 'error', message: 'Invalid invoice email request' });
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    public findPendingInvoices = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }
            const invoiceDTOs: InvoiceDTO[] = await this.invoiceDAO.findAllPendingInvoices(req.header('zone'));
            return res.json(invoiceDTOs);
        } catch (err) {
            catchError(err, next);
        }
    }

    public processAllPendingInvoices = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }
            const ids: string[] = req.body.invoiceIds;
            const invoiceDTOs: InvoiceDTO[] = await this.invoiceDAO.findPendingInvoices(ids, req.header('zone'));
            let success: number = 0;
            for (const invoiceDTO of invoiceDTOs) {
                try {
                    const advertiserDTO: any = await this.advertiserDAO.findById(invoiceDTO.advertiserId);
                    await this.chargePayment(invoiceDTO, advertiserDTO, req.header('zone'));
                    success++;
                } catch (err) {
                    logger.error(`${err.stack}\n${new Error().stack}`);
                }
            }
            return res.json(`Invoice successfully generated for ${success} of ${invoiceDTOs.length}`);
        } catch (err) {
            catchError(err, next);
        }
    }

    public findInvoicesWithoutQuickbooksInvoice = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }
            const invoiceDTOs: InvoiceDTO[] = await this.invoiceDAO.findAllMissingQuickbookInvoices(req.header('zone'));
            return res.json(invoiceDTOs);
        } catch (err) {
            catchError(err, next);
        }
    }

    public addMissingQuickbooksInvoices = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }
            const ids: string[] = req.body.invoiceIds;
            const invoiceDTOs: InvoiceDTO[] = await this.invoiceDAO.findMissingQuickbookInvoices(ids, req.header('zone'));
            let success: number = 0;
            for (const invoiceDTO of invoiceDTOs) {
                try {
                    const advertiserDTO: any = await this.advertiserDAO.findById(invoiceDTO.advertiserId);
                    await this.createQuickbooksInvoice(invoiceDTO, advertiserDTO, req.header('zone'));
                    success++;
                } catch (err) {
                    logger.error(`${err.stack}\n${new Error().stack}`);
                }
            }
            return res.json(`Quickbook invoice successfully generated for ${success} of ${invoiceDTOs.length}`);
        } catch (err) {
            catchError(err, next);
        }
    }

    public findInvoicesWithoutQuickbooksPayment = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }
            // in this case, invoice number won't match
            const invoiceDTOs: InvoiceDTO[] = await this.invoiceDAO.findAllMissingQuickbookPayments(req.header('zone'));
            res.json(invoiceDTOs);
        } catch (err) {
            catchError(err, next);
        }
    }

    public addMissingQuickbooksPayments = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() !== UserTypes.admin.toLowerCase()) {
                logger.error(`Unwanted access attempted by [${req.email}]`);
                throw new HandledApplicationError(500, 'User do not have priviliges to perform this operation');
            }
            const ids: string[] = req.body.invoiceIds;
            let success: number = 0;
            const invoiceDTOs: InvoiceDTO[] = await this.invoiceDAO.findMissingQuickbookPayments(ids, req.header('zone'));
            for (const invoiceDTO of invoiceDTOs) {
                try {
                    const advertiserDTO: any = await this.advertiserDAO.findById(invoiceDTO.advertiserId);
                    await this.createQuickbooksPayment(invoiceDTO, advertiserDTO, req.header('zone'));
                    success++;
                } catch (err) {
                    logger.error(`${err.stack}\n${new Error().stack}`);
                }
            }
            return res.json(`Quickbook invoice successfully generated for ${success} of ${invoiceDTOs.length}`);
        } catch (err) {
            catchError(err, next);
        }
    }

    private async sendEmail(invoice: any, email: string, zone: any) {
        const tmp = require('tmp-promise');
        const o = await tmp.dir({ unsafeCleanup: true });
        const timestamp = new Date().valueOf();
        const filepath = `INV-${timestamp}.pdf`;
        const fullpath = path.join(o.path, filepath);
        await this.createInvoicePdf(invoice, fullpath, zone);

        const attachment = fullpath;
        const attachments = [{ filename: filepath, path: attachment }];
        const duration = `${moment(invoice.invoiceStartDate).format('MM/DD/YYYY')}-${moment(invoice.invoiceEndDate).format('MM/DD/YYYY')}`;
        const invoiceMonth = dateformat(invoice.invoiceEndDate, 'mmmm');
        const invoiceYear = dateformat(invoice.invoiceEndDate, 'yyyy');
        const invoiceDateAndMonth = dateformat(invoice.invoiceDate, 'mmmm dS');
        const invoiceDate = dateformat(invoice.invoiceDate, 'mmmm dS, yyyy');
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        const toUser: UserDTO = await this.userDAO.findByEmail(email);
        if (toUser.hasPersonalProfile) {
            await this.advertiserMail.sendInvoiceMail(email, toUser.firstName,
                invoice.invoiceNumber, invoiceMonth, invoiceYear, invoiceDateAndMonth,
                invoiceDate, `${url}/advertiser/payments`, attachments);
        }
    }

    private async calculateOutstandingAmount(email: string, inputBrandId: string, zone: string): Promise<number> {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${email}]`);
        }

        const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
        await this.authorizeAdvertiser.authorize(email, authorizePayload, AdvertiserPermissions.viewPaymentInvoice);

        const advertiserInvoiceDetails: Map<string, any[]> = new Map();

        let brandIds = [];

        if (inputBrandId) {
            brandIds.push(mongoose.Types.ObjectId(inputBrandId));
        } else {
            brandIds = advertiserUser.brands.map((x) => x.brandId);
        }

        for (const brandId of brandIds) {
            const invoices = await this.invoiceDAO.getAllInvoicesByBrandId(brandId, zone);
            let startDate;
            let endDate;
            const brandDetails: AdvertiserBrandDTO = await advertiserBrandModel.findById(brandId).exec();
            if (invoices && invoices.length > 0) {
                const latestInvoice = invoices[0];
                startDate = latestInvoice.invoiceDate;
            } else {
                if (!startDate && brandDetails) {
                    startDate = brandDetails.created.at;
                }
            }

            endDate = new Date();

            const amountForBrand: any = await this.invoiceDAO.getAmountForBrands(brandId, startDate, endDate, zone);
            if (amountForBrand && amountForBrand._id && amountForBrand._id.advertiserId) {
                if (advertiserInvoiceDetails.get(amountForBrand._id.advertiserId)) {
                    const x: any[] = advertiserInvoiceDetails.get(amountForBrand._id.advertiserId);
                    x.push({ brandId, startDate, endDate, amount: x });
                    advertiserInvoiceDetails.set(amountForBrand._id.advertiserId, x);
                } else {
                    const x: any[] = [];
                    x.push({ brandId, startDate, endDate, amount: amountForBrand.totalAmount });
                    advertiserInvoiceDetails.set(amountForBrand._id.advertiserId, x);
                }
            }
        }

        let totalAmount = 0;
        for (const key of advertiserInvoiceDetails.keys()) {
            const values: any[] = advertiserInvoiceDetails.get(key);
            const amount: number[] = values.map((x) => x.amount);
            for (const a of amount) {
                totalAmount = totalAmount + a;
            }
        }
        return totalAmount;
    }

    private async createInvoicePdf(invoice: any, fullpath: string, zone: any): Promise<void> {
        const doc = new PDFDocument({ margin: 50 });
        this.generateHeader(doc, zone);
        await this.generateCustomerInformation(doc, invoice);
        this.generateInvoiceTable(doc, invoice, zone);
        // this.contactInformation(doc, invoice);
        // this.generateFooter(doc);
        doc.end();
        doc.pipe(fs.createWriteStream(fullpath, { flags: 'w' }));
    }

    private generateHeader(doc: any, zone: any) {
        doc
            .image('./resources/logo2.png', 450, 10, { width: 80, align: 'right' })
            .fontSize(9)
            .font('Helvetica-Bold').text('DOCEREE INC.', 450, 35, { lineBreak: true, })
            .fontSize(8)
            .font('Helvetica');
        if (zone === '1') {
            doc
                .text('14, Walsh Drive, Suite#302', 450, 45, { lineBreak: true, })
                .text('Parsippany', 450, 55, { lineBreak: true, })
                .text('New Jersy 07054', 450, 65, { lineBreak: true, })
                .text('United States', 450, 75, { lineBreak: true, });
        } else if (zone === '2') {
            doc
                .text('38, Okhla Phase 3 Rd', 450, 45, { lineBreak: true, })
                .text('Okhla Phase III', 450, 55, { lineBreak: true, })
                .text('Okhla Industrial Area', 450, 65, { lineBreak: true, })
                .text('New Delhi, Delhi 110020', 450, 75, { lineBreak: true, });
        }
        doc
            .text('www.doceree.com', 450, 85, { lineBreak: true, })

            .fillColor('#b260e4')
            .fontSize(23)
            .font('Helvetica-Bold').text('{', 25, 100, { align: 'left' })
            .fontSize(14)
            .font('Helvetica-Bold').text('INVOICE', 37, 103, { align: 'left' })
            .fontSize(23)
            .font('Helvetica-Bold').text('}', 97, 100, { align: 'left' })
            .moveDown();
    }

    private contactInformation(doc: any, invoice: any) {
        doc.font('Times-Roman')
            .fontSize(9)
            // tslint:disable-next-line: max-line-length
            .text(`For any queries, please email to ${config.get<string>('contactDetails.emailId')} or call ${config.get<string>('contactDetails.number')}. Any disputes on the invoice should be raised within 15 days of the date of invoice.`,
                50, 630, { lineBreak: true, });
    }

    private generateFooter(doc: any) {
        doc
        .fontSize(8)
        .font('Helvetica').text('If you find issue in this invoice, Please contact', 200, 755, { lineBreak: false })
        .fillColor('#b260e4')
        .font('Helvetica').text('accounts@doceree.com', 365, 755, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text('or call toll-free no.', 455, 755, { lineBreak: false })
        .font('Helvetica-Bold').text('1-888-261-4561', 520, 755, { lineBreak: false })

        .image('./resources/advertiser_invoice_footer.jpg', 186, 771, { width: 424, height: 22 })
        .fillColor('#fff')
        .fontSize(10)
        .font('Helvetica-Bold').text('Thank you for your business!', 200, 777, { lineBreak: false });
    }

    private async generateCustomerInformation(doc: any, invoice: any) {
        const invoiceInfo: any = await advertiserModel.find({ _id: invoice.advertiserId }).exec();
        const brand: any = await advertiserBrandModel.findById(invoice.brandId).exec();
        const advertiserUser: any = await this.userDAO.findByEmail(invoiceInfo[0].user.email);
        const billto = {
            businessName: invoiceInfo[0].businessName,
            address: `${invoiceInfo[0].address.addressLine1}, ${invoiceInfo[0].address.addressLine2}`,
            city: invoiceInfo[0].address.city,
            state: invoiceInfo[0].address.state,
            country: invoiceInfo[0].address.country
        };
        const iDate = new Date(new Date(invoice.invoiceDate).getFullYear(),
            new Date(invoice.invoiceDate).getMonth(), new Date(invoice.invoiceDate).getDate());
        const invoiceDate = `${iDate.getMonth() + 1}/${iDate.getDate()}/${iDate.getFullYear()}`;
        const billingCycle = `${moment(invoice.invoiceStartDate).format('MM/DD/YYYY')} - ${moment(invoice.invoiceEndDate).format('MM/DD/YYYY')}`;

        doc
            .image('./resources/advertiser_invoice_lhs.jpg', 0, 125, { width: 187, height: 665 })
            .lineWidth(425)
            .lineCap('butt')
            .moveTo(400, 125)
            .lineTo(400, 740)
            .stroke('#f2f2f2')
            .fillColor('#000')
            .fontSize(8)

            .font('Helvetica-Bold').text('Invoice No.: ', 200, 140, { lineBreak: true, })
            .font('Helvetica').text(`${invoice.invoiceNumber}`, 250, 140, { lineBreak: true, })

            .font('Helvetica-Bold').text('Advertiser Name:', 390, 140, { lineBreak: true, })
            .font('Helvetica').text(`${advertiserUser.firstName} ${advertiserUser.lastName}`, 460, 140, { lineBreak: true, })

            .font('Helvetica-Bold').text('Invoice Date: ', 200, 155, { lineBreak: true })
            .font('Helvetica').text(`${invoiceDate}`, 253, 155, { lineBreak: true })

            .font('Helvetica-Bold').text('Advertiser Address:', 390, 170, { lineBreak: true })
            // tslint:disable-next-line:max-line-length
            .font('Helvetica').text(`${billto.businessName}, ${billto.address}, ${billto.city}, ${billto.state}, ${billto.country}`, 470, 170, { lineBreak: true })

            .font('Helvetica-Bold').text('Invoice Period: ', 200, 170, { lineBreak: true })
            .font('Helvetica').text(`${billingCycle}`, 260, 170, { lineBreak: true })

            .font('Helvetica-Bold').text('Payment terms:', 200, 185, { lineBreak: true })
            .font('Helvetica').text(`${billingCycle}`, 263, 185, { lineBreak: true })

            .font('Helvetica-Bold').text('Brand Name:', 200, 200, { lineBreak: true })
            .font('Helvetica').text(`${brand.brandName}`, 255, 200, { width: 152, lineBreak: true })

            .font('Helvetica-Bold').text('Attention:', 200, 250, { lineBreak: true })
            .font('Helvetica').text(`Invoice for ${advertiserUser.firstName} ${advertiserUser.lastName} for ${brand.brandName} for ${moment(invoice.billStartDate).format('MM/DD/YYYY')} to ${moment(invoice.billEndDate).format('MM/DD/YYYY')}`, 245, 250, { lineBreak: true });
    }

    private generateTableRow(doc: any, y: any, c1: any, c2: any, c3: any, c4: any, length: any) {
        if (length !== 0 && length % 21 === 0) {
            this.generateFooter(doc);
            doc.addPage();
            doc
            .image('./resources/advertiser_invoice_lhs.jpg', 0, 0, { width: 187, height: 793 })
            .lineWidth(425)
            .lineCap('butt')
            .moveTo(400, 0)
            .lineTo(400, 740)
            .stroke('#f2f2f2');
        }
        if (c4 === 'Amount') {
            doc.font('Helvetica-Bold')
                .fillColor('#b260e4')
                .fontSize(9)
                .text(c1, 200, y - 30, { width: 30, align: 'justify' })
                .text(c2, 230, y - 30, { width: 150, align: 'justify' })
                .text(c3, 450, y - 30, { width: 110, align: 'justify' });
            doc.font('Helvetica-Bold').text(`${c4}`, 550, y - 30, { width: 90, align: 'justify' })
                .lineWidth(1)
                .moveTo(187, 280)
                .lineTo(650, 280)
                .stroke('#b260e4');
        } else {
            doc.font('Helvetica')
                .fillColor('#000')
                .fontSize(8)
                .text(c1, 200, y - 30, { width: 30, align: 'justify' })
                .text(c2, 230, y - 30, { width: 200, align: 'justify' })
                .text(c3, 450, y - 30, { width: 110, align: 'justify' });
            doc.font('Helvetica').text(`${c4}`, 550, y - 30, { width: 90, align: 'justify' });
        }
    }

    private generateInvoiceTable(doc: any, invoice: any, zone: any) {
        let width = 1;
        let invoiceTableTop = 270;
        let afterTableRowPosition = invoiceTableTop;
        const newGe = [];
        for (const invoic of invoice.items) {
            const item = invoic.item;
            let campaignName = invoic.campaignName;
            if (campaignName.length > 40) {
                campaignName = `${campaignName.substring(0, 37)}...`;
            }
            const duration = `${moment(invoic.startDate).format('MM/DD/YYYY')}-${moment(invoic.endDate).format('MM/DD/YYYY')}`;
            let amount: any;
            if (zone === '1') {
                amount = `$${parseFloat(invoic.amount).toFixed(2)}`;
            } else if (zone === '2') {
                amount = `Rs. ${parseFloat(invoic.amount).toFixed(2)}`;
            }
            newGe.push({ item, campaignName, duration, amount });
        }
        const newArray = [{ item: 'S.No', campaignName: 'Name', duration: 'Duration', amount: 'Amount' }, ...newGe];
        for (let i = 0; i < newArray.length; i++) {
            width++;
            const item = newArray[i];
            if (i !== 0 && i % 21 === 0) {
                invoiceTableTop = 0;
                width = 1;
            }
            const position = invoiceTableTop + (width + 1) * 20;
            afterTableRowPosition = position;
            this.generateTableRow(doc, position, item.item, item.campaignName, item.duration, item.amount, i);
        }
        this.createPdfBelowTable(doc, afterTableRowPosition, invoice, zone);
        this.generateFooter(doc);
    }
    private createPdfBelowTable(doc: any, afterTableRowPosition: any, invoice: any, zone: any) {
        const iDate = new Date(new Date(invoice.invoiceDate).getFullYear(),
            new Date(invoice.invoiceDate).getMonth(), new Date(invoice.invoiceDate).getDate());
        const invoiceDate = `${iDate.getMonth() + 1}/${iDate.getDate()}/${iDate.getFullYear()}`;
        let currencySymbol: string;
        if (zone === '1') {
            currencySymbol = '$';
        } else {
            currencySymbol = 'Rs.';
        }
        doc.font('Helvetica')

        .lineWidth(1)
        .moveTo(187, afterTableRowPosition)
        .lineTo(650, afterTableRowPosition)
        .stroke('#b260e4')

        .fillColor('#b260e4')
        .font('Helvetica-Bold').text('Total Amount:', 336, afterTableRowPosition + 10, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} ${parseFloat(invoice.amount).toFixed(2)}`, 400, afterTableRowPosition + 10, { lineBreak: false })

        .fillColor('#b260e4')
        .font('Helvetica-Bold').text('(+) Taxes:', 352, afterTableRowPosition + 20, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} 0`, 400, afterTableRowPosition + 20, { lineBreak: false })

        .fillColor('#b260e4')
        .font('Helvetica-Bold').text('Net Amount:', 342, afterTableRowPosition + 30, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} ${parseFloat(invoice.amount).toFixed(2)}`, 400, afterTableRowPosition + 30, { lineBreak: false })

        .lineWidth(425)
        .lineCap('butt')
        .moveTo(400, afterTableRowPosition + 45)
        .lineTo(400, 740)
        .stroke('#fff')

        .font('Helvetica-Bold').text('Payment Status:', 200, afterTableRowPosition + 60, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${invoice.invoiceStatus}`, 282, afterTableRowPosition + 60, { lineBreak: false })
        .fillColor('#b260e4')
        .font('Helvetica-Bold').text('Funding Source:', 200, afterTableRowPosition + 75, { lineBreak: false })

        .fillColor('#000')
        .font('Helvetica').text(`${invoice.accountNumber} on ${invoiceDate}`, 282, afterTableRowPosition + 75, { lineBreak: false });
    }
    private async chargePayment(invoice: any, advertiser: any, zone: string): Promise<boolean> {
        const paymentMethod: AdvertiserPaymentMethodDTO = await this.paymentMethodDAO.findById(invoice.paymentMethodId, zone);
        if (paymentMethod && paymentMethod.customerId && paymentMethod.customerProfileId) {
            const createRequest: any = this.authorizeNetHelper.chargeCustomerProfile(paymentMethod.customerId,
                paymentMethod.customerProfileId, invoice, advertiser);
            const ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());
            const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
            if (!isSandbox) {
                ctrl.setEnvironment(SDKConstants.endpoint.production);
            }
            return await ctrl.execute(async () => {
                const apiResponse = ctrl.getResponse();
                const response = new ApiContracts.CreateTransactionResponse(apiResponse);
                // save the response to the transaction table
                const transaction: AdvertiserPaymentTransactionDTO = new AdvertiserPaymentTransactionDTO();
                transaction.paymentMethodId = invoice.paymentMethodId;
                transaction.amount = `${invoice.amount}`;
                transaction.invoiceNumber = invoice.invoiceNumber;
                transaction.requestPayload = JSON.stringify(createRequest.getJSON());
                transaction.responsePayload = JSON.stringify(response);
                if (response != null) {
                    if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                        if (response.getTransactionResponse().getMessages() != null) {
                            const invoiceUpdateObject: InvoiceDTO = new InvoiceDTO();
                            invoiceUpdateObject.invoiceStatus = InvoiceStatus.paid;
                            invoiceUpdateObject.accountNumber = response.getTransactionResponse().getAccountNumber();
                            invoiceUpdateObject.accountType = response.getTransactionResponse().getAccountType();
                            invoice = await this.invoiceDAO.update(invoice._id, invoiceUpdateObject, zone);
                            await this.redisHelper.updateInvoice(invoice, zone);

                            transaction.accountNumber = response.getTransactionResponse().getAccountNumber();
                            transaction.accountType = response.getTransactionResponse().getAccountType();
                            transaction.responseCode = response.getTransactionResponse().getResponseCode();
                            transaction.transactionId = response.getTransactionResponse().getRefTransID();
                            transaction.status = PaymentStatus.success;

                            try {
                                if (invoice.quickbooks && invoice.quickbooks.invoiceGenerated) {
                                    await this.createQuickbooksPayment(invoice, advertiser, zone);
                                }
                            } catch (err) {
                                logger.error(`${err.stack}\n${new Error().stack}`);
                                invoice.quickbooks = { invoiceGenerated: true, paymentGenerated: false,
                                                       invoiceNumber: invoice.quickbooks.invoiceNumber };
                                invoice = await this.invoiceDAO.update(invoice._id, invoice, zone);
                            }
                            const sendMail = await this.sendEmail(invoice, advertiser.user.email, zone);
                        } else {
                            logger.error('Failed Transaction.');
                            if (response.getTransactionResponse().getErrors() != null) {
                                logger.error(`Error Code: ${response.getTransactionResponse().getErrors().getError()[0].getErrorCode()}`);
                                logger.error(`Error message: ${response.getTransactionResponse().getErrors().getError()[0].getErrorText()}`);
                            }
                            transaction.status = PaymentStatus.fail;

                        }
                    } else {
                        logger.error('Failed Transaction. ');
                        if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
                            logger.error(`Error Code: ${response.getTransactionResponse().getErrors().getError()[0].getErrorCode()}`);
                            logger.error(`Error message: ${response.getTransactionResponse().getErrors().getError()[0].getErrorText()}`);
                        } else {
                            logger.error(`Error Code: ${response.getMessages().getMessage()[0].getCode()}`);
                            logger.error(`Error message: ${response.getMessages().getMessage()[0].getText()}`);
                        }
                        transaction.status = PaymentStatus.fail;
                    }
                } else {
                    logger.error('Null Response.');
                    transaction.status = PaymentStatus.fail;
                }
                await this.advertiserPaymentTransactionDAO.create(transaction);
                if (transaction.status !== PaymentStatus.success) {
                    // TODO: send failure mail
                    const url: string = config.get<string>('mail.web-portal-redirection-url');
                    const toUser: UserDTO = await this.userDAO.findByEmail(advertiser.user.email);
                    await this.authorizeNetHelper.getCustomerPaymentProfile(paymentMethod.customerId,
                        paymentMethod.customerProfileId, (results: any) => {
                            if (results != null) {
                                let cardNumber: string = results.getPaymentProfile().getPayment().getCreditCard().getCardNumber();
                                cardNumber = cardNumber.replace(/\d(?=\d{4})/g, '*');
                                if (toUser.hasPersonalProfile) {
                                    this.advertiserMail.sendProblemWithPaymentMail(
                                        advertiser.user.email,
                                        toUser.firstName,
                                        cardNumber,
                                        `${url}/login`,
                                    );
                                }
                            }
                        });
                }
            });
        } else {
            logger.error(`No payment method associated with brand ${invoice.brandId}`);
        }
    }

    private async createQuickbooksInvoice(invoiceDTO: InvoiceDTO, advertiserDTO: AdvertiserDTO, zone: string) {
        const invoice: any = { };
        invoice.customerEmail = advertiserDTO.user.email;
        invoice.line = [];
        for (const item of invoiceDTO.items) {
            const line: any = { };

            line.itemName = 'Campaigns';
            line.quantity = 1;

            let campaignName = item.campaignName;
            if (campaignName.length > 40) {
                campaignName = `${campaignName.substring(0, 37)}...`;
            }
            const duration = `${moment(item.startDate).format('MM/DD/YYYY')}-${moment(item.endDate).format('MM/DD/YYYY')}`;
            const amount = `${parseFloat(item.amount).toFixed(2)}`;

            line.description = `Invoice for campaign ${campaignName} for duration ${duration}`;
            line.amount = amount;
            invoice.line.push(line);
        }

        invoice.dueDate = invoiceDTO.invoiceEndDate;
        const invoiceId = await this.quickbooksHelper.createInvoice(invoice, zone);

        // update the invoice id in the invoice
        invoiceDTO.quickbooks = { invoiceGenerated: true, paymentGenerated: false, invoiceNumber: invoiceId };
        this.invoiceDAO.update(invoiceDTO._id, invoiceDTO, zone);
    }

    private async createQuickbooksPayment(invoiceDTO: InvoiceDTO, advertiserDTO: AdvertiserDTO, zone: string) {
        const payment: any = { };
        payment.amount = invoiceDTO.amount;
        payment.customerEmail = advertiserDTO.user.email;
        payment.invoiceId = invoiceDTO.quickbooks.invoiceNumber;
        await this.quickbooksHelper.createPayment(payment, zone);
        invoiceDTO.quickbooks = { invoiceGenerated: true, paymentGenerated: true,
                                  invoiceNumber: invoiceDTO.quickbooks.invoiceNumber };
        invoiceDTO = await this.invoiceDAO.update(invoiceDTO._id, invoiceDTO, zone);
    }

    private async getAdvertiserCreditLineLimit(advertiserId: string, currentDate: Date, zone: string): Promise<number> {
        // check if this brand has any credit line approved
        const monthlyInvoiceDTO: MonthlyInvoiceDTO = await this.monthlyInvoiceDAO.findActiveMonthlyInvoice(advertiserId, currentDate, zone);
        if (monthlyInvoiceDTO) {
            return monthlyInvoiceDTO.approvedCreditLimit;
        } else {
            return 0;
        }
    }
}
