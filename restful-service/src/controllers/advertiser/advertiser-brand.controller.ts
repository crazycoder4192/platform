import express from 'express';
import mongoose from 'mongoose';
import RoleDAO from '../../daos/admin/role.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import PaymentMethodDAO from '../../daos/advertiser/payment-method.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import UserDAO from '../../daos/user.dao';
import RoleDTO from '../../dtos/admin/role.dto';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions, UserTypes } from '../../util/constants';
import { logger } from '../../util/winston';
import { AdvertiserUserHelper } from './advertiser-user.helper';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class AdvertiserBrandController {
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly paymentMethodDAO: PaymentMethodDAO;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly userDAO: UserDAO;
    private readonly subCampaignDAO: SubCampaignDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly roleDAO: RoleDAO;
    private readonly advertiserUserHelper: AdvertiserUserHelper;

    constructor() {
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.paymentMethodDAO = new PaymentMethodDAO();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.userDAO = new UserDAO();
        this.subCampaignDAO = new SubCampaignDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.roleDAO = new RoleDAO();
        this.advertiserUserHelper = new AdvertiserUserHelper();
    }

    public createBrand = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const dto: AdvertiserBrandDTO = this.toAdvertiserBrandDTO(req);

            try {
                const authorizePayload: any = { advertiserId: req.body.advertiser };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addBrand);
            } catch (error) {
                // there may be a case that there is no brand asssociated with the user yet
                const advertiserDTO: AdvertiserDTO = await this.advertiserDAO.findByEmail(req.email);
                if (advertiserDTO._id.toString() !== req.body.advertiser) {
                    throw error;
                }
            }

            dto.created = { at: new Date(), by: req.email };
            const result = await this.advertiserBrandDAO.create(dto);

            req.body.brandId = result._id.toString();
            const userRole: RoleDTO = await this.roleDAO.getRoleByModuleAndRoleName('Advertiser', 'admin');
            req.body.email = req.email;
            req.body.roleId = userRole._id.toString();
            req.body.advertiserId = result.advertiser.toString();
            const loggedInUser: UserDTO = await this.userDAO.findByEmail(req.email);
            await this.advertiserUserHelper.createUser(loggedInUser, req, res);
            res.json(result);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getBrandPerZone = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        if (req && req.body && req.body._id) {
            try {
                const authorizePayload: any = { brandId: req.body._id };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewBrand);
                const result = await this.advertiserBrandDAO.findById(req.body._id, req.header('zone'));
                res.json(result);
            } catch (err) {
                catchError(err, next);
            }
        } else {
            try {
                const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
                if (!advertiserUser) {
                    throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
                }
                const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewBrand);
                const zoneBrands: any[] = advertiserUser.brands.filter((thisBrand: any) => thisBrand.zone === req.header('zone'));
                if (zoneBrands && zoneBrands.length > 0) {
                    const watchingBrand = zoneBrands.find((thisBrand: any) => thisBrand.isWatching === true);
                    let result;
                    if (watchingBrand) {
                        result = await this.advertiserBrandDAO.findById(watchingBrand.brandId, req.header('zone'));
                    } else {
                        // TODO: set the first brand as watch brand in case no watching brand is set
                        result = await this.advertiserBrandDAO.findById(zoneBrands[0].brandId, req.header('zone'));
                    }
                    // TODO: We should not be passing the aray here, it should be corrected at the webportal side
                    res.json([result]);
                } else {
                    logger.error(`No brands found corresponding to the user email [${req.email}]`);
                }

            } catch (err) {
                catchError(err, next);
            }
        }
    }

    public getBrand = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewBrand);
            const allBrands: any[] = advertiserUser.brands;
            if (allBrands && allBrands.length > 0) {
                const result: any[] = [];
                for (const brand of allBrands) {
                    result.push(await this.advertiserBrandDAO.findByIdWithoutZone(brand.brandId));
                }
                // TODO: We should not be passing the aray here, it should be corrected at the webportal side
                res.json(result);
            } else {
                logger.error(`No brands found corresponding to the user email [${req.email}]`);
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    public brandforPaymentCards = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
                AdvertiserPermissions.viewBrand, AdvertiserPermissions.viewAnalytics);
            let allBrandIds: any[];
            allBrandIds = [];
            if (advertiserUser) {
                const zoneBrands: any[] = advertiserUser.brands.filter((thisBrand: any) => thisBrand.zone === req.header('zone'));
                for (const thisBrand of zoneBrands) {
                    const paymentByBrand = await this.paymentMethodDAO.findByBrandId(thisBrand.brandId, thisBrand.zone);
                    if (!paymentByBrand) {
                        allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
                    }
                }
            }
            if (req.body && req.query.id && req.query.id !== 'undefined') {
                const paymentDetails = await this.paymentMethodDAO.findById(req.query.id, req.header('zone'));
                if (paymentDetails) {
                    for (const brand of paymentDetails.brands) {
                        allBrandIds.push(brand);
                    }
                }
            }
            const brandsList = await this.advertiserBrandDAO.getBrandsNotInPayments(allBrandIds, req.header('zone'));
            res.json(brandsList);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getClientBrands = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewBrand);

            const allBrandIds: any[] = [];
            const zoneBrands: any[] = advertiserUser.brands.filter((thisBrand: any) => thisBrand.zone === req.header('zone'));
            zoneBrands.forEach((thisBrand: any) => {
                allBrandIds.push(mongoose.Types.ObjectId(thisBrand.brandId));
            });
            const adminClientBrands = await this.advertiserBrandDAO.getClientBrands(allBrandIds, req.header('zone'));
            const advertiserUserBrands = await this.advertiserUserDAO.findByBrandIdWithUserDetails(allBrandIds);
            const brandPayments = await this.paymentMethodDAO.findAllByEmailandBrands(req.email, allBrandIds, req.header('zone'));
            let isAdminRole: any;
            for (const element of advertiserUserBrands) {
                if (element.email === req.email) {
                    const roleIds = element.brands.map((x) => x.userRoleId);
                    const roles = await this.roleDAO.getRolesByIds(roleIds);
                    if (roles.find((x) => x.roleName.toLowerCase() === 'admin')) {
                        isAdminRole = true;
                    }
                }
            }
            let userWiseBrands: any = [];
            if (isAdminRole) {
                userWiseBrands = advertiserUserBrands;
            } else {
                advertiserUserBrands.forEach((element) => {
                    if (element.email === req.email) {
                        userWiseBrands.push(element);
                    }
                });
            }
            res.json({ clientWiseBrands: adminClientBrands, advertiserUserWiseBrands: userWiseBrands, userBrandPayments: brandPayments });
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateBrand = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            const dto: AdvertiserBrandDTO = this.toAdvertiserBrandDTO(req);
            const authorizePayload: any = { brandId: req.body._id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.editBrand);
            dto._id = req.body._id;
            dto.modified = { at: new Date(), by: req.email };
            const advertiserBrandDTO = await this.advertiserBrandDAO.update(dto._id, dto);
            res.json({ success: true, message: `${advertiserBrandDTO.brandName} updated successfully` });
        } catch (err) {
            catchError(err, next);
        }
    }

    public deactivateBrand = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        // We cannot deactivate or delete a brand for now. Will implement this in future
        throw new Error('Not implemented yet');
    }

    public getBrandById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            req
                .checkParams('id', "Mandatory field 'id' missing in request param")
                .exists();
            await throwIfInputValidationError(req);
            const authorizePayload: any = { brandId: req.params.id };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewBrand);
            const advertiserBrandDTO: AdvertiserBrandDTO = await this.advertiserBrandDAO.findById(req.params.id, req.header('zone'));
            res.json(advertiserBrandDTO);
        } catch (err) {
            catchError(err, next);
        }
    }

    public brandNameAvailability = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const authorizePayload: any = { advertiserId: req.body.advId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewBrand);
            const advertiserBrandDTO: AdvertiserBrandDTO[] = await this.advertiserBrandDAO.brandNameAvailability(
                req.body.name, req.body.advId, req.header('zone'));
            if (advertiserBrandDTO.length > 0) {
                return res.status(500).json({ type: 'Error', message: 'Brand name not available' });
            } else {
                return res.status(200).json({ type: 'Success', message: 'Brand name available' });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getBrandsDataByLoggedInUser = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction
    ) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const allBrandIds: any[] = [];
        if (advertiserUser) {
            advertiserUser.brands.forEach((thisBrand: any) => {
                allBrandIds.push(thisBrand.brandId);
            });
        }
        const brandsClientWise = [];
        if (allBrandIds.length > 0) {
            // NOTE: the zone is not used here, it is intentional
            const userBrandsByClients = await this.advertiserBrandDAO.getClientWiseBrands(allBrandIds, req.header('zone'));
            if (userBrandsByClients && userBrandsByClients.length > 0) {
                for (const userBrandsByClient of userBrandsByClients) {
                    const clientObj = { clientName: userBrandsByClient.clientName, clientType: userBrandsByClient.clientType, brands: new Array() };
                    for (const userBrand of userBrandsByClient.brands) {
                        const userRoleAndBrandWatchingStatus = await this.advertiserUserDAO.getUserAndBrandDetails(
                            req.email, userBrand.brandId);
                        const activeSubCampaigns = await this.subCampaignDAO.listOfActiveSubCampaignsByBrand(userBrand.brandId);
                        const brandObj = { brandId: userBrand.brandId, brandName: userBrand.brandName, brandType: userBrand.brandType,
                                           activeCampaigns: activeSubCampaigns.length, isWatching: userRoleAndBrandWatchingStatus.brand.isWatching,
                                           userList: new Array(), paymentMethods: new Array(), zone: userBrand.zone };
                        const roleDTO: RoleDTO = await this.roleDAO.getRoleById(userRoleAndBrandWatchingStatus.brand.userRoleId);
                        if (roleDTO.roleName.toLowerCase() === 'admin') {
                            const userListByBrand = await this.advertiserUserDAO.getUsersByBrand(userBrand.brandId);
                            for (const userByBrand of userListByBrand) {
                                const role: RoleDTO = await this.roleDAO.getRoleById(userByBrand.brand.userRoleId);
                                const isLoggedinUser = userByBrand.email === req.email ? true : false;
                                const userDetails = await this.userDAO.findByEmail(userByBrand.email);
                                const userObj = { firstName: '', lastName: '', status: '', isLoggedinUser,
                                                  email: userByBrand.email, userRole: role.roleName };
                                userObj.firstName = userDetails.firstName ? userDetails.firstName : '';
                                userObj.lastName = userDetails.lastName ? userDetails.lastName : '';
                                userObj.status = userDetails.userStatus;
                                brandObj.userList.push(userObj);
                            }
                        }
                        const brandPayments = await this.paymentMethodDAO.getPaymentMethodsByBrandId(userBrand.brandId, req.header('zone'));
                        if (brandPayments) {
                            brandObj.paymentMethods.push(brandPayments);
                        }
                        clientObj.brands.push(brandObj);
                    }
                    brandsClientWise.push(clientObj);
                }
            }
        }
        res.json(brandsClientWise);
    }

    public getUserDetailsVsBrandsByLoggedInUser = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction
    ) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const allAdminRoleBrandIds: any[] = [];
        if (advertiserUser) {
            for (const thisBrand of advertiserUser.brands) {
                const roleDTO: RoleDTO = await this.roleDAO.getRoleById(thisBrand.userRoleId);
                if (roleDTO.roleName.toLowerCase() === 'admin') {
                    allAdminRoleBrandIds.push(thisBrand.brandId);
                }
            }
        }
        const usersListWithAssociatedBrands: any[] = [];
        if (allAdminRoleBrandIds.length > 0) {
            const usersWithAssociatedBrands = await this.advertiserUserDAO.getBrandsAndRolesGoupByUserForGivenBrands(allAdminRoleBrandIds);
            for (const userWithBrands of usersWithAssociatedBrands) {
                const userDetails = await this.userDAO.findByEmail(userWithBrands.email);
                if (userDetails) {
                    const userObj = {
                        email: userWithBrands.email,
                        firstName: userDetails.firstName,
                        lastName: userDetails.lastName,
                        status: userDetails.userStatus,
                        isLoggedInUser: userDetails.email === req.email ? true : false,
                        brands: new Array()
                    };
                    for (const brand of userWithBrands.brands) {
                        const brandDetails = await this.advertiserBrandDAO.findByIdWithoutZone(brand.brandId);
                        if (brandDetails) {
                            const roleDTO: RoleDTO = await this.roleDAO.getRoleById(brand.userRoleId);
                            userObj.brands.push({ brandName: brandDetails.brandName, brandId: brandDetails._id, userRole: roleDTO.roleName });
                        }
                    }
                    usersListWithAssociatedBrands.push(userObj);
                }
            }
        }
        res.json(usersListWithAssociatedBrands);
    }

    private toAdvertiserBrandDTO(req: IAuthenticatedRequest): AdvertiserBrandDTO {
        const payload: AdvertiserBrandDTO = new AdvertiserBrandDTO();
        payload.brandName = req.body.brandName;
        payload.brandType = req.body.brandType;
        payload.clientName = req.body.clientName;
        payload.clientType = req.body.clientType;
        payload.advertiser = req.body.advertiser;
        payload.loggedInEmail = req.body.loggedInEmail;
        if (!req.header('zone')) {
            throw new Error('Zone parameter not passed');
        }
        payload.zone = req.header('zone');
        return payload;
    }
}
