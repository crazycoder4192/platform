import config from 'config';
import express from 'express';
import RoleDAO from '../../daos/admin/role.dao';
import AdvertiserAgencyTypeDAO from '../../daos/advertiser/advertiser-agency-type.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserClientTypeDAO from '../../daos/advertiser/advertiser-client-type.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserAgencyTypeDTO from '../../dtos/advertiser/advertiser-agency-type.dto';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import AdvertiserClientTypeDTO from '../../dtos/advertiser/advertiser-client-type.dto';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions, UserStatuses, UserTypes } from '../../util/constants';
import { GeneralMail } from '../../util/mail/general-mail';
import { logger } from '../../util/winston';
import { AuthorizeAdmin } from '././../admin/authorize-admin';

export class AdvertiserProfileController {

    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserClientTypeDAO: AdvertiserClientTypeDAO;
    private readonly advertiserAgencyTypeDAO: AdvertiserAgencyTypeDAO;
    private readonly userDAO: UserDAO;
    private readonly roleDAO: RoleDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly generalMail: GeneralMail;
    private readonly userTokenDAO: UserTokenDAO;

    constructor() {
        this.advertiserDAO = new AdvertiserDAO();
        this.advertiserClientTypeDAO = new AdvertiserClientTypeDAO();
        this.advertiserAgencyTypeDAO = new AdvertiserAgencyTypeDAO();
        this.userDAO = new UserDAO();
        this.roleDAO = new RoleDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.authorizeAdmin = new AuthorizeAdmin();
        this.generalMail = new GeneralMail();
        this.userTokenDAO = new UserTokenDAO();
     }

    public createProfile = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            // TODO: validate the inputs for advertiser profile

            // one advertiser account can have only one profile
            const existingAdvertiserDTO: AdvertiserDTO = await this.advertiserDAO.findByEmail(req.email);
            if (existingAdvertiserDTO) {
                throw new HandledApplicationError(500, 'Advertiser profile already created, please use update operation');
            }

            // if it does not exist, the check if the email is of advertiser
            const user: UserDTO = await this.userDAO.findByEmail(req.email);
            if (user.userType.toLowerCase() === UserTypes.advertiser.toLowerCase()
                && user.userStatus.toLowerCase() === UserStatuses.active.toLowerCase()) {
                const dto: AdvertiserDTO = this.toAdvertiserDTO(req);
                dto.created = { at: new Date(), by: req.email };
                const result = await this.advertiserDAO.create(dto);
                res.json(result);
            } else {
                logger.error(`Logged in user[${req.email}] is not an advertiser`);
                throw new HandledApplicationError(500, 'Logged in user is not an advertiser');
            }

        } catch (err) {
            catchError(err, next);
        }
    }

    public updateProfile = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            // TODO: validate the inputs for publisher profile

            // profile should be updateable by any of the brand user who has the update-profile premission
            const existingDTO: AdvertiserDTO = await this.advertiserDAO.findById(req.body._id);
            if (!existingDTO) {
                logger.error(`Invalid attempt to update advertiser profile; advertiserId [${req.body._id}]`);
                throw new HandledApplicationError(500, 'Invalid attempt to update advertiser profile');
            }

            if (existingDTO.user.email !== req.email) {
                // check if any other other advertiser user within this advertiser has update-profile permission in
                // any of the brand
                const a: AdvertiserUserDTO[] = await this.advertiserUserDAO.checkAdvertiserPermission(req.email,
                    req.body._id, [AdvertiserPermissions.viewDashboard]);
                if (!a || a.length === 0) {
                    logger.error(`User [${req.email}]do not have enough priviliges to edit the advertiser profile`);
                    throw new HandledApplicationError(500, 'User do not have enough priviliges to edit the advertiser profile');
                }
            }

            const newDTO: AdvertiserDTO = this.toAdvertiserDTO(req);
            newDTO._id = req.body._id;
            newDTO.modified = { at: new Date(), by: req.email };
            const result = await this.advertiserDAO.update(newDTO._id, newDTO);

            if (newDTO.advertiserType === 'Client') {
                const brands: AdvertiserBrandDTO[] = await this.advertiserBrandDAO.findByAdvertiserId(newDTO._id, req.header('zone'));
                await this.advertiserBrandDAO.updateManyClientInfo(brands.map((x) => x._id), newDTO.businessName, newDTO.organizationType);
            }
            res.json(result);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getProfileByEmail = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        let details: AdvertiserDTO = await this.advertiserDAO.findByEmail(
            req.email
        );
        if (!details) {
            const a: AdvertiserUserDTO[] = await this.advertiserUserDAO.findByEmailAndPermission(req.email,
                [AdvertiserPermissions.viewDashboard]);
            if (a.length === 0) {
                logger.error(`You [${req.email}] do not have permission to view the advertiser profile`);
                // throw new Error('You do not have permission to view the advertiser profile');
                return res.status(200).json(null);
            } else if (a.length > 1) {
                logger.error(`Multiple advertiser users against this email [${req.email}] found`);
                throw new HandledApplicationError(500, 'Multiple advertiser users against this email found');
            } else {
                details = await this.advertiserDAO.findById(a[0].advertiserId);
            }
        }

        if (details) {
            res.status(200).json({
                _id: details._id,
                businessName: details.businessName,
                advertiserType: details.advertiserType,
                organizationType: details.organizationType,
                acceptTerms: details.acceptTerms,
                addressLine1: details.address.addressLine1,
                addressLine2: details.address.addressLine2,
                city: details.address.city,
                state: details.address.state,
                zipcode: details.address.zipcode,
                country: details.address.country,
                phoneNumber: details.phoneNumber,
                isPhNumberVerified: details.isPhNumberVerified ? details.isPhNumberVerified : false
            });
        } else {
            res.status(200).json(null);
        }
    }

    public getProviderTypes = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            const advertiserClientTypes: AdvertiserClientTypeDTO[] = await this.advertiserClientTypeDAO.findAll();
            res.json(advertiserClientTypes);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getUniqueProviderTypes = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            const advertiserClientTypes: AdvertiserClientTypeDTO[] = await this.advertiserClientTypeDAO.findUniqueType();
            res.json(advertiserClientTypes);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getUniqueProviderAgencyTypes = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            const advertiserAgencyTypes: AdvertiserAgencyTypeDTO[] = await this.advertiserAgencyTypeDAO.findUniqueType();
            res.json(advertiserAgencyTypes);
        } catch (err) {
            catchError(err, next);
        }
    }

    public incompleteProfiles = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction,
    ) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            const usersFromUserTable: UserDTO[] = await this.userDAO.findActiveUserTypes('advertiser');
            const usersFromAdvertiserUserTables: any[] = await this.advertiserUserDAO.findUsers();
            const url: string = config.get<string>('mail.web-portal-redirection-url');

            const currentDate: Date = new Date();
            const userEmailsFromAdvertiserUserTables: string[] = usersFromAdvertiserUserTables.map((x) => x.email);
            const mailSentToUsers: string[] = [];
            for (const user of usersFromUserTable) {
                if (!userEmailsFromAdvertiserUserTables.includes(user.email)) {
                    if (user.reminder.profileCompletionPending) {
                        let lastMailSentAt: Date = user.reminder.profileCompletionPending.mailSentAt;
                        if (!lastMailSentAt) {
                            lastMailSentAt = user.created.at;
                        }
                        let noOfTimesReminderSent: number = 0;
                        if (user.reminder.profileCompletionPending.noOfMailsSent) {
                            noOfTimesReminderSent = user.reminder.profileCompletionPending.noOfMailsSent;
                        }
                        if (noOfTimesReminderSent < 3) {
                            if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
                                this.sendProfileCompletionPendingReminderAdvertiserMail(user,
                                    `${url}/advertiser/advertiserProfile`, currentDate, noOfTimesReminderSent);
                                mailSentToUsers.push(user.email);
                            } else {
                                // dont do anything
                            }
                        } else if (noOfTimesReminderSent === 3) {
                            if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
                                this.sendProfileCompletionPendingReminderAdvertiserMail(user,
                                    `${url}/advertiser/advertiserProfile`, currentDate, noOfTimesReminderSent);
                                mailSentToUsers.push(user.email);
                            } else {
                                // dont do anything
                            }
                        } else {
                            // dont do anything; we dont have to send mail post 4 reminders
                        }
                    }
                }
            }
            return res.status(200).json({ type: 'Success', message: `Mail sent to users: ${mailSentToUsers}` });
        } catch (err) {
            catchError(err, next);
        }
    }

    private toAdvertiserDTO(req: IAuthenticatedRequest): AdvertiserDTO {
        const payload: AdvertiserDTO = new AdvertiserDTO();
        payload.acceptTerms = req.body.acceptTerms;
        payload.businessName = req.body.businessName;
        payload.advertiserType = req.body.advertiserType;
        payload.phoneNumber = req.body.phoneNumber;
        payload.isPhNumberVerified = req.body.isPhNumberVerified;
        payload.address = {
            addressLine1: req.body.addressLine1, addressLine2: req.body.addressLine2,
            city: req.body.city, state: req.body.state, zipcode: req.body.zipcode, country: req.body.country
        };
        payload.organizationType = req.body.organizationType;
        payload.user = { email: req.email };
        return payload;
    }

    private async sendProfileCompletionPendingReminderAdvertiserMail(user: UserDTO, url: string, currentDate: Date,
                                                                     noOfTimesReminderSent: number) {

        const timeDifference = Math.abs(currentDate.getTime() - user.created.at.getTime());
        const noOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
        if (user.isSubscribedForEmail) {
            await this.generalMail.sendReminderToCompleteProfileAdvertiser(
                user.email,
                noOfDays,
                url,
                `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
            );
        }
        user.reminder.profileCompletionPending.mailSentAt = currentDate;
        user.reminder.profileCompletionPending.noOfMailsSent = noOfTimesReminderSent + 1;
        this.userDAO.update(user._id, user);
    }
}
