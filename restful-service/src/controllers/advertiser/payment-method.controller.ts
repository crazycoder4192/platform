import express from 'express';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import PaymentMethodDAO from '../../daos/advertiser/payment-method.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserPaymentMethodDTO from '../../dtos/advertiser/payment-method.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions, NotificationIcon, NotificationRoute, NotificationType } from '../../util/constants';
import { Encryption } from '../../util/crypto';
import { AdvertiserMail } from '../../util/mail/advertiser-mail';
import { codes } from '../../util/payment-error-codes';
import { logger } from '../../util/winston';
import { AuthorizeNetHelper } from '../util/authorize-net-helper';
import { QuickbooksHelper } from '../util/quickbooks-helper';
import { AuthorizeAdvertiser } from './authorize-advertiser';
const ApiContracts = require('authorizenet').APIContracts;
const ApiControllers = require('authorizenet').APIControllers;
const SDKConstants = require('authorizenet').Constants;
import config from 'config';
import RoleDAO from '../../daos/admin/role.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDTO from '../../dtos/user.dto';
import { RedisHelper } from '../../redis/redis-helper';
import { GeneralMail } from '../../util/mail/general-mail';
import { NotificationHelper } from '../../util/notification-helper';

export class PaymentMethodController {
    public res: any;
    public result = Array();
    private readonly paymentMethodDAO: PaymentMethodDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly userDAO: UserDAO;
    private readonly roleDAO: RoleDAO;
    private readonly encryption: Encryption;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly advertiserMail: AdvertiserMail;
    private readonly quickbooksHelper: QuickbooksHelper;
    private readonly authorizeNetHelper: AuthorizeNetHelper;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly generalMail: GeneralMail;
    private readonly redisHelper: RedisHelper;
    private readonly notificationHelper: NotificationHelper;

    constructor() {
        this.paymentMethodDAO = new PaymentMethodDAO();
        this.advertiserDAO = new AdvertiserDAO();
        this.encryption = new Encryption();
        this.result = new Array();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.userDAO = new UserDAO();
        this.roleDAO = new RoleDAO();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.advertiserMail = new AdvertiserMail();
        this.quickbooksHelper = new QuickbooksHelper();
        this.authorizeNetHelper = new AuthorizeNetHelper();
        this.userTokenDAO = new UserTokenDAO();
        this.brandDAO = new AdvertiserBrandDAO();
        this.generalMail = new GeneralMail();
        this.redisHelper = new RedisHelper();
        this.notificationHelper = new NotificationHelper();
    }

    public addPaymentMethod = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addPaymentMethod);

            const advertiserDetails = await this.advertiserDAO.findById(advertiserUser.advertiserId);
            const userDetails = await this.userDAO.findByEmail(advertiserDetails.user.email);
            const expiryDateAuth = `${req.body.expiryYear}-${this.minTwoDigits(+req.body.expiryMonth)}`;

            const customerDetails = await this.createCustomerForAuthorizeNet(advertiserUser.advertiserId, req);
            // TODO: This user is the advertiser admin user, who can see all the payment methods
            // since here we are checking using email, we should add the other user emails here
            // as well.
            const paymentMethodDetails = await this.paymentMethodDAO.findByEmailId(req.email, req.header('zone'));
            if (paymentMethodDetails != null) {
                const customerProfileIdObj = { customerProfileId: paymentMethodDetails.customerId };
                Object.assign(customerDetails, customerProfileIdObj);
                const createRequest = this.authorizeNetHelper.createAdvertiserPaymentProfile(customerDetails);
                const ctrl = new ApiControllers.CreateCustomerPaymentProfileController(createRequest.getJSON());
                const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
                if (!isSandbox) {
                    ctrl.setEnvironment(SDKConstants.endpoint.production);
                }
                logger.info(`createRequest: ${JSON.stringify(createRequest)}`);
                ctrl.execute(async () => {
                    const apiResponse = ctrl.getResponse();
                    logger.info(`apiResponse: ${JSON.stringify(apiResponse)}`);
                    const response = new ApiContracts.CreateCustomerPaymentProfileResponse(apiResponse);
                    if (response != null && response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                        const dto: AdvertiserPaymentMethodDTO = await this.toPaymentMethodDTO(req);
                        dto.created = { at: new Date(), by: req.email };
                        dto.customerId = paymentMethodDetails.customerId;
                        dto.customerProfileId = response.getCustomerPaymentProfileId();
                        await this.paymentMethodDAO.create(dto, req.header('zone'));
                        await this.redisHelper.updatePayment(advertiserDetails._id.toString(), req.header('zone'));
                        const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
                        const url: string = config.get<string>('mail.web-portal-redirection-url');
                        if (toUser.hasPersonalProfile) {
                            if (toUser.isSubscribedForEmail) {
                                this.advertiserMail.sendNewPaymentMethodAddedMail(
                                    req.email,
                                    toUser.firstName,
                                    `${url}/advertiser/payments`,
                                    `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                                );
                            }
                        }
                        // send notification - starts
                        // get all the admin users in the advertiser for notification
                        try {
                            const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(advertiserUser.advertiserId);
                            const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                            if (sendNotificationTo.length > 0) {
                                const brands: any[] = await this.brandDAO.getBrandNamesByBrandIdsForAdmin(dto.brands, req.header('zone'));
                                if (brands.length > 0) {
                                    const brandNames = brands.map((x) => x.brandName);
                                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                                    const notificationMessage: string = `${toUser.firstName} just added a new payment method for ${brandNames}`;
                                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                                        NotificationType.brand, brands[0]._id.toString(), NotificationRoute.advertiser_payment);
                                }
                            }
                        } catch (err) {
                            logger.error(`${err.stack}\n${new Error().stack}`);
                        }
                        // send notification - ends

                        res.status(200).json({ message: 'payment method added successfully', success: true });
                    } else {
                        logger.error(`Authorize.net error response: ${JSON.stringify(response)}`);
                        const eMessageCode = response.getMessages().getMessage()[0].getCode();
                        const messageTo = codes.find((item) => item.code === eMessageCode);
                        const resMessage = messageTo ? messageTo.text : 'Error';
                        res.status(200).json({ success: false, message: resMessage });
                    }
                });
            } else {
                const createRequest = this.authorizeNetHelper.createAdvertiserCustomerAndPaymentProfile(customerDetails);
                const ctrl = new ApiControllers.CreateCustomerProfileController(createRequest.getJSON());
                const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
                if (!isSandbox) {
                    ctrl.setEnvironment(SDKConstants.endpoint.production);
                }
                logger.info(`createRequest: ${JSON.stringify(createRequest)}`);
                ctrl.execute(async () => {
                    const apiResponse = ctrl.getResponse();
                    logger.info(`apiResponse: ${JSON.stringify(apiResponse)}`);
                    const response = new ApiContracts.CreateCustomerProfileResponse(apiResponse);
                    if (response != null && response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                        const dto: AdvertiserPaymentMethodDTO = await this.toPaymentMethodDTO(req);
                        dto.created = { at: new Date(), by: req.email };
                        dto.customerId = response.getCustomerProfileId();
                        dto.customerProfileId = response.getCustomerPaymentProfileIdList().getNumericString()[0];
                        await this.paymentMethodDAO.create(dto, req.header('zone'));
                        await this.redisHelper.updatePayment(advertiserDetails._id.toString(), req.header('zone'));
                        // TODO: update email link.
                        const goToPaymentsLink: string = 'todo update email link';
                        const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
                        const url: string = config.get<string>('mail.web-portal-redirection-url');
                        if (toUser.hasPersonalProfile) {
                            if (toUser.isSubscribedForEmail) {
                                this.advertiserMail.sendNewPaymentMethodAddedMail(
                                    req.email,
                                    toUser.firstName,
                                    `${url}/advertiser/payments`,
                                    `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                                );
                            }
                        }
                        // send notification - starts
                        try {
                            // get all the admin users in the advertiser for notification
                            const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(advertiserUser.advertiserId);
                            const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                            if (sendNotificationTo.length > 0) {
                                const brands: any[] = await this.brandDAO.getBrandNamesByBrandIdsForAdmin(dto.brands, req.header('zone'));
                                if (brands.length > 0) {
                                    const brandNames = brands.map((x) => x.brandName);
                                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                                    const notificationMessage: string = `${toUser.firstName} just added a new payment method for ${brandNames}`;
                                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                                        NotificationType.brand, brands[0]._id.toString(), NotificationRoute.advertiser_payment);
                                }
                            }
                        } catch (err) {
                            logger.error(`${err.stack}\n${new Error().stack}`);
                        }
                        // send notification - ends
                        res.status(200).json({ success: true, message: 'payment method added successfully' });
                    } else {
                        logger.error(`Authorize.net error response: ${JSON.stringify(response)}`);
                        const eMessageCode = response.getMessages().getMessage()[0].getCode();
                        const messageTo = codes.find((item) => item.code === eMessageCode);
                        const resMessage = messageTo ? messageTo.text : 'Error';
                        res.status(200).json({ success: false, message: resMessage });
                    }
                });
            }

            try {
                // create quickbooks customer if not already created
                const u = await this.userDAO.findByEmail(req.email);
                const customer: any = { };
                customer.organizationName = advertiserDetails.businessName;
                customer.email = req.email;
                customer.firstName = u.firstName;
                customer.lastName = u.lastName;
                customer.phoneNumber = advertiserDetails.phoneNumber;
                customer.addressLine1 = advertiserDetails.address.addressLine1;
                customer.city = advertiserDetails.address.city;
                customer.state = advertiserDetails.address.state;
                customer.country = advertiserDetails.address.country;
                const pbCustomerDetails = await this.quickbooksHelper.findCustomerByEmail(customer.email, req.header('zone'));
                if (!pbCustomerDetails) {
                    await this.quickbooksHelper.createCustomer(customer, req.header('zone'));
                }
            } catch (err) {
                // do nothing
                logger.error(`${err.stack}\n${new Error().stack}`);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAllPaymentMethods = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        if (!advertiserUser) {
            throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
        }
        const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
        await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewPaymentMethod);

        if (req.params.id) {
            try {
                const paymentMethodDTO = await this.paymentMethodDAO.findById(req.params.id, req.header('zone'));
                this.result = [];
                await this.authorizeNetHelper.getCustomerPaymentProfile(paymentMethodDTO.customerId,
                    paymentMethodDTO.customerProfileId, (response: any) => {
                    if (response != null) {
                        this.result.push({
                            _id: paymentMethodDTO._id,
                            brands: paymentMethodDTO.brands,
                            customerProfileId: paymentMethodDTO.customerProfileId,
                            customerId: paymentMethodDTO.customerId,
                            nameOnCard: paymentMethodDTO.nameOnCard,
                            billingCurrency: paymentMethodDTO.billingCurrency,
                            billingCountry: paymentMethodDTO.billingCountry,
                            cardNumber: response.getPaymentProfile().getPayment().getCreditCard().getCardNumber(),
                            expiryDate: this.encryption.decrypt(paymentMethodDTO.expiryDate),
                            cvv: this.encryption.decrypt(paymentMethodDTO.cvv) ? this.encryption.decrypt(paymentMethodDTO.cvv) : 'xxx',
                            hasBillingAddress: paymentMethodDTO.hasBillingAddress,
                            phoneNumber: paymentMethodDTO.phoneNumber,
                            isPhNumberVerified: paymentMethodDTO.isPhNumberVerified,
                            addressLine1: paymentMethodDTO.address ? paymentMethodDTO.address.addressLine1 : '',
                            addressLine2: paymentMethodDTO.address ? paymentMethodDTO.address.addressLine2 : '',
                            city: paymentMethodDTO.address ? paymentMethodDTO.address.city : '',
                            state: paymentMethodDTO.address ? paymentMethodDTO.address.state : '',
                            zipcode: paymentMethodDTO.address ? paymentMethodDTO.address.zipcode : '',
                            country: paymentMethodDTO.address ? paymentMethodDTO.address.country : ''
                        });
                        if (this.result.length > 0) {
                            res.json(this.result);
                        }
                    }
                });
            } catch (err) {
                catchError(err, next);
            }
        } else {
            try {
                const brandIds: string[] = advertiserUser.brands.map((x) => x.brandId);
                const listOfPaymentMethods = await this.paymentMethodDAO.findByBrandForAccounts(brandIds, req.header('zone'));
                this.result = [];
                let inc = 0;
                if (listOfPaymentMethods.length === 0) {
                    res.json(this.result);
                } else {
                    for (const dbmethod of listOfPaymentMethods) {
                        inc++;
                        await this.authorizeNetHelper.getCustomerPaymentProfile(dbmethod.customerId, dbmethod.customerProfileId, (results: any) => {
                            if (results != null) {
                                this.result.push({
                                    _id: dbmethod._id, brands: dbmethod.brands,
                                    cardNumber: results.getPaymentProfile().getPayment()
                                        .getCreditCard().getCardNumber(),
                                    cardType: results.getPaymentProfile().getPayment()
                                        .getCreditCard().getCardType(),
                                    expiryDate: this.encryption.decrypt(dbmethod.expiryDate),
                                    created: dbmethod.created.at
                                });
                            }
                            if (inc === this.result.length) {
                                this.result = this.result.sort((a, b) => (a.created > b.created) ? 1 : -1);
                                res.json(this.result);
                            }
                        });
                    }
                }
            } catch (err) {
                catchError(err, next);
            }
        }
    }

    public updatePaymentMethod = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.editPaymentMethod);

            const expiryDateAuth = `${req.body.expiryYear}-${this.minTwoDigits(+req.body.expiryMonth)}`;
            const paymentMethodDTO: any = {
                brands: req.body.brands,
                expiryDate: this.encryption.encrypt(expiryDateAuth),
                modified: { at: new Date(), by: req.email }, nameOnCard: req.body.nameOnCard
            };

            if (req.body.hasBillingAddress) {
                paymentMethodDTO.phoneNumber = `${req.body.countryCodePhNumber}${req.body.phoneNumber}`;
                paymentMethodDTO.isPhNumberVerified = req.body.isPhNumberVerified;
                paymentMethodDTO.address = {
                    addressLine1: req.body.addressLine1, addressLine2: req.body.addressLine2,
                    city: req.body.city, state: req.body.state, zipcode: req.body.zipcode, country: req.body.country
                };
            }

            const result = await this.paymentMethodDAO.findById(req.params.id, req.header('zone'));
            const customerDetails = await this.createCustomerForAuthorizeNet(advertiserUser.advertiserId, req);

            // the card number above will be obfuscated card number, we need to replace that with the correct one
            await this.authorizeNetHelper.getCustomerPaymentProfile(result.customerId, result.customerProfileId, (results: any) => {
                if (results != null) {
                    customerDetails.cardNumber = results.getPaymentProfile().getPayment().getCreditCard().getCardNumber();
                    const updatePaymentProfile = this.authorizeNetHelper.updateCustomerPaymentProfile(result.customerId,
                        result.customerProfileId, customerDetails);
                    const ctrl = new ApiControllers.UpdateCustomerPaymentProfileController(updatePaymentProfile.getJSON());
                    const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
                    if (!isSandbox) {
                        ctrl.setEnvironment(SDKConstants.endpoint.production);
                    }
                    ctrl.execute(async () => {
                        const apiResponse = ctrl.getResponse();
                        const response = new ApiContracts.UpdateCustomerPaymentProfileResponse(apiResponse);
                        if (response != null && response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                            this.paymentMethodDAO.updatePaymentMethodData(req.params.id, paymentMethodDTO, req.header('zone'));
                            await this.redisHelper.updatePayment(advertiserUser.advertiserId.toString(), req.header('zone'));
                            // send notification - starts
                            try {
                                // get all the admin users in the advertiser for notification
                                const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(advertiserUser.advertiserId);
                                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                                if (sendNotificationTo.length > 0) {
                                    const brands: any[] = await this.brandDAO.getBrandNamesByBrandIdsForAdmin(req.body.brands, req.header('zone'));
                                    if (brands.length > 0) {
                                        const brandNames = brands.map((x) => x.brandName);
                                        const user = await this.userDAO.findByEmail(req.email);
                                        const notificationIcon: NotificationIcon = NotificationIcon.info;
                                        const notificationMessage: string = `${user.firstName} just updated payment method for ${brandNames}`;
                                        await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                                            NotificationType.brand, brands[0]._id.toString(), NotificationRoute.advertiser_payment);
                                    }
                                }
                            } catch (err) {
                                logger.error(`${err.stack}\n${new Error().stack}`);
                            }
                            // send notification - ends
                            res.status(200).json({ success: true, message: 'payment method updated successfully' });
                        } else {
                            logger.error(`Authorize.net error response: ${JSON.stringify(response)}`);
                            const eMessageCode = response.getMessages().getMessage()[0].getCode();
                            const messageTo = codes.find((item) => item.code === eMessageCode);
                            const resMessage = messageTo ? messageTo.text : 'Error';
                            res.status(200).json({ success: false, message: resMessage });
                        }
                    });
                } else {
                    res.status(200).json({ success: false, message: 'Unable to get the customer payment details from authorize.net' });
                }
            });
        } catch (err) {
            catchError(err, next);
        }
    }

    public deletePaymentMethod = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.deletePaymentMethod);
            const dto: AdvertiserPaymentMethodDTO = await this.paymentMethodDAO.findById(req.params.id, req.header('zone'));
            dto.deleted = true;
            dto.modified = { at: new Date(), by: req.email };
            const list = await this.paymentMethodDAO.findAllByEmail(req.email, req.header('zone'));
            if (list.length === 1) {
                this.authorizeNetHelper.deleteCustomerProfile(dto.customerId);
            }
            this.authorizeNetHelper.deleteCustomerPaymentProfile(dto.customerId, dto.customerProfileId);
            await this.paymentMethodDAO.delete(req.params.id, dto, req.header('zone'));
            // send notification - starts
            try {
                // get all the admin users in the advertiser for notification
                const allAdminUsers: string[] = await this.advertiserUserDAO.getAdvertiserAdminUsers(req.body.advertiserId);
                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                if (sendNotificationTo.length > 0) {
                    const brands: any[] = await this.brandDAO.getBrands(req.body.brands, req.header('zone'));
                    if (brands.length > 0) {
                        const brandNames = brands.map((x) => x.brandName);
                        const user = await this.userDAO.findByEmail(req.email);
                        const notificationIcon: NotificationIcon = NotificationIcon.info;
                        const notificationMessage: string = `${user.firstName} deleted payment method for ${brandNames}`;
                        await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                            NotificationType.brand, brands[0]._id.toString(), NotificationRoute.advertiser_payment);
                    }
                }
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
            }
            // send notification - ends
            res.status(200).json({ success: true, message: 'payment method deleted successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    private async toPaymentMethodDTO(req: IAuthenticatedRequest): Promise<AdvertiserPaymentMethodDTO> {
        const dto = new AdvertiserPaymentMethodDTO();
        const expiryDateAuth = `${req.body.expiryYear}-${this.minTwoDigits(+req.body.expiryMonth)}`;
        dto.billingCountry = req.body.billingCountry ? req.body.billingCountry : '';
        dto.billingCurrency = req.body.billingCurrency ? req.body.billingCurrency : '';
        // dto.cardNumber = req.body.cardNumber ? req.body.cardNumber : '';
        dto.nameOnCard = req.body.nameOnCard ? req.body.nameOnCard : '';
        dto.expiryDate = expiryDateAuth ? this.encryption.encrypt(expiryDateAuth) : '';
        dto.cvv = req.body.cvv ? this.encryption.encrypt(req.body.cvv) : '';
        dto.brands = req.body.brands ? req.body.brands : '';
        // dto.amount = req.body.amount ? req.body.amount : '';

        dto.hasBillingAddress = req.body.hasBillingAddress;
        dto.address = {
            addressLine1: req.body.addressLine1, addressLine2: req.body.addressLine2,
            city: req.body.city, state: req.body.state, zipcode: req.body.zipcode, country: req.body.country
        };
        dto.phoneNumber = `${req.body.countryCodePhNumber}${req.body.phoneNumber}`;
        dto.isPhNumberVerified = req.body.isPhNumberVerified;

        return dto;
    }

    private minTwoDigits(n: number) {
        return (n < 10 ? '0' : '') + n;
    }

    private async createCustomerForAuthorizeNet(advertiserId: string, req: IAuthenticatedRequest) {
        const advertiserDetails = await this.advertiserDAO.findById(advertiserId);
        const names: string[] = req.body.nameOnCard.trim().split(' ');
        const expiryDateAuth = `${req.body.expiryYear}-${this.minTwoDigits(+req.body.expiryMonth)}`;

        const customerDetails = {
            firstName: names[0].trim(),
            lastName: req.body.nameOnCard.substring(names[0].length, req.body.nameOnCard.length).trim(),
            address: `${advertiserDetails.address.addressLine1} ${advertiserDetails.address.addressLine2}`,
            city: advertiserDetails.address.city,
            state: advertiserDetails.address.state,
            zipcode: advertiserDetails.address.zipcode,
            country: advertiserDetails.address.country,
            phoneNumber: advertiserDetails.phoneNumber,
            company: advertiserDetails.businessName,
            email: req.email,
            cardNumber: req.body.cardNumber,
            cvv: req.body.cvv,
            expiryDate: expiryDateAuth,
            // passing advertiser profile id in description param due to max length issue in merchant id
            description: `advertiser profile id: ${advertiserDetails._id}`
        };
        if (req.body.hasBillingAddress) {
            customerDetails.address = `${req.body.addressLine1} ${req.body.addressLine2}`;
            customerDetails.country = req.body.country;
            customerDetails.city = req.body.city;
            customerDetails.zipcode = req.body.zipcode;
            customerDetails.phoneNumber = `${req.body.countryCodePhNumber}${req.body.phoneNumber}`;
        }
        return customerDetails;
    }

}
