import express from 'express';
import mongoose from 'mongoose';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import ClientBrandMetadataDAO from '../../daos/advertiser/client-brand-metadata.dao';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';

export class ClientBrandMetadataController {

    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly clientBrandMetadataDAO: ClientBrandMetadataDAO;

    constructor() {
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.clientBrandMetadataDAO = new ClientBrandMetadataDAO();
    }

    public getAllClients = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const clients = await this.clientBrandMetadataDAO.getClients(req.header('zone'));
            res.json(clients);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAllBrands = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            if (req.params.clientId) {
                const clients = await this.clientBrandMetadataDAO.getBrands(mongoose.Types.ObjectId(req.params.clientId), req.header('zone'));
                res.json(clients);
            } else {
                throw new HandledApplicationError(500, "Mandatory param 'client' not passed");
            }
        } catch (err) {
            catchError(err, next);
        }
    }
}
