import express from 'express';
import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserDashBoardDAO from '../../daos/advertiser/advertiser-dashboard.dao';
import AnalyticsDAO from '../../daos/advertiser/analytics.dao';
import CampaignDAO from '../../daos/advertiser/campaign.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import UserDAO from '../../daos/user.dao';
import AnalyticsDTO from '../../dtos/advertiser/analytics.dto';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AdvertiserPermissions } from '../../util/constants';
import { logger } from '../../util/winston';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class AdvertiserAnalyticsController {

  private readonly analyticsDAO: AnalyticsDAO;
  private readonly subcampaignDAO: SubCampaignDAO;
  private readonly advertiserDashBoardDAO: AdvertiserDashBoardDAO;
  private readonly userDAO: UserDAO;
  private readonly authorizeAdvertiser: AuthorizeAdvertiser;
  private readonly helper: Helper;
  private readonly campaignDAO: CampaignDAO;
  private readonly advertiserBrandDAO: AdvertiserBrandDAO;

  constructor() {
    this.analyticsDAO = new AnalyticsDAO();
    this.subcampaignDAO = new SubCampaignDAO();
    this.advertiserDashBoardDAO = new AdvertiserDashBoardDAO();
    this.userDAO = new UserDAO();
    this.authorizeAdvertiser = new AuthorizeAdvertiser();
    this.helper = new Helper();
    this.campaignDAO = new CampaignDAO();
    this.advertiserBrandDAO = new AdvertiserBrandDAO();
  }

  public getAnalyticsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const authorizePayload = {
        brandId: req.body.brandId,
        campaignId: req.body.campaignId,
        subcampaignId: req.body.subcampaignId
      };

      await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewAnalytics);
      // let analyticsDTO: AnalyticsDTO = new AnalyticsDTO();
      const filter: FilterAnalyticsDTO = await this.toAnalyticsFilterDTO(req);

      const timezone: string = req.body.timezone;
      if (req.body.tabType === 'audienceTab') {
        const hcpIds: number[] = await this.analyticsDAO.getAudienceIds(filter, timezone, req.header('zone'));
        const audienceGenderExperienceData = await this.analyticsDAO.getAudienceGenderExperiencePieChart(hcpIds);
        const audienceSpecializationData = await this.analyticsDAO.getAudienceSpecializationStackBarChart(filter, hcpIds, req.header('zone'));
        const audienceLocationData = await this.analyticsDAO.getAudienceLocationMapChart(filter, hcpIds, req.header('zone'));
        const audienceArcheTypeData = await this.analyticsDAO.getAudienceArcheTypeBarChart(filter, hcpIds);
        const audienceFrequencyData = await this.analyticsDAO.getAudienceFrequency(filter, timezone, hcpIds, req.header('zone'));
        res.json({
          startDate: filter.startDate,
          endDate: filter.endDate,
          audienceGenderExperience: audienceGenderExperienceData,
          audienceSpecialization: audienceSpecializationData,
          audienceLocation: audienceLocationData,
          audienceArcheType: audienceArcheTypeData,
          audienceFrequency: audienceFrequencyData
        });
      }
      if (req.body.tabType === 'behaviorTab') {
        const behaviorTimeOfDayEnagementData = await this.analyticsDAO.getBehaviorTimeOfDayHeatChart(filter, timezone, req.header('zone'));
        const behaviorDisplayLocationData = await this.analyticsDAO.getBehaviorDisplayLocationStackBarChart(filter, timezone, req.header('zone'));
        const behaviorDevicesData = await this.analyticsDAO.getBehaviorDeviceDonutChart(filter, timezone, req.header('zone'));
        res.json({
          behaviorTimeOfDayEnagement: behaviorTimeOfDayEnagementData,
          behaviorDisplayLocation: behaviorDisplayLocationData,
          behaviorDevices: behaviorDevicesData,
        });
      }
      if (req.body.tabType === 'conversionTab') {
        const conversionEnagementData = await this.analyticsDAO.getConversionEngagementAreaLineChart(filter, timezone, req.header('zone'));
        const conversionResultsData = await this.analyticsDAO.getConversionResultsLineChart(filter, timezone, req.header('zone'));
        res.json({
          conversionEnagement: conversionEnagementData,
          conversionResult: conversionResultsData,
        });
      }
      if (req.body.tabType === 'campaignsTab') {
        const campaignsTopCampaignsData = await this.analyticsDAO.getOverviewTopAdAssetsScatterChart(filter, timezone, req.header('zone'));
        const campaignsActiveCampaignsData = await this.analyticsDAO.getActiveCampaignsAdTypeColumnChart(filter, timezone);
        res.json({
          campaignsTopCampaigns: campaignsTopCampaignsData,
          campaignsActiveCampaigns: campaignsActiveCampaignsData,
        });
      }
      // analyticsDTO = this.toAnalyticsDTO(filter, audienceGenderExperience, audienceSpecialization,
      //   audienceLocation, audienceArcheType, audienceFrequency,
      //   behaviorTimeOfDayEnagement, behaviorDisplayLocation, behaviorDevices,
      //   conversionEnagement, conversionResults,
      //   campaignsTopCampaigns, campaignsActiveCampaigns);
      // res.json(analyticsDTO);
    } catch (err) {
      catchError(err, next);
    }
  }
  public getAnalyticsCardsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const dto: FilterAnalyticsDTO = await this.toAnalyticsFilterDTO(req);
      const userData = await this.userDAO.findByEmail(req.email);
      const logOutTime = this.helper.sanitizeLastLogoutTime(userData);
      // the below service is using user logout. Hence, this is not compatible with custom duration
      // Fixing this for PLAT-626 Dt: 13JAN2020 - Shravan
      // const cardsData = await this.advertiserDashBoardDAO.getSubCampaignDataForCards(dto.subCampaignIds, logOutTime);
      const cardsData = await this.advertiserDashBoardDAO.getSubCampaignDataForAnalyticsCards(dto.subCampaignIds,
        dto.startDate, dto.endDate, req.header('zone'));
      res.json(cardsData);
    } catch (err) {
      catchError(err, next);
    }
  }

  private async toAnalyticsFilterDTO(
    req: IAuthenticatedRequest,
  ): Promise<FilterAnalyticsDTO> {
    const dto: FilterAnalyticsDTO = new FilterAnalyticsDTO();
    dto.startDate = req.body.startDate;
    dto.endDate = req.body.endDate;

    dto.specialization = [];

    if (req.body.specialization) {
      for (const a of req.body.specialization) {
        if (typeof(a) === 'string') {
          dto.specialization.push(a);
        } else if (Array.isArray(a)) {
          for (const b of a) {
            dto.specialization.push(b);
          }
        } else {
          logger.error(`uknown type encountered: ${JSON.stringify(a)}`);
        }
      }
    }

    /*dto.experience = [];

    dto.bannerType = [];
    if (req.body.bannerType) {
      for (const a of req.body.bannerType) {
        if (typeof(a) === 'string') {
          dto.bannerType.push(a);
        } else if (Array.isArray(a)) {
          for (const b of a) {
            dto.bannerType.push(b);
          }
        } else {
          logger.error(`uknown type encountered: ${JSON.stringify(a)}`);
        }
      }
    }*/
    dto.gender = [];
    if (req.body.gender) {
      for (const a of req.body.gender) {
        if (typeof(a) === 'string') {
          dto.gender.push(a);
        } else if (Array.isArray(a)) {
          for (const b of a) {
            dto.gender.push(b);
          }
        } else {
          logger.error(`uknown type encountered: ${JSON.stringify(a)}`);
        }
      }
    }
    dto.location = [];
    if (req.body.location) {
      for (const a of req.body.location) {
        if (typeof(a) === 'string') {
          dto.location.push(a);
        } else if (Array.isArray(a)) {
          for (const b of a) {
            dto.location.push(b);
          }
        } else {
          logger.error(`uknown type encountered: ${JSON.stringify(a)}`);
        }
      }
    }
    if (req.body.subCampaignId) {
      dto.subCampaignIds = [mongoose.Types.ObjectId(req.body.subCampaignId)];
      const subCampaign = await this.subcampaignDAO.findById(req.body.subCampaignId);
      if (new Date(dto.startDate) < new Date(subCampaign.created.at)) {
        dto.startDate = subCampaign.created.at;
      }
     // dto.endDate = new Date();
    } else if (req.body.campaignId) {
      const doc: any[] = await this.subcampaignDAO.listOfSubCampaignsByCampaignId(req.body.campaignId);
      const subcampaignIds: any[] = [];
      for (const d of doc) {
        subcampaignIds.push(mongoose.Types.ObjectId(d._id));
      }
      dto.subCampaignIds = subcampaignIds;
      const campaign = await this.campaignDAO.findById(req.body.campaignId);
      if (new Date(dto.startDate) < new Date(campaign.created.at)) {
        dto.startDate = campaign.created.at;
      }
     // dto.startDate = campaign.created.at;
      // dto.endDate = new Date();
    } else if (req.body.brandId) {
      const doc: any[] = await this.subcampaignDAO.listOfSubCampaignsByBrandId(req.body.brandId);
      const subcampaignIds: any[] = [];
      for (const d of doc) {
        subcampaignIds.push(mongoose.Types.ObjectId(d._id));
      }
      dto.subCampaignIds = subcampaignIds;
      const brand = await this.advertiserBrandDAO.findById(req.body.brandId, req.header('zone'));
      if (new Date(dto.startDate) < new Date(brand.created.at)) {
        dto.startDate = brand.created.at;
      }
      // dto.startDate = brand.created.at;
     // dto.endDate = new Date();
    }
    dto.isHcpFilterSelected = req.body.isHcpFilterSelected;
    dto.advertiserId = mongoose.Types.ObjectId(req.body.advertiserId);
    dto.brandId = mongoose.Types.ObjectId(req.body.brandId);
    dto.user = req.body.email;
    return dto;
  }

  private toAnalyticsDTO(
    filter: any,
    audienceGenderExperienceData: any,
    audienceSpecializationData: any,
    audienceLocationData: any,
    audienceArcheTypeData: any,
    audienceFrequencyData: any,

    behaviorTimeOfDayEnagementData: any,
    behaviorDisplayLocationData: any,
    behaviorDevicesData: any,

    conversionEnagementData: any,
    conversionResultsData: any,

    campaignsTopCampaigns: any,
    campaignsActiveCampaigns: any

  ): AnalyticsDTO {
    const dto: AnalyticsDTO = new AnalyticsDTO();
    dto.audienceGenderExperience = audienceGenderExperienceData;
    dto.audienceSpecialization = audienceSpecializationData;
    dto.audienceArcheType = audienceArcheTypeData;
    dto.audienceLocation = audienceLocationData;
    dto.audienceFrequency = audienceFrequencyData;

    dto.behaviorDisplayLocation = behaviorDisplayLocationData;
    dto.behaviorDevices = behaviorDevicesData;
    dto.behaviorTimeOfDayEnagement = behaviorTimeOfDayEnagementData;

    dto.conversionEnagement = conversionEnagementData;
    dto.conversionResult = conversionResultsData;

    dto.campaignsTopCampaigns = campaignsTopCampaigns;
    dto.campaignsActiveCampaigns = campaignsActiveCampaigns;
    dto.startDate = filter.startDate;
    dto.endDate = filter.endDate;
    return dto;
  }
}
