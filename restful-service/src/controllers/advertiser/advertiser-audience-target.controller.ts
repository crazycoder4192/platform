import express from 'express';
import { AdvertiserAudienceTargetDAO } from '../../daos/advertiser/advertiser-audience-target.dao';
import { AdvertiserAudienceDAO } from '../../daos/advertiser/advertiser-audience.dao';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import AdvertiserAudienceTargetDTO from '../../dtos/advertiser/advertiser-audience-target.dto';
import AdvertiserAudienceDTO from '../../dtos/advertiser/advertiser-audience.dto';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import Advertiser from '../../dtos/advertiser/advertiser.dto';
import SubCampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { RedisHelper } from '../../redis/redis-helper';
import { AdvertiserPermissions } from '../../util/constants';
import { logger } from '../../util/winston';
import { AuthorizeAdmin } from '../admin/authorize-admin';
import { AuthorizeAdvertiser } from './authorize-advertiser';

export class AdvertiserAudienceTargetController {

    private readonly advertiserDAO: AdvertiserDAO;
    private readonly brandDAO: AdvertiserBrandDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly advertiserAudienceDAO: AdvertiserAudienceDAO;
    private readonly advertiserAudienceTargetDAO: AdvertiserAudienceTargetDAO;
    private readonly subcampaignDAO: SubCampaignDAO;
    private readonly redisHelper: RedisHelper;

    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly authorizeAdmin: AuthorizeAdmin;

    constructor() {
        this.advertiserDAO = new AdvertiserDAO();
        this.brandDAO = new AdvertiserBrandDAO();
        this.advertiserAudienceDAO = new AdvertiserAudienceDAO();
        this.advertiserAudienceTargetDAO = new AdvertiserAudienceTargetDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.subcampaignDAO = new SubCampaignDAO();
        this.redisHelper = new RedisHelper();

        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.authorizeAdmin = new AuthorizeAdmin();
    }

    public createAudienceTarget = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            if (!req.body.brandId) {
                throw new HandledApplicationError(500, 'Mandatory field brandId is missing');
            }
            if (!req.body.advertiserId) {
                throw new HandledApplicationError(500, 'Mandatory field advertiserId is missing');
            }
            if (!req.body.name) {
                throw new HandledApplicationError(500, 'Mandatory field name is missing');
            }
            const authorizePayload: any = { brandId: req.body.brandId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);

            const existingName: any[] = await this.advertiserAudienceTargetDAO.findByName(req.body.brandId, req.body.name);
            if (existingName && existingName.length > 0) {
                throw new HandledApplicationError(500, `Audience target name '${req.body.name}' is already taken.`);
            }
            const targetDTO: AdvertiserAudienceTargetDTO = new AdvertiserAudienceTargetDTO();
            const advertiserDetails: Advertiser = await this.advertiserDAO.findById(
                advertiserUser.advertiserId,
            );
            const brandDetails: AdvertiserBrandDTO = await this.brandDAO.findById(
                req.body.brandId,
                req.header('zone')
            );
            targetDTO.advertiserId = advertiserDetails;
            targetDTO.brandId = brandDetails;
            targetDTO.name = req.body.name;
            if (req.body.isFileUpload) {
                targetDTO.audienceUploaded = true;
                targetDTO.uploadAudienceTargetDetails = req.body.uploadAudienceTargetDetails ? req.body.uploadAudienceTargetDetails : null;
                targetDTO.createAudienceTargetDetails = null;
            } else {
                targetDTO.audienceUploaded = false;
                targetDTO.createAudienceTargetDetails = req.body.createAudienceTargetDetails ? req.body.createAudienceTargetDetails : null;
                targetDTO.uploadAudienceTargetDetails = null;
            }
            targetDTO.created = { at: new Date(), by: req.email };
            targetDTO.modified = { at: new Date(), by: req.email };
            const savedTargetDTO = await this.advertiserAudienceTargetDAO.create(targetDTO);

            const audienceDTO: AdvertiserAudienceDTO = new AdvertiserAudienceDTO();
            let npis: number[] = [];
            if (req.body.isFileUpload) {
                npis = await this.getNpiListForUpload(req.body.uploadAudienceTargetDetails);
            } else {
                npis = await this.getNpiListForCreate(req.body.createAudienceTargetDetails);
            }
            audienceDTO.advertiserAudienceTargetId = savedTargetDTO;
            audienceDTO.npi = npis;
            audienceDTO.created = { at: new Date(), by: req.email };
            audienceDTO.modified = { at: new Date(), by: req.email };
            const savedAudienceDTO = await this.advertiserAudienceDAO.create(audienceDTO);
            res.json(savedTargetDTO);
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateAudienceTarget = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { brandId: req.body.brandId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);
            const targetDTO: AdvertiserAudienceTargetDTO = await this.advertiserAudienceTargetDAO.findById(req.body._id);
            if (!targetDTO) {
                throw new HandledApplicationError(500, 'Invalid audience target id');
            }
            if (targetDTO.name !== req.body.name) {
                const existingName: any[] = await this.advertiserAudienceTargetDAO.findByName(req.body.brandId, req.body.name);
                if (existingName && existingName.length > 0) {
                    throw new HandledApplicationError(500, `Audience target name '${req.body.name}' is already taken.`);
                }
                targetDTO.name = req.body.name;
            }
            /*If the existed file is not changed then simply update the name */
            if (req.body.isFileUpload &&
                req.body.uploadAudienceTargetDetails.actualFile === targetDTO.uploadAudienceTargetDetails.actualFile) {
                    const updatedTargetDTOOnlyName = await this.advertiserAudienceTargetDAO.update(targetDTO._id, targetDTO);
                    res.json(updatedTargetDTOOnlyName);
            } else {
                if (req.body.isFileUpload) {
                    targetDTO.audienceUploaded = true;
                    targetDTO.uploadAudienceTargetDetails = req.body.uploadAudienceTargetDetails ? req.body.uploadAudienceTargetDetails : null;
                    targetDTO.createAudienceTargetDetails = null;
                } else {
                    targetDTO.audienceUploaded = false;
                    targetDTO.uploadAudienceTargetDetails = null;
                    targetDTO.createAudienceTargetDetails = req.body.createAudienceTargetDetails ? req.body.createAudienceTargetDetails : null;
                }
                targetDTO.modified = { at: new Date(), by: req.email };
                const updatedTargetDTO = await this.advertiserAudienceTargetDAO.update(targetDTO._id, targetDTO);
                const audienceDTOs: AdvertiserAudienceDTO[] = await this.advertiserAudienceDAO.findByTargetId(targetDTO._id);
                let npis: number[] = [];
                if (req.body.isFileUpload) {
                    npis = await this.getNpiListForUpload(req.body.uploadAudienceTargetDetails);
                } else {
                    npis = await this.getNpiListForCreate(req.body.createAudienceTargetDetails);
                }
                if (audienceDTOs && audienceDTOs.length > 1) {
                    throw new HandledApplicationError(500, 'Did not expect multiple audience corresponding to a single target');
                }
                const audienceDTO = new AdvertiserAudienceDTO();
                audienceDTO.npi = npis;
                audienceDTO.modified = { at: new Date(), by: req.email };
                if (!audienceDTOs || audienceDTOs.length === 0) {
                    // this condition can arise because of some glitch but we should handle this
                    audienceDTO.created = { at: new Date(), by: req.email };
                    audienceDTO.advertiserAudienceTargetId = updatedTargetDTO;
                    const savedAudienceDTO = await this.advertiserAudienceDAO.create(audienceDTO);
                } else {
                    const updatedAudienceDTO = await this.advertiserAudienceDAO.update(audienceDTOs[0]._id, audienceDTO);
                    await this.redisHelper.loadSubcampaignsForNpi();
                }
                res.json(updatedTargetDTO);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public deleteAudienceTarget = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const targetDTO: AdvertiserAudienceTargetDTO = await this.advertiserAudienceTargetDAO.findById(req.params.id);
            if (!targetDTO) {
                throw new HandledApplicationError(500, 'Invalid audience target id');
            }
            const authorizePayload: any = { brandId: targetDTO.brandId.toString() };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);

            const subcampaignDTOs: SubCampaignDTO[] = await this.subcampaignDAO.findByAudienceTargetIds(req.params.id);

            if (subcampaignDTOs.length > 0) {
                let name: string = '';
                for (const s of subcampaignDTOs) {
                    name = `${name}${s.name}, `;
                }
                if (name.length > 2) {
                    name = name.substring(0, name.length - 2);
                }
                throw new HandledApplicationError(500,
                    `Cannot delete as the selected audience target is used in subcampaigns [${name}]`);
            } else {
                targetDTO.deleted = true;
                res.json(await this.advertiserAudienceTargetDAO.update(targetDTO._id, targetDTO));
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public listAudienceTarget = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const authorizePayload: any = { brandId: req.params.brandId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);
            // NOTE: this wont return the target details
            const targetDTOs: AdvertiserAudienceTargetDTO[] = await this.advertiserAudienceTargetDAO.findByBrand(req.params.brandId);
            const results: any[] = [];
            for (const t of targetDTOs) {
                const audienceDTOs: AdvertiserAudienceDTO[] = await this.advertiserAudienceDAO.findByTargetId(t._id);
                if (audienceDTOs && audienceDTOs.length > 1) {
                    throw new HandledApplicationError(500, 'Did not expect multiple audience corresponding to a single target');
                }
                const r: any = { };
                r._id = t._id;
                r.advertiserId = t.advertiserId;
                r.audienceUploaded = t.audienceUploaded;
                r.brandId = t.brandId;
                r.created = t.created;
                r.deleted = t.deleted;
                r.modified = t.modified;
                r.name = t.name;
                if (audienceDTOs[0] && audienceDTOs[0].npi) {
                    r.npiSize = audienceDTOs[0].npi.length;
                } else {
                    r.npiSize = 0;
                }
                results.push(r);
            }
            res.json(results);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAudienceTargetDetails = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const targetDTO: AdvertiserAudienceTargetDTO = await this.advertiserAudienceTargetDAO.findById(req.params.id);
            const authorizePayload: any = { brandId: targetDTO.brandId.toString() };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);
            res.json(targetDTO);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPotentialReachForCreate = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            // This function will be used when the user is playing around with create
            // targeting criteria.
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            if (!req.body.brandId) {
                throw new HandledApplicationError(500, 'Mandatory field brandId is missing');
            }
            const authorizePayload: any = { brandId: req.body.brandId };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);

            let totalAvailableReach: number = 0;
            let totalTargetedReach: number = 0;
            const zone = req.header('zone');
            if (zone === '1') {
                totalAvailableReach = await this.advertiserAudienceTargetDAO.getAvailableReachForUs();
                totalTargetedReach = await this.advertiserAudienceTargetDAO.getPotentialReachCountForCreateForUS(
                    req.body.createAudienceTargetDetails);
            } else if (zone === '2') {
                totalAvailableReach = await this.advertiserAudienceTargetDAO.getAvailableReachForIndia();
                totalTargetedReach = await this.advertiserAudienceTargetDAO.getPotentialReachCountForCreateForIndia(
                    req.body.createAudienceTargetDetails);
            }
            res.json({ totalHcps: totalAvailableReach, potentialReach: totalTargetedReach });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPotentialReach = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            // this function should be called in the view mode
            const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
            if (!advertiserUser) {
                throw new HandledApplicationError(500, `Invalid advertiser email [${req.email}]`);
            }
            const targetDTO: AdvertiserAudienceTargetDTO = await this.advertiserAudienceTargetDAO.findById(req.params.id);
            const authorizePayload: any = { brandId: targetDTO.brandId.toString() };
            await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);

            const audienceDTOs: AdvertiserAudienceDTO[] = await this.advertiserAudienceDAO.findByTargetId(targetDTO._id);
            if (audienceDTOs && audienceDTOs.length > 1) {
                throw new HandledApplicationError(500, 'Did not expect multiple audience corresponding to a single target');
            }
            let totalAvailableReach = 0;
            if (req.header('zone') === '1') {
                totalAvailableReach = await this.advertiserAudienceTargetDAO.getAvailableReachForUs();
            } else if (req.header('zone') === '2') {
                totalAvailableReach = await this.advertiserAudienceTargetDAO.getAvailableReachForIndia();
            }
            let totalTargetedReach = 0;
            if (!audienceDTOs || audienceDTOs.length === 0) {
                totalTargetedReach = 0;
            } else {
                totalTargetedReach = audienceDTOs[0].npi.length;
            }
            res.json({ totalHcps: totalAvailableReach, potentialReach: totalTargetedReach });
        } catch (err) {
            catchError(err, next);
        }
    }

    public refreshAudience = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
    ) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            // find only those audience which are associated with active subcampaign
            const audienceTargetIds: any[] = await this.subcampaignDAO.getActiveAudienceTargetIds();
            for (const advertiserTargetId of audienceTargetIds) {
                const advertiserTargetDTO = await this.advertiserAudienceTargetDAO.findById(advertiserTargetId);
                let npis: number[] = [];
                if (advertiserTargetDTO) {
                    if (advertiserTargetDTO.audienceUploaded) {
                        npis = await this.getNpiListForUpload(advertiserTargetDTO.uploadAudienceTargetDetails);
                    } else {
                        npis = await this.getNpiListForCreate(advertiserTargetDTO.createAudienceTargetDetails);
                    }
                    const newAudienceDTO = new AdvertiserAudienceDTO();
                    newAudienceDTO.npi = npis;
                    newAudienceDTO.modified = { at: new Date(), by: req.email };
                    const existingAudienceDTOs: AdvertiserAudienceDTO[] =
                        await this.advertiserAudienceDAO.findByTargetId(advertiserTargetDTO._id.toString());
                    for (const e of existingAudienceDTOs) {
                        await this.advertiserAudienceDAO.update(e._id, newAudienceDTO);
                    }
                }
            }
            await this.redisHelper.loadSubcampaignsForNpi();
            res.json({ message: 'update audience invoked' });
        } catch (err) {
            catchError(err, next);
        }
    }

    private async getNpiListForCreate(createAudienceTargetDetails: any): Promise<number[]> {
        return await this.advertiserAudienceTargetDAO.getPotentialReachForCreate(createAudienceTargetDetails);
    }

    private async getNpiListForUpload(uploadAudienceTargetDetails: any): Promise<number[]> {
        let url = uploadAudienceTargetDetails.curatedFile;
        if (!uploadAudienceTargetDetails.curatedFile) {
            // logger.debug('Curated file not present, going with the original file');
            url = uploadAudienceTargetDetails.actualFile;
        }
        const opts = {
            uri: url,
            method: 'GET'
        };
        const request = require('request');
        const res = await request(opts);

        const neatCsv = require('neat-csv');
        const output = await neatCsv(res);
        if (output && output.length === 0) {
            throw new HandledApplicationError(500, 'No data available in CSV format to read');
        }
        const inputNpis: number[] = [];
        for (const x of output) {
            for (const k of Object.keys(x)) {
                // TODO: I have no idea why the space is coming in the csv parsing
                if (k.trim() === 'NPI') {
                    try {
                        if (!isNaN(x[k])) {
                            const n: number = parseInt(x[k], 10);
                            inputNpis.push(n);
                        }
                    } catch (err) {
                        // dont do anything, reject that entry
                    }
                }
            }
        }
        return await this.advertiserAudienceTargetDAO.getPotentialReachForUpload(inputNpis);
    }
}
