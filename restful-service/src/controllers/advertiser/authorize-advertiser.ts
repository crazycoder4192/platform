import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import CampaignDAO from '../../daos/advertiser/campaign.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import HandledApplicationError from '../../error/handled-application-error';
import { logger } from '../../util/winston';

export class AuthorizeAdvertiser {
    private readonly subcampaignDAO: SubCampaignDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly campaignDAO: CampaignDAO;

    constructor() {
        this.subcampaignDAO = new SubCampaignDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.campaignDAO = new CampaignDAO();
    }

    public async authorize(email: string, authorizePayload: any, ...permission: string[]) {
        // check if this email belongs to advertiser
        let advertiserUserDTOs: AdvertiserUserDTO[] = await this.advertiserUserDAO.findUserBrandsByEmail(email);

        if (advertiserUserDTOs.length === 0) {
            throw new HandledApplicationError(500, `No advertiser with the email [${email}] exists`);
        }
        if (advertiserUserDTOs.length > 1) {
            throw new HandledApplicationError(500, `Did not expect to find multiple advertiser users for email [${email}]`);
        }
        const assignedBrandIds: string[] = advertiserUserDTOs[0].brands.map((x: any) => x.brandId.toString());
        if (assignedBrandIds.length === 0) {
            throw new HandledApplicationError(500, 'No brand is associated with this user');
        }

        // check if the advertiser user is associated with this brand
        let brandId: string = '';
        if (authorizePayload.brandId) {
            brandId = authorizePayload.brandId;
        } else if (authorizePayload.campaignId) {
            const result: any = await this.campaignDAO.findBrandIdByCampaignId(authorizePayload.campaignId);
            if (result) {
                brandId = result.brandId;
            } else {
                logger.error(`Unable to find brand corresponding to this campaignId [${authorizePayload.campaignId}]`);
                throw new HandledApplicationError(500, 'Unable to find brand corresponding to this campaign');
            }
        } else if (authorizePayload.subcampaignId) {
            const result: any = await this.subcampaignDAO.findBrandIdBySubcampaignId(authorizePayload.subcampaignId);
            if (result) {
                brandId = result.brandId;
            } else {
                logger.error(`Unable to find brand corresponding to this subcampaign [${authorizePayload.subcampaignId}]`);
                throw new HandledApplicationError(500, 'Unable to find brand corresponding to this subcampaign');
            }
        }

        if (brandId) {
            if (!assignedBrandIds.includes(brandId.toString())) {
                logger.error(`User [${email}] do not have access to this brand [${brandId}]`);
                throw new HandledApplicationError(500, `User [${email}] do not have access to this brand`);
            }

            // check if the user has appropriate permission
            advertiserUserDTOs = await this.advertiserUserDAO.checkBrandPermission(email, brandId, permission);
            if (!advertiserUserDTOs || advertiserUserDTOs.length === 0) {
                logger.error(`User [${email}] does not have permission to access this brand [${brandId}]`);
                throw new HandledApplicationError(500, `User [${email}] does not have permission to access this brand`);
            } else if (advertiserUserDTOs.length > 2) {
                logger.error(`User [${email}] has more than one advertiser user associated`);
            }
        } else {
            if (authorizePayload.advertiserId) {
                // the flow here is a bit twisted. Here we are chekcing if this  permission
                // is assigned to any brand, then we are allowing the user to have right on it.
                // though not completely right, we should use this very rarely.
                advertiserUserDTOs = await this.advertiserUserDAO.checkAdvertiserPermission(email, authorizePayload.advertiserId, permission);
                if (!advertiserUserDTOs || advertiserUserDTOs.length === 0) {
                    logger.error(`User [${email}] does not have permission to access this advertiser [${authorizePayload.advertiserId}]`);
                    throw new HandledApplicationError(500, `User [${email}] does not have permission to access this advertiser`);
                } else if (advertiserUserDTOs.length > 2) {
                    logger.error(`User [${email}] has more than one advertiser user associated`);
                }
            }
        }
    }
}
