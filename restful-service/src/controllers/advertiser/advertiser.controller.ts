import express from 'express';
import IController from '../../common/controller-interface';
import authenticationGuard from '../../guards/authentication.guard';
import { AdvertiserProfileController } from '../advertiser/profile.controller';
import { NotificationController } from '../notification.controller';
import { AdvertiserAnalyticsController } from './advertiser-analytics.controller';
import { AdvertiserAudienceTargetController } from './advertiser-audience-target.controller';
import { AdvertiserBrandController } from './advertiser-brand.controller';
import { AdvertiserMonthlyInvoiceController } from './advertiser-monthly-invoice.controller';
import { AdvertiserUserController } from './advertiser-user.controller';
import { CampaignController } from './campaign.controller';
import { ClientBrandMetadataController } from './client-brand-metadata.controller';
import { CreativeHubController } from './creative-hub.controller';
import { DashBoardController } from './dashboard.controller';
import { InvoiceGenerationController } from './invoice-generation.controller';
import { PaymentMethodController } from './payment-method.controller';
import { PaypalGatewayController } from './paypal-gateway.controller';
import { SubCampaignController } from './sub-campaign.controller';

export class AdvertiserController implements IController {
  public path = '/provider';
  public router = express.Router();
  private readonly advertiserProfileController: AdvertiserProfileController;
  private readonly advertiserBrandController: AdvertiserBrandController;
  private readonly advertiserUserController: AdvertiserUserController;
  private readonly campaignController: CampaignController;
  private readonly subCampaignController: SubCampaignController;
  private readonly paymentMethodController: PaymentMethodController;
  private readonly paypalGatewayController: PaypalGatewayController;
  private readonly invoiceGenerationController: InvoiceGenerationController;
  private readonly creativeHubController: CreativeHubController;
  private readonly dashBoardController: DashBoardController;
  private readonly advertiserAnalyticsController: AdvertiserAnalyticsController;
  private readonly clientBrandMetadataController: ClientBrandMetadataController;
  private readonly advertiserMonthlyInvoiceController: AdvertiserMonthlyInvoiceController;

  private readonly advertiserAudienceTargetController: AdvertiserAudienceTargetController;

  constructor() {
    this.advertiserProfileController = new AdvertiserProfileController();
    this.advertiserBrandController = new AdvertiserBrandController();
    this.advertiserUserController = new AdvertiserUserController();
    this.campaignController = new CampaignController();
    this.subCampaignController = new SubCampaignController();
    this.paymentMethodController = new PaymentMethodController();
    this.paypalGatewayController = new PaypalGatewayController();
    this.invoiceGenerationController = new InvoiceGenerationController();
    this.creativeHubController = new CreativeHubController();
    this.dashBoardController = new DashBoardController();
    this.advertiserAnalyticsController = new AdvertiserAnalyticsController();
    this.clientBrandMetadataController = new ClientBrandMetadataController();
    this.advertiserMonthlyInvoiceController = new AdvertiserMonthlyInvoiceController();

    this.advertiserAudienceTargetController = new AdvertiserAudienceTargetController();
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}/providerTypes`,
      authenticationGuard,
      this.advertiserProfileController.getProviderTypes
    );
    this.router.get(
      `${this.path}/unique/providerTypes`,
      authenticationGuard,
      this.advertiserProfileController.getUniqueProviderTypes,
    );
    this.router.get(
      `${this.path}/unique/providerAgencyTypes`,
      authenticationGuard,
      this.advertiserProfileController.getUniqueProviderAgencyTypes,
    );
    this.router.post(
      `${this.path}/profile`,
      authenticationGuard,
      this.advertiserProfileController.createProfile,
    );
    this.router.put(
      `${this.path}/profile`,
      authenticationGuard,
      this.advertiserProfileController.updateProfile,
    );
    this.router.get(
      `${this.path}/profile/email`,
      authenticationGuard,
      this.advertiserProfileController.getProfileByEmail,
    );
    this.router.get(
      `${this.path}/profile/reminder/incompleteprofile`,
      authenticationGuard,
      this.advertiserProfileController.incompleteProfiles
    );
    this.router.get(
      `${this.path}/brand`,
      authenticationGuard,
      this.advertiserBrandController.getBrandPerZone,
    );
    this.router.get(
      `${this.path}/allzonebrand`,
      authenticationGuard,
      this.advertiserBrandController.getBrand,
    );
    this.router.get(
      `${this.path}/brandforCards`,
      authenticationGuard,
      this.advertiserBrandController.brandforPaymentCards,
    );
    this.router.get(
      `${this.path}/clientbrands`,
      authenticationGuard,
      this.advertiserBrandController.getClientBrands,
    );

    this.router.get(
      `${this.path}/brandDetailsForLoggedInUser`,
      authenticationGuard,
      this.advertiserBrandController.getBrandsDataByLoggedInUser,
    );
    this.router.get(
      `${this.path}/userDetailsVsBrandsByLoggedInUser`,
      authenticationGuard,
      this.advertiserBrandController.getUserDetailsVsBrandsByLoggedInUser,
    );

    this.router.post(
      `${this.path}/brand`,
      authenticationGuard,
      this.advertiserBrandController.createBrand,
    );
    this.router.put(
      `${this.path}/brand`,
      authenticationGuard,
      this.advertiserBrandController.updateBrand,
    );
    this.router.delete(
      `${this.path}/brand/:id`,
      authenticationGuard,
      this.advertiserBrandController.deactivateBrand,
    );
    this.router.get(
      `${this.path}/brand/:id`,
      authenticationGuard,
      this.advertiserBrandController.getBrandById,
    );
    this.router.post(
      `${this.path}/brand/brandNameAvailability`,
      authenticationGuard,
      this.advertiserBrandController.brandNameAvailability,
    );
    this.router.get(
      `${this.path}/campaigns`,
      authenticationGuard,
      this.campaignController.getAllCampaigns
    );
    this.router.post(
      `${this.path}/campaigns`,
      authenticationGuard,
      this.campaignController.createCampaign
    );
    this.router.put(
      `${this.path}/campaigns/:id`,
      authenticationGuard,
      this.campaignController.updateCampaign,
    );
    this.router.delete(
      `${this.path}/campaigns/:id/:type`,
      authenticationGuard,
      this.campaignController.deleteCampaign
    );

    this.router.post(
      `${this.path}/campaigns/campaignNameAvailability`,
      authenticationGuard,
      this.campaignController.campaignNameAvailability
    );

    this.router.get(
      `${this.path}/campaigns/by/brand/:brandId`,
      authenticationGuard,
      this.campaignController.getCampaignsByBrandId
    );
    this.router.get(
      `${this.path}/subcampaigns/brand/active/:brandId`,
      authenticationGuard,
      this.subCampaignController.getAllActiveSubCampaignsByBrandId
    );

    this.router.get(
      `${this.path}/subcampaigns/brand/:brandId`,
      authenticationGuard,
      this.subCampaignController.getAllSubCampaignsByBrandId
    );

    this.router.put(
      `${this.path}/subcampaigns/potentialreach/bannersize/bidrange`,
      authenticationGuard,
      this.subCampaignController.getPotentialReachBannerSizeBidRange
    );

    this.router.get(
      `${this.path}/subcampaign/:id`,
      authenticationGuard,
      this.subCampaignController.getOneSubCampaign
    );
    this.router.post(
      `${this.path}/subcampaign/subCampaignNameAvailability`,
      authenticationGuard,
      this.subCampaignController.subCampaignNameAvailability
    );
    this.router.get(
      `${this.path}/subcampaign/:id`,
      authenticationGuard,
      this.subCampaignController.getOneSubCampaign
    );
    this.router.get(
      `${this.path}/subcampaign/reminder/nocampaigns`,
      authenticationGuard,
      this.subCampaignController.noCampaigns
    );
    this.router.get(
      `${this.path}/subcampaign/provider/subcampaign/creative/urls/:id`,
      authenticationGuard,
      this.subCampaignController.getCreativeUrls
    );

    this.router.get(
      `${this.path}/drafts`,
      authenticationGuard,
      this.subCampaignController.getAllDrafts
    );

    this.router.post(
      `${this.path}/subcampaigns`,
      authenticationGuard,
      this.subCampaignController.createSubCampaign
    );
    this.router.put(
      `${this.path}/subcampaigns`,
      authenticationGuard,
      this.subCampaignController.updateSubCampaign
    );
    this.router.delete(
      `${this.path}/subcampaigns/:id`,
      authenticationGuard,
      this.subCampaignController.deleteSubCampaign
    );
    this.router.get(
      `${this.path}/subcampaigns/deactivate`,
      authenticationGuard,
      this.subCampaignController.deactivateSubcampaign
    );
    this.router.post(
      `${this.path}/paymentmethod`,
      authenticationGuard,
      this.paymentMethodController.addPaymentMethod
    );
    this.router.get(
      `${this.path}/paymentmethod`,
      authenticationGuard,
      this.paymentMethodController.getAllPaymentMethods
    );
    this.router.get(
      `${this.path}/paymentmethod/:id`,
      authenticationGuard,
      this.paymentMethodController.getAllPaymentMethods
    );
    this.router.put(
      `${this.path}/paymentmethod/:id`,
      authenticationGuard,
      this.paymentMethodController.updatePaymentMethod
    );
    this.router.delete(
      `${this.path}/paymentmethod/:id`,
      authenticationGuard,
      this.paymentMethodController.deletePaymentMethod
    );

    this.router.get(`${this.path}/generateInvoices/`, authenticationGuard, this.invoiceGenerationController.generateInvoices);
    this.router.get(`${this.path}/getOutstandingAmount/`, authenticationGuard, this.invoiceGenerationController.getOutstandingAmount);
    this.router.get(`${this.path}/getOutstandingAmount/:brandId`, authenticationGuard, this.invoiceGenerationController.getOutStandingBrandAmount);
    this.router.get(`${this.path}/invoice/`, authenticationGuard, this.invoiceGenerationController.getInvoices);

    // paypal checkout integration
    this.router.post(`${this.path}/payNow`, authenticationGuard, this.paypalGatewayController.payNow);
    this.router.get(`${this.path}/paymentSuccess/:id/:paymentId/:PayerID`, this.paypalGatewayController.paymentSuccess);
    this.router.get(`${this.path}/paymentFailure/:id`, this.paypalGatewayController.paymentFailure);
    this.router.get(`${this.path}/downloadInvoice/:id`, authenticationGuard, this.invoiceGenerationController.downloadInvoiceById);
    this.router.get(`${this.path}/emailInvoice/:id`, authenticationGuard, this.invoiceGenerationController.emailInvoiceById);

    this.router.get(`${this.path}/pendingsubcampaign`, authenticationGuard, this.subCampaignController.findPublishedCampaigns);
    this.router.put(`${this.path}/changestatusSubCampaign`,
      authenticationGuard, this.subCampaignController.changeStatusSubCampaign);
    this.router.get(`${this.path}/manageCampaigns`, authenticationGuard, this.subCampaignController.findCampaignWithSubCampaigns);
    this.router.get(`${this.path}/providerList`, authenticationGuard, this.subCampaignController.providerList);

    this.router.get(`${this.path}/creatives`, authenticationGuard, this.creativeHubController.getAllCreatives);
    this.router.post(`${this.path}/creatives`, authenticationGuard, this.creativeHubController.addCreative);
    this.router.put(`${this.path}/creatives/:id`, authenticationGuard, this.creativeHubController.updateCreative);
    this.router.get(`${this.path}/creatives/:id`, authenticationGuard, this.creativeHubController.getCreative);
    this.router.delete(`${this.path}/creatives/:id`, authenticationGuard, this.creativeHubController.deleteCreative);
    this.router.get(`${this.path}/creatives/download/:name`, authenticationGuard, this.creativeHubController.downloadAllFilesInASection);
    this.router.get(`${this.path}/creatives/urls/:id`, authenticationGuard, this.creativeHubController.getCreativeUrls);
    this.router.put(`${this.path}/deletemanycreatives`, authenticationGuard, this.creativeHubController.deleteManyCreative);
    this.router.get(`${this.path}/creativesbybannersize`, authenticationGuard, this.creativeHubController.getAllCreativesByBannerSize);
    this.router.get(`${this.path}/creativesvideosbyuser`, authenticationGuard, this.creativeHubController.getAllVideoCreatives);
    this.router.put(`${this.path}/addtosection`, authenticationGuard, this.creativeHubController.addCreativeToSection);
    // Advertiser user
    this.router.post(`${this.path}/advertiseruser`, authenticationGuard, this.advertiserUserController.createAdvertiserUser);
    this.router.get(`${this.path}/advertiseruser`, authenticationGuard, this.advertiserUserController.getAllAdvertiserUsers);
    this.router.get(`${this.path}/advertiseruser/:email`, authenticationGuard, this.advertiserUserController.getAdvertiserUser);
    this.router.get(`${this.path}/advertiseruserdetails/:email`, authenticationGuard, this.advertiserUserController.getAdvertiserDetails);
    this.router.get(`${this.path}/advertiseruserpermissions/:email`, authenticationGuard, this.advertiserUserController.getAdvertiserUserPermissions);
    this.router.delete(`${this.path}/advertiseruser/:email`, authenticationGuard, this.advertiserUserController.deleteAdvertiserUsers);
    this.router.put(`${this.path}/advertiseruserbrand`, authenticationGuard, this.advertiserUserController.deleteBrandFromAdvertiserUsers);
    this.router.put(`${this.path}/advertiseruserbrandrole`, authenticationGuard, this.advertiserUserController.updateBrandRoleOfAdvertiserUsers);
    this.router.post(`${this.path}/regeneratetoken`, authenticationGuard, this.advertiserUserController.reGenerateToken);
    this.router.put(`${this.path}/advertiseruserswitchbrand`, authenticationGuard, this.advertiserUserController.switchToBrandOfLoggedinUser);
    this.router.get(`${this.path}/advertiseruserbrands`, authenticationGuard, this.advertiserUserController.getAdvertiserUserBrands);
    // campaign
    this.router.get(`${this.path}/campaign/useremail/:email`, authenticationGuard, this.campaignController.getCampaignsByCreatedByUserEmail);
    // get active sub campaigns by campaign id
    this.router.get(`${this.path}/subcampaignsbycampaignid/:campaignId`,
      authenticationGuard,
      this.subCampaignController.getAllSubCampaignsByCampaignId
    );

    // Advertiser dashboard
    this.router.get(`${this.path}/activeCampaignLifeTimeData/:id?`, authenticationGuard,
      this.dashBoardController.getActiveCampaignLifeTimeData);
    this.router.get(`${this.path}/clientWiseBrandsLifeTimeData`, authenticationGuard,
      this.dashBoardController.getClientWiseBrandsLifeTimeData);
    this.router.get(`${this.path}/dashboardLifeTimeData`, authenticationGuard,
      this.dashBoardController.getLifeTimeDataForDashboard);
    this.router.get(`${this.path}/lastCheckInData/:id?`, authenticationGuard,
      this.dashBoardController.getBrandDataForCards);
    this.router.post(`${this.path}/analytics`, authenticationGuard,
      this.advertiserAnalyticsController.getAnalyticsData);
    this.router.post(`${this.path}/analyticsCardsData`, authenticationGuard,
      this.advertiserAnalyticsController.getAnalyticsCardsData);
    this.router.post(`${this.path}/dashboardperformanceanalytics`, authenticationGuard,
      this.dashBoardController.getPerformanceAnalyticsData,
    );

    this.router.get(`${this.path}/clientbrand/metadata/allclients`, authenticationGuard,
      this.clientBrandMetadataController.getAllClients);
    this.router.get(`${this.path}/clientbrand/metadata/allbrands/:clientId`, authenticationGuard,
    this.clientBrandMetadataController.getAllBrands);

    this.router.post(`${this.path}/creditline`, authenticationGuard,
      this.advertiserMonthlyInvoiceController.addMonthlyInvoice,
    );
    this.router.get(`${this.path}/creditline`, authenticationGuard,
      this.advertiserMonthlyInvoiceController.getMonthlyInvoice,
    );
    this.router.put(`${this.path}/creditline`, authenticationGuard,
      this.advertiserMonthlyInvoiceController.updateMonthlyInvoice,
    );
    this.router.get(`${this.path}/creditlines/:status`, authenticationGuard,
      this.advertiserMonthlyInvoiceController.getMonthlyInvoiceByStatus,
    );
    this.router.put(`${this.path}/approveCreditline`, authenticationGuard,
      this.advertiserMonthlyInvoiceController.approveMonthlyInvoice,
    );
    this.router.put(`${this.path}/disproveCreditLine`, authenticationGuard,
      this.advertiserMonthlyInvoiceController.disproveMonthlyInvoice,
    );

    this.router.post(`${this.path}/audience/target`, authenticationGuard,
      this.advertiserAudienceTargetController.createAudienceTarget,
    );
    this.router.put(`${this.path}/audience/target`, authenticationGuard,
      this.advertiserAudienceTargetController.updateAudienceTarget,
    );
    this.router.delete(`${this.path}/audience/target/:id`, authenticationGuard,
      this.advertiserAudienceTargetController.deleteAudienceTarget,
    );
    this.router.get(`${this.path}/audience/target/list/:brandId`, authenticationGuard,
      this.advertiserAudienceTargetController.listAudienceTarget,
    );
    this.router.get(`${this.path}/audience/target/detail/:id`, authenticationGuard,
      this.advertiserAudienceTargetController.getAudienceTargetDetails,
    );
    this.router.get(`${this.path}/audience/target/potentialreach/:id`, authenticationGuard,
      this.advertiserAudienceTargetController.getPotentialReach,
    );
    this.router.put(`${this.path}/audience/target/potentialreach/forcreate`, authenticationGuard,
      this.advertiserAudienceTargetController.getPotentialReachForCreate,
    );
    this.router.put(`${this.path}/audience/target/refresh`, authenticationGuard,
      this.advertiserAudienceTargetController.refreshAudience,
    );
  }
}
