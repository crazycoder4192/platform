import config from 'config';
import express from 'express';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import MonthlyInvoiceDAO from '../../daos/advertiser/monthly-invoice.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import AdvertiserDTO from '../../dtos/advertiser/advertiser.dto';
import MonthlyInvoiceDTO from '../../dtos/advertiser/monthly-invoice.dto';
import UserDTO from '../../dtos/user.dto';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { RedisHelper } from '../../redis/redis-helper';
import { AdvertiserPermissions, CreditLineStatus, UserTypes } from '../../util/constants';
import { AdvertiserMail } from '../../util/mail/advertiser-mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { logger } from '../../util/winston';
import { AuthorizeAdvertiser } from './authorize-advertiser';
const dateformat = require('dateformat');

export class AdvertiserMonthlyInvoiceController {

    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly userDAO: UserDAO;
    private readonly advertiserMail: AdvertiserMail;
    private readonly monthlyInvoiceDAO: MonthlyInvoiceDAO;
    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly generalMail: GeneralMail;
    private readonly redisHelper: RedisHelper;

    constructor() {
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.userDAO = new UserDAO();
        this.monthlyInvoiceDAO = new MonthlyInvoiceDAO();
        this.advertiserMail = new AdvertiserMail();
        this.advertiserDAO = new AdvertiserDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.userTokenDAO = new UserTokenDAO();
        this.generalMail = new GeneralMail();
        this.redisHelper = new RedisHelper();
    }

    public addMonthlyInvoice = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        // what all we need - advertiserId, asking credit line limit, uploading the documents
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
        await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
            AdvertiserPermissions.addCreditLine);

        const payload: MonthlyInvoiceDTO = new MonthlyInvoiceDTO();
        payload.advertiserId = advertiserUser.advertiserId;
        payload.documents = req.body.documents;
        payload.rejectionReason = req.body.rejectionReason ? req.body.rejectionReason : '';
        payload.deleted = false;
        payload.rejectionReason = '';
        payload.status = CreditLineStatus.under_review;
        payload.comment = req.body.comment;
        // payload.askingCreditLimit = req.body.askingCreditLimit;
        // payload.startDate = req.body.startDate;
        // payload.endDate = req.body.endDate;
        payload.deleted = false;
        payload.created = { at: new Date(), by: req.email };
        payload.hasOrganizationRegistered = req.body.hasOrganizationRegistered;
        payload.name = req.body.name;
        payload.email = req.body.email;
        payload.companyName = req.body.companyName;
        payload.companyAddress = req.body.companyAddress;
        payload.streetAddress = req.body.streetAddress;
        payload.country = req.body.country;
        payload.state = req.body.state;
        payload.city = req.body.city;
        payload.zipCode = req.body.zipCode;
        payload.companyContactName = req.body.companyContactName;
        payload.taxId = req.body.taxId;
        payload.billingContactName = req.body.billingContactName;
        payload.billingEmail = req.body.billingEmail;
        payload.phoneNumber = req.body.phoneNumber;
        payload.dunsNumber = req.body.dunsNumber;
        payload.expectedIncome = req.body.expectedIncome;
        payload.startDate = req.body.startDate;
        const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        if (toUser.hasPersonalProfile) {
            if (toUser.isSubscribedForEmail) {
                await this.advertiserMail.sendApplicationForCreditLineMail(
                    req.email,
                    toUser.firstName,
                    `${url}/login`,
                    `${url}/login`,
                    `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
                );
            }
        }
        const result = await this.monthlyInvoiceDAO.create(payload, req.header('zone'));
        res.json(result);
    }

    public approveMonthlyInvoice = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
        if (userDTO && userDTO.userType.toLowerCase() === UserTypes.admin.toLowerCase()) {
            const dto: MonthlyInvoiceDTO = await this.monthlyInvoiceDAO.findById(req.body._id, req.header('zone'));
            dto.status = CreditLineStatus.approved;
            dto.rejectionReason = '';
            dto.approvedCreditLimit = req.body.approvedCreditLimit;
            dto.modified = { at: new Date(), by: req.email };
            await this.monthlyInvoiceDAO.update(dto._id, dto, req.header('zone'));

            const adto: AdvertiserDTO = await this.advertiserDAO.findById(dto.advertiserId);
            const url: string = config.get<string>('mail.web-portal-redirection-url');
            await this.redisHelper.updateMonthlyInvoice(adto, dto, req.header('zone'));
            const toUser: UserDTO = await this.userDAO.findByEmail(adto.created.by);
            if (toUser.hasPersonalProfile) {
                if (toUser.isSubscribedForEmail) {
                    const creditLineDate = dateformat(dto.startDate, 'mmmm dS, yyyy');
                    this.advertiserMail.sendConfirmationOfCreditLineStatusApprovedMail(
                        adto.created.by,
                        toUser.firstName,
                        creditLineDate,
                        `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(adto.created.by))}`
                    );
                }
            }
            res.json({ success: true, message: 'Approved successfully.' });
        } else {
            throw new HandledApplicationError(500, 'You do not have permission to perform this action');
        }
    }

    public disproveMonthlyInvoice = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);

        if (userDTO && userDTO.userType.toLowerCase() === UserTypes.admin.toLowerCase()) {
            const dto: MonthlyInvoiceDTO = await this.monthlyInvoiceDAO.findById(req.body._id, req.header('zone'));
            dto.status = CreditLineStatus.rejected;
            dto.rejectionReason = req.body.rejectionReason;
            dto.modified = { at: new Date(), by: req.email };
            await this.monthlyInvoiceDAO.update(dto._id, dto, req.header('zone'));

            const adto: AdvertiserDTO = await this.advertiserDAO.findById(dto.advertiserId);
            // TODO: get it from the config file
            adto.spendLimit = 1000;
            await this.advertiserDAO.update(adto._id, adto);
            const toUser: UserDTO = await this.userDAO.findByEmail(adto.created.by);
            if (toUser.hasPersonalProfile) {
                if (toUser.isSubscribedForEmail) {
                    const creditLineDate = dateformat(dto.startDate, 'mmmm dS, yyyy');
                    this.advertiserMail.sendConfirmationOfCreditLineStatusRejectedMail(
                        adto.created.by,
                        toUser.firstName,
                        creditLineDate,
                        `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(adto.created.by))}`
                    );
                }
            }
            res.json({ success: true, message: 'Disapproved successfully.' });
        } else {
            throw new HandledApplicationError(500, 'You do not have permission to perform this action');
        }
    }

    public getMonthlyInvoice = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
        await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
            AdvertiserPermissions.addCreditLine);
        const result = await this.monthlyInvoiceDAO.findByAdvertiserId(advertiserUser.advertiserId, req.header('zone'));
        res.json(result);

    }

    public updateMonthlyInvoice = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const authorizePayload: any = { advertiserId: advertiserUser.advertiserId };
        await this.authorizeAdvertiser.authorize(req.email, authorizePayload,
            AdvertiserPermissions.addCreditLine);
        const dto: MonthlyInvoiceDTO = await this.monthlyInvoiceDAO.findById(req.body._id, req.header('zone'));
        dto.documents = req.body.documents;
        dto.status = CreditLineStatus.under_review;
        dto.comment = req.body.comment;
        dto.hasOrganizationRegistered = req.body.hasOrganizationRegistered;
        dto.name = req.body.name;
        dto.email = req.body.email;
        dto.companyName = req.body.companyName;
        dto.companyAddress = req.body.companyAddress;
        dto.streetAddress = req.body.streetAddress;
        dto.country = req.body.country;
        dto.state = req.body.state;
        dto.city = req.body.city;
        dto.zipCode = req.body.zipCode;
        dto.companyContactName = req.body.companyContactName;
        dto.taxId = req.body.taxId;
        dto.billingContactName = req.body.billingContactName;
        dto.billingEmail = req.body.billingEmail;
        dto.phoneNumber = req.body.phoneNumber;
        dto.dunsNumber = req.body.dunsNumber;
        dto.expectedIncome = req.body.expectedIncome;
        dto.startDate = req.body.startDate;
        dto.rejectionReason = req.body.rejectionReason;
        dto.modified = { at: new Date(), by: req.email };
        await this.monthlyInvoiceDAO.update(dto._id, dto, req.header('zone'));
        res.json(dto);
    }

    public getMonthlyInvoiceByStatus = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        const creditLineDetails: MonthlyInvoiceDTO[] = await this.monthlyInvoiceDAO.findByStatus(req.params.status, req.header('zone'));
        let result: any[];
        result = [];
        for (const item of creditLineDetails) {
            const adto: AdvertiserDTO = await this.advertiserDAO.findById(item.advertiserId);
            const dto: MonthlyInvoiceDTO = new MonthlyInvoiceDTO();
            dto._id = item._id;
            dto.rejectionReason = item.rejectionReason;
            dto.status = item.status;
            dto.comment = item.comment;
            dto.approvedCreditLimit = item.approvedCreditLimit ? item.approvedCreditLimit : null;
            dto.documents = item.documents;
            dto.deleted = item.deleted;
            dto.created = item.created;
            dto.hasOrganizationRegistered = item.hasOrganizationRegistered;
            dto.name = item.name;
            dto.email = item.email;
            dto.companyName = item.companyName;
            dto.companyAddress = item.companyAddress;
            dto.streetAddress = item.streetAddress;
            dto.country = item.country;
            dto.state = item.state;
            dto.city = item.city;
            dto.zipCode = item.zipCode;
            dto.companyContactName = item.companyContactName;
            dto.taxId = item.taxId;
            dto.billingContactName = item.billingContactName;
            dto.billingEmail = item.billingEmail;
            dto.phoneNumber = item.phoneNumber;
            dto.dunsNumber = item.dunsNumber;
            dto.expectedIncome = item.expectedIncome;
            dto.startDate = item.startDate;
            dto.advertiserDetails = adto;
            result.push(dto);
        }
        res.json(result);
    }
}
