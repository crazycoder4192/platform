import express from 'express';
import RoleDAO from '../../daos/admin/role.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from '../admin/authorize-admin';

// This file will be used to migrate the existing publishers to new publisher user setup
export class NewPublisherFieldTrustedSiteController {

    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly publisherDAO: PublisherDAO;
    private readonly platformDAO: PublisherPlatformDAO;

    constructor() {
        this.authorizeAdmin = new AuthorizeAdmin();
        this.publisherDAO = new PublisherDAO();
        this.platformDAO = new PublisherPlatformDAO();
    }

    public migrate = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // should be accessed only by admins
            await this.authorizeAdmin.authorize(req.email);

            // get all the publishers
            const allPublishers: PublisherDTO[] = await this.publisherDAO.findAll();
            const zones: string[] = ['1', '2'];

            for (const z of zones) {
                for (const p of allPublishers) {
                    const allPlatforms: PublisherPlatformDTO[] = await this.platformDAO.findAllPlatformsByPublisherId(p._id.toString(), z);
                    for (const pl of allPlatforms) {
                        if (!pl.trustedSites) {
                            pl.trustedSites = '';
                            await this.platformDAO.update(pl._id, pl, z);
                        }
                    }
                }
            }
            res.json({ message: 'Added new field -trustedSites- for all platforms', success: true });
        } catch (err) {
            catchError(err, next);
        }
    }
}
