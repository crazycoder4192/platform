import express from 'express';
import AdvertiserBrandDAO from '../../daos/advertiser/advertiser-brand.dao';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import AdvertiserDAO from '../../daos/advertiser/advertiser.dao';
import AuditPublishedSubCampaignDAO from '../../daos/advertiser/audit-published-sub-campaign.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import AuditVerifiedAssetDAO from '../../daos/publisher/audit-verified-asset.doa';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import AdvertiserBrandDTO from '../../dtos/advertiser/advertiser-brand.dto';
import AdvertiserUserDTO from '../../dtos/advertiser/advertiser-user.dto';
import Advertiser from '../../dtos/advertiser/advertiser.dto';
import AuditPublishedSubCampaignDTO from '../../dtos/advertiser/audit-published-sub-campaign.dto';
import SubCampaignDTO from '../../dtos/advertiser/sub-campaign.dto';
import PublisherAssetDTO from '../../dtos/publisher/asset.dto';
import AuditVerifiedAssetDTO from '../../dtos/publisher/audit-verified-asset.dto';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from '../admin/authorize-admin';

// This file will be used to migrate the existing publishers to new publisher user setup
export class NewZoneController {

    private readonly authorizeAdmin: AuthorizeAdmin;

    private readonly advertiserDAO: AdvertiserDAO;
    private readonly advertiserBrandDAO: AdvertiserBrandDAO;
    private readonly advertiserSubCampaignDAO: SubCampaignDAO;
    private readonly advertiserAuditPublishedSubCampaignDAO: AuditPublishedSubCampaignDAO;
    private readonly advertiserUserDAO: AdvertiserUserDAO;

    private readonly publisherDAO: PublisherDAO;
    private readonly publisherPlatformDAO: PublisherPlatformDAO;
    private readonly publisherAssetDAO: PublisherAssetDAO;
    private readonly publisherAuditVerifiedAssetDAO: AuditVerifiedAssetDAO;
    private readonly publisherUserDAO: PublisherUserDAO;

    constructor() {
        this.authorizeAdmin = new AuthorizeAdmin();

        this.advertiserDAO = new AdvertiserDAO();
        this.advertiserBrandDAO = new AdvertiserBrandDAO();
        this.advertiserSubCampaignDAO = new SubCampaignDAO();
        this.advertiserAuditPublishedSubCampaignDAO = new AuditPublishedSubCampaignDAO();
        this.advertiserUserDAO = new AdvertiserUserDAO();

        this.publisherDAO = new PublisherDAO();
        this.publisherAuditVerifiedAssetDAO = new AuditVerifiedAssetDAO();
        this.publisherPlatformDAO = new PublisherPlatformDAO();
        this.publisherAssetDAO = new PublisherAssetDAO();
        this.publisherUserDAO = new PublisherUserDAO();
    }

    public migrate = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // should be accessed only by admins
            await this.authorizeAdmin.authorize(req.email);

            await this.updateBrands();
            await this.updatePlatforms();
            res.json({ message: 'Migrated to new Publisher roles', success: true });
        } catch (err) {
            catchError(err, next);
        }
    }

    private async updatePlatforms() {
        const allPublishers: PublisherDTO[] = await this.publisherDAO.findAll();

        for (const pu of allPublishers) {
            const allPlatforms: PublisherPlatformDTO[] = await this.publisherPlatformDAO.findAllMigrateZone(pu._id);
            for (const pl of allPlatforms) {
                const allAssets: PublisherAssetDTO[] = await this.publisherAssetDAO.findAssetsByPlatformId(pl._id);
                for (const a of allAssets) {
                    a.zone = '1';
                    await this.publisherAssetDAO.update(a._id, a);

                    const auditAssets: AuditVerifiedAssetDTO[] = await this.publisherAuditVerifiedAssetDAO.findByAssetId(a._id);
                    for (const aa of auditAssets) {
                        aa.zone = '1';
                        await this.publisherAuditVerifiedAssetDAO.update(aa._id, aa);
                    }
                }
                pl.zone = '1';
                await this.publisherPlatformDAO.updateMigrateZone(pl._id, pl);
            }

            const allPublisherUsers: PublisherUserDTO[] = await this.publisherUserDAO.findByPublisherId(pu._id);
            for (const puu of allPublisherUsers) {
                const platforms: any[] = puu.platforms;
                for (const pl of platforms) {
                    pl.zone = '1';
                }
                await this.publisherUserDAO.update(puu.email, puu);
            }
        }
    }

    private async updateBrands() {
        const allAdvertisers: Advertiser[] = await this.advertiserDAO.findAll();

        for (const ad of allAdvertisers) {
            const allBrands: AdvertiserBrandDTO[] = await this.advertiserBrandDAO.findAllMigrateZone(ad._id);
            for (const ab of allBrands) {
                const allSubcampaigns: SubCampaignDTO[] = await this.advertiserSubCampaignDAO.findByWatchingBrandId(ab._id);
                for (const abs of allSubcampaigns) {
                    abs.zone = '1';
                    await this.advertiserSubCampaignDAO.update(abs._id, abs);

                    const auditSubcampaigns: AuditPublishedSubCampaignDTO[] =
                        await this.advertiserAuditPublishedSubCampaignDAO.findBySubcampaignId(abs._id);
                    for (const aa of auditSubcampaigns) {
                        aa.zone = '1';
                        await this.advertiserAuditPublishedSubCampaignDAO.update(aa._id, aa);
                    }
                }
                ab.zone = '1';
                await this.advertiserBrandDAO.updateMigrateZone(ab._id, ab);
            }

            const allAdvertiserUsers: AdvertiserUserDTO[] = await this.advertiserUserDAO.findByAdvertiserId(ad._id);
            for (const adu of allAdvertiserUsers) {
                const brands: any[] = adu.brands;
                for (const b of brands) {
                    b.zone = '1';
                }
                await this.advertiserUserDAO.update(adu.email, adu);
            }
        }
    }
}
