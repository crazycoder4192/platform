import express from 'express';
import RoleDAO from '../../daos/admin/role.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { AuthorizeAdmin } from '../admin/authorize-admin';

// This file will be used to migrate the existing publishers to new publisher user setup
export class NewPublisherRolesController {

    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly publisherDAO: PublisherDAO;
    private readonly platformDAO: PublisherPlatformDAO;
    private readonly roleDAO: RoleDAO;
    private readonly publisherUserDAO: PublisherUserDAO;

    constructor() {
        this.authorizeAdmin = new AuthorizeAdmin();
        this.publisherDAO = new PublisherDAO();
        this.platformDAO = new PublisherPlatformDAO();
        this.roleDAO = new RoleDAO();
        this.publisherUserDAO = new PublisherUserDAO();
    }

    public migrate = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // should be accessed only by admins
            await this.authorizeAdmin.authorize(req.email);

            // get all the publishers
            const allPublishers: PublisherDTO[] = await this.publisherDAO.findAll();

            const publisherUserAdminRole = await this.roleDAO.getRoleByModuleAndRoleName('Publisher', 'Admin');

            const zones: string[] = ['1', '2'];
            for (const z of zones) {
                // get all the platforms in the publisher
                for (const p of allPublishers) {
                    // one publisher can have multiple platforms & of these multiple platforms, there
                    // will be one platform which is watching
                    const allPlatforms: PublisherPlatformDTO[] = await this.platformDAO.findAllPlatformsByPublisherId(p._id.toString(),
                        z);
                    const publisherUserDTO: PublisherUserDTO = new PublisherUserDTO();

                    publisherUserDTO.publisherId = p._id;
                    publisherUserDTO.email = p.created.by;
                    publisherUserDTO.notificationSettings = p.notificationSettings;
                    publisherUserDTO.created = { by: p.created.by, at: p.created.at };
                    if (p.modified && p.modified.by && p.modified.at) {
                        publisherUserDTO.modified = { by: p.modified.by, at: p.modified.at };
                    }
                    const pds = [];
                    for (const pl of allPlatforms) {
                        if (pl) {
                            pds.push({
                                platformId: pl._id,
                                isWatching: pl.isWatching,
                                userRoleId: publisherUserAdminRole._id.toString(),
                                zone: z
                            });
                        }
                    }
                    publisherUserDTO.platforms = pds;
                    // construct the publisher user
                    const existingPublisherUserDTO = await this.publisherUserDAO.findByEmail(p.created.by);
                    if (!existingPublisherUserDTO) {
                        await this.publisherUserDAO.create(publisherUserDTO);
                    }
                }
            }
            res.json({ message: 'Migrated to new Publisher roles', success: true });
        } catch (err) {
            catchError(err, next);
        }
    }
}
