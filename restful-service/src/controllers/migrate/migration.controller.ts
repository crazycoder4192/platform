import express from 'express';
import IController from '../../common/controller-interface';
import authenticationGuard from '../../guards/authentication.guard';
import { NewPublisherFieldTrustedSiteController } from './new-publisher-field-trusted-sites.controller';
import { NewPublisherRolesController } from './new-publisher-roles.controller';
import { NewZoneController } from './new-zone.controller';

export class MigrationController implements IController {
    public path = '/migrate';
    public router = express.Router();
    private readonly newPublisherRolesController: NewPublisherRolesController;
    private readonly newZoneController: NewZoneController;
    private readonly newPublisherFieldTrusetedSitesController: NewPublisherFieldTrustedSiteController;

    constructor() {
        this.newPublisherRolesController = new NewPublisherRolesController();
        this.newZoneController = new NewZoneController();
        this.newPublisherFieldTrusetedSitesController = new NewPublisherFieldTrustedSiteController();
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(`${this.path}/newpublisherroles`, authenticationGuard, this.newPublisherRolesController.migrate);
        this.router.post(`${this.path}/newzones`, authenticationGuard, this.newZoneController.migrate);
        this.router.post(`${this.path}/newpublisherfield/trustedsites`, authenticationGuard, this.newPublisherFieldTrusetedSitesController.migrate);
    }
}
