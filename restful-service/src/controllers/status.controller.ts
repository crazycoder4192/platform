import express from 'express';
import IController from '../common/controller-interface';
import catchError from '../error/catch-error';
import { publisherSiteTypeModel } from '../models/publisher/site-type-schema';
import { RedisHelper } from '../redis/redis-helper';

export class StatusController implements IController {
    public path = '/status';
    public router = express.Router();
    private readonly redisHelper: RedisHelper;

    constructor() {
        this.initializeRoutes();
        this.redisHelper = new RedisHelper();
    }

    public getStatus = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            let healthCheckSuccess: boolean = true;
            let mongoStatus: string = 'OK';
            let redisStatus: string = 'OK';
            try {
                // check connectivity with mongo db
                await publisherSiteTypeModel.findOne({ }).exec();
            } catch (error) {
                mongoStatus = 'ERROR';
                healthCheckSuccess = false;
            }

            try {
                // check connectivity with redis
                const a = await this.redisHelper.getInfo();
            } catch (error) {
                redisStatus = 'ERROR';
                healthCheckSuccess = false;
            }

            // quickbook connectivity

            // zendesk connectivity

            if (!healthCheckSuccess) {
                return res.status(500).json({ server: 'OK', mongoDB: mongoStatus, redis: redisStatus });
            } else {
                return res.json({ server: 'OK', mongoDB: mongoStatus, redis: redisStatus });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    private initializeRoutes() {
        this.router.get(`${this.path}/`, this.getStatus);
    }
}
