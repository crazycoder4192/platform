import csv from 'fast-csv';
import BannerDAO from '../../daos/admin/banner.dao';
import PermissionDAO from '../../daos/admin/permission.dao';
import AdvertiserAgencyTypeDAO from '../../daos/advertiser/advertiser-agency-type.dao';
import AdvertiserClientTypeDAO from '../../daos/advertiser/advertiser-client-type.dao';
import ObjectivesDAO from '../../daos/advertiser/objectives.dao';
import PublisherApplicationTypeDAO from '../../daos/publisher/publisher-application-type.dao';
import PublisherSiteTypeDAO from '../../daos/publisher/publisher-site-type.dao';
import TherapeuticAreaDAO from '../../daos/publisher/therapeutic-area.dao';
import ArchetypeDAO from '../../daos/utils/archetype.dao';
import CityDetailDAO from '../../daos/utils/city-detail.dao';
import TaxonomyDAO from '../../daos/utils/taxonomy.dao';
import TooltipDAO from '../../daos/utils/tooltip.dao';
import BannerDTO from '../../dtos/admin/banner.dto';
import PermissionDTO from '../../dtos/admin/permission.dto';
import AdvertiserAgencyTypeDTO from '../../dtos/advertiser/advertiser-agency-type.dto';
import AdvertiserClientTypeDTO from '../../dtos/advertiser/advertiser-client-type.dto';
import ObjectivesDTO from '../../dtos/advertiser/objectives.dto';
import PublisherApplicationTypeDTO from '../../dtos/publisher/publisher-application-type.dto';
import PublisherSiteTypeDTO from '../../dtos/publisher/publisher-site-type.dto';
import TherapeuticAreaDTO from '../../dtos/publisher/therapeutic-area.dto';
import TaxonomyDTO from '../../dtos/taxonomy.dto';
import ArchetypeDTO from '../../dtos/utils/archetype.dto';
import CityDetailDTO from '../../dtos/utils/city-detail.dto';
import TooltipDTO from '../../dtos/utils/tooltip.dto';

export class LoadMetadata {
  public async load() {
    await this.loadSiteTypesForPublisher();
    await this.loadAppTypesForPublisher();
    await this.loadStateCitiesAndPincode();
    await this.loadIndiaStateCitiesAndPincode();
    await this.loadIndiaStateCitiesAndPincodeStarter();
    await this.loadIndiaTaxonomyStarter();
    await this.loadAdvertiserTypes();
    await this.loadAdvertiserAgencyTypes();
    await this.loadPermissions();
    await this.loadTherapaticArea();
    await this.loadBanners();
    await this.loadObjectives();
    await this.loadHCPArchetypes();
    await this.loadToolTips();
  }

  // to load application types for publisher
  public async loadAppTypesForPublisher() {
    try {
      // TODO: are we creating duplicate entries here?
      const dao: PublisherApplicationTypeDAO = new PublisherApplicationTypeDAO();
      const details: PublisherApplicationTypeDTO[] = await dao.findAll();
      if (details.length === 0) {
        csv
          .fromPath('./resources/publisher_app_types.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  // to load sites types for publisher
  public async loadSiteTypesForPublisher() {
    try {
      const dao: PublisherSiteTypeDAO = new PublisherSiteTypeDAO();
      const details: PublisherSiteTypeDTO[] = await dao.findAll();
      if (details.length === 0) {
        csv
          .fromPath('./resources/publisher_site_types.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  // to load states, cities and pincode in US
  public async loadStateCitiesAndPincode() {
    try {
      const dao: CityDetailDAO = new CityDetailDAO();
      const details: CityDetailDTO[] = await dao.findAll('1');
      if (details.length === 0) {
        csv
          .fromPath('./resources/zip_code_database.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details, '1');
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  // to load states, cities and pincode in US
  public async loadIndiaStateCitiesAndPincode() {
    try {
      const dao: CityDetailDAO = new CityDetailDAO();
      const details: CityDetailDTO[] = await dao.findAll('2');
      if (details.length === 0) {
        csv
          .fromPath('./resources/zip_code_database_india.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details, '2');
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  // to load starter states, cities and pincode in India
  public async loadIndiaStateCitiesAndPincodeStarter() {
    try {
      const dao: CityDetailDAO = new CityDetailDAO();
      const zipCodes: string[] = await dao.findUniqueZipCodesInStarter('2');
      const details: CityDetailDTO[] = [];
      if (zipCodes.length === 0) {
        csv
          .fromPath('./resources/zip_code_database_india_starter.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createStarterMany(details, '2');
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  // to load taxonomy in India
  public async loadIndiaTaxonomyStarter() {
    try {
      const dao: TaxonomyDAO = new TaxonomyDAO();
      const details: TaxonomyDTO[] = [];
      const codes: string[] = await dao.findTaxonomiesInStarter('2');
      if (codes.length === 0) {
        csv
          .fromPath('./resources/taxonomy-india-starter.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createStarterMany(details, '2');
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  // to load types of advertiser client organizations
  public async loadAdvertiserTypes() {
    try {
      const dao: AdvertiserClientTypeDAO = new AdvertiserClientTypeDAO();
      const details: AdvertiserClientTypeDTO[] = await dao.findAll();
      if (details.length === 0) {
        csv
          .fromPath('./resources/advertiser_types.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

    // to load types of advertiser agency organization types
  public async loadAdvertiserAgencyTypes() {
      try {
        const dao: AdvertiserAgencyTypeDAO = new AdvertiserAgencyTypeDAO();
        const details: AdvertiserAgencyTypeDTO[] = await dao.findAll();
        if (details.length === 0) {
          csv
            .fromPath('./resources/advertiser_agency_types.csv', {
              headers: true,
              ignoreEmpty: true,
            })
            .on('data', (data: any) => {
              data.created = { by: 'admin@doceree.com' };
              details.push(data);
            })
            .on('end', async () => {
              try {
                await dao.createMany(details);
              } catch (err) {
                throw err;
              }
            });
        }
      } catch (err) {
        throw err;
      }
    }

  public async loadPermissions() {
    try {
      const dao: PermissionDAO = new PermissionDAO();
      const details: PermissionDTO[] = await dao.findAll();
      if (details.length === 0) {
        csv
          .fromPath('./resources/user_permissions_roles.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  public loadTherapaticArea = async () => {
    try {
      const dao: TherapeuticAreaDAO = new TherapeuticAreaDAO();
      const details: TherapeuticAreaDTO[] = await dao.findAll();
      if (details.length <= 0) {
        csv
          .fromPath('./resources/therapeutic_area.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  public async loadBanners() {
    try {
      const dao: BannerDAO = new BannerDAO();
      const details: BannerDTO[] = await dao.findAll();
      if (details.length === 0) {
        csv
          .fromPath('./resources/banners.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  public loadObjectives = async () => {
    try {
      const dao: ObjectivesDAO = new ObjectivesDAO();
      const details: ObjectivesDTO[] = await dao.findAll();
      if (details.length <= 0) {
        csv
          .fromPath('./resources/objectives.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            details.push(data);
          })
          .on('end', async () => {
            try {
              await dao.createMany(details);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  public loadHCPArchetypes = async () => {
    try {
      const archetypeDao: ArchetypeDAO = new ArchetypeDAO();
      const archetypes: ArchetypeDTO[] = await archetypeDao.findAll();
      if (archetypes.length <= 0) {
        csv
          .fromPath('./resources/hcp_archetypes.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            archetypes.push(data);
          })
          .on('end', async () => {
            try {
              await archetypeDao.createMany(archetypes);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }

  public loadToolTips = async () => {
    try {
      const tooltipDao: TooltipDAO = new TooltipDAO();
      const tooltips: TooltipDTO[] = await tooltipDao.findAll();
      if (tooltips.length <= 0) {
        csv
          .fromPath('./resources/tooltip.csv', {
            headers: true,
            ignoreEmpty: true,
          })
          .on('data', (data: any) => {
            data.created = { by: 'admin@doceree.com' };
            tooltips.push(data);
          })
          .on('end', async () => {
            try {
              await tooltipDao.createMany(tooltips);
            } catch (err) {
              throw err;
            }
          });
      }
    } catch (err) {
      throw err;
    }
  }
}
