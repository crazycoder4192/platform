import config from 'config';
const ApiContracts = require('authorizenet').APIContracts;
const ApiControllers = require('authorizenet').APIControllers;
const SDKConstants = require('authorizenet').Constants;
import moment from 'moment';
import { logger } from '../../util/winston';

export class AuthorizeNetHelper {

    public createAdvertiserCustomerAndPaymentProfile(customer: any) {
        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const creditCard = new ApiContracts.CreditCardType();
        const paymentType = new ApiContracts.PaymentType();
        creditCard.setCardNumber(customer.cardNumber);
        creditCard.setExpirationDate(customer.expiryDate);
        paymentType.setCreditCard(creditCard);

        const customerAddress = this.setCustomerAddress(customer);
        const customerPaymentProfileType = new ApiContracts.CustomerPaymentProfileType();
        customerPaymentProfileType.setCustomerType(ApiContracts.CustomerTypeEnum.INDIVIDUAL);
        customerPaymentProfileType.setBillTo(customerAddress);
        customerPaymentProfileType.setPayment(paymentType);

        const paymentProfilesList = [];
        paymentProfilesList.push(customerPaymentProfileType);

        const customerProfileType = new ApiContracts.CustomerProfileType();
        customerProfileType.setMerchantCustomerId(customer.merchantId);
        customerProfileType.setDescription(customer.description);
        customerProfileType.setEmail(customer.email);
        customerProfileType.setPaymentProfiles(paymentProfilesList);

        const createRequest = new ApiContracts.CreateCustomerProfileRequest();
        createRequest.setProfile(customerProfileType);
        createRequest.setValidationMode(ApiContracts.ValidationModeEnum.LIVEMODE);
        createRequest.setMerchantAuthentication(merchantAuthenticationType);
        return createRequest;
    }

    public updateCustomerPaymentProfile(customerProfileId: any, customerPaymentProfileId: any, customer: any) {
        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const creditCard = new ApiContracts.CreditCardType();
        const paymentType = new ApiContracts.PaymentType();
        creditCard.setExpirationDate(customer.expiryDate);
        creditCard.setCardNumber(customer.cardNumber);
        paymentType.setCreditCard(creditCard);

        const customerAddress = this.setCustomerAddress(customer);

        const customerPaymentProfileType = new ApiContracts.CustomerPaymentProfileExType();
        customerPaymentProfileType.setCustomerType(ApiContracts.CustomerTypeEnum.INDIVIDUAL);
        customerPaymentProfileType.setBillTo(customerAddress);
        customerPaymentProfileType.setPayment(paymentType);
        customerPaymentProfileType.setCustomerPaymentProfileId(customerPaymentProfileId);

        const updateRequest = new ApiContracts.UpdateCustomerPaymentProfileRequest();
        updateRequest.setMerchantAuthentication(merchantAuthenticationType);
        updateRequest.setCustomerProfileId(customerProfileId);
        updateRequest.setPaymentProfile(customerPaymentProfileType);
        updateRequest.setValidationMode(ApiContracts.ValidationModeEnum.LIVEMODE);
        return updateRequest;
    }

    public createAdvertiserPaymentProfile(customer: any) {
        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const creditCard = new ApiContracts.CreditCardType();
        const paymentType = new ApiContracts.PaymentType();

        creditCard.setCardNumber(customer.cardNumber);
        creditCard.setExpirationDate(customer.expiryDate);
        paymentType.setCreditCard(creditCard);

        const customerAddress = this.setCustomerAddress(customer);
        const profile = new ApiContracts.CustomerPaymentProfileType();
        profile.setBillTo(customerAddress);
        profile.setPayment(paymentType);

        // profile.setDefaultPaymentProfile(true);
        const createRequest = new ApiContracts.CreateCustomerPaymentProfileRequest();
        createRequest.setMerchantAuthentication(merchantAuthenticationType);
        createRequest.setCustomerProfileId(customer.customerProfileId);
        createRequest.setPaymentProfile(profile);

        return createRequest;
    }

    public deleteCustomerPaymentProfile(customerProfileId: any, customerPaymentProfileId: any) {

        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const deleteRequest = new ApiContracts.DeleteCustomerPaymentProfileRequest();
        deleteRequest.setMerchantAuthentication(merchantAuthenticationType);
        deleteRequest.setCustomerProfileId(customerProfileId);
        deleteRequest.setCustomerPaymentProfileId(customerPaymentProfileId);

        // pretty print request
        // print(JSON.stringify(deleteRequest.getJSON(), null, 2));
        const ctrl = new ApiControllers.DeleteCustomerPaymentProfileController(deleteRequest.getJSON());
        const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
        if (!isSandbox) {
            ctrl.setEnvironment(SDKConstants.endpoint.production);
        }
        ctrl.execute(() => {
            const apiResponse = ctrl.getResponse();
        });
        return 'deleted';
    }

    public deleteCustomerProfile(customerProfileId: any) {
        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const deleteRequest = new ApiContracts.DeleteCustomerProfileRequest();
        deleteRequest.setMerchantAuthentication(merchantAuthenticationType);
        deleteRequest.setCustomerProfileId(customerProfileId);

        // pretty print request
        const ctrl = new ApiControllers.DeleteCustomerProfileController(deleteRequest.getJSON());
        const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
        if (!isSandbox) {
            ctrl.setEnvironment(SDKConstants.endpoint.production);
        }
        ctrl.execute(() => {
            const apiResponse = ctrl.getResponse();
        });
        return 'deleted';
    }

    public async getCustomerPaymentProfile(customerProfileId: any, customerPaymentProfileId: any, callback: any) {

        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const getRequest = new ApiContracts.GetCustomerPaymentProfileRequest();
        getRequest.setMerchantAuthentication(merchantAuthenticationType);
        getRequest.setCustomerProfileId(customerProfileId);
        getRequest.setCustomerPaymentProfileId(customerPaymentProfileId);

        // pretty print request
        const ctrl = new ApiControllers.GetCustomerProfileController(getRequest.getJSON());
        const isSandbox = config.get<string>('paymentGateway.environment') === 'sandbox' ? true : false;
        if (!isSandbox) {
            ctrl.setEnvironment(SDKConstants.endpoint.production);
        }
        ctrl.execute(async () => {
            const apiResponse = ctrl.getResponse();
            const response = await new ApiContracts.GetCustomerPaymentProfileResponse(apiResponse);
            callback(response);
        });
    }

    /*public async chargeCustomerProfile(customerProfileId: any, customerPaymentProfileId: any,
                                       invoiceNumber: any, amount: number) {
        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const profileToCharge = new ApiContracts.CustomerProfilePaymentType();
        profileToCharge.setCustomerProfileId(customerProfileId);

        const paymentProfile = new ApiContracts.PaymentProfile();
        paymentProfile.setPaymentProfileId(customerPaymentProfileId);
        profileToCharge.setPaymentProfile(paymentProfile);

        const orderDetails = new ApiContracts.OrderType();
        orderDetails.setInvoiceNumber(invoiceNumber);
        orderDetails.setDescription('Product Description');

        const transactionRequestType = new ApiContracts.TransactionRequestType();
        transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
        transactionRequestType.setProfile(profileToCharge);
        transactionRequestType.setAmount(amount);
        // transactionRequestType.setLineItems(lineItems);
        transactionRequestType.setOrder(orderDetails);
        // transactionRequestType.setShipTo(shipTo);
        const createRequest = new ApiContracts.CreateTransactionRequest();
        createRequest.setMerchantAuthentication(merchantAuthenticationType);
        createRequest.setTransactionRequest(transactionRequestType);

        // pretty print request
        return createRequest;
    }*/

    public chargeCustomerProfile(customerProfileId: any, customerPaymentProfileId: any,
                                 invoice: any, advertiser: any) {
        const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
        merchantAuthenticationType.setName(config.get<string>('paymentGateway.apiLoginKey'));
        merchantAuthenticationType.setTransactionKey(config.get<string>('paymentGateway.transactionKey'));

        const profileToCharge = new ApiContracts.CustomerProfilePaymentType();
        profileToCharge.setCustomerProfileId(customerProfileId);

        const paymentProfile = new ApiContracts.PaymentProfile();
        paymentProfile.setPaymentProfileId(customerPaymentProfileId);
        profileToCharge.setPaymentProfile(paymentProfile);

        const orderDetails = new ApiContracts.OrderType();
        orderDetails.setInvoiceNumber(invoice.invoiceNumber);
        orderDetails.setDescription(`Invoice for brand ${invoice.brandName} from ${moment(invoice.invoiceStartDate).format('MM/DD/YYYY')}-${moment(invoice.invoiceStartDate).format('MM/DD/YYYY')}`);

        const lineItemList = [];
        for (const invoiceItem of invoice.items) {
            const lineItem = new ApiContracts.LineItemType();
            lineItem.setItemId(invoiceItem.item);

            let name = invoiceItem.campaignName;
            if (invoiceItem.campaignName.length > 30) {
                name = `${invoiceItem.campaignName.substring(0, 27)}...`;
            }
            lineItem.setName(name);
            const duration = `Invoice for duration ${moment(invoiceItem.startDate).format('MM/DD/YYYY')}-${moment(invoiceItem.endDate).format('MM/DD/YYYY')}`;
            lineItem.setDescription(duration);
            lineItem.setQuantity(1);
            lineItem.setUnitPrice(parseFloat(invoiceItem.amount).toFixed(2));
            lineItemList.push(lineItem);
        }

        const lineItems = new ApiContracts.ArrayOfLineItem();
        lineItems.setLineItem(lineItemList);

        const shipTo = new ApiContracts.CustomerAddressType();

        shipTo.setFirstName(advertiser.firstName);
        shipTo.setLastName(advertiser.lastName);
        shipTo.setCompany(advertiser.businessName);
        shipTo.setAddress(advertiser.address.addressLine1);
        shipTo.setCity(advertiser.address.city);
        shipTo.setState(advertiser.address.state);
        shipTo.setZip(advertiser.address.zipcode);
        shipTo.setCountry(advertiser.address.country);
        shipTo.setPhoneNumber(advertiser.address.phoneNumber);

        const transactionRequestType = new ApiContracts.TransactionRequestType();
        transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
        transactionRequestType.setProfile(profileToCharge);
        transactionRequestType.setAmount(Number(parseFloat(invoice.amount).toFixed(2)));
        transactionRequestType.setLineItems(lineItems);
        transactionRequestType.setOrder(orderDetails);
        transactionRequestType.setShipTo(shipTo);

        const createRequest = new ApiContracts.CreateTransactionRequest();
        createRequest.setMerchantAuthentication(merchantAuthenticationType);
        createRequest.setTransactionRequest(transactionRequestType);

        invoice.authorizeNetRequest = JSON.stringify(createRequest.getJSON());

        return createRequest;
    }

    private setCustomerAddress(customer: any) {
        const customerAddress = new ApiContracts.CustomerAddressType();
        customerAddress.setFirstName(customer.firstName);
        customerAddress.setLastName(customer.lastName);
        customerAddress.setAddress(customer.address);
        customerAddress.setCity(customer.city);
        customerAddress.setState(customer.state);
        customerAddress.setZip(customer.zipcode);
        customerAddress.setCountry(customer.country);
        customerAddress.setPhoneNumber(customer.phoneNumber);
        customerAddress.setCompany(customer.company);
        return customerAddress;
    }
}
