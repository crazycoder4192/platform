import Request from 'request-promise';

export class ZendeskHelper {

    public async createOrganization(orgName: string, orgExternalId: string) {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/organizations.json';
        const payload: any = {
            organization: {
                name: orgName,
                external_id: orgExternalId
            }
        };
        const header: any = this.getHeader();
        const response = await Request.post({
            url: targetUrl,
            headers: header,
            body: payload,
            json: true
        });
        // console.log('create organization', response);
        return response;
    }

    public async createUser(firstName: string, lastName: string, email: string, phone: string,
                            userType: string, userExternalId: string) {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/users.json';
        const payload: any = {
            user: {
                name: `${firstName} ${lastName}`,
                email,
                phone,
                external_id: userExternalId,
                user_fields: {
                    user_type: userType
                }
            }
        };
        const header: any = this.getHeader();
        const response = await Request.post({
            url: targetUrl,
            headers: header,
            body: payload,
            json: true
        });
        // console.log('create user', response);
        return response;
    }

    public async associateUserWithOrganization(orgExternalId: string, userExternalId: string) {
        const orgId = await this.getOrgId(orgExternalId);
        const userId = await this.getUserId(userExternalId);
        const targetUrl = 'https://doceree.zendesk.com/api/v2/organization_memberships.json';
        const header: any = this.getHeader();
        const payload: any = {
            organization_membership: {
                user_id: userId,
                organization_id: orgId
            }
        };
        const response = await Request.post({
            url: targetUrl,
            headers: header,
            body: payload,
            json: true
        });
        // console.log('create organization membership', response);
        return response;
    }

    public async createTicket(userExternalId: string, subject: string, comment: string, priority: string) {
        const userId: string = await this.getUserId(userExternalId);
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/tickets.json';
        const payload: any = {
            ticket: {
                subject,
                comment: {
                    body: comment
                },
                priority,
                requester_id: userId
            }
        };
        const header: any = this.getHeader();
        const response = await Request.post({
            url: targetUrl,
            headers: header,
            body: payload,
            json: true
        });
        // console.log('creat ticket', response);
        return response;
    }

    public async updateTicket(ticketId: string, zendeskUserId: string, subject: string, priority: string, status: string) {
        const targetUrl: string = `https://doceree.zendesk.com/api/v2/tickets/${ticketId}.json`;
        const payload: any = {
            ticket: {
                subject,
                priority,
                status,
                requester_id: parseInt(zendeskUserId, 10)
            }
        };
        const header: any = this.getHeader();
        const response = await Request.put({
            url: targetUrl,
            headers: header,
            body: payload,
            json: true
        });
        // console.log('creat ticket', response);
        return response;
    }

    public async getAllTicketsForUser(userExternalId: string) {
        const userId: string = await this.getUserId(userExternalId);
        if (!userId) {
            return null;
        } else {
            const targetUrl: string = `https://doceree.zendesk.com/api/v2/users/${userId}/tickets/requested.json`;
            const header: any = this.getHeader();
            const response = await Request.get({
                url: targetUrl,
                headers: header,
                json: true
            });
            // console.log('get all tickets for user', response);
            response.userId = userId;
            return response;
        }
    }

    public async getAllTicketsForOrganization(orgExternalId: string) {
        const orgId: string = await this.getOrgId(orgExternalId);
        const targetUrl: string = `https://doceree.zendesk.com/api/v2/organizations/${orgId}/tickets.json`;
        const header: any = this.getHeader();
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            json: true
        });
        // console.log('get all tickets for organization', response);
        return response;
    }

    public async verifyTicketId(ticketId: string, userExternalId: string): Promise<any> {
        const userId: string = await this.getUserId(userExternalId);
        const targetUrl: string = `https://doceree.zendesk.com/api/v2/tickets/${ticketId}.json`;
        const header: any = this.getHeader();
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            json: true
        });
        if (response.ticket.requester_id === userId) {
            return { valid: true, ticket: response.ticket, userId };
        }
        return { };
    }

    public async getTicketComments(ticketId: string, userExternalId: string) {
        const targetUrl: string = `https://doceree.zendesk.com/api/v2/tickets/${ticketId}/comments.json`;
        const header: any = this.getHeader();
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            json: true
        });
        // console.log(response);
        return response;
    }

    public async addTicketComment(ticketId: string, userExternalId: string, comment: string) {
        const userId: string = await this.getUserId(userExternalId);
        const targetUrl: string = `https://doceree.zendesk.com/api/v2/tickets/${ticketId}.json`;
        const header: any = this.getHeader();
        const payload: any = {
            ticket: {
                comment: {
                    body: comment,
                    author_id: userId
                }
            }
        };
        const response = await Request.put({
            url: targetUrl,
            headers: header,
            body: payload,
            json: true
        });
        // console.log('add ticket comments', response);
        return response;
    }

    public async getArticles(pageNumber: number) {
        const targetUrl: string = `https://doceree.zendesk.com/api/v2/help_center/en-us/articles.json?page=${pageNumber}`;
        const header: any = this.getHeader();
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            json: true
        });
        return response;
    }

    public async getSections() {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/help_center/en-us/sections.json';
        const header: any = this.getHeader();
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            json: true
        });
        return response;
    }

    public async getOrg(orgExternalId: string): Promise<any> {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/organizations/search.json';
        const header: any = this.getHeader();
        const queryString: any = {
            external_id: orgExternalId
        };
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            qs: queryString,
            json: true
        });
        // console.log('get org', response);
        return response;
    }

    public async getUser(userExternalId: string): Promise<any> {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/users/search.json';
        const header: any = this.getHeader();
        const queryString: any = {
            external_id: userExternalId
        };
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            qs: queryString,
            json: true
        });
        // console.log('get user', response);
        return response;
    }

    private async getOrgId(orgExternalId: string): Promise<string> {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/organizations/search.json';
        const header: any = this.getHeader();
        const queryString: any = {
            external_id: orgExternalId
        };
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            qs: queryString,
            json: true
        });
        if (response && response.organizations[0]) {
            const orgId = response.organizations[0].id;
            // console.log('get orgId', orgId);
            return orgId;
        } else {
            return null;
        }
    }

    private async getUserId(userExternalId: string): Promise<string> {
        const targetUrl: string = 'https://doceree.zendesk.com/api/v2/users/search.json';
        const header: any = this.getHeader();
        const queryString: any = {
            external_id: userExternalId
        };
        const response = await Request.get({
            url: targetUrl,
            headers: header,
            qs: queryString,
            json: true
        });
        if (response && response.users[0]) {
            const userId = response.users[0].id;
            // console.log('get userId', userId);
            return userId;
        } else {
            return null;
        }
    }

    private getHeader(): string {
        const c = 'jeril.jacob@doceree.com/token:WYWlgiVjWx9cPQnDDrvniXs6NBrWg2rB8cT5WtQZ';
        const buff = new Buffer(c);
        const base64data = buff.toString('base64');
        const h: any = { };
        h.Authorization = `Basic ${base64data}`;
        return h;
    }

}
