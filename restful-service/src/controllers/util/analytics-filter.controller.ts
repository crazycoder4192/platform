import express from 'express';
import UserDAO from '../../daos/user.dao';
import AnalyticsFilterDAO from '../../daos/utils/analytics-filter.dao';
import UserDTO from '../../dtos/user.dto';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import authenticationGuard from '../../guards/authentication.guard';
import { UserStatuses, UserTypes } from '../../util/constants';
import { logger } from '../../util/winston';

export class AnalyticsFilterController {
  public router = express.Router();
  public path = '/util';

  private readonly userDAO: UserDAO;
  private readonly analyticsDAO: AnalyticsFilterDAO;

  constructor() {
    this.router.get(`${this.path}`, authenticationGuard, this.getRoot);
    this.userDAO = new UserDAO();
    this.analyticsDAO = new AnalyticsFilterDAO();
    this.router.get(
      `${this.path}/filter/email/:email`,
      authenticationGuard,
      this.getAnalyticsFiltersByUserEmail
    );
    this.router.post(
      `${this.path}/filter`,
      authenticationGuard,
      this.saveFilter
    );
    this.router.get(
      `${this.path}/filter/:id`,
      authenticationGuard,
      this.saveFilter
    );
    this.router.post(
      `${this.path}/filter/checkfilternameexists`,
      authenticationGuard,
      this.checkFilterNameExists
    );
  }

  private readonly getAnalyticsFiltersByUserEmail = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      if ((user.userType.toLowerCase() === UserTypes.publisher.toLowerCase() ||
          user.userType.toLowerCase() === UserTypes.advertiser.toLowerCase())
          && user.userStatus.toLowerCase() === UserStatuses.active.toLowerCase()) {
            const locations = await this.analyticsDAO.getFiltersByUserEmail(req.email);
            res.json(locations);
      } else {
        logger.error(`No filter found against user [${req.email}]`);
        res.json({ });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly saveFilter = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      if ((user.userType.toLowerCase() === UserTypes.publisher.toLowerCase() ||
          user.userType.toLowerCase() === UserTypes.advertiser.toLowerCase())
          && user.userStatus.toLowerCase() === UserStatuses.active.toLowerCase()) {
            const filter: FilterAnalyticsDTO = await this.analyticsDAO.createFilter(this.toAnalyticsFilterDTO(req));
            res.json({ message: 'Filter added successfully' });
      } else {
        logger.error(`Unable to save filter against logged in user [${req.email}]`);
        throw new HandledApplicationError(500, 'Unable to save filter against logged in user');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly checkFilterNameExists = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      if ((user.userType.toLowerCase() === UserTypes.publisher.toLowerCase() ||
          user.userType.toLowerCase() === UserTypes.advertiser.toLowerCase())
          && user.userStatus.toLowerCase() === UserStatuses.active.toLowerCase()) {
            const filter: FilterAnalyticsDTO[] = await this.analyticsDAO.getFilterByNameAndEmail(req.body.filterName, req.email);
            if (filter.length === 0) {
              res.json({ filterExist: false, message: `Filter with the name ${req.body.filterName} does not exist` });
            } else {
              res.json({ filterExist: true, message: `Filter with the name ${req.body.filterName} exists` });
            }
      } else {
        logger.error(`Unable to save filter against logged in user [${req.email}]`);
        throw new HandledApplicationError(500, 'Unable to save filter against logged in user');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  private toAnalyticsFilterDTO(
    req: IAuthenticatedRequest,
  ): FilterAnalyticsDTO {
    const dto: FilterAnalyticsDTO = new FilterAnalyticsDTO();
    dto.specialization = req.body.specialization;
    dto.gender = req.body.gender;
    dto.location = req.body.location;
    dto.user = req.email;
    dto.created = { at: new Date(), by: req.email };
    dto.modified = { at: new Date(), by: req.email };
    dto.filterName = req.body.filterName;
    return dto;
  }

  private readonly getRoot = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ) => {
    response.send('Doceree Publisher API page');
  }

}
