import config from 'config';
import QuickbooksTokenDAO from '../../daos/quickbooks-token.dao';
import QuickbooksTokenDTO from '../../dtos/quickbooks-token.dto';
import HandledApplicationError from '../../error/handled-application-error';
const OAuthClient = require('intuit-oauth');
const QuickBooks = require('node-quickbooks-promise');

export class QuickbooksHelper {

    private oauthClient: any = null;
    private readonly quickbooksTokenDAO: QuickbooksTokenDAO;

    constructor() {
        this.quickbooksTokenDAO = new QuickbooksTokenDAO();
    }

    public async getAccessToken(zone: string) {
        const tokens: any[] = await this.quickbooksTokenDAO.find(zone);
        if (tokens && tokens.length > 0) {
            if (tokens.length > 1) {
                throw new HandledApplicationError(500, 'Did not expect more than one records');
            }
        } else {
            throw new HandledApplicationError(500, 'You have to manually insert the first token');
        }
        const accessToken = tokens[0].accessToken;
        const refreshToken = tokens[0].refreshToken;

        let realmId: string = '';
        if (zone === '1') {
            realmId = config.get<string>('quickbooks.realmId');
        } else if (zone === '2') {
            realmId = config.get<string>('quickbooks.india.realmId');
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
        this.getOauthClient(zone);
        if (this.oauthClient.isAccessTokenValid(accessToken)) {
            return {
                accessToken,
                refreshToken,
                realmId
            };
        } else {
            const s = await this.oauthClient.refreshUsingToken(refreshToken);
            const tokenDTO: QuickbooksTokenDTO = tokens[0];
            tokenDTO.accessToken = s.token.access_token;
            tokenDTO.refreshToken = s.token.refresh_token;
            await this.quickbooksTokenDAO.update(tokenDTO._id, tokenDTO, zone);

            return {
                accessToken: s.token.access_token,
                refreshToken: s.token.refresh_token,
                realmId
            };
        }
    }

    // one time activity
    public async createBankAccount(accountName: string, zone: string): Promise<any> {
        const qbo = await this.getQbo(zone);
        const payload: any = { };
        payload.Name = accountName;
        payload.AccountType = 'Bank';
        const output = await qbo.createAccount(payload);
        return output;
    }

    // one time activity
    public async createItemForAdvertiser(item: any, zone: string) {
        const qbo = await this.getQbo(zone);
        const payload: any = { };
        payload.Name = item.name;
        payload.Taxable = false;
        payload.Description = item.description;
        payload.Type = item.type;

        const account: any = await this.findIncomeAccountByName(item.accountName, zone);
        if (!account) {
            throw new Error(`Unable to find quickbook account by name ${item.accountName}`);
        }
        payload.IncomeAccountRef = { };
        payload.IncomeAccountRef.name = account.Name;
        payload.IncomeAccountRef.value = account.Id;
        return await qbo.createItem(payload);
    }

    // one time activity
    public async createItemForPublisher(item: any, zone: string) {
        const qbo = await this.getQbo(zone);
        const payload: any = { };
        payload.Name = item.name;
        payload.Taxable = false;
        payload.Description = item.description;
        payload.Type = item.type;

        const account: any = await this.findIncomeAccountByName(item.accountName, zone);
        if (!account) {
            throw new Error(`Unable to find quickbook account by name ${item.accountName}`);
        }
        payload.ExpenseAccountRef = { };
        payload.ExpenseAccountRef.name = account.Name;
        payload.ExpenseAccountRef.value = account.Id;
        return await qbo.createItem(payload);
    }

    public async createCustomer(customer: any, zone: string) {

        const payload: any = { };
        payload.FullyQualifiedName = customer.organizationName;
        payload.PrimaryEmailAddr = { };
        payload.PrimaryEmailAddr.Address = customer.email;
        payload.DisplayName = `${customer.firstName} ${customer.lastName}`;

        payload.GivenName = customer.firstName;
        payload.FamilyName = customer.lastName;

        payload.PrimaryPhone = { };
        payload.PrimaryPhone.FreeFormNumber = customer.phoneNumber;
        payload.CompanyName = customer.organizationName;

        payload.BillAddr = { };
        payload.BillAddr.CountrySubDivisionCode = customer.state;
        payload.BillAddr.City = customer.city;
        payload.BillAddr.PostalCode = customer.zip;
        payload.BillAddr.Line1 = customer.addressLine1;
        payload.BillAddr.Country = customer.country;

        const qbo = await this.getQbo(zone);
        await qbo.createCustomer(payload);
    }

    public async findCustomerByEmail(email: string, zone: string): Promise<any> {
        const qbo = await this.getQbo(zone);
        const queryString = `where PrimaryEmailAddr = '${email}'`;
        const output = await qbo.findCustomers(queryString);
        if (output.QueryResponse && output.QueryResponse.Customer && output.QueryResponse.Customer.length > 0) {
            return output.QueryResponse.Customer[0];
        }
        return null;
    }

    public async createInvoice(invoice: any, zone: string): Promise<any> {
        const payload: any = { };
        payload.Line = [];
        if (invoice.line && invoice.line.length > 0) {
            for (const b of invoice.line) {
                const a: any = { };
                a.DetailType = 'SalesItemLineDetail';
                a.Description = b.description;
                a.Amount = b.amount;

                a.SalesItemLineDetail = { };
                a.SalesItemLineDetail.ItemRef = { };
                const i = await this.findItemByName(b.itemName, zone);
                if (!i) {
                    throw new Error(`Unable to find quickbook item by name ${b.itemName}`);
                }
                a.SalesItemLineDetail.ItemRef.name = i.Name;
                a.SalesItemLineDetail.ItemRef.value = i.Id;
                a.SalesItemLineDetail.Qty = b.quantity;
                payload.Line.push(a);
            }
        }

        payload.DueDate = invoice.dueDate;

        payload.CustomerRef = { };
        const customer = await this.findCustomerByEmail(invoice.customerEmail, zone);
        if (!customer) {
            throw new Error(`Unable to find quickbook customer by email ${invoice.customerEmail}`);
        }
        payload.CustomerRef.value = customer.Id;
        payload.BillEmail = { };
        payload.BillEmail.Address = invoice.customerEmail;
        // payload.EmailStatus = 'NeedToSend';

        const qbo = await this.getQbo(zone);
        const output = await qbo.createInvoice(payload);
        // await qbo.sendInvoicePdf(output.Id);
        return output.Id;
    }

    public async createPayment(payment: any, zone: string): Promise<any> {
        const payload: any = { };

        payload.CustomerRef = { };
        const customer = await this.findCustomerByEmail(payment.customerEmail, zone);
        if (!customer) {
            throw new Error(`Unable to find quickbook customer by email ${payment.customerEmail}`);
        }
        payload.CustomerRef.value = customer.Id;
        payload.TotalAmt = payment.amount;

        payload.Line = [];
        const a: any = { };
        a.Amount = payment.amount;
        a.LinkedTxn = [];

        const linkedTxn: any = { };
        linkedTxn.TxnId = payment.invoiceId;
        linkedTxn.TxnType = 'Invoice';
        a.LinkedTxn.push(linkedTxn);

        payload.Line.push(a);
        const qbo = await this.getQbo(zone);
        const output = await qbo.createPayment(payload);
    }

    public async createVendor(vendor: any, zone: string) {
        const payload: any = { };
        payload.FullyQualifiedName = vendor.organizationName;
        payload.PrimaryEmailAddr = { };
        payload.PrimaryEmailAddr.Address = vendor.email;
        payload.DisplayName = `${vendor.firstName} ${vendor.lastName}`;

        payload.GivenName = vendor.firstName;
        payload.FamilyName = vendor.lastName;

        payload.PrimaryPhone = { };
        payload.PrimaryPhone.FreeFormNumber = vendor.phoneNumber;
        payload.CompanyName = vendor.organizationName;

        payload.BillAddr = { };
        payload.BillAddr.CountrySubDivisionCode = vendor.state;
        payload.BillAddr.City = vendor.city;
        payload.BillAddr.PostalCode = vendor.zip;
        payload.BillAddr.Line1 = vendor.addressLine1;
        payload.BillAddr.Country = vendor.country;

        const qbo = await this.getQbo(zone);
        await qbo.createVendor(payload);
    }

    public async findVendorByEmail(email: string, zone: string): Promise<any> {
        const qbo = await this.getQbo(zone);
        const output = await qbo.findVendors();
        if (output.QueryResponse && output.QueryResponse.Vendor && output.QueryResponse.Vendor.length > 0) {
            for (const o of output.QueryResponse.Vendor) {
                if (o.PrimaryEmailAddr) {
                    if (o.PrimaryEmailAddr.Address === email) {
                        return o;
                    }
                }
            }
        }
        return null;
    }

    public async createBill(bill: any, zone: string): Promise<any> {
        const payload: any = { };
        payload.Line = [];

        if (bill.line && bill.line.length > 0) {
            for (const b of bill.line) {
                const a: any = { };
                a.Description = b.description;
                a.Amount = b.amount;
                a.DetailType = 'ItemBasedExpenseLineDetail';

                a.ItemBasedExpenseLineDetail = { };
                a.ItemBasedExpenseLineDetail.ItemRef = { };
                const i = await this.findItemByName(b.itemName, zone);
                if (!i) {
                    throw new Error(`Unable to find quickbook item by name ${b.itemName}`);
                }
                a.ItemBasedExpenseLineDetail.ItemRef.name = i.Name;
                a.ItemBasedExpenseLineDetail.ItemRef.value = i.Id;
                payload.Line.push(a);
            }
        }

        payload.DueDate = bill.dueDate;

        payload.VendorRef = { };
        const vendor = await this.findVendorByEmail(bill.vendorEmail, zone);
        if (!vendor) {
            throw new Error(`Unable to find quickbook vendor by email ${bill.vendorEmail}`);
        }
        payload.VendorRef.value = vendor.Id;

        const qbo = await this.getQbo(zone);
        return await qbo.createBill(payload);
    }

    private getOauthClient(zone: string) {
        if (zone === '1') {
            this.oauthClient = new OAuthClient({
                clientId: config.get<string>('quickbooks.clientId'),
                clientSecret: config.get<string>('quickbooks.clientSecret'),
                environment: config.get<string>('quickbooks.environment'),
                redirectUri: config.get<string>('quickbooks.redirectUri')
            });
        } else if (zone === '2') {
            this.oauthClient = new OAuthClient({
                clientId: config.get<string>('quickbooks.india.clientId'),
                clientSecret: config.get<string>('quickbooks.india.clientSecret'),
                environment: config.get<string>('quickbooks.india.environment'),
                redirectUri: config.get<string>('quickbooks.india.redirectUri')
            });
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }
    }

    private async findIncomeAccountByName(accountName: string, zone: string): Promise<any> {
        const qbo = await this.getQbo(zone);
        const queryString = `where Name = '${accountName}'`;
        const output = await qbo.findAccounts(queryString);
        if (output.QueryResponse && output.QueryResponse.Account && output.QueryResponse.Account.length > 0) {
            return output.QueryResponse.Account[0];
        }
        return null;
    }

    private async findItemByName(name: string, zone: string): Promise<any> {
        const qbo = await this.getQbo(zone);
        const queryString = `where Name = '${name}'`;
        const output = await qbo.findItems(queryString);
        if (output.QueryResponse && output.QueryResponse.Item && output.QueryResponse.Item.length > 0) {
            return output.QueryResponse.Item[0];
        }
        return null;
    }

    private async getQbo(zone: string): Promise<any> {
        const r: any = await this.getAccessToken(zone);

        let isSandbox: boolean = true;
        let clientId: string = '';
        let clientSecret: string = '';
        if (zone === '1') {
            isSandbox = config.get<string>('quickbooks.environment') === 'sandbox' ? true : false;
            clientId = config.get<string>('quickbooks.clientId');
            clientSecret = config.get<string>('quickbooks.clientSecret');
        } else if (zone === '2') {
            isSandbox = config.get<string>('quickbooks.india.environment') === 'sandbox' ? true : false;
            clientId = config.get<string>('quickbooks.india.clientId');
            clientSecret = config.get<string>('quickbooks.india.clientSecret');
        } else {
            throw new Error(`Invalid zone ${zone}`);
        }

        const qbo = new QuickBooks(
            clientId,
            clientSecret,
            r.accessToken,
            false, // no token secret for oAuth 2.0
            r.realmId,
            isSandbox, // use the sandbox?
            false, // enable debugging?
            null, // set minorversion, or null for the latest version
            '2.0', // oAuth version
            r.refreshToken);
        return qbo;
    }
}
