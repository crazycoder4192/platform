import express from 'express';
import request = require('request');
import IController from '../../common/controller-interface';
import BannerDAO from '../../daos/admin/banner.dao';
import ObjectivesDAO from '../../daos/advertiser/objectives.dao';
import SubCampaignDAO from '../../daos/advertiser/sub-campaign.dao';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import TherapeuticAreaDAO from '../../daos/publisher/therapeutic-area.dao';
import ArchetypeDAO from '../../daos/utils/archetype.dao';
import CityDetailDAO from '../../daos/utils/city-detail.dao';
import TaxonomyDAO from '../../daos/utils/taxonomy.dao';
import TooltipDAO from '../../daos/utils/tooltip.dao';
import ValidatedHcpDAO from '../../daos/validated-hcp.dao';
import BannerDTO from '../../dtos/admin/banner.dto';
import ObjectivesDTO from '../../dtos/advertiser/objectives.dto';
import TherapeuticAreaDTO from '../../dtos/publisher/therapeutic-area.dto';
import ArchetypeDTO from '../../dtos/utils/archetype.dto';
import CityDetailDTO from '../../dtos/utils/city-detail.dto';
import TooltipDTO from '../../dtos/utils/tooltip.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import authenticationGuard from '../../guards/authentication.guard';
import { logger } from '../../util/winston';
import { AuthorizeAdmin } from '../admin/authorize-admin';
import { LoadMetadata } from './load-metadata';

export class UtilController implements IController {
  public path = '/util';
  public router = express.Router();
  private readonly cityDetailDAO: CityDetailDAO;
  private readonly bannerDetailDAO: BannerDAO;
  private readonly therapeuticAreaDAO: TherapeuticAreaDAO;
  private readonly objectivesDAO: ObjectivesDAO;
  private readonly archetypeDAO: ArchetypeDAO;
  private readonly validatedHcpDAO: ValidatedHcpDAO;
  private readonly taxonomyDAO: TaxonomyDAO;
  private readonly subcampaignDAO: SubCampaignDAO;
  private readonly assetDAO: PublisherAssetDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly tooltipDAO: TooltipDAO;

  constructor() {
    this.initializeRoutes();
    this.bannerDetailDAO = new BannerDAO();
    this.therapeuticAreaDAO = new TherapeuticAreaDAO();
    this.objectivesDAO = new ObjectivesDAO();
    this.cityDetailDAO = new CityDetailDAO();
    this.archetypeDAO = new ArchetypeDAO();
    this.validatedHcpDAO = new ValidatedHcpDAO();
    this.taxonomyDAO = new TaxonomyDAO();
    this.subcampaignDAO = new SubCampaignDAO();
    this.assetDAO = new PublisherAssetDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
    this.tooltipDAO = new TooltipDAO();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}`,
      authenticationGuard,
      this.getRoot
    );
    this.router.get(
      `${this.path}/banners`,
      authenticationGuard,
      this.getBanners
    );
    this.router.get(
      `${this.path}/cities`,
      authenticationGuard,
      this.getCities
    );
    this.router.get(
      `${this.path}/cities/unique`,
      authenticationGuard,
      this.getUniqueCities
    );
    this.router.get(
      `${this.path}/citiessearch/:citystring`,
      authenticationGuard,
      this.getCitySearch
    );
    this.router.get(
      `${this.path}/citydetailsbycityname/:cityname`,
      authenticationGuard,
      this.getcityDetailsList
    );
    this.router.get(
      `${this.path}/states/unique`,
      authenticationGuard,
      this.getUniqueStates
    );
    this.router.get(
      `${this.path}/state/:city`,
      authenticationGuard,
      this.getStateByCity
    );
    this.router.get(
      `${this.path}/therapeuticTypes/unique`,
      authenticationGuard,
      this.getUniqueTherapeutic
    );
    this.router.get(
      `${this.path}/therapeuticTypes`,
      authenticationGuard,
      this.getAllTherapeutic
    );
    this.router.get(
      `${this.path}/objectives`,
      authenticationGuard,
      this.getAllObjectives
    );
    this.router.get(
      `${this.path}/archetypes`,
      authenticationGuard,
      this.getArchetypes
    );
    this.router.get(
      `${this.path}/validHCPLocations`,
      authenticationGuard,
      this.getLocationForValidatedHCP
    );
    this.router.get(
      `${this.path}/validHCPLocations/:type/:id`,
      authenticationGuard,
      this.getLocationForValidatedHCPByTypeAndId
    );
    this.router.get(
      `${this.path}/validHCPLocationList/:type/:id`,
      authenticationGuard,
      this.getLocationListForValidatedHCPByTypeAndId
    );
    this.router.get(
      `${this.path}/validHCPSpecialization`,
      authenticationGuard,
      this.getSpecializationForValidatedHCP
    );
    this.router.get(
      `${this.path}/validHCPSpecialization/:type/:id`,
      authenticationGuard,
      this.getSpecializationForValidatedHCPByTypeAndId
    );
    this.router.get(
      `${this.path}/validHCPSpecializationList/:type/:id`,
      authenticationGuard,
      this.getSpecializationListForValidatedHCPByTypeAndId
    );
    this.router.get(
      `${this.path}/validHCPGender/:type/:id`,
      authenticationGuard,
      this.getGenderForValidatedHCPByTypeAndId
    );
    this.router.get(
      `${this.path}/zipcodes/unique`,
      authenticationGuard,
      this.getUniqueZipCodes
    );
    this.router.get(
      `${this.path}/zipcodessearch/:zipcodestring`,
      authenticationGuard,
      this.getZipCodeSearch
    );
    this.router.get(
      `${this.path}/citydetailsbyzipcode/:zipcode`,
      authenticationGuard,
      this.getcityDetailsByZipCode
    );
    this.router.post(
      `${this.path}/citydetailsbyzipcodes`,
      authenticationGuard,
      this.getcityDetailsByZipCodes
    );
    this.router.post(
      `${this.path}/loadmetadata`,
      authenticationGuard,
      this.loadMetadata
    );
    this.router.post(
      `${this.path}/urlExists`,
      authenticationGuard,
      this.verifyURLExistsOrNot
    );
    this.router.get(
      `${this.path}/tooltips`,
      authenticationGuard,
      this.getTooltips
    );
  }

  private readonly getRoot = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    res.send('Doceree publisher API page');
  }

  private readonly getBanners = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const bannerList: BannerDTO[] = await this.bannerDetailDAO.findAll();
      res.json(bannerList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getCities = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const cityList: CityDetailDTO[] = await this.cityDetailDAO.findAll(req.header('zone'));
      res.json(cityList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getUniqueCities = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    // TODO: why do we have get cities and get unique cities, I observed it at other places as well
    try {
      if (!req.header('zone') || req.header('zone') === null || req.header('zone') === 'null') {
        res.json([]);
      }
      const cityList: CityDetailDTO[] = await this.cityDetailDAO.findUniqueCity(req.header('zone'));
      const cityListArray: string[] = [];
      cityList.map((elem) => cityListArray.push(elem.city));
      res.json(cityListArray);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getUniqueZipCodes = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    // TODO: why do we have get cities and get unique cities, I observed it at other places as well
    try {
      if (!req.header('zone') || req.header('zone') === null || req.header('zone') === 'null') {
        res.json([]);
      }
      const zipCodeList: CityDetailDTO[] = await this.cityDetailDAO.findUniqueCity(req.header('zone'));
      const zipCodeListArray: string[] = [];
      zipCodeList.map((elem) => zipCodeListArray.push(elem.zipcode));
      res.json(zipCodeListArray);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getUniqueStates = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!req.header('zone') || req.header('zone') === null || req.header('zone') === 'null') {
        res.json([]);
      }
      const cityList: CityDetailDTO[] = await this.cityDetailDAO.findByState(req.header('zone'));
      res.json(cityList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getStateByCity = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const cityList: CityDetailDTO[] = await this.cityDetailDAO.findByCity(
        req.params.city,
        req.header('zone')
      );
      res.json(cityList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getUniqueCountries = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const countryList: CityDetailDTO[] = await this.cityDetailDAO.findUniqueCountry(req.header('zone'));
      res.json(countryList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getCitySearch = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const cityList: CityDetailDTO[] = await this.cityDetailDAO.searchCity(
        req.params.citystring,
        req.header('zone')
      );
      res.json(cityList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getcityDetailsList = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const cityDetailsList: CityDetailDTO[] = await this.cityDetailDAO.findByCityName(
        req.params.cityname,
        req.header('zone')
      );
      res.json(cityDetailsList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getUniqueTherapeutic = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const therapeuticList: TherapeuticAreaDTO[] = await this.therapeuticAreaDAO.findUnique();
      res.json(therapeuticList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getAllTherapeutic = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const therapeuticList: TherapeuticAreaDTO[] = await this.therapeuticAreaDAO.findAll();
      res.json(therapeuticList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getAllObjectives = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const objectsList: ObjectivesDTO[] = await this.objectivesDAO.findAll();
      res.json(objectsList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getArchetypes = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const archetypes: ArchetypeDTO[] = await this.archetypeDAO.findAll();
      res.json(archetypes);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getLocationForValidatedHCP = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      // const locations: CityDetailDTO[] = await this.cityDetailDAO.findAllLocations();
      let hcpZips: string[] = [];
      if (req.header('zone') === '1') {
        hcpZips = await this.validatedHcpDAO.findZipCodes();
      } else if (req.header('zone') === '2') {
        hcpZips = await this.cityDetailDAO.findUniqueZipCodesInStarter(req.header('zone'));
      } else {
        throw new Error(`Invalid zone ${req.header('zone')}`);
      }
      const locations: any[] = await this.cityDetailDAO.findLocationByZip(hcpZips, req.header('zone'));
      res.json(locations);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getSpecializationForValidatedHCP = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      let hcpTaxonomies: string[] = []; // await this.validatedHcpDAO.findTaxonomies();
      let taxonomies: any[];
      if (req.header('zone') === '1') {
        hcpTaxonomies = await this.validatedHcpDAO.findTaxonomies();
      } else if (req.header('zone') === '2') {
        hcpTaxonomies = await this.taxonomyDAO.findTaxonomiesInStarter(req.header('zone'));
      } else {
        throw new Error(`Invalid zone ${req.header('zone')}`);
      }
      taxonomies = await this.taxonomyDAO.findSpecializationByTaxonomies(hcpTaxonomies);
      res.json(taxonomies);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getSpecializationForValidatedHCPByTypeAndId = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const hcpIds: number[] = await this.getHcps(req.params.type, req.params.id, req.header('zone'));
      const hcpTaxonomies: string[] = await this.validatedHcpDAO.findTaxonomiesByHcpIds(hcpIds);
      const taxonomies: any[] = await this.taxonomyDAO.findSpecializationByTaxonomies(hcpTaxonomies);
      res.json(taxonomies);
    } catch (err) {
      catchError(err, next);
    }
  }
  private readonly getSpecializationListForValidatedHCPByTypeAndId = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const hcpIds: number[] = await this.getHcps(req.params.type, req.params.id, req.header('zone'));
      const hcpTaxonomies: string[] = await this.validatedHcpDAO.findTaxonomiesByHcpIds(hcpIds);
      const taxonomies: any[] = await this.taxonomyDAO.findSpecializationListByTaxonomies(hcpTaxonomies);
      res.json(taxonomies);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getGenderForValidatedHCPByTypeAndId = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      // TODO: Do we really need to check with DB to get M, F and O?
      const hcpIds: number[] = await this.getHcps(req.params.type, req.params.id, req.header('zone'));
      const hcpGenders: string[] = await this.validatedHcpDAO.findGendersByHcpIds(hcpIds);

      const rootNode: any = { };
      rootNode.text = 'Genders';
      const rootNodeChildren: any[] = [];
      for (const g of hcpGenders) {
        const child: any = { };
        if (g.toUpperCase() === 'F') {
          child.text = 'Female';
          child.value = 'F';
        } else if (g.toUpperCase() === 'M') {
          child.text = 'Male';
          child.value = 'M';
        } else if (g.toUpperCase() === 'O') {
          child.text = 'Other';
          child.value = 'O';
        } else {
          logger.error(`Unknown gender type found [${g}]`);
        }
        rootNodeChildren.push(child);
      }
      rootNode.children = rootNodeChildren;
      res.json(rootNode);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getLocationForValidatedHCPByTypeAndId = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const hcpIds: number[] = await this.getHcps(req.params.type, req.params.id, req.header('zone'));
      const hcpZips: string[] = await this.validatedHcpDAO.findZipCodesByHcpIds(hcpIds);
      const locations: any[] = await this.cityDetailDAO.findLocationByZip(hcpZips, req.header('zone'));
      res.json(locations);
    } catch (err) {
      catchError(err, next);
    }
  }
  private readonly getLocationListForValidatedHCPByTypeAndId = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const hcpIds: number[] = await this.getHcps(req.params.type, req.params.id, req.header('zone'));
      const hcpZips: string[] = await this.validatedHcpDAO.findZipCodesByHcpIds(hcpIds);
      const locations: any[] = await this.cityDetailDAO.findLocationListByZip(hcpZips, req.header('zone'));
      res.json(locations);
    } catch (err) {
      catchError(err, next);
    }
  }

  private async getHcps(type: string, id: string, zone: string): Promise<number[]> {
    let hcpIds: number[] = [];
    if (type.toLowerCase() === 'brand' || type.toLowerCase() === 'campaign' || type.toLowerCase() === 'subcampaign') {
      hcpIds = await this.subcampaignDAO.getHcp(type, id, zone);
    } else if (type.toLowerCase() === 'platform' || type.toLowerCase() === 'asset') {
      hcpIds = await this.assetDAO.getHcp(type, id, zone);
    } else {
      throw new HandledApplicationError(500, 'Unknown type; expected type to be brand, campaign, subcampaign, platform or asset');
    }
    return hcpIds;
  }

  private readonly getZipCodeSearch = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const zipCodeList: CityDetailDTO[] = await this.cityDetailDAO.searchZipeCode(
        req.params.zipcodestring,
        req.header('zone')
      );
      res.json(zipCodeList);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getcityDetailsByZipCode = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const cityDetails: CityDetailDTO = await this.cityDetailDAO.findByZipCode(
        req.params.zipcode,
        req.header('zone')
      );
      res.json(cityDetails);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly getcityDetailsByZipCodes = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const zipCodes: string[] = req.body.zipCodes;
      const cityDetails: CityDetailDTO[] = await this.cityDetailDAO.findByZipCodes(
        zipCodes,
        req.header('zone')
      );
      res.json(cityDetails);
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly loadMetadata = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      this.authorizeAdmin.authorize(req.email);
      await new LoadMetadata().load();
      res.json({ });
    } catch (err) {
      catchError(err, next);
    }
  }

  private readonly verifyURLExistsOrNot = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const isUrl = require('is-valid-http-url');
      if (isUrl(req.body.urlToValidate)) {
        res.status(200).json('true');
      } else {
        res.status(500).json('false');
      }
      /*request.get(req.body.urlToValidate)
      .on('response', (reqRsponse: any) => {
        res.json(reqRsponse);
      })
      .on('error', (reqError: any) => {
        res.json(reqError);
      });*/
    } catch (err) {
      res.json(err);
    }
  }

  private readonly getTooltips = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const tooltips: TooltipDTO[] = await this.tooltipDAO.findAll();
      res.json(tooltips);
    } catch (err) {
      catchError(err, next);
    }
  }

}
