import config from 'config';
import express from 'express';
import mongoose from 'mongoose';
import IController from '../../common/controller-interface';
import OtpDAO from '../../daos/utils/otp.dao';
import OtpDTO from '../../dtos/utils/otp.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import authenticationGuard from '../../guards/authentication.guard';
import { logger } from '../../util/winston';

export class SMSController implements IController {
  public path = '/sms';
  public router = express.Router();
  private readonly otpDao: OtpDAO;
  // private readonly MSG_PART1 = 'Your OTP for phone number verification is: ';
  private readonly MSG_PART2 = ' is your Doceree OTP. Do not share it with anyone.';
  private readonly OTP_LIMIT_PART1 = 'You have ';
  private readonly OTP_LIMIT_PART2 = ' more attempts left.';
  private readonly OTP_LIMIT_EXHAUST = ' more attempts left to enter valid OTP.';

  constructor() {
    this.initializeRoutes();
    this.otpDao = new OtpDAO();
  }

  public verifyPhoneNumber = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      let phNumber: string;
      let otpLimit: string;
      const count: number = await this.otpDao.getOTPsOnCurrentDateForSpecificUserByStatus(req.email, 'InvalidEntry');
      if (count > 2) {
        return res.status(500).json({ message: 'OTP limit exhausted. Please try again later' });
      } else {
        const limit = 2 - count;
        if (limit === 0) {
          return res.status(500).json({ message: 'OTP limit exhausted. Please try again later' });
        }
        otpLimit = this.OTP_LIMIT_PART1 + limit + this.OTP_LIMIT_EXHAUST;
      }
      // check expired OTPs
      await this.otpDao.markPreviousOTPsForPhoneNumberVerificationAsExpired(req.email);

      if (!req.body.phNumber) {
        return res.status(500).json({ message: 'Required parameters missing' });
      } else {
        if (!this.validatePhoneNumber(req.body.phNumber)) {
          return res.status(500).json({ message: 'Invalid Ph Number' });
        } else {
          phNumber = req.body.phNumber;
        }
      }

      // check for existing valid OTPs which are not expired yet
      const existingOTPS: OtpDTO[] = await this.otpDao.pendingOTPsForPhoneNumberVerification(req.email);
      const ids: any[] = [];
      // update existing valid OTPs to Expired
      for (const otpVal of existingOTPS) {
        ids.push(mongoose.Types.ObjectId(otpVal._id));
      }
      await this.otpDao.updateOTPStatus(ids, 'Expired');

      const otp = this.generateOTP();
      const status: string = await this.sendSMSFromClickSend(phNumber, otp);
      if (status === 'success') {
        const otpDTO: OtpDTO = new OtpDTO();
        const currentDate = new Date();
        const expiryTime = new Date(currentDate.getTime() + 15 * 60000);
        otpDTO.expiryTime = expiryTime;
        otpDTO.gateWay = 'ClickSend';
        otpDTO.otpStatus = 'Pending';
        otpDTO.otpChannel = 'SMS';
        otpDTO.purpose = 'phoneNumberVerification';
        otpDTO.email = req.email;
        otpDTO.otp = otp;
        otpDTO.created = { at: currentDate, by: req.email };
        otpDTO.modified = { at: currentDate, by: req.email };
        await this.otpDao.createOTP(otpDTO);
        return res.status(200).json({ message: 'SMS Sent Successfully' });
      } else {
        return res.status(500).json({ message: 'SMS Gateway not available. Please try later' });
      }

    } catch (err) {
      logger.error(JSON.stringify(err));
      catchError(err, next);
    }
  }

  public verifyOTP = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    // check expired OTPS
    await this.otpDao.markPreviousOTPsForPhoneNumberVerificationAsExpired(req.email);
    const currentOTPs: OtpDTO[] = await this.otpDao.pendingOTPsForPhoneNumberVerification(req.email);
    if (!req.body.otpValue) {
      return res.status(500).json({ message: 'Required parameters missing' });
    } else if (!this.validateOTP(req.body.otpValue)) {
      return res.status(500).json({ message: 'Invalid OTP' });
    }
    if (currentOTPs && currentOTPs.length === 1) {
      if (currentOTPs[0].otp === req.body.otpValue) {
        const ids: any[] = [];
        ids.push(mongoose.Types.ObjectId(currentOTPs[0]._id));
        await this.otpDao.updateOTPStatus(ids, 'Verified');
        return res.status(200).json({ message: 'Phone Number Verified Successfully' });
      } else {
        // verify OTP should also have a cap of 3 incorrect OTP
        let otpLimit: string;
        let count: number = await this.otpDao.getOTPsOnCurrentDateForSpecificUserByStatus(req.email, 'Expired');
        if (count > 1) {
          return res.status(500).json({ message: 'OTP limit exhausted. Please try again later' });
        }
        count = await this.otpDao.getOTPsOnCurrentDateForSpecificUserByStatus(req.email, 'InvalidEntry');
        if (count > 2) {
          return res.status(500).json({ message: 'OTP limit exhausted. Please try again later' });
        } else {
          const limit = 2 - count;
          if (limit === 0) {
            return res.status(500).json({ message: 'OTP limit exhausted. Please try again later' });
          }
          otpLimit = this.OTP_LIMIT_PART1 + limit + this.OTP_LIMIT_EXHAUST;
        }
        const ids: any[] = [];
        ids.push(mongoose.Types.ObjectId(currentOTPs[0]._id));
        await this.otpDao.updateOTPStatus(ids, 'InvalidEntry');
        const oldOTP: OtpDTO = await this.otpDao.findById(currentOTPs[0]._id);
        const currentDate = new Date();
        const otpDTO: OtpDTO = new OtpDTO();
        otpDTO.expiryTime = oldOTP.expiryTime;
        otpDTO.gateWay = oldOTP.gateWay;
        otpDTO.otpStatus = 'Pending';
        otpDTO.otpChannel = oldOTP.otpChannel;
        otpDTO.purpose = oldOTP.purpose;
        otpDTO.email = oldOTP.email;
        otpDTO.otp = oldOTP.otp;
        otpDTO.created = { at: currentDate, by: req.email };
        otpDTO.modified = { at: currentDate, by: req.email };
        await this.otpDao.createOTP(otpDTO);
        return res.status(500).json({ message: 'Invalid OTP or OTP Expired', otpLimitMsg: otpLimit });
      }
    } else if (currentOTPs && currentOTPs.length > 1) {
      const ids: any[] = [];
      // update existing OTPs to Expired
      for (const otp of currentOTPs) {
        ids.push(mongoose.Types.ObjectId(otp._id));
      }
      await this.otpDao.updateOTPStatus(ids, 'Expired');
      return res.status(500).json({ message: 'OTP Expired. Generate new OTP' });
    } else {
      return res.status(500).json({ message: 'Invalid Action Performed' });
    }
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/verifyphonenumber`, authenticationGuard, this.verifyPhoneNumber);
    this.router.post(`${this.path}/verifyotp`, authenticationGuard, this.verifyOTP);
  }

  private generateOTP() {
    return Math.floor(100000 + Math.random() * 900000);
  }

  private async sendSMSFromClickSend(phNumber: string, otp: number): Promise<any> {
    const api = require('../../../node_modules/clicksend/api.js');
    const smsApi = new api.SMSApi(config.get<string>('clicksend.username'), config.get<string>('clicksend.hash_key'));
    const smsMessage = new api.SmsMessage();

    smsMessage.source = 'sdk';
    smsMessage.to = phNumber;
    // smsMessage.body = this.MSG_PART1 + otp + this.MSG_PART2;
    smsMessage.body = otp + this.MSG_PART2;
    const smsCollection = new api.SmsMessageCollection();

    smsCollection.messages = [smsMessage];

    return await smsApi.smsSendPost(smsCollection).then((response: any) => {
      return 'success';
    }).catch((err: any) => {
      logger.error(`${err.stack}\n${new Error().stack}`);
      return 'failed';
    });
  }

  private validateOTP(val: string): boolean {
    const otp = val.replace(/[^0-9]/g, '');
    if (otp.length !== 6) {
      return false;
    }
    return true;
  }

  private validatePhoneNumber(val: string): boolean {
    const phoneNumber = val.replace(/[^0-9]/g, '');
    if (phoneNumber.startsWith('1') && phoneNumber.length !== 11) {
      return false;
    } else if (phoneNumber.startsWith('91') && phoneNumber.length !== 12) {
      return false;
    }
    return true;
  }
}
