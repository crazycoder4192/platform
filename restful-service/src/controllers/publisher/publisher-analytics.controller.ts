import express from 'express';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import AnalyticsDAO from '../../daos/publisher/publisher-analytics.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherAnalyticsDTO from '../../dtos/publisher/publisher-analytics.dto';
import FilterAnalyticsDTO from '../../dtos/utils/filter-analytics.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { PublisherPermissions } from '../../util/constants';
import { AuthorizePublisher } from './authorize-publisher';

export class PublisherAnalyticsController {

    private readonly analyticsDAO: AnalyticsDAO;
    private readonly assetDAO: PublisherAssetDAO;
    private readonly authorizePublisher: AuthorizePublisher;
    private readonly publisherPlatformDAO: PublisherPlatformDAO;

    constructor() {
        this.analyticsDAO = new AnalyticsDAO();
        this.assetDAO = new PublisherAssetDAO();
        this.authorizePublisher = new AuthorizePublisher();
        this.publisherPlatformDAO = new PublisherPlatformDAO();
    }

    public getFilterDetailsForAnalyticsByPlatformId = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const authorizePayload = { platformId: req.params.platformId };
            await this.authorizePublisher.authorize(req.email, authorizePayload);
            const filterData: any = await this.analyticsDAO.getFilterDetailsForAnalyticsByPlatformId(req.params.platformId, req.header('zone'));
            res.json(filterData);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getAnalyticsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // let analyticsDTO: PublisherAnalyticsDTO = new PublisherAnalyticsDTO();
            const authorizePayload = { platformId: req.body.platformId, assetId: req.body.assetId };
            await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewAnalytics);

            const filter: FilterAnalyticsDTO = await this.toAnalyticsFilterDTO(req);
            let audienceGenderExperienceData: any;
            let audienceSpecializationData: any;
            let audienceLocationData: any;
            let audienceArcheTypeData: any;

            let overviewTopAdAssetsData = { };
            let overviewAssetTypeAndUtilizationData = { };
            let overviewValidatedHcpsData = { };
            if (req.body.tabType === 'overviewTab') {
                if (filter.assetSnippetIds.length > 0) {
                    overviewTopAdAssetsData = await this.analyticsDAO.getOverviewTopAdAssetsScatterChart(filter, req.header('zone'));
                    overviewAssetTypeAndUtilizationData = await this.analyticsDAO.getOverviewAssetTypeAndUtilizationDonutChart(filter);
                    overviewValidatedHcpsData = await this.analyticsDAO.getOverviewValidatedHcpsDonutChart(filter, req.header('zone'));
                }
                res.json({
                    startDate: filter.startDate,
                    endDate: filter.endDate,
                    overviewTopAdAssets: overviewTopAdAssetsData,
                    overviewAssetTypeAndUtilization: overviewAssetTypeAndUtilizationData,
                    overviewValidatedHcps: overviewValidatedHcpsData
                });
            }
            const timezone: string = req.body.timezone;
            if (req.body.tabType === 'audienceTab') {
                const hcpIds: any[] = await this.analyticsDAO.getAudienceIds(filter, timezone, req.header('zone'));
                if (this.checkNotNULL(hcpIds)) {
                    audienceGenderExperienceData = await this.analyticsDAO.getAudienceGenderExperiencePieChart(hcpIds);
                    audienceSpecializationData = await this.analyticsDAO.getAudienceSpecializationStackBarChart(hcpIds, req.header('zone'));
                    audienceLocationData = await this.analyticsDAO.getAudienceLocationMapChart(hcpIds, req.header('zone'));
                    audienceArcheTypeData = await this.analyticsDAO.getAudienceArcheTypeBarChart(hcpIds, req.header('zone'));
                }
                const audienceAssetTypeData = await this.analyticsDAO.getAudienceAssetType(filter, req.header('zone'));
                res.json({
                    audienceGenderExperience: audienceGenderExperienceData,
                    audienceSpecialization: audienceSpecializationData,
                    audienceLocation: audienceLocationData,
                    audienceArcheType: audienceArcheTypeData,
                    audienceAssetType: audienceAssetTypeData
                });
            }
            if (req.body.tabType === 'behaviorTab') {
                const behaviorTimeOfDayData = await this.analyticsDAO.getBehaviorTimeOfDayHeatChart(filter, timezone, req.header('zone'));
                const behaviorDevicesData = await this.analyticsDAO.getBehaviorDeviceDonutChart(filter, timezone, req.header('zone'));
                res.json({
                    behaviorTimeOfDay: behaviorTimeOfDayData,
                    behaviorDevices: behaviorDevicesData
                });
            }
            if (req.body.tabType === 'conversionTab') {
                const conversionEnagementData = await this.analyticsDAO.getConversionEngagementAreaLineChart(filter, timezone, req.header('zone'));
                const conversionResultsData = await this.analyticsDAO.getConversionResultLineChart(filter, timezone, req.header('zone'));
                res.json({
                    conversionEnagement: conversionEnagementData,
                    conversionResults: conversionResultsData
                });
            }
            // analyticsDTO = this.toAnalyticsDTO(
            //     filter,
            //     overviewTopAdAssets,
            //     overviewAssetTypeAndUtilization,
            //     overviewValidatedHcps,
            //     audienceGenderExperience,
            //     audienceSpecialization,
            //     audienceLocation,
            //     audienceArcheType,
            //     audienceAssetType,
            //     behaviorTimeOfDay,
            //     behaviorDevices,
            //     conversionEnagement,
            //     conversionResults);
            // res.json(analyticsDTO);
        } catch (err) {
            catchError(err, next);
        }
    }

    private async toAnalyticsFilterDTO(
        req: IAuthenticatedRequest,
    ): Promise<FilterAnalyticsDTO> {
        const dto: FilterAnalyticsDTO = new FilterAnalyticsDTO();

        dto.startDate = req.body.startDate;
        dto.endDate = req.body.endDate;

        dto.specialization = req.body.specialization;
        dto.gender = req.body.gender;
        dto.location = req.body.location;

        if (req.body.assetId) {
            dto.assetSnippetIds = [await this.assetDAO.getSnippetIdByAssetId(req.body.assetId)];
            const asset = await this.assetDAO.findById(req.body.assetId);
            if (new Date(dto.startDate) < new Date(asset.created.at)) {
                dto.startDate = asset.created.at;
            }
        } else if (req.body.platformId) {
            dto.platformId = req.body.platformId;
            const doc: string[] = await this.assetDAO.listOfAssetSnippetIdsByPlatformId(req.body.platformId);
            const assetSnippetIds: string[] = [];
            for (const d of doc) {
                assetSnippetIds.push(d);
            }
            dto.assetSnippetIds = assetSnippetIds;
            const publisherPlatform = await this.publisherPlatformDAO.findById(req.body.platformId, req.header('zone'));
            if (new Date(dto.startDate) < new Date(publisherPlatform.created.at)) {
                dto.startDate = publisherPlatform.created.at;
            }
        }
        dto.user = req.body.email;
        return dto;
    }

    private toAnalyticsDTO(filter: any, overviewTopAdAssets: any, overviewAssetTypeAndUtilization: any, overviewValidatedHcps: any,
                           audienceGenderExperience: any, audienceSpecialization: any, audienceLocation: any, audienceArcheType: any,
                           audienceAssetType: any, behaviorTimeOfDay: any, behaviorDevices: any, conversionEnagement: any,
                           conversionResults: any): PublisherAnalyticsDTO {
            const dto: PublisherAnalyticsDTO = new PublisherAnalyticsDTO();
            dto.overviewTopAdAssets = overviewTopAdAssets;
            dto.overviewAssetTypeAndUtilization = overviewAssetTypeAndUtilization;
            dto.overviewValidatedHcps = overviewValidatedHcps;
            dto.audienceGenderExperience = audienceGenderExperience;
            dto.audienceSpecialization = audienceSpecialization;
            dto.audienceLocation = audienceLocation;
            dto.audienceArcheType = audienceArcheType;
            dto.audienceAssetType = audienceAssetType;
            dto.behaviorTimeOfDay = behaviorTimeOfDay;
            dto.behaviorDevices = behaviorDevices;
            dto.conversionEnagement = conversionEnagement;
            dto.conversionResults = conversionResults;
            dto.startDate = filter.startDate;
            dto.endDate = filter.endDate;
            return dto;
    }
    private checkNotNULL(data: any) {
        if (data instanceof Array) {
            if (data.length > 0) {
               return true;
              }
            return false;
        } else if (data instanceof Object) {
            if (data && (Object.keys(data).length !== 0)) {
                return true;
            }
            return false;
        }
    }

}
