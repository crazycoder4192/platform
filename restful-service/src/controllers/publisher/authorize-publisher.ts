
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import PublisherAssetDTO from '../../dtos/publisher/asset.dto';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import HandledApplicationError from '../../error/handled-application-error';
import { logger } from '../../util/winston';

export class AuthorizePublisher {
    private readonly publisherDAO: PublisherDAO;
    private readonly publisherUserDAO: PublisherUserDAO;
    private readonly platformDAO: PublisherPlatformDAO;
    private readonly assetDAO: PublisherAssetDAO;

    constructor() {
        this.publisherDAO = new PublisherDAO();
        this.publisherUserDAO = new PublisherUserDAO();
        this.platformDAO = new PublisherPlatformDAO();
        this.assetDAO = new PublisherAssetDAO();
    }

    public async authorize(email: string, authorizePayload: any, ...permission: string[]) {
        // check if this email belongs to publisher
        let publisherUserDTOs: PublisherUserDTO[] = await this.publisherUserDAO.findAllByEmail(email);

        if (publisherUserDTOs.length === 0) {
            throw new HandledApplicationError(500, `No publisher with the email [${email}] exists`);
        }
        if (publisherUserDTOs.length > 1) {
            throw new HandledApplicationError(500, `Did not expect to find multiple publisher users for email [${email}]`);
        }
        const assignedPlatformIds: string[] = publisherUserDTOs[0].platforms.map((x: any) => x.platformId.toString());
        if (assignedPlatformIds.length === 0) {
            throw new HandledApplicationError(500, 'No platform is associated with this user');
        }

        // check if the publisher user is associated with this platform
        let platformId: string = '';
        if (authorizePayload.platformId) {
            platformId = authorizePayload.platformId;
        }

        if (platformId) {
            if (!assignedPlatformIds.includes(platformId.toString())) {
                logger.error(`User [${email}] do not have access to this platform [${platformId}]`);
                throw new HandledApplicationError(500, `User [${email}] do not have access to this platform`);
            }

            // check if the user has appropriate permission
            publisherUserDTOs = await this.publisherUserDAO.checkPlatformPermission(email, platformId, permission);
            if (!publisherUserDTOs || publisherUserDTOs.length === 0) {
                logger.error(`User [${email}] does not have permission to access this platform [${platformId}]`);
                throw new HandledApplicationError(500, `User [${email}] does not have permission to access this platform`);
            } else if (publisherUserDTOs.length > 2) {
                logger.error(`User [${email}] has more than one publisher user associated`);
            }
        } else {
            if (authorizePayload.publisherId) {
                // the flow here is a bit twisted. Here we are chekcing if this  permission
                // is assigned to any platform, then we are allowing the user to have right on it.
                // though not completely right, we should use this very rarely.
                publisherUserDTOs = await this.publisherUserDAO.checkPublisherPermission(email, authorizePayload.publisherId, permission);
                if (!publisherUserDTOs || publisherUserDTOs.length === 0) {
                    logger.error(`User [${email}] does not have permission to access this publisher [${authorizePayload.publisherId}]`);
                    throw new HandledApplicationError(500, `User [${email}] does not have permission to access this publisher`);
                } else if (publisherUserDTOs.length > 2) {
                    logger.error(`User [${email}] has more than one publisher user associated`);
                }
            }
        }
    }

    /*public async authorize(email: string, authorizePayload: any) {
        // check if this email belongs to advertiser
        // check if the advertiser user is associated with this brand
        let publisherId: string = '';
        if (authorizePayload.publisherId) {
            publisherId = authorizePayload.publisherId;
        } else if (authorizePayload.platformId) {
            const result: PublisherPlatformDTO = await this.platformDAO.findById(authorizePayload.platformId);
            if (result) {
                publisherId = result.publisherId.toString();
            } else {
                logger.error(`Unable to find publisher corresponding to this platform [${authorizePayload.platformId}]`);
                throw new HandledApplicationError(500, 'Unable to find publisher corresponding to this platform');
            }
        } else if (authorizePayload.assetId) {
            const result: PublisherAssetDTO = await this.assetDAO.findById(authorizePayload.assetId);
            if (result) {
                publisherId = result.publisherId.toString();
            } else {
                logger.error(`Unable to find publisher corresponding to this platform [${authorizePayload.assetId}]`);
                throw new HandledApplicationError(500, 'Unable to find publisher corresponding to this platform');
            }
        }

        if (publisherId) {
            const publisher = await this.publisherDAO.findById(publisherId);
            if (publisher.user.email !== email) {
                logger.error(`User [${email}] do not have access to this publisher [${publisherId}]`);
                throw new HandledApplicationError(500, `User [${email}] do not have access to this publisher`);
            }
        } else {
            logger.error(`Unable to determine the publisherId from platform/asset [${authorizePayload.platformId}], [${authorizePayload.assetId}]`);
            throw new HandledApplicationError(500, 'Unable to determine the publisherId from platform/asset ');
        }
    }*/

    public async authorizeAndReturnPublisherDTO(email: string): Promise<PublisherDTO> {
        const publisherDTO: PublisherDTO = await this.publisherDAO.findByEmail(email);
        if (!publisherDTO) {
            logger.error(`No publisher profile present for the logged in user [${email}]`);
            throw new HandledApplicationError(500, 'No publisher profile present for the logged in user');
        }
        return publisherDTO;
    }
}
