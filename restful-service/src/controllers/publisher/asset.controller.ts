import config from 'config';
import express from 'express';
import moment from 'moment';
import uniqid from 'uniqid';
import AdvertiserUserDAO from '../../daos/advertiser/advertiser-user.dao';
import IncomingHcpRequestDetailsDAO from '../../daos/incoming-hcp-request-details.dao';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import AuditVerifiedAssetDAO from '../../daos/publisher/audit-verified-asset.doa';
import DashboardDAO from '../../daos/publisher/dashboard.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherSiteTypeDAO from '../../daos/publisher/publisher-site-type.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import PublisherAssetDTO from '../../dtos/publisher/asset.dto';
import AuditVerifiedAssetDTO from '../../dtos/publisher/audit-verified-asset.dto';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherSiteTypeDTO from '../../dtos/publisher/publisher-site-type.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { RedisHelper } from '../../redis/redis-helper';
import { AdvertiserPermissions, PublisherPermissions } from '../../util/constants';
import { HashPwd } from '../../util/hash-pwd';
import { GeneralMail } from '../../util/mail/general-mail';
import { PublisherMail } from '../../util/mail/publisher-mail';
import { logger } from '../../util/winston';
import { AuthorizeAdvertiser } from '../advertiser/authorize-advertiser';
import { AuthorizeAdmin } from './../admin/authorize-admin';
import { AuthorizePublisher } from './authorize-publisher';
const dateformat = require('dateformat');
const escapeHtml = require('escape-html');

export class AssetController {
  private readonly assetDAO: PublisherAssetDAO;
  private readonly platformDAO: PublisherPlatformDAO;
  private readonly publisherDAO: PublisherDAO;
  private readonly publisherUserDAO: PublisherUserDAO;
  private readonly authorizePublisher: AuthorizePublisher;
  private readonly authorizeAdvertiser: AuthorizeAdvertiser;
  private readonly advertiserUserDAO: AdvertiserUserDAO;
  private readonly userDAO: UserDAO;
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly hashPwd: HashPwd;
  private readonly publisherMail: PublisherMail;
  private readonly incomingHcpRequestDetailsDAO: IncomingHcpRequestDetailsDAO;
  private readonly siteTypeDAO: PublisherSiteTypeDAO;
  private readonly auditVerifiedAssetDAO: AuditVerifiedAssetDAO;
  private readonly userTokenDAO: UserTokenDAO;
  private readonly generalMail: GeneralMail;
  private readonly redisHelper: RedisHelper;
  private readonly dashboardDAO: DashboardDAO;

  constructor() {
    this.assetDAO = new PublisherAssetDAO();
    this.platformDAO = new PublisherPlatformDAO();
    this.publisherDAO = new PublisherDAO();
    this.hashPwd = new HashPwd();
    this.publisherUserDAO = new PublisherUserDAO();
    this.authorizePublisher = new AuthorizePublisher();
    this.authorizeAdvertiser = new AuthorizeAdvertiser();
    this.advertiserUserDAO = new AdvertiserUserDAO();
    this.userDAO = new UserDAO();
    this.publisherMail = new PublisherMail();
    this.incomingHcpRequestDetailsDAO = new IncomingHcpRequestDetailsDAO();
    this.authorizeAdmin = new AuthorizeAdmin();
    this.siteTypeDAO = new PublisherSiteTypeDAO();
    this.auditVerifiedAssetDAO = new AuditVerifiedAssetDAO();
    this.userTokenDAO = new UserTokenDAO();
    this.generalMail = new GeneralMail();
    this.redisHelper = new RedisHelper();
    this.dashboardDAO = new DashboardDAO();
  }

  public getAssetDetails = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (req && req.query && req.query.id) {
        try {
          const authorizePayload = { assetId: req.query.id };
          await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewAsset);
          const asset: PublisherAssetDTO = await this.assetDAO.findById(
            req.query.id,
          );
          res.json(asset);
        } catch (err) {
          catchError(err, next);
        }
      } else {
        try {
          const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
          if (publisherUser) {
            const watchingPlatform = publisherUser.platforms.find((thisPlatform: any) =>
            (thisPlatform.isWatching === true && thisPlatform.zone === req.header('zone')));
            if (watchingPlatform) {
              const authorizePayload = { platformId: watchingPlatform.platformId };
              await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewAsset);
              const assetAll: PublisherAssetDTO[] = await this.assetDAO.findAll(publisherUser.publisherId,
                watchingPlatform.platformId, req.header('zone'));
              res.json(assetAll);
            } else {
              logger.error(`No watching platform for profile [${publisherUser.publisherId}] and user [${req.email}]`);
              throw new HandledApplicationError(500, 'No watching platform');
            }
          } else {
            logger.error(`No publisher profile present for the logged in user [${req.email}]`);
            throw new HandledApplicationError(500, 'No publisher profile present for the logged in user');
          }
        } catch (err) {
          catchError(err, next);
        }
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public getBannerSizesForExistedAssets = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      // this api is accessed by both publishers and advertiser
      const publisherDTO: PublisherDTO = await this.publisherDAO.findByEmail(req.email);
      if (!publisherDTO) {
        const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
        const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true);
        const authorizePayload: any = { brandId: watchingBrand.brandId };
        await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.addCampaign);
      }
      const assetAll: PublisherAssetDTO[] = await this.assetDAO.getBannerSizes();
      res.json(assetAll);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createAsset = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const authorizePayload = { platformId: req.body.platformId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.addAsset);
      // TODO: Check if all the fields are provided
      const dto: PublisherAssetDTO = await this.toPublisherAsset(req);
      dto.created = { at: new Date(), by: req.email };

      const asset = await this.assetDAO.create(dto);
      await this.redisHelper.updateAsset(asset);
      res.json(asset);

    } catch (err) {
      catchError(err, next);
    }
  }

  public updateAsset = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      let authorizePayload = { assetId: req.body._id };
      if (req.body.id) {
        authorizePayload = { assetId: req.body.id };
      }
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.editAsset);

      const dto: PublisherAssetDTO = await this.toPublisherAsset(req);
      if (req.body.id) {
        dto._id = req.body.id;
      } else if (req.body._id) {
        dto._id = req.body._id;
      }
      const prevDto: PublisherAssetDTO = await this.assetDAO.findById(dto._id);
      // restore the value of isVerified and isDeleted. isVerified and isDeleted
      // are not set via this update process
      // dto.isVerified = prevDto.isVerified;
      // shravan: committed above line so that the user can deactivate and activate the assets
      dto.deleted = prevDto.deleted;
      dto.modified = { at: new Date(), by: req.email };
      const asset = await this.assetDAO.update(dto._id, dto);
      if (asset.isVerified) {
        await this.auditVerifiedAssetDAO.create(this.toAuditDTO(req.email, asset));
      }
      await this.redisHelper.updateAsset(asset);

      // this signifies it transition from active to deactive
      if (prevDto.assetStatus === 'Active' && dto.assetStatus === 'Inactive') {
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
        if (toUser.hasPersonalProfile) {
          if (toUser.isSubscribedForEmail) {
            this.publisherMail.sendAssetsDeactivateMail(
              req.email,
              toUser.firstName,
              dto.name,
              url, // FeedbackFormLink
              `${url}/publisher/assetCreation`, // assetsRestoreLink
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }
      res.json(asset);
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteAsset = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const authorizePayload = { assetId: req.params.id };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.deleteAsset);

      const dto: PublisherAssetDTO = await this.assetDAO.findById(req.params.id);

      dto._id = req.params.id;
      dto.deleted = true;
      dto.assetStatus = 'Deleted';
      dto.modified = { at: new Date(), by: req.email };

      const totalReachResult = await this.dashboardDAO.getReach([dto.codeSnippet], req.header('zone'));
      if (totalReachResult && totalReachResult.length > 0) {
        if (totalReachResult[0].hcpCount > 0) {
          throw new HandledApplicationError(500, 'Cannot delete this asset as it has transactional data associated with it.');
        }
      }

      await this.assetDAO.delete(req.params.id, dto);
      if (dto.isVerified) {
        await this.redisHelper.updateAsset(dto);
      }

      const url: string = config.get<string>('mail.web-portal-redirection-url');
      const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
      if (toUser.hasPersonalProfile) {
        if (toUser.isSubscribedForEmail) {
          this.publisherMail.sendAssetsRemovedMail(
            req.email,
            toUser.firstName,
            dto.name,
            `${url}/publisher/assetCreation`, // buttonToRemoveAssetLink
            `${url}/publisher/assetCreation`, // assetsRestoreLink
            url, // FeedbackFormLink
            `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
          );
        }
      }
      res.json({ message: 'Slot deleted successfully', success: true });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getAssetsByPlatformId = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const authorizePayload = { platformId: req.params.platformId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewAsset, PublisherPermissions.viewAnalytics);

      const assetAll: PublisherAssetDTO[] = await this.assetDAO.getAssetsByPlatformId(req.params.platformId);
      res.json(assetAll);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getActiveAssetsbyPlatformId = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const authorizePayload = { platformId: req.params.platformId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewAsset);

      const assetAll: PublisherAssetDTO[] = await this.assetDAO.getActiveAssetsByPlatformId(req.params.platformId);
      res.json(assetAll);
    } catch (err) {
      catchError(err, next);
    }
  }

  public checkAssetNameAvailability = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const watchingPlatform = publisherUser.platforms.find((thisPlatform: any) =>
        (thisPlatform.isWatching === true && thisPlatform.zone === req.header('zone')));
      const authorizePayload = { platformId: watchingPlatform.platformId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.addAsset, PublisherPermissions.editAsset);
      const detail: PublisherAssetDTO[] = await this.assetDAO.checkUniqueAssetNameWithUser(
        req.body.name, watchingPlatform.platformId);
      if (detail.length > 0) {
        return res.status(500).json({ type: 'Error', message: 'Asset name not available' });
      } else {
        return res.status(200).json({ type: 'Success', message: 'Asset name available' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public noAssets = async (
    req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const platforms: PublisherPlatformDTO[] = await this.platformDAO.findAllActivePlatforms(req.header('zone'));
      const currentDate: Date = new Date();

      const platformIdsFromAssetTable: any[] = await this.assetDAO.findAllPlatforms();
      const mailSentToUsers: string[] = [];
      for (const a of platforms) {
        const id = a._id.toString();
        if (!platformIdsFromAssetTable.find((x) => x.toString() === id)) {
          const user = await this.userDAO.findActiveUserByEmail(a.created.by);
          if (user) {
            let lastMailSentAt: Date = a.created.at;
            let noOfTimesReminderSent: number = 0;
            if (user.reminder && user.reminder.noAsset) {
              lastMailSentAt = user.reminder.noAsset.mailSentAt;
              if (!lastMailSentAt) {
                lastMailSentAt = a.created.at;
              }
              if (user.reminder.noAsset.noOfMailsSent) {
                noOfTimesReminderSent = user.reminder.noAsset.noOfMailsSent;
              }
            }
            if (noOfTimesReminderSent < 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
                this.sendNoAssetReminderMail(user, currentDate, a.name, a.created.at, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else if (noOfTimesReminderSent === 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
                this.sendNoAssetReminderMail(user, currentDate, a.name, a.created.at, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            }
          }
        }
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to users: ${mailSentToUsers}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public oneAsset = async (
    req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const platforms: PublisherPlatformDTO[] = await this.platformDAO.findAllActivePlatforms(req.header('zone'));
      const currentDate: Date = new Date();
      const mailSentToUsers: string[] = [];
      for (const p of platforms) {
        const assets: any[] = await this.assetDAO.findAllAssetsByPlatformId(p._id);
        if (assets.length === 1) {
          const a = assets[0];
          const user = await this.userDAO.findActiveUserByEmail(a.created.by);
          let lastMailSentAt: Date = a.created.at;
          let noOfTimesReminderSent: number = 0;
          if (user) {
            if (user.reminder && user.reminder.oneAsset) {
              lastMailSentAt = user.reminder.oneAsset.mailSentAt;
              if (!lastMailSentAt) {
                lastMailSentAt = a.created.at;
              }
              if (user.reminder.oneAsset.noOfMailsSent) {
                noOfTimesReminderSent = user.reminder.oneAsset.noOfMailsSent;
              }
            }
            if (noOfTimesReminderSent < 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
                this.sendOnlyOneAssetReminderMail(user, currentDate, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else if (noOfTimesReminderSent === 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
                this.sendOnlyOneAssetReminderMail(user, currentDate, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else {
              // dont do anything; we dont have to send mail post 4 reminders
            }
          }
        }
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to users: ${mailSentToUsers}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getSuggestedBidRange = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const authorizePayload = { platformId: req.params.platformId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.addAsset, PublisherPermissions.editAsset);

      const validatedHcpCount: number =
        await this.incomingHcpRequestDetailsDAO.countByPlatformId(req.params.platformId, req.header('zone'));
      const siteType: string =
        await this.platformDAO.findSiteTypeById(req.params.platformId, req.header('zone'));

      const siteTypes: PublisherSiteTypeDTO[] = await this.siteTypeDAO.findAll();
      const weightage: number = siteTypes.find((x) => x.siteType.toLowerCase() === siteType.toLowerCase()).weightage;

      let multiplier: number = 0;
      if (validatedHcpCount <= 10000) {
        multiplier = 0.3;
      } else if (validatedHcpCount > 10000 && validatedHcpCount <= 25000) {
        multiplier = 0.55;
      } else if (validatedHcpCount > 25000 && validatedHcpCount <= 50000) {
        multiplier = 0.8;
      } else if (validatedHcpCount > 50000 && validatedHcpCount <= 100000) {
        multiplier = 1;
      } else if (validatedHcpCount > 100000) {
        multiplier = 1.2;
      }

      const minCPC: number = 75;
      const maxCPC: number = 225;
      const minCPM: number = 300;
      const maxCPM: number = 900;

      return res.status(200).json({
        suggestedMinCPC: minCPC * weightage * multiplier,
        suggestedMaxCPC: maxCPC * weightage * multiplier,
        suggestedMinCPM: minCPM * weightage * multiplier,
        suggestedMaxCPM: maxCPM * weightage * multiplier
      });

    } catch (err) {
      catchError(err, next);
    }
  }

  public sendInvitationAndAdIntegrationCode = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const sender: UserDTO = await this.userDAO.findByEmail(req.email);
      const assetDTO: PublisherAssetDTO = await this.assetDAO.findById(req.body.assetId);
      const platformDTO: PublisherPlatformDTO = await this.platformDAO.findById(assetDTO.platformId.toString(), req.header('zone'));
      const url: string = config.get<string>('mail.web-portal-redirection-url');
      let tokenStr: string;
      const jwtExpiresInVal = config.get<string>('jwt.expiresIn');
      const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
      tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);
      const tokenDTO: UserTokenDTO = new UserTokenDTO();
      tokenDTO.email = req.body.email;
      tokenDTO.expiryTime = new Date(expiresIn);
      tokenDTO.token = tokenStr;
      tokenDTO.tokenType = 'addUser';
      tokenDTO.created = { at: new Date(), by: req.email };
      await this.userTokenDAO.create(tokenDTO);
      if (platformDTO.platformType.toLowerCase() === 'website') {
        let content: string = assetDTO.codeSnippet;
        content = escapeHtml(content);
        await this.publisherMail.sendInvitationAndAdIntegrationCode(
          req.body.email,
          sender.firstName,
          platformDTO.name,
          `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
          content
        );
      }

      if (platformDTO.platformType.toLowerCase() === 'mobileapp') {
        let snippet1: string = assetDTO.mobileASISnippet1;
        let snippet2: string = assetDTO.mobileASISnippet2;
        let snippet3: string = assetDTO.mobileASISnippet3;

        snippet1 = escapeHtml(snippet1);
        snippet2 = escapeHtml(snippet2);
        snippet3 = escapeHtml(snippet3);

        await this.publisherMail.sendInvitationAndMobileAdIntegrationCode(
          req.body.email,
          sender.firstName,
          platformDTO.name,
          `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
          snippet1,
          snippet2,
          snippet3
        );
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public sendAdIntegrationCode = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
      try {
        const sender: UserDTO = await this.userDAO.findByEmail(req.email);
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        const assetDTO: PublisherAssetDTO = await this.assetDAO.findById(req.body.assetId);
        const platformDTO: PublisherPlatformDTO = await this.platformDAO.findById(assetDTO.platformId.toString(), req.header('zone'));
        if (platformDTO.platformType.toLowerCase() === 'website') {
          let content: string = assetDTO.codeSnippet;
          content = escapeHtml(content);
          await this.publisherMail.sendAdIntegrationCode(
            req.body.email,
            sender.firstName,
            platformDTO.name,
            `${url}/login`,
            content
          );
        }

        if (platformDTO.platformType.toLowerCase() === 'mobileapp') {
          let snippet1: string = assetDTO.mobileASISnippet1;
          let snippet2: string = assetDTO.mobileASISnippet2;
          let snippet3: string = assetDTO.mobileASISnippet3;

          snippet1 = escapeHtml(snippet1);
          snippet2 = escapeHtml(snippet2);
          snippet3 = escapeHtml(snippet3);

          await this.publisherMail.sendMobileAdIntegrationCode(
            req.body.email,
            sender.firstName,
            platformDTO.name,
            `${url}/login`,
            snippet1,
            snippet2,
            snippet3
          );
        }

        return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
      } catch (err) {
          catchError(err, next);
      }
  }

  private async toPublisherAsset(
    req: IAuthenticatedRequest,
  ): Promise<PublisherAssetDTO> {
    const dto = new PublisherAssetDTO();
    const platformDetail: PublisherPlatformDTO = await this.platformDAO.findById(
      req.body.platformId,
      req.header('zone')
    );
    const contentType =
      req.body.assetType === 'Banner'
        ? config.get<string>('adServerScript.contentTypeBanner')
        : config.get<string>('adServerScript.contentTypeVideo');
    const snippetId = uniqid('DOC_');
    dto.assetDimensions = req.body.assetDimensions
      ? req.body.assetDimensions
      : '';

    dto.assetSize = req.body.assetSize ? req.body.assetSize : null;
    dto.assetUnit = req.body.assetUnit ? req.body.assetUnit : '';
    dto.assetStatus = req.body.assetStatus ? req.body.assetStatus : '';
    dto.isVerified = req.body.isVerified ? req.body.isVerified : false;
    dto.assetType = req.body.assetType ? req.body.assetType : '';
    dto.fileFormats = req.body.fileFormats ? req.body.fileFormats : '';
    dto.fullUrl = req.body.fullUrl ? req.body.fullUrl : '';
    dto.name = req.body.name ? req.body.name : '';
    dto.platformId = platformDetail;
    dto.platformType = platformDetail.platformType;
    dto.publisherId = req.body.publisherId
      ? req.body.publisherId
      : platformDetail.publisherId;
    dto.url = req.body.url ? req.body.url : '';
    if (await this.changeCodeSnippet(req)) {
      if (dto.platformType.toLowerCase() === 'website') {
        const codeSnippet = this.createCodeSnippet(
          req.body.assetDimensions,
          contentType, snippetId
        );
        dto.codeSnippet = req.body.codeSnippet ? req.body.codeSnippet : codeSnippet;
      }
      if (dto.platformType.toLowerCase() === 'mobileapp') {
        const mobileASISnippet1 = this.mobileASISnippet1(
          req.body.assetDimensions,
          snippetId
        );
        dto.mobileASISnippet1 = req.body.mobileASISnippet1 ? req.body.mobileASISnippet1 : mobileASISnippet1;

        const mobileASISnippet2 = this.mobileASISnippet2(
          req.body.assetDimensions,
          snippetId
        );
        dto.mobileASISnippet2 = req.body.mobileASISnippet2 ? req.body.mobileASISnippet2 : mobileASISnippet2;

        const mobileASISnippet3 = this.mobileASISnippet3(
          req.body.assetDimensions,
          snippetId
        );
        dto.mobileASISnippet3 = req.body.mobileASISnippet3 ? req.body.mobileASISnippet3 : mobileASISnippet3;
      }

      dto.codeSnippetId = snippetId;
      dto.assetStatus = 'New';
      dto.isVerified = false;
    }
    const bidval = req.body.bidRange;
    dto.bidRange = {
      cpc: {
        min: +bidval.cpc.min,
        max: +bidval.cpc.max
      },
      cpm: {
        min: +bidval.cpm.min,
        max: +bidval.cpm.max
      }
    };
    dto.zone = req.header('zone');
    dto.addedSlotScriptOption = req.body.addedSlotScriptOption;
    return dto;
  }

  private createCodeSnippet(dimension: string, contentType: string,
                            snippetId: string): string {
    let urlStr = `${config.get<string>('adServerScript.hostname')}`;
    urlStr += ':' + `${config.get<string>('adServerScript.port')}` + '/';
    urlStr += `${config.get<string>('adServerScript.script_path')}`;
    return `<div id='${snippetId}'>
    <script type='text/javascript'>
    var docCont={contet_id:'${snippetId}',
    content_sizes:['${dimension}'],
    content_type:'${contentType}'};
    </script>
    <script type='text/javascript'
    src="${urlStr}">
    </script>
    </div>`;
  }

  private mobileASISnippet1(dimension: string, snippetId: string): string {
    return `<com.doceree.androidadslibrary.ads.DocereeAdView
            android:id="@+id/adsView_${snippetId}"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_alignParentBottom="true"
            ads:adUnitId="${snippetId}"
            ads:adSize="${dimension}"/>
        `;
  }

  private mobileASISnippet2(dimension: string, snippetId: string): string {
    return `
        DocereeAdView adView = new DocereeAdView(this);
        adView.setAdSlotId("${snippetId}");
        adView.setAdSize("${dimension}");`;
  }

  private mobileASISnippet3(dimension: string, snippetId: string): string {
    return `package ...
    import androidx.appcompat.app.AppCompatActivity;
    ...
    public class DisplayAdsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_display_ads);
    final AdRequest adRequest = new AdRequest.AdRequestBuilder().build();
    DocereeAdView docereeAdView = findViewById(R.id.ad_view_banner);
    docereeAdView.loadAd(adRequest);
    }
    }`;
  }

  private async changeCodeSnippet(req: IAuthenticatedRequest): Promise<boolean> {
    if (req.body._id) {
      const asset: PublisherAssetDTO = await this.assetDAO.findById(req.body._id);
      if (asset.url !== req.body.url) {
        return true;
      } else if (asset.assetDimensions !== req.body.assetDimensions) {
        return true;
      }
      return false;
    }
    return true;
  }

  private async sendNoAssetReminderMail(user: UserDTO, currentDate: Date,
                                        platformName: string, platformCreatedAt: Date,
                                        noOfTimesReminderSent: number) {
    const url: string = config.get<string>('mail.web-portal-redirection-url');
    if (user.hasPersonalProfile) {
      if (user.isSubscribedForEmail) {
        const date = dateformat(platformCreatedAt, 'mmmm dS, yyyy');
        await this.publisherMail.sendNoAssetReminderMail(
          user.email,
          user.firstName,
          platformName,
          date,
          `${url}/publisher/assetCreation`,
          `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
        );
      }
    }
    user.reminder.noAsset.mailSentAt = currentDate;
    user.reminder.noAsset.noOfMailsSent = noOfTimesReminderSent + 1;
    this.userDAO.update(user._id, user);
  }

  private async sendOnlyOneAssetReminderMail(user: UserDTO, currentDate: Date,
                                             noOfTimesReminderSent: number) {
    const url: string = config.get<string>('mail.web-portal-redirection-url');
    if (user.hasPersonalProfile) {
      if (user.isSubscribedForEmail) {
        await this.publisherMail.sendAddedJustOneAssetsAddMoreAssetsMail(
          user.email,
          user.firstName,
          `${url}/publisher/assetCreation`,
          `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
        );
      }
    }
    user.reminder.oneAsset.mailSentAt = currentDate;
    user.reminder.oneAsset.noOfMailsSent = noOfTimesReminderSent + 1;
    this.userDAO.update(user._id, user);
  }

  private toAuditDTO(email: string, dto: PublisherAssetDTO): AuditVerifiedAssetDTO {
    const auditDTO = new AuditVerifiedAssetDTO();
    auditDTO.assetId = dto._id.toString();
    auditDTO.deleted = dto.deleted;
    auditDTO.isVerified = dto.isVerified;
    auditDTO.assetDimensions = dto.assetDimensions;

    auditDTO.assetSize = dto.assetSize;
    auditDTO.assetUnit = dto.assetUnit;
    auditDTO.assetStatus = dto.assetStatus;
    auditDTO.assetType = dto.assetType;
    auditDTO.fileFormats = dto.fileFormats;
    auditDTO.fullUrl = dto.fullUrl;
    auditDTO.name = dto.name;
    auditDTO.codeSnippetId = dto.codeSnippetId;
    auditDTO.codeSnippet = dto.codeSnippet;
    auditDTO.assetStatus = dto.assetStatus;
    auditDTO.bidRange = dto.bidRange;

    if (dto.created) {
      auditDTO.created = dto.created;
    }
    if (dto.modified) {
      auditDTO.assetModified = dto.modified;
    }
    auditDTO.created = { at: new Date(), by: email };
    auditDTO.zone = dto.zone;
    auditDTO.addedSlotScriptOption = dto.addedSlotScriptOption;
    return auditDTO;
  }
}
