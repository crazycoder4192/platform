import config from 'config';
import express from 'express';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { UserStatuses, UserTypes } from '../../util/constants';
import { GeneralMail } from '../../util/mail/general-mail';
import { logger } from '../../util/winston';
import { AuthorizeAdmin } from '././../admin/authorize-admin';
import { AuthorizePublisher } from './authorize-publisher';

export class ProfileController {
  public router = express.Router();
  private readonly publisherDAO: PublisherDAO;
  private readonly userDAO: UserDAO;
  private readonly authorizePublisher: AuthorizePublisher;
  private readonly authorizeAdmin: AuthorizeAdmin;
  private readonly generalMail: GeneralMail;
  private readonly userTokenDAO: UserTokenDAO;

  constructor() {
    this.publisherDAO = new PublisherDAO();
    this.authorizePublisher = new AuthorizePublisher();
    this.authorizeAdmin = new AuthorizeAdmin();
    this.userDAO = new UserDAO();
    this.generalMail = new GeneralMail();
    this.userTokenDAO = new UserTokenDAO();
  }

  public createProfile = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      // TODO: validate the inputs for publisher profile
      const user: UserDTO = await this.userDAO.findByEmail(req.email);
      if (user.userType.toLowerCase() === UserTypes.publisher.toLowerCase()
          && user.userStatus.toLowerCase() === UserStatuses.active.toLowerCase()) {
            const dto: PublisherDTO = this.toPublisherDTO(req);
            dto.created = { at: new Date(), by: req.email };
            const result = await this.publisherDAO.create(dto);
            res.json(result);
      } else {
        logger.error(`Logged in user[${req.email}] is not a publisher`);
        throw new HandledApplicationError(500, 'Logged in user is not a publisher');
      }

    } catch (err) {
      catchError(err, next);
    }
  }

  public updateProfile = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      // TODO: validate the inputs for publisher profile
      const dto: PublisherDTO = this.toPublisherDTO(req);
      dto._id = req.body.id;
      dto.modified = { at: new Date(), by: req.email };
      const result = await this.publisherDAO.update(req.body.id, dto);
      res.json(result);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getProfileByEmail = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const details: PublisherDTO = await this.publisherDAO.findByEmail(req.email);

      if (details) {
        res.status(200).json(details);
      } else {
        res.status(200).json(null);
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public incompleteProfiles = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      await this.authorizeAdmin.authorize(req.email);
      const usersFromUserTable: UserDTO[] = await this.userDAO.findActiveUserTypes('publisher');
      const usersFromPublisherTables: any[] = await this.publisherDAO.findUsers();
      const url: string = config.get<string>('mail.web-portal-redirection-url');

      const currentDate: Date = new Date();
      const userEmailsFromPublisherTables: string[] = usersFromPublisherTables.map((x) => x.email);
      const mailSentToUsers: string[] = [];
      for (const user of usersFromUserTable) {
        if (!userEmailsFromPublisherTables.includes(user.email)) {
          if (user.reminder.profileCompletionPending) {
            let lastMailSentAt: Date = user.reminder.profileCompletionPending.mailSentAt;
            if (!lastMailSentAt) {
              lastMailSentAt = user.created.at;
            }
            let noOfTimesReminderSent: number = 0;
            if (user.reminder.profileCompletionPending.noOfMailsSent) {
              noOfTimesReminderSent = user.reminder.profileCompletionPending.noOfMailsSent;
            }
            if (noOfTimesReminderSent < 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
                this.sendProfileCompletionPendingReminderPublisherMail(user, `${url}/publisher/publisherProfile`,
                currentDate, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else if (noOfTimesReminderSent === 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
                this.sendProfileCompletionPendingReminderPublisherMail(user, `${url}/publisher/publisherProfile`,
                currentDate, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else {
              // dont do anything; we dont have to send mail post 4 reminders
            }
          }
        }
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to users: ${mailSentToUsers}` });
    } catch (err) {
      catchError(err, next);
    }

  }

  private toPublisherDTO(req: IAuthenticatedRequest): PublisherDTO {
    const payload: PublisherDTO = new PublisherDTO();
    payload.acceptTerms = req.body.acceptTerms;
    payload.address = { addressLine1: req.body.addressLine1, addressLine2: req.body.addressLine2,
                        city: req.body.city, state: req.body.state, zipcode: req.body.zipCode,
                        country: req.body.country};
    payload.organizationName = req.body.organization;
    payload.user = { email : req.body.email };
    payload.phoneNumber = req.body.countryCodePhNumber + req.body.phoneNumber;
    payload.isPhNumberVerified = req.body.isPhNumberVerified;
    return payload;
  }

  private async sendProfileCompletionPendingReminderPublisherMail(user: UserDTO, url: string, currentDate: Date,
                                                                  noOfTimesReminderSent: number) {
    const timeDifference = Math.abs(currentDate.getTime() - user.created.at.getTime());
    const noOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
    if (user.isSubscribedForEmail) {
      await this.generalMail.sendReminderToCompleteProfilePublisher(
        user.email,
        noOfDays,
        url,
        `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
      );
    }
    user.reminder.profileCompletionPending.mailSentAt = currentDate;
    user.reminder.profileCompletionPending.noOfMailsSent = noOfTimesReminderSent + 1;
    this.userDAO.update(user._id, user);
  }
}
