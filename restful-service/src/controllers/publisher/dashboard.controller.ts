import config from 'config';
import express from 'express';
import mongoose from 'mongoose';
import { Helper } from '../../common/helper';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import DashboardDAO from '../../daos/publisher/dashboard.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserDAO from '../../daos/user.dao';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { PublisherPermissions } from '../../util/constants';
import { logger } from '../../util/winston';
import { AuthorizePublisher } from './authorize-publisher';

export class DashboardController {
  private readonly assetDAO: PublisherAssetDAO;
  private readonly dashboardDAO: DashboardDAO;
  private readonly publisherDAO: PublisherDAO;
  private readonly userDAO: UserDAO;
  private readonly platFormDAO: PublisherPlatformDAO;
  private readonly publisherUserDAO: PublisherUserDAO;
  private readonly authorizePublisher: AuthorizePublisher;
  private readonly helper: Helper;

  constructor() {
    this.dashboardDAO = new DashboardDAO();
    this.publisherDAO = new PublisherDAO();
    this.assetDAO = new PublisherAssetDAO();
    this.platFormDAO = new PublisherPlatformDAO();
    this.publisherUserDAO = new PublisherUserDAO();
    this.userDAO = new UserDAO();
    this.authorizePublisher = new AuthorizePublisher();
    this.helper = new Helper();
  }

  public getDashboardDetails = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const authorizePayload = { publisherId: publisherUser.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewDashboard);

      const thisPublisherUser: PublisherUserDTO = await this.publisherUserDAO.findByEmail(req.email);
      const zonePlatforms: any[] = thisPublisherUser.platforms.filter((thisPlatform: any) => thisPlatform.zone === req.header('zone'));
      const setUniquePlatformIds: Set<string> = new Set(zonePlatforms.map((x) => x.platformId.toString()));
      let uniquePlatformIds: any[] = [...setUniquePlatformIds];
      const inputPlatformId = req.params.platformId;
      if (inputPlatformId) {
        uniquePlatformIds = uniquePlatformIds.find((x) => (x === inputPlatformId));
      }
      const platformWiseData: PublisherPlatformDTO[] = await this.platFormDAO.findNonDeletedByIdsAndZone(uniquePlatformIds, req.header('zone'));

      const userData = await this.userDAO.findByEmail(req.email);
      const logOutTime = this.helper.sanitizeLastLogoutTime(userData);
      const returnObj: any = { };
      const codeSnippetIds: string[] = [];
      returnObj.publisherId = publisherUser.publisherId;
      let earnings = 0; let impressions = 0; let reach = 0;
      let totalLastLogOutEarnings = 0; let totalLastLogOutImpressions = 0; let totalLastLogOutreach = 0;
      let increasedEarnings = 0; let increasedImpressions = 0; let increasedReach = 0;
      if (platformWiseData && platformWiseData.length > 0) {
        const platformsTemp = [];
        for (const platForm of platformWiseData) {
          const assetDetails = await this.assetDAO.findAllAssetsByPlatformId(platForm._id);
          let assetTemp = [];
          if (assetDetails && assetDetails.length > 0) {
            for (const asset of assetDetails) {
              codeSnippetIds.push(asset.codeSnippetId);
              const assetAll = await this.dashboardDAO.findByAssetId(asset.codeSnippetId, req.header('zone'));
              let assetEarnings = 0; let assetImpressions = 0; let assetClicks = 0; let assetViews = 0;
              let assetCTR = 0; let assetCPC = 0; let assetCPM = 0; let assetCPV = 0;
              if (assetAll && assetAll.length > 0) {
                earnings += +(assetAll[0].earnings * config.get<number>('docereeProfitCalculationValue') / 100);
                impressions += +assetAll[0].impressions;
                reach += +assetAll[0].reachCount;
                assetEarnings = (assetAll[0].earnings * config.get<number>('docereeProfitCalculationValue') / 100);
                assetImpressions = assetAll[0].impressions; assetClicks = assetAll[0].clicks;
                assetViews = assetAll[0].views;
                assetCTR = assetAll[0].CTR;
                assetCPC = (assetAll[0].CPC * config.get<number>('docereeProfitCalculationValue') / 100);
                assetCPM = (assetAll[0].CPM * config.get<number>('docereeProfitCalculationValue') * 1000 / 100);
                assetCPV = (assetAll[0].CPV * config.get<number>('docereeProfitCalculationValue') / 100);
              }
              assetTemp.push({
                assetId: asset._id, assetName: asset.name, assetType: asset.assetType,
                earnings: assetEarnings, impressions: assetImpressions, clicks: assetClicks,
                views: assetViews, CTR: assetCTR, CPC: assetCPC, CPM: assetCPM, CPV: assetCPV
              });

              const assetAllLogOut = await this.dashboardDAO.findByAssetId(asset.codeSnippetId, req.header('zone'), logOutTime);
              if (assetAllLogOut && assetAllLogOut.length > 0) {
                assetAllLogOut.forEach((element: any) => {
                  totalLastLogOutEarnings += (element.earnings * config.get<number>('docereeProfitCalculationValue') / 100);
                  totalLastLogOutImpressions += element.Impressions;
                  totalLastLogOutreach += element.reachCount;
                });
              }
              if (totalLastLogOutEarnings && earnings) {
                increasedEarnings = ((earnings - totalLastLogOutEarnings) / earnings) * 100;
              }
              if (totalLastLogOutImpressions && impressions) {
                increasedImpressions = ((impressions - totalLastLogOutImpressions) / impressions) * 100;
              }
              if (totalLastLogOutreach && reach) {
                increasedReach = ((reach - totalLastLogOutreach) / reach * 100);
              }
              increasedImpressions = increasedEarnings;
              increasedEarnings = increasedImpressions;
              increasedReach = increasedReach;
            }
          }
          if (assetTemp.length > 0) {
            assetTemp.sort((a, b) => (a.earnings < b.earnings) ? 1 : -1);
            if (assetTemp.length > 8) {
              assetTemp = assetTemp.slice(0, 8);
            }
          }
          const platFormObj = { platformId: platForm._id, platformName: platForm.name, openState: false, assets: assetTemp };
          platformsTemp.push(platFormObj);
        }

        const totalReachResult = await this.dashboardDAO.getReach(codeSnippetIds, req.header('zone'));
        const totalLastLogoutReachResult = await this.dashboardDAO.getReach(codeSnippetIds, req.header('zone'), logOutTime);
        let totalReach: number = 0;
        let toalLastLogoutReach: number = 0;
        if (totalReachResult && totalReachResult.length > 0) {
          totalReach = totalReachResult[0].hcpCount;
        }
        if (totalLastLogoutReachResult && totalLastLogoutReachResult.length > 0) {
          toalLastLogoutReach = totalLastLogoutReachResult[0].hcpCount;
        }
        increasedReach = ((totalReach - toalLastLogoutReach) / reach * 100);

        returnObj.earnings = { total: earnings, performanceChange: increasedEarnings };
        returnObj.impressions = { total: impressions, performanceChange: increasedImpressions };
        returnObj.reach = { total: totalReach, performanceChange: increasedReach };
        returnObj.platforms = platformsTemp;
      }
      res.json(returnObj);
    } catch (err) {
      catchError(err, next);
    }
  }

  public analyticsCard = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      let platformId; let codeSnippetId; let startDate; let endDate;
      if (req && req.body && req.body.platformId) {
        platformId = req.body.platformId;
        const authorizePayload = { platformId: req.body.platformId };
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewAnalytics);
      }
      if (req && req.body && req.body.assetId) {
        const authorizePayload = { assetId: req.body.assetId };
        await this.authorizePublisher.authorize(req.email, authorizePayload);
        codeSnippetId = await this.assetDAO.getSnippetIdByAssetId(req.body.assetId);
      }
      if (req && req.body && req.body.startDate) {
        startDate = req.body.startDate;
      }
      if (req && req.body && req.body.endDate) {
        endDate = req.body.endDate;
      }

      const returnObj: any = { };
      let earnings = 0; let impressions = 0; let reach = 0; let clicks = 0; let views = 0;
      let totalLastLogOutEarnings = 0; let totalLastLogOutImpressions = 0; let totalLastLogOutreach = 0;
      let totalLastLogOutCpc = 0; let totalLastLogOutCpm = 0; let totalLastLogOutCpv = 0; let totalLastLogOutCtr = 0;
      let totalLastLogOutClicks = 0; let totalLastLogOutViews = 0;
      let increasedEarnings = 0; let increasedImpressions = 0; let increasedClicks = 0; let increasedReach = 0;
      let increasedCpc = 0; let increasedCpm = 0; let increasedCpv = 0; let increasedCtr = 0; let increasedViews = 0;
      let cpc = 0; let cpm = 0; let cpv = 0; let ctr = 0;
      // Commenting these lines as we don't need this data for analytics: Dt:10JAN2020 - Shravan
    /*  const assetAll = await this.dashboardDAO.findAnalyticsForAsset(codeSnippetId, platformId);
      if (assetAll && assetAll.length > 0) {
        earnings += +(assetAll[0].earnings * config.get<number>('docereeProfitCalculationValue') / 100);
        impressions += +assetAll[0].impressions;
        reach += +assetAll[0].reachCount;
        clicks = assetAll[0].clicks;
        views = assetAll[0].views;
        ctr = assetAll[0].CTR;
        cpc = (assetAll[0].CPC * config.get<number>('docereeProfitCalculationValue') / 100);
        cpm = (assetAll[0].CPM * config.get<number>('docereeProfitCalculationValue') * 1000 / 100);
        cpv = (assetAll[0].CPV * config.get<number>('docereeProfitCalculationValue') / 100);
      } */
      const userData = await this.userDAO.findByEmail(req.body.email);
      let logOutTime = null;
      if (!endDate) {
        logOutTime = this.helper.sanitizeLastLogoutTime(userData);
      }
      const assetAllLogOut = await this.dashboardDAO.findAnalyticsForAsset(codeSnippetId, platformId,
        startDate, endDate, logOutTime, req.header('zone'));
      if (assetAllLogOut && assetAllLogOut.length > 0) {
        assetAllLogOut.forEach((element: any) => {
          totalLastLogOutEarnings += (element.earnings * config.get<number>('docereeProfitCalculationValue') / 100);
          totalLastLogOutImpressions += element.impressions;
          totalLastLogOutClicks += element.clicks;
          totalLastLogOutViews += element.views;
          totalLastLogOutreach += element.reachCount;
          totalLastLogOutCtr = element.CTR;
          totalLastLogOutCpc += (element.CPC * config.get<number>('docereeProfitCalculationValue') / 100);
          totalLastLogOutCpv += (element.CPV * config.get<number>('docereeProfitCalculationValue') / 100);
          totalLastLogOutCpm += (element.CPM * config.get<number>('docereeProfitCalculationValue') / 100);
        });
      }
     // Fix for PLAT-626 Dt:10JAN2020 - Shravan
      if (!logOutTime) {
        earnings = totalLastLogOutEarnings;
        impressions = totalLastLogOutImpressions;
        clicks = totalLastLogOutClicks;
        views = totalLastLogOutViews;
        reach = totalLastLogOutreach;
        clicks = clicks;
        ctr = totalLastLogOutCtr;
        cpc = totalLastLogOutCpc;
        cpm = totalLastLogOutCpm;
        cpv = totalLastLogOutCpv;
      }

      if (totalLastLogOutEarnings && earnings) {
        increasedEarnings = ((earnings - totalLastLogOutEarnings) / earnings) * 100;
      }
      if (totalLastLogOutImpressions && impressions) {
        increasedImpressions = ((impressions - totalLastLogOutImpressions) / impressions) * 100;
      }
      if (totalLastLogOutClicks && clicks) {
        increasedClicks = ((clicks - totalLastLogOutClicks) / clicks) * 100;
      }
      if (totalLastLogOutViews && views) {
        increasedViews = ((views - totalLastLogOutViews) / views) * 100;
      }
      if (totalLastLogOutreach && reach) {
        increasedReach = ((reach - totalLastLogOutreach) / reach * 100);
      }
      if (totalLastLogOutCpc && cpc) {
        increasedCpc = ((cpc - totalLastLogOutCpc) / cpc * 100);
      }
      if (totalLastLogOutCpm && cpm) {
        increasedCpm = ((cpm - totalLastLogOutCpm) / cpm * 100);
      }
      if (totalLastLogOutCpv && cpv) {
        increasedCpv = ((cpv - totalLastLogOutCpv) / cpv * 100);
      }
      if (totalLastLogOutCtr && ctr) {
        increasedCtr = ((ctr - totalLastLogOutCtr) / ctr * 100);
      }

      increasedImpressions = increasedEarnings;
      increasedEarnings = increasedImpressions;
      increasedReach = increasedReach;
      returnObj.earnings = { total: earnings, performanceChange: increasedEarnings };
      returnObj.impressions = { total: impressions, performanceChange: increasedImpressions };
      returnObj.clicks = { total: clicks, performanceChange: increasedClicks };
      returnObj.views = { total: views, performanceChange: increasedViews };
      returnObj.reach = { total: reach, performanceChange: increasedReach };
      returnObj.cpc = { total: cpc, performanceChange: increasedCpc };
      returnObj.cpm = { total: cpm, performanceChange: increasedCpm };
      returnObj.cpv = { total: cpv, performanceChange: increasedCpv };
      returnObj.ctr = { total: ctr, performanceChange: increasedCtr };
      res.json(returnObj);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getPerformanceAnalyticsData = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const filter: any = {
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        publisherId: req.body.publisherId,
        platformId: req.body.platformId,
        publisherCreationDate: new Date()
      };
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const authorizePayload = { publisherId: publisherUser.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewDashboard);
      const publisherDTO: PublisherDTO = await this.publisherDAO.findById(publisherUser.publisherId);
      const zonePlatforms: any[] = publisherUser.platforms.filter((thisPlatform: any) => thisPlatform.zone === req.header('zone'));
      const platformIds: Set<string> = new Set(zonePlatforms.map((x) => x.platformId.toString()));
      filter.platformIds = [...platformIds].map((x) => mongoose.Types.ObjectId(x));

      if (publisherDTO && publisherDTO._id.toString() === req.body.publisherId) {
        const timezone: string = req.body.timezone;
        filter.publisherCreationDate = publisherDTO.created.at;
        const performanceResults = await this.dashboardDAO.getPerformanceAnalyticsForDashboard(filter,
          timezone, req.header('zone'));
        res.json(performanceResults);
      } else {
        logger.error(`Publisher ID [${req.body.pubilsherId}] passed in request param does not
          belong to logged in publisher [${req.email}]`);
        throw new HandledApplicationError(500, 'Publisher ID passed in request body does not belong to logged in publisher');
      }
    } catch (err) {
      catchError(err, next);
    }
  }
}
