import config from 'config';
import express from 'express';
import MongoClient from 'mongodb';
import IncomingHcpRequestDetailsDAO from '../../daos/incoming-hcp-request-details.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import Publisher from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { NotificationIcon, NotificationRoute, NotificationType, PlatformHCPValidationStatus } from '../../util/constants';
import { PublisherMail } from '../../util/mail/publisher-mail';
import { NotificationHelper } from '../../util/notification-helper';
import { AuthorizeAdmin } from '../admin/authorize-admin';
import { AuthorizePublisher } from './authorize-publisher';

export class PublisherTransformedHCPController {

    private readonly transformedPublisherHcpDAO: IncomingHcpRequestDetailsDAO;
    private readonly publisherDAO: PublisherDAO;
    private readonly platformDAO: PublisherPlatformDAO;
    private readonly authorizePublisher: AuthorizePublisher;
    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly notificationHelper: NotificationHelper;
    private readonly publisherMail: PublisherMail;
    private readonly promiseClient: Promise<MongoClient.MongoClient>;

    constructor() {
        this.transformedPublisherHcpDAO = new IncomingHcpRequestDetailsDAO();
        this.publisherDAO = new PublisherDAO();
        this.platformDAO = new PublisherPlatformDAO();
        this.authorizePublisher = new AuthorizePublisher();
        this.authorizeAdmin = new AuthorizeAdmin();
        this.notificationHelper = new NotificationHelper();
        this.publisherMail = new PublisherMail();
        this.promiseClient = MongoClient.connect(config.get<string>('raw-publisher.connection-string'));
    }

    public notifyHcpValidationStatus = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            const result: any = await this.getUploadStatus(req.params.platformId);
            const platformDTO: PublisherPlatformDTO = await this.platformDAO.findById(req.params.platformId, req.header('zone'));
            const publisherDTO: Publisher = await this.publisherDAO.findById(platformDTO.publisherId.toString());
            const email = publisherDTO.user.email;
            if (result.state === PlatformHCPValidationStatus.uploadPending) {
                const notificationIcon = NotificationIcon.error;
                const notificationMessage = `No HCP data uploaded for ${platformDTO.name}. Please do that to complete platform creation.`;
                this.notificationHelper.addNotification([email], notificationMessage, notificationIcon,
                    NotificationType.platform, req.params.platformId, NotificationRoute.publisher_manage_platform);
            }
            if (result.state === PlatformHCPValidationStatus.verificationFailed) {
                const notificationIcon = NotificationIcon.error;
                const notificationMessage = `HCP verification unsucessful for ${platformDTO.name}. Please check your email for next steps.`;
                this.notificationHelper.addNotification([email], notificationMessage, notificationIcon,
                    NotificationType.platform, req.params.platformId, NotificationRoute.publisher_manage_platform);
            }
            if (result.state === PlatformHCPValidationStatus.verified) {
                const notificationIcon = NotificationIcon.success;
                const notificationMessage = `HCP Verification is now complete for ${platformDTO.name} and your platform has been added`;
                if (platformDTO.hcpScript && platformDTO.hcpScript.trim().length > 0) {
                    // TODO: check if user has not already created an asset
                    this.notificationHelper.addNotification([email], notificationMessage, notificationIcon,
                        NotificationType.platform, req.params.platformId, NotificationRoute.publisher_manage_platform);
                } else {
                    throw new HandledApplicationError(500, 'HCP script not added yet, cannot notifiy user about successful HCP validation');
                }
            }
            res.status(200).json({ message: result.msg, status: result.state });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getValidatedHcpCounts = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            /*const authorizePayload = { platformId: req.params.platformId };
            await this.authorizePublisher.authorize(req.email, authorizePayload);

            const result: any = await this.getUploadStatus(req.params.platformId);*/
            res.status(200).json({ });
        } catch (err) {
            catchError(err, next);
        }
    }

    private async getUploadStatus(platformId: string) {
        /*let totalRecordsUploaded: number;
        const client = await this.promiseClient;
        const db = client.db((config.get<string>('raw-publisher.db-name')));
        totalRecordsUploaded = await db.collection(`publisher-${platformId}`).count();
        const totalTransformed: TransformedPublisherHcpDTO[] = await this.transformedPublisherHcpDAO.findByPlatformId(platformId);
        const totalMatched: TransformedPublisherHcpDTO[] = await this.transformedPublisherHcpDAO.findMatchedByPlatformId(platformId);*/
        const msg: string = '';
        const state: string = '';
        /*if (totalRecordsUploaded === 0) {
            msg = 'Uploaded doctors verification is pending';
            state = PlatformHCPValidationStatus.uploadPending;
        } else {
            if (totalTransformed.length === 0) {
                msg = 'Doctor verification could not be initiated due to bad data. We will get back to you soon with the details over email.';
                state = PlatformHCPValidationStatus.verificationFailed;
            } else {
                if (totalMatched.length === 0) {
                    msg = 'None of the doctors provided could be validated. We will get back to you soon with more details over email.';
                    state = PlatformHCPValidationStatus.verificationFailed;
                } else {
                    msg = `${totalMatched.length} of ${totalTransformed.length} doctors validated successfully`;
                    state = PlatformHCPValidationStatus.verified;
                }
            }
        }*/
        return { msg, state };
    }
}
