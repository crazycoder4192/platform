import express from 'express';
import PublisherBankDetailDAO from '../../daos/publisher/bank-detail.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserDAO from '../../daos/user.dao';
import PublisherBankDetailDTO from '../../dtos/publisher/bank-detail.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { PublisherPermissions } from '../../util/constants';
import { Encryption } from '../../util/crypto';
import { logger } from '../../util/winston';
import { QuickbooksHelper } from '../util/quickbooks-helper';
import { AuthorizePublisher } from './authorize-publisher';
import { PlatformController } from './platform.controller';

export class BankDetailController {
  private readonly bankDetailDAO: PublisherBankDetailDAO;
  private readonly publisherDAO: PublisherDAO;
  private readonly platformDAO: PublisherPlatformDAO;
  private readonly publisherUserDAO: PublisherUserDAO;
  private readonly userDAO: UserDAO;
  private readonly authorizePublisher: AuthorizePublisher;
  private readonly encryption: Encryption;
  private readonly quickbooksHelper: QuickbooksHelper;

  constructor() {
    this.bankDetailDAO = new PublisherBankDetailDAO();
    this.publisherDAO = new PublisherDAO();
    this.platformDAO = new PublisherPlatformDAO();
    this.publisherUserDAO = new PublisherUserDAO();
    this.userDAO = new UserDAO();
    this.authorizePublisher = new AuthorizePublisher();
    this.encryption = new Encryption();
    this.quickbooksHelper = new QuickbooksHelper();
  }

  public getAllBankDetails = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      if (req && req.params && req.params.id) {
        const bankDetails: PublisherBankDetailDTO = await this.bankDetailDAO.findById(req.params.id, req.header('zone'));
        if (bankDetails.publisherId.toString() !== publisherUser.publisherId.toString()) {
          logger.error(`Bank ID [${req.params.id}] passed in request param does not
          belong to logged in publisher [${req.email}]`);
          throw new HandledApplicationError(500, 'Bank ID passed in request param does not belong to logged in publisher');
        }
        const authorizePayload = { publisherId:  publisherUser.publisherId.toString() };
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentMethod);
        bankDetails.accountNumber = this.hideAccountNumber(bankDetails.accountNumber);
        return res.json(bankDetails);
      } else {
        const bankDetails: PublisherBankDetailDTO[] = await this.bankDetailDAO.findAll(publisherUser.publisherId, req.header('zone'));
        bankDetails.forEach((detail) => {
          detail.accountNumber = this.hideAccountNumber(detail.accountNumber);
        });
        return res.json(bankDetails);
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public getBankDetailsById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const bankDetails: PublisherBankDetailDTO = await this.bankDetailDAO.findById(req.params.id, req.header('zone'));
      if (bankDetails.publisherId.toString() !== publisherUser.publisherId.toString()) {
        logger.error(`Bank ID [${req.params.id}] passed in request param does not
        belong to logged in publisher [${req.email}]`);
        throw new HandledApplicationError(500, 'Bank ID passed in request param does not belong to logged in publisher');
      }
      const authorizePayload = { publisherId:  publisherUser.publisherId.toString() };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentMethod);
      bankDetails.accountNumber = this.hideAccountNumber(bankDetails.accountNumber);
      return res.json(bankDetails);
    } catch (err) {
      catchError(err, next);
    }
  }
  public getAllBankDetailsByPlatformId = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const authorizePayload = { platformId: req.params.platformId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentMethod);
      const platform: PublisherBankDetailDTO[] = await this.bankDetailDAO.findByPlatformId(req.params.platformId, req.header('zone'));
      res.json(platform);
    } catch (err) {
      catchError(err, next);
    }
  }

  public createBankDetail = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      for (const p of req.body.platforms) {
        const authorizePayload = { platformId: p };
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.addPaymentMethod);

        // check if the platform is already associated with a bank account
        const b: PublisherBankDetailDTO[] = await this.bankDetailDAO.findByPlatformId(p, req.header('zone'));
        if (b && b.length > 0) {
          const platformList = await this.platformDAO.findByIds(req.body.platforms, req.header('zone'));
          let platformNames: string;
          platformNames = '';
          platformList.forEach((plat) => {
            const findedPlatform = b[0].platforms.find((obj) => obj.toString() === plat._id.toString());
            if (findedPlatform) {
              platformNames = `${platformNames} ${plat.name}`;
            }
          });
          return res.status(200).json({ success: false, message: `Platform ${platformNames} is already associated with a payment method` });
        }
      }
      if (req.body.accountHolderName) {
        req.body.accountHolderName = req.body.accountHolderName.trim();
      }
      if (req.body.bankName) {
        req.body.bankName = req.body.bankName.trim();
      }
      if (req.body.accountNumber) {
        req.body.accountNumber = req.body.accountNumber.trim();
      }
      if (req.body.accountType) {
        req.body.accountType = req.body.accountType.trim();
      }
      if (req.body.swiftCode) {
        req.body.swiftCode = req.body.swiftCode.trim();
      }
      if (req.body.taxNumber) {
        req.body.taxNumber = req.body.taxNumber.trim();
      }
      if (req.body.accountHolderAddress) {
        req.body.accountHolderAddress = req.body.accountHolderAddress.trim();
      }
      if (req.body.accountHolderCIN) {
        req.body.accountHolderCIN = req.body.accountHolderCIN.trim();
      }
      if (req.body.accountHolderGSTIN) {
        req.body.accountHolderGSTIN = req.body.accountHolderGSTIN.trim();
      }
      if (req.body.bankAddress) {
        req.body.bankAddress = req.body.bankAddress.trim();
      }
      if (req.body.ifsc) {
        req.body.ifsc = req.body.ifsc.trim();
      }
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const bankDetailDTO: PublisherBankDetailDTO = this.toModelConvert(req);
      bankDetailDTO.publisherId = publisherUser.publisherId;
      bankDetailDTO.created = { at: new Date(), by: req.email };
      const encryptedAccNumber = this.encryption.encrypt(bankDetailDTO.accountNumber);
      const checkDetails: PublisherBankDetailDTO[] = await this.bankDetailDAO.checkAccountNumberAvailability(encryptedAccNumber,
                  bankDetailDTO.publisherId, req.header('zone'));
      if (checkDetails.length > 0) {
        return res.status(200).json({ success: false, message: 'Account number already exists' });
      } else {
        await this.bankDetailDAO.create(bankDetailDTO, req.header('zone'));

        try {
          // create quickbooks vendor
          const publisherDTO = await this.publisherDAO.findById(publisherUser.publisherId);
          const u = await this.userDAO.findByEmail(publisherDTO.user.email);
          const vendor: any = { };
          vendor.organizationName = publisherDTO.organizationName;
          vendor.email = req.email;
          vendor.firstName = u.firstName;
          vendor.lastName = u.lastName;
          vendor.phoneNumber = publisherDTO.phoneNumber;
          vendor.addressLine1 = publisherDTO.address.addressLine1;
          vendor.city = publisherDTO.address.city;
          vendor.state = publisherDTO.address.state;
          vendor.country = publisherDTO.address.country;
          const vendorDetails = await this.quickbooksHelper.findVendorByEmail(vendor.email, req.header('zone'));
          if (!vendorDetails) {
              await this.quickbooksHelper.createVendor(vendor, req.header('zone'));
          }
        } catch (err) {
          logger.error(`${err.stack}\n${new Error().stack}`);
        }

        return res.status(200).json({ success: true, message: 'Account details added successfully' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public updateBankDetail = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      let bankDetailDTO: PublisherBankDetailDTO = await this.bankDetailDAO.findById(req.params.id, req.header('zone'));

      if (bankDetailDTO.publisherId.toString() !== publisherUser.publisherId.toString()) {
        logger.error(`Bank ID [${req.params.id}] passed in request param does not
        belong to logged in publisher [${req.email}]`);
        return res.status(200).json({ status: false, message: 'Bank ID passed in request param does not belong to logged in publisher' });
      }

      for (const p of req.body.platforms) {
        const authorizePayload = { platformId: p };
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.editPaymentMethod);

        // check if the platform is already associated with a bank account
        const b: PublisherBankDetailDTO[] = await this.bankDetailDAO.findByPlatformId(p, req.header('zone'));
        if (b && b.length > 0) {
          if (b[0]._id.toString() !== bankDetailDTO._id.toString()) {
            const plat = await this.platformDAO.findById(p, req.header('zone'));
            return res.status(200).json({ status: false, message: `Platform ${plat.name} is already associated with a payment method` });
          }
        }
      }

      bankDetailDTO = this.toModelConvertForUpdate(req);
      bankDetailDTO.publisherId = publisherUser.publisherId;
      bankDetailDTO.modified = { at: new Date(), by: req.email };
      const details = await this.bankDetailDAO.findById(req.params.id, req.header('zone'));
      bankDetailDTO.accountNumber = details.accountNumber;
      /*const encryptedAccNumber = this.encryption.encrypt(bankDetailDTO.accountNumber);
      const checkDetails: PublisherBankDetailDTO[] = await this.bankDetailDAO.checkAccountNumberAvailability(encryptedAccNumber,
        bankDetailDTO.publisherId);
      if (checkDetails.length > 0) {
        res.status(500).json({ message: 'Account number already exists' });
      } else {
        this.bankDetailDAO.update(req.params.id, bankDetailDTO);
        res.status(200).json({ success: true, message: 'Account updated successfully' });
      }*/
      this.bankDetailDAO.update(req.params.id, bankDetailDTO, req.header('zone'));
      return res.status(200).json({ success: true, message: 'Account updated successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  public deleteBankDetail = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const bankDetailDTO: PublisherBankDetailDTO = await this.bankDetailDAO.findById(req.params.id, req.header('zone'));
      if (bankDetailDTO.publisherId.toString() !== publisherUser.publisherId.toString()) {
        logger.error(`Bank ID [${req.params.id}] passed in request param does not
        belong to logged in publisher [${req.email}]`);
        throw new HandledApplicationError(500, 'Bank ID passed in request param does not belong to logged in publisher');
      }
      for (const p of bankDetailDTO.platforms) {
        const authorizePayload = { platformId: p };
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.editPaymentMethod);
      }

      bankDetailDTO.deleted = true;
      bankDetailDTO.modified = { at: new Date(), by: req.email };
      await this.bankDetailDAO.delete(req.params.id, bankDetailDTO, req.header('zone'));
      res.status(200).json({ success: true, message: 'Account details deleted successfully' });
    } catch (err) {
      catchError(err, next);
    }
  }

  private hideAccountNumber(accNumber: string) {
      const xValueLength = accNumber.length - 4;
      let xValue = '';
      for (let i = 0; i < xValueLength; i++) {
        xValue += 'X';
      }
      return accNumber.substring(0, 4) + xValue;
  }

  private toModelConvert(
    req: IAuthenticatedRequest,
  ): PublisherBankDetailDTO {
    const bankDetailDTO = this.toModelConvertForUpdate(req);
    bankDetailDTO.accountNumber = req.body.accountNumber;
    return bankDetailDTO;
  }

  // account number should not be updated.
  private toModelConvertForUpdate(
    req: IAuthenticatedRequest,
  ): PublisherBankDetailDTO {
    const bankDetailDTO = new PublisherBankDetailDTO();
    bankDetailDTO.bankName = req.body.bankName;
    bankDetailDTO.bankCountry = req.body.bankCountry;
    bankDetailDTO.accountHolderName = req.body.accountHolderName;
    bankDetailDTO.accountType = req.body.accountType;
    bankDetailDTO.swiftCode = req.body.swiftCode;
    bankDetailDTO.taxNumber = req.body.taxNumber;
    bankDetailDTO.publisherId = req.body.publisherId;
    bankDetailDTO.platforms = req.body.platforms;
    if (req.headers.zone === '2') {
      bankDetailDTO.bankAddress = req.body.bankAddress;
      bankDetailDTO.ifsc = req.body.ifsc;
      bankDetailDTO.accountHolderAddress = req.body.accountHolderAddress;
      bankDetailDTO.accountHolderCIN = req.body.accountHolderCIN;
      bankDetailDTO.accountHolderGSTIN = req.body.accountHolderGSTIN;
    }
    return bankDetailDTO;
  }

}
