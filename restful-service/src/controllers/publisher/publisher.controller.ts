import express from 'express';
import IController from '../../common/controller-interface';
import authenticationGuard from '../../guards/authentication.guard';
import { AssetController } from './asset.controller';
import { BankDetailController } from './bank-detail.controller';
import { BillController } from './bill.controller';
import { DashboardController } from './dashboard.controller';
import { PlatformController } from './platform.controller';
import { ProfileController } from './profile.controller';
import { PublisherAnalyticsController } from './publisher-analytics.controller';
import { PublisherTransformedHCPController } from './publisher-transformed-hcp.controller';
import { PublisherUserController } from './publisher-user.controller';

export class PublisherController implements IController {
  public path = '/publisher';
  public router = express.Router();
  private readonly profileController: ProfileController;
  private readonly platformController: PlatformController;
  private readonly assetController: AssetController;
  private readonly bankDetailsController: BankDetailController;
  private readonly dashboardController: DashboardController;
  private readonly billController: BillController;
  private readonly publisherAnalyticsController: PublisherAnalyticsController;
  private readonly publisherTransformedHCPController: PublisherTransformedHCPController;
  private readonly publisherUserController: PublisherUserController;

  constructor() {
    this.profileController = new ProfileController();
    this.platformController = new PlatformController();
    this.assetController = new AssetController();
    this.bankDetailsController = new BankDetailController();
    this.dashboardController = new DashboardController();
    this.billController = new BillController();
    this.publisherAnalyticsController = new PublisherAnalyticsController();
    this.publisherTransformedHCPController = new PublisherTransformedHCPController();
    this.publisherUserController = new PublisherUserController();
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, authenticationGuard, this.getRoot);

    this.router.post(
      `${this.path}/profile/email`,
      authenticationGuard,
      this.profileController.getProfileByEmail,
    );
    this.router.put(
      `${this.path}/profile`,
      authenticationGuard,
      this.profileController.updateProfile,
    );
    this.router.post(
      `${this.path}/profile`,
      authenticationGuard,
      this.profileController.createProfile,
    );
    this.router.get(
      `${this.path}/profile/reminder/incompleteprofile`,
      authenticationGuard,
      this.profileController.incompleteProfiles,
    );

    this.router.get(
      `${this.path}/appTypes`,
      authenticationGuard,
      this.platformController.getAppTypes,
    );
    this.router.get(
      `${this.path}/unique/appTypes`,
      authenticationGuard,
      this.platformController.getUniqueAppTypes,
    );
    this.router.get(
      `${this.path}/platform/getHJS`,
      authenticationGuard,
      this.platformController.getHeaderScript,
    );
    this.router.get(
      `${this.path}/platform/getDocereeLoginSnippet`,
      authenticationGuard,
      this.platformController.getDocereeLoginSnippetScript,
    );
    this.router.get(
      `${this.path}/platform/getDocereeLogoutSnippet`,
      authenticationGuard,
      this.platformController.getDocereeLogoutSnippetScript,
    );

    this.router.get(
      `${this.path}/platform/getMobilePISnippet0`,
      authenticationGuard,
      this.platformController.getMobilePISnippet0,
    );
    this.router.get(
      `${this.path}/platform/getMobilePISnippet1`,
      authenticationGuard,
      this.platformController.getMobilePISnippet1,
    );
    this.router.get(
      `${this.path}/platform/getMobilePISnippet2`,
      authenticationGuard,
      this.platformController.getMobilePISnippet2,
    );
    this.router.get(
      `${this.path}/platform/getMobilePISnippet3`,
      authenticationGuard,
      this.platformController.getMobilePISnippet3,
    );
    this.router.get(
      `${this.path}/platform/getMobileAISnippet1`,
      authenticationGuard,
      this.platformController.getMobileAISnippet1,
    );
    this.router.get(
      `${this.path}/platform/getMobileAISnippet2`,
      authenticationGuard,
      this.platformController.getMobileAISnippet2,
    );

    this.router.post(
      `${this.path}/platform`,
      authenticationGuard,
      this.platformController.createPlatform,
    );
    this.router.get(
      `${this.path}/platform`,
      authenticationGuard,
      this.platformController.getPlatformPerZone,
    );
    this.router.get(
      `${this.path}/allzoneplatform`,
      authenticationGuard,
      this.platformController.getPlatform,
    );
    this.router.get(
      `${this.path}/platform/:id`,
      authenticationGuard,
      this.platformController.getPlatformById,
    );
    this.router.put(
      `${this.path}/platform`,
      authenticationGuard,
      this.platformController.updatePlatform,
    );
    this.router.get(
      `${this.path}/platform/viewing/:publisherId`,
      authenticationGuard,
      this.platformController.getViewingPlatform,
    );
    this.router.post(
      `${this.path}/platform/checkPlatformNameAvailablity`,
      authenticationGuard,
      this.platformController.checkPlatformNameAvailablity
    );
    this.router.put(
      `${this.path}/platform/updatePaymentSlots`,
      authenticationGuard,
      this.platformController.updatePlatformPaymentSlots,
    );

    this.router.post(
      `${this.path}/platform/checkPlatformURLAvailablity`,
      authenticationGuard,
      this.platformController.checkPlatformURLAvailablity,
    );

    this.router.get(
      `${this.path}/websiteandmobileappsplatforms`,
      authenticationGuard,
      this.platformController.getPlatformsByPlatformTypeAndPublisherId,
    );

    this.router.get(
      `${this.path}/siteTypes`,
      authenticationGuard,
      this.platformController.getSiteTypes,
    );
    this.router.get(
      `${this.path}/unique/siteTypes`,
      authenticationGuard,
      this.platformController.getUniqueSiteTypes,
    );

    this.router.get(
      `${this.path}/asset`,
      authenticationGuard,
      this.assetController.getAssetDetails,
    );
    this.router.post(
      `${this.path}/asset`,
      authenticationGuard,
      this.assetController.createAsset,
    );
    this.router.put(
      `${this.path}/asset`,
      authenticationGuard,
      this.assetController.updateAsset,
    );
    this.router.get(
      `${this.path}/asset/platform/:platformId`,
      authenticationGuard,
      this.assetController.getAssetsByPlatformId,
    );
    this.router.get(
      `${this.path}/asset/platform/active/:platformId`,
      authenticationGuard,
      this.assetController.getActiveAssetsbyPlatformId,
    );
    this.router.post(
      `${this.path}/asset/checkAssetNameAvailablity`,
      authenticationGuard,
      this.assetController.checkAssetNameAvailability,
    );
    this.router.delete(
      `${this.path}/asset/:id`,
      authenticationGuard,
      this.assetController.deleteAsset,
    );
    this.router.get(
      `${this.path}/platform/reminder/noplatform`,
      authenticationGuard,
      this.platformController.noPlatform,
    );
    this.router.get(
      `${this.path}/asset/reminder/noassets`,
      authenticationGuard,
      this.assetController.noAssets,
    );
    this.router.get(
      `${this.path}/asset/reminder/oneasset`,
      authenticationGuard,
      this.assetController.oneAsset,
    );
    this.router.get(
      `${this.path}/asset/getSuggestedBidRange/:platformId`,
      authenticationGuard,
      this.assetController.getSuggestedBidRange,
    );

    this.router.get(
      `${this.path}/getBannerSizesForExistedAssets`,
      authenticationGuard,
      this.assetController.getBannerSizesForExistedAssets,
    );

    this.router.get(
      `${this.path}/bank-details`,
      authenticationGuard,
      this.bankDetailsController.getAllBankDetails,
    );
    this.router.get(
      `${this.path}/bank-details/:id`,
      authenticationGuard,
      this.bankDetailsController.getAllBankDetails,
    );
    this.router.get(
      `${this.path}/getPlatformBankDetails/:platformId`,
      authenticationGuard,
      this.bankDetailsController.getAllBankDetailsByPlatformId,
    );
    this.router.post(
      `${this.path}/bank-details`,
      authenticationGuard,
      this.bankDetailsController.createBankDetail,
    );
    this.router.put(
      `${this.path}/bank-details/:id`,
      authenticationGuard,
      this.bankDetailsController.updateBankDetail,
    );
    this.router.delete(
      `${this.path}/bank-details/:id`,
      authenticationGuard,
      this.bankDetailsController.deleteBankDetail,
    );
    this.router.post(
      `${this.path}/analytics`,
      authenticationGuard,
      this.publisherAnalyticsController.getAnalyticsData,
    );
    this.router.post(
      `${this.path}/dashboardperformanceanalytics`,
      authenticationGuard,
      this.dashboardController.getPerformanceAnalyticsData,
    );
    this.router.get(
      `${this.path}/analytics/metadata/:platformId`,
      authenticationGuard,
      this.publisherAnalyticsController.getFilterDetailsForAnalyticsByPlatformId,
    );
    this.router.get(
      `${this.path}/dashboard`,
      authenticationGuard,
      this.dashboardController.getDashboardDetails,
    );
    this.router.get(
      `${this.path}/dashboard/:platformId`,
      authenticationGuard,
      this.dashboardController.getDashboardDetails,
    );
    this.router.post(
      `${this.path}/analyticsCards`, authenticationGuard, this.dashboardController.analyticsCard,
    );
    this.router.put(
      `${this.path}/switchtoplatform/:id`,
      authenticationGuard,
      this.platformController.switchToPlatform
    );
    // to generate all platforms bills
    this.router.get(
      `${this.path}/generateBills`, authenticationGuard, this.billController.generatePublisherBill,
    );
    this.router.get(
      `${this.path}/getOutstandingAmount`, authenticationGuard, this.billController.getOutstandingAmount,
    );

    // to generate bill by platform id, start date and end date: format yyyy-mm-dd
    this.router.get(
      `${this.path}/generateBillByPlatformId/:platformId/:startDate?/:endDate?`,
      authenticationGuard,
      this.billController.generateBillByPlatformId,
    );

    this.router.get(
      `${this.path}/bills`,
      authenticationGuard,
      this.billController.getBills,
    );
    this.router.get(
      `${this.path}/bill/:id`,
      authenticationGuard,
      this.billController.getBillById,
    );
    this.router.put(
      `${this.path}/bill/:id`,
      authenticationGuard,
      this.billController.updateBill,
    );
    this.router.get(
      `${this.path}/pendingBills`,
      authenticationGuard,
      this.billController.getAllPendingBills,
    );
    this.router.get(
      `${this.path}/downloadBill/:id`,
      authenticationGuard,
      this.billController.downloadBillById,
    );
    this.router.get(`${this.path}/emailBill/:id`, authenticationGuard, this.billController.emailBillById);
    this.router.get(`${this.path}/platform/checkHCPStatus/:platformId`, authenticationGuard,
       this.publisherTransformedHCPController.getValidatedHcpCounts);
    this.router.get(`${this.path}/platform/notifyHCPStatus/:platformId`, authenticationGuard,
       this.publisherTransformedHCPController.notifyHcpValidationStatus);

    // Publisher user
    this.router.post(`${this.path}/publisheruser`, authenticationGuard, this.publisherUserController.createPublisherUser);
    this.router.get(`${this.path}/publisheruserdetails/:publisherId`, authenticationGuard, this.publisherUserController.getPublisherUsersDetails);
    this.router.put(`${this.path}/publisheruserplatform`, authenticationGuard, this.publisherUserController.removePlatformFromPublisherUsers);
    this.router.put(`${this.path}/publisheruserplatformrole`, authenticationGuard, this.publisherUserController.updatePlatformRoleOfPublisherUsers);
    this.router.post(`${this.path}/regeneratetoken`, authenticationGuard, this.publisherUserController.reGenerateToken);
    this.router.put(`${this.path}/publisheruserswitchplatform/:platformId`, authenticationGuard,
    this.publisherUserController.switchToPlatformOfLoggedinUser);

    this.router.get(`${this.path}/publisherdetails/:email`, authenticationGuard, this.publisherUserController.getPublisherDetails);
    this.router.get(`${this.path}/publisheruserpermissions/:email`, authenticationGuard, this.publisherUserController.getPublisherUserPermissions);
    this.router.delete(`${this.path}/publisheruser/:email`, authenticationGuard, this.publisherUserController.deletePublisherUsers);
    this.router.get(`${this.path}/publisheruserplatforms`, authenticationGuard, this.publisherUserController.getPublisherUserPlatforms);

    this.router.post(
      `${this.path}/platform/sendInvitationAndHeaderCodeMail`,
      authenticationGuard,
      this.platformController.sendInvitationAndHeaderCode,
    );
    this.router.post(
      `${this.path}/platform/sendHeaderCodeMail`,
      authenticationGuard,
      this.platformController.sendHeaderCode,
    );
    this.router.post(
      `${this.path}/platform/sendInvitationAndInvocationCodeMail`,
      authenticationGuard,
      this.platformController.sendInvitationAndInvocationCode,
    );
    this.router.post(
      `${this.path}/platform/sendInvocationCodeMail`,
      authenticationGuard,
      this.platformController.sendInvocationCode,
    );
    this.router.post(
      `${this.path}/asset/sendInvitationAndAdIntegrationCodeMail`,
      authenticationGuard,
      this.assetController.sendInvitationAndAdIntegrationCode,
    );
    this.router.post(
      `${this.path}/asset/sendAdIntegrationCodeMail`,
      authenticationGuard,
      this.assetController.sendAdIntegrationCode,
    );
    this.router.post(
      `${this.path}/publisheruser/sendSubUserMails`,
      authenticationGuard,
      this.publisherUserController.sendSubUserMails,
    );
  }

  private readonly getRoot = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ) => {
    response.send('Doceree Publisher API page');
  }
}
