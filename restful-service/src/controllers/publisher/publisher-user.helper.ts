import config from 'config';
import express from 'express';
import moment = require('moment');
import RoleDAO from '../../daos/admin/role.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { NotificationIcon, NotificationRoute, NotificationType } from '../../util/constants';
import { HashPwd } from '../../util/hash-pwd';
import { PublisherMail } from '../../util/mail/publisher-mail';
import { NotificationHelper } from '../../util/notification-helper';
import { logger } from '../../util/winston';

export class PublisherUserHelper {

    private readonly publisherUserDAO: PublisherUserDAO;
    private readonly userDAO: UserDAO;
    private readonly hashPwd: HashPwd;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly publisherMail: PublisherMail;
    private readonly roleDAO: RoleDAO;
    private readonly platformDAO: PublisherPlatformDAO;
    private readonly notificationHelper: NotificationHelper;

    constructor() {
        this.publisherUserDAO = new PublisherUserDAO();
        this.userDAO = new UserDAO();
        this.hashPwd = new HashPwd();
        this.userTokenDAO = new UserTokenDAO();
        this.publisherMail = new PublisherMail();
        this.roleDAO = new RoleDAO();
        this.platformDAO = new PublisherPlatformDAO();
        this.notificationHelper = new NotificationHelper();
    }

    public async createUser(
        loggedInUser: UserDTO,
        req: IAuthenticatedRequest,
        res: express.Response) {
        const newPublisherDTO: PublisherUserDTO = this.toPublisherUserDTO(req);
        newPublisherDTO.created = { at: new Date(), by: req.email };

        // get user
        const user: UserDTO = await this.userDAO.findByEmail(req.body.email);
        if (user) {
            // existing user flow
            if (user.userType !== 'publisher') {
                return { message: 'User exists but not a publisher!', success: false };
            } else {
                // check if user is already associated with this publisher
                const existingPublisherUser: PublisherUserDTO =
                    await this.publisherUserDAO.findByPublisherIdAndEmail(req.body.publisherId, req.body.email);

                if (existingPublisherUser) {
                    // tslint:disable-next-line: prefer-switch
                    if (user.userStatus === 'active' || user.userStatus === 'pending') {
                        if (!existingPublisherUser.platforms.length) {
                            newPublisherDTO.platforms[0].isWatching = true;
                        } else {
                            // zone specific modification - set the default platform in case we have two zones
                            const existingPublisherUserInZone: any[] = existingPublisherUser.platforms.filter((x) => x.zone === req.header('zone'));
                            if (existingPublisherUserInZone.length === 0) {
                                newPublisherDTO.platforms[0].isWatching = true;
                            }
                        }

                        let sameRolePlatformExists = false;
                        for (const s of existingPublisherUser.platforms) {
                            if (s.platformId.toString() === newPublisherDTO.platforms[0].platformId.toString() &&
                                s.userRoleId.toString() === newPublisherDTO.platforms[0].userRoleId.toString()) {
                                sameRolePlatformExists = true;
                                break;
                            }
                        }
                        if (!sameRolePlatformExists) {
                            existingPublisherUser.platforms.push(newPublisherDTO.platforms[0]);
                        }

                        existingPublisherUser.modified = { at: new Date(), by: req.email };
                        await this.publisherUserDAO.update(existingPublisherUser.email, existingPublisherUser);
                        return { message: 'Platform Added successfully !', success: true };
                    } else if (user.userStatus === 'deleted') {
                        return { message: 'User is deleted', success: false };
                    } else {
                        throw new Error('Publisher user role addition: Unhandled scenario occurred');
                    }
                } else {
                    const checkIfUserAssociatedWithAnotherPublisher: PublisherUserDTO =
                        await this.publisherUserDAO.findByEmail(req.body.email);
                    if (checkIfUserAssociatedWithAnotherPublisher) {
                        // user is associated with a different publisher
                        return {
                            message: 'User belongs to a different publisher, cannot be associated to this publisher',
                            success: false
                        };
                    } else {
                        newPublisherDTO.platforms[0].isWatching = true;
                        const result = await this.publisherUserDAO.create(newPublisherDTO);
                        let dontSendMail = false;
                        if (req.body.dontSendMail) {
                            dontSendMail = req.body.dontSendMail;
                        }
                        // add send email & notification here
                        if (req.body.platformId && req.body.roleId) {
                            await this.existingUserEmail(newPublisherDTO.platforms[0].userRoleId, newPublisherDTO.platforms[0].platformId,
                                loggedInUser, req.body.email, dontSendMail, req.body.publisherId, req.header('zone'));
                        }
                        return result;
                    }
                }
            }
        } else {
            const userDetail: UserDTO = new UserDTO();
            userDetail.created = { at: new Date(), by: req.email };
            userDetail.email = req.body.email;
            userDetail.userRole = null;
            userDetail.userStatus = 'pending';
            userDetail.userType = 'publisher';
            userDetail.hasPassword = false;
            userDetail.hasPersonalProfile = false;
            userDetail.hasSecurityQuestions = false;
            const createdUser: UserDTO = await this.userDAO.create(userDetail);

            newPublisherDTO.platforms[0].isWatching = true;
            const result = await this.publisherUserDAO.create(newPublisherDTO);

            let dontSendMail = false;
            if (req.body.dontSendMail) {
                dontSendMail = req.body.dontSendMail;
            }
            await this.newUserEmails(newPublisherDTO.platforms[0].userRoleId, newPublisherDTO.platforms[0].platformId,
                loggedInUser, req.body.email, dontSendMail, req.body.publisherId, req.header('zone'));

            return { message: 'Added successfully.', success: true };
        }
    }

    public toPublisherUserDTO(req: IAuthenticatedRequest): PublisherUserDTO {
        const payload: PublisherUserDTO = new PublisherUserDTO();
        payload.email = req.body.email;
        payload.platforms = [{
            zone: req.header('zone'),
            platformId: req.body.platformId,
            userRoleId: req.body.userRoleId,
            isWatching: false
        }];
        payload.publisherId = req.body.publisherId;
        return payload;
    }

    public async newUserEmails(userRoleId: string, platformId: string, fromUser: UserDTO, toEmail: string, dontSendMail: boolean,
                               publisherId: string, zone: string) {
        const roleDTO = await this.roleDAO.getRoleById(userRoleId);
        const platformDTO = await this.platformDAO.findById(platformId, zone);
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        const sender: UserDTO = await this.userDAO.findByEmail(fromUser.email);
        let sendStatus: boolean = false;

        const jwtExpiresInVal = config.get<string>('jwt.addUserExpiresIn');
        const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
        const tokenStr: string = await this.hashPwd.jwtToken(toEmail, expiresIn);

        const tokenDTO: UserTokenDTO = new UserTokenDTO();
        tokenDTO.email = toEmail;
        tokenDTO.expiryTime = new Date(expiresIn);
        tokenDTO.token = tokenStr;
        tokenDTO.tokenType = 'addUser';
        tokenDTO.created = { at: new Date(), by: fromUser.email };
        await this.userTokenDAO.create(tokenDTO);

        if (roleDTO.roleName.toLowerCase() === 'admin') {
            if (!dontSendMail) {
                sendStatus = await this.publisherMail.sendGrantedNewUserAdminAccessMail(
                    toEmail,
                    sender.firstName,
                    platformDTO.name,
                    `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
                );
            }
        }

        if (roleDTO.roleName.toLowerCase() === 'analyst') {
            if (!dontSendMail) {
                sendStatus = await this.publisherMail.sendGrantedNewUserAnalyticsAccessMail(
                    toEmail,
                    sender.firstName,
                    platformDTO.name,
                    `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
                );
            }
        }
        if (roleDTO.roleName.toLowerCase() === 'developer') {
            if (!dontSendMail) {
                logger.info('Sending developer invitation mail');
                sendStatus = await this.publisherMail.sendGrantedNewUserDeveloperAccessMail(
                    toEmail,
                    sender.firstName,
                    platformDTO.name,
                    `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
                );
            }
        }
        // send notification - starts
        try {
            // get all the admin users in the publisher for notification
            const allAdminUsers: string[] = await this.publisherUserDAO.getPublisherAdminUsers(publisherId);
            const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
            if (sendNotificationTo.length > 0) {
                const notificationIcon: NotificationIcon = NotificationIcon.info;
                const notificationMessage: string = `A quick update on recent account activity: ${toEmail} has been added to the team for ${platformDTO.name} as ${roleDTO.roleName} by ${fromUser.firstName}`;
                await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                    NotificationType.platform, platformDTO._id.toString(), NotificationRoute.publisher_manage_account);
            }
        } catch (err) {
            logger.error(`${err.stack}\n${new Error().stack}`);
        }
        // send notification - ends
        return sendStatus;
    }

    public async existingUserEmail(userRoleId: string, platformId: string, fromUser: UserDTO, toEmail: string, dontSendMail: boolean,
                                   publisherId: string, zone: string) {
        const roleDTO = await this.roleDAO.getRoleById(userRoleId);
        const platformDTO = await this.platformDAO.findById(platformId, zone);
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        const sender: UserDTO = await this.userDAO.findByEmail(fromUser.email);
        if (fromUser.email !== toEmail) {
            if (!dontSendMail) {
                await this.publisherMail.sendGrantedUserAccessExistingUserMail(
                    toEmail,
                    sender.firstName,
                    platformDTO.name,
                    roleDTO.roleName,
                    `${url}/login`
                );
            }
        }

        // send notification - starts
        try {
            // get all the admin users in the publisher for notification
            const allAdminUsers: string[] = await this.publisherUserDAO.getPublisherAdminUsers(publisherId);
            const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
            if (sendNotificationTo.length > 0) {
                const notificationIcon: NotificationIcon = NotificationIcon.info;
                const notificationMessage: string = `A quick update on recent account activity: ${toEmail} has been added to the team for ${platformDTO.name} as ${roleDTO.roleName} by ${fromUser.firstName}`;
                await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                    NotificationType.platform, platformDTO._id.toString(), NotificationRoute.publisher_manage_account);
            }
        } catch (err) {
            logger.error(`${err.stack}\n${new Error().stack}`);
        }
        // send notification - ends
    }
}
