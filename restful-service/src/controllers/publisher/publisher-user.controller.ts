import config from 'config';
import express from 'express';
import moment = require('moment');
import mongoose from 'mongoose';
import RoleDAO from '../../daos/admin/role.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import RoleDTO from '../../dtos/admin/role.dto';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherUserDTO from '../../dtos/publisher/publisher-user.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import throwIfInputValidationError from '../../error/input-validation-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { NotificationIcon, NotificationRoute, NotificationType, PublisherPermissions, UserStatuses, UserTypes } from '../../util/constants';
import { HashPwd } from '../../util/hash-pwd';
import { Mail } from '../../util/mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { PublisherMail } from '../../util/mail/publisher-mail';
import { NotificationHelper } from '../../util/notification-helper';
import { logger } from '../../util/winston';
import { AuthorizePublisher } from './authorize-publisher';
import { PublisherUserHelper } from './publisher-user.helper';

export class PublisherUserController {

    private readonly publisherUserDAO: PublisherUserDAO;
    private readonly publisherDAO: PublisherDAO;
    private readonly userDAO: UserDAO;
    private readonly hashPwd: HashPwd;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly publisherMail: PublisherMail;
    private readonly generalMail: GeneralMail;
    private readonly roleDAO: RoleDAO;
    private readonly platformDAO: PublisherPlatformDAO;
    private readonly notificationHelper: NotificationHelper;
    private readonly authorizePublisher: AuthorizePublisher;
    private readonly publisherUserHelper: PublisherUserHelper;

    constructor() {
        this.publisherUserDAO = new PublisherUserDAO();
        this.publisherDAO = new PublisherDAO();
        this.userDAO = new UserDAO();
        this.hashPwd = new HashPwd();
        this.userTokenDAO = new UserTokenDAO();
        this.publisherMail = new PublisherMail();
        this.generalMail = new GeneralMail();
        this.roleDAO = new RoleDAO();
        this.platformDAO = new PublisherPlatformDAO();
        this.notificationHelper = new NotificationHelper();
        this.authorizePublisher = new AuthorizePublisher();
        this.publisherUserHelper = new PublisherUserHelper();
    }

    public createPublisherUser = async (
        req: IAuthenticatedRequest,
        res: express.Response,
        next: express.NextFunction
    ) => {
        try {
            const loggedInUser: UserDTO = await this.userDAO.findByEmail(req.email);
            if (loggedInUser.userType.toLowerCase() !== UserTypes.publisher.toLowerCase()
                || loggedInUser.userStatus.toLowerCase() !== UserStatuses.active.toLowerCase()) {
                    logger.error(`Logged in user[${req.email}] is not a publisher`);
                    throw new HandledApplicationError(500, 'Logged in user is not a publisher');
            }

            req.checkBody('email', 'Email is required').notEmpty();
            req.checkBody('email', 'Email does not appear to be valid').isEmail();

            // check the validation object for errors
            const validationErrors = req.validationErrors();
            if (validationErrors) {
                return res.json({
                    message: 'Request body has no valid data',
                    success: false,
                });
            }

            const createUserResponse = await this.publisherUserHelper.createUser(loggedInUser, req, res);
            return res.json(createUserResponse);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPublisherUsersDetails = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // NOTE: Zone is not considered here, its intended
            // only admin will have right to view all the users
            const authorizePayload: any = { publisherId: req.params.publisherId };
            // await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewUser);

            const publisherUsers: any[] = await this.publisherUserDAO.findByPublisherId(req.params.publisherId);
            const thisPublisherUser: any = await this.publisherUserDAO.findByEmail(req.email);
            const details: any[] = [];

            const setUniquePlatformIds: Set<string> = new Set();
            for (const a of publisherUsers) {
                if (a.platforms && a.platforms.length > 0) {
                    const tempPlatformIds: string[] = a.platforms.map((y: any) => y.platformId.toString());
                    for (const t of tempPlatformIds) {
                        setUniquePlatformIds.add(t);
                    }
                }
            }
            let uniquePlatformIds: any[] = [...setUniquePlatformIds];
            uniquePlatformIds = uniquePlatformIds.map((x) => mongoose.Types.ObjectId(x));

            const setUniqueRoleIds: Set<string> = new Set();
            for (const a of publisherUsers) {
                if (a.platforms && a.platforms.length > 0) {
                    const tempRoleIds: string[] = a.platforms.map((y: any) => y.userRoleId);
                    for (const t of tempRoleIds) {
                        setUniqueRoleIds.add(t);
                    }
                }
            }
            let uniqueRoleIds: any[] = [...setUniqueRoleIds];
            uniqueRoleIds = uniqueRoleIds.map((x) => mongoose.Types.ObjectId(x));

            const platforms: PublisherPlatformDTO[] = await this.platformDAO.findNonDeletedByIds(uniquePlatformIds);
            const roles: RoleDTO[] = await this.roleDAO.getRolesByIds(uniqueRoleIds);

            for (const pu of publisherUsers) {
                const user: UserDTO = await this.userDAO.findByEmail(pu.email);
                for (const p of pu.platforms) {
                    const exists = thisPublisherUser.platforms.find((y: any) => (y.platformId.toString() === p.platformId.toString()));
                    if (exists) {
                        const platform = platforms.find((x) => (x._id.toString() === p.platformId.toString()));
                        const role = roles.find((x) => (x._id.toString() === p.userRoleId));
                        if (platform && role && user) {
                            const d: any = {
                                email: user.email,
                                firstName: user.firstName,
                                lastName: user.lastName,
                                platformName: platform.name,
                                platformId: platform._id,
                                roleName: role.roleName,
                                roleId: role._id
                            };
                            details.push(d);
                        }
                    }
                }
            }
            res.json(details);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPublisherUser = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUser = await this.publisherUserDAO.findByEmailWithPublisherDetails(req.params.email);
            if (publisherUser && publisherUser.length > 0) {
                res.json(publisherUser[0]);
            } else {
                res.json(null);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPublisherDetails = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUser = await this.publisherUserDAO.findByEmail(req.params.email);
            if (publisherUser) {
                const publisher = await this.publisherDAO.findById(publisherUser.publisherId);
                res.json(publisher);
            } else {
                res.json(null);
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPublisherUserPermissions = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUser = await this.publisherUserDAO.findByEmail(req.params.email);
            let watchingPlatform: any;
            if (publisherUser) {
                if (req.header('zone') === '1' || req.header('zone') === '2') {
                    watchingPlatform = publisherUser.platforms.find((obj) => obj.isWatching && obj.zone === req.header('zone'));
                    if (!watchingPlatform) {
                        watchingPlatform = publisherUser.platforms.find((x) => x.zone === req.header('zone'));
                    }
                } else {
                    watchingPlatform = publisherUser.platforms.find((obj) => obj.isWatching);
                }
                const userRole: RoleDTO = await this.roleDAO.getRoleById(
                    watchingPlatform.userRoleId
                );
                return res.json({ userRole: userRole.roleName, userPermissions: userRole.permissions, zone: watchingPlatform.zone });
            } else {
                return res.json({ userRole: '', userPermissions: [] });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public deletePublisherUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            req
                .checkParams('email', "Mandatory field 'email' missing in request param")
                .exists();
            await throwIfInputValidationError(req);
            // get user and change.
            const user: UserDTO = await this.userDAO.findByEmail(req.params.email);
            // get publisher user and change
            const publisherUser: PublisherUserDTO = await this.publisherUserDAO.findByEmail(req.params.email);
            publisherUser.deleted = true;
            publisherUser.modified = { at: new Date(), by: req.email };
            // delete token
            await this.userTokenDAO.deleteByEmailAndTokenType(user.email, 'addUser');
            // update publisher user
            const publisherUserDTO: PublisherUserDTO = await this.publisherUserDAO.update(publisherUser.email, publisherUser);
            // get all the admin users in the publisher for notification
            try {
                const allAdminUsers: string[] = await this.publisherUserDAO.getPublisherAdminUsers(req.body.publisherId);
                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                if (sendNotificationTo.length > 0) {
                    const loggedInUser = await this.userDAO.findByEmail(req.email);
                    const name: string = user.firstName ? user.firstName : user.email;
                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                    const notificationMessage: string = `A quick update on recent account activity: ${name} has been removed by ${loggedInUser.firstName}`;
                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                        NotificationType.platform, null, NotificationRoute.publisher_manage_account);
                }
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
            }

            res.json({ success: true, message: `${publisherUserDTO.email} user deleted from publisher successfully` });
        } catch (err) {
            catchError(err, next);
        }
    }

    public removePlatformFromPublisherUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // get publisher user and change
            const publisherUser: PublisherUserDTO = await this.publisherUserDAO.findByEmail(req.body.email);
            publisherUser.platforms.splice(publisherUser.platforms.findIndex((item) => item.platformId === req.body.platformId), 1);
            publisherUser.modified = { at: new Date(), by: req.email };
            // update publisher user
            const publisherUserDTO: PublisherUserDTO = await this.publisherUserDAO.update(publisherUser.email, publisherUser);

            try {
                // get all the admin users in the publisher for notification
                const allAdminUsers: string[] = await this.publisherUserDAO.getPublisherAdminUsers(publisherUserDTO.publisherId);
                const sendNotificationTo: string[] = allAdminUsers; // .filter((x) => x !== req.email);
                if (sendNotificationTo.length > 0) {
                    const removedUser = await this.userDAO.findByEmail(req.body.email);
                    const loggedInUser = await this.userDAO.findByEmail(req.email);
                    const platform = await this.platformDAO.findById(req.body.platformId, req.header('zone'));
                    const notificationIcon: NotificationIcon = NotificationIcon.info;
                    const name: string = removedUser.firstName ? removedUser.firstName : removedUser.email;
                    const notificationMessage: string = `A quick update on recent account activity: ${name} has been removed for ${platform.name} by ${loggedInUser.firstName}`;
                    await this.notificationHelper.addNotification(sendNotificationTo, notificationMessage, notificationIcon,
                        NotificationType.platform, req.body.platformId, NotificationRoute.publisher_manage_account);
                }
            } catch (err) {
                logger.error(`${err.stack}\n${new Error().stack}`);
            }
            res.json({ success: true, message: 'User deleted from the platform successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public updatePlatformRoleOfPublisherUsers = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // get publisher user and change
            const publisherUser: PublisherUserDTO = await this.publisherUserDAO.findByEmail(req.body.email);
            // get user role
            const userRole: RoleDTO = await this.roleDAO.getRoleByModuleAndRoleName('Publisher', req.body.userRole);
            publisherUser.platforms.forEach((element) => {
                if (element.platformId && element.platformId.toString() === req.body.platformId) {
                    element.userRoleId = userRole._id;
                }
            });
            publisherUser.modified = { at: new Date(), by: req.email };
            // update publisher user
            const publisherUserDTO: PublisherUserDTO = await this.publisherUserDAO.update(publisherUser.email, publisherUser);
            res.json({ success: true, message: 'Platform user role updated successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public reGenerateToken = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        let tokenStr: string;
        try {
            // delete token
            const dlatedToken = await this.userTokenDAO.deleteByEmailAndTokenType(req.body.email, 'addUser');
            // create token and send email
            const jwtExpiresInVal = config.get<string>('jwt.addUserExpiresIn');
            const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
            tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);

            const tokenDTO: UserTokenDTO = new UserTokenDTO();
            tokenDTO.email = req.body.email;
            tokenDTO.expiryTime = new Date(expiresIn);
            tokenDTO.token = tokenStr;
            tokenDTO.tokenType = 'addUser';
            tokenDTO.created = { at: new Date(), by: req.email };
            await this.userTokenDAO.create(tokenDTO);
            const result: UserDTO = await this.userDAO.findByEmail(req.body.email);
            const url: string = config.get<string>('mail.web-portal-redirection-url');
            const sendStatus: boolean = await this.generalMail.sendRegistrationCompletedPublisherMail(
            req.body.email,
            `${url}/verifytoken?token=${tokenStr}&tokenType=addUser`
            );
            if (sendStatus) {
                return res.json({ message: 'Activation link sent successfully.', success: true });
            } else {
                return res.json({ message: 'Email not sent', success: false });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public switchToPlatformOfLoggedinUser = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // get publisher user and change
            const publisherUser: PublisherUserDTO = await this.publisherUserDAO.findByEmail(req.email);
            let isAdmin = false;
            if (publisherUser.platforms.length === 1) {
                publisherUser.platforms[0].isWatching = true;
                const role: RoleDTO = await this.roleDAO.getRoleById(publisherUser.platforms[0].userRoleId);
                if (role.roleName === 'Admin') {
                    isAdmin = true;
                } else {
                    isAdmin = false;
                }
            } else {
                publisherUser.platforms.forEach(async (element) => {
                    if (element.zone === req.header('zone')) {
                        if (element.isWatching === true) {
                            element.isWatching = false;
                        }

                        if (element.platformId.toString() === req.params.platformId) {
                            element.isWatching = true;
                            const role: RoleDTO = await this.roleDAO.getRoleById(element.userRoleId);
                            if (role.roleName === 'Admin') {
                                isAdmin = true;
                            } else {
                                isAdmin = false;
                            }
                        }
                    }
                });
            }
            publisherUser.modified = { at: new Date(), by: req.email };
            // update publisher user
            const publisherUserDTO: PublisherUserDTO = await this.publisherUserDAO.update(publisherUser.email, publisherUser);
            res.json({ isAdminPlatform: isAdmin, success: true, message: 'Platform switched successfully' });
        } catch (err) {
            catchError(err, next);
        }
    }

    public getPublisherUserPlatforms = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUserPlatformsDetail = await this.publisherUserDAO.findByEmail(req.email);
            return res.json(publisherUserPlatformsDetail);
        } catch (err) {
            catchError(err, next);
        }
    }

    public sendSubUserMails = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const loggedInUser: UserDTO = await this.userDAO.findByEmail(req.email);
            // check if the logged in user is an admin
            const platformId: string = req.body.platformId;
            const roleId: string = req.body.userRoleId;
            const toEmail: string = req.body.email;
            const authorizePayload = { platformId: req.body._id };
            await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.addUser);

            const publisherUserPlatformsDetail = await this.publisherUserDAO.findByEmail(req.body.email);
            const noOfPlatforms: any[] = publisherUserPlatformsDetail.platforms.filter((x) => x.platformId.toString() === platformId);
            if (noOfPlatforms.length === 1) {
                this.publisherUserHelper.newUserEmails(roleId, platformId, loggedInUser, toEmail, false, publisherUserPlatformsDetail.publisherId,
                    req.header('zone'));
            } else if (noOfPlatforms.length > 1) {
                this.publisherUserHelper.existingUserEmail(roleId, platformId, loggedInUser, toEmail, false, publisherUserPlatformsDetail.publisherId,
                    req.header('zone'));
            } else {
                // nothing to do
            }
            return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
        } catch (err) {
            catchError(err, next);
        }
    }
}
