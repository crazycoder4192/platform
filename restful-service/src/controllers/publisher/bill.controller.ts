import config from 'config';
import express from 'express';
import moment from 'moment';
import mongoose from 'mongoose';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import PublisherBankDetailDAO from '../../daos/publisher/bank-detail.dao';
import PublisherBillDAO from '../../daos/publisher/publisher-bill.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import BillDTO from '../../dtos/publisher/publisher-bill.dto';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import catchError from '../../error/catch-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { Mail } from '../../util/mail';
import { GeneralMail } from '../../util/mail/general-mail';
import { PublisherMail } from '../../util/mail/publisher-mail';
const dateformat = require('dateformat');

const randomString = require('random-string');
const fs = require('fs');
const PDFDocument = require('pdfkit');
import appRoot from 'app-root-path';
import path from 'path';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import UserDAO from '../../daos/user.dao';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import UserDTO from '../../dtos/user.dto';
import HandledApplicationError from '../../error/handled-application-error';
import { BillStatus, NotificationIcon, NotificationRoute, NotificationType, PublisherPermissions } from '../../util/constants';
import { NotificationHelper } from '../../util/notification-helper';
import { logger } from '../../util/winston';
import { AuthorizeAdmin } from '../admin/authorize-admin';
import { QuickbooksHelper } from '../util/quickbooks-helper';
import { AuthorizePublisher } from './authorize-publisher';

export class BillController {
    private readonly publisherDAO: PublisherDAO;
    private readonly publisherMail: PublisherMail;
    private readonly publisherBillDAO: PublisherBillDAO;
    private readonly publisherPlatformDAO: PublisherPlatformDAO;
    private readonly publisherAssetDAO: PublisherAssetDAO;
    private readonly publisherBankDetailDAO: PublisherBankDetailDAO;
    private readonly publisherUserDAO: PublisherUserDAO;
    private readonly authorizePublisher: AuthorizePublisher;
    private readonly authorizeAdmin: AuthorizeAdmin;
    private readonly quickbooksHelper: QuickbooksHelper;
    private readonly userDAO: UserDAO;
    private readonly userTokenDAO: UserTokenDAO;
    private readonly generalMail: GeneralMail;
    private readonly notificationHelper: NotificationHelper;

    constructor() {
        this.publisherDAO = new PublisherDAO();
        this.publisherMail = new PublisherMail();
        this.publisherBillDAO = new PublisherBillDAO();
        this.publisherPlatformDAO = new PublisherPlatformDAO();
        this.publisherAssetDAO = new PublisherAssetDAO();
        this.publisherBankDetailDAO = new PublisherBankDetailDAO();
        this.publisherUserDAO = new PublisherUserDAO();
        this.authorizePublisher = new AuthorizePublisher();
        this.authorizeAdmin = new AuthorizeAdmin();
        this.quickbooksHelper = new QuickbooksHelper();
        this.userDAO = new UserDAO();
        this.userTokenDAO = new UserTokenDAO();
        this.generalMail = new GeneralMail();
        this.notificationHelper = new NotificationHelper();
    }

    // to get platformwise bill generation
    public generatePublisherBill = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.authorizeAdmin.authorize(req.email);
            const defaultPercentage: number = config.get<number>('docereeProfitCalculationValue');
            let endDate: Date;
            if (req.params.endDate) {
                // we are expecting this date to be in UTC format
                endDate = new Date(req.params.endDate);
            }

            // if end date is not provided, then this should be a scheduled job
            // and it should be run on 28th of every month
            if (!endDate) {
                endDate = new Date();
                endDate = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate(),
                    0, 0, 0);
                if (endDate.getDate() !== config.get<number>('billDefaultExecutionDate')) { // this should be the scheduled job
                    logger.error('Scheduled job can be only run on 28th of every month');
                    return res.json({ status: 'fail', message: 'Scheduled job can be only run on 28th of every month' });
                }
            }

            const platforms = await this.publisherPlatformDAO.findAllForBills(req.header('zone'));
            const bills: any[] = [];
            const achs: any[] = [];
            if (platforms && platforms.length > 0) {
                for (const platform of platforms) {

                    // start date should be always picked from the last bill end date
                    const lastBillDTO: BillDTO = await this.publisherBillDAO.getLastBillByPlatformId(platform._id.toString(), req.header('zone'));
                    // for the first time publishers, there wont be any last bill
                    let startDate: Date = null;
                    if (lastBillDTO) {
                        startDate = lastBillDTO.billEndDate;
                    } else {
                        // this will be the publisher that is coming in for the first time,
                        // setting the start date to a year back in past
                        startDate = platform.created.at;
                    }

                    const bill: any = { };
                    const ach: any = { };
                    const paymentMethod = await this.publisherBankDetailDAO.findByPlatformId(platform._id, req.header('zone'));
                    if (!paymentMethod || paymentMethod.length === 0) {
                        logger.error(`No payment method defined for platform ${platform._id}`);
                        bill.billStatus = BillStatus.noBankDetail;
                    } else {
                        bill.billStatus = BillStatus.pending;
                        bill.paymentMethodId = paymentMethod[0]._id;
                        const bankDetail = await this.publisherBankDetailDAO.findById(paymentMethod[0]._id, req.header('zone'));
                        ach.accountNumber = bankDetail.accountNumber;
                        ach.routingNumber = bankDetail.swiftCode;
                        ach.payeeName = bankDetail.accountHolderName;
                    }

                    bill._id = null;
                    bill.publisherId = platform.publisherId;
                    bill.platformId = platform._id;

                    ach.payeeId = platform._id;
                    const assetsList = Array();
                    let i = 1; let currentCycleAmountWithoutAnyCut = 0;

                    const assets = await this.publisherAssetDAO.findAllAssetsByPlatformId(platform._id);
                    if (assets && assets.length > 0) {
                        for (const asset of assets) {
                            const assetAmount = await this.publisherBillDAO.getAmount(asset.codeSnippetId, startDate, endDate, req.header('zone'));
                            if (assetAmount > 0) {
                                const assetAmountWithCut = assetAmount * defaultPercentage / 100;
                                assetsList.push({ no: i, name: asset.name, dimensions: asset.assetDimensions, amount: assetAmountWithCut });
                                // totalAmount += (amount * config.get<number>('docereeProfitCalculationValue') / 100);
                                currentCycleAmountWithoutAnyCut += assetAmount;
                                i++;
                            }
                        }
                    }
                    bill.items = assetsList;

                    const result = await this.getAmounts(platform._id.toString(), currentCycleAmountWithoutAnyCut, req.header('zone'));
                    bill.amount = result.amount;
                    bill.bonus = result.bonus;
                    bill.cut = result.cut;

                    const randomBillNumber = randomString({ length: 10, numeric: true, letters: false, special: false });
                    const taxAmount = await this.publisherBillDAO.getTaxAmount(currentCycleAmountWithoutAnyCut, req.header('zone'));
                    bill.tax = taxAmount;
                    bill.billNumber = randomBillNumber;
                    bill.billStartDate = startDate;
                    bill.billEndDate = endDate;
                    bill.billDate = new Date();
                    bill.created = { at: new Date(), by : req.email };
                    bill.modified = { at: new Date(), by : req.email };
                    bill.deleted = false;

                    if (assetsList.length > 0 && currentCycleAmountWithoutAnyCut > 0) {
                        const nbillDTO: BillDTO = await this.publisherBillDAO.create(bill, req.header('zone'));
                        // TODO: Not sure why I am not able to get the billDTO._id from above query
                        const billDTO: BillDTO = await this.publisherBillDAO.findByBillNumber(randomBillNumber, req.header('zone'));
                        const publisherDTO: PublisherDTO = await this.publisherDAO.findById(bill.publisherId);
                        ach.addenda = `${platform.name}-${publisherDTO.organizationName}-${randomBillNumber}`;
                        ach.addenda = ach.addenda.substring(0, 80);
                        ach.amount = bill.amount.toFixed(2);
                        try {
                            const quickbookBill: any = { };
                            quickbookBill.vendorEmail = publisherDTO.user.email;
                            quickbookBill.line = [];
                            let line: any = { };
                            line.description = 'Description for Assets';
                            line.amount = bill.amount;
                            line.itemName = 'Assets';
                            line.quantity = 1;
                            quickbookBill.line.push(line);
                            if (quickbookBill.bonus > 0) {
                                line = { };
                                line.description = 'Description for Bonus';
                                line.amount = bill.bonus;
                                line.itemName = 'Bonus';
                                line.quantity = 1;
                                quickbookBill.line.push(line);
                            }

                            quickbookBill.dueDate = bill.billDate;
                            const output = await this.quickbooksHelper.createBill(quickbookBill, req.header('zone'));
                        } catch (err) {
                            console.trace(err);// tslint:disable-line
                            logger.error(`Error while creating quickbooks bill: ${JSON.stringify(err)}`);
                            billDTO.quickbooks = { billGenerated: false };
                            await this.publisherBillDAO.update(billDTO._id, billDTO, req.header('zone'));
                        }
                        await this.sendEmail(billDTO, publisherDTO.user.email, req.header('zone'));
                        try {
                            const notificationIcon: NotificationIcon = NotificationIcon.info;
                            const notificationMessage: string = `A new bill for ${platform.name} is now available`;
                            await this.notificationHelper.addNotification([publisherDTO.user.email], notificationMessage, notificationIcon,
                                NotificationType.platform, billDTO.platformId, NotificationRoute.publisher_payment);
                        } catch (err) {
                            logger.error(`${err.stack}\n${new Error().stack}`);
                        }
                    }
                    bills.push(bill);
                    if (ach.accountNumber && ach.amount > 0) {
                        achs.push(ach);
                    }
                }
            }
            if (achs && achs.length > 0) {
                try {
                    await this.generateAchFile(achs, endDate);
                } catch (err) {
                    logger.error(`${err.stack}\n${new Error().stack}`);
                }
            }
            if (bills && bills.length > 0) {
                res.json(bills);
            } else {
                res.json({ status: 'fail', message: 'no bills gererated' });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public getOutstandingAmount = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
            const authorizePayload = { publisherId: publisherUser.publisherId };
            await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewDashboard);

            let endDate: Date = new Date();
            if (req.params.endDate) {
                // we are expecting this date to be in UTC format
                endDate = new Date(req.params.endDate);
            }

            const platforms = await this.publisherPlatformDAO.findAllPlatformsByPublisherId(publisherUser.publisherId, req.header('zone'));
            let outstandingAmount = 0;
            if (platforms && platforms.length > 0) {
                for (const platform of platforms) {

                    // start date should be always picked from the last bill end date
                    const lastBillDTO: BillDTO = await this.publisherBillDAO.getLastBillByPlatformId(platform._id.toString(), req.header('zone'));
                    // for the first time publishers, there wont be any last bill
                    let startDate: Date = null;
                    if (lastBillDTO) {
                        startDate = lastBillDTO.billEndDate;
                    } else {
                        if (endDate.getUTCMonth() === 0) {
                            startDate = new Date(endDate.getUTCFullYear() - 1, 11, endDate.getUTCDate(),
                                0, 0, 0);
                        } else {
                            startDate = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth() - 1, endDate.getUTCDate(),
                                0, 0, 0);
                        }
                    }

                    let i = 1; let currentCycleAmountWithoutAnyCut = 0;
                    const assets = await this.publisherAssetDAO.findAllAssetsByPlatformId(platform._id);
                    if (assets && assets.length > 0) {
                        for (const asset of assets) {
                            const assetAmount = await this.publisherBillDAO.getAmount(asset.codeSnippetId, startDate, endDate, req.header('zone'));
                            if (assetAmount > 0) {
                                currentCycleAmountWithoutAnyCut += assetAmount;
                                i++;
                            }
                        }
                    }

                    const result = await this.getAmounts(platform._id.toString(), currentCycleAmountWithoutAnyCut, req.header('zone'));
                    outstandingAmount = outstandingAmount + result.amount;
                }
            }
            res.json({ status: true, outstandingAmount });
        } catch (err) {
            catchError(err, next);
        }
    }

    // to get platformid bill generation
    public generateBillByPlatformId = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            this.authorizeAdmin.authorize(req.email);

            let platformId; let startDate: Date; let endDate: Date;
            if (req.params.platformId) {
                platformId = req.params.platformId;
            }
            if (req.params.startDate) {
                startDate = new Date(req.params.startDate);
            }
            if (req.params.endDate) {
                endDate = new Date(req.params.endDate);
            }

            const platforms = await this.publisherPlatformDAO.findAllForBills(platformId);
            const billObj: any = { };
            if (platforms && platforms.length > 0) {
                for (const platform of platforms) {
                    const paymentMethod = await this.publisherBankDetailDAO.findByPlatformId(platform._id, req.header('zone'));
                    if (paymentMethod && paymentMethod.length > 0) {
                        billObj.paymentMethodId = paymentMethod[0]._id;
                    } else {
                        billObj.paymentMethodId = null;
                    }
                    billObj.publisherId = platform.publisherId;
                    billObj.platformId = platform._id;
                    billObj.billingCycle = `${startDate}-${endDate}`;
                    billObj.transactionNumber = '';
                    const assetsList = Array();
                    let i = 1; let totalAmount = 0;
                    const assets = await this.publisherAssetDAO.findAllAssetsByPlatformId(platform._id);
                    if (assets && assets.length > 0) {
                        for (const asset of assets) {
                            let amount = await this.publisherBillDAO.getAmount(asset.codeSnippetId, startDate, endDate, req.header('zone'));
                            amount = (amount * config.get<number>('docereeProfitCalculationValue') / 100);
                            if (amount > 0) {
                                assetsList.push({ no: i, name: asset.name, dimensions: asset.assetDimensions, amount, duration: '' });
                                totalAmount += amount;
                                i++;
                            }
                        }
                    }
                    billObj.items = assetsList;
                    billObj.amount = totalAmount;
                    billObj.billStatus = 'pending';
                    const randomBillNumber = randomString({ length: 10, numeric: true, letters: false, special: false });
                    const taxAmount = await this.publisherBillDAO.getTaxAmount(totalAmount, req.header('zone'));
                    billObj.tax = taxAmount;
                    billObj.billNumber = randomBillNumber;
                    billObj.billDate = new Date();
                    // billObj.paidDate = new Date();
                    billObj.created = { at: new Date(), by : '' };
                    billObj.modified = { at: new Date(), by : '' };
                    billObj.deleted = false;
                    if (assetsList.length > 0 && totalAmount > 0) {
                        await this.publisherBillDAO.create(billObj, req.header('zone'));
                    }
                }
            }
            if (billObj.items) {
                res.json(billObj);
            } else {
                res.json({ status: 'fail', message: 'no bills gererated' });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    public downloadBillById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const billDTO = await this.publisherBillDAO.findById(req.params.id, req.header('zone'));
            if (billDTO) {
                const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
                if (publisherUser.publisherId.toString() !== billDTO.publisherId.toString()) {
                    logger.error(`Bill ID [${req.params.id}] passed in request param does not
                    belong to logged in publisher [${req.email}]`);
                    throw new HandledApplicationError(500, 'Bill ID passed in request param does not belong to logged in publisher');
                }
                const authorizePayload = { platformId: billDTO.platformId };
                await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentBill);

                const tmp = require('tmp-promise');
                const o = await tmp.dir({ unsafeCleanup: true });
                const timestamp = new Date().valueOf();
                const filepath = `BILL-${timestamp}.pdf`;
                const fullpath = path.join(o.path, filepath);
                await this.createBillPdf(billDTO, fullpath, req.header('zone'));

                fs.exists(fullpath, (existed: any) => {
                    if (existed) {
                        res.writeHead(200, {
                            'Content-Type': 'application/octet-stream'
                        });
                        fs.createReadStream(fullpath).pipe(res);
                        o.cleanup();
                    } else {
                        res.writeHead(400, { 'Content-Type': 'text/plain' });
                        res.end('ERROR File does not exist');
                    }
                });
            } else {
                logger.error(`Invalid bill download request for bill id [${req.params.id}]`);
                res.writeHead(400, { 'Content-Type': 'text/plain' });
                res.end('ERROR Invalid bill download request');
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    // to send an email bill pdf
    public emailBillById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const billDTO = await this.publisherBillDAO.findById(req.params.id, req.header('zone'));
            if (billDTO) {
                const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
                if (publisherUser.publisherId.toString() !== billDTO.publisherId.toString()) {
                    logger.error(`Bill ID [${req.params.id}] passed in request param does not
                    belong to logged in publisher [${req.email}]`);
                    throw new HandledApplicationError(500, 'Bill ID passed in request param does not belong to logged in publisher');
                }
                const authorizePayload = { platformId: billDTO.platformId };
                await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentBill);

                await this.sendEmail(billDTO, req.email, req.header('zone'));
                res.json({ status: 'success', message: 'Email has been sent successfully' });
            } else {
                logger.error(`Invalid bill email request for bill id [${req.params.id}]`);
                res.json({ status: 'error', message: 'Invalid bill email request' });
            }
        } catch (err) {
            catchError(err, next);
        }
    }

    // to get publisher bills
    public getBills = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
            const listOfBills = await this.publisherBillDAO.findAllByPublisherId(publisherUser.publisherId, req.header('zone'));
            const platformIds = listOfBills.map((x) => mongoose.Types.ObjectId(x.platformId));
            const platformDTOs = await this.publisherPlatformDAO.findByIds(platformIds, req.header('zone'));
            const accountNumberByPlatform = new Map();
            for (const p of platformIds) {
                const authorizePayload = { platformId: p.toString() };
                await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentBill);

                const bankDetail = await this.publisherBankDetailDAO.findByPlatformId(p.toString(), req.header('zone'));
                let accountNumber = bankDetail[0].accountNumber;
                accountNumber = accountNumber.replace(/\d(?=\d{4})/g, '*');
                accountNumberByPlatform.set(p.toString(), accountNumber);
            }
            const results: any[] = [];
            for (const l of listOfBills) {
                const result: any = JSON.parse(JSON.stringify(l));
                result.platformName = platformDTOs.find((x) => (x._id.toString() === l.platformId)).name;
                result.accountNumber = accountNumberByPlatform.get(l.platformId);
                results.push(result);
            }
            res.json(results);
        } catch (err) {
            catchError(err, next);
        }
    }

    public getBillById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
            const billDTO = await this.publisherBillDAO.findById(req.params.id, req.header('zone'));
            if (publisherUser.publisherId !== billDTO.publisherId) {
                logger.error(`Bill ID [${req.params.id}] passed in request param does not
                belong to logged in publisher [${req.email}]`);
                throw new HandledApplicationError(500, 'Bill ID passed in request param does not belong to logged in publisher');
            }
            const authorizePayload = { platformId: billDTO.platformId };
            await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPaymentBill);

            res.json(billDTO);
        } catch (err) {
            catchError(err, next);
        }
    }

    // to get publisher pending bills
    public getAllPendingBills = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            this.authorizeAdmin.authorize(req.email);
            const listOfBills = await this.publisherBillDAO.findPendingBills(req.header('zone'));
            res.json(listOfBills);
        } catch (err) {
            catchError(err, next);
        }
    }

    public updateBill = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
          req
            .checkBody('_id', "Mandatory field '_id' missing in request body")
            .exists();
          const dto = await this.publisherBillDAO.findById(req.params.id, req.header('zone'));
          dto.transactionNumber = req.body.transactionNumber;
          dto.billStatus = 'Paid';
          dto.paidDate = new Date(req.params.expiryDate);
          dto.modified = { at: new Date(req.params.expiryDate), by: req.email };
          await this.publisherBillDAO.update(req.params.id, dto, req.header('zone'));
          res.json({ success: true, message: `${dto.billNumber} updated successfully` });
        } catch (err) {
            catchError(err, next);
        }
    }

    public markBillAsPaid = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            /*req
              .checkBody('_id', "Mandatory field '_id' missing in request body")
              .exists(); */
            const dto = await this.publisherBillDAO.findById(req.body.billId, req.header('zone'));
            dto.transactionNumber = req.body.transactionNumber;
            dto.billStatus = 'Paid';
            dto.paidDate = new Date(req.body.transactionDate);
            dto.modified = { at: new Date(), by: req.email };
            await this.publisherBillDAO.update(req.body.billId, dto, req.header('zone'));
            res.json({ success: true, message: `${dto.billNumber} updated successfully` });
          } catch (err) {
              catchError(err, next);
          }
    }

    public generatePublisherBillsForAdmin = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            this.authorizeAdmin.authorize(req.email);
            const bills = await this.publisherBillDAO.billsGroupbyPublisherIdForAdmin(req.params.type, req.header('zone'));
            const publisherIds = [];
            const platformIds = new Set();
            for (const bill of bills) {
                publisherIds.push(bill._id.publisherId);
                for (const detail of bill.data) {
                    const platformId = detail.platformId;
                    platformIds.add(mongoose.Types.ObjectId(platformId));
                }
            }
            const publisherPlatforms = await this.publisherPlatformDAO.findByIds(Array.from(platformIds), req.header('zone'));
            const platformNames = new Map();
            for (const platform of publisherPlatforms) {
                const platformId: string = platform._id;
                platformNames.set(platformId.toString(), platform.name);
            }
            const publishers: PublisherDTO[] = await this.publisherDAO.findByIds(publisherIds);
            const billDetails = [];
            for (const publisher of publishers) {
                for (const bill of bills) {
                    if (bill._id.publisherId.equals(publisher._id)) {
                        const details: any = { };
                        details.publisherId = publisher._id;
                        details.publisherName = publisher.organizationName;
                        let totalPublisherAmount = 0;
                        let totalPublisherReceivedAmount = 0;
                        let totalPublisherOutstandingAmount = 0;
                        const platformDetails = [];
                        for (const data of bill.data) {
                            const platformDetail: any = { };
                            platformDetail.outstandingAmount = 0;
                            platformDetail.paidAmount = 0;
                            platformDetail.billId = data.billId;
                            platformDetail.billDate = data.billDate;
                            platformDetail.billNumber = data.billNumber;
                            totalPublisherAmount = totalPublisherAmount + data.amount;
                            platformDetail.platformId = data.platformId;
                            const platformId: string = platformDetail.platformId;
                            platformDetail.platformName = platformNames.get(platformId.toString());
                            platformDetail.amount = data.amount;
                            platformDetail.billStatus = data.billStatus;
                            if (data.billStatus === 'Paid') {
                                totalPublisherReceivedAmount = totalPublisherReceivedAmount + data.amount;
                                platformDetail.paidAmount = data.amount;
                                platformDetail.transactionNumber = data.transactionNumber;
                                platformDetail.paidDate = data.paidDate;
                            } else {
                                totalPublisherOutstandingAmount = totalPublisherOutstandingAmount + data.amount;
                                platformDetail.outstandingAmount = data.amount;
                            }
                            platformDetail.billStartDate = data.billStartDate;
                            platformDetail.billEndDate = data.billEndDate;
                            platformDetails.push(platformDetail);
                        }
                        details.platformDetails = platformDetails;
                        details.amountBilled = totalPublisherAmount;
                        details.amountPaid = totalPublisherReceivedAmount;
                        details.amountOutstanding = totalPublisherOutstandingAmount;
                        details.showPlatform = false;
                        billDetails.push(details);
                    }
                }
            }
            return res.json(billDetails);
          } catch (err) {
              catchError(err, next);
          }
    }
    private async getAmounts(platformId: string, currentCycleTotalAmount: number, zone: string): Promise<any> {
        // get the total amount earned by the particular publisher
        // should include amount + bonus amount
        const amountAndBonusAndCut: any = await this.publisherBillDAO.getTotalAmountAndBonusAndCutByPlatformId(platformId, zone);

        // get the breakage
        const platformDTO: PublisherPlatformDTO = await this.publisherPlatformDAO.findById(platformId, zone);
        const defaultPercentage: number = config.get<number>('docereeProfitCalculationValue') / 100;

        let totalAmtPaid: number = amountAndBonusAndCut.amount + amountAndBonusAndCut.bonus
        + amountAndBonusAndCut.cut + currentCycleTotalAmount ;
        const totalBonusPaid: number = amountAndBonusAndCut.bonus;

        // 0(68)
        // 0(95), 1000(85), 1200(68)
        // 500, 1000, 1100, 1200, 6000

        let bonusToBePaid: number = 0;
        if (platformDTO.paymentSlots && platformDTO.paymentSlots.length === 3) {
            let a: number = platformDTO.paymentSlots[1].thresholdAmount - platformDTO.paymentSlots[0].thresholdAmount;
            let p: number = platformDTO.paymentSlots[0].percentageGiven / 100;
            if (totalAmtPaid - a >= 0) {
                bonusToBePaid = bonusToBePaid + (a) * (p - defaultPercentage);
                totalAmtPaid = totalAmtPaid - a;
                a = platformDTO.paymentSlots[2].thresholdAmount - platformDTO.paymentSlots[1].thresholdAmount;
                p = platformDTO.paymentSlots[1].percentageGiven / 100;
                if (totalAmtPaid - a >= 0) {
                    bonusToBePaid = bonusToBePaid + (a) * (p - defaultPercentage);
                } else {
                    bonusToBePaid = bonusToBePaid + totalAmtPaid * (p - defaultPercentage);
                }
            } else {
                bonusToBePaid = bonusToBePaid + totalAmtPaid * (p - defaultPercentage);
            }
        }

        if (platformDTO.paymentSlots && platformDTO.paymentSlots.length === 2) {
            const a: number = platformDTO.paymentSlots[1].thresholdAmount - platformDTO.paymentSlots[0].thresholdAmount;
            const p: number = platformDTO.paymentSlots[0].percentageGiven / 100;
            if (totalAmtPaid - a >= 0) {
                bonusToBePaid = bonusToBePaid + (a) * (p - defaultPercentage);
            } else {
                bonusToBePaid = bonusToBePaid + totalAmtPaid * (p - defaultPercentage);
            }
        }

        if (platformDTO.paymentSlots && platformDTO.paymentSlots.length === 1) {
            const a: number = platformDTO.paymentSlots[0].thresholdAmount;
            const p: number = platformDTO.paymentSlots[0].percentageGiven / 100;
            if (totalAmtPaid - a >= 0) {
                bonusToBePaid = bonusToBePaid + (a) * (p - defaultPercentage);
                totalAmtPaid = totalAmtPaid - a;
            } else {
                bonusToBePaid = bonusToBePaid + totalAmtPaid * (p - defaultPercentage);
            }
        }

        let currentCycleBonus: number = 0;
        if (bonusToBePaid - totalBonusPaid > 0) {
            currentCycleBonus = bonusToBePaid - totalBonusPaid;
        }

        const currentCycleAmount: number = (currentCycleTotalAmount) * defaultPercentage;
        const currentCycleCut: number = currentCycleTotalAmount - currentCycleAmount - currentCycleBonus;
        return { amount: currentCycleAmount, bonus: currentCycleBonus, cut: currentCycleCut };
    }
    private async createBillPdf(bill: any, fullpath: string, zone: string) {
        const doc = new PDFDocument({ margin: 50 });
        this.generateHeader(doc, zone);
        await this.generateCustomerInformation(doc, bill, zone);
        this.generateBillTable(doc, bill, zone);
        doc.end();
        doc.pipe(fs.createWriteStream(fullpath, { flags: 'w' }));
    }

    private generateHeader(doc: any, zone: any) {
        doc
            .image('./resources/logo2.png', 450, 10, { width: 80, align: 'right' })
            .fontSize(9)
            .font('Helvetica-Bold').text('DOCEREE INC.', 450, 35, { lineBreak: true, })
            .fontSize(8)
            .font('Helvetica');
        if (zone === '1') {
            doc
            .text('14, Walsh Drive, Suite#302', 450, 45, { lineBreak: true, })
            .text('Parsippany', 450, 55, { lineBreak: true, })
            .text('New Jersy 07054', 450, 65, { lineBreak: true, })
            .text('United States', 450, 75, { lineBreak: true, });
        } else if (zone === '2') {
            doc
            .text('38, Okhla Phase 3 Rd', 450, 45, { lineBreak: true, })
            .text('Okhla Phase III', 450, 55, { lineBreak: true, })
            .text('Okhla Industrial Area', 450, 65, { lineBreak: true, })
            .text('New Delhi, Delhi 110020', 450, 75, { lineBreak: true, });
        }
        doc
            .text('www.doceree.com', 450, 85, { lineBreak: true, })
            .fillColor('#037ffc')
            .fontSize(23)
            .font('Helvetica-Bold').text('{', 25, 100, { align: 'left' })
            .fontSize(14)
            .font('Helvetica-Bold').text('BILL', 37, 103, { align: 'left' })
            .fontSize(23)
            .font('Helvetica-Bold').text('}', 67, 100, { align: 'left' })
            .moveDown();
    }
    private generateFooter(doc: any) {
        doc
        .fillColor('#000')
        .fontSize(8)
        .font('Helvetica').text('If you find issue in this invoice, Please contact', 200, 750, { lineBreak: false })
        .fillColor('#037ffc')
        .font('Helvetica').text('accounts@doceree.com', 365, 750, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text('or call toll-free no.', 455, 750, { lineBreak: false })
        .font('Helvetica-Bold').text('1-888-261-4561', 520, 750, { lineBreak: false })

        .image('./resources/publisher_bill_footer.jpg', 188, 766, { width: 424, height: 21 })
        .fillColor('#fff')
        .fontSize(10)
        .font('Helvetica-Bold').text('Thank you for your business!', 200, 771, { lineBreak: false });
    }
    private async generateCustomerInformation(doc: any, bill: any, zone: string) {
        const billtoInfo: any = await this.publisherDAO.findById(bill.publisherId);
        const platform: any = await this.publisherPlatformDAO.findById(bill.platformId, zone);
        const publisherUser: any = await this.userDAO.findByEmail(billtoInfo.user.email);
        let billingAddress: any;
        if (!billtoInfo.address.addressLine2) {
            billingAddress = billtoInfo.address.addressLine1;
        } else {
            billingAddress = `${billtoInfo.address.addressLine1}, ${billtoInfo.address.addressLine2}`;
        }
        const billto = {
            businessName: billtoInfo.organizationName,
            address: billingAddress,
            city: billtoInfo.address.city,
            state: billtoInfo.address.state,
            country: billtoInfo.address.country
        };
        const iDate = new Date(new Date(bill.billDate).getFullYear(),
        new Date(bill.billDate).getMonth(), new Date(bill.billDate).getDate());
        const billDate = `${iDate.getMonth() + 1}/${iDate.getDate()}/${iDate.getFullYear()}`;
        doc
            .image('./resources/publisher_bill_lhs.jpg', 0, 125, { width: 187, height: 660 })
            .lineWidth(425)
            .lineCap('butt')
            .moveTo(400, 125)
            .lineTo(400, 740)
            .stroke('#dee3e3')
            .fillColor('#000')
            .fontSize(8)

            .font('Helvetica-Bold').text('Bill No.:', 200, 140, { lineBreak: true, })
            .font('Helvetica').text(`${bill.billNumber}`, 235, 140, { lineBreak: true, })

            .font('Helvetica-Bold').text('Publisher Name:', 390, 140, { lineBreak: true, })
            .font('Helvetica').text(`${publisherUser.firstName} ${publisherUser.lastName}`, 457, 140, { lineBreak: true, })

            .font('Helvetica-Bold').text('Bill Date:', 200, 155, { lineBreak: true })
            .font('Helvetica').text(`${billDate}`, 237, 155, { lineBreak: true })

            .font('Helvetica-Bold').text('Publisher Address:', 390, 170, { lineBreak: true })
            // tslint:disable-next-line:max-line-length
            .font('Helvetica').text(`${billto.businessName}, ${billto.address}, ${billto.city}, ${billto.state}, ${billto.country}`, 467, 170, { lineBreak: true })

            .font('Helvetica-Bold').text('Billing Period:', 200, 170, { lineBreak: true })
            // tslint:disable-next-line:max-line-length
            .font('Helvetica').text(`${moment(bill.billStartDate).format('MM/DD/YYYY')} to ${moment(bill.billEndDate).format('MM/DD/YYYY')}`, 256, 170, { lineBreak: true })

            .font('Helvetica-Bold').text('Payment terms:', 200, 185, { lineBreak: true })
            // tslint:disable-next-line:max-line-length
            .font('Helvetica').text(`${moment(bill.billStartDate).format('MM/DD/YYYY')} to ${moment(bill.billEndDate).format('MM/DD/YYYY')}`, 263, 185, { lineBreak: true })

            .font('Helvetica-Bold').text('Platform Name:', 200, 200, { lineBreak: true })
            .font('Helvetica').text(`${platform.name}`, 263, 200, { width: 140, lineBreak: true })

            .font('Helvetica-Bold').text('Attention:', 200, 230, { lineBreak: true })
            .font('Helvetica').text(`Bill for ${publisherUser.firstName} ${publisherUser.lastName} for ${platform.name} for ${moment(bill.billStartDate).format('MM/DD/YYYY')} to ${moment(bill.billEndDate).format('MM/DD/YYYY')}`, 240, 230, { lineBreak: true });
    }

    private generateTableRow(doc: any, y: any, c1: any, c2: any, c3: any, c4: any, length: any, zone: any) {
        if (length !== 0 && length % 21 === 0) {
            this.generateFooter(doc);
            doc.addPage();
            doc
            .image('./resources/publisher_bill_lhs.jpg', 0, 0, { width: 187, height: 785 })
            .lineWidth(425)
            .lineCap('butt')
            .moveTo(400, 0)
            .lineTo(400, 740)
            .stroke('#f2f2f2');
        }
        if (c4 === 'Amount') {
            doc.font('Helvetica-Bold')
                .fillColor('#037ffc')
                .fontSize(9)
                .text(c1, 200, y - 10, { width: 30, align: 'justify' })
                .text(c2, 230, y - 10, { width: 200, align: 'justify' })
                .text(c3, 450, y - 10, { width: 80, align: 'justify' });
            doc.font('Helvetica-Bold').text(`${c4}`, 550, y - 10, { width: 90, align: 'justify' })
                .lineWidth(1)
                .moveTo(187, 260)
                .lineTo(650, 260)
                .stroke('#037ffc');
        } else {
            doc.font('Helvetica')
                .fillColor('#000')
                .fontSize(8)
                .text(c1, 200, y - 10, { width: 30, align: 'justify' })
                .text(c2, 230, y - 10, { width: 200, align: 'justify' })
                .text(c3, 450, y - 10, { width: 80, align: 'justify' });
            if (zone === '1') {
                doc.font('Helvetica').text(`$${parseFloat(c4).toFixed(2)}`, 550, y - 10, { width: 90, align: 'justify' });
            } else if (zone === '2') {
                doc.font('Helvetica').text(`Rs. ${parseFloat(c4).toFixed(2)}`, 550, y - 10, { width: 90, align: 'justify' });
            }
        }

    }

    private generateBillTable(doc: any, bill: any, zone: any) {
        let width = 1;
        let billTableTop = 230;
        let afterTableRowPosition = billTableTop;
        bill.items = [{ no: 'S.No', name: 'Name', dimensions: 'Dimension', amount: 'Amount' }, ...bill.items];
        for (let i = 0; i < bill.items.length; i++) {
            width++;
            const item = bill.items[i];
            if (i !== 0 && i % 21 === 0) {
                billTableTop = 0;
                width = 1;
            }
            const position = billTableTop + (width + 1) * 20;
            afterTableRowPosition = position;
            this.generateTableRow(doc, position, item.no, item.name, item.dimensions, item.amount, i, zone);
        }
        this.createPdfBelowTable(doc, afterTableRowPosition, bill, zone);
        this.generateFooter(doc);
    }
    private createPdfBelowTable(doc: any, afterTableRowPosition: any, bill: any, zone: any) {
        let currencySymbol: string;
        if (zone === '1') {
            currencySymbol = '$';
        } else {
            currencySymbol = 'Rs.';
        }
        doc.font('Helvetica')

        .lineWidth(1)
        .moveTo(187, afterTableRowPosition + 20)
        .lineTo(650, afterTableRowPosition + 20)
        .stroke('#037ffc')

        .fillColor('#037ffc')
        .font('Helvetica-Bold').text('Total Amount:', 334, afterTableRowPosition + 30, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} ${parseFloat(bill.amount).toFixed(2)}`, 400, afterTableRowPosition + 30, { lineBreak: false })

        .fillColor('#037ffc')
        .font('Helvetica-Bold').text('(+) Special Offer:', 323, afterTableRowPosition + 45, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} 0`, 400, afterTableRowPosition + 45, { lineBreak: false })
        .fillColor('#037ffc')
        .font('Helvetica-Bold').text('(+) Taxes:', 352, afterTableRowPosition + 60, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} 0`, 400, afterTableRowPosition + 60, { lineBreak: false })
        .fillColor('#037ffc')
        .font('Helvetica-Bold').text('Net Amount:', 341, afterTableRowPosition + 75, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${currencySymbol} ${parseFloat(bill.amount).toFixed(2)}`, 400, afterTableRowPosition + 75, { lineBreak: false })

        .lineWidth(425)
        .lineCap('butt')
        .moveTo(400, afterTableRowPosition + 90)
        .lineTo(400, 741)
        .stroke('#fff')

        .fillColor('#037ffc')
        .font('Helvetica-Bold').text('Payment Status:', 200, afterTableRowPosition + 100, { lineBreak: false })
        .fillColor('#000')
        .font('Helvetica').text(`${bill.billStatus}`, 282, afterTableRowPosition + 100, { lineBreak: false })
        .fillColor('#037ffc');
        // .font('Helvetica-Bold').text('Credited To:', 200, afterTableRowPosition + 105, { lineBreak: false })
    }
    private async sendEmail(bill: any, email: string, zone: string) {
        const tmp = require('tmp-promise');
        const o = await tmp.dir({ unsafeCleanup: true });
        const timestamp = new Date().valueOf();
        const filepath = `BILL-${timestamp}.pdf`;
        const fullpath = path.join(o.path, filepath);
        await this.createBillPdf(bill, fullpath, zone);

        const attachment = fullpath;
        const attachments = [{ filename: filepath, path: attachment }];
        const duration = `${moment(bill.billStartDate).format('MM/DD/YYYY')}-${moment(bill.billEndDate).format('MM/DD/YYYY')}`;
        const billMonth = dateformat(bill.billEndDate, 'mmmm');
        const url: string = config.get<string>('mail.web-portal-redirection-url');
        const toUser: UserDTO = await this.userDAO.findByEmail(email);
        if (toUser.hasPersonalProfile) {
            if (toUser.isSubscribedForEmail) {
                await this.publisherMail.sendMonthlyPayment(email,
                    toUser.firstName,
                    billMonth,
                    attachments,
                    `${url}/publisher/payments`,
                    `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(email))}`
                );
            }
        }
    }

    private async generateAchFile(achs: any[], billDate: any) {
        let content: string = '';
        const rowHeader1: string = 'Indicator,File ID (Modifier),File creation date,File creation time,Total trxn,Total ACH credit amount,Total ACH debit amount,Batch Count,,';
        const rowHeader2: string = 'Indicator,Service class code,Chase Acct,SEC Code,Entry description,Delivery by date,Batch credit amount,Batch debit amount,Batch number,Trxn in Batch';
        const rowHeader3: string = 'Indicator,Trxn Code,Routing Num,Acct number,Trxn amount,ID Number,Payee name,Trxn ID,Addenda,';

        const rowIndicator1: string = '1';
        const currentDate: Date = new Date();
        const fileId: string = dateformat(currentDate, 'mmmm dS');
        const fileCreationDate: string = dateformat(currentDate, 'yymmdd');
        const fileCreationTime: string = dateformat(currentDate, 'hhMM');
        const totalTxns: number = achs.length;
        let totalCreditAmount: number = 0;
        for (const a of achs) {
            totalCreditAmount = totalCreditAmount + this.toZeroDecimal((parseFloat(a.amount) * 100));
        }
        const totalDebitAmount: number = 0;
        const batchCount: number = 1;
        const rowValue1 = `${rowIndicator1}, ${fileId}, ${fileCreationDate}, ${fileCreationTime}, ${totalTxns}, ${totalCreditAmount}, ${totalDebitAmount}, ${batchCount}`;
        content = `${content}${rowHeader1}\n${rowValue1}\n`;

        const rowIndicator2: string = '5';
        const serviceClass: number = 220;
        const chaseAccount: number = 4964983;
        const secCode: string = 'CCD';
        const entryDescription: string = 'PAYMENTS';
        const dateTwoDaysLater: Date = new Date(currentDate.setDate(currentDate.getDate() + 2));
        const deliveryDate: Date = dateformat(dateTwoDaysLater, 'yymmdd');
        const batchCreditAmount: number = totalCreditAmount;
        const batchDebitAmount: number = 0;
        const batchNumber: number = 100;
        const totalBatchTxns: number = achs.length;
        const rowValue2: string = `${rowIndicator2}, ${serviceClass}, ${chaseAccount}, ${secCode}, ${entryDescription}, ${deliveryDate}, ${batchCreditAmount}, ${batchDebitAmount}, ${batchNumber}, ${totalBatchTxns}`;
        content = `${content}${rowHeader2}\n${rowValue2}\n`;

        let row3content = '';
        const rowIndicator3: string = '6';
        const transactionCode: number = 22;
        let i: number = 1;
        for (const a of achs) {
            const routingNumber = a.routingNumber;
            const accountNumber = a.accountNumber;
            const trxnAmount = this.toZeroDecimal(parseFloat(a.amount) * 100);
            const idNumber = a.payeeId;
            const payeeName = a.payeeName;
            const trxnId = `${batchNumber}${i}`;
            const addenda = a.addenda;

            const rowValue3: string = `${rowIndicator3}, ${transactionCode}, ${routingNumber}, ${accountNumber}, ${trxnAmount}, ${idNumber}, ${payeeName}, ${trxnId}, ${addenda}`;
            i++;
            row3content = `${row3content}${rowValue3}\n`;
        }
        content = `${content}${rowHeader3}\n${row3content}`;

        const tmp = require('tmp-promise');
        const o = await tmp.dir({ unsafeCleanup: true });
        const timestamp = new Date().valueOf();
        const filepath = `ACH-${timestamp}.csv`;
        const fullpath = path.join(o.path, filepath);
        fs.writeFileSync(fullpath, content);

        const attachment = fullpath;
        const attachments = [{ filename: filepath, path: attachment }];
        const billMonth = dateformat(billDate, 'mmmm');
        await this.publisherMail.sendMonthlyAch(
            'jeril.jacob@doceree.com',
            billMonth,
            attachments
        );
    }

    private toTwoDecimal(num: number) {
        const with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        return parseFloat(with2Decimals);
    }

    private toZeroDecimal(num: number) {
        const with0Decimals = num.toString().match(/^-?\d+(?:\.)?/)[0];
        return parseInt(with0Decimals, 10);
    }
}
