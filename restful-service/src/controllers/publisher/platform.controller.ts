import * as AWS from 'aws-sdk';
import config from 'config';
import express from 'express';
import csv from 'fast-csv';
import fs from 'fs';
import moment = require('moment');
import { relativeTimeThreshold } from 'moment';
import RoleDAO from '../../daos/admin/role.dao';
import PublisherAssetDAO from '../../daos/publisher/asset.dao';
import DashboardDAO from '../../daos/publisher/dashboard.dao';
import PublisherApplicationTypeDAO from '../../daos/publisher/publisher-application-type.dao';
import PublisherPlatformDAO from '../../daos/publisher/publisher-platform.dao';
import PublisherSiteTypeDAO from '../../daos/publisher/publisher-site-type.dao';
import PublisherUserDAO from '../../daos/publisher/publisher-user.dao';
import PublisherDAO from '../../daos/publisher/publisher.dao';
import UserTokenDAO from '../../daos/user-token.dao';
import UserDAO from '../../daos/user.dao';
import RoleDTO from '../../dtos/admin/role.dto';
import PublisherApplicationTypeDTO from '../../dtos/publisher/publisher-application-type.dto';
import PublisherPlatformDTO from '../../dtos/publisher/publisher-platform.dto';
import PublisherSiteTypeDTO from '../../dtos/publisher/publisher-site-type.dto';
import PublisherDTO from '../../dtos/publisher/publisher.dto';
import UserTokenDTO from '../../dtos/user-token.dto';
import UserDTO from '../../dtos/user.dto';
import catchError from '../../error/catch-error';
import HandledApplicationError from '../../error/handled-application-error';
import IAuthenticatedRequest from '../../guards/authenticated.request';
import { RedisHelper } from '../../redis/redis-helper';
import { AlexaScore } from '../../util/alexa-score';
import { PublisherPermissions, UserTypes } from '../../util/constants';
import { HashPwd } from '../../util/hash-pwd';
import { GeneralMail } from '../../util/mail/general-mail';
import { PublisherMail } from '../../util/mail/publisher-mail';
import { logger } from '../../util/winston';
import { AuthorizePublisher } from './authorize-publisher';
import { PublisherUserHelper } from './publisher-user.helper';
const dateformat = require('dateformat');
const escapeHtml = require('escape-html');

export class PlatformController {
  private readonly publisherApplicationTypeDAO: PublisherApplicationTypeDAO;
  private readonly publisherPlatformDAO: PublisherPlatformDAO;
  private readonly publisherSiteTypeDAO: PublisherSiteTypeDAO;
  private readonly publisherDAO: PublisherDAO;
  private readonly hashPwd: HashPwd;
  private readonly publisherUserDAO: PublisherUserDAO;
  private readonly assetDAO: PublisherAssetDAO;
  private readonly userDAO: UserDAO;
  private readonly publisherMail: PublisherMail;
  private readonly alexaScore: AlexaScore;
  private readonly redisHelper: RedisHelper;
  private readonly roleDAO: RoleDAO;

  private readonly s3Access: AWS.S3;
  private readonly s3Params: any;
  private readonly authorizePublisher: AuthorizePublisher;
  private readonly userTokenDAO: UserTokenDAO;
  private readonly generalMail: GeneralMail;
  private readonly headerFilePath: string = './resources/scripts/platform-header.html';
  private readonly docereeUsLoginSnippetFilePath: string = './resources/scripts/doceree-login.us.snippet.html';
  private readonly docereeIndiaLoginSnippetFilePath: string = './resources/scripts/doceree-login.india.snippet.html';
  private readonly docereeLogoutSnippetFilePath: string = './resources/scripts/doceree-logout.snippet.html';

  private readonly mobilePISnippet0FilePath: string = './resources/scripts/mobile-pi-snippet-0.html';
  private readonly mobilePISnippet1FilePath: string = './resources/scripts/mobile-pi-snippet-1.html';
  private readonly mobilePISnippet2FilePath: string = './resources/scripts/mobile-pi-snippet-2.html';
  private readonly mobilePISnippet3FilePath: string = './resources/scripts/mobile-pi-snippet-3.html';
  private readonly usMobileAISnippet1FilePath: string = './resources/scripts/us-mobile-ai-snippet-1.html';
  private readonly indiaMobileAISnippet1FilePath: string = './resources/scripts/india-mobile-ai-snippet-1.html';
  private readonly mobileAISnippet2FilePath: string = './resources/scripts/mobile-ai-snippet-2.html';

  private readonly publisherUserHelper: PublisherUserHelper;
  private readonly dashboardDAO: DashboardDAO;

  constructor() {
    this.publisherApplicationTypeDAO = new PublisherApplicationTypeDAO();
    this.publisherPlatformDAO = new PublisherPlatformDAO();
    this.publisherMail = new PublisherMail();
    this.hashPwd = new HashPwd();
    this.publisherSiteTypeDAO = new PublisherSiteTypeDAO();
    this.publisherDAO = new PublisherDAO();
    this.publisherUserDAO = new PublisherUserDAO();
    this.assetDAO = new PublisherAssetDAO();
    this.userDAO = new UserDAO();
    this.alexaScore = new AlexaScore();
    this.authorizePublisher = new AuthorizePublisher();
    this.userTokenDAO = new UserTokenDAO();
    this.generalMail = new GeneralMail();
    this.redisHelper = new RedisHelper();
    this.publisherUserHelper = new PublisherUserHelper();
    this.roleDAO = new RoleDAO();
    this.dashboardDAO = new DashboardDAO();

    // Set up credentials
    this.s3Access = new AWS.S3({
      accessKeyId: config.get<string>('aws-s3.accessId'),
      secretAccessKey: config.get<string>('aws-s3.secretAccessKey')
    });
    this.s3Params = {
      Bucket: config.get<string>('aws-s3.bucket'),
      region: config.get<string>('aws-s3.region')
    };
  }

  public getAppTypes = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const applicationTypes: PublisherApplicationTypeDTO[] = await this.publisherApplicationTypeDAO.findAll();
      return res.json(applicationTypes);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getUniqueAppTypes = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const applicationTypes: PublisherApplicationTypeDTO[] = await this.publisherApplicationTypeDAO.findUniqueAppType();
      return res.json(applicationTypes);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getHeaderScript = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      // console.log(req.query);
      const content = fs.readFileSync(this.headerFilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getDocereeLoginSnippetScript = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      let content: string = '';
      if (req.header('zone') === '1') {
        content = fs.readFileSync(this.docereeUsLoginSnippetFilePath, { encoding: 'utf8' });
      } else if (req.header('zone') === '2') {
        content = fs.readFileSync(this.docereeIndiaLoginSnippetFilePath, { encoding: 'utf8' });
      }
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getDocereeLogoutSnippetScript = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const content: string = fs.readFileSync(this.docereeLogoutSnippetFilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getMobilePISnippet0 = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      // console.log(req.query);
      const content = fs.readFileSync(this.mobilePISnippet0FilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getMobilePISnippet1 = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      // console.log(req.query);
      const content = fs.readFileSync(this.mobilePISnippet1FilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getMobilePISnippet2 = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      // console.log(req.query);
      const content = fs.readFileSync(this.mobilePISnippet2FilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getMobilePISnippet3 = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      // console.log(req.query);
      const content = fs.readFileSync(this.mobilePISnippet3FilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getMobileAISnippet1 = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      let content: string = '';
      if (req.header('zone') === '1') {
        content = fs.readFileSync(this.usMobileAISnippet1FilePath, { encoding: 'utf8' });
      } else if (req.header('zone') === '2') {
        content = fs.readFileSync(this.indiaMobileAISnippet1FilePath, { encoding: 'utf8' });
      }
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public getMobileAISnippet2 = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const content: string = fs.readFileSync(this.mobileAISnippet2FilePath, { encoding: 'utf8' });
      return res.json({ content });
    } catch (err) {
      catchError(err, next);
    }
  }

  public createPlatform = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const authorizePayload = { publisherId: req.body.publisherId };
      try {
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.addPlatform);
      } catch (error) {
        // there may be a case that there is no platform asssociated with the user yet
        const publisherDTO: PublisherDTO = await this.publisherDAO.findByEmail(req.email);
        if (publisherDTO._id.toString() !== req.body.publisherId) {
            throw error;
        }
      }

      // TODO: validate request body
      const platformDTO: PublisherPlatformDTO = await this.toPublisherPlatformDTO(req);
      if (req.files && req.files.file) {
        platformDTO.isHCPUploaded = true;
      }
      platformDTO.created = { at: new Date(), by: req.email };
      const platforms: PublisherPlatformDTO[] =
        await this.publisherPlatformDAO.findAll(platformDTO.publisherId, req.header('zone'));

      // TODO: this may be a time taking operation
      platformDTO.webrank = await this.alexaScore.evaluate(platformDTO.domainUrl);
      const detail: PublisherPlatformDTO[] = await this.publisherPlatformDAO.checkPlatformNameAvailablity(req.body.name,
        platformDTO.publisherId._id, req.header('zone'));
      if (detail.length > 0) {
        return res.status(403).json({ type: 'Error', message: 'Platform name not available' });
      }
      if (platformDTO.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        const keys = this.generateKey();
        platformDTO.appTestKey = keys[0];
        platformDTO.appLiveKey = keys[1];
      }
      const result: PublisherPlatformDTO = await this.publisherPlatformDAO.create(platformDTO);

      const buttonToPlatform: string = 'todo update link';
      const url: string = config.get<string>('mail.web-portal-redirection-url');
      const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
      if (toUser.hasPersonalProfile) {
        if (toUser.isSubscribedForEmail) {
          if (platforms.length > 0) {
            this.publisherMail.sendPlatformAddedMail(
              req.email,
              toUser.firstName,
              platformDTO.name,
              `${url}/publisher/dashboard`,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
          if (platforms.length === 0) {
            this.publisherMail.sendFirstPlatformAddedMail(
              req.email,
              toUser.firstName,
              platformDTO.name,
              `${url}/publisher/dashboard`,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }

      req.body.platformId = result._id.toString();
      const userRole: RoleDTO = await this.roleDAO.getRoleByModuleAndRoleName('Publisher', 'Admin');
      req.body.userRoleId = userRole._id.toString();
      req.body.email = req.email;
      req.body.publisherId = result.publisherId.toString();
      const loggedInUser: UserDTO = await this.userDAO.findByEmail(req.email);
      const createUserResponse = await this.publisherUserHelper.createUser(loggedInUser, req, res);

      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const platform: any = publisherUser.platforms.find((x) => (result._id.toString() === x.platformId.toString()));
      if (platform.isWatching) {
        result.isWatching = true;
      }
      return res.json(result);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getPlatformPerZone = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const authorizePayload = { publisherId: publisherUser.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPlatform);
      const result: PublisherPlatformDTO[] = await this.publisherPlatformDAO.findNonDeletedByIdsAndZone(
        publisherUser.platforms.map((x) => x.platformId), req.header('zone'));
      const watchingPlatform = publisherUser.platforms.find((x) => (x.isWatching === true));
      if (watchingPlatform) {
        const watchingPlatformId = watchingPlatform.platformId.toString();
        for (const r of result) {
          r.isWatching = false;
          if (r._id.toString() === watchingPlatformId) {
            r.isWatching = true;
          }
        }
      }
      return res.json(result);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getPlatform = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const authorizePayload = { publisherId: publisherUser.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPlatform);
      // NOTE: Zone is not considered here, its intended
      const result: PublisherPlatformDTO[] = await this.publisherPlatformDAO.findNonDeletedByIds(publisherUser.platforms.map((x) => x.platformId));
      const watchingPlatforms: any[] = publisherUser.platforms.filter((x) => (x.isWatching === true));
      if (watchingPlatforms && watchingPlatforms.length > 0) {
        for (const r of result) {
          r.isWatching = false;
        }
        for (const watchingPlatform of watchingPlatforms) {
          const watchingPlatformId = watchingPlatform.platformId.toString();
          for (const r of result) {
            if (r._id.toString() === watchingPlatformId) {
              r.isWatching = true;
            }
          }
        }
      }
      return res.json(result);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getPlatformById = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      if (! await this.checkForAdminUser(req.email)) {
        const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
        const result: PublisherPlatformDTO = await this.publisherPlatformDAO.findById(req.params.id, req.header('zone'));
        const platform: any = publisherUser.platforms.find((x) => (req.params.id === x.platformId.toString()));
        if (platform.isWatching) {
          result.isWatching = true;
        }
        return res.json(result);
      } else {
        const result: PublisherPlatformDTO = await this.publisherPlatformDAO.findById(req.params.id, req.header('zone'));
        return res.json(result);
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public updatePlatform = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const authorizePayload = { platformId: req.body._id };
      if (!this.checkForAdminUser(req.email)) {
        await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.editPlatform);
      }
      // TODO: validate the inputs for publisher profile
      const dto: PublisherPlatformDTO = await this.toPublisherPlatformDTO(req);
      dto._id = req.body._id;
      dto.modified = { at: new Date(), by: req.email };

      const platformDetail = await this.publisherPlatformDAO.findById(req.body._id, req.header('zone'));
      dto.domainUrl = platformDetail.domainUrl;
      if (platformDetail.name.toLowerCase() !== dto.name.toLowerCase()) {
        const detail: PublisherPlatformDTO[] = await this.publisherPlatformDAO.checkPlatformNameAvailablity(req.body.name,
          req.body.publisherId, req.header('zone'));
        if (detail.length > 0) {
          return res.status(403).json({ type: 'Error', message: 'Platform name not available' });
        }
      }
      if (dto.isDeleted) {
        // check if platform has any data corresponding to it
        const codeSnippetIds = await this.assetDAO.listOfAssetSnippetIdsByPlatformId(req.body._id);
        const totalReachResult = await this.dashboardDAO.getReach(codeSnippetIds, req.header('zone'));
        if (totalReachResult && totalReachResult.length > 0) {
          if (totalReachResult[0].hcpCount > 0) {
            throw new HandledApplicationError(500, 'Cannot delete this platform as it has transactional data associated with it.');
          }
        }
      }

      const result = await this.publisherPlatformDAO.update(dto._id, dto, req.header('zone'));
      await this.redisHelper.updatePlatform(await this.publisherPlatformDAO.findById(dto._id.toString(), req.header('zone')));
      const url: string = config.get<string>('mail.web-portal-redirection-url');
      if (!platformDetail.isDeleted && result.isDeleted) {
        const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
        if (toUser.hasPersonalProfile) {
          if (toUser.isSubscribedForEmail) {
            this.publisherMail.sendPlatformRemovedMail(
              req.email,
              toUser.firstName,
              platformDetail.name,
              `${url}/publisher/platformCreation`,
              `${url}/publisher/platformCreation`,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }

        // we need to delete all the assets
        await this.assetDAO.deleteMany(result._id);
      } else if (!platformDetail.isDeactivated && result.isDeactivated) {
        const toUser: UserDTO = await this.userDAO.findByEmail(req.email);
        if (toUser.hasPersonalProfile) {
          if (toUser.isSubscribedForEmail) {
            this.publisherMail.sendPlatformDeactivateMail(
              req.email,
              toUser.firstName,
              `${url}/publisher/platformCreation`,
              `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(req.email))}`
            );
          }
        }
      }

      // TODO: This looks shady, why are we referring asset in platform
      if (req.body.assetDeleted) {
        await this.assetDAO.deleteMany(result._id);
      }
      if (req.body.assetDeactivated) {
        await this.assetDAO.deActivateMany(result._id);
      }
      res.json(result);
    } catch (err) {
      catchError(err, next);
    }
  }

  public updatePlatformPaymentSlots = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);
      /* The request should look like:
      {
        "id": "5d7b310c74b4210017147672",
        "paymentSlots": [
          {
            "thresholdAmount": 0,
            "percentageGiven": 95
          },
          {
            "thresholdAmount": 1000,
            "percentageGiven": 85
          },
          {
            "thresholdAmount": 1200,
            "percentageGiven": 68
          }
        ]
      }
     */
      if (userDTO && userDTO.userType.toLowerCase() === UserTypes.admin.toLowerCase()) {
          // all good, let it go ahead
          if (!req.body.id) {
            throw new HandledApplicationError(500, 'Mandatory field platform id is not provided');
          }
          const dto = await this.publisherPlatformDAO.findById(req.body.id, req.header('zone'));
          dto.paymentSlots = req.body.paymentSlots;
          const result = await this.publisherPlatformDAO.update(dto._id, dto, req.header('zone'));
          res.json(result);
      } else {
        throw new HandledApplicationError(500, 'Logged in user do not have access on this API');
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public switchToPlatform = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      throw new Error('Deprecated');
      /*const authorizePayload = { platformId: req.params.id };
      await this.authorizePublisher.authorize(req.email, authorizePayload);

      // TODO: validate the inputs for publisher profile
      const platformDetails: PublisherPlatformDTO = await this.publisherPlatformDAO.findPlatformById(req.params.id);
      await this.publisherPlatformDAO.findIsWatchingPlatformAndUpdate(platformDetails.publisherId);
      platformDetails.modified = { at: new Date(), by: req.email };
      platformDetails.isWatching = true;
      const result = await this.publisherPlatformDAO.update(platformDetails._id, platformDetails);
      res.json({ success: true, message: 'Platform switched successfully.' });*/
    } catch (err) {
      catchError(err, next);
    }
  }

  public getPlatformsByPlatformTypeAndPublisherId = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      const authorizePayload = { publisherId: publisherUser.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPlatform);
      // NOTE: We are not using zone here, this is intended
      const result: any[] = await this.publisherPlatformDAO.findPlatformsByPlatformTypeAndPublisherId(
        req.query.platformType, publisherUser.publisherId, req.header('zone')
      );
      const modifiedResult: any[] = [];
      for (const r of result) {
        if (r.platforms && r.platforms.length === 1 &&
          publisherUser.platforms.find((x) => x.platformId.toString() === r.platforms[0]._id.toString())) {
          modifiedResult.push(r);
        }
      }

      const watchingPlatform = publisherUser.platforms.find((x) => (x.isWatching === true && x.zone === req.header('zone')));
      if (watchingPlatform) {
        const watchingPlatformId = watchingPlatform.platformId.toString();
        for (const r of modifiedResult) {
          r.isWatching = false;
          if (r.platforms[0]._id.toString() === watchingPlatformId) {
            r.isWatching = true;
          }
        }
      }
      return res.json(modifiedResult);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getSiteTypes = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const siteTypes: PublisherSiteTypeDTO[] = await this.publisherSiteTypeDAO.findAll();
      return res.json(siteTypes);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getUniqueSiteTypes = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const siteTypes: PublisherSiteTypeDTO[] = await this.publisherSiteTypeDAO.findUniqueSiteType();
      res.json(siteTypes);
    } catch (err) {
      catchError(err, next);
    }
  }

  public getViewingPlatform = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const authorizePayload = { publisherId: req.params.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPlatform);
      const viewingPlatform: PublisherPlatformDTO = await this.publisherUserDAO.findWatchingPlatform(req.email,
        req.header('zone'));
      viewingPlatform.isWatching = true;
      return res.json(viewingPlatform);
    } catch (err) {
      catchError(err, next);
    }
  }

  public checkPlatformNameAvailablity = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
      if (publisherUser.publisherId.toString() !== req.body.publisherId) {
        throw new Error('Provided input publisher id does not match the users publisher id');
      }

      const authorizePayload = { publisherId: publisherUser.publisherId };
      await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewPlatform);
      const detail: PublisherPlatformDTO[] = await this.publisherPlatformDAO.checkPlatformNameAvailablity(req.body.name,
        req.body.publisherId, req.header('zone'));
      if (detail.length > 0) {
        return res.status(403).json({ type: 'Error', message: 'Platform name not available' });
      } else {
        return res.status(200).json({ type: 'Success', message: 'Platform name available' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }

  public checkPlatformURLAvailablity = async (
    req: IAuthenticatedRequest,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
    //  const publisher = await this.authorizePublisher.authorizeAndReturnPublisherDTO(req.email);
      const detail: PublisherPlatformDTO[] = await this.publisherPlatformDAO.checkPlatformURLAvailablity(req.body.url, req.header('zone'));
      if (detail.length > 0) {
        return res.status(403).json({ type: 'Error', message: 'A Platform with this URL already exists' });
      } else {
        return res.status(200).json({ type: 'Success', message: 'URL available' });
      }
    } catch (err) {
      catchError(err, next);
    }
  }
  // end

  public noPlatform = async (
    req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
  ) => {
    try {
      const publishers: PublisherDTO[] = await this.publisherDAO.findAll();
      const currentDate: Date = new Date();

      const publisherIdsFromPlatformTable: any[] = await this.publisherPlatformDAO.findAllPublisher(req.header('zone'));
      const mailSentToUsers: string[] = [];
      for (const a of publishers) {
        const id = a._id.toString();
        if (!publisherIdsFromPlatformTable.find((x) => x.toString() === id)) {
          const user = await this.userDAO.findByEmail(a.user.email);
          if (user.reminder.noPlatform) {
            let lastMailSentAt: Date = user.reminder.noPlatform.mailSentAt;
            if (!lastMailSentAt) {
              lastMailSentAt = a.created.at;
            }
            let noOfTimesReminderSent: number = 0;
            if (user.reminder.noPlatform.noOfMailsSent) {
              noOfTimesReminderSent = user.reminder.noPlatform.noOfMailsSent;
            }
            if (noOfTimesReminderSent < 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 3 * 24 * 60 * 60 * 1000) {
                this.sendSetupCompleteButNotAddedPlatformMail(user, currentDate, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else if (noOfTimesReminderSent === 3) {
              if (currentDate.getTime() - lastMailSentAt.getTime() > 7 * 24 * 60 * 60 * 1000) {
                this.sendSetupCompleteButNotAddedPlatformMail(user, currentDate, noOfTimesReminderSent);
                mailSentToUsers.push(user.email);
              } else {
                // dont do anything
              }
            } else {
              // dont do anything; we dont have to send mail post 4 reminders
            }
          }
        }
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to users: ${mailSentToUsers}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public sendInvitationAndHeaderCode = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      logger.info('sendInvitationAndHeaderCode');
      const sender: UserDTO = await this.userDAO.findByEmail(req.email);
      const platformName: string = req.body.platformName;
      const url: string = config.get<string>('mail.web-portal-redirection-url');
      let tokenStr: string;
      const jwtExpiresInVal = config.get<string>('jwt.expiresIn');
      const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
      tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);
      const tokenDTO: UserTokenDTO = new UserTokenDTO();
      tokenDTO.email = req.body.email;
      tokenDTO.expiryTime = new Date(expiresIn);
      tokenDTO.token = tokenStr;
      tokenDTO.tokenType = 'addUser';
      tokenDTO.created = { at: new Date(), by: req.email };
      await this.userTokenDAO.create(tokenDTO);
      if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
        let content = fs.readFileSync(this.headerFilePath, { encoding: 'utf8' });
        content = escapeHtml(content);
        await this.publisherMail.sendInvitationAndHeaderCode(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
          content
        );
      } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        let snippet0 = fs.readFileSync(this.mobilePISnippet0FilePath, { encoding: 'utf8' });
        let snippet1 = fs.readFileSync(this.mobilePISnippet1FilePath, { encoding: 'utf8' });
        let snippet2 = fs.readFileSync(this.mobilePISnippet2FilePath, { encoding: 'utf8' });
        let snippet3 = fs.readFileSync(this.mobilePISnippet3FilePath, { encoding: 'utf8' });

        snippet0 = escapeHtml(snippet0);
        snippet1 = escapeHtml(snippet1);
        snippet2 = escapeHtml(snippet2);
        snippet3 = escapeHtml(snippet3);

        await this.publisherMail.sendInvitationAndMobilePISnippet(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
          snippet0,
          snippet1,
          snippet2,
          snippet3
        );
      }

      return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public sendHeaderCode = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      logger.info('sendHeaderCode');
      const sender: UserDTO = await this.userDAO.findByEmail(req.email);
      const platformName: string = req.body.platformName;
      const url: string = config.get<string>('mail.web-portal-redirection-url');
      if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
        let content = fs.readFileSync(this.headerFilePath, { encoding: 'utf8' });
        content = escapeHtml(content);
        await this.publisherMail.sendHeaderCode(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/login`,
          content
        );
      } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        let snippet0 = fs.readFileSync(this.mobilePISnippet1FilePath, { encoding: 'utf8' });
        let snippet1 = fs.readFileSync(this.mobilePISnippet1FilePath, { encoding: 'utf8' });
        let snippet2 = fs.readFileSync(this.mobilePISnippet2FilePath, { encoding: 'utf8' });
        let snippet3 = fs.readFileSync(this.mobilePISnippet3FilePath, { encoding: 'utf8' });

        snippet0 = escapeHtml(snippet0);
        snippet1 = escapeHtml(snippet1);
        snippet2 = escapeHtml(snippet2);
        snippet3 = escapeHtml(snippet3);

        await this.publisherMail.sendMobilePISnippet(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/login`,
          snippet0,
          snippet1,
          snippet2,
          snippet3
        );
      }

      return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public sendInvitationAndInvocationCode = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      logger.info('sendInvitationAndInvocationCode');
      const sender: UserDTO = await this.userDAO.findByEmail(req.email);
      const platformName: string = req.body.platformName;
      const url: string = config.get<string>('mail.web-portal-redirection-url');
      let tokenStr: string;
      const jwtExpiresInVal = config.get<string>('jwt.expiresIn');
      const expiresIn = moment().add(jwtExpiresInVal, 'minutes').valueOf();
      tokenStr = await this.hashPwd.jwtToken(req.body.email, expiresIn);
      const tokenDTO: UserTokenDTO = new UserTokenDTO();
      tokenDTO.email = req.body.email;
      tokenDTO.expiryTime = new Date(expiresIn);
      tokenDTO.token = tokenStr;
      tokenDTO.tokenType = 'addUser';
      tokenDTO.created = { at: new Date(), by: req.email };
      await this.userTokenDAO.create(tokenDTO);
      let contentForDocereeLogin: string = '';
      if (req.header('zone') === '1') {
        if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.docereeUsLoginSnippetFilePath, { encoding: 'utf8' });
        } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.usMobileAISnippet1FilePath, { encoding: 'utf8' });
        }
      } else if (req.header('zone') === '2') {
        if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.docereeIndiaLoginSnippetFilePath, { encoding: 'utf8' });
        } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.indiaMobileAISnippet1FilePath, { encoding: 'utf8' });
        }
      }
      contentForDocereeLogin = escapeHtml(contentForDocereeLogin);
      let contentForDocereeLogout: string = '';
      if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
        contentForDocereeLogout = fs.readFileSync(this.docereeLogoutSnippetFilePath, { encoding: 'utf8' });
      } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        contentForDocereeLogout = fs.readFileSync(this.mobileAISnippet2FilePath, { encoding: 'utf8' });
      }
      contentForDocereeLogout = escapeHtml(contentForDocereeLogout);

      if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
        await this.publisherMail.sendInvitationAndInvocationCode(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
          contentForDocereeLogin,
          contentForDocereeLogout
        );
      } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        await this.publisherMail.sendInvitationAndInvocationCodeForMobile(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/verifytoken?token=${tokenStr}&tokenType=${tokenDTO.tokenType}`,
          contentForDocereeLogin,
          contentForDocereeLogout
        );
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  public sendInvocationCode = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
    try {
      logger.info('sendInvocationCode');
      const sender: UserDTO = await this.userDAO.findByEmail(req.email);
      const platformName: string = req.body.platformName;
      let contentForDocereeLogin: string = '';
      if (req.header('zone') === '1') {
        if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.docereeUsLoginSnippetFilePath, { encoding: 'utf8' });
        } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.usMobileAISnippet1FilePath, { encoding: 'utf8' });
        }
      } else if (req.header('zone') === '2') {
        if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.docereeIndiaLoginSnippetFilePath, { encoding: 'utf8' });
        } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
          contentForDocereeLogin = fs.readFileSync(this.indiaMobileAISnippet1FilePath, { encoding: 'utf8' });
        }
      }
      contentForDocereeLogin = escapeHtml(contentForDocereeLogin);

      let contentForDocereeLogout: string = '';
      if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
        contentForDocereeLogout = fs.readFileSync(this.docereeLogoutSnippetFilePath, { encoding: 'utf8' });
      } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        contentForDocereeLogout = fs.readFileSync(this.mobileAISnippet2FilePath, { encoding: 'utf8' });
      }
      contentForDocereeLogout = escapeHtml(contentForDocereeLogout);

      const url: string = config.get<string>('mail.web-portal-redirection-url');
      if (req.query.platformType.toLowerCase() === 'Website'.toLowerCase()) {
      await this.publisherMail.sendInvocationCode(
        req.body.email,
        sender.firstName,
        platformName,
        `${url}/login`,
        contentForDocereeLogin,
        contentForDocereeLogout
      );
      } else if (req.query.platformType.toLowerCase() === 'MobileApp'.toLowerCase()) {
        await this.publisherMail.sendInvocationCodeForMobile(
          req.body.email,
          sender.firstName,
          platformName,
          `${url}/login`,
          contentForDocereeLogin,
          contentForDocereeLogout
        );
      }
      return res.status(200).json({ type: 'Success', message: `Mail sent to ${req.body.email}` });
    } catch (err) {
      catchError(err, next);
    }
  }

  private async sendSetupCompleteButNotAddedPlatformMail(user: UserDTO, currentDate: Date,
                                                         noOfTimesReminderSent: number) {
    const url: string = config.get<string>('mail.web-portal-redirection-url');
    if (user.hasPersonalProfile) {
      if (user.isSubscribedForEmail) {
        const date = dateformat(user.created.at, 'mmmm dS, yyyy');
        await this.publisherMail.sendSetupCompleteButNotAddedPlatformMail(
          user.email,
          user.firstName,
          date,
          `${this.generalMail.getUnsubscribeLink(await this.userTokenDAO.getUserUnsubscribeToken(user.email))}`
        );
      }
    }
    user.reminder.noPlatform.mailSentAt = currentDate;
    user.reminder.noPlatform.noOfMailsSent = noOfTimesReminderSent + 1;
    this.userDAO.update(user._id, user);
  }

  private async toPublisherPlatformDTO(
    req: IAuthenticatedRequest,
  ): Promise<PublisherPlatformDTO> {
    const dto: PublisherPlatformDTO = new PublisherPlatformDTO();
    if (this.checkNotNULL(req.body.name)) {
      dto.name = req.body.name;
    } else {
      throw new HandledApplicationError(500, 'name missing');
    }
    if (this.checkNotNULL(req.body.platformType)) {
      dto.platformType = req.body.platformType;
    } else {
      throw new HandledApplicationError(500, 'platformType missing');
    }
    if (this.checkNotNULL(req.body.domainUrl)) {
      dto.domainUrl = req.body.domainUrl;
    } else {
      throw new HandledApplicationError(500, 'domainUrl missing');
    }
    if (this.checkNotNULL(req.body.typeOfSite)) {
      dto.typeOfSite = req.body.typeOfSite;
    } else {
      throw new HandledApplicationError(500, 'typeOfSite Missing');
    }
    if (this.checkNotNULL(req.body.description)) {
      dto.description = req.body.description;
    } else {
    throw new HandledApplicationError(500, 'Description missing');
    }
    dto.keywords = req.body.keywords;
    if (!req.body.excludeAdvertiserTypes || req.body.excludeAdvertiserTypes === 'null') {
      dto.excludeAdvertiserTypes = [];
    } else if (Array.isArray(req.body.excludeAdvertiserTypes)) {
      dto.excludeAdvertiserTypes = req.body.excludeAdvertiserTypes;
    } else {
      dto.excludeAdvertiserTypes = req.body.excludeAdvertiserTypes.split(',');
    }
    if (this.checkNotNULL(req.body.publisherId)) {
     dto.publisherId = await this.publisherDAO.findById(req.body.publisherId);
     if (!this.checkNotNULL(dto.publisherId)) {
      throw new HandledApplicationError(500, 'invalid publisher');
     }
    } else {
      throw new HandledApplicationError(500, 'publisher Id  missing');
    }
    dto.gaApiKey = req.body.gaApiKey;
    dto.gaAnalyticsStatus = 'Not Verified';
    dto.webrank = 0;
    dto.isDeleted = req.body.isDeleted;
    dto.isDeactivated = req.body.isDeactivated;
    dto.isHCPUploaded = req.body.isHCPUploaded;
    dto.isHeadScriptAdded = req.body.isHeadScriptAdded;
    dto.isInvocationScriptAdded = req.body.isInvocationScriptAdded;
    // Shravan : only Admin user is allowed to add HCP Script Dt:24Oct19
    if (req.body.hcpScript) {
      if (this.checkForAdminUser(req.email)) {
        dto.hcpScript = req.body.hcpScript;
      }
    }
    dto.modified = { at: new Date(), by: req.email };
    dto.zone = req.header('zone');
    dto.trustedSites = req.body.trustedSites;
    dto.appType = req.body.appType;

    return dto;
  }

  private async checkForAdminUser(email: string) {
    const user: UserDTO = await this.userDAO.findByEmail(email);
    if (user.userStatus.toLowerCase() === 'active' && user.userType.toLowerCase() === 'admin') {
      return true;
    } else {
      return false;
    }
  }

  private async checkNotNULL(data: any) {
    if (data instanceof Array) {
        if (data.length > 0) {
           return true;
          }
        return false;
    } else if (data instanceof Object) {
        if (data && (Object.keys(data).length !== 0)) {
            return true;
        }
        return false;
    } else if (typeof(data) === 'string') {
      if (data && data.length !== 0) {
          return true;
      }
      return false;
    }
  }

  private generateKey() {
    const uuidv4 = require('uuid/v4');
    const key1 = uuidv4();
    const key2 = uuidv4();
    return [key1, key2];
  }
}
