import config from 'config';
import express from 'express';
import IController from '../common/controller-interface';
import catchError from '../error/catch-error';
import IAuthenticatedRequest from '../guards/authenticated.request';
import authenticationGuard from '../guards/authentication.guard';
import { logger } from '../util/winston';
import { ZendeskHelper } from './util/zendesk-helper';

export class ZendeskController implements IController {
    public path = '/zendesk';
    public router = express.Router();
    private readonly zendeskHelper: ZendeskHelper;

    constructor() {
        this.initializeRoutes();
        this.zendeskHelper = new ZendeskHelper();
    }

    public test = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            res.json(await this.zendeskHelper.createOrganization(null, null));
        } catch (err) {
            logger.error(JSON.stringify(err));
            catchError(err, next);
        }
    }

    private initializeRoutes() {
        this.router.get(`${this.path}/test`, authenticationGuard, this.test);
    }

}
