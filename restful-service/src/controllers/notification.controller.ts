import express from 'express';
import IController from '../common/controller-interface';
import AdvertiserUserDAO from '../daos/advertiser/advertiser-user.dao';
import PublisherUserDAO from '../daos/publisher/publisher-user.dao';
import UserDAO from '../daos/user.dao';
import PublisherPlatformDTO from '../dtos/publisher/publisher-platform.dto';
import PublisherDTO from '../dtos/publisher/publisher.dto';
import UserDTO from '../dtos/user.dto';
import catchError from '../error/catch-error';
import IAuthenticatedRequest from '../guards/authenticated.request';
import authenticationGuard from '../guards/authentication.guard';
import { AdvertiserPermissions, PublisherPermissions, UserTypes } from '../util/constants';
import { NotificationHelper } from '../util/notification-helper';
import { AuthorizeAdvertiser } from './advertiser/authorize-advertiser';
import { AuthorizePublisher } from './publisher/authorize-publisher';

export class NotificationController implements IController {
    public path = '/notification';
    public router = express.Router();
    private readonly notificationHelper: NotificationHelper;
    private readonly advertiserUserDAO: AdvertiserUserDAO;
    private readonly publisherUserDAO: PublisherUserDAO;
    private readonly authorizeAdvertiser: AuthorizeAdvertiser;
    private readonly authorizePublisher: AuthorizePublisher;
    private readonly userDAO: UserDAO;

    constructor() {
        this.notificationHelper = new NotificationHelper();
        this.advertiserUserDAO = new AdvertiserUserDAO();
        this.publisherUserDAO = new PublisherUserDAO();
        this.authorizeAdvertiser = new AuthorizeAdvertiser();
        this.authorizePublisher = new AuthorizePublisher();
        this.userDAO = new UserDAO();
        this.initializeRoutes();
    }

    public getAllNotifications = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            // the notification needs to be sent to advertiser and publisher

            // when we are sending notification to advertiser/publisher - it should
            // be brand specific or platform specific or it could be generic.

            // Notification should be received for viewing brand/viewing platform along
            // with generic notification

            const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);

            if (userDTO.userType === UserTypes.advertiser) {
                const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
                const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true
                && thisBrand.zone === req.header('zone'));
                const authorizePayload = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                return res.json(await this.notificationHelper.notificationForAdvertiser(req.email, watchingBrand.brandId.toString()));

            } else if (userDTO.userType === UserTypes.publisher) {
                const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
                const watchingPlatform = publisherUser.platforms.find((thisPlatform: any) => thisPlatform.isWatching === true
                && thisPlatform.zone === req.header('zone'));
                const authorizePayload = { platformId: watchingPlatform.platformId };
                await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewDashboard);
                return res.json(await this.notificationHelper.notificationForPublisher(req.email, watchingPlatform.platformId.toString()));
            }
            return res.json({ });
        } catch (err) {
            catchError(err, next);
        }
    }

    public markNotificationAsRead = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            await this.notificationHelper.markNotificationAsRead(req.email, req.params.id);
            return res.json({ message: 'Notification marked as read successfully', success: true });
        } catch (err) {
            catchError(err, next);
        }

    }

    public markDelete = async (
        req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction
        ) => {
        try {
            await this.notificationHelper.markDelete(req.params.id);
            res.json({ message: 'Notification marked as deleted successfully', success: true });
        } catch (err) {
            catchError(err, next);
        }
    }
    public getUnreadNotificationCount = async (req: IAuthenticatedRequest, res: express.Response, next: express.NextFunction) => {
        try {
            const userDTO: UserDTO = await this.userDAO.findByEmail(req.email);

            if (userDTO.userType === UserTypes.advertiser) {
                const advertiserUser = await this.advertiserUserDAO.findByEmail(req.email);
                const watchingBrand = advertiserUser.brands.find((thisBrand: any) => thisBrand.isWatching === true
                && thisBrand.zone === req.header('zone'));
                const authorizePayload = { brandId: watchingBrand.brandId };
                await this.authorizeAdvertiser.authorize(req.email, authorizePayload, AdvertiserPermissions.viewDashboard);
                return res.json(await this.notificationHelper.unreadNotificationsForAdvertiser(req.email, watchingBrand.brandId.toString()));

            } else if (userDTO.userType === UserTypes.publisher) {
                const publisherUser = await this.publisherUserDAO.findByEmail(req.email);
                const watchingPlatform = publisherUser.platforms.find((thisPlatform: any) => thisPlatform.isWatching === true
                && thisPlatform.zone === req.header('zone'));
                const authorizePayload = { platformId: watchingPlatform.platformId };
                await this.authorizePublisher.authorize(req.email, authorizePayload, PublisherPermissions.viewDashboard);
                return res.json(await this.notificationHelper.unreadNotificationsForPublisher(req.email, watchingPlatform.platformId.toString()));
            }

            return res.json({ });
        } catch (err) {
            catchError(err, next);
        }
    }

    private initializeRoutes() {
        // notification
        this.router.get(`${this.path}/all`, authenticationGuard, this.getAllNotifications);
        this.router.put(`${this.path}/markasread/:id`, authenticationGuard, this.markNotificationAsRead);
        this.router.delete(`${this.path}/:id`, authenticationGuard, this.markDelete);
        this.router.get(`${this.path}/unreadNotificationCount`, authenticationGuard, this.getUnreadNotificationCount);
    }
}
