import UserDTO from '../dtos/user.dto';
import HandledApplicationError from '../error/handled-application-error';
import { TimeFilters } from '../util/constants';
import { logger } from '../util/winston';

export class Helper {

    public getStartDateEndDateUnitForDashboard(filter: any, brandOrPlatformCreationDate: any) {
        let startDate: Date = new Date();
        let endDate: Date = new Date();
        let unit: any;
        if (filter.endDate) {
            // if end date is passed, this will be a custom range
            startDate = new Date(
                new Date(filter.startDate).getFullYear(),
                new Date(filter.startDate).getMonth(),
                new Date(filter.startDate).getDate(),
                new Date(filter.startDate).getHours(),
                new Date(filter.startDate).getMinutes(),
                new Date(filter.startDate).getSeconds()
            );
            endDate = new Date(
                new Date(filter.endDate).getFullYear(),
                new Date(filter.endDate).getMonth(),
                new Date(filter.endDate).getDate(),
                new Date(filter.endDate).getHours(),
                new Date(filter.endDate).getMinutes(),
                new Date(filter.endDate).getSeconds()
            );
            const diffTime = Math.abs(endDate.getTime() - startDate.getTime());
            const diffHours = Math.ceil(diffTime / (1000 * 60 * 60));

            if (diffHours >= 0 && diffHours < 1 * 3 * 24) {
                unit = 'hours';
            } else if (diffHours >= 1 * 3 * 24 && diffHours < 1 * 90 * 24) {
                unit = 'days';
            } else {
                unit = 'months';
            }
        } else if (filter.selectDuration) {
            const selectDuration: string = filter.selectDuration;
            if (selectDuration.toLowerCase() === TimeFilters.last24Hours) {
                startDate.setHours(startDate.getHours() - 24);
                unit = 'hours';
            } else if (selectDuration.toLowerCase() === TimeFilters.last48Hours) {
                startDate.setHours(startDate.getHours() - 48);
                unit = 'hours';
            } else if (selectDuration.toLowerCase() === TimeFilters.last1Week) {
                startDate.setDate(startDate.getDate() - 7);
                unit = 'days';
            } else if (selectDuration.toLowerCase() === TimeFilters.last1Month) {
                startDate.setMonth(startDate.getMonth() - 1);
                unit = 'days';
            } else if (selectDuration.toLowerCase() === TimeFilters.last6Months) {
                startDate.setMonth(startDate.getMonth() - 6);
                unit = 'months';
            }
        } else {
            // this is the default date that we have to consider since the starting of the brand
            startDate = brandOrPlatformCreationDate; // setting the start date to the start of year 2019
            // as no brand can have date earlier to this date (since we have not launced at that date)
            const diffTime = Math.abs(endDate.getTime() - startDate.getTime());
            const diffHours = Math.ceil(diffTime / (1000 * 60 * 60));

            if (diffHours >= 0 && diffHours < 1 * 3 * 24) {
                unit = 'hours';
            } else if (diffHours >= 1 * 3 * 24 && diffHours < 1 * 90 * 24) {
                unit = 'days';
            } else {
                unit = 'months';
            }
        }
        return { startDate, endDate, unit };
    }

    public getXAxisPoints(startDate: Date, endDate: Date, unit: string, timezone: string): string[] {

        let clientTimezoneOffset: number = 0;
        if (!isNaN(+timezone)) {
            clientTimezoneOffset = +timezone;
        } else {
            throw new HandledApplicationError(500, `Expected timezone as a number, got ${timezone}`);
        }

        const ranges: string[] = [];
        const tempDate = new Date(startDate.getTime() - (clientTimezoneOffset * 60000));
        endDate = new Date(endDate.getTime() - (clientTimezoneOffset * 60000));

        if (unit === 'hours') {
            while (true) {
                const d = tempDate.getUTCDate();
                const h = tempDate.getUTCHours();
                ranges.push(`${d}-${h}`);
                tempDate.setHours(tempDate.getHours() + 1);
                if (tempDate.getTime() > endDate.getTime()) {
                    if (tempDate.getHours() === endDate.getHours()) {
                        ranges.push(`${tempDate.getUTCDate()}-${tempDate.getUTCHours()}`);
                    }
                    break;
                }
            }
        }
        if (unit === 'days') {
            while (true) {
                const m = tempDate.getUTCMonth();
                const d = tempDate.getUTCDate();
                ranges.push(`${m + 1}-${d}`);
                tempDate.setDate(tempDate.getDate() + 1);
                if (tempDate.getTime() > endDate.getTime()) {
                    if (tempDate.getDate() === endDate.getDate()) {
                        ranges.push(`${tempDate.getUTCMonth() + 1}-${tempDate.getUTCDate()}`);
                    }
                    break;
                }
            }
        }
        if (unit === 'months') {
            while (true) {
                const y = tempDate.getFullYear();
                const m = tempDate.getUTCMonth();
                ranges.push(`${y}-${m + 1}`);
                tempDate.setMonth(tempDate.getMonth() + 1);
                if (tempDate.getTime() > endDate.getTime()) {
                    if (tempDate.getMonth() === endDate.getMonth()) {
                        ranges.push(`${tempDate.getFullYear()}-${tempDate.getUTCMonth() + 1}`);
                    }
                    break;
                }
            }
        }
        return ranges;
    }

    public getActivationAtInstance(unit: string, timezone: string) {
        return this.getDuration('$activated.at', unit, timezone);
    }

    public getDurationInstance(unit: string, timezone: string) {
        return this.getDuration('$dateAndTime', unit, timezone);
    }

    public convert(timezone: string) {
        let h = +timezone / 60;
        h = Math.trunc(h);
        let m = +timezone % 60;
        if (m < 0) {
            m = m * -1;
        }
        if (h < 0) {
            h = h * -1;
        }
        const sign = (+timezone > 0) ? '+' : '-';
        return (`${sign}${(`0${h}`).slice(-2)}${(`0${m}`).slice(-2)}`);
    }

    public addOffsetForActivatedAt(timezone: string) {
        return this.addOffsetPrivate('activated.at', timezone);
    }

    public addOffset(timezone: string) {
        return this.addOffsetPrivate('dateAndTime', timezone);
    }

    public addOffsetPrivate(fieldName: string, timezone: string) {
        const q: any = { };
        const offset = parseInt(timezone, 10) * -1;
        q[fieldName] = { $add: [`$${fieldName}`, offset * 60 * 1000] };
        return q;
    }

    public sanitizeLastLogoutTime(userData: UserDTO): Date {
        if (userData.lastLogoutTime) {
            return userData.lastLogoutTime;
        } else {
            if (userData.lastLoginTime) {
                return userData.lastLoginTime;
            } else {
                if (userData.created && userData.created.at) {
                    return userData.created.at;
                }
            }
        }
        return new Date();
    }

    private getDuration(fieldName: string, unit: string, timezone: string) {
        let durationInstance: any = { };
        timezone = '+0000';

        switch (unit) {
            case 'months':
                durationInstance = {
                    $concat: [
                        { $substr: [{ $year: { date: fieldName, timezone } }, 0, 4] },
                        '-',
                        {
                            $cond: [
                                { $gt: [{ $month: { date: fieldName, timezone } }, 9] },
                                { $substr: [{ $month: { date: fieldName, timezone } }, 0, 2] },
                                { $substr: [{ $month: { date: fieldName, timezone } }, 0, 1] }
                            ]
                        }
                    ]
                };
                break;
            case 'days':
                durationInstance = {
                    $concat: [
                        {
                            $cond: [
                                { $gt: [{ $month: { date: fieldName, timezone } }, 9] },
                                { $substr: [{ $month: { date: fieldName, timezone } }, 0, 2] },
                                { $substr: [{ $month: { date: fieldName, timezone } }, 0, 1] }
                            ]
                        },
                        '-',
                        {
                            $cond: [
                                { $gt: [{ $dayOfMonth: { date: fieldName, timezone } }, 9] },
                                { $substr: [{ $dayOfMonth: { date: fieldName, timezone } }, 0, 2] },
                                { $substr: [{ $dayOfMonth: { date: fieldName, timezone } }, 0, 1] }
                            ]
                        }
                    ]
                };
                break;
            case 'hours':
                durationInstance = {
                    $concat: [
                        {
                            $cond: [
                                { $gt: [{ $dayOfMonth: { date: fieldName, timezone } }, 9] },
                                { $substr: [{ $dayOfMonth: { date: fieldName, timezone } }, 0, 2] },
                                { $substr: [{ $dayOfMonth: { date: fieldName, timezone } }, 0, 1] }
                            ]
                        },
                        '-',
                        {
                            $cond: [
                                { $gt: [{ $hour: { date: fieldName, timezone } }, 9] },
                                { $substr: [{ $hour: { date: fieldName, timezone } }, 0, 2] },
                                { $substr: [{ $hour: { date: fieldName, timezone } }, 0, 1] }
                            ]
                        }
                    ]
                };
                break;
            default:
                throw new HandledApplicationError(500,
                    'Unable to determine the unit while plotting performance analytics');
        }
        return durationInstance;
    }
}
